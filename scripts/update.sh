#!/usr/bin/env bash
set -exo pipefail
# don't use 'set -euxo pipefail' as the command 'deactivate' of venv produces an 'unbound variable' error.

cd /home/debian/fim_portal/

# Stop the batch services and timers.
# Ignore errors in case they are not currently running.
# The fim_portal_app.service must not be stopped, but is reloaded instead for zero-downtime.
sudo systemctl stop fim_portal_datenfelder_import.timer || true
sudo systemctl stop fim_portal_datenfelder_import.service || true
sudo systemctl stop fim_portal_leistungen_import.timer || true
sudo systemctl stop fim_portal_leistungen_import.service || true
sudo systemctl stop fim_portal_prozesse_import.timer || true
sudo systemctl stop fim_portal_prozesse_import.service || true
sudo systemctl stop fim_portal_gebiet_import.timer || true
sudo systemctl stop fim_portal_gebiet_import.service || true
sudo systemctl stop fim_portal_link_check.timer || true
sudo systemctl stop fim_portal_link_check.service || true
sudo systemctl stop fim_portal_kataloge_import.timer || true
sudo systemctl stop fim_portal_kataloge_import.service || true
sudo systemctl stop fim_portal_internal_update.service || true
sudo systemctl stop fim_portal_update_cms_data.timer || true
sudo systemctl stop fim_portal_update_cms_data.service || true

# Install the new package to the virtual env
source ./ENV_3_11/bin/activate
pip uninstall -y fimportal
pip install -r requirements.txt
pip install ./fimportal-0.1.0-py3-none-any.whl

# Exit virtual env
deactivate

# Execute all new migrations
sudo systemd-run \
    -p LoadCredentialEncrypted=config:/etc/fimportal.config.json.cred \
    --wait -P \
    /home/debian/fim_portal/ENV_3_11/bin/python -m fimportal migrate

# Compile the CMS data
sudo systemd-run \
    -p LoadCredentialEncrypted=config:/etc/fimportal.config.json.cred \
    --wait -P \
    /home/debian/fim_portal/ENV_3_11/bin/python -m fimportal compile-cms-content

# Reload the app service to apply the new code.
sudo systemctl reload fim_portal_app.service

# Restart the services and timers.
# Do not start the import manually.
sudo systemctl start fim_portal_internal_update.service
sudo systemctl start fim_portal_datenfelder_import.timer
sudo systemctl start fim_portal_leistungen_import.timer
sudo systemctl start fim_portal_gebiet_import.timer
sudo systemctl start fim_portal_kataloge_import.timer
sudo systemctl start fim_portal_update_cms_data.timer
# currently deactivated
# sudo systemctl start link_check.timer
# sudo systemctl start fim_portal_prozesse_import.timer

