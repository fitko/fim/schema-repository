from dataclasses import dataclass
from enum import Enum, IntEnum
from typing import Annotated, TypeVar

from fimportal import xml
from fimportal.xmlstruct import (
    Attribute as XmlAttribute,
)
from fimportal.xmlstruct import (
    Encoding,
    RequiredValueEncoding,
    XmlElement,
)
from fimportal.xmlstruct import (
    Value as XmlValue,
)

FREIGABE_STATUS_CODE_LIST_URI = "urn:xoev-de:xprozess:codeliste:status"
FREIGABE_STATUS_CODE_LIST_VERSION = "2022-07-12"


@dataclass(slots=True)
class RawCode:
    list_uri: Annotated[str, XmlAttribute("listURI", namespace=None)]
    list_version_id: Annotated[str, XmlAttribute("listVersionID", namespace=None)]
    code: Annotated[str, XmlValue(namespace=None)]

    def get_title(self, label_mapping: dict[int, str] | None) -> str:
        if label_mapping is not None:
            label = label_mapping[int(self.code)]
            return f"{label} ({self.code})"

        return self.code


E = TypeVar("E", bound=Enum)


def decode_raw_code(node: XmlElement) -> str:
    """
    Create a specialized parser for nested code items of the form

    ```xml
    <outer>
        <code>Actual Value</code>
    </outer>
    ```

    directly into a str, without needing to parse the nested structure
    into a custom dataclass with only one member `code`.
    """

    if len(node) == 0:
        raise xml.MissingValue("code")

    code_node = node[0]
    if code_node.tag != "code":
        raise xml.UnexpectedChildNodeException(code_node.tag)

    return xml.parse_token(code_node)


RawCodeEncoding = RequiredValueEncoding(decode=decode_raw_code)


def create_code_encoding(cls: type[E]) -> Encoding[E]:
    """
    Create an encoding to parse a code directly into an enum
    """

    if issubclass(cls, IntEnum):

        def _decode(node: XmlElement) -> E:
            value = decode_raw_code(node)
            try:
                return cls(int(value))
            except ValueError as error:
                raise xml.ParserException(str(error)) from error

    else:

        def _decode(node: XmlElement) -> E:
            value = decode_raw_code(node)
            try:
                return cls(value)
            except ValueError as error:
                raise xml.ParserException(str(error)) from error

    return RequiredValueEncoding(decode=_decode)


class FreigabeStatus(IntEnum):
    """
    See: https://www.xrepository.de/details/urn:xoev-de:xprozess:codeliste:status
    """

    IN_PLANUNG = 1
    IN_BEARBEITUNG = 2
    ENTWURF = 3
    METHODISCH_FREIGEGEBEN = 4
    FACHLICH_FREIGEGEBEN_SILBER = 5
    FACHLICH_FREIGEGEBEN_GOLD = 6
    INAKTIV = 7
    VORGESEHEN_ZUM_LOESCHEN = 8

    @staticmethod
    def from_label(value: str) -> "FreigabeStatus":
        return STRING_TO_FREIGABE_STATUS[value]

    def to_label(self) -> str:
        return FREIGABE_STATUS_TO_STRING[self]

    def __str__(self) -> str:
        return self.to_label()


STRING_TO_FREIGABE_STATUS: dict[str, FreigabeStatus] = {
    "in Planung": FreigabeStatus.IN_PLANUNG,
    "in Bearbeitung": FreigabeStatus.IN_BEARBEITUNG,
    "Entwurf": FreigabeStatus.ENTWURF,
    "methodisch freigegeben": FreigabeStatus.METHODISCH_FREIGEGEBEN,
    "fachlich freigegeben (silber)": FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER,
    "fachlich freigegeben (gold)": FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
    "inaktiv": FreigabeStatus.INAKTIV,
    "vorgesehen zum Löschen": FreigabeStatus.VORGESEHEN_ZUM_LOESCHEN,
}

FREIGABE_STATUS_TO_STRING: dict[FreigabeStatus, str] = {
    status: label for label, status in STRING_TO_FREIGABE_STATUS.items()
}


class Anwendungsgebiet(Enum):
    SCHLESWIG_HOLSTEIN = "01"
    HAMBURG = "02"
    NIEDERSACHSEN = "03"
    BREMEN = "04"
    NORDRHEIN_WESTFALEN = "05"
    HESSEN = "06"
    RHEINLAND_PFALZ = "07"
    BADEN_WUERTTEMBERG = "08"
    BAYERN = "09"
    SAARLAND = "10"
    BERLIN = "11"
    BRANDENBURG = "12"
    MECKLENBURG_VORPOMMERN = "13"
    SACHSEN = "14"
    SACHSEN_ANHALT = "15"
    THUERINGEN = "16"
    BUND = "17"

    def to_label(self) -> str:
        if self == Anwendungsgebiet.BUND:
            return "Bund"
        return BUNDESLAND_TO_STRING[Bundesland(self.value)]


class Bundesland(Enum):
    """
    See: https://www.xrepository.de/details/urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:bundesland
    """

    SCHLESWIG_HOLSTEIN = "01"
    HAMBURG = "02"
    NIEDERSACHSEN = "03"
    BREMEN = "04"
    NORDRHEIN_WESTFALEN = "05"
    HESSEN = "06"
    RHEINLAND_PFALZ = "07"
    BADEN_WUERTTEMBERG = "08"
    BAYERN = "09"
    SAARLAND = "10"
    BERLIN = "11"
    BRANDENBURG = "12"
    MECKLENBURG_VORPOMMERN = "13"
    SACHSEN = "14"
    SACHSEN_ANHALT = "15"
    THUERINGEN = "16"

    @staticmethod
    def from_label(value: str) -> "Bundesland":
        return STRING_TO_BUNDESLAND[value]

    def to_label(self) -> str:
        return BUNDESLAND_TO_STRING[self]

    def to_anwendungsgebiet(self) -> Anwendungsgebiet:
        return Anwendungsgebiet(self.value)


STRING_TO_BUNDESLAND: dict[str, Bundesland] = {
    "Schleswig-Holstein": Bundesland.SCHLESWIG_HOLSTEIN,
    "Hamburg": Bundesland.HAMBURG,
    "Niedersachsen": Bundesland.NIEDERSACHSEN,
    "Bremen": Bundesland.BREMEN,
    "Nordrhein-Westfalen": Bundesland.NORDRHEIN_WESTFALEN,
    "Hessen": Bundesland.HESSEN,
    "Rheinland-Pfalz": Bundesland.RHEINLAND_PFALZ,
    "Baden-Württemberg": Bundesland.BADEN_WUERTTEMBERG,
    "Bayern": Bundesland.BAYERN,
    "Saarland": Bundesland.SAARLAND,
    "Berlin": Bundesland.BERLIN,
    "Brandenburg": Bundesland.BRANDENBURG,
    "Mecklenburg-Vorpommern": Bundesland.MECKLENBURG_VORPOMMERN,
    "Sachsen": Bundesland.SACHSEN,
    "Sachsen-Anhalt": Bundesland.SACHSEN_ANHALT,
    "Thüringen": Bundesland.THUERINGEN,
}

BUNDESLAND_TO_STRING: dict[Bundesland, str] = {
    status: label for label, status in STRING_TO_BUNDESLAND.items()
}


@dataclass(slots=True)
class VerwaltungspolitischeKodierung:
    """
    XZuFi sometimes adds a label to the xml node.
    """

    kreis: Annotated[str, RawCodeEncoding] | None
    bezirk: Annotated[str, RawCodeEncoding] | None
    bundesland: Annotated[Bundesland, create_code_encoding(Bundesland)] | None
    gemeindeschluessel: Annotated[str, RawCodeEncoding] | None
    regionalschluessel: Annotated[str, RawCodeEncoding] | None
    nation: Annotated[str, RawCodeEncoding] | None
    gemeindeverband: Annotated[str, RawCodeEncoding] | None
