"""
Serialize a code list into xml.

We only need a very limited amount of features, so the code contains some guards against
unsupported input.
"""

import logging
import xml.etree.ElementTree as ET


from .parse import (
    COLUMN,
    COLUMN_REF,
    COLUMN_SET,
    DATA,
    KEY,
    ROW,
    SHORT_NAME,
    SIMPLE_CODE_LIST,
    SIMPLE_VALUE,
    VALUE,
    Identifier,
    CANONICAL_URI,
    CANONICAL_VERSION_URI,
    CODE_LIST,
    IDENTIFICATION,
    VERSION,
    CodeList,
)


logger = logging.getLogger(__name__)


def serialize_code_list(code_list: CodeList) -> bytes:
    root = _serialize_code_list(code_list)

    xml_content = ET.tostring(
        root,
        encoding="utf-8",
        xml_declaration=True,
    )

    return xml_content


def _serialize_code_list(code_list: CodeList) -> ET.Element:
    root = ET.Element(CODE_LIST)

    root.append(_serialize_identification(code_list.identifier))
    root.append(_serialize_column_set(code_list))
    root.append(_serialize_simple_code_list(code_list))

    return root


def _serialize_identification(identification: Identifier) -> ET.Element:
    element = ET.Element(IDENTIFICATION)

    ET.SubElement(element, CANONICAL_URI).text = identification.canonical_uri
    ET.SubElement(
        element, CANONICAL_VERSION_URI
    ).text = identification.canonical_version_uri

    if identification.version is not None:
        ET.SubElement(element, VERSION).text = identification.version

    return element


def _serialize_column_set(code_list: CodeList) -> ET.Element:
    # NOTE(Felix): We do not support this feature.
    assert (
        code_list.default_name_key is not None
    ), "Serailizing code lists without a name column is not supported"

    element = ET.Element(COLUMN_SET)

    column = ET.SubElement(
        element, COLUMN, {"Id": code_list.default_code_key, "Use": "required"}
    )
    ET.SubElement(column, SHORT_NAME).text = "Code"
    ET.SubElement(column, DATA, {"Type": "string"})

    column = ET.SubElement(
        element, COLUMN, {"Id": code_list.default_name_key, "Use": "required"}
    )
    ET.SubElement(column, SHORT_NAME).text = "Name"
    ET.SubElement(column, DATA, {"Type": "string"})

    key = ET.SubElement(element, KEY, {"Id": "codeKey"})
    ET.SubElement(key, SHORT_NAME).text = "CodeKey"
    ET.SubElement(key, COLUMN_REF, {"Ref": code_list.default_code_key})

    key = ET.SubElement(element, KEY, {"Id": "codenameKey"})
    ET.SubElement(key, SHORT_NAME).text = "CodenameKey"
    ET.SubElement(key, COLUMN_REF, {"Ref": code_list.default_name_key})

    return element


def _serialize_simple_code_list(code_list: CodeList) -> ET.Element:
    # NOTE(Felix): We do not support this feature.
    assert (
        len(code_list.columns) == 2
    ), "Code list must have exaclty one key and code column to be serialized"

    element = ET.Element(SIMPLE_CODE_LIST)

    code_key = code_list.default_code_key
    name_key = code_list.default_name_key
    assert name_key is not None

    code_column = code_list.get_key_column()
    name_column = code_list.get_key_column(code_list.default_name_key)

    for code, name in zip(code_column, name_column):
        row = ET.SubElement(element, ROW)
        row.append(_create_value(ref=code_key, value=code))
        row.append(_create_value(ref=name_key, value=name))

    return element


def _create_value(ref: str, value: str) -> ET.Element:
    element = ET.Element(VALUE, {"ColumnRef": ref})
    ET.SubElement(element, SIMPLE_VALUE).text = value

    return element
