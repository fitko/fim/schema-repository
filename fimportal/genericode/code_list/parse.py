"""
A minimal parser for Genericode code lists.

Since xdf3, the actual columns used from the code list are potentially defined by the schema.
We therefore parse all available columns and lists.

## Usage

Use the top-level function `parse_code_list` to parse a string containing the xml data or
`load_code_list` to directly parse the code list from a file.

## XRepository

The code lists from XRepository often do not include name space specifiers within
the xml document.
To still be able to also parse those, all namespaces are ignored within a code list.
"""

from dataclasses import dataclass
from pathlib import Path

from fimportal import xml


GENERICODE_NS = "{http://docs.oasis-open.org/codelist/ns/genericode/1.0/}"
CODE_LIST = f"{GENERICODE_NS}CodeList"

IDENTIFICATION = "Identification"
SHORT_NAME = "ShortName"
LONG_NAME = "LongName"
VERSION = "Version"
CANONICAL_URI = "CanonicalUri"
CANONICAL_VERSION_URI = "CanonicalVersionUri"

COLUMN_SET = "ColumnSet"
DATA = "Data"
COLUMN = "Column"
KEY = "Key"
COLUMN_REF = "ColumnRef"

SIMPLE_CODE_LIST = "SimpleCodeList"
ROW = "Row"
VALUE = "Value"
SIMPLE_VALUE = "SimpleValue"


class GenericodeException(Exception):
    pass


class ParserException(GenericodeException):
    pass


class UnsupportedFeatureException(GenericodeException):
    pass


@dataclass(slots=True, frozen=True)
class Identifier:
    canonical_uri: str
    version: str | None
    canonical_version_uri: str


@dataclass(slots=True)
class KeyColumn:
    id: str
    ref: str | None


@dataclass(slots=True)
class ColumnSet:
    default_code_key: str
    default_name_key: str | None
    columns: set[str]


@dataclass(slots=True)
class CodeList:
    identifier: Identifier
    default_code_key: str
    default_name_key: str | None
    columns: dict[str, list[str | None]]

    def has_column(self, column: str) -> bool:
        return column in self.columns

    def get_key_column(self, column: str | None = None) -> list[str]:
        """
        Return either the specified column or the default key column
        as a unique list of values.

        This fails if the requested column has duplicate or None values.
        """
        if column is None:
            column = self.default_code_key

        items = self.columns.get(column)
        if items is None:
            raise GenericodeException(f"Unknown column '{column}'")

        item_set: set[str] = set()
        for item in items:
            if item is None:
                raise GenericodeException(f"Missing value in key column '{column}'")
            elif item in item_set:
                raise GenericodeException(f"Duplicate value '{item}' in key column")
            else:
                item_set.add(item)

        # We checked already that `None` is not in the list
        return items  # type: ignore

    def _get_column(self, column: str) -> list[str]:
        """
        Return the specified column.

        This fails if the requested column has None values.
        """
        items = self.columns.get(column)
        if items is None:
            raise GenericodeException(f"Unknown column '{column}'")

        for item in items:
            if item is None:
                raise GenericodeException(f"Missing value in key column '{column}'")

        # We checked already that `None` is not in the list
        return items  # type: ignore

    def select_name_column(self) -> list[str]:
        """
        Try to select a name column.
        If no default column was specified in the code list, try some
        conventions from FRED Classic.
        """
        if self.default_name_key is not None:
            return self._get_column(self.default_name_key)
        elif self.has_column("Codename"):
            return self._get_column("Codename")
        elif self.has_column("name"):
            return self._get_column("name")
        else:
            raise GenericodeException(
                f"Cannot select name column for code list {self.identifier.canonical_version_uri}"
            )


CodeListWithContent = tuple[CodeList, str]


@dataclass(slots=True)
class Identification:
    version: str | None
    canonical_uri: str
    canonical_version_uri: str


Row = dict[str, str]


def load_code_list(filepath: str | Path) -> CodeList:
    """
    Load and parse a code list from a file.
    """

    with open(filepath, "rb") as file:
        return parse_code_list(file.read())


def parse_code_list(input: bytes | str) -> CodeList:
    """
    Parse a code list from the provided input.
    """

    if isinstance(input, str):
        input = input.encode("utf-8")

    parser = xml.XmlParser(input)

    try:
        parser.expect_child(CODE_LIST)

        identification = xml.ParsedValue[Identification]()
        column_set = xml.ParsedValue[ColumnSet]()
        rows = xml.ParsedValue[list[Row]]()

        while (child := parser.next_child()) is not None:
            local_name = xml.get_local_name(child.tag)

            if local_name == IDENTIFICATION:
                identification.set(_parse_identification(parser))
            elif local_name == COLUMN_SET:
                column_set.set(_parse_column_set(parser))
            elif local_name == SIMPLE_CODE_LIST:
                rows.set(_parse_simple_code_list(parser))
            else:
                parser.skip_node()

        identification = identification.expect(IDENTIFICATION)
        column_set = column_set.expect(COLUMN_SET)
        rows = rows.expect(SIMPLE_CODE_LIST)

        return CodeList(
            identifier=Identifier(
                canonical_uri=identification.canonical_uri,
                version=identification.version,
                canonical_version_uri=identification.canonical_version_uri,
            ),
            default_code_key=column_set.default_code_key,
            default_name_key=column_set.default_name_key,
            columns=_filter_items(column_set, rows),
        )
    except xml.ParserException as exception:
        raise ParserException(
            f"Could not parse code list: {str(exception)}, line {parser.current_line}"
        ) from exception


def parse_code_list_with_content(data: bytes) -> tuple[CodeList, str]:
    code_list = parse_code_list(data)
    try:
        content = data.decode("utf-8")
    except UnicodeDecodeError:
        content = data.decode("ISO-8859-1")
    return (
        CodeList(
            identifier=code_list.identifier,
            default_code_key=code_list.default_code_key,
            default_name_key=code_list.default_name_key,
            columns=code_list.columns,
        ),
        content,
    )


def _parse_identification(parser: xml.XmlParser) -> Identification:
    version = xml.ParsedValue[str]()
    canonical_uri = xml.ParsedValue[str]()
    canonical_version_uri = xml.ParsedValue[str]()

    while (child := parser.next_child()) is not None:
        local_name = xml.get_local_name(child.tag)

        if local_name == VERSION:
            version.set(parser.parse_optional_value())
        elif local_name == CANONICAL_URI:
            canonical_uri.set(parser.parse_value(CANONICAL_URI))
        elif local_name == CANONICAL_VERSION_URI:
            canonical_version_uri.set(parser.parse_value(CANONICAL_VERSION_URI))
        else:
            parser.skip_node()

    return Identification(
        version=version.get(),
        canonical_uri=canonical_uri.expect(CANONICAL_URI),
        canonical_version_uri=canonical_version_uri.expect(CANONICAL_VERSION_URI),
    )


def _parse_column_set(parser: xml.XmlParser) -> ColumnSet:
    key_columns: list[KeyColumn] = []
    columns: set[str] = set()

    while (child := parser.next_child()) is not None:
        local_name = xml.get_local_name(child.tag)

        if local_name == KEY:
            key_columns.append(_parse_key_column(parser, child.get("Id", None)))
        elif local_name == COLUMN:
            key_id: str | None = child.get("Id", None)
            if key_id is None:
                raise xml.ParserException("Missing attribute `Id` in <Column> node")

            if key_id in columns:
                raise xml.ParserException(f"Duplicate column id '{key_id}'")

            columns.add(key_id)
            parser.skip_node()
        else:
            parser.skip_node()

    default_code_key = _get_default_code_key(key_columns)
    default_name_key = _get_default_name_key(key_columns)

    return ColumnSet(
        default_code_key=default_code_key,
        default_name_key=default_name_key,
        columns=columns,
    )


def _parse_key_column(parser: xml.XmlParser, key_id: str | None) -> KeyColumn:
    if key_id is None:
        raise xml.ParserException("Missing attribute `Id` in <Key> node")

    references: list[str | None] = []

    while (child := parser.next_child()) is not None:
        local_name = xml.get_local_name(child.tag)

        if local_name == COLUMN_REF:
            if len(references) > 0:
                raise UnsupportedFeatureException(
                    "Multi-Column Keys are currently not supported"
                )

            ref: str | None = child.get("Ref", None)
            if ref is None:
                raise xml.ParserException("Missing attribute `Ref` in <ColumnRef> node")

            if ref == "":
                ref = None

            references.append(ref)

            # Always skip the rest of the node, as the content is not necessary for
            # parsing.
            parser.skip_node()
        else:
            parser.skip_node()

    if len(references) == 0:
        raise xml.ParserException("Missing ColumnRef node")

    assert len(references) == 1

    return KeyColumn(id=key_id, ref=references[0])


def _parse_simple_code_list(parser: xml.XmlParser) -> list[Row]:
    rows: list[Row] = []

    while (child := parser.next_child()) is not None:
        local_name = xml.get_local_name(child.tag)

        if local_name == ROW:
            row = _parse_row(parser)
            if row is not None:
                rows.append(row)
        else:
            parser.skip_node()

    return rows


def _parse_row(parser: xml.XmlParser) -> Row | None:
    row: Row = {}

    while (child := parser.next_child()) is not None:
        local_name = xml.get_local_name(child.tag)

        if local_name == VALUE:
            column_ref = child.get("ColumnRef", None)
            if column_ref is None:
                raise xml.ParserException(
                    "Missing attribute `ColumnRef` in <Value> node"
                )

            if column_ref in row:
                raise xml.ParserException(f"Duplicate reference to column {column_ref}")

            value = _parse_value(parser)
            if value is not None:
                row[column_ref] = value
        else:
            parser.skip_node()

    if len(row) == 0:
        return None

    return row


def _parse_value(parser: xml.XmlParser) -> str | None:
    """
    Parse nodes of the form:

    ```xml
    <gc:Value ColumnRef="name">
        <gc:SimpleValue>Anschrift des Antragstellers</gc:SimpleValue>
    </gc:Value>
    ```
    """

    value = xml.ParsedValue[str]()

    while (child := parser.next_child()) is not None:
        local_name = xml.get_local_name(child.tag)

        if local_name == SIMPLE_VALUE:
            value.set(parser.parse_optional_value())
        else:
            parser.skip_node()

    return value.get()


def _filter_items(
    column_set: ColumnSet, rows: list[Row]
) -> dict[str, list[str | None]]:
    columns = {key: [] for key in column_set.columns}

    for row in rows:
        for key in column_set.columns:
            columns[key].append(row.get(key))

    return columns


def _get_default_code_key(key_columns: list[KeyColumn]) -> str:
    """
    Extract the default code column of the code list based on the provided
    key columns.

    This is only based on conventions of how the keys are defined in different contexts.
    For example, XRepository has a different convention than FRED classic.
    This function should extract the correct default key in both szenarios.
    """

    if len(key_columns) == 0:
        raise xml.ParserException("Missing Key node")

    for key_column in key_columns:
        # First look for the default key as defined in FRED Classic.
        # The key sometimes has an empty reference, in which case the column names
        # are standarized. The column 'Code' is then always the default key column.
        if key_column.id.lower() == "codekey":
            return key_column.ref or "Code"

    # Just return the first available key column.
    # This should be okay, as xdf3 is not making any guaranties about which column is selected by
    # default if the user does not explicitly specify a `codeKey` within the xdf3 schema.
    for key_column in key_columns:
        if key_column.ref is not None:
            return key_column.ref

    raise xml.ParserException("Cannot find key column")


def _get_default_name_key(key_columns: list[KeyColumn]) -> str | None:
    if len(key_columns) == 0:
        raise xml.ParserException("Missing Key node")

    for key_column in key_columns:
        if key_column.id.lower() == "codenamekey":
            return key_column.ref

    return None
