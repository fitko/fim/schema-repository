<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xzufi="http://xoev.de/schemata/xzufi/2_2_0" targetNamespace="http://xoev.de/schemata/xzufi/2_2_0" version="2.2.0" elementFormDefault="qualified" attributeFormDefault="unqualified">
  <xs:annotation>
    <xs:documentation>Das Paket Transfer enthält die Nachrichten und Datentypen welche zu einer einfachen (schreibenden) Datenübermittlung dienen. Es wird hier nicht dem serviceorientierten Ansatz (Anfrage/Antwort) der anderen Pakete gefolgt.</xs:documentation>
    <xs:appinfo>
      <standard>
        <nameLang>XZuFi - XÖV-Standard für Zuständigkeitsfinder</nameLang>
        <nameKurz>XZuFi</nameKurz>
        <nameTechnisch>xzufi</nameTechnisch>
        <kennung>urn:xoev-de:fim:standard:xzufi</kennung>
        <beschreibung>XZuFi standardisiert den von Produkt und Hersteller unabhängigen Austausch von Informationen zu Verwaltungsdienstleistungen, Online-Diensten, Gebieten, Formularen und den hierfür zuständigen Organisationseinheiten im Kontext von Zuständigkeitsfindern, Bürger- und Unternehmensinformationssystemen und Leistungskatalogen. Eine Vielzahl von Systemen im öffentlichen Bereich benötigt Daten, die originär in Zuständigkeitsfindern erhoben bzw. verwaltet werden. Durch die Standardisierung des Datenaustausches wird die Interoperabilität dieser Systeme erhöht. Diese Systeme stellen normalerweise umfangreiche Schnittstellen zur Verfügung und sind hierüber mit anderen Systemen vernetzt. XZuFi definiert unabhängig von Programm und Hersteller einen Standard, um einem standardisierten Datenaustausch zwischen verschiedenen Systemen zu ermöglichen.</beschreibung>
      </standard>
      <versionStandard>
        <version>2.2.0</version>
        <versionXOEVHandbuch>2.1</versionXOEVHandbuch>
        <versionXGenerator>2.6.1</versionXGenerator>
        <versionModellierungswerkzeug>18.5</versionModellierungswerkzeug>
        <nameModellierungswerkzeug>MagicDraw</nameModellierungswerkzeug>
      </versionStandard>
    </xs:appinfo>
  </xs:annotation>
  <xs:include schemaLocation="xzufi-baukasten.xsd" />
  <xs:complexType name="KontaktpersonTransferobjekt">
    <xs:annotation>
      <xs:documentation>Spezielles Transferobjekt für Personen. Dieses kann die ID der Organisationseinheiten beinhalten.</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="organisationseinheitID" minOccurs="0" maxOccurs="unbounded" type="xzufi:Identifikator">
        <xs:annotation>
          <xs:documentation>ID der übergeordneten Objekt(e), d.h. die IDs der Organisationseinheiten, welchen die Person zugeordnet ist.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="kontaktperson" maxOccurs="unbounded" type="xzufi:Kontaktperson">
        <xs:annotation>
          <xs:documentation>Die Kontaktperson(en).</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="LoescheObjekt">
    <xs:annotation>
      <xs:documentation>Datentyp zum Löschen von Objekten.</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="id" type="xzufi:Identifikator">
        <xs:annotation>
          <xs:documentation>ID des zu löschenden Objekts.</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
    <xs:attribute name="klasse" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>Optionale Angabe der Objektklasse des zu löschenden Objekts. Dies ist insbesondere notwendig, wenn die IDs im Zielsystem nicht klassenübergreifend eindeutig sind. Angegeben muss hier exakt der Name der Klasse aus dem Baukasten, wie z.B. "Onlinedienst", "Gebiet", "Zustaendigkeit" oder "Kontaktperson". Bei Klassen mit Veerbung wird immer die oberste Klasse angegeben.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:complexType>
  <xs:complexType name="SchreibeObjekt">
    <xs:annotation>
      <xs:documentation>Datentyp zur schreibenden Übermittlung von Objekten.</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="kennzeichenNeu" minOccurs="0" type="xs:boolean">
        <xs:annotation>
          <xs:documentation>Ist das Kennzeichen "true", handelt es sich aus Sicht des sendenden Systems um ein neues Objekt, bei "false" um eine Aktualisierung. Ist das Kennzeichen gar nicht gesetzt, handelt es sich um ein "CreateUpdate".</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:group ref="xzufi:TransferObjektklassenAuswahl">
        <xs:annotation>
          <xs:documentation>Auswahl der eigentlichen Objektklasse.</xs:documentation>
        </xs:annotation>
      </xs:group>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ZustaendigkeitTransferObjekt">
    <xs:annotation>
      <xs:documentation>Spezielles Transferobjekt für Zuständigkeiten. Dieses kann die ID des übergeordneten Objekts beinhalten.</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="uebergeordnetesObjektID" minOccurs="0" type="xzufi:Identifikator">
        <xs:annotation>
          <xs:documentation>ID des übergeordneten Objekt, d.h. die ID der Organisationseinheit, des Onlinedienstes oder Person, welcher die Zuständigkeit zugeordnet ist.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="zustaendigkeit" maxOccurs="unbounded" type="xzufi:Zustaendigkeit">
        <xs:annotation>
          <xs:documentation>Das eigentliche Zuständigkeitsobjekt (mit Ableitungen).</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:group name="TransferObjektklassenAuswahl">
    <xs:annotation>
      <xs:documentation>Hilfsgruppe zur Auswahl des gewünschten Datentyps. Kontaktpersonen können hier auch separat übertragen werden, falls diese unabhängig von OEs in einem Verzeichnis gepflegt werden.</xs:documentation>
    </xs:annotation>
    <xs:choice>
      <xs:element name="leistung" type="xzufi:Leistung" />
      <xs:element name="spezialisierung" type="xzufi:LeistungSpezialisierung" />
      <xs:element name="organisationseinheit" type="xzufi:Organisationseinheit" />
      <xs:element name="person" type="xzufi:KontaktpersonTransferobjekt" />
      <xs:element name="formular" type="xzufi:Formular" />
      <xs:element name="gebiet" type="xzufi:Gebiet" />
      <xs:element name="zustaendigkeitTransferObjekt" type="xzufi:ZustaendigkeitTransferObjekt" />
      <xs:element name="ozgLeistung" type="xzufi:OZGLeistung" />
      <xs:element name="onlinedienst" type="xzufi:Onlinedienst" />
      <xs:element name="leistungskategorie" type="xzufi:Leistungskategorie" />
      <xs:element name="organisationseinheitskategorie" type="xzufi:Organisationseinheitskategorie" />
    </xs:choice>
  </xs:group>
  <xs:group name="TransferOperation">
    <xs:annotation>
      <xs:documentation>Die TransferOperation besteht entweder aus einer Löschanweisung oder einer schreibenden Datenübermittlung.</xs:documentation>
    </xs:annotation>
    <xs:choice>
      <xs:element name="loesche" type="xzufi:LoescheObjekt" />
      <xs:element name="schreibe" type="xzufi:SchreibeObjekt" />
    </xs:choice>
  </xs:group>
  <xs:element name="transfer.rueckantwort.040501">
    <xs:annotation>
      <xs:appinfo>
        <rechtsgrundlage>Keine Angabe</rechtsgrundlage>
      </xs:appinfo>
      <xs:documentation>Nachricht zur Übertragungen einer Rückanwort auf Transfer-Nachrichten.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:complexContent>
        <xs:extension base="xzufi:Basisnachricht">
          <xs:sequence>
            <xs:element name="referenzierteNachrichtUUID" type="xs:normalizedString">
              <xs:annotation>
                <xs:documentation>UUID (Universally Unique Identifier) der Nachricht, auf welche hier die Rückantwort erfolgt.</xs:documentation>
              </xs:annotation>
            </xs:element>
            <xs:element name="antwortrueckgabecode" type="xzufi:Code.NachrichtAntwortCode">
              <xs:annotation>
                <xs:documentation>Rückgabecode der Antwortnachricht</xs:documentation>
              </xs:annotation>
            </xs:element>
            <xs:element name="antwortrueckgabezusatz" minOccurs="0" type="xs:string">
              <xs:annotation>
                <xs:documentation>Textuelle Zusatzinformationen zum Rückgabecode der Antwort.</xs:documentation>
              </xs:annotation>
            </xs:element>
          </xs:sequence>
        </xs:extension>
      </xs:complexContent>
    </xs:complexType>
  </xs:element>
  <xs:element name="transfer.operation.040502">
    <xs:annotation>
      <xs:appinfo>
        <rechtsgrundlage>Keine Angabe</rechtsgrundlage>
      </xs:appinfo>
      <xs:documentation>Nachricht zur einfachen Übermittlungen von Datenobjekten aus dem XZuFi-Baukasten oder deren Löschung. Die Nachricht kann mit transfer.rueckantwort.xxx beantwortet werden.
Bei der Übertragung soll sichergestellt sein, dass die Daten redundanzfrei und konsistent übertragen werden. D.h. innerhalb eines Exports, sollen sich Daten nicht doppeln und zu jeder Referenz auch das referenzierte Objekt logisch aufschlüsselbar sein.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:complexContent>
        <xs:extension base="xzufi:Basisnachricht">
          <xs:sequence>
            <xs:element name="transaktionID" minOccurs="0" type="xs:normalizedString">
              <xs:annotation>
                <xs:documentation>Angabe einer optionalen ID für eine Transaktion. Über diese können z.B.  mehrere Nachrichten einen Import/Export-Vorgang zugeordnet werden.</xs:documentation>
              </xs:annotation>
            </xs:element>
            <xs:element name="transaktionPosition" minOccurs="0" type="xs:unsignedShort">
              <xs:annotation>
                <xs:documentation>Position in der Reihenfolge aller Nachrichten in der aktuellen Transaktion.</xs:documentation>
              </xs:annotation>
            </xs:element>
            <xs:group ref="xzufi:TransferOperation" minOccurs="0" maxOccurs="unbounded">
              <xs:annotation>
                <xs:documentation>Hier werden alle Operationen angegeben. Diese können vom Typ her gemischt sein. Das empfangende System soll die Daten in der hier angegebenen Reihenfolge abarbeiten.</xs:documentation>
              </xs:annotation>
            </xs:group>
          </xs:sequence>
        </xs:extension>
      </xs:complexContent>
    </xs:complexType>
  </xs:element>
</xs:schema>

