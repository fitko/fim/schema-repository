<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xzufi="http://xoev.de/schemata/xzufi/2_2_0" targetNamespace="http://xoev.de/schemata/xzufi/2_2_0" version="2.2.0" elementFormDefault="qualified" attributeFormDefault="unqualified">
  <xs:annotation>
    <xs:documentation>Das Paket Gebiete enthält die Nachrichten und Datentypen zur Abfrage und Rückgabe von Daten im Kontext von Gebieten.</xs:documentation>
    <xs:appinfo>
      <standard>
        <nameLang>XZuFi - XÖV-Standard für Zuständigkeitsfinder</nameLang>
        <nameKurz>XZuFi</nameKurz>
        <nameTechnisch>xzufi</nameTechnisch>
        <kennung>urn:xoev-de:fim:standard:xzufi</kennung>
        <beschreibung>XZuFi standardisiert den von Produkt und Hersteller unabhängigen Austausch von Informationen zu Verwaltungsdienstleistungen, Online-Diensten, Gebieten, Formularen und den hierfür zuständigen Organisationseinheiten im Kontext von Zuständigkeitsfindern, Bürger- und Unternehmensinformationssystemen und Leistungskatalogen. Eine Vielzahl von Systemen im öffentlichen Bereich benötigt Daten, die originär in Zuständigkeitsfindern erhoben bzw. verwaltet werden. Durch die Standardisierung des Datenaustausches wird die Interoperabilität dieser Systeme erhöht. Diese Systeme stellen normalerweise umfangreiche Schnittstellen zur Verfügung und sind hierüber mit anderen Systemen vernetzt. XZuFi definiert unabhängig von Programm und Hersteller einen Standard, um einem standardisierten Datenaustausch zwischen verschiedenen Systemen zu ermöglichen.</beschreibung>
      </standard>
      <versionStandard>
        <version>2.2.0</version>
        <versionXOEVHandbuch>2.1</versionXOEVHandbuch>
        <versionXGenerator>2.6.1</versionXGenerator>
        <versionModellierungswerkzeug>18.5</versionModellierungswerkzeug>
        <nameModellierungswerkzeug>MagicDraw</nameModellierungswerkzeug>
      </versionStandard>
    </xs:appinfo>
  </xs:annotation>
  <xs:include schemaLocation="xzufi-baukasten.xsd" />
  <xs:complexType name="AnfrageGebiet">
    <xs:annotation>
      <xs:documentation>Anfrage für Gebiete.</xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="xzufi:NachrichtAnfrage">
        <xs:sequence>
          <xs:element name="suchprofil" type="xzufi:SuchprofilGebiet">
            <xs:annotation>
              <xs:documentation>Zugeordnetes Suchprofil.</xs:documentation>
            </xs:annotation>
          </xs:element>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  <xs:complexType name="AnfrageGebietsklasse">
    <xs:annotation>
      <xs:documentation>Anfrage von Gebietsklassen mit Suchprofil.</xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="xzufi:NachrichtAnfrage">
        <xs:sequence>
          <xs:element name="suchprofil" type="xzufi:SuchprofilGebietsklasse">
            <xs:annotation>
              <xs:documentation>Zugeordnetes Suchprofil.</xs:documentation>
            </xs:annotation>
          </xs:element>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  <xs:complexType name="AnfrageGebietstyp">
    <xs:annotation>
      <xs:documentation>Anfrage nach Gebietstypen mit Suchprofil.</xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="xzufi:NachrichtAnfrage">
        <xs:sequence>
          <xs:element name="suchprofil" type="xzufi:SuchprofilGebietstyp">
            <xs:annotation>
              <xs:documentation>Zugeordnetes Suchprofil.</xs:documentation>
            </xs:annotation>
          </xs:element>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  <xs:complexType name="AntwortGebiet">
    <xs:annotation>
      <xs:documentation>Antwort für Gebiete.</xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="xzufi:NachrichtAntwort">
        <xs:sequence>
          <xs:element name="anfrage" type="xzufi:AnfrageGebiet">
            <xs:annotation>
              <xs:documentation>Gestellte Anfrage zur Referenz.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element name="ergebnis" minOccurs="0" type="xzufi:AuswahlGebietBasisinformation">
            <xs:annotation>
              <xs:documentation>Die Gebiete als Ergebnis. Je nach Wahl im Suchprofil als Gebiete oder Basisinformation.</xs:documentation>
            </xs:annotation>
          </xs:element>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  <xs:complexType name="AntwortGebietsklasse">
    <xs:annotation>
      <xs:documentation>Antwort zu einer Gebietsklassenanfrage.</xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="xzufi:NachrichtAntwort">
        <xs:sequence>
          <xs:element name="anfrage" type="xzufi:AnfrageGebietsklasse">
            <xs:annotation>
              <xs:documentation>Die gestellte Anfrage zur Referenz.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element name="ergebnisGebietsklasse" minOccurs="0" maxOccurs="unbounded" type="xzufi:Gebietsklasse">
            <xs:annotation>
              <xs:documentation>Ergebnis der Anfrage.</xs:documentation>
            </xs:annotation>
          </xs:element>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  <xs:complexType name="AntwortGebietstyp">
    <xs:annotation>
      <xs:documentation>Antwort zu einer Anfrage nach Gebietstypen.</xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="xzufi:NachrichtAntwort">
        <xs:sequence>
          <xs:element name="anfrage" type="xzufi:AnfrageGebietstyp">
            <xs:annotation>
              <xs:documentation>Die gestellte Anfrage zur Referenz.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element name="ergebnisGebietstyp" minOccurs="0" maxOccurs="unbounded" type="xzufi:Gebietstyp">
            <xs:annotation>
              <xs:documentation>Ergebnis der Anfrage.</xs:documentation>
            </xs:annotation>
          </xs:element>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  <xs:complexType name="AuswahlGebietBasisinformation">
    <xs:annotation>
      <xs:documentation>Ergebnis einer Gebietsanfrage. Enthält entweder die Gebiete oder die Basisinformationsobjekte zu Gebieten.</xs:documentation>
    </xs:annotation>
    <xs:choice>
      <xs:element name="gebiet" minOccurs="0" maxOccurs="unbounded" type="xzufi:Gebiet">
        <xs:annotation>
          <xs:documentation>Menge von Gebieten.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="basisinformation" minOccurs="0" maxOccurs="unbounded" type="xzufi:BasisinformationObjekt">
        <xs:annotation>
          <xs:documentation>Menge von Basisinformationen zu Gebieten.</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:choice>
  </xs:complexType>
  <xs:complexType name="SuchprofilGebiet">
    <xs:annotation>
      <xs:documentation>Suchprofil für Gebiete.</xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="xzufi:Suchprofil">
        <xs:sequence>
          <xs:element name="gebietstyp" minOccurs="0" maxOccurs="unbounded" type="xzufi:SuchparameterKategorie">
            <xs:annotation>
              <xs:documentation>Einschränkung auf bestimmte Gebietstypen. Diese werden ODER verknüpft.

Es wird der Datentyp SuchparameterKategorie verwendet. In diesem Kontext ist ein Gebietstyp  einer Kategorie gleichzusetzen.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element name="uebergeordneteGebietID" minOccurs="0" type="xzufi:Identifikator">
            <xs:annotation>
              <xs:documentation>Gebiet ID, von denen die Kinder zurückgegeben werden sollen.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element name="kennzeichenNurBasisinformation" default="false" type="xs:boolean">
            <xs:annotation>
              <xs:documentation>Kennzeichen, ob nur Basisinformationen zurückgegeben werden soll. In NachrichtenAntworten wird dann anstatt des eigentlichen Objekts das BasisinformationObjekt zurückgegeben.</xs:documentation>
            </xs:annotation>
          </xs:element>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  <xs:complexType name="SuchprofilGebietsklasse">
    <xs:annotation>
      <xs:documentation>Suchprofil für Gebietsklasse.</xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="xzufi:Suchprofil" />
    </xs:complexContent>
  </xs:complexType>
  <xs:complexType name="SuchprofilGebietstyp">
    <xs:annotation>
      <xs:documentation>Suchprofil für Gebietstypen.</xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="xzufi:Suchprofil">
        <xs:sequence>
          <xs:element name="gebietstypKlasseID" minOccurs="0" maxOccurs="unbounded" type="xzufi:Identifikator">
            <xs:annotation>
              <xs:documentation>Einschränkung auf bestimmte Gebietsklassen.</xs:documentation>
            </xs:annotation>
          </xs:element>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  <xs:element name="gebiete.anfrage.gebiet.040201">
    <xs:annotation>
      <xs:appinfo>
        <rechtsgrundlage>keine Angabe</rechtsgrundlage>
      </xs:appinfo>
      <xs:documentation>Nachricht zur Anfrage von Gebieten.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:complexContent>
        <xs:extension base="xzufi:Basisnachricht">
          <xs:sequence>
            <xs:element name="anfrage" maxOccurs="unbounded" type="xzufi:AnfrageGebiet">
              <xs:annotation>
                <xs:documentation>Eine oder mehrere Gebietsanfragen.</xs:documentation>
              </xs:annotation>
            </xs:element>
          </xs:sequence>
        </xs:extension>
      </xs:complexContent>
    </xs:complexType>
  </xs:element>
  <xs:element name="gebiete.anfrage.gebietsklasse.040202">
    <xs:annotation>
      <xs:appinfo>
        <rechtsgrundlage>keine Angabe</rechtsgrundlage>
      </xs:appinfo>
      <xs:documentation>Nachricht zur Anfrage von Gebietsklassen.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:complexContent>
        <xs:extension base="xzufi:Basisnachricht">
          <xs:sequence>
            <xs:element name="anfrage" maxOccurs="unbounded" type="xzufi:AnfrageGebietsklasse">
              <xs:annotation>
                <xs:documentation>Eine oder mehrere Gebietsklassenanfragen.</xs:documentation>
              </xs:annotation>
            </xs:element>
          </xs:sequence>
        </xs:extension>
      </xs:complexContent>
    </xs:complexType>
  </xs:element>
  <xs:element name="gebiete.anfrage.gebietstyp.040203">
    <xs:annotation>
      <xs:appinfo>
        <rechtsgrundlage>keine Angabe</rechtsgrundlage>
      </xs:appinfo>
      <xs:documentation>Nachricht zur Anfrage von Gebietstypen.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:complexContent>
        <xs:extension base="xzufi:Basisnachricht">
          <xs:sequence>
            <xs:element name="anfrage" maxOccurs="unbounded" type="xzufi:AnfrageGebietstyp">
              <xs:annotation>
                <xs:documentation>Eine oder mehrere Gebietstypenanfragen.</xs:documentation>
              </xs:annotation>
            </xs:element>
          </xs:sequence>
        </xs:extension>
      </xs:complexContent>
    </xs:complexType>
  </xs:element>
  <xs:element name="gebiete.antwort.gebiet.040204">
    <xs:annotation>
      <xs:appinfo>
        <rechtsgrundlage>keine Angabe</rechtsgrundlage>
      </xs:appinfo>
      <xs:documentation>Nachricht zur Übertragung von Antworten zu Gebietsanfragen.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:complexContent>
        <xs:extension base="xzufi:Basisnachricht">
          <xs:sequence>
            <xs:element name="antwort" maxOccurs="unbounded" type="xzufi:AntwortGebiet">
              <xs:annotation>
                <xs:documentation>Antwort für Gebiete</xs:documentation>
              </xs:annotation>
            </xs:element>
          </xs:sequence>
        </xs:extension>
      </xs:complexContent>
    </xs:complexType>
  </xs:element>
  <xs:element name="gebiete.antwort.gebietsklasse.040205">
    <xs:annotation>
      <xs:appinfo>
        <rechtsgrundlage>keine Angabe</rechtsgrundlage>
      </xs:appinfo>
      <xs:documentation>Nachricht zur Übertragung von Antworten zu Gebietsklassenanfragen.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:complexContent>
        <xs:extension base="xzufi:Basisnachricht">
          <xs:sequence>
            <xs:element name="antwort" maxOccurs="unbounded" type="xzufi:AntwortGebietsklasse">
              <xs:annotation>
                <xs:documentation>Übertragene Antworten innerhalb der Nachricht.</xs:documentation>
              </xs:annotation>
            </xs:element>
          </xs:sequence>
        </xs:extension>
      </xs:complexContent>
    </xs:complexType>
  </xs:element>
  <xs:element name="gebiete.antwort.gebietstyp.040206">
    <xs:annotation>
      <xs:appinfo>
        <rechtsgrundlage>keine Angabe</rechtsgrundlage>
      </xs:appinfo>
      <xs:documentation>Nachricht zur Übertragung von Antworten zu Gebietstypanfragen.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:complexContent>
        <xs:extension base="xzufi:Basisnachricht">
          <xs:sequence>
            <xs:element name="antwort" maxOccurs="unbounded" type="xzufi:AntwortGebietstyp">
              <xs:annotation>
                <xs:documentation>Übertragene Antworten innerhalb der Nachricht.</xs:documentation>
              </xs:annotation>
            </xs:element>
          </xs:sequence>
        </xs:extension>
      </xs:complexContent>
    </xs:complexType>
  </xs:element>
</xs:schema>

