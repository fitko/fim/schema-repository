from dataclasses import dataclass
from enum import Enum
from typing import Annotated
from fimportal import xml, xmlstruct
from fimportal.common import create_code_encoding
from fimportal.xzufi.common import (
    XZUFI_NAMESPACE,
    Herausgeber,
    Identifikator,
    StringLocalized,
    Zeitraum,
)
from fimportal.xmlstruct import (
    Value as XmlValue,
)


@dataclass(slots=True)
class Zustaendigkeit:
    id: Identifikator
    id_sekundaer: list[Identifikator]
    leistung_id: Annotated[list[Identifikator], XmlValue(name="leistungID")]
    gebiet_id: Annotated[list[Identifikator], XmlValue(name="gebietID")]
    weitere_informationen: list[StringLocalized]
    herausgeber: Herausgeber | None
    gueltigkeit: list[Zeitraum]


ZustaendigkeitEncoding = xmlstruct.derive(
    Zustaendigkeit, namespace=XZUFI_NAMESPACE, local_name="zustaendigkeit"
)

parse_zustaendigkeit = ZustaendigkeitEncoding.parse


class Zustaendigkeitsrolle(Enum):
    ZUSTAENDIGE_STELLE_UND_ANSPRECHPUNKT = "01"
    ZUSTAENDIGE_STELLE = "02"
    ANSPRECHPUNKT = "03"


class ZustaendigkeitskriteriumCode(Enum):
    ANFANGSBUCHSTABE_FAMILIENNAME = "001"
    ANFANGSBUCHSTABE_VORNAME = "002"
    BEGINN_FAMILIENNAME = "003"
    BEGINN_VORNAME = "004"
    ALTER = "020"
    GEBURTSTAG = "021"
    GEBURTSMONAT = "022"
    GEBURTSJAHR = "023"
    GEBURTSDATUM = "024"
    PERSONENGRUPPE = "041"
    POSTLEITZAHL = "051"
    STRASSENNAME = "052"
    GESCHÄFTSZEICHEN = "061"
    KENNNUMMER = "062"
    VERSICHERUNGSNUMMER = "063"
    HUNDERASSE = "101"
    KFZ_KLASSE = "111"
    KFZ_KENNZEICHEN = "112"


@dataclass(slots=True)
class ZustaendigkeitskriteriumCodeErweiterbar:
    code: ZustaendigkeitskriteriumCode
    nicht_gelisteter_wert: str


@dataclass(slots=True)
class Zustaendigkeitskriterium:
    code_kriterium: ZustaendigkeitskriteriumCodeErweiterbar
    auspraegung: list[str]


@dataclass(slots=True)
class ZustaendigkeitOrganisationseinheit(Zustaendigkeit):
    rolle: (
        Annotated[Zustaendigkeitsrolle, create_code_encoding(Zustaendigkeitsrolle)]
        | None
    )
    kriterium: list[Zustaendigkeitskriterium]


@dataclass(slots=True)
class ZustaendigkeitPerson(Zustaendigkeit):
    kriterium: list[Zustaendigkeitskriterium]


def _decode_zustaendigkeit(node: xml.XmlElement):
    attribute_type_value = node.attrib.get(
        "{http://www.w3.org/2001/XMLSchema-instance}type"
    )
    if attribute_type_value is None:
        return xmlstruct.derive(
            Zustaendigkeit, namespace=XZUFI_NAMESPACE, local_name="zustaendigkeit"
        ).parse(node)

    match attribute_type_value:
        case "xzufi:ZustaendigkeitOrganisationseinheit":
            return xmlstruct.derive(
                ZustaendigkeitOrganisationseinheit,
                namespace=XZUFI_NAMESPACE,
                local_name="zustaendigkeit",
            ).parse(node)
        case "xzufi:ZustaendigkeitPerson":
            return xmlstruct.derive(
                ZustaendigkeitPerson,
                namespace=XZUFI_NAMESPACE,
                local_name="zustaendigkeit",
            ).parse(node)
        case _:
            raise Exception(f"Unknown modul typ: {attribute_type_value}")


Zustaendigkeiten = (
    Zustaendigkeit | ZustaendigkeitOrganisationseinheit | ZustaendigkeitPerson
)


def parse_zustaendigkeit_variante(content: xml.XmlDataSource) -> Zustaendigkeiten:
    document = xml.parse_document(content)

    return _decode_zustaendigkeit(document)


@dataclass(slots=True)
class ZustaendigkeitTransferObjekt:
    uebergeordnetes_objekt_id: Annotated[
        Identifikator | None, XmlValue("uebergeordnetesObjektID")
    ]
    zustaendigkeit: list[
        Annotated[
            Zustaendigkeiten,
            xmlstruct.RequiredValueEncoding(decode=_decode_zustaendigkeit),
        ]
    ]


parse_zustaendigkeit_transfer_objekt = xmlstruct.derive(
    ZustaendigkeitTransferObjekt,
    namespace=XZUFI_NAMESPACE,
    local_name="zustaendigkeitTransferObjekt",
).parse
