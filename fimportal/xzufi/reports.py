from enum import Enum

from pydantic import BaseModel

from .common import Leistungsschluessel, XzufiException
from .leistung import (
    LanguageCode,
    ModulTextTyp,
    Leistung,
)


class ErrorCode(Enum):
    MISSING_MODUL = 0
    DUPLICATE_TEXT_MODUL = 1
    TEXT_MODULE_MISSING_TRANSLATION = 2
    TEXT_MODUL_DUPLICATE_LANGUAGE = 3
    NO_LEIKA_ID = 4
    MULTIPLE_LEIKA_IDS = 5
    INVALID_LEIKA_ID = 6
    MISSING_SDG_INFORMATION = 7
    INVALID_SDG_CODE = 8
    INVALID_LINK = 9


class FailedCheck(BaseModel):
    code: ErrorCode
    params: list[str]
    message: str


class LinkStatus(BaseModel):
    uri: str
    is_online: bool


class QualityReport(BaseModel):
    redaktion_id: str | None
    leistung_id: str
    link_statuses: list[LinkStatus]
    failed_checks: list[FailedCheck]


"""
These are all requried text modules for "Leistungsbeschreibungen".
AFAIK, every entry in the PVOG is a finished Leistungsbeschreibung, so these should all be required here.

Information from:
https://fimportal.de/fim-haus -> 'QS-Kriterien (Leistungen)'
"""
REQUIRED_TEXT_MODULE = [
    ModulTextTyp.LEISTUNGSBEZEICHNUNG,
    ModulTextTyp.LEISTUNGSBEZEICHNUNG_II,
    ModulTextTyp.VOLLTEXT,
    ModulTextTyp.ERFORDERLICHE_UNTERLAGEN,
    ModulTextTyp.VORAUSSETZUNGEN,
    ModulTextTyp.VERFAHRENSABLAUF,
    ModulTextTyp.FORMUALRE,
    ModulTextTyp.HINWEISE,
    ModulTextTyp.URHEBER,
    ModulTextTyp.ZUSTAENDIGE_STELLE,
    ModulTextTyp.ANSPRECHPUNKT,
    ModulTextTyp.RECHTSBEHELF,
    ModulTextTyp.TEASER,
]


def check_text_modul_missing_translation(
    leistung: Leistung, failed_checks: list[FailedCheck]
):
    for text_modul in leistung.modul_text:
        # Skip modules without any translation, as only a link could been provided.
        # But if there is a translation, german should be supplied.
        if len(text_modul.inhalt) == 0:
            continue

        translations = [content.language_code for content in text_modul.inhalt]

        for sprachversion in leistung.sprachversion:
            if sprachversion.language_code not in translations:
                failed_checks.append(
                    FailedCheck(
                        code=ErrorCode.TEXT_MODULE_MISSING_TRANSLATION,
                        params=[
                            text_modul.leika_textmodul.name,
                            sprachversion.language_code,
                        ],
                        message=f"No translation for {sprachversion.language_code} in TextModul of type {text_modul.leika_textmodul}",
                    )
                )


def check_unique_text_modul(leistung: Leistung, failed_checks: list[FailedCheck]):
    existing_text_module: set[ModulTextTyp] = set()
    for text_modul in leistung.modul_text:
        if text_modul.leika_textmodul in existing_text_module:
            failed_checks.append(
                FailedCheck(
                    code=ErrorCode.DUPLICATE_TEXT_MODUL,
                    params=[text_modul.leika_textmodul.name],
                    message=f"Duplicate TextModul of type {text_modul.leika_textmodul}",
                )
            )

        existing_text_module.add(text_modul.leika_textmodul)


def check_missing_modul(leistung: Leistung, failed_checks: list[FailedCheck]):
    existing_text_module: set[ModulTextTyp] = set(
        [modul.leika_textmodul for modul in leistung.modul_text]
    )

    for modul_typ in REQUIRED_TEXT_MODULE:
        if modul_typ not in existing_text_module:
            failed_checks.append(
                FailedCheck(
                    code=ErrorCode.MISSING_MODUL,
                    params=[modul_typ.name],
                    message=f"Missing TextModul of type {modul_typ}",
                )
            )


def check_text_modul_unique_language(
    leistung: Leistung, failed_checks: list[FailedCheck]
):
    for text_modul in leistung.modul_text:
        existing_languages: set[LanguageCode] = set()

        for content in text_modul.inhalt:
            if content.language_code in existing_languages:
                failed_checks.append(
                    FailedCheck(
                        code=ErrorCode.TEXT_MODUL_DUPLICATE_LANGUAGE,
                        params=[content.language_code],
                        message=f"Duplicate inhalt in TextModul for language {content.language_code}",
                    )
                )

            existing_languages.add(LanguageCode(content.language_code))


def check_leistungsschluessel(leistung: Leistung, failed_checks: list[FailedCheck]):
    leistungsschluessel = leistung.get_leistungsschluessel()

    if len(leistungsschluessel) == 0:
        failed_checks.append(
            FailedCheck(
                code=ErrorCode.NO_LEIKA_ID,
                params=[],
                message="Keine LeiKa Referenze",
            )
        )
    elif len(leistungsschluessel) > 1:
        failed_checks.append(
            FailedCheck(
                code=ErrorCode.MULTIPLE_LEIKA_IDS,
                params=[],
                message="Mehrere LeiKa Referenzen",
            )
        )

    for referenz in leistungsschluessel:
        try:
            _ = Leistungsschluessel.from_str(referenz)
        except XzufiException as error:
            failed_checks.append(
                FailedCheck(
                    code=ErrorCode.INVALID_LEIKA_ID,
                    params=[],
                    message=f"Ungueltige LeiKa Referenz: {str(error)}",
                )
            )


def check_sdg_codes(leistung: Leistung, failed_checks: list[FailedCheck]):
    if len(leistung.informationsbereich_sdg) == 0:
        failed_checks.append(
            FailedCheck(
                code=ErrorCode.MISSING_SDG_INFORMATION,
                params=[],
                message="Kein SDG Informationsbereich angegeben",
            )
        )

    for sdg_code in leistung.informationsbereich_sdg:
        if not _is_valid_sdg_code(sdg_code):
            failed_checks.append(
                FailedCheck(
                    code=ErrorCode.INVALID_SDG_CODE,
                    params=[],
                    message="Ungueltiger SDG Informationsbereich",
                )
            )


def _is_valid_sdg_code(value: str) -> bool:
    if len(value) != 7:
        return False

    try:
        int(value)
    except Exception:
        return False

    # TODO: Check if code is in list of all possible codes

    return True


def check_leistung(leistung: Leistung) -> list[FailedCheck]:
    failed_checks = []

    check_missing_modul(leistung, failed_checks)
    check_unique_text_modul(leistung, failed_checks)
    check_text_modul_missing_translation(leistung, failed_checks)
    check_text_modul_unique_language(leistung, failed_checks)
    check_leistungsschluessel(leistung, failed_checks)
    check_sdg_codes(leistung, failed_checks)

    return failed_checks
