from datetime import datetime
from enum import Enum
import logging
from dataclasses import dataclass
from typing import Annotated

from fimportal import xml, xmlstruct
from fimportal.common import VerwaltungspolitischeKodierung, create_code_encoding
from fimportal.xzufi.leistung import KategorieKlasse

from .common import (
    XZUFI_NAMESPACE,
    Herausgeber,
    HyperlinkErweitert,
    Identifikator,
    Sprachversion,
    StringLocalized,
    Versionsinformation,
    XzufiException,
    Zeitraum,
    Zahlungsweise,
)

logger = logging.getLogger(__name__)


class HierarchieTyp(Enum):
    OB = "Oberbegriff"
    UB = "Unterbegriff"
    VB = "Verwandter Begriff"
    BF = "Benutzt fuer"
    BS = "Benutzte Synonym"


@dataclass(slots=True)
class Hierarchie:
    ziel_id: Identifikator
    hierarchie_type: Annotated[HierarchieTyp, create_code_encoding(HierarchieTyp)]


@dataclass(slots=True)
class AlternativeOrganisationshierarchie(Hierarchie):
    # Codeliste 'OrganisationshierarchieTyp', die nicht im Standard oder xrepository definiert ist
    fachlicher_typ: str | None


@dataclass(slots=True)
class Kategorie:
    id: Identifikator
    id_sekundaer: list[Identifikator]
    uebergeordnete_kategorie_id: Identifikator | None
    untergeordnete_kategorie_id: Identifikator | None
    bezeichnung: list[StringLocalized]
    beschreibung: list[StringLocalized]


@dataclass(slots=True)
class OrganisationseinheitskategorieKlasse(KategorieKlasse):
    pass


@dataclass(slots=True)
class Organisationseinheitskategorie(Kategorie):
    klasse: OrganisationseinheitskategorieKlasse | None


@dataclass(slots=True)
class NameOrganisation:
    name: list[StringLocalized]
    kurzbeschreibung: list[StringLocalized]
    gueltigkeit: Zeitraum | None


class Tagesposition(Enum):
    ERSTER = "01"
    ZWEITER = "02"
    DRITTER = "03"
    VIERTER = "04"
    FUENFTER = "05"
    ZEHNTER = "10"
    FUENFZEHNTER = "15"
    ZWANZIGSTER = "20"
    JEDER = "40"
    VORLETZTER = "79"
    LETZTER = "80"


class Tagestyp(Enum):
    MONTAG = "01"
    DIENSTAG = "02"
    MITTWOCH = "03"
    DONNERSTAG = "04"
    FREITAG = "05"
    SAMSTAG = "06"
    SONNTAG = "07"
    TAG = "08"
    ARBEITSTAG_MO_FR = "09"
    ARBEITSTAG_MO_SA = "10"


@dataclass(slots=True)
class Zeitserie:
    tagesposition: Annotated[Tagesposition, create_code_encoding(Tagesposition)]
    tagestyp: Annotated[Tagestyp, create_code_encoding(Tagestyp)]
    beginn: datetime
    ende: datetime
    zusatz: StringLocalized | None


@dataclass(slots=True)
class Zeitserien:
    tagesposition: Annotated[Tagesposition, create_code_encoding(Tagesposition)]
    hinweistext: StringLocalized | None
    gueltigkeit: list[Zeitraum]
    regulaere_zeiten: list[Zeitserie]
    abweichende_zeiten: list[Zeitraum]


class Anschrifttyp(Enum):
    HAUSANSCHRIFT = "001"
    BESUCHERANSCHRIFT = "002"
    POSTFACH = "003"
    GROSSEMPFAENGERPOSTFACH = "004"
    LIEFERANSCHRIFT = "005"
    POSTANSCHRIFT = "006"


@dataclass(slots=True)
class Geokodierung:
    x: float
    y: float


def _decode_geokodierung(node: xml.XmlElement) -> Geokodierung | None:
    assert node[0].tag == "{http://www.opengis.net/gml/3.2}Point"
    assert node[0][0].tag == "{http://www.opengis.net/gml/3.2}pos"
    value = node[0][0].text
    assert value is not None
    x, y = value.split(" ")
    return Geokodierung(float(x), float(y))


@dataclass(slots=True)
class Anschrift:
    typ: Annotated[Anschrifttyp | None, create_code_encoding(Anschrifttyp)]
    strasse: str | None
    hausnummer: str | None
    postfach: str | None
    postleitzahl: str
    ort: str
    ort_id: list[Identifikator]
    verwaltungspolitische_kodierung: VerwaltungspolitischeKodierung | None
    zusatz: str | None
    geokodierung: list[
        Annotated[
            Geokodierung, xmlstruct.RequiredValueEncoding(decode=_decode_geokodierung)
        ]
    ]
    id: Identifikator | None
    gueltigkeit: list[Zeitraum]


@dataclass(slots=True)
class AnschriftOrganisationseinheit(Anschrift):
    anfahrtsuri: list[HyperlinkErweitert]
    info_parkplatz: list[StringLocalized]
    info_oepnv: list[StringLocalized]
    info_barrierefreiheit: list[StringLocalized]
    kennzeichen_aufzug: bool | None
    kennzeichen_rollstuhlgerecht: bool | None


class Erreichbarkeitskanal(Enum):
    EMAIL = "01"
    TELEFON_FESTNETZ = "02"
    TELEFON_MOBIL = "03"
    FAX = "04"
    INSTANT_MESSENGER = "05"
    PAGER = "06"
    SONSTIGES = "07"
    DE_MAIL = "08"
    WEB = "09"


@dataclass(slots=True)
class Erreichbarkeit:
    kanal: Annotated[Erreichbarkeitskanal, create_code_encoding(Erreichbarkeitskanal)]
    # Required by standard but one resource ('L100040', '553507057') doesn't fulfill the requirements.
    kennung: str | None
    kennungszusatz: str | None
    zusatz: str | None
    gueltigkeit: list[Zeitraum]


class KommunikationssystemTyp(Enum):
    """
    See https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.kommunikationssystemtyp
    """

    EGVP = "001"
    EMAIL_CLIENT = "002"
    WEBSERVICE = "003"
    FIT_CONNECT = "004"
    TH_AVEL = "005"
    OSCI = "006"
    FALLMANAGEMENT_KOMMUNAL_HESSEN = "007"
    FALLMANAGEMENT_MECKLENBURG_VORPOMMERN = "008"
    BE_BPO = "009"
    UNIVERSALANTRAG_BRANDENBURG = "010"
    BE_A = "011"
    E_AKTE_BAYERN = "012"
    E_RECHNUGSPLATTFORM = "013"
    BEZAHLDIENST = "014"
    PEPPOL = "015"


@dataclass(slots=True)
class Kommunikationssystem:
    kanal: Annotated[
        KommunikationssystemTyp, create_code_encoding(KommunikationssystemTyp)
    ]
    kennung: str
    kennungzusatz: str | None
    gueltigkeit: list[Zeitraum]


@dataclass(slots=True)
class Bankverbindung:
    beschreibung: list[StringLocalized]
    empfaenger: str
    iban: str
    bic: str | None
    bankinstitut: str | None
    verwendeungszweck: list[StringLocalized]
    id: Identifikator | None
    id_sekundaer: list[Identifikator]
    gueltigkeit: list[Zeitraum]


@dataclass(slots=True)
class Gebietsklasse(KategorieKlasse):
    kurzbezeichnung: list[StringLocalized]


@dataclass(slots=True)
class Gebietstyp:
    id: Identifikator
    id_sekundaer: list[Identifikator]
    klasse: Gebietsklasse
    bezeichnung: list[str]
    kurzbezeichnung: list[str]
    beschreibung: list[StringLocalized]


@dataclass(slots=True)
class Gebiet:
    id: Identifikator
    id_sekundaer: list[Identifikator]
    typ: Gebietstyp
    uebergeordnetes_gebiet_id: list[Identifikator]
    untergeordnetes_gebiet_id: list[Identifikator]
    verwaltungspolitische_kodierung: VerwaltungspolitischeKodierung | None
    bezeichnung: list[StringLocalized]
    kurzbezeichnung: list[StringLocalized]
    beschreibung: list[StringLocalized]
    herausgeber: Herausgeber | None
    versionsinformationen: Versionsinformation | None
    gueltigkeit: list[Zeitraum]


@dataclass(slots=True)
class Organisationseinheit:
    id: Identifikator
    id_sekundaer: list[Identifikator]
    externe_organisationseinheitsermittlung: bool
    uebergeordnete_organisationseinheit_id: Identifikator | None
    untergeordnete_organisationseinheit_id: list[Identifikator]
    alternative_hierarchie: list[AlternativeOrganisationshierarchie]
    kategorie: list[Organisationseinheitskategorie]
    name: list[NameOrganisation]
    beschreibung: list[StringLocalized]
    kurzbeschreibung: list[StringLocalized]
    info_oeffnungszeiten_text: list[StringLocalized]
    info_oeffnungszeiten_struktur: list[Zeitserien]
    info_intern_servicecenter: list[StringLocalized]
    info_sonstige: list[StringLocalized]
    anschrift: list[AnschriftOrganisationseinheit]
    erreichbarkeit: list[Erreichbarkeit]
    kommunikationssystem: list[Kommunikationssystem]
    internetaddresse: list[HyperlinkErweitert]
    bankverbindung: list[Bankverbindung]
    glaeubiger_identifikationsnummer: StringLocalized | None
    zahlungsweise: list[Annotated[Zahlungsweise, create_code_encoding(Zahlungsweise)]]
    zahlungsweise_text: list[StringLocalized]
    synonym: list[StringLocalized]
    herausgeber: Herausgeber | None
    versions_informationen: Versionsinformation | None
    sprachversion: list[Sprachversion]
    gueltigkeit: list[Zeitraum]


OrganisationseinheitEncoding = xmlstruct.derive(
    Organisationseinheit,
    namespace=XZUFI_NAMESPACE,
    local_name="organisationseinheit",
)

parse_raw_organisationseinheit = OrganisationseinheitEncoding.parse


def parse_organisationseinheit(content: str):
    try:
        return parse_raw_organisationseinheit(content)
    except xml.ParserException as ex:
        raise XzufiException(f"Could not parse Organisationseinheit: {str(ex)}") from ex
