from __future__ import annotations
from enum import Enum

import logging
from dataclasses import dataclass
from typing import Annotated

from fimportal import xml, xmlstruct
from fimportal.xmlstruct import (
    Attribute as XmlAttribute,
)
from fimportal.common import create_code_encoding

from .common import (
    XZUFI_NAMESPACE,
    Herausgeber,
    HyperlinkErweitert,
    Identifikator,
    Sprache,
    Sprachversion,
    Staatsangehoerigkeit,
    StringLocalized,
    Versionsinformation,
    Vertrauensniveau,
    XzufiException,
    Zeitraum,
    Zahlungsweise,
)

logger = logging.getLogger(__name__)


class LinkTyp(Enum):
    """
    Source: https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:onlinedienstlinktyp
    """

    INTERNETAUFRUF = "01"
    DOI_AUFRUF = "02"
    STATUSABFRAGE = "03"
    HOCHLADEN = "04"
    REST_SERVICE = "05"


class OnlineDienstDynamischeParameterTyp(Enum):
    """
    Codelist defined in standard
    """

    LEISTUNG = "001"
    GEBIET = "002"
    ORGANISATIONSEINHEIT = "003"
    LEIKA_LEISTUNG = "004"
    FORMULAR = "005"
    SPRACHE = "006"


@dataclass(slots=True)
class OnlineDienstDynamischeParameter:
    typ: Annotated[
        OnlineDienstDynamischeParameterTyp,
        create_code_encoding(OnlineDienstDynamischeParameterTyp),
    ]
    parameter_name: str


@dataclass(slots=True)
class OnlinedienstLink:
    language_code: Annotated[str | None, XmlAttribute("languageCode", namespace=None)]
    typ: Annotated[LinkTyp, create_code_encoding(LinkTyp)]
    link: str
    dynamische_parameter: list[OnlineDienstDynamischeParameter]
    titel: str | None
    beschreibung: str | None
    position_darstellung: int | None


@dataclass(slots=True)
class OnlinedienstParameter:
    parameter_name: str
    parameter_wert: str | None


class Identifizierungsmittel(Enum):
    """
    See codelist: https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:identifizierungsmittel
    """

    ELEKTRONISCHE_IDENTIFIZIERUNG = "1000"
    ELEKTRONISCHE_IDENTIFIZIERUNG_NATIONALE_EID = "1100"
    ELEKTRONISCHE_IDENTIFIZIERUNG_NATIONALE_EID_AUSWEIS = "1101"
    ELEKTRONISCHE_IDENTIFIZIERUNG_NATIONALE_EID_SOFTWAREZERTIFIKAT = "1102"
    ELEKTRONISCHE_IDENTIFIZIERUNG_NATIONALE_EID_BENUTZERNAME_PASSWORT = "1103"
    ELEKTRONISCHE_IDENTIFIZIERUNG_NATIONALE_EID_MANUELLE_EINGABE = "1104"
    ELEKTRONISCHE_IDENTIFIZIERUNG_NOTIFIZIERTEN_EID = "1200"
    ELEKTRONISCHE_IDENTIFIZIERUNG_NOTIFIZIERTEN_EID_EIDAS_CONNECTOR = "1201"
    DIGITALE_AUTHENTIFIZIERUNGSMITTEL_UKRAINE_DIIA = "1300"
    RECHTSVERBINDLICHE_UNTERSCHRIFT = "2000"
    RECHTSVERBINDLICHE_UNTERSCHRIFT_FERNSIGNATUR = "2100"
    RECHTSVERBINDLICHES_SIEGEL_BEHOERDEN = "3000"
    ELEKTRONISCHE_GESUNDHEITSKARTE = "4000"
    KEINE_IDENTIFIZIERUNG = "9000"


@dataclass(slots=True)
class Onlinedienst:
    id: Identifikator
    id_sekundaer: list[Identifikator]
    bezeichnung: list[StringLocalized]
    kurzbezeichnung: list[StringLocalized]
    beschreibung: list[StringLocalized]
    teaser: list[StringLocalized]
    link: list[OnlinedienstLink]
    parameter: list[OnlinedienstParameter]
    durchfuehrung_staatsangehoerigkeit: list[
        Annotated[Staatsangehoerigkeit, create_code_encoding(Staatsangehoerigkeit)]
    ]
    durchfuehrung_sprachen: list[Sprache]
    zahlungsweise: list[Annotated[Zahlungsweise, create_code_encoding(Zahlungsweise)]]
    vertrauensniveau: Annotated[
        Vertrauensniveau, create_code_encoding(Vertrauensniveau)
    ]
    identifizierungsmittel: list[
        Annotated[Identifizierungsmittel, create_code_encoding(Identifizierungsmittel)]
    ]
    logo: list[Identifikator]
    hilfe_link: list[HyperlinkErweitert]
    hilfe_text: list[StringLocalized]
    herausgeber: Herausgeber | None
    versionsinformationen: Versionsinformation | None
    sprachversion: list[Sprachversion]
    gueltigkeit: list[Zeitraum]


OnlinedienstEncoding = xmlstruct.derive(
    Onlinedienst, namespace=XZUFI_NAMESPACE, local_name="onlinedienst"
)


def parse_onlinedienst(data: xml.XmlDataSource | xml.XmlElement):
    try:
        return OnlinedienstEncoding.parse(data)
    except xml.ParserException as ex:
        raise XzufiException(ex)
