"""
Provide the labels for large XZuFi enumns from the corresponding json files.
"""

from pathlib import Path
import orjson


CURRENT_DIR = Path(__file__).parent


def _load_labels(filename: str) -> dict[int, str]:
    with open(CURRENT_DIR / filename, "rb") as file:
        data = orjson.loads(file.read())

    return {int(key): label for key, label in data["daten"]}


def _load_sdg_labels(filename: str) -> dict[str, str]:
    with open(CURRENT_DIR / filename, "rb") as file:
        data = orjson.loads(file.read())

    return {entry[0]: entry[1] for entry in data["daten"]}


def _load_pvlagen_labels(filename: str) -> dict[str, str]:
    with open(CURRENT_DIR / filename, "rb") as file:
        data = orjson.loads(file.read())

    return {entry[0]: entry[1] for entry in data["daten"]}


LEISTUNGS_GRUPPIERUNG_TO_LABEL = _load_labels("leistungs_gruppierung.json")
LEISTUNGS_KENNUNG_TO_LABEL = {}
VERRICHTUNGS_KENNUNG_TO_LABEL = _load_labels("verrichtungs_kennungen.json")
VERRICHTUNGS_DETAIL_TO_LABEL = {}
SDG_INFORMATIONSBEREICH_TO_LABEL = _load_sdg_labels("sdg_informationsbereiche.json")
PV_LAGEN_TO_LABEL = _load_pvlagen_labels("PVLagen_20230308.json")
