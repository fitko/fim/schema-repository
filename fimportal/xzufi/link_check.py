from __future__ import annotations

import asyncio
import logging
from abc import ABC, abstractmethod

import httpx


logger = logging.getLogger(__name__)


class Service:
    def __init__(self, client: BaseLinkChecker):
        self._client = client

    async def check(self, links: list[str]) -> list[bool]:
        tasks = asyncio.gather(*[self._client.check(link) for link in links])

        try:
            return await tasks
        except Exception:
            tasks.cancel()
            raise


class BaseLinkChecker(ABC):
    """
    Interface for a XZuFi client.
    Makes it easy to provide a test client during tests.
    """

    @abstractmethod
    async def check(self, link: str) -> bool: ...


class LinkChecker(BaseLinkChecker):
    def __init__(self):
        self._client = httpx.AsyncClient(timeout=5)

    async def check(self, link: str):
        logger.info("Check %s", link)

        try:
            response = await self._client.head(link, follow_redirects=True)
            return response.status_code == 200
        except:
            return False


class FakeLinkChecker(BaseLinkChecker):
    def __init__(self):
        self._cache: set[str] = set()

    async def check(self, link: str) -> bool:
        return link in self._cache

    def register_link(self, link: str):
        self._cache.add(link)

    def remove_link(self, link: str):
        self._cache.remove(link)

    def clear(self):
        self._cache = set()
