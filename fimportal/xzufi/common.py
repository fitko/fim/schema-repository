from dataclasses import dataclass
from datetime import date, datetime, timezone
from enum import Enum
import logging
from typing import Annotated, Any, NewType

import ciso8601

from fimportal import xml
from fimportal import xmlstruct
from fimportal.common import VerwaltungspolitischeKodierung
from fimportal.xmlstruct import (
    Attribute as XmlAttribute,
    RequiredValueEncoding,
    TextValue as XmlTextValue,
    Value as XmlValue,
)

XZUFI_NAMESPACE = "http://xoev.de/schemata/xzufi/2_2_0"
XPATH_NS_MAP = {"xzufi": XZUFI_NAMESPACE}

XZUFI_ID = f"{{{XZUFI_NAMESPACE}}}id"
XZUFI_SEKUNDAER_ID = f"{{{XZUFI_NAMESPACE}}}idSekundaer"
XZUFI_BEZEICHNUNG = f"{{{XZUFI_NAMESPACE}}}bezeichnung"
XZUFI_KATEGORIE = f"{{{XZUFI_NAMESPACE}}}kategorie"
XZUFI_KLASSE = f"{{{XZUFI_NAMESPACE}}}klasse"

XZUFI_LINK = f"{{{XZUFI_NAMESPACE}}}link"
XZUFI_TYP = f"{{{XZUFI_NAMESPACE}}}typ"

XZUFI_ONLINEDIENST = f"{{{XZUFI_NAMESPACE}}}onlinedienst"
XZUFI_ORGANISATIONSEINHEIT = f"{{{XZUFI_NAMESPACE}}}organisationseinheit"
XZUFI_ZUSTAENDIGKEIT_TRANSFER_OBJEKT = (
    f"{{{XZUFI_NAMESPACE}}}zustaendigkeitTransferObjekt"
)
XZUFI_SPEZIALISIERUNG = f"{{{XZUFI_NAMESPACE}}}spezialisierung"

XZUFI_ZUSTAENDIGKEIT = f"{{{XZUFI_NAMESPACE}}}zustaendigkeit"


RedaktionsId = NewType("RedaktionsId", str)
LeistungsId = NewType("LeistungsId", str)
XzufiLeistungsIdentifier = tuple[RedaktionsId, LeistungsId]


@dataclass(slots=True)
class XzufiIdentifier:
    redaktion_id: str
    id: str

    @staticmethod
    def from_xzufi_leistung_identifier(
        id: XzufiLeistungsIdentifier,
    ) -> "XzufiIdentifier":
        return XzufiIdentifier(redaktion_id=id[0], id=id[1])

    def to_xzufi_leistung_identifier(self):
        return (RedaktionsId(self.redaktion_id), LeistungsId(self.id))


class LeistungsTypisierung(Enum):
    """
    https://www.xrepository.de/details/urn:de:fim:leika:typisierung
    """

    TYP_1 = "1"
    TYP_2 = "2"
    TYP_2A = "2a"
    TYP_2B = "2b"
    TYP_3 = "3"
    TYP_3A = "3a"
    TYP_3B = "3b"
    TYP_2_3 = "2/3"
    TYP_2_3A = "2/3a"
    TYP_2_3B = "2/3b"
    TYP_4 = "4"
    TYP_4A = "4a"
    TYP_4B = "4b"
    TYP_5 = "5"
    TYP_6 = "6"
    TYP_7 = "7"
    TYP_8 = "8"
    TYP_9 = "9"
    TYP_10 = "10"
    TYP_11 = "11"
    TYP_12 = "12"

    def to_label(self) -> str:
        return LEISTUNSTYP_TO_STRING[self]


"""
All Leistungstypisierungen, that potentially have a Stammtext.
"""
LEISTUNGSTYPISIERUNG_WITH_STAMMTEXT = frozenset(
    [
        LeistungsTypisierung.TYP_2,
        LeistungsTypisierung.TYP_2A,
        LeistungsTypisierung.TYP_2B,
        LeistungsTypisierung.TYP_3,
        LeistungsTypisierung.TYP_3A,
        LeistungsTypisierung.TYP_3B,
        LeistungsTypisierung.TYP_2_3,
        LeistungsTypisierung.TYP_2_3A,
        LeistungsTypisierung.TYP_2_3B,
    ]
)


STRING_TO_LEISTUNGSTYP: dict[str, LeistungsTypisierung] = {
    "1": LeistungsTypisierung.TYP_1,
    "2": LeistungsTypisierung.TYP_2,
    "2a": LeistungsTypisierung.TYP_2A,
    "2b": LeistungsTypisierung.TYP_2B,
    "3": LeistungsTypisierung.TYP_3,
    "3a": LeistungsTypisierung.TYP_3A,
    "3b": LeistungsTypisierung.TYP_3B,
    "2/3": LeistungsTypisierung.TYP_2_3,
    "2/3a": LeistungsTypisierung.TYP_2_3A,
    "2/3b": LeistungsTypisierung.TYP_2_3B,
    "4": LeistungsTypisierung.TYP_4,
    "4a": LeistungsTypisierung.TYP_4A,
    "4b": LeistungsTypisierung.TYP_4B,
    "5": LeistungsTypisierung.TYP_5,
    "6": LeistungsTypisierung.TYP_6,
    "7": LeistungsTypisierung.TYP_7,
    "8": LeistungsTypisierung.TYP_8,
    "9": LeistungsTypisierung.TYP_9,
    "10": LeistungsTypisierung.TYP_10,
    "11": LeistungsTypisierung.TYP_11,
    "12": LeistungsTypisierung.TYP_12,
}

LEISTUNSTYP_TO_STRING: dict[LeistungsTypisierung, str] = {
    status: label for label, status in STRING_TO_LEISTUNGSTYP.items()
}


class LeistungRedaktionId(Enum):
    SH = "L100012"
    HH = "S100002"
    NI = "L100040"
    HB = "S100003"
    NW = "L100002"
    HE = "L100001"
    RP = "L100039"
    BW = "L100022"
    BY = "L100042"
    SL = "L100010"
    BE = "L100108"
    BB = "L100041"
    MV = "L100027"
    SN = "L100009"
    ST = "L100008"
    TH = "L100038"
    BUND = "B100019"
    LEIKA = "TSA_TELEPORT"

    @staticmethod
    def get_possible_beschreibungen(
        typisierung: list[LeistungsTypisierung],
    ) -> "list[LeistungRedaktionId]":
        # TODO: This is not exhaustive, as e.g. a TYP_12 Leistung could also have a Beschreibung from the BUND.
        if LeistungsTypisierung.TYP_1 in typisierung:
            if len(typisierung) == 1:
                return [LeistungRedaktionId.BUND]
            else:
                return [LeistungRedaktionId.BUND, *LANDESREDAKTIONEN]
        else:
            return LANDESREDAKTIONEN

    def to_label(self) -> str:
        match self:
            case LeistungRedaktionId.SH:
                return "Schleswig-Holstein"
            case LeistungRedaktionId.HH:
                return "Hamburg"
            case LeistungRedaktionId.NI:
                return "Niedersachsen"
            case LeistungRedaktionId.HB:
                return "Bremen"
            case LeistungRedaktionId.NW:
                return "Nordrhein-Westfalen"
            case LeistungRedaktionId.HE:
                return "Hessen"
            case LeistungRedaktionId.RP:
                return "Rheinland-Pfalz"
            case LeistungRedaktionId.BW:
                return "Baden-Württemberg"
            case LeistungRedaktionId.BY:
                return "Bayern"
            case LeistungRedaktionId.SL:
                return "Saarland"
            case LeistungRedaktionId.BE:
                return "Berlin"
            case LeistungRedaktionId.BB:
                return "Brandenburg"
            case LeistungRedaktionId.MV:
                return "Mecklenburg-Vorpommern"
            case LeistungRedaktionId.SN:
                return "Sachsen"
            case LeistungRedaktionId.ST:
                return "Sachsen-Anhalt"
            case LeistungRedaktionId.TH:
                return "Thüringen"
            case LeistungRedaktionId.BUND:
                return "Bund"
            case LeistungRedaktionId.LEIKA:
                return "Baustein Leistungen"

    def to_shorthand(self) -> str:
        match self:
            case LeistungRedaktionId.SH:
                return "SH"
            case LeistungRedaktionId.HH:
                return "HH"
            case LeistungRedaktionId.NI:
                return "NI"
            case LeistungRedaktionId.HB:
                return "HB"
            case LeistungRedaktionId.NW:
                return "NW"
            case LeistungRedaktionId.HE:
                return "HE"
            case LeistungRedaktionId.RP:
                return "RP"
            case LeistungRedaktionId.BW:
                return "BW"
            case LeistungRedaktionId.BY:
                return "BY"
            case LeistungRedaktionId.SL:
                return "SL"
            case LeistungRedaktionId.BE:
                return "BE"
            case LeistungRedaktionId.BB:
                return "BB"
            case LeistungRedaktionId.MV:
                return "MV"
            case LeistungRedaktionId.SN:
                return "SN"
            case LeistungRedaktionId.ST:
                return "ST"
            case LeistungRedaktionId.TH:
                return "TH"
            case LeistungRedaktionId.BUND:
                return "Bund"
            case LeistungRedaktionId.LEIKA:
                # The shorthand is only used for the overview of available Leistungsbeschreibungen,
                # but the LEIKA never published those.
                assert False


REDAKTION_ID_TO_LABEL = {
    redaktion_id.value: redaktion_id.to_label() for redaktion_id in LeistungRedaktionId
}


LABEL_TO_REDAKTION_ID = {
    redaktion_id.to_label(): redaktion_id for redaktion_id in LeistungRedaktionId
}


LANDESREDAKTIONEN = [
    LeistungRedaktionId.SH,
    LeistungRedaktionId.HH,
    LeistungRedaktionId.NI,
    LeistungRedaktionId.HB,
    LeistungRedaktionId.NW,
    LeistungRedaktionId.HE,
    LeistungRedaktionId.RP,
    LeistungRedaktionId.BW,
    LeistungRedaktionId.BY,
    LeistungRedaktionId.SL,
    LeistungRedaktionId.BE,
    LeistungRedaktionId.BB,
    LeistungRedaktionId.MV,
    LeistungRedaktionId.SN,
    LeistungRedaktionId.ST,
    LeistungRedaktionId.TH,
]
assert len(LANDESREDAKTIONEN) == 16
# Sort Landesredaktionen by the UI label
LANDESREDAKTIONEN.sort(key=lambda i: i.to_label())


# The redaktion id of the XZuFi Redaktion.
XZUFI_REDAKTION_ID = RedaktionsId(LeistungRedaktionId.LEIKA.value)


class XzufiException(Exception):
    pass


class InternalXzufiException(Exception):
    """
    An internal exception of the xzufi module. This should
    always be caught at module API boundaries and never actually
    escape to the call-side.

    This is done as a convenient method to catch internal parser errors,
    which can then be wrapped in a `ParserException` with the current line number.

    This way, the actual location of where the exception occurred does not need access to the
    current line information.
    """

    pass


class LeistungHierarchyType(Enum):
    """
    This describes how the Xzufi Leistung should be interpreted according to the FIM methodology:

    - steckbrief: The Leistung is from the LeiKa and either has no Stammtext (e.g. Typ 1), or has no published Stammtext yet.
    - stammtext: The Leistung is form the LeiKa and also has a published Stammtext.
    - beschreibung: The Leistung is from the PVOG.
    """

    STECKBRIEF = "steckbrief"
    STAMMTEXT = "stammtext"
    BESCHREIBUNG = "beschreibung"

    def to_label(self) -> str:
        match self:
            case LeistungHierarchyType.STECKBRIEF:
                return "Steckbrief"
            case LeistungHierarchyType.STAMMTEXT:
                return "Stammtext"
            case LeistungHierarchyType.BESCHREIBUNG:
                return "Leistungsbeschreibung"


LanguageCode = NewType("LanguageCode", str)


def _decode_language_code(node: xml.XmlElement) -> LanguageCode | None:
    value = node.text
    if value is None:
        return None
    return LanguageCode(value)


GERMAN = LanguageCode("de-DE")
ALT_GERMAN = LanguageCode("de")
ENGLISH = LanguageCode("en")


@dataclass(slots=True)
class StringLocalized:
    language_code: Annotated[str, XmlAttribute("languageCode", namespace=None)]
    value: Annotated[str, XmlTextValue()]


def get_translation(
    translations: list[StringLocalized], language_code: LanguageCode
) -> str | None:
    for content in translations:
        if content.language_code == language_code:
            return content.value

    return None


def expect_translation(
    translations: list[StringLocalized], language_code: LanguageCode
) -> str:
    translation = get_translation(translations, language_code)
    if translation is None:
        raise XzufiException(f"Missing translation for language code {language_code}")

    return translation


def parse_value_translation(child: xml.XmlElement) -> tuple[LanguageCode, str]:
    language_code = child.get("languageCode")

    if language_code is None:
        raise InternalXzufiException(f"Missing language code in {child.tag}")

    value = xml.parse_value(child)

    return LanguageCode(language_code), value


class ModulTextTyp(Enum):
    """
    https://www.xrepository.de/details/urn:de:xzufi:codeliste:leistungstextmodulleika

    Better descriptions in:
    https://fimportal.de/fim-haus -> Leistung -> QS-Kriterien
    """

    # Bezeichnung der Leistung aus Sicht der Verwaltung.
    LEISTUNGSBEZEICHNUNG = "02"
    # Bezeichnung der Leistung aus Sicht der Nutzenden
    LEISTUNGSBEZEICHNUNG_II = "03"
    # Kurzbeschreibung der Leistung zur Beauskunftung durch die 115
    KURZTEXT = "05"
    # ausführliche Beschreibung der Leistung
    VOLLTEXT = "06"
    RECHTSGRUNDLAGEN = "07"
    ERFORDERLICHE_UNTERLAGEN = "08"
    VORAUSSETZUNGEN = "09"
    VERFAHRENSABLAUF = "11"
    FORMUALRE = "14"
    # Links auf Internetseiten mit Informationen rund um die Verwaltungsleistung
    WEITERFUEHRENDE_INFORMATIONEN = "15"
    HINWEISE = "16"
    URHEBER = "18"
    ZUSTAENDIGE_STELLE = "22"
    ANSPRECHPUNKT = "23"
    # kurze Beschreibung der Leistung
    TEASER = "24"
    RECHTSBEHELF = "25"

    def to_label(self) -> str:
        match self:
            case ModulTextTyp.LEISTUNGSBEZEICHNUNG:
                return "Leistungsbezeichnung"
            case ModulTextTyp.LEISTUNGSBEZEICHNUNG_II:
                return "Leistungsbezeichnung II"
            case ModulTextTyp.KURZTEXT:
                return "Kurztext"
            case ModulTextTyp.VOLLTEXT:
                return "Volltext"
            case ModulTextTyp.RECHTSGRUNDLAGEN:
                return "Handlungsgrundlage"  # Rename due to demand of UAG
            case ModulTextTyp.ERFORDERLICHE_UNTERLAGEN:
                return "Erforderliche Unterlagen"
            case ModulTextTyp.VORAUSSETZUNGEN:
                return "Voraussetzungen"
            case ModulTextTyp.VERFAHRENSABLAUF:
                return "Verfahrensablauf"
            case ModulTextTyp.FORMUALRE:
                return "Formulare"
            case ModulTextTyp.WEITERFUEHRENDE_INFORMATIONEN:
                return "Weiterführende Informationen"
            case ModulTextTyp.HINWEISE:
                return "Hinweise"
            case ModulTextTyp.URHEBER:
                return "Urheber"
            case ModulTextTyp.ZUSTAENDIGE_STELLE:
                return "Zuständige Stelle"
            case ModulTextTyp.ANSPRECHPUNKT:
                return "Ansprechpunkt"
            case ModulTextTyp.TEASER:
                return "Teaser"
            case ModulTextTyp.RECHTSBEHELF:
                return "Rechtsbehelf"


class LeistungsAdressat(Enum):
    BUERGER = "001"
    UNTERNEHMEN = "002"
    VERWALTUNG = "003"

    def to_label(self) -> str:
        match self:
            case LeistungsAdressat.BUERGER:
                return "Bürgerinnen und Bürger"
            case LeistungsAdressat.UNTERNEHMEN:
                return "Unternehmen"
            case LeistungsAdressat.VERWALTUNG:
                return "Verwaltung"


@dataclass(slots=True)
class Zeitraum:
    beginn: datetime | None
    ende: datetime | None
    zusatz: list[StringLocalized]


@dataclass(slots=True, frozen=True)
class Identifikator:
    scheme_agency_id: Annotated[
        str | None, XmlAttribute("schemeAgencyID", namespace=None)
    ]
    scheme_agency_name: Annotated[
        str | None,
        XmlAttribute("schemeAgencyName", namespace=None),
    ]
    scheme_data_uri: Annotated[
        str | None, XmlAttribute("schemeDataURI", namespace=None)
    ]
    scheme_id: Annotated[str | None, XmlAttribute("schemeID", namespace=None)]
    scheme_name: Annotated[str | None, XmlAttribute("schemeName", namespace=None)]
    scheme_uri: Annotated[str | None, XmlAttribute("schemeURI", namespace=None)]
    scheme_version_id: Annotated[
        str | None, XmlAttribute("schemeVersionID", namespace=None)
    ]
    value: Annotated[str, XmlTextValue()]

    def to_xzufi_identifier(self):
        if self.scheme_agency_id is None:
            return None
        return XzufiIdentifier(self.scheme_agency_id, self.value)

    def assert_xzufi_identifier(self):
        identifier = self.to_xzufi_identifier()
        assert identifier is not None
        return identifier


def parse_identifikator(node: xml.XmlElement) -> Identifikator:
    return Identifikator(
        scheme_agency_id=xml.optional_str_to_token(node.get("schemeAgencyID")),
        scheme_agency_name=node.get("schemeAgencyName"),
        scheme_data_uri=node.get("schemeDataURI"),
        scheme_id=xml.optional_str_to_token(node.get("schemeID")),
        scheme_name=node.get("schemeName"),
        scheme_uri=node.get("schemeURI"),
        scheme_version_id=xml.optional_str_to_token(node.get("schemeVersionID")),
        value=xml.parse_token(node),
    )


@dataclass(slots=True)
class Leistungsmodul:
    position_darstellung: Annotated[int | None, XmlValue(name="positionDarstellung")]
    id: Identifikator | None
    id_sekundaer: Annotated[list[Identifikator], XmlValue(name="idSekundaer")]
    gueltigkeit: list[Zeitraum]


class LeiKaInstanz(Enum):
    LEIKA = "99"
    SCHLESWIG_HOLSTEIN = "01"
    HAMBURG = "02"
    NIEDERSACHSEN = "03"
    BREMEN = "04"
    NORDRHEIN_WESTFALEN = "05"
    HESSEN = "06"
    RHEINLAND_PFALZ = "07"
    BADEN_WUERTTEMBERG = "08"
    BAYERN = "09"
    SAARLAND = "10"
    BERLIN = "11"
    BRANDENBURG = "12"
    MECKENBURG_VORPOMMERN = "13"
    SACHSEN = "14"
    SACHSEN_ANHALT = "15"
    THUERINGEN = "16"


@dataclass(slots=True)
class Leistungsschluessel:
    """
    A class representing a syntactically correct Leistungsschluessel.
    Each `Leistungsschluessel` is the result of concatenating 4 parts.

    See more:
    https://fimportal.de/fim-haus -> LeiKa-Handbuch
    """

    instanz: LeiKaInstanz
    leistungs_gruppierung: int
    leistungs_kennung: int
    verrichtungs_kennung: int
    verrichtungs_detail: int

    @staticmethod
    def from_str(value: str) -> "Leistungsschluessel":
        if len(value) != 14:
            raise XzufiException(
                f"Invalid Leistungsschluessel: invalid length [value=[{value}]"
            )

        try:
            int(value)
        except Exception:
            raise XzufiException(
                f"Invalid Leistungsschluessel: invalid characters, only digits are allowed [value={value}]"
            )

        try:
            instanz = LeiKaInstanz(value[:2])
        except Exception:
            raise XzufiException(
                f"Invalid Leistungsschluessel: unknown 'Instanz' [value={value}]"
            )

        # TODO: Check for valid value: https://www.xrepository.de/details/urn:de:fim:leika:leistungsgruppierung
        leistungs_gruppierung = int(value[2:5])
        leistungs_kennung = int(value[5:8])
        # TODO: Check for valid value: https://www.xrepository.de/details/urn:de:fim:leika:verrichtung
        verrichtungs_kennung = int(value[8:11])
        verrichtungs_detail = int(value[11:14])

        if leistungs_gruppierung == 0:
            raise XzufiException(
                f"Invalid Leistungsschluessel: '000' not valid for 'Leistungsgruppierung' [value={value}]"
            )

        # If the LeiKa instanz is the central LeiKa catalogue, only numbers in the range 000-899 are allows
        # for the following identifiers.
        if instanz == LeiKaInstanz.LEIKA:
            if (
                leistungs_gruppierung >= 900
                or leistungs_kennung >= 900
                or verrichtungs_kennung >= 900
                or verrichtungs_detail >= 900
            ):
                raise XzufiException(
                    f"Invalid Leistungsschluessel: invalid 'Instanz' [value={value}]"
                )

        return Leistungsschluessel(
            instanz=instanz,
            leistungs_gruppierung=leistungs_gruppierung,
            leistungs_kennung=leistungs_kennung,
            verrichtungs_kennung=verrichtungs_kennung,
            verrichtungs_detail=verrichtungs_detail,
        )

    def __str__(self) -> str:
        return f"{self.instanz.value}{self.leistungs_gruppierung:03}{self.leistungs_kennung:03}{self.verrichtungs_kennung:03}{self.verrichtungs_detail:03}"


logger = logging.getLogger(__name__)


def language_code_tuple_to_dict(
    raw: list[tuple[LanguageCode, str]],
) -> dict[LanguageCode, str]:
    # Each string must only be included once per language.
    # This is normally an error, however there are so many documents with this error that we had to just log a warning instead
    # and just ignore the duplicates.
    # See: https://api.infodienste.de/fileadmin/Bibliothek/tsa.de/Schnittstellen_Dokumente/20190626_xzufi_2_2_0_Spezifikation.pdf
    # Chapter: II.1.4 String.Localized
    language_code_to_inhalt: dict[LanguageCode, str] = {}
    for language_code, inhalt in raw:
        if language_code in language_code_to_inhalt:
            logger.warning(
                "Duplicate entry for language code '%s'. Existing value will be overwritten. [existing=%s, new=%s]",
                language_code,
                language_code_to_inhalt[language_code][:100],
                inhalt[:100],
            )

        language_code_to_inhalt[language_code] = inhalt
    return language_code_to_inhalt


def parse_optional_date(node: xml.XmlElement) -> date | None:
    # NOTE(Felix): Despite the return value being optional, at this point we want to ensure there is
    # a value here. Returning `None` is only necessary because there are sometimes invalid dates specified,
    # which we want to handle as `None`. But a missing value here would be an actual error.
    value = xml.parse_value(node)

    # NOTE(Felix): Some dates begin with a '-', e.g. '-0001-11-30'. This is AFAIK just a weird placeholder
    # for `None`.
    if value.startswith("-"):
        return None

    # Some xzufi dates have the form "YYYY-MM-DD+??:??". However, timezone information'
    # is not a valid part of an ISO 8601 date.
    # As we do not need the timezone for dates, just try to split it away, by removing
    # the content starting from the last '+'.
    if "+" in value:
        _value = value.rsplit("+", maxsplit=1)[0]
    else:
        _value = value

    try:
        return date.fromisoformat(_value)
    except ValueError as error:
        raise InternalXzufiException(f"Invalid date '{value}'") from error


def parse_optional_datetime(node: xml.XmlElement) -> datetime | None:
    # NOTE(Felix): Despite the return value being optional, at this point we want to ensure there is
    # a value here. Returning `None` is only necessary because there are sometimes invalid dates specified,
    # which we want to handle as `None`. But a missing value here would be an actual error.
    value = xml.parse_value(node)

    # NOTE(Felix): Some datetimes begin with a '-', e.g. '-0001-11-30T00:00:00+00:53'. This is AFAIK just a weird placeholder
    # for `None`.
    if value.startswith("-"):
        return None

    try:
        return ciso8601.parse_datetime(value).astimezone(timezone.utc)
    except ValueError as error:
        raise InternalXzufiException(f"Invalid datetime '{value}'") from error


@dataclass(slots=True)
class Versionsinformation:
    erstellt_datum_zeit: datetime | None
    erstellt_durch: str | None  # string.latin
    geaendert_datum_zeit: datetime | None
    geaendert_durch: str | None  # string.latin
    version: str | None  # string.latin


@dataclass(slots=True)
class Sprache:
    language_code: Annotated[
        LanguageCode, RequiredValueEncoding(decode=_decode_language_code)
    ]
    sprachbezeichnung_deutsch: str | None
    sprachbezeichnung_nativ: str | None


@dataclass(slots=True)
class Sprachversion(Sprache):
    erstellt_datum_zeit: datetime | None
    erstellt_durch: str | None
    geaendert_datum_zeit: datetime | None
    geaendert_durch: str | None
    version: str | None


@dataclass(slots=True)
class HyperlinkErweitert:
    language_code: Annotated[str | None, XmlAttribute(namespace=None)]
    uri: str
    titel: str | None
    beschreibung: str | None
    position_darstellung: int | None


@dataclass(slots=True)
class Kategorieklasse:
    id: Identifikator
    idSekundaer: list[Identifikator]
    bezeichnung: list[StringLocalized]
    beschreibung: list[StringLocalized]
    version: str | None


@dataclass(slots=True)
class Gebietsklasse(Kategorieklasse):
    kurzbezeichnung: list[StringLocalized]


@dataclass(slots=True)
class Gebietstyp:
    id: Identifikator
    idSekundaer: list[Identifikator]
    klasse: Gebietsklasse
    bezeichnung: list[str]
    kurzbezeichnung: list[str]
    beschreibung: list[StringLocalized]


@dataclass(slots=True)
class Herausgeber:
    id: Identifikator
    id_sekundaer: list[Identifikator]
    bezeichnung: list[str]
    kurzbezeichnung: list[str]
    beschreibung: list[StringLocalized]
    standard_zustaendigkeitsgebiet: list["Gebiet"]


@dataclass(slots=True)
class Gebiet:
    id: Identifikator
    idSekundaer: list[Identifikator]
    typ: Gebietstyp
    uebergeordnetesGebietId: list[Identifikator]
    untergeordnetesGebietId: list[Identifikator]
    verwaltungspolitischeKodierung: VerwaltungspolitischeKodierung | None
    bezeichnung: list[StringLocalized]
    kurzbezeichnung: list[StringLocalized]
    beschreibung: list[StringLocalized]
    herausgeber: Herausgeber | None
    versionsinformationen: Versionsinformation | None
    gueltigkeit: list[Zeitraum]


GebietEncoding = xmlstruct.derive(
    Gebiet, namespace=XZUFI_NAMESPACE, local_name="gebiet"
)


def remove_user_data(xml_content: str) -> str:
    """
    Remove all tags containing user data from the xml content of an <xzufi:leistung>.
    """
    document = xml.parse_document(xml_content)

    for path in [
        "//xzufi:leistung/xzufi:sprachversion/xzufi:erstelltDurch",
        "//xzufi:leistung/xzufi:sprachversion/xzufi:geaendertDurch",
        "//xzufi:leistung/xzufi:versionsinformation/xzufi:erstelltDurch",
        "//xzufi:leistung/xzufi:versionsinformation/xzufi:geaendertDurch",
    ]:
        nodes: Any = document.xpath(path, namespaces=XPATH_NS_MAP)

        for node in nodes:
            node.getparent().remove(node)

    return xml.serialize(document).decode("utf-8")


class SdgRelevanz(Enum):
    YES = "yes"
    NO = "no"
    UNKNOWN = "unknown"

    @staticmethod
    def from_sdg_informationsbereiche(values: list[str]) -> "SdgRelevanz":
        """
        See: https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:sdginformationsbereich
        """
        if len(values) == 0:
            return SdgRelevanz.UNKNOWN
        elif values == ["0000000"]:
            return SdgRelevanz.NO
        else:
            return SdgRelevanz.YES

    def is_sdg_relevant(self) -> bool | None:
        match self:
            case SdgRelevanz.YES:
                return True
            case SdgRelevanz.NO:
                return False
            case SdgRelevanz.UNKNOWN:
                return None


class Zahlungsweise(Enum):
    """
    See codelist: https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.zahlungsweise
    """

    BARGELDZAHLUNG = "1000000"
    HALBBARE_ZAHLUNG = "2000000"
    UEBERWEISUNG_ZAHLSCHEIN = "2000100"
    POSTNACHNAHME = "2000200"
    BARSCHECK = "2000300"
    WESTERN_UNION_GELDTRANSFER = "2000400"
    BARGELDLOSE_ZAHLUNG = "3000000"
    UEBERWEISUNG = "3010000"
    SEPA_UEBERWEISUNG = "3010100"
    SAMMELUEBERWEISUNG = "3010200"
    DAUERAUFTRAG = "3010300"
    LASTSCHRIFTVERFAHREN = "3020000"
    SEPA_LASTSCHRIFT = "3020100"
    SEPA_FIRMENLASTSCHRIFT = "3020200"
    RECHNUNG = "3040000"
    KREDITKARTEN = "3050000"
    KLASSISCHE_KREDITKARTE = "3050100"
    VISA_KARTE = "3050101"
    AMERICAN_EXPRESS = "3050102"
    MASTERCARD = "3050103"
    DINERS_CLUB = "3050104"
    DEBITKARTE = "3050200"
    GIROCARD = "3050201"
    MAESTRO_CARD = "3050202"
    MASTERCARD_DEBIT = "3050203"
    VISA_ELECTRON_IN_EUROPA = "3050204"
    V_PAY = "3050205"
    PREPAIDKARTEN = "3050300"
    PAYSAFECARD = "3050302"
    GELDKARTE = "3050303"
    BEZAHLSYSTEME = "3060000"
    PAYPAL = "3060100"
    AMAZON_PAYMENT = "3060200"
    BILLPAY = "3060300"
    PAYPAL_PLUS = "3060400"
    KLARNA = "3060600"
    PAYMORROW = "3060700"
    RATEPAY = "3060800"
    BARZAHLEN_DE = "3061000"
    SKRILL = "3061200"
    WIRECARD = "3061300"
    PAYDIREKT = "3061400"
    NETELLER = "3061500"
    WORLDLINE_SCHWEIZ = "3061600"
    MOBILE_PAYMENT = "3070000"
    SMS = "3070200"
    HANDYPAY = "3070201"
    BOKU = "3070202"
    SMARTPHONE_BEZAHLSYSTEME = "3070300"
    BOON = "3070301"
    GO4Q = "3070303"
    KONTAKTLOS_BEZAHLEN = "3070400"
    GIROGO = "3070401"
    VISA_PAYWAVE = "3070402"
    PAYPASS = "3070403"
    GOOGLE_PAY = "3070404"
    APPLE_PAY = "3070405"
    CASHCLOUD = "3070406"
    TARGOBANK_BEZAHLCHIP = "3070408"
    ONLINE_BANKING = "3080000"
    GIROPAY = "3080100"
    SOFORTUEBERWEISUNG_DE = "3080200"
    KRYPTOWAEHRUNG = "3090000"
    BITCOIN = "3090100"
    ETHEREUM = "3090200"
    RIPPLE = "3090300"
    ETHEREUM_CLASSIC = "3090500"
    LITECOIN = "3090600"
    DASH = "3090700"


class Staatsangehoerigkeit(Enum):
    """
    See codelist: https://www.xrepository.de/details/urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:staatsangehoerigkeit
    """

    AFGHANISTAN = "423"
    AEGYPTEN = "287"
    ALBANIEN = "121"
    ALGERIEN = "221"
    ANDORRA = "123"
    ANGOLA = "223"
    ANTIGUA_UND_BARBUDA = "320"
    AEQUATORIALGUINEA = "274"
    ARGENTINIEN = "323"
    ARMENIEN = "422"
    ASERBAIDSCHAN = "425"
    AETHIOPIEN = "225"
    AUSTRALIEN = "523"
    BAHAMAS = "324"
    BAHRAIN = "424"
    BANGLADESCH = "460"
    BARBADOS = "322"
    BELARUS = "169"
    BELGIEN = "124"
    BELIZE = "330"
    BENIN = "229"
    BHUTAN = "426"
    BOLIVIEN = "326"
    BOSNIEN_UND_HERZEGOWINA = "122"
    BOTSUANA = "227"
    BRASILIEN = "327"
    BRUNEI_DARUSSALAM = "429"
    BULGARIEN = "125"
    BURKINA_FASO = "258"
    BURUNDI = "291"
    CABO_VERDE = "242"
    CHILE = "332"
    CHINA = "479"
    COSTA_RICA = "334"
    COTE_DIVOIRE = "231"
    DAENEMARK = "126"
    DEUTSCHLAND = "000"
    DOMINICA = "333"
    DOMINIKANISCHE_REPUBLIK = "335"
    DSCHIBUTI = "230"
    ECUADOR = "336"
    EL_SALVADOR = "337"
    ERITREA = "224"
    ESTLAND = "127"
    ESWATINI = "281"
    FIDSCHI = "526"
    FINNLAND = "128"
    FRANKREICH = "129"
    GABUN = "236"
    GAMBIA = "237"
    GEORGIEN = "430"
    GHANA = "238"
    GRENADA = "340"
    GRIECHENLAND = "134"
    GUATEMALA = "345"
    GUINEA = "261"
    GUINEA_BISSAU = "259"
    GUYANA = "328"
    HAITI = "346"
    HONDURAS = "347"
    HONGKONG = "411"
    INDIEN = "436"
    INDONESIEN = "437"
    IRAK = "438"
    IRAN = "439"
    IRLAND = "135"
    ISLAND = "136"
    ISRAEL = "441"
    ITALIEN = "137"
    JAMAIKA = "355"
    JAPAN = "442"
    JEMEN = "421"
    JORDANIEN = "445"
    JUGOSLAWIEN = "120"
    JUGOSLAWIEN_BUNDESREPUBLIK = "138"
    KAMBODSCHA = "446"
    KAMERUN = "262"
    KANADA = "348"
    KASACHSTAN = "444"
    KATAR = "447"
    KENIA = "243"
    KIRGISISTAN = "450"
    KIRIBATI = "530"
    KOLUMBIEN = "349"
    KOMOREN = "244"
    KONGO = "245"
    KONGO_DEMOKRATISCHE_REPUBLIK = "246"
    KOREA_DEMOKRATISCHE_VOLKSREPUBLIK = "434"
    KOREA_REPUBLIK = "467"
    KOSOVO = "150"
    KROATIEN = "130"
    KUBA = "351"
    KUWAIT = "448"
    LAOS = "449"
    LESOTHO = "226"
    LETTLAND = "139"
    LIBANON = "451"
    LIBERIA = "247"
    LIBYEN = "248"
    LIECHTENSTEIN = "141"
    LITAUEN = "142"
    LUXEMBURG = "143"
    MACAU = "412"
    MADAGASKAR = "249"
    MALAWI = "256"
    MALAYSIA = "482"
    MALEDIVEN = "454"
    MALI = "251"
    MALTA = "145"
    MAROKKO = "252"
    MARSHALLINSELN = "544"
    MAURETANIEN = "239"
    MAURITIUS = "253"
    MEXIKO = "353"
    MIKRONESIEN = "545"
    MOLDAU = "146"
    MONACO = "147"
    MONGOLEI = "457"
    MONTENEGRO = "140"
    MOSAMBIK = "254"
    MYANMAR = "427"
    NAMIBIA = "267"
    NAURU = "531"
    NEPAL = "458"
    NEUSEELAND = "536"
    NICARAGUA = "354"
    NIEDERLANDE = "148"
    NIGER = "255"
    NIGERIA = "232"
    NORDMAZEDONIEN = "144"
    NORWEGEN = "149"
    OESTERREICH = "151"
    PAKISTAN = "461"
    PALAE_STINENSISCHE_GEBIETE = "459"
    PALAU = "537"
    PANAMA = "357"
    PAPUA_NEUGUINEA = "538"
    PARAGUAY = "359"
    PERU = "361"
    PHILIPPINEN = "462"
    POLEN = "152"
    PORTUGAL = "153"
    RUANDA = "265"
    RUMAENIEN = "154"
    RUSSISCHE_FOEDERATION = "160"
    SALOMONEN = "524"
    SAMBIA = "257"
    SAMOA = "543"
    SAN_MARINO = "156"
    SAO_TOME_UND_PRINCIPE = "268"
    SAUDI_ARABIEN = "472"
    SCHWEDEN = "157"
    SCHWEIZ = "158"
    SENEGAL = "269"
    SERBIEN = "170"
    SERBIEN_EINSCHLIESSLICH_KOSOVO = "133"
    SERBIEN_UND_MONTENEGRO = "132"
    SEYCHELLEN = "271"
    SIERRA_LEONE = "272"
    SIMBABWE = "233"
    SINGAPUR = "474"
    SLOWAKEI = "155"
    SLOWENIEN = "131"
    SOMALIA = "273"
    SOWJETUNION = "159"
    SPANIEN = "161"
    SRI_LANKA = "431"
    ST_KITTS_UND_NEVIS = "370"
    ST_LUCIA = "366"
    ST_VINCENT_UND_DIE_GRENADINEN = "369"
    SUEDAFRIKA = "263"
    SUDAN = "277"
    SUDAN_EINSCHLIESSLICH_SUEDSUDAN = "276"
    SUEDSUDAN = "278"
    SURINAME = "364"
    SYRIEN = "475"
    TADSCHIKISTAN = "470"
    TAIWAN = "465"
    TANSANIA = "282"
    THAILAND = "476"
    TIMOR_LESTE = "483"
    TOGO = "283"
    TONGA = "541"
    TRINIDAD_UND_TOBAGO = "371"
    TSCHAD = "284"
    TSCHECHIEN = "164"
    TSCHECHOSLOWAKEI = "162"
    TUNESIEN = "285"
    TUERKEI = "163"
    TURKMENISTAN = "471"
    TUVALU = "540"
    UGANDA = "286"
    UKRAINE = "166"
    UNGARN = "165"
    URUGUAY = "365"
    USBEKISTAN = "477"
    VANUATU = "532"
    VATIKANSTADT = "167"
    VENEZUELA = "367"
    VEREINIGTE_ARABISCHE_EMIRATE = "469"
    VEREINIGTE_STAATEN = "368"
    VEREINIGTES_KOENIGREICH = "168"
    VIETNAM = "432"
    ZENTRALAFRIKANISCHE_REPUBLIK = "289"
    ZYPERN = "181"


class Vertrauensniveau(Enum):
    """
    urn:xoev-de:fim:codeliste:vertrauensniveau
    """

    BASISREGISTRIERUNG = "10"
    NIEDRIG = "15"
    SUBSTANTIELL = "20"
    HOCH = "30"
    HOCH_PLUS = "40"
    UNBESTIMMT = "00"
