from dataclasses import dataclass
from typing import Annotated

from fimportal import xml, xmlstruct
from fimportal.xzufi.common import (
    XZUFI_NAMESPACE,
    Identifikator,
    StringLocalized,
    XzufiException,
    XzufiIdentifier,
)

from fimportal.xzufi.leistung import (
    Dokumentmodul,
    FachlicheFreigabeModul,
    Textmodul,
    Bearbeitungsdauermodul,
    Kostenmodul,
    TextmodulIndividuell,
    Fristmodul,
    UrsprungsportalModul,
    BegriffImKontextModul,
    Auskunftshinweismodul,
)

Leistungsmodule = (
    Textmodul
    | Bearbeitungsdauermodul
    | Kostenmodul
    | TextmodulIndividuell
    | Fristmodul
    | FachlicheFreigabeModul
    | UrsprungsportalModul
    | BegriffImKontextModul
    | Auskunftshinweismodul
    | Dokumentmodul
)


_KostenmodulEncoding = xmlstruct.derive(
    Kostenmodul, namespace=XZUFI_NAMESPACE, local_name="modul"
)
_TextmodulEncoding = xmlstruct.derive(
    Textmodul, namespace=XZUFI_NAMESPACE, local_name="modul"
)
_FristmodulEncoding = xmlstruct.derive(
    Fristmodul, namespace=XZUFI_NAMESPACE, local_name="modul"
)
_BearbeitungsdauermodulEncoding = xmlstruct.derive(
    Bearbeitungsdauermodul, namespace=XZUFI_NAMESPACE, local_name="modul"
)
_TextmodulIndividuellEncoding = xmlstruct.derive(
    TextmodulIndividuell, namespace=XZUFI_NAMESPACE, local_name="modul"
)
_FachlicheFreigabeModulEncoding = xmlstruct.derive(
    FachlicheFreigabeModul, namespace=XZUFI_NAMESPACE, local_name="modul"
)
_BegriffImKontextModulEncoding = xmlstruct.derive(
    BegriffImKontextModul, namespace=XZUFI_NAMESPACE, local_name="modul"
)
_AuskunftshinweismodulEncoding = xmlstruct.derive(
    Auskunftshinweismodul, namespace=XZUFI_NAMESPACE, local_name="modul"
)
_UrsprungsportalModulEncoding = xmlstruct.derive(
    UrsprungsportalModul, namespace=XZUFI_NAMESPACE, local_name="modul"
)
_DokumentModulEncoding = xmlstruct.derive(
    Dokumentmodul, namespace=XZUFI_NAMESPACE, local_name="modul"
)


def _decode_leistungsmodul_spezialisierung(
    node: xml.XmlElement,
) -> Leistungsmodule | None:
    attribute_type_value = node.attrib.get(
        "{http://www.w3.org/2001/XMLSchema-instance}type"
    )
    assert attribute_type_value is not None

    match attribute_type_value:
        case "xzufi:Kostenmodul":
            return _KostenmodulEncoding.parse(node)
        case "xzufi:Textmodul":
            return _TextmodulEncoding.parse(node)
        case "xzufi:Fristmodul":
            return _FristmodulEncoding.parse(node)
        case "xzufi:Bearbeitungsdauermodul":
            return _BearbeitungsdauermodulEncoding.parse(node)
        case "xzufi:TextmodulIndividuell":
            return _TextmodulIndividuellEncoding.parse(node)
        case "xzufi:FachlicheFreigabeModul":
            return _FachlicheFreigabeModulEncoding.parse(node)
        case "xzufi:BegriffImKontextModul":
            return _BegriffImKontextModulEncoding.parse(node)
        case "xzufi:Auskunftshinweismodul":
            return _AuskunftshinweismodulEncoding.parse(node)
        case "xzufi:UrsprungsportalModul":
            return _UrsprungsportalModulEncoding.parse(node)
        case "xzufi:Dokumentmodul":
            return _DokumentModulEncoding.parse(node)
        case _:
            raise Exception(f"Unknown modul typ: {attribute_type_value}")


@dataclass(slots=True)
class LeistungsmodulSpezialisierung:
    modul: (
        Annotated[
            Leistungsmodule,
            xmlstruct.RequiredValueEncoding(
                decode=_decode_leistungsmodul_spezialisierung
            ),
        ]
        | None
    )
    ersetzung: bool


@dataclass(slots=True)
class Spezialisierung:
    id_leistung: Identifikator
    id_gebiet: list[Identifikator]
    bezeichnung: list[StringLocalized]
    modul_spezialisiert: list[LeistungsmodulSpezialisierung]
    id: Identifikator | None
    id_sekundaer: list[Identifikator]

    def get_identifier(self) -> XzufiIdentifier | None:
        if self.id is None:
            return None
        return self.id.to_xzufi_identifier()


SpezialisierungEncoding = xmlstruct.derive(
    Spezialisierung, namespace=XZUFI_NAMESPACE, local_name="spezialisierung"
)


def parse_spezialisierung(content: str) -> Spezialisierung:
    try:
        return SpezialisierungEncoding.parse(content)
    except xml.ParserException as ex:
        raise XzufiException(f"Could not parse Spezialisierung: {str(ex)}") from ex
