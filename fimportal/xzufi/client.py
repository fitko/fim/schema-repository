from __future__ import annotations

from abc import ABC, abstractmethod
from pathlib import Path
from typing import AsyncGenerator, TypeVar

import httpx

from fimportal import xml

from .common import (
    XZUFI_REDAKTION_ID,
    InternalXzufiException,
    XzufiException,
)
from .leistung import (
    XZUFI_NAMESPACE,
    Leistung,
    RedaktionsId,
    parse_leistung_response,
)

XSD_SCHEMA_PATH = Path(__file__).parent / "xsd" / "xzufi-leistungen.xsd"
XSD_SCHEMA = xml.load_xsd(XSD_SCHEMA_PATH)


SOAP_NAMESPACE = "http://schemas.xmlsoap.org/soap/envelope/"

SOAP_ENVELOPE = f"{{{SOAP_NAMESPACE}}}Envelope"
SOAP_BODY = f"{{{SOAP_NAMESPACE}}}Body"
XZUFI_ANTWORT_LEISTUNG = f"{{{XZUFI_NAMESPACE}}}leistungen.antwort.leistung.040104"


SEARCH_LEISTUNGEN = """
<xzufi:leistungen.anfrage.leistung.040101
    xmlns:xzufi="http://xoev.de/schemata/xzufi/2_2_0"
    produktbezeichnung="FIM Sammelrepository" produkthersteller="FITKO" xzufiVersion="2.2.0">
    
    <!-- Nachrichtenkopf --> 
    <xzufi:nachrichtenkopf>
        <xzufi:nachrichtUUID>b484bb1e-6002-4931-b945-bfbade577fa9</xzufi:nachrichtUUID>
        <xzufi:erstelltDatumZeit>2017-06-20T06:10:59.099Z</xzufi:erstelltDatumZeit>
        <xzufi:sender>Teleport Beispiel</xzufi:sender>
    </xzufi:nachrichtenkopf>
    
    <!-- Anfrage und Suchprofil -->
    <xzufi:anfrage>
        <xzufi:suchprofil>
            <xzufi:kennzeichenNurBasisinformation>false</xzufi:kennzeichenNurBasisinformation>
        </xzufi:suchprofil>
    </xzufi:anfrage>
    
</xzufi:leistungen.anfrage.leistung.040101>
"""


class Client(ABC):
    """
    Interface for a XZuFi client.
    Makes it easy to provide a test client during tests.
    """

    @abstractmethod
    async def iterate_leistung_batches(
        self,
    ) -> AsyncGenerator[list[tuple[Leistung, str]], None]: ...

    @abstractmethod
    def get_redaktions_id(self) -> RedaktionsId: ...


class HttpClient(Client):
    """
    The server uses WSSE [1] for authetication.
    The final request should look like this:

    ```
        <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
            <s:Header>
                <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                    <wsse:UsernameToken>
                        <wsse:Username>[plain text username goes here]</wsse:Username>
                        <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText"/>
                    </wsse:UsernameToken>
                </wsse:Security>
            </s:Header>
            <s:Body>
                <!-- Request specific body -->
            </s:Body>
        </s:Envelope>
    ```

    1: https://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0.pdf
    """

    def __init__(self, base_url: str, redaktions_id: RedaktionsId, api_key: str):
        self._base_url = base_url
        self._redaktions_id = redaktions_id

        self._soap_start = (
            f'<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xzufi="{XZUFI_NAMESPACE}">'
            "<s:Header>"
            '<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'
            "<wsse:UsernameToken>"
            f"<wsse:Username>{api_key}</wsse:Username>"
            '<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText"/>'
            "</wsse:UsernameToken>"
            "</wsse:Security>"
            "</s:Header>"
            "<s:Body>"
        )
        self._soap_end = "</s:Body></s:Envelope>"

        self._client = httpx.AsyncClient(timeout=120)

    @staticmethod
    def create_baustein_client(api_key: str) -> HttpClient:
        return HttpClient(
            # base_url="https://xzufi-v2-2-0-leika-schul.zfinder.de:443/",
            base_url="https://xzufi-v2-2-0-leika.infodienste.de/?wsdl",
            redaktions_id=XZUFI_REDAKTION_ID,
            api_key=api_key,
        )

    def get_redaktions_id(self) -> RedaktionsId:
        return self._redaktions_id

    async def iterate_leistung_batches(
        self,
    ) -> AsyncGenerator[list[tuple[Leistung, str]], None]:
        offset = 0

        while True:
            batch = await self._load_leistungen_batch(offset)
            if len(batch) == 0:
                return

            offset += len(batch)

            # Make sure, that all imports from this Redaktion are really
            for leistung, _ in batch:
                if leistung.id.scheme_agency_id != self._redaktions_id:
                    raise XzufiException(
                        f"Leistung {leistung.id.value} has a wrong RedaktionId [value={leistung.id.scheme_agency_id}, expected={self._redaktions_id}]",
                    )

            yield batch

    async def _load_leistungen_batch(self, offset: int) -> list[tuple[Leistung, str]]:
        content = self._soap_start + SEARCH_LEISTUNGEN + self._soap_end

        http_response = await self._client.post(
            self._base_url,
            content=content,
            headers={"xzufi-result-offset": str(offset)},
        )
        data = http_response.read()

        if not http_response.is_success:
            raise XzufiException(f"Failed to load Leistungen: \n{data}")

        return parse_leistung_soap_response(data)


TEST_LEISTUNG_XML = """
<xzufi:leistung xmlns:xzufi="http://xoev.de/schemata/xzufi/2_2_0">
    <xzufi:id schemeAgencyID="TSA_TELEPORT" schemeAgencyName="TSA Public Service GmbH"
        schemeID="TSA_LEIKA_OID" schemeName="Leika Schul ObjektID">123456789</xzufi:id>
</xzufi:leistung>
"""


class FakeClient(Client):
    def __init__(self, redaktions_id: RedaktionsId):
        self._redaktions_id = redaktions_id
        self.leistungen: list[tuple[Leistung, str]] = []

    async def iterate_leistung_batches(
        self,
    ) -> AsyncGenerator[list[tuple[Leistung, str]], None]:
        yield self.leistungen

    def get_redaktions_id(self) -> RedaktionsId:
        return self._redaktions_id

    def add_leistung(self, leistung: Leistung, xml_content: str = TEST_LEISTUNG_XML):
        self.leistungen.append((leistung, xml_content))


T = TypeVar("T")


def parse_leistung_soap_response(data: bytes) -> list[tuple[Leistung, str]]:
    try:
        response = _parse_soap_content(data)
        return parse_leistung_response(response)
    except (InternalXzufiException, xml.ParserException) as error:
        raise XzufiException(
            f"Could not parse leitungs-response: {str(error)}"
        ) from error


def _parse_soap_content(data: bytes) -> xml.XmlElement:
    document = xml.parse_document(data)

    response = document.find(
        "s:Body/x:leistungen.antwort.leistung.040104",
        {"s": SOAP_NAMESPACE, "x": XZUFI_NAMESPACE},
    )

    if response is None:
        raise XzufiException(
            "Invalid response: Could not find element 's:Body/x:leistungen.antwort.leistung.040104'"
        )

    # Deactivate this check, as not all responses from the actual XZuFi Prod System are valid.
    """
    try:
        XSD_SCHEMA.assertValid(response)
    except lxml.etree.DocumentInvalid as error:
        with open("invalid_soap_response_2.xml", "wb") as file:
            file.write(data)
        raise XzufiException(f"Invalid response: {str(error)}") from error
    """

    return response
