"""
XZuFi has a special `Transfer` message to sync with an external data source.
These messages contain all historical write and delete events for each of the XZuFI resources.
The events must be processed in order to recreate the current state.
"""

from __future__ import annotations
from dataclasses import dataclass
from enum import Enum
from typing import Generator

from fimportal import xml
from .common import (
    XZUFI_ID,
    XZUFI_NAMESPACE,
    XZUFI_ONLINEDIENST,
    XZUFI_ORGANISATIONSEINHEIT,
    XZUFI_SPEZIALISIERUNG,
    Identifikator,
    InternalXzufiException,
    XzufiException,
    parse_identifikator,
)
from .leistung import XZUFI_LEISTUNG


XZUFI_TRANSFER_OPREATION = f"{{{XZUFI_NAMESPACE}}}transfer.operation.040502"
XZUFI_TRANFER_SCHREIBE_OBJEKT = f"{{{XZUFI_NAMESPACE}}}schreibe"
XZUFI_TRANFER_LOESCHE_OBJEKT = f"{{{XZUFI_NAMESPACE}}}loesche"
XZUFI_FORMULAR = f"{{{XZUFI_NAMESPACE}}}formular"
XZUFI_GEBIET = f"{{{XZUFI_NAMESPACE}}}gebiet"

XZUFI_ZUSTAENDIGKEIT_TRANSFER_OBJEKT = (
    f"{{{XZUFI_NAMESPACE}}}zustaendigkeitTransferObjekt"
)


@dataclass(slots=True)
class SchreibEvent:
    klasse: SchreibKlasse
    node: xml.XmlElement


class SchreibKlasse(Enum):
    """
    The type of resource being written.
    """

    LEISTUNG = "Leistung"
    ORGANISATIONSEINHEIT = "Organisationseinheit"
    ZUSTAENDIGKEIT_TRANSFER_OBJEKT = "ZustaendigkeitTransferObjekt"
    ONLINEDIENST = "Onlinedienst"
    SPEZIALISIERUNG = "Spezialisierung"
    FORMULAR = "Formular"
    GEBIET = "Gebiet"


@dataclass(slots=True)
class LoeschEvent:
    klasse: LoeschKlasse
    id: Identifikator


class LoeschKlasse(Enum):
    """
    The type of resource being removed.
    This is slightly different than SchreibKlasse and therefore modelled as its own enum.
    """

    LEISTUNG = "Leistung"
    ZUSTAENGIKEIT = "Zustaendigkeit"
    ORGANISATIONSEINHEIT = "Organisationseinheit"
    ONLINEDIENST = "Onlinedienst"
    SPEZIALISIERUNG = "LeistungSpezialisierung"
    FORMULAR = "Formular"
    GEBIET = "Gebiet"


def _parse_loesch_klasse(child: xml.XmlElement) -> LoeschKlasse:
    klasse = child.get("klasse")

    if klasse is None:
        raise InternalXzufiException(f"Missing klasse in {child.tag}")

    try:
        return LoeschKlasse(klasse)
    except ValueError as error:
        raise InternalXzufiException(f"Invalid Klasse '{klasse}'") from error


TransferEvent = SchreibEvent | LoeschEvent


def iter_transfer_events(
    node: xml.XmlElement,
) -> Generator[TransferEvent, None, None]:
    """
    Iterate over all the transfer events contained in the document.
    """

    if node.tag != XZUFI_TRANSFER_OPREATION:
        raise XzufiException(f"Expected {XZUFI_TRANSFER_OPREATION}, found: {node.tag}")

    for child in node:
        if child.tag == XZUFI_TRANFER_SCHREIBE_OBJEKT:
            yield _parse_schreib_event(child)
        elif child.tag == XZUFI_TRANFER_LOESCHE_OBJEKT:
            yield _parse_loesch_event(child)


def _parse_schreib_event(node: xml.XmlElement) -> SchreibEvent:
    resource: xml.ParsedValue[tuple[SchreibKlasse, xml.XmlElement]] = xml.ParsedValue()

    for child in node:
        if child.tag == XZUFI_LEISTUNG:
            resource.set((SchreibKlasse.LEISTUNG, child))
        elif child.tag == XZUFI_ORGANISATIONSEINHEIT:
            resource.set((SchreibKlasse.ORGANISATIONSEINHEIT, child))
        elif child.tag == XZUFI_ZUSTAENDIGKEIT_TRANSFER_OBJEKT:
            resource.set((SchreibKlasse.ZUSTAENDIGKEIT_TRANSFER_OBJEKT, child))
        elif child.tag == XZUFI_ONLINEDIENST:
            resource.set((SchreibKlasse.ONLINEDIENST, child))
        elif child.tag == XZUFI_SPEZIALISIERUNG:
            resource.set((SchreibKlasse.SPEZIALISIERUNG, child))
        elif child.tag == XZUFI_FORMULAR:
            resource.set((SchreibKlasse.FORMULAR, child))
        elif child.tag == XZUFI_GEBIET:
            resource.set((SchreibKlasse.GEBIET, child))
        else:
            assert False, child.tag

    klasse, child_node = resource.expect("xzufi:TransferObjektklassenAuswahl")
    return SchreibEvent(klasse=klasse, node=child_node)


def _parse_loesch_event(node: xml.XmlElement) -> LoeschEvent:
    identifikator: xml.ParsedValue[Identifikator] = xml.ParsedValue()

    klasse = _parse_loesch_klasse(node)

    for child in node:
        if child.tag == XZUFI_ID:
            identifikator.set(parse_identifikator(child))

    id = identifikator.expect(XZUFI_ID)

    return LoeschEvent(klasse=klasse, id=id)
