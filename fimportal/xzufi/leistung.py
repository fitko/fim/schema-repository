import logging
from dataclasses import dataclass
from datetime import date
from enum import Enum
from typing import Annotated, Union

from fimportal import xml, xmlstruct
from fimportal.common import (
    FreigabeStatus,
    RawCode,
    RawCodeEncoding,
    create_code_encoding,
    decode_raw_code,
)
from fimportal.xmlstruct import (
    Attribute as XmlAttribute,
)
from fimportal.xmlstruct import (
    RequiredValueEncoding,
    XmlStructError,
)
from fimportal.xmlstruct import (
    TextValue as XmlTextValue,
)
from fimportal.xmlstruct import (
    Value as XmlValue,
)
from fimportal.xmlstruct import (
    Variant as XmlVariant,
)

from .common import (
    ALT_GERMAN,
    GERMAN,
    LEISTUNGSTYPISIERUNG_WITH_STAMMTEXT,
    XZUFI_NAMESPACE,
    Herausgeber,
    HyperlinkErweitert,
    Identifikator,
    InternalXzufiException,
    LanguageCode,
    LeistungsAdressat,
    LeistungsId,
    Leistungsmodul,
    LeistungsTypisierung,
    ModulTextTyp,
    RedaktionsId,
    Sprachversion,
    StringLocalized,
    Versionsinformation,
    Vertrauensniveau,
    XzufiException,
    Zeitraum,
    expect_translation,
    get_translation,
    parse_optional_date,
)

logger = logging.getLogger(__name__)


XSI_TYPE = "{http://www.w3.org/2001/XMLSchema-instance}type"
PV_LAGEN_URI = "urn:xoev-de:fim:codeliste:pvlagen"


@dataclass(slots=True)
class KategorieKlasse:
    id: Identifikator
    id_sekundaer: list[Identifikator]
    bezeichnung: list[StringLocalized]
    beschreibung: list[StringLocalized]
    version: str | None

    def __post_init__(self):
        if len(self.bezeichnung) == 0:
            raise XzufiException(f"missing 'bezeichnung' for Kategorieklasse {self.id}")

    def get_german_bezeichnung(self):
        for content in self.bezeichnung:
            if content.language_code == GERMAN or content.language_code == ALT_GERMAN:
                return content.value

        raise XzufiException(
            f"Missing german 'bezeichnung' for LeistungskategorieKlasse {self.id}"
        )


@dataclass(slots=True)
class LeistungskategorieKlasse(KategorieKlasse):
    pass


@dataclass(slots=True)
class Leistungskategorie:
    id: Identifikator
    id_sekundaer: list[Identifikator]
    uebergeordnete_kategorie_id: Annotated[
        Identifikator | None, XmlValue("uebergeordneteKategorieID")
    ]
    untergeordnete_kategorie_id: Annotated[
        list[Identifikator], XmlValue("untergeordneteKategorieID")
    ]
    bezeichnung: list[StringLocalized]
    beschreibung: list[StringLocalized]
    klasse: LeistungskategorieKlasse

    def __post_init__(self):
        if len(self.bezeichnung) == 0:
            raise XzufiException(
                f"missing 'bezeichnung' for Leistungskategorie {self.id}"
            )

    def get_german_bezeichnung(self) -> str:
        for translation in self.bezeichnung:
            if (
                translation.language_code == GERMAN
                or translation.language_code == ALT_GERMAN
            ):
                return translation.value

        raise XzufiException(
            f"Missing german 'bezeichnung' for Leistungskategorie {self.id}"
        )

    def get_title(self, german: LanguageCode) -> str:
        """
        Extract a human-readable title for this class for presentation in the UI.
        """
        for content in self.bezeichnung:
            if content.language_code == german:
                return f"{content.value} (individuell)"

        return "Unbekannt"


class LeistungsTyp(Enum):
    LEISTUNGS_OBJEKT = "lo"
    LEISTUNGS_OBJEKT_MIT_VERRICHTUNG = "lov"
    LEISTUNGS_OBJEKT_MIT_VERRICHTUNG_UND_DETAIL = "lovd"

    def to_label(self) -> str:
        match self:
            case LeistungsTyp.LEISTUNGS_OBJEKT:
                return "Leistungsobjekt"
            case LeistungsTyp.LEISTUNGS_OBJEKT_MIT_VERRICHTUNG:
                return "Leistungobjekt mit Verrichtung"
            case LeistungsTyp.LEISTUNGS_OBJEKT_MIT_VERRICHTUNG_UND_DETAIL:
                return "Leistungobjekt mit Verrichtung und Detail"

    def to_short_label(self) -> str:
        return self.value.upper()


LeistungsgruppierungAuswahl = Union[
    Annotated[RawCode, XmlVariant("leistungsgruppierungLeiKa")],
    Annotated[Leistungskategorie, XmlVariant("leistungsgruppierungIndividuell")],
]


LeistungsverrichtungAuswahl = Union[
    Annotated[RawCode, XmlVariant("verrichtungLeiKa")],
    Annotated[Leistungskategorie, XmlVariant("verrichtungIndividuell")],
]


@dataclass(slots=True)
class LeistungsstrukturObjekt:
    leistungsgruppierung: LeistungsgruppierungAuswahl | None


LeistungsstrukturObjektEncoding = xmlstruct.derive(
    LeistungsstrukturObjekt, local_name="struktur", namespace=XZUFI_NAMESPACE
)


@dataclass(slots=True)
class LeistungsstrukturObjektMitVerrichtung:
    leistungsgruppierung: LeistungsgruppierungAuswahl | None
    leistungsobjekt_id: Annotated[Identifikator, XmlValue("leistungsobjektID")]
    verrichtung: LeistungsverrichtungAuswahl


LeistungsstrukturObjektMitVerrichtungEncoding = xmlstruct.derive(
    LeistungsstrukturObjektMitVerrichtung,
    local_name="struktur",
    namespace=XZUFI_NAMESPACE,
)


@dataclass(slots=True)
class LeistungsstrukturObjektMitVerrichtungUndDetail:
    leistungsgruppierung: LeistungsgruppierungAuswahl | None
    leistungsobjekt_id: Annotated[Identifikator, XmlValue("leistungsobjektID")]
    verrichtung: LeistungsverrichtungAuswahl
    verrichtungsdetail: list[StringLocalized]

    def __post_init__(self):
        if len(self.verrichtungsdetail) < 1:
            raise xmlstruct.XmlStructError(
                "Missing `verrichtungsdetail` in LeistungsstrukturObjektMitVerrichtungUndDetail"
            )


LeistungsstrukturObjektMitVerrichtungUndDetailEncoding = xmlstruct.derive(
    LeistungsstrukturObjektMitVerrichtungUndDetail,
    local_name="struktur",
    namespace=XZUFI_NAMESPACE,
)


Leistungsstruktur = Union[
    LeistungsstrukturObjekt,
    LeistungsstrukturObjektMitVerrichtung,
    LeistungsstrukturObjektMitVerrichtungUndDetail,
]


def get_struktur_typ(struktur: Leistungsstruktur) -> LeistungsTyp:
    if isinstance(struktur, LeistungsstrukturObjektMitVerrichtungUndDetail):
        return LeistungsTyp.LEISTUNGS_OBJEKT_MIT_VERRICHTUNG_UND_DETAIL
    if isinstance(struktur, LeistungsstrukturObjektMitVerrichtung):
        return LeistungsTyp.LEISTUNGS_OBJEKT_MIT_VERRICHTUNG
    return LeistungsTyp.LEISTUNGS_OBJEKT


def _decode_leistungsstruktur(node: xml.XmlElement) -> Leistungsstruktur:
    # NOTE(Felix): According to the standard, these types should be
    # represented by the values "lo", "lov" and "lovd". The actual place where
    # these values should be specified is however never mentioned.
    # In practice, these values are never actually used anywhere.
    # Instead, the type is derived from the attribute "xsi:type".
    # The used values are not mentioned anywhere in the standard and are AFAIK
    # just based on the implementation from the Redaktionssystem.
    value = node.get(XSI_TYPE)
    match value:
        case "xzufi:LeistungsstrukturObjekt":
            return LeistungsstrukturObjektEncoding.parse(node)
        case "xzufi:LeistungsstrukturObjektMitVerrichtung":
            return LeistungsstrukturObjektMitVerrichtungEncoding.parse(node)
        case "xzufi:LeistungsstrukturObjektMitVerrichtungUndDetail":
            return LeistungsstrukturObjektMitVerrichtungUndDetailEncoding.parse(node)
        case _:
            raise XzufiException(f"Invalid 'xsi:type' in 'xzufi:struktur': {value}")


LeistungsstrukturEncoding = RequiredValueEncoding(decode=_decode_leistungsstruktur)


@dataclass(slots=True)
class Textmodul(Leistungsmodul):
    leika_textmodul: Annotated[ModulTextTyp, create_code_encoding(ModulTextTyp)]

    leika_textmodul_abweichende_bezeichnung: Annotated[
        list[StringLocalized], XmlValue("leikaTextModulAbweichendeBezeichnung")
    ]

    inhalt: list[StringLocalized]
    weiterfuehrender_link: list[HyperlinkErweitert]

    def expect_inhalt(self, language_code: LanguageCode) -> str:
        try:
            return expect_translation(self.inhalt, language_code)
        except XzufiException as error:
            raise XzufiException(
                f"Error in Textmodul {self.leika_textmodul}: {str(error)}"
            ) from error

    def get_inhalt(self, language_code: LanguageCode) -> str | None:
        return get_translation(self.inhalt, language_code)


@dataclass(slots=True)
class TextmodulIndividuell(Leistungsmodul):
    individuelles_textmodultyp: RawCode
    inhalt: list[StringLocalized]
    weiterfuehrender_link: list[HyperlinkErweitert]


class Zeiteinheit(Enum):
    JAHR = "JAHR"
    MONAT = "MONAT"
    WOCHE = "WOCHE"
    TAG = "TAG"
    WERKTAG = "WERKTAG"
    STUNDE = "STUNDE"
    MINUTE = "MINUTE"
    SEKUNDE = "SEKUNDE"

    def to_label(self) -> str:
        match self:
            case Zeiteinheit.JAHR:
                return "Jahr(e)"
            case Zeiteinheit.MONAT:
                return "Monat(e)"
            case Zeiteinheit.WOCHE:
                return "Woche(n)"
            case Zeiteinheit.TAG:
                return "Tag(e)"
            case Zeiteinheit.WERKTAG:
                return "Werktag(e)"
            case Zeiteinheit.STUNDE:
                return "Stunde(n)"
            case Zeiteinheit.MINUTE:
                return "Minute(n)"
            case Zeiteinheit.SEKUNDE:
                return "Sekunde(n)"


@dataclass(slots=True)
class FristDauer:
    dauer: int
    einheit: Annotated[Zeiteinheit, create_code_encoding(Zeiteinheit)]
    dauer_bis: int | None
    einheit_bis: Annotated[Zeiteinheit, create_code_encoding(Zeiteinheit)] | None


def _parse_date_with_timezone(node: xml.XmlElement) -> date | None:
    """
    Some XZuFi dates have timezone information in dates, e.g. '2023-03-15+01:00'.
    This is not ISO8601 compliant and can savely be ignored, as the timezone information
    is useless without an actual timestamp.
    """
    value = node.text
    if value is None:
        return None

    if "+" in value:
        value = value.rsplit("+", maxsplit=1)[0]

    return date.fromisoformat(xml.str_to_token(value))


TimezoneDateEncoding = RequiredValueEncoding(_parse_date_with_timezone)


@dataclass(slots=True)
class MonatTag:
    value: Annotated[str, XmlTextValue()]


FristStichtagWert = Union[
    Annotated[date, XmlVariant("datum"), TimezoneDateEncoding],
    Annotated[MonatTag, XmlVariant("monatTag")],
    # This variant never actually exists in our data, so there is currently no test case.
    Annotated[str, XmlVariant("tag")],
]


@dataclass(slots=True)
class FristStichtag:
    von: FristStichtagWert | None
    bis: FristStichtagWert | None


FristAuswahl = Union[
    Annotated[FristDauer, XmlVariant("fristDauer")],
    Annotated[FristStichtag, XmlVariant("fristStichtag")],
]


class Fristtyp(Enum):
    ANHOERUNGSFRIST = "001"
    ANTRAGSFRIST = "002"
    AUFBEWAHRUNGSFRIST = "003"
    GELTUNGSDAUER = "005"
    GENEHMIGUNGSFIKTION = "006"
    WIDERSPRUCHSFIRST = "009"

    def to_label(self) -> str:
        match self:
            case Fristtyp.ANHOERUNGSFRIST:
                return "Anhörungsfrist"
            case Fristtyp.ANTRAGSFRIST:
                return "Antragsfrist"
            case Fristtyp.AUFBEWAHRUNGSFRIST:
                return "Aufbewahrungsfrist"
            case Fristtyp.GELTUNGSDAUER:
                return "Geltungsdauer"
            case Fristtyp.GENEHMIGUNGSFIKTION:
                return "Genehmigungsfiktion"
            case Fristtyp.WIDERSPRUCHSFIRST:
                return "Widerspruchsfrist"


# Called "Fristtyp_Erweiterbar" in the standard
FristtypErweiterbar = Union[
    Annotated[Fristtyp, XmlVariant("code"), create_code_encoding(Fristtyp)],
    # This variant never actually exists in our data, so there is currently no test case.
    Annotated[str, XmlVariant("nichtGelisteterWert")],
]


@dataclass(slots=True)
class FristMitTyp:
    typ: FristtypErweiterbar
    fristauswahl: FristAuswahl
    beschreibung: list[StringLocalized]
    position_darstellung: int | None
    gueltigkeit: list[Zeitraum]


@dataclass(slots=True)
class Fristmodul(Leistungsmodul):
    frist: list[FristMitTyp]
    beschreibung: list[StringLocalized]
    weiterfuehrender_link: list[HyperlinkErweitert]


class Kostentyp(Enum):
    ABGABE = "001"
    ANSCHLUSSGEBUEHR = "002"
    BENUTZUNGSGEBUEHR = "003"
    BEITRAG = "004"
    BUSSGELD = "005"
    ENTSCHAEDIGUNG_HONORAR = "006"
    ERSTATTUNG = "007"
    GEBUEHR = "008"
    HONORAR = "009"
    LOHN = "010"
    LOHN_GEHALT = "011"
    ORDNUNGSGELD = "012"
    SPENDE = "013"
    SPESEN = "014"
    STAATLICHE_LEISTUNG = "015"
    STEUER = "016"
    VERWALTUNGSGEBUEHR = "017"
    VERWARNGELD = "018"

    def to_label(self) -> str:
        match self:
            case Kostentyp.ABGABE:
                return "Abgabe"
            case Kostentyp.ANSCHLUSSGEBUEHR:
                return "Anschlussgebühr"
            case Kostentyp.BENUTZUNGSGEBUEHR:
                return "Benutzungsgebühr"
            case Kostentyp.BEITRAG:
                return "Beitrag"
            case Kostentyp.BUSSGELD:
                return "Bußgeld"
            case Kostentyp.ENTSCHAEDIGUNG_HONORAR:
                return "Entschädigung/Honorar"
            case Kostentyp.ERSTATTUNG:
                return "Erstattung"
            case Kostentyp.GEBUEHR:
                return "Gebühr"
            case Kostentyp.HONORAR:
                return "Honorar"
            case Kostentyp.LOHN:
                return "Lohn"
            case Kostentyp.LOHN_GEHALT:
                return "Lohn/Gehalt"
            case Kostentyp.ORDNUNGSGELD:
                return "Ordnungsgeld"
            case Kostentyp.SPENDE:
                return "Spende"
            case Kostentyp.SPESEN:
                return "Spesen"
            case Kostentyp.STAATLICHE_LEISTUNG:
                return "Staatliche Leistung"
            case Kostentyp.STEUER:
                return "Steuer"
            case Kostentyp.VERWALTUNGSGEBUEHR:
                return "Verwaltungsgebühr"
            case Kostentyp.VERWARNGELD:
                return "Verwarngeld"


# Called "Kostentyp_Erweiterbar" in the standard
KostentypErweiterbar = Union[
    Annotated[Kostentyp, XmlVariant("code"), create_code_encoding(Kostentyp)],
    Annotated[str, XmlVariant("nichtGelisteterWert")],
]


@dataclass(slots=True)
class Amount:
    currency_code: Annotated[str, XmlAttribute("currencyCode", namespace=None)]
    value: Annotated[float, XmlTextValue()]


@dataclass(slots=True)
class KostenFix:
    betrag: Amount


@dataclass(slots=True)
class KostenFrei:
    beschreibung_kostenfreiheit: list[StringLocalized]


@dataclass(slots=True)
class KostenVariabel:
    betrag_untergrenze: Amount | None
    betrag_obergrenze: Amount | None
    beschreibung_variabilitaet: list[StringLocalized]


KostenAuswahl = Union[
    Annotated[KostenFix, XmlVariant("kostenFix")],
    Annotated[KostenFrei, XmlVariant("kostenFrei")],
    Annotated[KostenVariabel, XmlVariant("kostenVariabel")],
]


@dataclass(slots=True)
class Kosten:
    typ: KostentypErweiterbar
    kostenauswahl: KostenAuswahl
    beschreibung: list[StringLocalized]
    vorkasse: bool | None
    link_kostenbildung: str | None
    position_darstellung: int | None
    gueltigkeit: list[Zeitraum]


@dataclass(slots=True)
class Kostenmodul(Leistungsmodul):
    kosten: list[Kosten]
    beschreibung: list[StringLocalized]
    weiterfuehrender_link: list[HyperlinkErweitert]


@dataclass(slots=True)
class FristOhneTyp:
    fristauswahl: FristAuswahl
    beschreibung: list[StringLocalized]
    position_darstellung: int | None
    gueltigkeit: list[Zeitraum]


@dataclass(slots=True)
class Bearbeitungsdauermodul(Leistungsmodul):
    bearbeitungsdauer: list[FristOhneTyp]
    beschreibung: list[StringLocalized]
    weiterfuehrender_link: list[HyperlinkErweitert]


@dataclass(slots=True)
class FachlicheFreigabeModul(Leistungsmodul):
    fachlich_freigegeben_am: (
        Annotated[date, RequiredValueEncoding(decode=parse_optional_date)] | None
    )
    fachlich_freigegeben_durch: list[StringLocalized]

    def get_fachlich_freigegeben_durch(self, language_code: LanguageCode) -> str | None:
        for content in self.fachlich_freigegeben_durch:
            if content.language_code == language_code:
                return content.value
        return None


@dataclass(slots=True)
class UrsprungsportalModul(Leistungsmodul):
    language_code: Annotated[str, XmlAttribute("languageCode", namespace=None)]
    uri: str
    titel: str | None


class BegriffImKontextTyp(Enum):
    SYNONYM = "001"
    SCHLAGWORT = "002"
    FEHLBESCHREIBUNG = "003"
    UNBESTIMMT = "999"

    def to_label(self) -> str:
        match self:
            case BegriffImKontextTyp.SYNONYM:
                return "Synonym"
            case BegriffImKontextTyp.SCHLAGWORT:
                return "Schlagwort"
            case BegriffImKontextTyp.FEHLBESCHREIBUNG:
                return "Fehlbeschreibung"
            case BegriffImKontextTyp.UNBESTIMMT:
                return "Unbestimmt"


@dataclass(slots=True)
class BegriffImKontext:
    begriff: StringLocalized
    typ: (
        Annotated[BegriffImKontextTyp, create_code_encoding(BegriffImKontextTyp)] | None
    )


@dataclass(slots=True)
class BegriffImKontextModul(Leistungsmodul):
    begriff_im_kontext: list[BegriffImKontext]


@dataclass(slots=True)
class Auskunftshinweismodul(Leistungsmodul):
    inhalt: list[StringLocalized]
    weiterfuehrender_link: list[HyperlinkErweitert]


class Signatur(Enum):
    KEINE_SIGNATUR = "001"
    HANDSCHRIFTLICHE_SIGNATUR = "004"
    EINFACHE_ELEKTRONISCHE_SIGNATUR = "101"
    FORTGESCHRITTENE_ELEKTRONISCHE_SIGNATUR = "102"
    QUALIFIZIERTE_ELEKTRONISCHE_SIGNATUR = "103"
    QUALIFIZIERTE_ELEKTRONISCHE_SIGNATUR_MIT_ANBIETERAKKREDITIERUNG = "104"


# Called "Wirtschaftszweig_Spezifizierbar" in the standard
@dataclass(slots=True)
class WirtschaftszweigSpezifizierbar:
    # Codelist: urn:de:destatis:wirtschaftszweigklassifikationen:klassifikation_der_wirtschaftszweige
    # cannot be found in xrepository
    code: Annotated[str, RawCodeEncoding]
    spezifische_id: Identifikator


class DokumentTyp(Enum):
    """
    urn:de:fim:codeliste:dokumenttyp
    """

    ABSCHRIFT = "001"
    ANTRAG = "002"
    ANZEIGE = "003"
    AUSWEIS = "004"
    BERICHT = "005"
    BESCHEID = "006"
    BESCHEINIGUNG = "007"
    ERKLAERUNG = "008"
    ERLAUBNIS = "009"
    GENEHMIGUNG = "010"
    GESTATTUNG_ZULASSUNG = "011"
    GUTACHTEN = "012"
    LICHTBILD = "013"
    MITTEILUNG = "014"
    NACHWEIS = "015"
    REGISTERAUSKUNFT = "016"
    REGISTERAUSZUG = "017"
    SATZUNG = "018"
    STELLUNGNAHME = "019"
    UNTERSCHRIFT = "020"
    URKUNDE = "021"
    VERTRAG = "022"
    ZEUGNIS = "023"
    KARTENMATERIAL = "024"
    MULTIMEDIA = "025"
    REGISTERANFRAGE = "026"
    VOLLMACHT = "027"
    WILLENSERKLAERUNG = "028"


# Called "DokumentTyp_Erweiterbar" in the standard
@dataclass(slots=True)
class DokumentTypErweiterbar:
    code: Annotated[DokumentTyp, create_code_encoding(DokumentTyp)] | None
    nicht_gelisteter_wert: str | None


@dataclass(slots=True)
class Dokument:
    typ: DokumentTypErweiterbar
    bezeichnung: list[StringLocalized]
    beschreibung: list[StringLocalized]
    referenz_formular_id: list[Identifikator]
    angabe_signatur: Annotated[Signatur, create_code_encoding(Signatur)] | None


@dataclass(slots=True)
class Dokumentmodul(Leistungsmodul):
    dokument_eingehend: list[Dokument]
    dokument_ausgehend: list[Dokument]
    beschreibung: list[StringLocalized]
    weiterfuehrender_link: list[HyperlinkErweitert]


@dataclass(slots=True)
class Klassifizierung:
    list_uri: Annotated[str, XmlAttribute("listURI", namespace=None)]
    value: Annotated[str, XmlTextValue()]

    @staticmethod
    def from_str(value: str) -> "Klassifizierung":
        list_uri, value = value.rsplit("::", maxsplit=1)

        return Klassifizierung(list_uri, value)

    def to_str(self):
        return f"{self.list_uri}::{self.value}"


def _decode_klassifizierung(node: xml.XmlElement) -> Klassifizierung:
    code = decode_raw_code(node)
    list_uri = node.attrib.get("listURI")
    if list_uri is None:
        raise xml.MissingAttribute("Missing listURI for Klassifizierung")
    return Klassifizierung(list_uri, code)


LEIKA_SCHEME_ID = "LEIKA_LEISTUNG_SCHLUESSEL"


def _parse_freigabe_status(value: str) -> FreigabeStatus:
    try:
        return FreigabeStatus(int(value))
    except ValueError as error:
        raise XzufiException(f"Invalid FreigabeStatus '{value}'") from error


@dataclass(slots=True)
class Leistung:
    """
    The class only very loosely checks the data of a Leistung for correctnes.
    This is only done to allow more QS-checks without a parser error for the PVOG data analysis.
    To actually work with Leistungen in the sammelrepo, `xzufi.Leistung` should be used instead.

    There are several codelists that are typed as string here. The reason for that can be either
    a very large code lists or issues with their defintion.

    - klassifizierung: Code list not defined through standard (see 'KlassifizierungBenutzerdefiniert') or xrepository
    - informationsbereich_sdg: Large code list - urn:xoev-de:fim:codeliste:sdginformationsbereich
    - relevant_fuer_rechtsform: Code list not defined through standard and ambivalent information in xrepository.
                                Standard says code lists of 'XJustiz' are aimed to be used.
    - relevant_fuer_staatsangehörigkeit: Large code list - urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:staatsangehoerigkeit
    """

    id: Identifikator
    id_sekundaer: list[Identifikator]
    struktur: Annotated[Leistungsstruktur, LeistungsstrukturEncoding] | None
    referenz_leika: Annotated[
        list[Annotated[str, RawCodeEncoding]], XmlValue("referenzLeiKa")
    ]

    modul_text: list[Textmodul]
    modul_text_individuell: list[TextmodulIndividuell]
    modul_frist: Fristmodul | None
    modul_kosten: Kostenmodul | None
    modul_bearbeitungsdauer: Bearbeitungsdauermodul | None
    modul_begriff_im_kontext: list[BegriffImKontextModul]
    modul_fachliche_freigabe: FachlicheFreigabeModul | None
    modul_auskunftshinweis: Auskunftshinweismodul | None
    modul_dokument: Dokumentmodul | None
    modul_ursprungsportal: list[UrsprungsportalModul]

    typisierung: list[
        Annotated[LeistungsTypisierung, create_code_encoding(LeistungsTypisierung)]
    ]
    vertrauensniveau: (
        Annotated[Vertrauensniveau, create_code_encoding(Vertrauensniveau)] | None
    )
    leistungsadressat: list[
        Annotated[LeistungsAdressat, create_code_encoding(LeistungsAdressat)]
    ]
    kennzeichen_schritformerfordernis: bool | None
    kategorie: list[Leistungskategorie]
    klassifizierung: list[
        Annotated[
            Klassifizierung, RequiredValueEncoding(decode=_decode_klassifizierung)
        ]
    ]
    informationsbereich_sdg: Annotated[
        list[Annotated[str, RawCodeEncoding]], XmlValue("informationsbereichSDG")
    ]
    id_leistung_im_kontext: list[Identifikator]
    id_prozess: list[Identifikator]
    herausgeber: Herausgeber | None
    gueltigkeit_gebiet_id: list[Identifikator]
    kennzeichen_ea: Annotated[bool | None, XmlValue("kennzeichenEA")]
    relevant_fuer_wirtschaftszweig: list[WirtschaftszweigSpezifizierbar]
    relevant_fuer_rechtsform: list[Annotated[str, RawCodeEncoding]]
    relevant_fuer_staatsangehoerigkeit: list[Annotated[str, RawCodeEncoding]]
    versionsinformation: Versionsinformation | None
    sprachversion: list[Sprachversion]
    gueltigkeit: list[Zeitraum]

    def __post_init__(self):
        # There must be at least one sprachversion
        if len(self.sprachversion) == 0:
            raise XzufiException(
                f"Missing <xzufi:sprachversion> in leistung {self.id.value} [redaktion_id={self.id.scheme_agency_id}]"
            )

    def get_pv_lagen(self) -> list[str]:
        return [i.value for i in self.klassifizierung if i.list_uri == PV_LAGEN_URI]

    def get_text_modul(self, typ: ModulTextTyp) -> Textmodul | None:
        for modul in self.modul_text:
            if modul.leika_textmodul == typ:
                return modul

        return None

    def get_leistungsschluessel(self) -> list[str]:
        return [
            item.value
            for item in self.id_sekundaer
            if item.scheme_id == LEIKA_SCHEME_ID
        ] + [referenz for referenz in self.referenz_leika]

    def get_redaktions_id(self):
        return self.id.scheme_agency_id

    def get_identifier(self):
        if self.id.scheme_agency_id is None:
            return None

        redaktion = RedaktionsId(self.id.scheme_agency_id)
        id = LeistungsId(self.id.value)

        return redaktion, id

    def get_german_language_code(self) -> LanguageCode | None:
        for sprachversion in self.sprachversion:
            if sprachversion.language_code in [GERMAN, ALT_GERMAN]:
                return sprachversion.language_code

        return None

    def get_freigabe_status_katalog(self) -> FreigabeStatus | None:
        for modul in self.modul_text_individuell:
            if modul.individuelles_textmodultyp.code == "STATUSKATALOG":
                if len(modul.inhalt) == 0:
                    raise XzufiException(
                        f"Textmodul STATUSKATALOG is empty [redaktion_id={self.get_redaktions_id()}, leistung_id={self.id.value}]"
                    )

                return _parse_freigabe_status(modul.inhalt[0].value)

        return None

    def get_freigabe_status_bibliothek(self) -> FreigabeStatus | None:
        for modul in self.modul_text_individuell:
            if modul.individuelles_textmodultyp.code == "STATUS":
                if len(modul.inhalt) == 0:
                    raise XzufiException(
                        f"Textmodul STATUS is empty [redaktion_id={self.get_redaktions_id()}, leistung_id={self.id.value}]"
                    )

                return _parse_freigabe_status(modul.inhalt[0].value)

        return None

    def unwrap_redaktions_id(self) -> RedaktionsId:
        assert self.id.scheme_agency_id is not None

        return RedaktionsId(self.id.scheme_agency_id)

    def get_links(self) -> list[str]:
        modul_text_links = [
            link.uri
            for modul in self.modul_text
            for link in modul.weiterfuehrender_link
            if modul.leika_textmodul != ModulTextTyp.URHEBER
        ]
        modul_text_individuell_links = [
            link.uri
            for modul in self.modul_text_individuell
            for link in modul.weiterfuehrender_link
        ]
        modul_frist_links = (
            [link.uri for link in self.modul_frist.weiterfuehrender_link]
            if self.modul_frist is not None
            else []
        )
        modul_kosten_links = (
            [link.uri for link in self.modul_kosten.weiterfuehrender_link]
            if self.modul_kosten is not None
            else []
        )
        modul_bearbeitungsdauer_links = (
            [link.uri for link in self.modul_bearbeitungsdauer.weiterfuehrender_link]
            if self.modul_bearbeitungsdauer is not None
            else []
        )
        modul_auskunftshinweis_links = (
            [link.uri for link in self.modul_auskunftshinweis.weiterfuehrender_link]
            if self.modul_auskunftshinweis is not None
            else []
        )
        modul_dokument_links = (
            [link.uri for link in self.modul_dokument.weiterfuehrender_link]
            if self.modul_dokument is not None
            else []
        )

        return [
            uri
            for uri in (
                modul_text_links
                + modul_text_individuell_links
                + modul_frist_links
                + modul_kosten_links
                + modul_bearbeitungsdauer_links
                + modul_auskunftshinweis_links
                + modul_dokument_links
            )
            if not uri.startswith("https://leika-schul.zfinder.de")
        ]


def can_have_stammtext(typisierung: list[LeistungsTypisierung]) -> bool:
    """
    Returns whether or not a Leistung with the provided typisierung can
    have a stammtext.
    """
    return len(typisierung) > 0 and set(typisierung).issubset(
        LEISTUNGSTYPISIERUNG_WITH_STAMMTEXT
    )


LeistungEncoding = xmlstruct.derive(
    Leistung, local_name="leistung", namespace=XZUFI_NAMESPACE
)


def parse_leistung(data: xml.XmlDataSource | xml.XmlElement) -> Leistung:
    try:
        return LeistungEncoding.parse(data)
    except (XmlStructError, InternalXzufiException) as error:
        raise XzufiException(f"Could not parse leistung: {str(error)}") from error


XZUFI_ANTWORT = f"{{{XZUFI_NAMESPACE}}}antwort"
XZUFI_ERGEBNIS = f"{{{XZUFI_NAMESPACE}}}ergebnis"
XZUFI_LEISTUNG = f"{{{XZUFI_NAMESPACE}}}leistung"


def _parse_ergebnis(node: xml.XmlElement) -> list[tuple[Leistung, str]]:
    leistungen: list[tuple[Leistung, str]] = []

    for child in node:
        if child.tag == XZUFI_LEISTUNG:
            xml_data = xml.serialize(child).decode("utf-8")

            try:
                leistung = parse_leistung(child)
            except XzufiException as error:
                logger.exception("Could not import leistung: %s", str(error))
            else:
                leistungen.append((leistung, xml_data))

    return leistungen


def _parse_antwort(node: xml.XmlElement) -> list[tuple[Leistung, str]]:
    leistungen: xml.ParsedValue[list[tuple[Leistung, str]]] = xml.ParsedValue()

    for child in node:
        if child.tag == XZUFI_ERGEBNIS:
            leistungen.set(_parse_ergebnis(child))

    return leistungen.expect(XZUFI_ANTWORT)


def parse_leistung_response(node: xml.XmlElement) -> list[tuple[Leistung, str]]:
    leistungen: xml.ParsedValue[list[tuple[Leistung, str]]] = xml.ParsedValue()

    for child in node:
        if child.tag == XZUFI_ANTWORT:
            leistungen.set(_parse_antwort(child))

    return leistungen.expect(XZUFI_ANTWORT)
