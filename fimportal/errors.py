class ServiceException(Exception):
    pass


class AuthManagerException(Exception):
    pass


class ConversionException(ServiceException):
    pass


class ImportException(ServiceException):
    """
    A generic error during the import of a resource.
    """

    @staticmethod
    def from_immutable_data_violation(
        resource_name: str,
        different_attributes: dict[str, tuple[str, str]],
    ) -> "ImportException":
        message = f"Cannot update {resource_name} because the following attributes are different:\n"

        # Sort ouput by key name to make this stable for tests
        lines = [
            f"  - {key}: {old} -> {new}"
            for key, (old, new) in (
                sorted(different_attributes.items(), key=lambda x: x[0])
            )
        ]

        message += "\n".join(lines)

        return ImportException(message)


class InternalServiceException(ServiceException):
    pass


class DatabaseDeadlockException(InternalServiceException):
    pass


class StatusHasNotChangedException(ServiceException):
    pass


class InvalidFimIdException(Exception): ...


class CannotImportLeistung(Exception): ...


class CannotImportOrganisationseinheit(Exception): ...


class CannotImportSpezialisierung(Exception): ...


class CannotImportZustaendigkeit(Exception): ...


class CannotImportOnlinedienst(Exception): ...
