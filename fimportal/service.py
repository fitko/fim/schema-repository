from __future__ import annotations

import base64
import hashlib
import json
import logging
import os
import random
import urllib.parse
from contextlib import asynccontextmanager
from dataclasses import dataclass
from datetime import date, datetime, timedelta, timezone
from typing import Any, Iterable

import orjson
from asyncpg import Connection, Pool
from asyncpg.exceptions import DeadlockDetectedError

from fimportal import genericode, json_schema, xrepository, xsd
from fimportal.genericode.code_list.parse import CodeList
from fimportal.helpers import utc_now
from fimportal.models.csv_export import CSVSearchResult
from fimportal.models.imports import (
    ElementIdentifier,
    NewCodeList,
    NewField,
    NewGroup,
    NewImport,
    NewRule,
    RuleIdentifier,
    Xdf2SchemaImport,
    Xdf2SteckbriefImport,
    Xdf3SchemaImport,
    XdfVersion,
)
from fimportal.models.kataloge import NewKatalog
from fimportal.models.prozess import (
    NewProzess,
    NewProzessklasse,
    ProzessklasseOut,
    ProzessOut,
)
from fimportal.models.token import ApiTokenModel
from fimportal.models.xzufi import (
    FullLeistungsbeschreibungOut,
    FullLeistungssteckbriefOut,
    LeistungsbeschreibungOut,
    LeistungssteckbriefOut,
    Link,
    NewLeistung,
    NewOnlinedienst,
    NewOrganisationseinheit,
    NewSpezialisierung,
    NewZustaendigkeit,
    PvogResourceClass,
    XzufiSource,
)
from fimportal.xdatenfelder import xdf2, xdf3
from fimportal.xdatenfelder.reports import (
    QualityReport,
    check_xdf2_quality,
    check_xdf3_quality,
)
from fimportal.xdatenfelder.xdf3.steckbrief_message import SteckbriefMessage
from fimportal.xprozesse.prozess import Prozess, Prozessklasse
from fimportal.xzufi import link_check
from fimportal.xzufi.common import (
    RedaktionsId,
    XzufiException,
    XzufiIdentifier,
    XzufiLeistungsIdentifier,
    remove_user_data,
)
from fimportal.xzufi.leistung import (
    Leistung,
    parse_leistung,
)
from fimportal.xzufi.onlinedienst import Onlinedienst, parse_onlinedienst
from fimportal.xzufi.organisationseinheit import (
    Organisationseinheit,
    parse_organisationseinheit,
)
from fimportal.xzufi.reports import (
    LinkStatus,
    check_leistung,
)
from fimportal.xzufi.reports import (
    QualityReport as LeistungQualityReport,
)
from fimportal.xzufi.spezialisierung import (
    Spezialisierung,
    parse_spezialisierung,
)
from fimportal.xzufi.zustaendigkeit import Zustaendigkeit, parse_zustaendigkeit_variante

from .database import (
    Database,
    FieldSearchOptions,
    GroupSearchOptions,
    LeistungsbeschreibungSearchOptions,
    LeistungssteckbriefSearchOptions,
    PaginatedResult,
    PaginationOptions,
    ProzessklasseSearchOptions,
    ProzessSearchOptions,
    SchemaSearchOptions,
    SteckbriefSearchOptions,
    XzufiContentInfo,
)
from .errors import (
    CannotImportLeistung,
    CannotImportOnlinedienst,
    CannotImportOrganisationseinheit,
    CannotImportZustaendigkeit,
    ConversionException,
    DatabaseDeadlockException,
    ImportException,
    StatusHasNotChangedException,
)
from .helpers import (
    collect_element_identifiers_from_json_keys,
    sort_versions,
)
from .models.xdf3 import (
    CodeListOut,
    DatenfeldgruppeOut,
    DatenfeldgruppeOutWithDirectChildren,
    DatenfeldOut,
    FullDatenfeldgruppeOut,
    FullDatenfeldOut,
    FullSchemaOut,
    FullSteckbriefOut,
    JsonLegend,
    JsonSchemaFileModel,
    NewSteckbrief,
    RuleOut,
    SchemaOut,
    SteckbriefOut,
    TreeRelationModel,
    XsdFileModel,
    map_identifier_to_element,
)

logger = logging.getLogger(__name__)


CANONICAL_JSON_SCHEMA_ID = "canonical-json-schema-id"


@dataclass(slots=True)
class FileExport:
    content: str
    filename: str


@dataclass(slots=True)
class Access:
    """
    The access rights of an API token.
    This must not contain sensitive information, as this is returned to the client
    in `/api/v1/token/access`.
    """

    nummernkreis: str


def _generate_api_token() -> str:
    return base64.b64encode(random.randbytes(64)).decode("utf-8")


def hash(value: str) -> str:
    return hashlib.scrypt(
        password=value.encode("utf-8"), salt="salt".encode("utf-8"), n=16384, r=8, p=1
    ).hex()


class Service:
    """
    The "business level" logic of the project.

    The provided `Database` instance must contain a database connection that is in an open transaction.
    This is a simplification in order to not handle transaction logic in every public function of the service,
    and still ensure that every action via the service is contained in a transaction and is automatically rolled back
    in case of an error.

    IMPORTANT: Making multiple calls to the same `Service` instance, or multiple `Service` instances sharing a single `Database` instance,
    executes all calls within a single transaction. Long-running batch operations should therefore be modelled in a way to work "piece-by-piece" over multiple
    transactions instead of calling the same `Service` instance in a potentially long loop. Otherwise, there is a risk of locking and therefore blocking a large part of the database
    and making the web-server effectively pause while the task is running.
    """

    def __init__(
        self,
        immutable_base_url: str,
        database: Database,
        xrepository_service: xrepository.Service,
        link_checker: link_check.Service,
    ):
        self._immutable_base_url = immutable_base_url
        self._database = database
        self._xrepository_service = xrepository_service
        self._link_checker = link_checker

    @staticmethod
    @asynccontextmanager
    async def from_pool(
        immutable_base_url: str,
        pool: Pool,
        xrepository_service: xrepository.Service,
        link_checker: link_check.Service,
    ):
        """
        Directly create a service instance with an active session from the engine.
        Useful during tests and commands.
        """

        async with pool.acquire() as connection:
            async with connection.transaction():
                yield Service.from_connection(
                    immutable_base_url,
                    connection,
                    xrepository_service,
                    link_checker,
                )

    @staticmethod
    def from_connection(
        immutable_base_url: str,
        connection: Connection,
        xrepository_service: xrepository.Service,
        link_checker: link_check.Service,
    ) -> Service:
        database = Database(connection)
        return Service(
            immutable_base_url,
            database,
            xrepository_service,
            link_checker,
        )

    def _get_json_schema_path(self, filename: str) -> str:
        filepath = os.path.join("/immutable/schemas/", filename)

        return urllib.parse.urljoin(self._immutable_base_url, filepath)

    async def create_admin_session(self) -> str:
        token = base64.b64encode(random.randbytes(64)).decode("utf-8")
        now = utc_now()

        await self._database.save_admin_session(token, now)

        return token

    async def is_valid_admin_session(self, token: str) -> bool:
        creation_time = await self._database.load_admin_session_creation_time(token)
        if creation_time is None:
            return False

        creation_time = creation_time.astimezone(timezone.utc)
        now = utc_now()

        # A session is marked as invalid after 30 days
        return now - creation_time < timedelta(days=30)

    async def remove_admin_session(self, token: str):
        await self._database.remove_admin_session(token)

    async def create_api_token(
        self, nummernkreis: str, description: str, token: str | None = None
    ) -> str:
        token = token or _generate_api_token()

        await self._database.save_api_token(
            nummernkreis=nummernkreis,
            token=hash(token),
            description=description,
        )

        logger.info(
            f"Created API token for Nummernkreis {nummernkreis} [description={description}]"
        )

        return token

    async def get_api_token(self, token_id: int) -> ApiTokenModel | None:
        return await self._database.load_api_token_by_id(token_id)

    async def get_all_api_tokens(self) -> list[ApiTokenModel]:
        return await self._database.load_all_api_tokens()

    async def check_api_token(self, token: str) -> Access | None:
        api_token = await self._database.load_api_token(hash(token))

        if api_token is None:
            return None

        return Access(nummernkreis=api_token.nummernkreis)

    async def delete_api_token(self, token_id: int):
        await self._database.delete_api_token(token_id)

    async def get_schema(self, fim_id: str, fim_version: str) -> FullSchemaOut | None:
        if fim_version == "latest":
            schema = await self._database.load_latest_schema(
                fim_id, immutable_base_url=self._immutable_base_url
            )
        else:
            schema = await self._database.load_schema(
                fim_id, fim_version, immutable_base_url=self._immutable_base_url
            )

        if schema is None:
            return None

        # Load the full tree including all child elements
        schema_tree = await self._database.load_schema_tree(fim_id, schema.fim_version)
        groups = await self._database.load_groups(schema_tree.group_identifiers)
        fields = await self._database.load_fields(schema_tree.field_identifiers)

        return FullSchemaOut.from_model(
            schema,
            groups=DatenfeldgruppeOutWithDirectChildren.map(
                groups, schema_tree.group_to_children
            ),
            fields=fields,
            children=schema_tree.schema_children,
        )

    async def get_schema_versions(self, fim_id: str) -> list[SchemaOut]:
        models = await self._database.load_all_schema_versions(fim_id)

        version_to_schema = {model.fim_version: model for model in models}
        sorted_versions = sort_versions(list(version_to_schema.keys()))

        return [version_to_schema[version] for version in sorted_versions]

    async def get_schema_quality_report(
        self, fim_id: str, fim_version: str
    ) -> QualityReport | None:
        if fim_version == "latest":
            latest_fim_version = await self._database.load_latest_schema_version(fim_id)
            if latest_fim_version is None:
                return None

            fim_version = latest_fim_version

        return await self._database.load_schema_quality_report(fim_id, fim_version)

    async def export_json_schema_file(self, filename: str) -> str | None:
        return await self._database.load_json_schema_content(filename)

    async def export_xsd_file(self, filename: str) -> str | None:
        return await self._database.load_xsd_content(filename)

    async def export_xdf_schema_message(
        self, fim_id: str, fim_version: str
    ) -> xdf2.SchemaMessage | xdf3.SchemaMessage | None:
        if fim_version == "latest":
            latest_fim_version = await self._database.load_latest_schema_version(fim_id)
            if latest_fim_version is None:
                return None

            fim_version = latest_fim_version

        message_export = await self._database.load_xdf_schema_message_data(
            fim_id, fim_version
        )
        if message_export is None:
            return None

        return message_export.to_message()

    async def export_xdf_datenfeldgruppe_message(
        self, namespace: str, fim_id: str, fim_version: str
    ) -> xdf2.DatenfeldgruppeMessage | xdf3.DatenfeldgruppeMessage | None:
        if fim_version == "latest":
            latest_fim_version = await self._database.load_latest_group_version(
                namespace, fim_id
            )
            if latest_fim_version is None:
                return None

            fim_version = latest_fim_version

        message_export = await self._database.load_xdf_group_message_data(
            namespace, fim_id, fim_version
        )
        if message_export is None:
            return None

        return message_export.to_message()

    async def export_xdf_datenfeld_message(
        self, namespace: str, fim_id: str, fim_version: str
    ) -> xdf2.DatenfeldMessage | xdf3.DatenfeldMessage | None:
        if fim_version == "latest":
            latest_fim_version = await self._database.load_latest_field_version(
                namespace, fim_id
            )
            if latest_fim_version is None:
                return None

            fim_version = latest_fim_version

        message_export = await self._database.load_xdf_field_message_data(
            namespace, fim_id, fim_version
        )
        if message_export is None:
            return None

        return message_export.to_message()

    async def import_xdf2_steckbrief(
        self, steckbrief_import: Xdf2SteckbriefImport, check_status: bool
    ):
        model = NewSteckbrief.from_xdf2(
            steckbrief=steckbrief_import.steckbrief,
            freigabe_status=steckbrief_import.freigabe_status,
            letzte_aenderung=steckbrief_import.letzte_aenderung,
            last_update=utc_now(),
        )

        await self._import_steckbrief(
            model, steckbrief_import.xml_content, check_status=check_status
        )

    async def import_xdf3_steckbrief(
        self,
        steckbrief_message: SteckbriefMessage,
        xml_content: str,
        check_status: bool,
    ):
        model = NewSteckbrief.from_xdf3(
            steckbrief=steckbrief_message.steckbrief,
            letzte_aenderung=steckbrief_message.header.erstellungs_zeitpunkt,
            last_update=utc_now(),
        )
        await self._import_steckbrief(model, xml_content, check_status=check_status)

    async def _import_steckbrief(
        self, steckbrief: NewSteckbrief, content: str, check_status: bool
    ):
        old_steckbrief = await self._database.load_steckbrief_for_import_checks(
            steckbrief.fim_id, steckbrief.fim_version
        )
        if old_steckbrief is not None:
            old_immutable_json, old_veroeffentlichungsdatum, old_freigabe_status = (
                old_steckbrief
            )

            if check_status:
                if old_freigabe_status == steckbrief.freigabe_status:
                    raise StatusHasNotChangedException(
                        f"The status of the document profile {steckbrief.fim_id}V{steckbrief.fim_version} has not changed"
                    )

            # Check immutability constraint
            differing_attributes = _get_immutable_data_diff(
                json.loads(old_immutable_json),
                steckbrief.immutable_json,
                old_veroeffentlichungsdatum,
                steckbrief.veroeffentlichungsdatum,
            )

            if len(differing_attributes) > 0:
                raise ImportException.from_immutable_data_violation(
                    f"document-profile {steckbrief.fim_id} version {steckbrief.fim_version}",
                    differing_attributes,
                )

        await self._database.save_steckbrief(steckbrief, content)

        logger.info(
            "Steckbrief imported [id=%s, version=%s]",
            steckbrief.fim_id,
            steckbrief.fim_version,
        )

    async def search_steckbriefe(
        self, options: SteckbriefSearchOptions
    ) -> PaginatedResult[SteckbriefOut]:
        return await self._database.search_dokumentsteckbriefe(options)

    async def get_steckbrief(
        self, steckbrief_id: str, steckbrief_version: str
    ) -> FullSteckbriefOut | None:
        if steckbrief_version == "latest":
            return await self._database.load_latest_steckbrief(steckbrief_id)
        else:
            return await self._database.load_steckbrief(
                steckbrief_id, steckbrief_version
            )

    async def get_steckbrief_versions(self, fim_id: str) -> list[SteckbriefOut]:
        models = await self._database.load_steckbrief_versions(fim_id)
        version_to_steckbrief = {model.fim_version: model for model in models}

        sorted_versions = sort_versions(list(version_to_steckbrief.keys()))

        return [version_to_steckbrief[version] for version in sorted_versions]

    async def get_steckbrief_xml_content(
        self, fim_id: str, fim_version: str
    ) -> tuple[str, str] | None:
        """
        Return both the content and the full filename including the resolved version.
        """
        if fim_version == "latest":
            steckbrief = await self._database.load_latest_steckbrief(fim_id)
            if steckbrief is None:
                return None

            fim_version = steckbrief.fim_version

        result = await self._database.load_steckbrief_xml_content(fim_id, fim_version)
        if result is None:
            return None

        xdf_version, content = result

        extension = ""
        match xdf_version:
            case XdfVersion.XDF2:
                extension = "xdf2"
            case XdfVersion.XDF3:
                extension = "xdf3"
        filename = f"{fim_id}V{fim_version}.{extension}.xml"

        return filename, content

    async def get_rule(self, rule_id: str, rule_version: str) -> RuleOut | None:
        return await self._database.load_rule(rule_id, rule_version)

    async def get_rule_xml_content(self, fim_id: str, fim_version: str) -> str | None:
        return await self._database.load_rule_xml_content(fim_id, fim_version)

    async def update_generated_data(self, schema_id: str, schema_version: str):
        logger.info("Update generated data for schema %s %s", schema_id, schema_version)

        schema_import = await self._database.load_latest_schema_import(
            schema_id, schema_version
        )
        if schema_import is None:
            return

        code_lists = await self._load_code_list_map_for_schema(schema_import.filename)

        if schema_import.version == XdfVersion.XDF2:
            message = xdf2.parse_schema_message(schema_import.xml_content)

            await self._update_xdf2_conversions(
                message, code_lists, schema_import.filename
            )

            quality_report = check_xdf2_quality(message, code_lists)
            await self._database.update_quality_report(
                schema_id,
                schema_version,
                quality_report,
                now=utc_now(),
            )
        else:
            message = xdf3.parse_schema_message(schema_import.xml_content)

            await self._update_xdf3_conversions(
                message, schema_import.filename, code_lists
            )

            quality_report = check_xdf3_quality(message, code_lists)
            await self._database.update_quality_report(
                schema_id,
                schema_version,
                quality_report,
                now=utc_now(),
            )

    async def import_xdf3_schema(
        self,
        xdf3_import: Xdf3SchemaImport,
        strict: bool = True,
        check_status: bool = False,
    ):
        if xdf3_import.freigabe_status not in xdf3.FREIGABE_STATUS_VALID_FOR_UPLOAD:
            raise ImportException(
                f"Freigabestatus not allowed for import [got={xdf3_import.freigabe_status}]"
            )

        external_code_lists = await self._load_external_code_lists_for_import(
            xdf3_import.schema_message.get_code_list_identifiers()
        )

        new_xdf_import = NewImport.from_xdf3_import(
            xdf3_import, external_code_lists=external_code_lists
        )

        upload_filename = await self._import_schema(
            new_xdf_import,
            strict=strict,
            check_status=check_status,
        )

        await self._update_xdf3_conversions(
            xdf3_import.schema_message,
            upload_filename,
            code_lists=new_xdf_import.get_all_loaded_code_lists(),
        )

    async def _update_xdf3_conversions(
        self,
        message: xdf3.SchemaMessage,
        filename: str,
        code_lists: dict[str, genericode.CodeList],
    ):
        try:
            json_schema_content = json_schema.from_xdf3(
                CANONICAL_JSON_SCHEMA_ID, message, code_lists
            )

        except json_schema.JsonSchemaException as error:
            logger.warning(
                "Cannot convert schema to JSON Schema [id=%s, version=%s]: %s",
                message.id,
                message.version,
                str(error),
            )
        else:
            await self._update_json_schema(
                schema_fim_id=message.id,
                schema_fim_version=message.schema.identifier.assert_version(),
                content=json_schema_content,
                source_filename=filename,
            )

    async def import_xdf2_schema(
        self,
        xdf2_import: Xdf2SchemaImport,
        strict: bool = True,
        check_status: bool = False,
    ):
        if xdf2_import.freigabe_status not in xdf3.FREIGABE_STATUS_VALID_FOR_UPLOAD:
            raise ImportException(
                f"Freigabestatus not allowed for import [got={xdf2_import.freigabe_status}]"
            )

        external_code_lists = await self._load_external_code_lists_for_import(
            xdf2_import.get_external_code_lists()
        )

        new_xdf_import = NewImport.from_xdf2_import(
            xdf2_import, external_code_lists=external_code_lists
        )

        upload_filename = await self._import_schema(
            new_xdf_import,
            strict=strict,
            check_status=check_status,
        )

        await self._update_xdf2_conversions(
            xdf2_import.schema_message,
            code_lists=new_xdf_import.get_all_loaded_code_lists(),
            filename=upload_filename,
        )

    async def _update_xdf2_conversions(
        self,
        message: xdf2.SchemaMessage,
        code_lists: dict[str, genericode.CodeList],
        filename: str,
    ):
        try:
            json_schema_content = json_schema.from_xdf2(
                CANONICAL_JSON_SCHEMA_ID, message, code_lists
            )
        except (ConversionException, json_schema.JsonSchemaException) as error:
            logger.warning(
                "Cannot convert schema to JSON Schema [id=%s, version=%s]: %s",
                message.id,
                message.version,
                str(error),
            )
        else:
            await self._update_json_schema(
                schema_fim_id=message.id,
                schema_fim_version=message.assert_version(),
                content=json_schema_content,
                source_filename=filename,
            )

        try:
            xsd_content = xsd.from_xdf2(message, code_lists)
        except xsd.XsdException as error:
            logger.warning(
                "Cannot convert schema to JSON Schema [id=%s, version=%s]: %s",
                message.id,
                message.version,
                str(error),
            )
        else:
            await self._update_xsd(
                schema_fim_id=message.id,
                schema_fim_version=message.assert_version(),
                content=xsd_content,
                source_filename=filename,
            )

    async def _load_external_code_lists_for_import(
        self,
        external_identifiers: Iterable[genericode.Identifier],
    ) -> dict[str, NewCodeList]:
        """Load code lists from database and load only missing code lists from XRepository"""

        uri_to_identifier = {
            identifier.canonical_version_uri: identifier
            for identifier in external_identifiers
        }

        uri_to_source_and_content = await self._database.load_external_code_lists(
            uri_to_identifier.keys()
        )

        code_lists: dict[str, NewCodeList] = {}
        unknown_code_lists: set[str] = set()
        for uri in uri_to_identifier.keys():
            entry = uri_to_source_and_content.get(uri)
            if entry is None:
                unknown_code_lists.add(uri)
            else:
                source, content = entry
                code_list = genericode.parse_code_list_with_content(
                    content.encode("utf-8")
                )
                code_lists[uri] = NewCodeList.from_code_list_with_content(
                    code_list, source=source
                )

        imported_code_lists = await self._xrepository_service.load_available_code_lists(
            unknown_code_lists
        )

        for uri, code_list in imported_code_lists.items():
            if code_list is None:
                identifier = uri_to_identifier[uri]
                code_lists[uri] = NewCodeList.external(identifier)
            else:
                code_lists[uri] = NewCodeList.from_code_list_with_content(
                    code_list, "xrepository"
                )

        return code_lists

    async def _import_schema(
        self,
        schema_import: NewImport,
        strict: bool,
        check_status: bool,
    ) -> str:
        try:
            return await self._execute_schema_import(
                schema_import,
                strict=strict,
                check_status=check_status,
            )
        except DeadlockDetectedError as error:
            raise DatabaseDeadlockException(
                "Another transaction is currently using this resource. Please try again."
            ) from error

    async def _execute_schema_import(
        self,
        schema_import: NewImport,
        strict: bool,
        check_status: bool,
    ) -> str:
        """
        Import a schema into the database.

        This creates and updates the schema and all elements (fields, groups) and rules that the caller
        has write-access to (see `strict` options below).
        Additionaly, the complete tree structure of the original schema is saved into the database.
        The nodes in the tree are reused if elements with the same identifier are already present.
        The relations of schemas/fields/groups to rules are saved into the database.

        Immutability of the whole schema tree is enforced. If an immutable attribute of the schema
        or of the groups, fields and rules belonging to its structure is different in the import
        than in the database, then the import fails.

        If `strict == True`, then the import only has write-acess to the elements within the same Nummernkreis as the schema itself.
        All elements from another Nummernkreis included in the schema, that already exist in our database, will not be updated.
        If an element from another Nummernkreis is not currently present in our database, then the import fails.

        If `strict == False`, then elements from another Nummernkreis are never updated, but created if necessary.

        One exception to the access-rules are elements from the Nummernkreis `99`, as these are considered schema-local.

        If `check_status` is `True` then raise `StatusHasNotChangedException` if the schema already exists and has the same
        status as the new import.

        ## Code lists

        In xdf3, all referenced code lists always point to an externally defined resource, e.g. from the XRepository.
        Internal value lists are modelled directly within the Datenfeld.
        An external code list is uniquely identified by its URI and only imported once.
        The identical imported code list is then referenced by all fields that use it.

        In xdf2, those internal value lists where also modelled via code lists.
        These are however internal to the source Redaktion and therefore not uniquely identified by their URI.
        To avoid conflicts, we import a fresh code list for each newly created xdf2 field that references an internal code list.
        This has the upside, that the import logic is very simple: Just create a new code list, regardless of whether it already exists
        from the same Redaktionssystem.
        The downside is slightly more storage space used, as we probably duplicate a few code lists this way, but this should be negligible.
        """

        schema = schema_import.schema
        fields = schema_import.fields
        groups = schema_import.groups
        rules = schema_import.rules

        field_dict = {field.identifier: field for field in fields}
        group_dict = {group.identifier: group for group in groups}
        rule_dict = {rule.to_identifier(): rule for rule in rules}

        # NOTE: Load the existing immutable data with FOR UPDATE to
        # signal all competing uploads to wait until the current
        # transaction has finished.
        old_entities = await self._database.load_entities_for_schema_import_checks(
            schema.fim_id,
            schema.fim_version,
            field_dict.keys(),
            group_dict.keys(),
            rule_dict.keys(),
        )

        old_freigabestatus = None
        for entity in old_entities:
            if entity.type == "schema":
                old_freigabestatus = entity.freigabe_status
                assert old_freigabestatus is not None
                break

        # TODO: remove once we no longer save the import XML content as an immutable upload.
        # Currently, we ignore all changes to the mutable content of groups/fields/rules within the schema.
        if check_status:
            if old_freigabestatus == schema.freigabe_status:
                raise StatusHasNotChangedException(
                    f"The status of the schema {schema.fim_id}V{schema.fim_version} has not changed: {schema.freigabe_status}"
                )

        must_create_schema = True
        code_list_ids: set[int] = set()
        existing_groups: set[ElementIdentifier] = set()
        existing_fields: set[ElementIdentifier] = set()
        existing_rules: set[RuleIdentifier] = set()
        for entity in old_entities:
            match entity.type:
                case "schema":
                    new_immutable_json = schema.immutable_json
                    new_veroeffentlichungsdatum = schema.veroeffentlichungsdatum
                    must_create_schema = False
                case "field":
                    assert entity.namespace is not None
                    identifier = ElementIdentifier(
                        entity.namespace, entity.fim_id, entity.fim_version
                    )
                    existing_fields.add(identifier)
                    if entity.code_list_id is not None:
                        code_list_ids.add(entity.code_list_id)

                    field = field_dict[identifier]
                    new_immutable_json = field.immutable_json
                    new_veroeffentlichungsdatum = field.veroeffentlichungsdatum
                case "group":
                    assert entity.namespace is not None
                    identifier = ElementIdentifier(
                        entity.namespace, entity.fim_id, entity.fim_version
                    )
                    existing_groups.add(identifier)

                    group = group_dict[identifier]
                    new_immutable_json = group.immutable_json
                    new_veroeffentlichungsdatum = group.veroeffentlichungsdatum
                case "rule":
                    identifier = RuleIdentifier(entity.fim_id, entity.fim_version)
                    existing_rules.add(identifier)

                    rule = rule_dict[identifier]
                    new_immutable_json = rule.immutable_json
                    new_veroeffentlichungsdatum = rule.veroeffentlichungsdatum

            differing_attributes = _get_immutable_data_diff(
                json.loads(entity.immutable_json),
                new_immutable_json,
                entity.veroeffentlichungsdatum,
                new_veroeffentlichungsdatum,
            )

            if len(differing_attributes) > 0:
                raise ImportException.from_immutable_data_violation(
                    f"{entity.type} {entity.fim_id} version {entity.fim_version}",
                    differing_attributes,
                )

        # Check access to fields
        # TODO: Use set to not add a code list multiple times
        update_fields: list[NewField] = []
        create_fields: list[NewField] = []
        for field in fields:
            is_internal = (
                field.identifier.is_local() or field.nummernkreis == schema.nummernkreis
            )

            if field.identifier in existing_fields:
                if is_internal:
                    update_fields.append(field)
            else:
                if strict and not is_internal:
                    raise ImportException(
                        f"Cannot create field {field.identifier.fim_id}: The field is from another Nummernkreis and not yet published to the Sammelrepository by the original author. Creating fields, groups or rules from another Nummernkreis is not allowed."
                    )

                create_fields.append(field)

        # Check access to groups
        update_groups: list[NewGroup] = []
        create_groups: list[NewGroup] = []
        for group in groups:
            is_internal = (
                group.identifier.is_local() or group.nummernkreis == schema.nummernkreis
            )

            if group.identifier in existing_groups:
                if is_internal:
                    update_groups.append(group)
            else:
                if strict and not is_internal:
                    raise ImportException(
                        f"Cannot create group {group.identifier.fim_id}: The group is from another Nummernkreis and not yet published to the Sammelrepository by the original author. Creating fields, groups or rules from another Nummernkreis is not allowed."
                    )

                create_groups.append(group)

        # Check access to rules
        update_rules: list[NewRule] = []
        create_rules: list[NewRule] = []
        for rule in rules:
            is_internal = rule.nummernkreis == schema.nummernkreis

            if rule.to_identifier() in existing_rules:
                if is_internal:
                    update_rules.append(rule)
            else:
                if strict and not is_internal:
                    raise ImportException(
                        f"Cannot create rule {rule.fim_id}: The rule is from another Nummernkreis and not yet published to the Sammelrepository by the original author. Creating fields, groups or rules from another Nummernkreis is not allowed."
                    )

                create_rules.append(rule)

        await self._database.update_groups(update_groups)
        await self._database.update_fields(update_fields)
        await self._database.update_rules(update_rules)

        if not must_create_schema:
            # Simple sanity check: If the schema already exists, then no new elements should be created.
            # This is implied by the fact, that the complete structure of the schema is immutable, and therefore
            # no new elements can be introduced.
            assert len(create_groups) == 0
            assert len(create_fields) == 0
            assert len(create_rules) == 0

            await self._database.update_schema(schema)
        else:
            # IMPORTANT: The rules must be created first, as all other elements can contain references to them.
            await self._database.create_rules(create_rules)
            await self._database.create_groups(create_groups)
            created_code_list_ids = await self._database.create_fields(create_fields)
            await self._database.create_schema(
                schema, fts_content=schema_import.fts_content
            )

            # Update the code list IDs for the created fields
            # TODO: Remove, once the schema file must no longer be uploaded.
            code_list_ids.update(created_code_list_ids)

            # Load or create all necessary nodes
            (
                schema_node_id,
                identifier_to_node_id,
            ) = await self._database.load_or_create_nodes(
                schema_fim_id=schema.fim_id,
                schema_fim_version=schema.fim_version,
                field_identifiers=[field.identifier for field in fields],
                group_identifiers=[group.identifier for group in groups],
            )

            # Save tree structure
            connections: list[TreeRelationModel] = []
            if must_create_schema:
                for index, reference in enumerate(schema.children):
                    connections.append(
                        TreeRelationModel(
                            parent_id=schema_node_id,
                            child_index=index,
                            child_id=identifier_to_node_id[reference.to_identifier()],
                            anzahl=reference.anzahl,
                            bezug=reference.bezug,
                        )
                    )
            for group in create_groups:
                parent_node_id = identifier_to_node_id[group.identifier]
                for index, reference in enumerate(group.children):
                    connections.append(
                        TreeRelationModel(
                            parent_id=parent_node_id,
                            child_index=index,
                            child_id=identifier_to_node_id[reference.to_identifier()],
                            anzahl=reference.anzahl,
                            bezug=reference.bezug,
                        )
                    )

            await self._database.save_tree_structure(connections)

        # TODO: Remove once the immutable xdf routes are no longer needed
        # Upload the file content
        upload_filename = await self._database.upload_schema_file(
            schema, schema_import.xdf_content, code_list_ids=code_list_ids
        )

        logger.info(
            "Schema imported [id=%s, version=%s]",
            schema.fim_id,
            schema.fim_version,
        )

        return upload_filename

    async def export_schema_file(self, filename: str) -> str | None:
        return await self._database.load_schema_file(filename)

    async def _load_code_list_map_for_schema(
        self, schema_filename: str
    ) -> dict[str, genericode.CodeList]:
        code_list_files = (
            await self._database.load_code_list_contents_for_schema_import(
                schema_filename
            )
        )

        code_lists: dict[str, genericode.CodeList] = {}
        for file in code_list_files:
            # NOTE(Felix): This should never fail, as the code lists are checked during import
            # to be valid. If this fails, just let the error bubble up, as the server will
            # then correctly log the exception and return a 500 error.
            code_list = genericode.parse_code_list(file)

            code_lists[code_list.identifier.canonical_version_uri] = code_list

        return code_lists

    async def export_code_list(self, code_list_id: int) -> str | None:
        return await self._database.load_code_list_content(code_list_id)

    async def get_all_schema_identifiers(self) -> list[tuple[str, str]]:
        return await self._database.get_all_schema_identifiers()

    async def search_schemas(
        self, options: SchemaSearchOptions
    ) -> PaginatedResult[SchemaOut]:
        return await self._database.search_schemas(options)

    async def get_code_lists(
        self, pagination_options: PaginationOptions
    ) -> PaginatedResult[CodeListOut]:
        return await self._database.load_code_lists(
            pagination_options, immutable_base_url=self._immutable_base_url
        )

    async def search_fields(
        self,
        options: FieldSearchOptions,
    ) -> PaginatedResult[DatenfeldOut]:
        return await self._database.search_fields(options)

    async def get_field_versions(
        self, namespace: str, fim_id: str
    ) -> list[DatenfeldOut]:
        """
        Return a list of available field versions, sorted in ascending version order.
        """
        models = await self._database.load_all_field_versions(namespace, fim_id)

        version_to_field = {model.fim_version: model for model in models}
        sorted_versions = sort_versions(list(version_to_field.keys()))

        return [version_to_field[version] for version in sorted_versions]

    async def get_field(
        self, namespace: str, fim_id: str, fim_version: str
    ) -> FullDatenfeldOut | None:
        if fim_version == "latest":
            field = await self._database.load_latest_field(
                namespace, fim_id, immutable_base_url=self._immutable_base_url
            )
        else:
            field = await self._database.load_field(
                namespace=namespace,
                fim_id=fim_id,
                fim_version=fim_version,
                immutable_base_url=self._immutable_base_url,
            )

        if field is None:
            return None

        schemas = await self._database.load_schemas_for_element(
            namespace=namespace, fim_id=fim_id, fim_version=field.fim_version
        )

        return FullDatenfeldOut.create(field, schemas)

    async def search_groups(
        self,
        options: GroupSearchOptions,
    ) -> PaginatedResult[DatenfeldgruppeOut]:
        return await self._database.search_groups(options)

    async def get_group_versions(
        self, namespace: str, fim_id: str
    ) -> list[DatenfeldgruppeOut]:
        """
        Return a list of available group versions, sorted in ascending version order.
        """
        models = await self._database.load_all_group_versions(namespace, fim_id)

        version_to_gruppe = {model.fim_version: model for model in models}
        sorted_versions = sort_versions(list(version_to_gruppe.keys()))

        return [version_to_gruppe[version] for version in sorted_versions]

    async def get_group(
        self, namespace: str, fim_id: str, fim_version: str
    ) -> FullDatenfeldgruppeOut | None:
        if fim_version == "latest":
            group = await self._database.load_latest_group(namespace, fim_id)
        else:
            group = await self._database.load_group(
                namespace=namespace, fim_id=fim_id, fim_version=fim_version
            )

        if group is None:
            return None

        # Load the full tree including all child elements
        group_tree = await self._database.load_group_tree(
            namespace, fim_id, group.fim_version
        )
        groups = await self._database.load_groups(group_tree.group_identifiers)
        fields = await self._database.load_fields(group_tree.field_identifiers)

        schemas = await self._database.load_schemas_for_element(
            namespace=namespace, fim_id=fim_id, fim_version=group.fim_version
        )

        return FullDatenfeldgruppeOut.create(
            group,
            schemas,
            fields,
            groups=DatenfeldgruppeOutWithDirectChildren.map(
                groups, group_tree.group_to_children
            ),
            children=group_tree.group_to_children[(namespace, fim_id, fim_version)],
        )

    async def import_leistung_batch(
        self,
        leistungen: list[tuple[Leistung, str]],
        source: XzufiSource,
    ) -> list[XzufiLeistungsIdentifier]:
        new_leistungen: list[NewLeistung] = []
        for leistung, xml_content in leistungen:
            try:
                new_leistung = NewLeistung.from_leistung(
                    leistung,
                    remove_user_data(xml_content),
                    source=source,
                )
            except CannotImportLeistung as error:
                logger.warning(
                    "Skip importing Leistung: %s [id=%s]", str(error), leistung.id
                )
            else:
                new_leistungen.append(new_leistung)

        await self._database.save_xzufi_leistung_batch(new_leistungen)

        for leistung in new_leistungen:
            logger.info(
                "Leistung imported [redaktion_id=%s, id=%s, titel=%s]",
                leistung.redaktion_id,
                leistung.id,
                leistung.title,
            )

        return [(leistung.redaktion_id, leistung.id) for leistung in new_leistungen]

    async def search_leistungssteckbriefe(
        self,
        options: LeistungssteckbriefSearchOptions,
    ) -> PaginatedResult[LeistungssteckbriefOut]:
        return await self._database.search_leistungssteckbriefe(options)

    async def search_leistungsbeschreibungen(
        self,
        options: LeistungsbeschreibungSearchOptions,
    ) -> PaginatedResult[LeistungsbeschreibungOut]:
        return await self._database.search_leistungsbeschreibungen(options)

    async def get_leistung_identifiers_for_redaktion(
        self, redaktion_id: RedaktionsId
    ) -> list[XzufiLeistungsIdentifier]:
        return await self._database.load_leistung_identifiers_for_redaktion(
            redaktion_id
        )

    async def get_leistungssteckbrief(
        self, leistungsschluessel: str
    ) -> FullLeistungssteckbriefOut | None:
        return await self._database.load_leistungssteckbrief(leistungsschluessel)

    async def get_leistungssteckbrief_content(
        self, leistungsschluessel: str
    ) -> XzufiContentInfo | None:
        return await self._database.load_leistungssteckbrief_content_info(
            leistungsschluessel
        )

    async def get_leistungsbeschreibung(
        self, identifier: XzufiLeistungsIdentifier
    ) -> FullLeistungsbeschreibungOut | None:
        return await self._database.load_leistungsbeschreibung(identifier)

    async def get_leistungsbeschreibung_content_info(
        self, identifier: XzufiLeistungsIdentifier
    ) -> XzufiContentInfo | None:
        return await self._database.load_leistungsbeschreibung_content_info(identifier)

    async def delete_leistungsbeschreibung(
        self, identifier: XzufiLeistungsIdentifier
    ) -> None:
        await self._database.delete_leistungsbeschreibung(identifier)

        logger.info(
            "Leistung removed [redation=%s, id=%s]", identifier[0], identifier[1]
        )

    async def delete_leistungsbeschreibung_batch(
        self, identifiers: list[XzufiLeistungsIdentifier]
    ):
        await self._database.delete_leistungsbeschreibung_batch(identifiers)

    async def sync_links(self):
        await self._database.remove_unused_links()

    async def import_links(self, links: list[str]):
        await self._database.save_links(links)

    async def get_link(self, uri: str):
        return await self._database.load_link(uri)

    async def get_links(self, options: PaginationOptions) -> PaginatedResult[Link]:
        return await self._database.load_links(options)

    async def get_links_for_leistung(
        self, leistung_identifier: XzufiLeistungsIdentifier
    ):
        return await self._database.load_links_for_leistung(leistung_identifier)

    async def check_new_link(self, uri: str):
        link_statuses = await self._link_checker.check([uri])
        return link_statuses[0]

    async def check_link_status(
        self, last_checked_before: datetime, batch_size: int = 100
    ):
        assert batch_size >= 2

        links = await self._database.load_oldest_links(batch_size, last_checked_before)
        if len(links) == 0:
            return 0
        link_statuses = await self._link_checker.check([link.uri for link in links])
        now = utc_now()
        updated_links = [
            Link(
                uri=link.uri,
                last_checked=now,
                last_online=now if status else link.last_online,
            )
            for (link, status) in zip(links, link_statuses)
        ]
        await self.update_links(updated_links)
        return len(links)

    async def update_links(self, links: list[Link]):
        await self._database.update_link_statuses(links)

    async def get_leistung_failed_checks(
        self, leistung: Leistung
    ) -> LeistungQualityReport:
        failed_checks = check_leistung(leistung)

        checked_links = await self._check_uris(leistung.get_links())

        return LeistungQualityReport(
            redaktion_id=leistung.id.scheme_agency_id,
            leistung_id=leistung.id.value,
            link_statuses=checked_links,
            failed_checks=failed_checks,
        )

    async def _check_uris(self, uris: list[str]) -> list[LinkStatus]:
        known_link_statuses = await self._database.load_link_statuses(uris)
        known_link_uris = [
            known_link_status.uri for known_link_status in known_link_statuses
        ]
        unknown_link_uris = [uri for uri in uris if uri not in known_link_uris]

        status_unknown_links = await self._link_checker.check(unknown_link_uris)

        checked_links = []
        for link in known_link_statuses:
            checked_links.append(
                LinkStatus(
                    uri=link.uri,
                    is_online=link.is_online(),
                )
            )
        for uri, status in zip(unknown_link_uris, status_unknown_links):
            checked_links.append(
                LinkStatus(
                    uri=uri,
                    is_online=status,
                )
            )

        return checked_links

    async def get_leistung_quality_report(
        self, leistung_identifier: XzufiLeistungsIdentifier
    ) -> LeistungQualityReport | None:
        if await self._database.load_leistungsbeschreibung(leistung_identifier) is None:
            return None
        links = await self._database.load_links_for_leistung(leistung_identifier)
        checked_links = [
            LinkStatus(
                uri=link.uri,
                is_online=link.is_online(),
            )
            for link in links
        ]
        return LeistungQualityReport(
            redaktion_id=leistung_identifier[0],
            leistung_id=leistung_identifier[1],
            link_statuses=checked_links,
            failed_checks=[],
        )

    async def get_json_data_legend(self, data: Any) -> JsonLegend:
        (
            group_identifiers,
            field_identifiers,
        ) = collect_element_identifiers_from_json_keys(data)

        group_ids = [identifier[0] for identifier in group_identifiers]
        field_ids = [identifier[0] for identifier in field_identifiers]

        groups = await self._database.load_group_batch_by_ids(group_ids)
        fields = await self._database.load_field_batch_by_ids(field_ids)

        identifier_to_group = map_identifier_to_element(groups)
        legend_groups: dict[str, DatenfeldgruppeOut | None] = {}
        for identifier in group_identifiers:
            group = identifier_to_group.get(identifier)
            if group is None:
                key = (
                    identifier[0]
                    if identifier[1] == "latest"
                    else f"{identifier[0]}V{identifier[1]}"
                )
            else:
                key = f"{group.fim_id}V{group.fim_version}"

            legend_groups[key] = group

        identifier_to_field = map_identifier_to_element(fields)
        legend_fields: dict[str, DatenfeldOut | None] = {}
        for identifier in field_identifiers:
            field = identifier_to_field.get(identifier)
            if field is None:
                key = (
                    identifier[0]
                    if identifier[1] == "latest"
                    else f"{identifier[0]}V{identifier[1]}"
                )
            else:
                key = f"{field.fim_id}V{field.fim_version}"

            legend_fields[key] = field

        return JsonLegend(groups=legend_groups, fields=legend_fields)

    async def _update_json_schema(
        self,
        schema_fim_id: str,
        schema_fim_version: str,
        content: dict[str, Any],
        source_filename: str,
    ):
        """
        Get the latest json schema content, and update it when there was a change to the current
        saved json schema.

        This is done not by comparing the content directly, but by first creating a canonical
        hash of the content with a constant `$id`. If this is identical to the one already saved,
        no action is required.

        If the `canonical_hash` is different, upload the new json schema content.
        """
        assert content["$id"] == CANONICAL_JSON_SCHEMA_ID
        canonical_hash = hashlib.sha3_512(orjson.dumps(content)).hexdigest()

        latest_hash = await self._database.load_latest_json_schema_hash(
            schema_fim_id, schema_fim_version
        )
        if latest_hash == canonical_hash:
            return

        now = utc_now()
        filename = JsonSchemaFileModel.create_filename(
            schema_fim_id, schema_fim_version, now
        )
        json_schema_id = self._get_json_schema_path(filename)
        content["$id"] = json_schema_id

        await self._database.save_json_schema_file(
            JsonSchemaFileModel(
                filename=filename,
                created_at=now,
                generated_from=source_filename,
                content=orjson.dumps(content).decode("utf-8"),
                canonical_hash=canonical_hash,
            )
        )

        await self._database.update_schema_last_update(
            schema_fim_id, schema_fim_version, last_update=utc_now()
        )

    async def _update_xsd(
        self,
        schema_fim_id: str,
        schema_fim_version: str,
        content: str,
        source_filename: str,
    ):
        canonical_hash = hashlib.sha3_512(content.encode("utf-8")).hexdigest()

        latest_hash = await self._database.load_latest_xsd_hash(
            schema_fim_id, schema_fim_version
        )
        if latest_hash == canonical_hash:
            return

        await self._database.save_xsd_file(
            XsdFileModel.create(
                schema_fim_id=schema_fim_id,
                schema_fim_version=schema_fim_version,
                content=content,
                generated_from=source_filename,
                canonical_hash=canonical_hash,
            )
        )

        await self._database.update_schema_last_update(
            schema_fim_id, schema_fim_version, last_update=utc_now()
        )

    async def import_prozess_batch(self, batch: list[tuple[Prozess, str]]):
        new_prozesse: list[NewProzess] = []
        prozess_ids: list[str] = []
        for prozess, xml_content in batch:
            new_prozess = NewProzess.from_prozess(
                prozess=prozess, xml_content=xml_content
            )
            new_prozesse.append(new_prozess)
            prozess_ids.append(new_prozess.id)

        await self._database.save_prozess_batch(new_prozesse)

        for prozess in new_prozesse:
            logger.info("Prozess imported [id=%s, name=%s]", prozess.id, prozess.name)

        return prozess_ids

    async def import_prozessklasse_batch(self, batch: list[tuple[Prozessklasse, str]]):
        new_prozessklassen: list[NewProzessklasse] = []
        prozessklasse_ids: list[str] = []
        for prozessklasse, xml_content in batch:
            new_prozessklasse = NewProzessklasse.from_prozessklasse(
                prozessklasse=prozessklasse, xml_content=xml_content
            )
            new_prozessklassen.append(new_prozessklasse)
            prozessklasse_ids.append(new_prozessklasse.id)

        await self._database.save_prozessklasse_batch(new_prozessklassen)

        for prozessklasse in new_prozessklassen:
            logger.info(
                "Prozessklasse imported [id=%s, name=%s]",
                prozessklasse.id,
                prozessklasse.name,
            )

        return prozessklasse_ids

    async def search_prozesse(
        self, options: ProzessSearchOptions
    ) -> PaginatedResult[ProzessOut]:
        return await self._database.search_prozesse(options)

    async def search_prozessklassen(
        self, options: ProzessklasseSearchOptions
    ) -> PaginatedResult[ProzessklasseOut]:
        return await self._database.search_prozessklassen(options)

    async def get_prozess(self, prozess_id: str):
        return await self._database.load_prozess(prozess_id)

    async def get_prozessklasse(self, prozessklasse_id: str):
        return await self._database.load_prozessklasse(prozessklasse_id)

    async def get_prozess_xml(self, prozess_id: str):
        return await self._database.load_prozess_xml_content(prozess_id)

    async def get_prozess_report_file(self, prozess_id: str):
        return await self._database.load_prozess_report_file(prozess_id)

    async def get_prozess_visualization_file(self, prozess_id: str):
        return await self._database.load_prozess_visualization_file(prozess_id)

    async def get_prozessklasse_xml(self, prozessklasse_id: str):
        return await self._database.load_prozessklasse_xml_content(prozessklasse_id)

    async def get_prozess_ids(self):
        return await self._database.load_prozess_ids()

    async def get_prozessklassen_ids(self):
        return await self._database.load_prozessklasse_ids()

    async def delete_prozess(self, id: str):
        await self._database.delete_prozess(id)

    async def delete_prozessklasse(self, id: str):
        await self._database.delete_prozessklasse(id)

    async def save_pvog_batch(self, batch_id: int, batch_xml: str):
        await self._database.save_pvog_batch(batch_id, batch_xml)

    async def get_latest_pvog_batch_id(self) -> int:
        return await self._database.get_latest_pvog_batch_id()

    async def get_raw_pvog_resource_identifiers(
        self, resource_class: PvogResourceClass
    ) -> list[XzufiLeistungsIdentifier]:
        return await self._database.get_raw_pvog_resource_identifiers(resource_class)

    async def save_raw_pvog_resource(
        self,
        identifier: XzufiLeistungsIdentifier,
        leistung_xml: str,
        resource_class: PvogResourceClass,
    ):
        await self._database.save_raw_pvog_resource(
            identifier, leistung_xml, resource_class
        )

    async def delete_raw_pvog_resource(
        self,
        identifier: XzufiLeistungsIdentifier,
        resource_class: PvogResourceClass,
    ):
        await self._database.delete_raw_pvog_resource(identifier, resource_class)

    async def sync_pvog_leistungen(self):
        batch = await self._database.get_raw_pvog_resource_for_sync(
            PvogResourceClass.LEISTUNG
        )
        if len(batch) == 0:
            return False

        leistungen_batch: list[tuple[Leistung, str]] = []
        for (redaktion_id, id), leistung_xml in batch:
            try:
                leistungen_batch.append((parse_leistung(leistung_xml), leistung_xml))
            except XzufiException as error:
                logger.exception(
                    f"Failed parsing leistung: {error} [redaktion_id={redaktion_id}, id={id}]"
                )

        await self.import_leistung_batch(leistungen_batch, XzufiSource.PVOG)

        await self._database.mark_raw_pvog_resource_as_synced(
            [identifier for identifier, _ in batch],
            PvogResourceClass.LEISTUNG,
        )
        logger.info(f"Synced new pvog leistungen - {len(leistungen_batch)}")
        return True

    async def sync_pvog_organisationseinheit(self):
        batch = await self._database.get_raw_pvog_resource_for_sync(
            PvogResourceClass.ORGANISATIONSEINHEIT
        )
        if len(batch) == 0:
            return False

        resource_batch: list[tuple[Organisationseinheit, str]] = []
        for (redaktion_id, id), content_xml in batch:
            try:
                resource_batch.append(
                    (parse_organisationseinheit(content_xml), content_xml)
                )
            except XzufiException as error:
                logger.exception(
                    f"Failed parsing organisationseinheit: {error} [redaktion_id={redaktion_id}, id={id}]"
                )

        await self.import_organisationseinheit_batch(resource_batch)

        await self._database.mark_raw_pvog_resource_as_synced(
            [identifier for identifier, _ in batch],
            PvogResourceClass.ORGANISATIONSEINHEIT,
        )
        logger.info(f"Synced new pvog Organisationseinheit - {len(resource_batch)}")
        return True

    async def sync_pvog_onlinedienst(self):
        batch = await self._database.get_raw_pvog_resource_for_sync(
            PvogResourceClass.ONLINEDIENST
        )
        if len(batch) == 0:
            return False

        resource_batch: list[tuple[Onlinedienst, str]] = []
        for (redaktion_id, id), content_xml in batch:
            try:
                resource_batch.append((parse_onlinedienst(content_xml), content_xml))
            except XzufiException as error:
                logger.exception(
                    f"Failed parsing onlinedienst: {error} [redaktion_id={redaktion_id}, id={id}]"
                )

        await self.import_onlinedienst_batch(resource_batch)

        await self._database.mark_raw_pvog_resource_as_synced(
            [identifier for identifier, _ in batch],
            PvogResourceClass.ONLINEDIENST,
        )
        logger.info(f"Synced new pvog Onlinedienst - {len(resource_batch)}")
        return True

    async def sync_pvog_spezialisierung(self):
        batch = await self._database.get_raw_pvog_resource_for_sync(
            PvogResourceClass.SPEZIALISIERUNG
        )
        if len(batch) == 0:
            return False

        resource_batch: list[tuple[Spezialisierung, str]] = []
        for (redaktion_id, id), content_xml in batch:
            try:
                resource_batch.append((parse_spezialisierung(content_xml), content_xml))
            except XzufiException as error:
                logger.exception(
                    f"Failed parsing Spezialisierung: {error}, [redaktion_id={redaktion_id}, id={id}]"
                )

        await self.import_spezialisierung_batch(resource_batch)

        await self._database.mark_raw_pvog_resource_as_synced(
            [identifier for identifier, _ in batch],
            PvogResourceClass.SPEZIALISIERUNG,
        )
        logger.info(f"Synced new pvog Spezialisierung - {len(resource_batch)}")
        return True

    async def sync_pvog_zustaendigkeit(self):
        batch = await self._database.get_raw_pvog_resource_for_sync(
            PvogResourceClass.ZUSTAENDIGKEIT
        )
        if len(batch) == 0:
            return False

        resource_batch: list[tuple[Zustaendigkeit, str]] = []
        for (redaktion_id, id), content_xml in batch:
            try:
                resource_batch.append(
                    (parse_zustaendigkeit_variante(content_xml), content_xml)
                )
            except XzufiException as error:
                logger.exception(
                    f"Failed parsing Zustaendigkeit: {error} [redaktion_id={redaktion_id}, id={id}]"
                )

        await self.import_zustaendigkeit_batch(resource_batch)

        await self._database.mark_raw_pvog_resource_as_synced(
            [identifier for identifier, _ in batch],
            PvogResourceClass.ZUSTAENDIGKEIT,
        )
        logger.info(f"Synced new pvog Zustaendigkeit - {len(resource_batch)}")
        return True

    async def import_organisationseinheit_batch(
        self,
        organisationseinheiten: list[tuple[Organisationseinheit, str]],
    ) -> list[XzufiIdentifier]:
        new_organisationseinheiten: list[NewOrganisationseinheit] = []
        for organisationseinheit, xml_content in organisationseinheiten:
            try:
                new_organisationseinheit = (
                    NewOrganisationseinheit.from_organisationseinheit(
                        organisationseinheit, xml_content
                    )
                )
            except CannotImportOrganisationseinheit as error:
                logger.warning(
                    "Skip importing Organisationseinheit: %s [id=%s]",
                    str(error),
                    organisationseinheit.id,
                )
            else:
                new_organisationseinheiten.append(new_organisationseinheit)

        await self._database.save_organisationseinheit_batch(new_organisationseinheiten)

        return [
            organisationseinheit.id
            for organisationseinheit in new_organisationseinheiten
        ]

    async def import_onlinedienst_batch(
        self,
        onlinedienste: list[tuple[Onlinedienst, str]],
    ) -> list[XzufiIdentifier]:
        new_onlinedienste: list[NewOnlinedienst] = []
        for onlinedienst, xml_content in onlinedienste:
            try:
                new_onlinedienst = NewOnlinedienst.from_onlinedienst(
                    onlinedienst, xml_content
                )
            except CannotImportOnlinedienst as error:
                logger.warning(
                    "Skip importing Onlinedienst: %s [id=%s]",
                    str(error),
                    onlinedienst.id,
                )
            else:
                new_onlinedienste.append(new_onlinedienst)

        await self._database.save_onlinedienst_batch(new_onlinedienste)

        return [organisationseinheit.id for organisationseinheit in new_onlinedienste]

    async def import_zustaendigkeit_batch(
        self,
        zustaendigkeiten: list[tuple[Zustaendigkeit, str]],
    ) -> list[XzufiIdentifier]:
        new_zustaendigkeiten: list[NewZustaendigkeit] = []
        for zustaendigkeit, xml_content in zustaendigkeiten:
            try:
                new_zustaendigkeit = NewZustaendigkeit.from_zustaendigkeit(
                    zustaendigkeit, xml_content
                )
            except CannotImportZustaendigkeit as error:
                logger.warning(
                    "Skip importing Zustaendigkeit: %s [id=%s]",
                    str(error),
                    zustaendigkeit.id,
                )
            else:
                new_zustaendigkeiten.append(new_zustaendigkeit)

        await self._database.save_zustaendigkeit_batch(new_zustaendigkeiten)

        return [
            organisationseinheit.id for organisationseinheit in new_zustaendigkeiten
        ]

    async def import_spezialisierung_batch(
        self,
        spezialisierungen: list[tuple[Spezialisierung, str]],
    ) -> list[XzufiIdentifier]:
        new_spezialisierungen: list[NewSpezialisierung] = []
        for spezialisierung, xml_content in spezialisierungen:
            try:
                new_spezialisierung = NewSpezialisierung.from_spezialisierung(
                    spezialisierung, xml_content
                )
            except CannotImportOrganisationseinheit as error:
                logger.warning(
                    "Skip importing Spezialisierung: %s [id=%s]",
                    str(error),
                    spezialisierung.id,
                )
            else:
                new_spezialisierungen.append(new_spezialisierung)

        await self._database.save_spezialisierung_batch(new_spezialisierungen)

        return [spezialisierung.id for spezialisierung in new_spezialisierungen]

    async def search_xzufi_zustaendigkeiten(self, options: PaginationOptions):
        return await self._database.search_xzufi_zustaendigkeiten(options)

    async def get_xzufi_zustaendigkeit(self, identifier: XzufiIdentifier):
        return await self._database.load_xzufi_zustaendigkeit(identifier)

    async def get_xzufi_zustaendigkeit_xml(self, identifier: XzufiIdentifier):
        return await self._database.load_xzufi_zustaendigkeit_xml(identifier)

    async def search_xzufi_onlinedienste(self, options: PaginationOptions):
        return await self._database.search_xzufi_onlinedienste(options)

    async def get_xzufi_onlinedienst(self, identifier: XzufiIdentifier):
        return await self._database.load_xzufi_onlinedienst(identifier)

    async def get_xzufi_onlinedienst_xml(self, identifier: XzufiIdentifier):
        return await self._database.load_xzufi_onlinedienst_xml(identifier)

    async def search_xzufi_organisationseinheiten(self, options: PaginationOptions):
        return await self._database.search_xzufi_organisationseinheiten(options)

    async def get_xzufi_organisationseinheit(self, identifier: XzufiIdentifier):
        return await self._database.load_xzufi_organisationseinheit(identifier)

    async def get_xzufi_organisationseinheit_xml(self, identifier: XzufiIdentifier):
        return await self._database.load_xzufi_organisationseinheit_xml(identifier)

    async def search_xzufi_spezialisierung(self, options: PaginationOptions):
        return await self._database.search_xzufi_spezialisierung(options)

    async def get_xzufi_spezialisierung_xml(self, identifier: XzufiIdentifier):
        return await self._database.get_xzufi_spezialisierung_xml(identifier)

    async def get_xzufi_spezialisierung(self, identifier: XzufiIdentifier):
        return await self._database.get_xzufi_spezialisierung(identifier)

    async def delete_removed_pvog_resources(self):
        await self._database.delete_removed_pvog_leistungen()
        logger.info("Deleted old pvog leistungen")
        await self._database.delete_removed_spezialisierungen()
        logger.info("Deleted old pvog spezialisierungen")
        await self._database.delete_removed_organisationseinheiten()
        logger.info("Deleted old pvog organisationseinheiten")
        await self._database.delete_removed_zustaendigkeiten()
        logger.info("Deleted old pvog zustaendigkeiten")
        await self._database.delete_removed_onlinedienste()
        logger.info("Deleted old pvog onlinedienste")

    async def import_gebiet_id(self, gebiet_id: CodeList):
        ids = gebiet_id.columns[gebiet_id.default_code_key]
        version = gebiet_id.identifier.version
        if version is None:
            raise Exception("Can only import gebiet id code list with version.")

        id_to_label: dict[str, str] = {}
        for id, label in zip(ids, gebiet_id.columns["Bezeichnung"]):
            assert id is not None
            assert label is not None

            # Some older code list versions used an unsuitable data type for the ids,
            # causing large ids to be displayed like '1,634E+11'.
            # These invalid keys shoul be ignored.
            if "E+" in id:
                continue

            if id in id_to_label:
                raise Exception(
                    f"Duplicate id {id} in code list {gebiet_id.identifier.canonical_version_uri}"
                )
            id_to_label[id] = label

        await self._database.upsert_gebiet_id(
            list(id_to_label.items()),
            urn=gebiet_id.identifier.canonical_uri,
            version=date.fromisoformat(version),
        )

        logger.info(
            "imported GebietIds from codelist %s",
            gebiet_id.identifier.canonical_version_uri,
        )

    async def get_gebiet_id_label(self, id: str):
        return await self._database.get_gebiet_id_label(id)

    async def get_gebiet_ids(self):
        return await self._database.get_gebiet_ids()

    async def delete_removed_gebiet_id(self, last_import: datetime):
        await self._database.delete_removed_gebiet_ids(last_import)

    async def get_recently_changed_service_steckbriefe(
        self, timespan: timedelta
    ) -> list[LeistungssteckbriefOut]:
        return await self._database.get_recently_updated_services(utc_now() - timespan)

    async def get_recently_changed_processes(self, timespan: timedelta):
        return await self._database.get_recently_updated_processes(utc_now() - timespan)

    async def get_recently_changed_schema_resources(self, timespan: timedelta):
        (
            result_schemas,
            result_groups,
            result_fields,
            result_document_profiles,
        ) = await self._database.get_recently_updated_datenfelder_resources(
            utc_now() - timespan
        )

        return sorted(
            [
                *result_schemas,
                *result_document_profiles,
                *result_groups,
                *result_fields,
            ],
            key=lambda item: item.last_update,
            reverse=True,
        )

    async def import_katalog(self, katalog: NewKatalog):
        await self._database.save_katalog(katalog)

    async def get_katalog_content(self, filename: str):
        return await self._database.get_katalog_content(filename)

    async def get_dokumentsteckbrief_csv_search_results(
        self, options: SteckbriefSearchOptions
    ) -> list[CSVSearchResult]:
        return await self._database.get_dokumentsteckbrief_csv_search_results(options)

    async def get_schema_csv_search_results(
        self, options: SchemaSearchOptions
    ) -> list[CSVSearchResult]:
        return await self._database.get_schema_csv_search_results(options)

    async def get_group_csv_search_results(
        self, options: GroupSearchOptions
    ) -> list[CSVSearchResult]:
        return await self._database.get_group_csv_search_results(options)

    async def get_field_csv_search_results(
        self, options: FieldSearchOptions
    ) -> list[CSVSearchResult]:
        return await self._database.get_field_csv_search_results(options)

    async def get_leistungssteckbrief_csv_search_results(
        self, options: LeistungssteckbriefSearchOptions
    ) -> list[CSVSearchResult]:
        return await self._database.get_leistungssteckbrief_csv_search_results(options)

    async def get_leistungsbeschreibung_csv_search_results(
        self, options: LeistungsbeschreibungSearchOptions
    ) -> list[CSVSearchResult]:
        return await self._database.get_leistungsbeschreibung_csv_search_results(
            options
        )

    async def get_prozess_csv_search_results(
        self, options: ProzessSearchOptions
    ) -> list[CSVSearchResult]:
        return await self._database.get_prozess_csv_search_results(options)

    async def get_prozessklasse_csv_search_results(
        self, options: ProzessklasseSearchOptions
    ) -> list[CSVSearchResult]:
        return await self._database.get_prozessklasse_csv_search_results(options)


def _get_immutable_data_diff(
    old_immutable_dict: dict[str, Any],
    new_immutable_dict: dict[str, Any],
    old_veroeffentlichungsdatum: date | None,
    new_veroeffentlichungsdatum: date | None,
) -> dict[str, tuple[str, str]]:
    diff = {}

    for attribute in new_immutable_dict:
        if new_immutable_dict[attribute] != old_immutable_dict[attribute]:
            diff[attribute] = (
                str(old_immutable_dict[attribute]),
                str(new_immutable_dict[attribute]),
            )

    if old_veroeffentlichungsdatum is not None:
        if old_veroeffentlichungsdatum != new_veroeffentlichungsdatum:
            diff["veroeffentlichungsdatum"] = (
                str(old_veroeffentlichungsdatum),
                str(new_veroeffentlichungsdatum),
            )

    return diff
