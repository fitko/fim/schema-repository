ALTER TABLE prozess ADD COLUMN letzter_aenderungszeitpunkt TIMESTAMP WITH TIME ZONE DEFAULT NULL;
ALTER TABLE prozess ALTER COLUMN letzter_aenderungszeitpunkt DROP DEFAULT;