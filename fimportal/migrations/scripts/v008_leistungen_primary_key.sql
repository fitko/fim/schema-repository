-- Remove all invalid Leistungen we still have in the database first.
DELETE FROM leistung WHERE redaktion_id IS NULL;

ALTER TABLE leistung DROP CONSTRAINT leistung_pkey;
ALTER INDEX idx_leistung_redaktion_id RENAME TO leistung_pkey;
ALTER TABLE leistung ADD PRIMARY KEY USING INDEX leistung_pkey;

