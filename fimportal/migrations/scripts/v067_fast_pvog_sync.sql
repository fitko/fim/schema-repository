ALTER TABLE raw_pvog_resource ADD COLUMN synced BOOL NOT NULL DEFAULT FALSE;
ALTER TABLE raw_pvog_resource DROP COLUMN last_synced;

CREATE INDEX idx_raw_pvog_resource_unsynced_resources ON raw_pvog_resource (resource_class)
WHERE NOT synced;

