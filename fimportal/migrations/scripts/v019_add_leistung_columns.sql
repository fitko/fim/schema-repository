-- First create the column with dummy values, then immediately remove the default value, as this must be filled during import.
-- The dummy values will be updated the next time the importer runs.
ALTER TABLE leistung ADD COLUMN sdg_informationsbereiche TEXT ARRAY NOT NULL DEFAULT ARRAY[]::text[];
ALTER TABLE leistung ALTER COLUMN sdg_informationsbereiche DROP DEFAULT;

ALTER TABLE leistung ADD COLUMN has_modul_bezeichnung_2 BOOL NOT NULL DEFAULT FALSE;
ALTER TABLE leistung ALTER COLUMN has_modul_bezeichnung_2 DROP DEFAULT;

ALTER TABLE leistung ADD COLUMN has_modul_rechtsgrundlagen BOOL NOT NULL DEFAULT FALSE;
ALTER TABLE leistung ALTER COLUMN has_modul_rechtsgrundlagen DROP DEFAULT;

ALTER TABLE leistung ADD COLUMN has_modul_teaser BOOL NOT NULL DEFAULT FALSE;
ALTER TABLE leistung ALTER COLUMN has_modul_teaser DROP DEFAULT;

ALTER TABLE leistung ADD COLUMN has_modul_zustaendige_stelle BOOL NOT NULL DEFAULT FALSE;
ALTER TABLE leistung ALTER COLUMN has_modul_zustaendige_stelle DROP DEFAULT;

ALTER TABLE leistung ADD COLUMN has_modul_volltext BOOL NOT NULL DEFAULT FALSE;
ALTER TABLE leistung ALTER COLUMN has_modul_volltext DROP DEFAULT;

