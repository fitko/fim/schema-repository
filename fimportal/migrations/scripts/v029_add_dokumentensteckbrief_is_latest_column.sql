ALTER TABLE dokumentensteckbrief ADD COLUMN is_latest BOOL NOT NULL DEFAULT FALSE;

-- Add unique constraint (only allow one IS_LATEST = TRUE per fim_id)
CREATE UNIQUE INDEX idx_unique_is_latest_per_fim_id_dokumentensteckbrief
ON dokumentensteckbrief (fim_id)
WHERE is_latest = TRUE;


-- Automatically update is_latest

CREATE FUNCTION update_latest_dokumentensteckbrief(p_fim_id TEXT)
RETURNS void AS $$
BEGIN
    -- Set all rows for the same fim_id to FALSE
    UPDATE dokumentensteckbrief
    SET is_latest = FALSE
    WHERE fim_id = p_fim_id;

    -- Set the row with the highest fim_version to TRUE
    UPDATE dokumentensteckbrief
    SET is_latest = TRUE
    WHERE fim_id = p_fim_id
      AND fim_version = (
          SELECT fim_version
          FROM dokumentensteckbrief
          WHERE fim_id = p_fim_id
          ORDER BY string_to_array(fim_version, '.')::int[] DESC
          LIMIT 1
      );
END;
$$ LANGUAGE plpgsql;


CREATE FUNCTION call_update_latest_dokumentensteckbrief()
RETURNS TRIGGER AS $$
BEGIN

    PERFORM update_latest_dokumentensteckbrief(NEW.fim_id);

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER trg_update_is_latest
AFTER INSERT ON dokumentensteckbrief
FOR EACH ROW
EXECUTE FUNCTION call_update_latest_dokumentensteckbrief();


-- Update on migration
DO $$
DECLARE
    rec RECORD;
BEGIN
    FOR rec IN
        SELECT DISTINCT fim_id
        FROM dokumentensteckbrief
    LOOP
        PERFORM update_latest_dokumentensteckbrief(rec.fim_id);
    END LOOP;
END;
$$;