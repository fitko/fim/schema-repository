CREATE TABLE pvog_batch (
    batch_id INTEGER PRIMARY KEY,
    batch_xml TEXT NOT NUll
);

CREATE TABLE raw_pvog_leistung (
    redaktion_id VARCHAR,
    leistung_id VARCHAR,
    xml_content VARCHAR NOT NULL,
    last_synced TIMESTAMP WITH TIME ZONE,
    PRIMARY KEY(redaktion_id, leistung_id)
);