-- First delete all rule-related data, as these will be backfilled anyway.
DELETE FROM schema_regel_relation;
DELETE FROM datenfeldgruppe_regel_relation;
DELETE FROM datenfeld_regel_relation;
DELETE FROM regel;

-- Add columns to regel table
ALTER TABLE regel 
    ADD COLUMN nummernkreis TEXT NOT NULL,
    ADD COLUMN name TEXT NOT NULL,
    ADD COLUMN letzte_aenderung TIMESTAMP WITH TIME ZONE NOT NULL,
    ADD COLUMN xdf2_veroeffentlichungsdatum DATE,
    ADD COLUMN last_update TIMESTAMP WITH TIME ZONE NOT NULL;

-- Add missing column to the steckbrief table
ALTER TABLE dokumentensteckbrief ADD COLUMN veroeffentlichungsdatum DATE;

-- ADDITION: Fix wrong bezug values due to previous bug
UPDATE datenfeldgruppe SET bezug = '{}' WHERE bezug = '{""}' AND xdf_version = '2.0';
UPDATE datenfeld SET bezug = '{}' WHERE bezug = '{""}' AND xdf_version = '2.0';
