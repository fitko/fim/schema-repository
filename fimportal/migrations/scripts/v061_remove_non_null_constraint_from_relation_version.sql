-- The previous primary key implicitly made all included columns NOT NULL.
-- Since the target_fim_version can be NULL however, we now use a simple serial primary key instead.
DROP TABLE dokumentsteckbrief_relations;
CREATE TABLE dokumentsteckbrief_relations (
    id SERIAL PRIMARY KEY,
    source_fim_id TEXT NOT NULL,
    source_fim_version TEXT NOT NULL,
    target_fim_id TEXT NOT NULL,
    target_fim_version TEXT,
    praedikat TEXT NOT NULL,
    UNIQUE (source_fim_id, source_fim_version, target_fim_id, target_fim_version, praedikat),
    FOREIGN KEY (source_fim_id, source_fim_version) REFERENCES dokumentensteckbrief (fim_id, fim_version) ON DELETE CASCADE
);

DROP TABLE schema_relations;
CREATE TABLE schema_relations (
    id SERIAL PRIMARY KEY,
    source_fim_id TEXT NOT NULL,
    source_fim_version TEXT NOT NULL,
    target_fim_id TEXT NOT NULL,
    target_fim_version TEXT,
    praedikat TEXT NOT NULL,
    UNIQUE (source_fim_id, source_fim_version, target_fim_id, target_fim_version, praedikat),
    FOREIGN KEY (source_fim_id, source_fim_version) REFERENCES schema (fim_id, fim_version) ON DELETE CASCADE
);

DROP TABLE datenfeldgruppe_relations;
CREATE TABLE datenfeldgruppe_relations (
    id SERIAL PRIMARY KEY,
    source_namespace TEXT NOT NULL,
    source_fim_id TEXT NOT NULL,
    source_fim_version TEXT NOT NULL,
    target_fim_id TEXT NOT NULL,
    target_fim_version TEXT,
    praedikat TEXT NOT NULL,
    UNIQUE (source_fim_id, source_namespace, source_fim_version, target_fim_id, target_fim_version, praedikat),
    FOREIGN KEY (source_namespace, source_fim_id, source_fim_version) REFERENCES datenfeldgruppe (namespace, fim_id, fim_version) ON DELETE CASCADE
);

DROP TABLE datenfeld_relations;
CREATE TABLE datenfeld_relations (
    id SERIAL PRIMARY KEY,
    source_namespace TEXT NOT NULL,
    source_fim_id TEXT NOT NULL,
    source_fim_version TEXT NOT NULL,
    target_fim_id TEXT NOT NULL,
    target_fim_version TEXT,
    praedikat TEXT NOT NULL,
    UNIQUE (source_fim_id, source_namespace, source_fim_version, target_fim_id, target_fim_version, praedikat),
    FOREIGN KEY (source_namespace, source_fim_id, source_fim_version) REFERENCES datenfeld (namespace, fim_id, fim_version) ON DELETE CASCADE
);


