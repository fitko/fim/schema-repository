ALTER TABLE leistung RENAME COLUMN bezeichnung TO title;

ALTER TABLE leistung ADD COLUMN has_modul_bezeichnung BOOL NOT NULL DEFAULT TRUE;
ALTER TABLE leistung ALTER COLUMN has_modul_bezeichnung DROP DEFAULT;
