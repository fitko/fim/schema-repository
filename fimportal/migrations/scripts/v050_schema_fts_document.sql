-- The trigger is no longer needed, as the raw_content of this table
-- is now filled explicitly during the INSERT call.
DROP TRIGGER trg_schema_raw_content ON schema;

