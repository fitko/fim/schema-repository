-- Relation between Prozess and Dokumentstesteckbrief
-- Since the relation is defined on the Prozess, there is no guarantee that the referenced Dokumentsteckbrief
-- actually exists. Therefore, the relation is not defined as a foreign key.
CREATE TABLE prozess_dokumentsteckbrief_relation (
    prozess_id TEXT NOT NULL,
    -- The Prozess only references the FIM-id of the Dokumentsteckbrief, not the specific version.
    -- In practice, the `is_latest` steckbrief should be referenced.
    dokumentsteckbrief_fim_id TEXT NOT NULL,
    -- The role should be either `ausloeser` or `ergebnis`, depending on on the role of the Dokumentsteckbrief in the Prozess.
    role TEXT NOT NULL,

    PRIMARY KEY (prozess_id, dokumentsteckbrief_fim_id),
    FOREIGN KEY (prozess_id) REFERENCES prozess (id)
);

