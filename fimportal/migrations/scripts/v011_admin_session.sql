CREATE TABLE admin_session (
    token VARCHAR PRIMARY KEY,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL
);
