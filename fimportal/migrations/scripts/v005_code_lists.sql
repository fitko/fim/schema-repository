ALTER TABLE code_list ADD source VARCHAR;
ALTER TABLE code_list ALTER COLUMN is_external DROP EXPRESSION;

-- This index will be included once the duplicated external code lists are removed.
-- CREATE UNIQUE INDEX idx_canonical_version_uri ON code_list (genericode_canonical_version_uri) WHERE is_external = true;
