CREATE FUNCTION xzufi_xml_to_tsvector(xml_data varchar) RETURNS tsvector AS $$
	DECLARE
		content text;
		ns_array text[][];
BEGIN
	ns_array := ARRAY[ARRAY['xzufi', 'http://xoev.de/schemata/xzufi/2_2_0']];
	content := array_to_string(xpath('//text()', xmlparse(DOCUMENT xml_data), ns_array), ' ');
    RETURN to_tsvector('german', content);
END;
$$ LANGUAGE plpgsql;

ALTER TABLE leistung ADD COLUMN xml_content_fts tsvector;

CREATE INDEX idx_leistung_xml_content_fts ON leistung USING GIN(xml_content_fts);

UPDATE leistung SET xml_content_fts = xzufi_xml_to_tsvector(xml_content);

CREATE FUNCTION leistung_tsvector_trigger() RETURNS trigger AS $$
DECLARE
BEGIN 
  NEW.xml_content_fts := xzufi_xml_to_tsvector(NEW.xml_content);
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trg_leistung_tsvector BEFORE INSERT OR UPDATE ON leistung FOR EACH ROW EXECUTE FUNCTION leistung_tsvector_trigger();

ALTER TABLE leistung ALTER COLUMN xml_content_fts SET NOT NULL;