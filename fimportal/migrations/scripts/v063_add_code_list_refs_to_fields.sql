ALTER TABLE datenfeld
    ADD COLUMN code_list_id INTEGER,
    ADD constraint fk_code_list_id FOREIGN KEY (code_list_id) REFERENCES code_list(id);

