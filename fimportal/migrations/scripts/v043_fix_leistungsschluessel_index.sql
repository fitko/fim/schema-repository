DROP INDEX idx_leistungsschluessel;

-- Also drop this older index
DROP INDEX idx_leistung_leistungsschluessel;

-- This index is specifically made for the search of related Leistungsbeschreibungen for a spezific Leistungsschluessel.
-- The previous version of this index was not used because it was not a gin index.
CREATE INDEX idx_leistungsschluessel ON leistung USING gin (leistungsschluessel array_ops)
	WHERE root_for_leistungsschluessel IS NULL;
