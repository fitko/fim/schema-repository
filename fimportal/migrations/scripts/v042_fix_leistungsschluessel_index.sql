DROP INDEX idx_leistungsschluessel;

-- This index is only used when sorting "Leistungsbeschreibungen",
-- which is determined by the root_for_leistungsschluessel column being empty.
CREATE INDEX idx_leistungsschluessel ON leistung(leistungsschluessel)
WHERE root_for_leistungsschluessel IS NULL;

-- Create the equivalent index for "Steckbriefe".
CREATE INDEX idx_root_for_leistungsschluessel ON leistung(root_for_leistungsschluessel)
WHERE root_for_leistungsschluessel IS NOT NULL;
