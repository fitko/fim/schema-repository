CREATE TABLE access_token (
    id SERIAL PRIMARY KEY,
    token VARCHAR NOT NULL UNIQUE,
    nummernkreis VARCHAR NOT NULL UNIQUE,
    description VARCHAR NOT NULL
);
CREATE TABLE dokumentensteckbrief (
    fim_id VARCHAR,
    fim_version VARCHAR,
    nummernkreis VARCHAR NOT NULL,
    name VARCHAR NOT NULL,
    definition VARCHAR,
    bezeichnung VARCHAR,
    beschreibung VARCHAR,
    freigabe_status SMALLINT NOT NULL,
    versionshinweis VARCHAR,
    status_gesetzt_durch VARCHAR,
    status_gesetzt_am DATE,
    gueltig_ab DATE,
    gueltig_bis DATE,
    bezug VARCHAR ARRAY NOT NULL,
    stichwort VARCHAR ARRAY NOT NULL,
    letzte_aenderung TIMESTAMP WITH TIME ZONE NOT NULL,
    last_update TIMESTAMP WITH TIME ZONE NOT NULL,
    dokumentart VARCHAR,
    is_referenz BOOLEAN NOT NULL,
    hilfetext VARCHAR,
    xdf_version VARCHAR NOT NULL,
    PRIMARY KEY (fim_id, fim_version)
);
CREATE TABLE schema (
    fim_id VARCHAR,
    fim_version VARCHAR,
    nummernkreis VARCHAR NOT NULL,
    name VARCHAR NOT NULL,
    beschreibung VARCHAR,
    definition VARCHAR,
    bezug VARCHAR ARRAY NOT NULL,
    freigabe_status SMALLINT NOT NULL,
    status_gesetzt_durch VARCHAR,
    gueltig_ab DATE,
    gueltig_bis DATE,
    versionshinweis VARCHAR,
    letzte_aenderung TIMESTAMP WITH TIME ZONE NOT NULL,
    stichwort VARCHAR ARRAY NOT NULL,
    steckbrief_id VARCHAR,
    xdf_version VARCHAR NOT NULL,
    last_update TIMESTAMP WITH TIME ZONE NOT NULL,
    bezeichnung VARCHAR,
    status_gesetzt_am DATE,
    bezug_components VARCHAR ARRAY NOT NULL,
    quality_report JSONB NOT NULL,
    PRIMARY KEY (fim_id, fim_version)
);
CREATE TABLE datenfeld (
    namespace VARCHAR,
    fim_id VARCHAR,
    fim_version VARCHAR,
    nummernkreis VARCHAR NOT NULL,
    xdf_version VARCHAR NOT NULL,
    name VARCHAR NOT NULL,
    beschreibung VARCHAR,
    definition VARCHAR,
    status_gesetzt_durch VARCHAR,
    feldart VARCHAR NOT NULL,
    datentyp VARCHAR NOT NULL,
    PRIMARY KEY (namespace, fim_id, fim_version)
);
CREATE TABLE datenfeldgruppe (
    namespace VARCHAR,
    fim_id VARCHAR,
    fim_version VARCHAR,
    nummernkreis VARCHAR NOT NULL,
    xdf_version VARCHAR NOT NULL,
    name VARCHAR NOT NULL,
    beschreibung VARCHAR,
    definition VARCHAR,
    status_gesetzt_durch VARCHAR,
    PRIMARY KEY (namespace, fim_id, fim_version)
);
-- The base table for each node in a schema tree:
-- - schema: always the root node
-- - group: mostly nodes in the middle of the tree, sometimes leaf nodes in case of malformed xdf2
-- - fields: always leaf nodes
CREATE TABLE tree_node (
    id SERIAL PRIMARY KEY,
    schema_fim_id VARCHAR,
    schema_fim_version VARCHAR,
    UNIQUE(schema_fim_id, schema_fim_version),
    FOREIGN KEY (schema_fim_id, schema_fim_version) REFERENCES schema (fim_id, fim_version) MATCH FULL,
    group_namespace VARCHAR,
    group_fim_id VARCHAR,
    group_fim_version VARCHAR,
    UNIQUE(group_namespace, group_fim_id, group_fim_version),
    FOREIGN KEY (group_namespace, group_fim_id, group_fim_version) REFERENCES datenfeldgruppe (namespace, fim_id, fim_version) MATCH FULL,
    field_namespace VARCHAR,
    field_fim_id VARCHAR,
    field_fim_version VARCHAR,
    UNIQUE(field_namespace, field_fim_id, field_fim_version),
    FOREIGN KEY (field_namespace, field_fim_id, field_fim_version) REFERENCES datenfeld (namespace, fim_id, fim_version) MATCH FULL
);
-- The relations between nodes in the schema tree.
-- Each connection is uniquely defined by the parent_id and the index.
-- Each child_id can occur multiple times, so would not be necessary to uniquely define
-- the relation.
CREATE TABLE tree_relation (
    parent_id INTEGER,
    child_index INTEGER,
    child_id INTEGER NOT NULL,
    anzahl VARCHAR NOT NULL,
    bezug VARCHAR ARRAY NOT NULL,
    PRIMARY KEY (parent_id, child_index),
    FOREIGN KEY (parent_id) REFERENCES tree_node (id) ON DELETE CASCADE,
    FOREIGN KEY (child_id) REFERENCES tree_node (id) ON DELETE RESTRICT
);
CREATE TABLE schema_file (
    filename VARCHAR PRIMARY KEY,
    content VARCHAR NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    schema_fim_id VARCHAR NOT NULL,
    schema_fim_version VARCHAR NOT NULL,
    FOREIGN KEY (schema_fim_id, schema_fim_version) REFERENCES schema (fim_id, fim_version) ON DELETE CASCADE
);
CREATE TABLE code_list (
    id SERIAL PRIMARY KEY,
    genericode_canonical_version_uri VARCHAR NOT NULL,
    genericode_canonical_uri VARCHAR NOT NULL,
    genericode_version VARCHAR,
    is_external BOOLEAN NOT NULL GENERATED ALWAYS AS (content IS NULL) STORED,
    content VARCHAR
);
CREATE TABLE schema_file_code_list (
    code_list_id INTEGER,
    schema_filename VARCHAR,
    PRIMARY KEY (code_list_id, schema_filename),
    FOREIGN KEY (schema_filename) REFERENCES schema_file(filename) ON DELETE CASCADE,
    FOREIGN KEY (code_list_id) REFERENCES code_list(id) ON DELETE RESTRICT
);
CREATE TABLE json_schema_file (
    filename VARCHAR PRIMARY KEY,
    content VARCHAR NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    generated_from VARCHAR NOT NULL,
    canonical_hash VARCHAR NOT NULL,
    FOREIGN KEY (generated_from) REFERENCES schema_file(filename) ON DELETE CASCADE
);
CREATE TABLE xsd_file (
    filename VARCHAR PRIMARY KEY,
    content VARCHAR NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    generated_from VARCHAR NOT NULL,
    FOREIGN KEY (generated_from) REFERENCES schema_file(filename) ON DELETE CASCADE
);
CREATE TABLE steckbrief_file (
    filename VARCHAR PRIMARY KEY,
    content VARCHAR NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    steckbrief_fim_id VARCHAR NOT NULL,
    steckbrief_fim_version VARCHAR NOT NULL,
    FOREIGN KEY (steckbrief_fim_id, steckbrief_fim_version) REFERENCES dokumentensteckbrief(fim_id, fim_version) ON DELETE CASCADE
)
