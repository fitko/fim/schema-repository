-- Rules:
-- These mutable columns are all dropped for xdf3, but are kept for xdf2 compatiblity.
ALTER TABLE regel ADD COLUMN freigabe_status SMALLINT DEFAULT NULL;
ALTER TABLE regel ALTER COLUMN freigabe_status DROP DEFAULT; 

ALTER TABLE regel ADD COLUMN status_gesetzt_am DATE DEFAULT NULL;
ALTER TABLE regel ALTER COLUMN status_gesetzt_am DROP DEFAULT;

ALTER TABLE regel RENAME COLUMN xdf2_veroeffentlichungsdatum TO veroeffentlichungsdatum;

-- Add the list of relations to Dokumentsteckbriefe, Datenschemas, Datenfeldgruppen and Datenfelder

CREATE TABLE dokumentsteckbrief_relations (
    source_fim_id TEXT NOT NULL,
    source_fim_version TEXT NOT NULL,
    target_fim_id TEXT NOT NULL,
    target_fim_version TEXT,
    praedikat TEXT NOT NULL,
    PRIMARY KEY (source_fim_id, source_fim_version, target_fim_id, target_fim_version, praedikat),
    FOREIGN KEY (source_fim_id, source_fim_version) REFERENCES dokumentensteckbrief (fim_id, fim_version) ON DELETE CASCADE
);

CREATE TABLE schema_relations (
    source_fim_id TEXT NOT NULL,
    source_fim_version TEXT NOT NULL,
    target_fim_id TEXT NOT NULL,
    target_fim_version TEXT,
    praedikat TEXT NOT NULL,
    PRIMARY KEY (source_fim_id, source_fim_version, target_fim_id, target_fim_version, praedikat),
    FOREIGN KEY (source_fim_id, source_fim_version) REFERENCES schema (fim_id, fim_version) ON DELETE CASCADE
);

CREATE TABLE datenfeldgruppe_relations (
    source_namespace TEXT NOT NULL,
    source_fim_id TEXT NOT NULL,
    source_fim_version TEXT NOT NULL,
    target_fim_id TEXT NOT NULL,
    target_fim_version TEXT,
    praedikat TEXT NOT NULL,
    PRIMARY KEY (source_fim_id, source_fim_version, target_fim_id, target_fim_version, praedikat),
    FOREIGN KEY (source_namespace, source_fim_id, source_fim_version) REFERENCES datenfeldgruppe (namespace, fim_id, fim_version) ON DELETE CASCADE
);

CREATE TABLE datenfeld_relations (
    source_namespace TEXT NOT NULL,
    source_fim_id TEXT NOT NULL,
    source_fim_version TEXT NOT NULL,
    target_fim_id TEXT NOT NULL,
    target_fim_version TEXT,
    praedikat TEXT NOT NULL,
    PRIMARY KEY (source_fim_id, source_fim_version, target_fim_id, target_fim_version, praedikat),
    FOREIGN KEY (source_namespace, source_fim_id, source_fim_version) REFERENCES datenfeld (namespace, fim_id, fim_version) ON DELETE CASCADE
);

