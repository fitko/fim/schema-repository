-- Add this as nullable for now to allow backfilling the missing redaktionen
ALTER TABLE leistung ADD COLUMN redaktion_id VARCHAR;

CREATE UNIQUE INDEX idx_leistung_redaktion_id ON leistung (redaktion_id, id);

