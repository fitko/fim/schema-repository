-- Add these as nullable for now to allow backfilling

ALTER TABLE schema
    ADD COLUMN veroeffentlichungsdatum DATE;

ALTER TABLE datenfeld
    ADD COLUMN bezug VARCHAR ARRAY,  -- later NOT NULL
    ADD COLUMN freigabe_status SMALLINT,  -- later NOT NULL
    ADD COLUMN status_gesetzt_am DATE,
    ADD COLUMN gueltig_ab DATE,
    ADD COLUMN gueltig_bis DATE,
    ADD COLUMN versionshinweis VARCHAR,
    ADD COLUMN veroeffentlichungsdatum DATE,
    ADD COLUMN letzte_aenderung TIMESTAMP WITH TIME ZONE,  -- later NOT NULL
    ADD COLUMN last_update TIMESTAMP WITH TIME ZONE;  -- later NOT NULL

ALTER TABLE datenfeldgruppe
    ADD COLUMN bezug VARCHAR ARRAY,  -- later NOT NULL
    ADD COLUMN freigabe_status SMALLINT,  -- later NOT NULL
    ADD COLUMN status_gesetzt_am DATE,
    ADD COLUMN gueltig_ab DATE,
    ADD COLUMN gueltig_bis DATE,
    ADD COLUMN versionshinweis VARCHAR,
    ADD COLUMN veroeffentlichungsdatum DATE,
    ADD COLUMN letzte_aenderung TIMESTAMP WITH TIME ZONE,  -- later NOT NULL
    ADD COLUMN last_update TIMESTAMP WITH TIME ZONE;  -- later NOT NULL
