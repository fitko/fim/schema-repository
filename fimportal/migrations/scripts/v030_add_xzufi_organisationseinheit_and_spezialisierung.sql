ALTER TABLE raw_pvog_leistung RENAME TO raw_pvog_resource;
ALTER TABLE raw_pvog_resource ADD COLUMN resource_class TEXT;
UPDATE raw_pvog_resource SET resource_class = 'leistung';
ALTER TABLE raw_pvog_resource ALTER COLUMN resource_class SET NOT NULL;
ALTER TABLE raw_pvog_resource DROP CONSTRAINT raw_pvog_leistung_pkey; 
ALTER TABLE raw_pvog_resource RENAME COLUMN leistung_id TO id;
ALTER TABLE raw_pvog_resource ADD PRIMARY KEY (resource_class, redaktion_id, id);

ALTER TABLE leistung ADD COLUMN source TEXT NOT NULL DEFAULT 'pvog';
UPDATE leistung SET source = 'xzufi' WHERE redaktion_id = 'TSA_TELEPORT';
ALTER TABLE leistung ALTER COLUMN source DROP DEFAULT;
CREATE INDEX leistung_source_idx ON leistung (source);

CREATE TABLE xzufi_organisationseinheit (
    redaktion_id TEXT,
    id TEXT,
    xml_content TEXT NOT NULL,

    PRIMARY KEY (redaktion_id, id)
);

CREATE TABLE xzufi_spezialisierung (
    redaktion_id TEXT,
    id TEXT,
    leistung_id TEXT NOT NULL,
    leistung_redaktion_id TEXT NOT NULL,
    xml_content TEXT NOT NULL,

    PRIMARY KEY (redaktion_id, id)
);

