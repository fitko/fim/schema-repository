ALTER TABLE leistung ADD COLUMN leistungstyp VARCHAR;

-- Set all types to Leistungsbeschreibung to avoid a second migration later to add the NOT NULL constraint.
ALTER TABLE leistung ADD COLUMN hierarchy_type VARCHAR NOT NULL DEFAULT 'beschreibung';
ALTER TABLE leistung ALTER COLUMN hierarchy_type DROP DEFAULT;

-- There belong potentially multiple entries in this table to a single Leistungsschluessel. This group of entries
-- should have a single root. This root is either a Steckbrief/Stammtext or a Leistungsbeschreibung where no Steckbrief/Stammtext exists.
ALTER TABLE leistung ADD COLUMN root_for_leistungsschluessel VARCHAR;
ALTER TABLE leistung ADD CONSTRAINT leistung_unique_root_for_leistungsschluessel UNIQUE (root_for_leistungsschluessel);

-- LeiKa-IDs/LeiKa-Referezen/LeiKa-Schluessel should just be called Leistungsschluessel, as there is no
-- guarantee that they are actually in the LeiKa.
ALTER TABLE leistung RENAME COLUMN leika_referenzen TO leistungsschluessel;
