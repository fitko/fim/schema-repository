ALTER TABLE datenfeldgruppe ADD COLUMN xml_content VARCHAR;
ALTER TABLE datenfeldgruppe ADD COLUMN xml_content_fts tsvector;
CREATE INDEX idx_datenfeldgruppe_xml_content_fts ON datenfeldgruppe USING GIN(xml_content_fts);

ALTER TABLE datenfeld ADD COLUMN xml_content VARCHAR;
ALTER TABLE datenfeld ADD COLUMN xml_content_fts tsvector;
CREATE INDEX idx_datenfeld_xml_content_fts ON datenfeld USING GIN(xml_content_fts);

CREATE FUNCTION schema_element_xml_to_tsvector(xml_data varchar) RETURNS tsvector AS $$
	DECLARE
		content text;
		ns_array text[][];
BEGIN
	ns_array := ARRAY[ARRAY['ns0', 'urn:xoev-de:fim:standard:xdatenfelder_2'], ARRAY['ns0', 'urn:xoev-de:fim:standard:xdatenfelder_3.0.0']];
	content := array_to_string(xpath('//text()', xmlparse(DOCUMENT xml_data), ns_array), ' ');
    RETURN to_tsvector('german', content);
END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION schema_element_tsvector_trigger() RETURNS trigger AS $$
DECLARE
BEGIN 
  NEW.xml_content_fts := schema_element_xml_to_tsvector(NEW.xml_content);
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trg_datenfeldgruppe_tsvector BEFORE INSERT OR UPDATE ON datenfeldgruppe FOR EACH ROW EXECUTE FUNCTION schema_element_tsvector_trigger();
CREATE TRIGGER trg_datenfeld_tsvector BEFORE INSERT OR UPDATE ON datenfeld FOR EACH ROW EXECUTE FUNCTION schema_element_tsvector_trigger();
