CREATE TABLE xzufi_onlinedienst (
    redaktion_id TEXT,
    id TEXT,
    xml_content TEXT NOT NULL,

    PRIMARY KEY (redaktion_id, id)
);