ALTER TABLE schema ADD COLUMN immutable_json JSONB;  -- later NOT NULL
ALTER TABLE schema ADD COLUMN immutable_hash TEXT;  -- later NOT NULL

ALTER TABLE datenfeldgruppe ADD COLUMN immutable_json JSONB;  -- later NOT NULL
ALTER TABLE datenfeldgruppe ADD COLUMN immutable_hash TEXT;  -- later NOT NULL

ALTER TABLE datenfeld ADD COLUMN immutable_json JSONB;  -- later NOT NULL
ALTER TABLE datenfeld ADD COLUMN immutable_hash TEXT;  -- later NOT NULL

ALTER TABLE dokumentensteckbrief ADD COLUMN immutable_json JSONB;  -- later NOT NULL
ALTER TABLE dokumentensteckbrief ADD COLUMN immutable_hash TEXT;  -- later NOT NULL

ALTER TABLE regel ADD COLUMN immutable_json JSONB;  -- later NOT NULL
ALTER TABLE regel ADD COLUMN immutable_hash TEXT;  -- later NOT NULL
