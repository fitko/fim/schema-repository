CREATE TABLE leistung (
    id VARCHAR PRIMARY KEY,
    typisierung VARCHAR ARRAY NOT NULL,
    leika_referenzen VARCHAR ARRAY NOT NULL,
    bezeichnung VARCHAR NOT NULL,
    kurztext VARCHAR,
    volltext VARCHAR,
    xml_content VARCHAR NOT NULL
);
