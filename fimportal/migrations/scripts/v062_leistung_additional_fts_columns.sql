ALTER TABLE leistung ADD COLUMN leistungsbezeichnung_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', leistungsbezeichnung)) STORED;
ALTER TABLE leistung ADD COLUMN leistungsbezeichnung_2_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', leistungsbezeichnung_2)) STORED;
ALTER TABLE leistung ADD COLUMN rechtsgrundlagen_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', rechtsgrundlagen)) STORED;

-- Postgres assumes array_to_string to be not immutable, however this is only due environment settings during database creation time.
-- These settings are inherited by operating system of machine the database is running on, e.g. locale. Since we're not using a multi-region
-- we're not affected by that. Therefore this wrapper simply declares an immutable function that is array_to_string effectively.
CREATE FUNCTION immutable_array_to_string(text[], text) RETURNS text
LANGUAGE sql IMMUTABLE STRICT AS
$$
SELECT array_to_string($1, $2);
$$;

ALTER TABLE schema ADD COLUMN bezug_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', immutable_array_to_string(bezug, ' '))) STORED;
ALTER TABLE schema ADD COLUMN raw_bezug text GENERATED ALWAYS AS (immutable_array_to_string(bezug, ' ')) STORED;
ALTER TABLE schema ADD COLUMN status_gesetzt_durch_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', status_gesetzt_durch)) STORED;
ALTER TABLE schema ADD COLUMN versionshinweis_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', versionshinweis)) STORED;
ALTER TABLE schema ADD COLUMN stichwort_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', immutable_array_to_string(stichwort, ' '))) STORED;
ALTER TABLE schema ADD COLUMN raw_stichwort text GENERATED ALWAYS AS (immutable_array_to_string(stichwort, ' ')) STORED;

ALTER TABLE dokumentensteckbrief ADD COLUMN bezug_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', immutable_array_to_string(bezug, ' '))) STORED;
ALTER TABLE dokumentensteckbrief ADD COLUMN raw_bezug text GENERATED ALWAYS AS (immutable_array_to_string(bezug, ' ')) STORED;
ALTER TABLE dokumentensteckbrief ADD COLUMN status_gesetzt_durch_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', status_gesetzt_durch)) STORED;
ALTER TABLE dokumentensteckbrief ADD COLUMN versionshinweis_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', versionshinweis)) STORED;

ALTER TABLE datenfeldgruppe ADD COLUMN bezug_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', immutable_array_to_string(bezug, ' '))) STORED;
ALTER TABLE datenfeldgruppe ADD COLUMN raw_bezug text GENERATED ALWAYS AS (immutable_array_to_string(bezug, ' ')) STORED;
ALTER TABLE datenfeldgruppe ADD COLUMN status_gesetzt_durch_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', status_gesetzt_durch)) STORED;
ALTER TABLE datenfeldgruppe ADD COLUMN versionshinweis_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', versionshinweis)) STORED;

ALTER TABLE datenfeld ADD COLUMN bezug_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', immutable_array_to_string(bezug, ' '))) STORED;
ALTER TABLE datenfeld ADD COLUMN raw_bezug text GENERATED ALWAYS AS (immutable_array_to_string(bezug, ' ')) STORED;
ALTER TABLE datenfeld ADD COLUMN status_gesetzt_durch_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', status_gesetzt_durch)) STORED;
ALTER TABLE datenfeld ADD COLUMN versionshinweis_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', versionshinweis)) STORED;