CREATE FUNCTION schema_xml_to_tsvector(xml_data varchar) RETURNS tsvector AS $$
	DECLARE
		content text;
		ns_array text[][];
BEGIN
	ns_array := ARRAY[ARRAY['xdf', 'urn:xoev-de:fim:standard:xdatenfelder_2'], ARRAY['xdf', 'urn:xoev-de:fim:standard:xdatenfelder_3.0.0']];
	content := array_to_string(xpath('//text()', xmlparse(DOCUMENT xml_data), ns_array), ' ');
    RETURN to_tsvector('german', content);
END;
$$ LANGUAGE plpgsql;

ALTER TABLE schema ADD COLUMN content_fts tsvector;

CREATE INDEX idx_schema_content_fts ON schema USING GIN(content_fts);

UPDATE schema SET content_fts = schema_xml_to_tsvector(schema_file.content)
FROM (
	SELECT schema_fim_id, schema_fim_version, content FROM schema_file WHERE (schema_fim_id, schema_fim_version, created_at) IN (
        SELECT schema_fim_id, schema_fim_version,  MAX(created_at)
        FROM schema_file
        GROUP BY schema_fim_id, schema_fim_version
    )
) as schema_file
WHERE schema.fim_id = schema_file.schema_fim_id AND schema.fim_version = schema_file.schema_fim_version;


CREATE FUNCTION update_schema_fts_content() RETURNS trigger AS $$
DECLARE
  transformed_content tsvector;
BEGIN 
  transformed_content := schema_xml_to_tsvector(NEW.content);
  UPDATE schema SET content_fts = transformed_content WHERE fim_id = NEW.schema_fim_id AND fim_version = NEW.schema_fim_version;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trg_schema_tsvector BEFORE INSERT OR UPDATE ON schema_file FOR EACH ROW EXECUTE FUNCTION update_schema_fts_content();

-- Ultimately one can assume that there is no schema without a schema_file referencing the schema. However, schema_files are inserted
-- after the schema due to their foreign key constraint. Therefore content_fts is NULL until the schema_file is inserted. 
-- Postgres offers DEFERABLE constraints. That means the constraint is only evaluated at the end of the transaction. This would make the 
-- NOT NULL constraint applicable. Nonetheless it would require to explicitly set the deferability status of each constraint within the
-- transaction.

-- ALTER TABLE schema ALTER COLUMN content_fts SET NOT NULL;