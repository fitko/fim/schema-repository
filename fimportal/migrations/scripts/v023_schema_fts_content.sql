
ALTER TABLE schema ADD COLUMN xml_content TEXT;

UPDATE schema SET xml_content = schema_file.content
FROM (
	SELECT schema_fim_id, schema_fim_version, content FROM schema_file WHERE (schema_fim_id, schema_fim_version, created_at) IN (
        SELECT schema_fim_id, schema_fim_version,  MAX(created_at)
        FROM schema_file
        GROUP BY schema_fim_id, schema_fim_version
    )
) as schema_file
WHERE schema.fim_id = schema_file.schema_fim_id AND schema.fim_version = schema_file.schema_fim_version;

CREATE FUNCTION update_schema_content() RETURNS trigger AS $$
DECLARE
    xml_content TEXT;
BEGIN
    UPDATE schema SET xml_content = NEW.content WHERE fim_id = NEW.schema_fim_id AND fim_version = NEW.schema_fim_version;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trg_schema_content BEFORE INSERT OR UPDATE ON schema_file FOR EACH ROW EXECUTE FUNCTION update_schema_content();

-- Adding FTS for document profiles

CREATE FUNCTION steckbrief_xml_to_tsvector(xml_data varchar) RETURNS tsvector AS $$
	DECLARE
		content text;
		ns_array text[][];
BEGIN
	ns_array := ARRAY[ARRAY['xdf', 'urn:xoev-de:fim:standard:xdatenfelder_2'], ARRAY['xdf', 'urn:xoev-de:fim:standard:xdatenfelder_3.0.0']];
	content := array_to_string(xpath('//text()', xmlparse(DOCUMENT xml_data), ns_array), ' ');
    RETURN to_tsvector('german', content);
END;
$$ LANGUAGE plpgsql;

ALTER TABLE dokumentensteckbrief ADD COLUMN xml_content_fts tsvector;

UPDATE dokumentensteckbrief SET xml_content_fts = steckbrief_xml_to_tsvector(xml_content);

ALTER TABLE dokumentensteckbrief ALTER COLUMN xml_content_fts SET NOT NULL;

CREATE INDEX idx_dokumentensteckbrief_xml_content_fts ON dokumentensteckbrief USING GIN(xml_content_fts);

CREATE FUNCTION dokumentensteckbrief_tsvector_trigger() RETURNS trigger AS $$
DECLARE
BEGIN 
  NEW.xml_content_fts := steckbrief_xml_to_tsvector(NEW.xml_content);
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trg_steckbrief_tsvector BEFORE INSERT OR UPDATE ON dokumentensteckbrief FOR EACH ROW EXECUTE FUNCTION dokumentensteckbrief_tsvector_trigger();
