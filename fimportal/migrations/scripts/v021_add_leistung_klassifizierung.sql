-- First create the column with dummy values, then immediately remove the default value, as this must be filled during import.
-- The dummy values will be updated the next time the importer runs.
ALTER TABLE leistung ADD COLUMN klassifizierung TEXT[] NOT NULL DEFAULT ARRAY[]::text[];
ALTER TABLE leistung ALTER COLUMN klassifizierung DROP DEFAULT;

