-- The old setup used two independent triggers to update
-- 1. the raw_content column, which included the full text to be searched
-- 2. the content_fts column, which included the vector representation of the full text
--
-- The following changes will be made:
-- 1. Remove old triggers, indices and columns.
-- 2. Compute tsvector representation based on raw_content.
-- 3. Base indexes on the fts_tsvector columns.

-- 1. Remove old triggers
-- Leistungen
DROP FUNCTION IF EXISTS xzufi_xml_to_tsvector(xml_data varchar) CASCADE;
DROP FUNCTION IF EXISTS leistung_tsvector_trigger() CASCADE;
DROP INDEX idx_leistung_xml_content_fts;
ALTER TABLE leistung DROP COLUMN xml_content_fts;

-- Schemata
DROP FUNCTION IF EXISTS schema_xml_to_tsvector(xml_data varchar) CASCADE;
DROP FUNCTION IF EXISTS update_schema_fts_content() CASCADE;
DROP INDEX idx_schema_content_fts;
ALTER TABLE schema DROP COLUMN content_fts;

-- Field, Groups
DROP FUNCTION IF EXISTS schema_element_xml_to_tsvector(xml_data varchar) CASCADE;
DROP FUNCTION IF EXISTS schema_element_tsvector_trigger() CASCADE;
DROP INDEX idx_datenfeld_xml_content_fts;
DROP INDEX idx_datenfeldgruppe_xml_content_fts;
ALTER TABLE datenfeld DROP COLUMN xml_content_fts;
ALTER TABLE datenfeldgruppe DROP COLUMN xml_content_fts;

-- Steckbriefe
DROP FUNCTION IF EXISTS steckbrief_xml_to_tsvector(xml_data varchar) CASCADE;
DROP FUNCTION IF EXISTS dokumentensteckbrief_tsvector_trigger() CASCADE;
DROP INDEX idx_dokumentensteckbrief_xml_content_fts;
ALTER TABLE dokumentensteckbrief DROP COLUMN xml_content_fts;

-- Prozesse
DROP FUNCTION IF EXISTS prozess_xml_to_tsvector(xml_data varchar) CASCADE;
DROP INDEX idx_prozess_xml_content_fts;
ALTER TABLE prozess DROP COLUMN xml_content_fts;

-- 2. Add computed tsvector columns
ALTER TABLE leistung ADD COLUMN raw_content_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', raw_content)) STORED;
ALTER TABLE schema ADD COLUMN raw_content_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', raw_content)) STORED;
ALTER TABLE datenfeld ADD COLUMN raw_content_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', raw_content)) STORED;
ALTER TABLE datenfeldgruppe ADD COLUMN raw_content_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', raw_content)) STORED;
ALTER TABLE dokumentensteckbrief ADD COLUMN raw_content_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', raw_content)) STORED;
ALTER TABLE prozess ADD COLUMN raw_content_fts tsvector GENERATED ALWAYS AS (to_tsvector('german', raw_content_without_visualization)) STORED;

-- 3. Create new indexes
CREATE INDEX idx_leistung_fts ON leistung USING GIN (raw_content_fts);
CREATE INDEX idx_schema_fts ON schema USING GIN (raw_content_fts);
CREATE INDEX idx_datenfeld_fts ON datenfeld USING GIN (raw_content_fts);
CREATE INDEX idx_datenfeldgruppe_fts ON datenfeldgruppe USING GIN (raw_content_fts);
CREATE INDEX idx_dokumentsteckbrief_fts ON dokumentensteckbrief USING GIN (raw_content_fts);
CREATE INDEX idx_prozess_fts ON prozess USING GIN (raw_content_fts);
