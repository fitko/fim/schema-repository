-- Sorting by leistungsschluessel cannot re-use the gin index, so we need a btree index for that.
CREATE INDEX idx_btree_leistungsschluessel ON leistung USING btree (leistungsschluessel) 
    WHERE (root_for_leistungsschluessel IS NULL);

