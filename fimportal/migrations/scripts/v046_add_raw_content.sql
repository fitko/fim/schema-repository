-- PROZESS
ALTER TABLE prozess ADD COLUMN raw_content_without_visualization TEXT; --Should be NOT NULL but requires backfill due to filtering out visualizations

CREATE FUNCTION prozess_xml_to_raw_content(xml_data TEXT) RETURNS TEXT AS $$
	DECLARE
		content text;
		ns_array text[][];
BEGIN
	ns_array := ARRAY[ARRAY['xprozess', 'http://www.regierung-mv.de/xprozess/2']];
	content := array_to_string(xpath('//text()', xmlparse(DOCUMENT xml_data), ns_array), ' ');
    return content;
END;
$$ LANGUAGE plpgsql;


--Leistung
ALTER TABLE leistung ADD COLUMN raw_content TEXT;
CREATE FUNCTION xzufi_xml_to_raw_content(xml_data TEXT) RETURNS TEXT AS $$
	DECLARE
		content text;
		ns_array text[][];
BEGIN
	ns_array := ARRAY[ARRAY['xzufi', 'http://xoev.de/schemata/xzufi/2_2_0']];
	content := array_to_string(xpath('//text()', xmlparse(DOCUMENT xml_data), ns_array), ' ');
    RETURN content;
END;
$$ LANGUAGE plpgsql;

UPDATE leistung SET raw_content = xzufi_xml_to_raw_content(xml_content);

CREATE FUNCTION leistung_raw_content_trigger() RETURNS trigger AS $$
DECLARE
BEGIN 
  NEW.raw_content := xzufi_xml_to_raw_content(NEW.xml_content);
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trg_leistung_raw_content BEFORE INSERT OR UPDATE ON leistung FOR EACH ROW EXECUTE FUNCTION leistung_raw_content_trigger();

ALTER TABLE leistung ALTER COLUMN raw_content SET NOT NULL;

--Datenfelder
ALTER TABLE datenfeld ADD COLUMN raw_content TEXT;
ALTER TABLE datenfeldgruppe ADD COLUMN raw_content TEXT;
ALTER TABLE schema ADD COLUMN raw_content TEXT;
ALTER TABLE dokumentensteckbrief ADD COLUMN raw_content TEXT;

CREATE FUNCTION schema_element_xml_to_raw_content(xml_data TEXT) RETURNS TEXT AS $$
	DECLARE
		content text;
		ns_array text[][];
BEGIN
	ns_array := ARRAY[ARRAY['ns0', 'urn:xoev-de:fim:standard:xdatenfelder_2'], ARRAY['ns0', 'urn:xoev-de:fim:standard:xdatenfelder_3.0.0']];
	content := array_to_string(xpath('//text()', xmlparse(DOCUMENT xml_data), ns_array), ' ');
    RETURN content;
END;
$$ LANGUAGE plpgsql;

UPDATE datenfeld SET raw_content = schema_element_xml_to_raw_content(xml_content);
ALTER TABLE datenfeld ALTER COLUMN raw_content SET NOT NULL;

UPDATE datenfeldgruppe SET raw_content = schema_element_xml_to_raw_content(xml_content);
ALTER TABLE datenfeldgruppe ALTER COLUMN raw_content SET NOT NULL;

UPDATE schema SET raw_content = schema_element_xml_to_raw_content(xml_content);
-- ALTER TABLE schema ALTER COLUMN raw_content SET NOT NULL;

UPDATE dokumentensteckbrief SET raw_content = schema_element_xml_to_raw_content(xml_content);
ALTER TABLE dokumentensteckbrief ALTER COLUMN raw_content SET NOT NULL;

CREATE FUNCTION schema_element_raw_content_trigger() RETURNS trigger AS $$
DECLARE
BEGIN 
  NEW.raw_content := schema_element_xml_to_raw_content(NEW.xml_content);
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trg_datenfeld_raw_content BEFORE INSERT OR UPDATE ON datenfeld FOR EACH ROW EXECUTE FUNCTION schema_element_raw_content_trigger();
CREATE TRIGGER trg_datenfeldgruppe_raw_content BEFORE INSERT OR UPDATE ON datenfeldgruppe FOR EACH ROW EXECUTE FUNCTION schema_element_raw_content_trigger();
CREATE TRIGGER trg_schema_raw_content BEFORE INSERT OR UPDATE ON schema FOR EACH ROW EXECUTE FUNCTION schema_element_raw_content_trigger();
CREATE TRIGGER trg_dokumentensteckbrief_raw_content BEFORE INSERT OR UPDATE ON dokumentensteckbrief FOR EACH ROW EXECUTE FUNCTION schema_element_raw_content_trigger();