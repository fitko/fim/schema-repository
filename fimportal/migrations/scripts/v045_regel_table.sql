CREATE TABLE regel (
    fim_id TEXT NOT NULL,
    fim_version TEXT NOT NULL,
    xdf_version TEXT NOT NULL,
    xml_content TEXT NOT NULL,
    PRIMARY KEY (fim_id, fim_version)
);

CREATE TABLE schema_regel_relation (
    schema_fim_id TEXT NOT NULL,
    schema_fim_version TEXT NOT NULL,
    regel_fim_id TEXT NOT NULL,
    regel_fim_version TEXT NOT NULL,
    PRIMARY KEY (schema_fim_id, schema_fim_version, regel_fim_id, regel_fim_version),
    FOREIGN KEY (schema_fim_id, schema_fim_version) REFERENCES schema (fim_id, fim_version) ON DELETE CASCADE,
    FOREIGN KEY (regel_fim_id, regel_fim_version) REFERENCES regel (fim_id, fim_version)
);

CREATE TABLE datenfeldgruppe_regel_relation (
    datenfeldgruppe_namespace TEXT NOT NULL,
    datenfeldgruppe_fim_id TEXT NOT NULL,
    datenfeldgruppe_fim_version TEXT NOT NULL,
    regel_fim_id TEXT NOT NULL,
    regel_fim_version TEXT NOT NULL,
    PRIMARY KEY (datenfeldgruppe_namespace, datenfeldgruppe_fim_id, datenfeldgruppe_fim_version, regel_fim_id, regel_fim_version),
    FOREIGN KEY (datenfeldgruppe_namespace, datenfeldgruppe_fim_id, datenfeldgruppe_fim_version) REFERENCES datenfeldgruppe (namespace, fim_id, fim_version) ON DELETE CASCADE,
    FOREIGN KEY (regel_fim_id, regel_fim_version) REFERENCES regel (fim_id, fim_version)
);

CREATE TABLE datenfeld_regel_relation (
    datenfeld_namespace TEXT NOT NULL,
    datenfeld_fim_id TEXT NOT NULL,
    datenfeld_fim_version TEXT NOT NULL,
    regel_fim_id TEXT NOT NULL,
    regel_fim_version TEXT NOT NULL,
    PRIMARY KEY (datenfeld_namespace, datenfeld_fim_id, datenfeld_fim_version, regel_fim_id, regel_fim_version),
    FOREIGN KEY (datenfeld_namespace, datenfeld_fim_id, datenfeld_fim_version) REFERENCES datenfeld (namespace, fim_id, fim_version) ON DELETE CASCADE,
    FOREIGN KEY (regel_fim_id, regel_fim_version) REFERENCES regel (fim_id, fim_version)
);
