ALTER TABLE prozess ADD COLUMN freigabe_status SMALLINT;
ALTER TABLE prozess ADD COLUMN detaillierungsstufe TEXT;
ALTER TABLE prozess ADD COLUMN prozessklasse TEXT NOT NULL DEFAULT 'UNKNOWN';
ALTER TABLE prozess ADD COLUMN verwaltungspolitische_kodierung TEXT; 

CREATE TABLE prozessklasse (
    id TEXT PRIMARY KEY,
    name TEXT NOT NULL,
    version TEXT,
    letzter_aenderungszeitpunkt TIMESTAMP WITH TIME ZONE,
    freigabe_status SMALLINT,
    xml_content TEXT NOT NULL,
    raw_content TEXT NOT NULL,
    raw_content_fts tsvector NOT NULL GENERATED ALWAYS AS (to_tsvector('german', raw_content)) STORED
);

CREATE INDEX idx_prozessklasse_raw_content_fts ON prozessklasse USING GIN(raw_content_fts);

CREATE FUNCTION prozessklasse_xml_to_raw_content(xml_data TEXT) RETURNS TEXT AS $$
	DECLARE
		content text;
		ns_array text[][];
BEGIN
	ns_array := ARRAY[ARRAY['xprozess', 'http://www.regierung-mv.de/xprozess/2']];
	content := array_to_string(xpath('//text()', xmlparse(DOCUMENT xml_data), ns_array), ' ');
    RETURN content;
END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION prozessklasse_xml_content_trigger() RETURNS trigger AS $$
DECLARE
BEGIN 
  NEW.raw_content := prozessklasse_xml_to_raw_content(NEW.xml_content);
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trg_prozessklasse_transform_content BEFORE INSERT OR UPDATE ON prozessklasse FOR EACH ROW EXECUTE FUNCTION prozessklasse_xml_content_trigger();