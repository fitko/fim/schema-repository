-- The xdf3 "FreigabeStatus" for xdf2 fields/groups must only be 6, if "status_gesetzt_am"
-- is not None. All other fields/groups should have "FreigabeStatus" 2 instead.
UPDATE datenfeld
SET freigabe_status = 2
WHERE xdf_version = '2.0'
AND freigabe_status = 6
AND status_gesetzt_am IS NULL;

UPDATE datenfeldgruppe
SET freigabe_status = 2
WHERE xdf_version = '2.0'
AND freigabe_status = 6
AND status_gesetzt_am IS NULL;

