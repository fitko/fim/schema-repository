ALTER TABLE datenfeld
    ALTER COLUMN bezug SET NOT NULL,
    ALTER COLUMN freigabe_status SET NOT NULL,
    ALTER COLUMN letzte_aenderung SET NOT NULL,
    ALTER COLUMN last_update SET NOT NULL;

ALTER TABLE datenfeldgruppe
    ALTER COLUMN bezug SET NOT NULL,
    ALTER COLUMN freigabe_status SET NOT NULL,
    ALTER COLUMN letzte_aenderung SET NOT NULL,
    ALTER COLUMN last_update SET NOT NULL;