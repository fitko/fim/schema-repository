ALTER TABLE gebiet_id DROP CONSTRAINT gebiet_id_pkey;
ALTER TABLE gebiet_id DROP COLUMN last_update;
ALTER TABLE gebiet_id ADD COLUMN urn TEXT NOT NULL;
ALTER TABLE gebiet_id ADD COLUMN version DATE NOT NULL; 
ALTER TABLE gebiet_id ADD PRIMARY KEY (urn, id);