
-- Use the built-it text type to avoid implicit conversions during filtering.
ALTER TABLE leistung ALTER COLUMN leistungsschluessel SET DATA TYPE text[] USING leistungsschluessel::text[];
ALTER TABLE leistung ALTER COLUMN hierarchy_type SET DATA TYPE text USING hierarchy_type::text;
ALTER TABLE leistung ALTER COLUMN redaktion_id SET DATA TYPE text USING redaktion_id::text;
ALTER TABLE leistung ALTER COLUMN root_for_leistungsschluessel SET DATA TYPE text USING root_for_leistungsschluessel::text;

-- Create a partial index over the Leistungsschluessel array only for Leistungsbeschreibungen.
-- This index is specifically made for the search of related Leistungsbeschreibungen for a spezific Leistungsschluessel.
CREATE INDEX idx_leistung_leistungsschluessel ON leistung USING gin (leistungsschluessel array_ops)
	WHERE hierarchy_type = 'beschreibung';
