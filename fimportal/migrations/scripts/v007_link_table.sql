CREATE TABLE link (
    uri VARCHAR PRIMARY KEY,
    last_checked TIMESTAMP WITH TIME ZONE,
    last_online TIMESTAMP WITH TIME ZONE
);

CREATE TABLE link_leistung_relation(
    uri VARCHAR NOT NULL,
    redaktion VARCHAR NOT NULL,
    leistung VARCHAR NOT NULL,
    PRIMARY KEY (uri, redaktion, leistung),
    FOREIGN KEY (uri) REFERENCES link (uri) ON DELETE CASCADE,
    FOREIGN KEY (redaktion, leistung) REFERENCES leistung (redaktion_id, id) ON DELETE CASCADE
);