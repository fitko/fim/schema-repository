DROP TABLE download;

CREATE TABLE katalog (
    filename TEXT PRIMARY KEY,
    source TEXT NOT NULL,
    content BYTEA NOT NULL,
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL 
);
