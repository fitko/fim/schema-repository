-- The actual timestamp we imported the leistung first.
ALTER TABLE leistung ADD COLUMN created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now();

-- The timestamp according to the file itself when it was created and updated.
ALTER TABLE leistung ADD COLUMN erstellt_datum_zeit TIMESTAMP WITH TIME ZONE DEFAULT NULL;
ALTER TABLE leistung ADD COLUMN geaendert_datum_zeit TIMESTAMP WITH TIME ZONE DEFAULT NULL;

-- Drop defaults and require them explicitly during import.
ALTER TABLE leistung ALTER COLUMN erstellt_datum_zeit DROP DEFAULT;
ALTER TABLE leistung ALTER COLUMN geaendert_datum_zeit DROP DEFAULT;

