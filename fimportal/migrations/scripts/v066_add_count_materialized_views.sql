CREATE MATERIALIZED VIEW xzufi_zustaendigkeit_count AS SELECT COUNT(*) AS total FROM xzufi_zustaendigkeit;
CREATE MATERIALIZED VIEW xzufi_spezialisierung_count AS SELECT COUNT(*) AS total FROM xzufi_spezialisierung;
CREATE MATERIALIZED VIEW xzufi_organisationseinheit_count AS SELECT COUNT(*) AS total FROM xzufi_organisationseinheit;
CREATE MATERIALIZED VIEW xzufi_onlinedienst_count AS SELECT COUNT(*) AS total FROM xzufi_onlinedienst;