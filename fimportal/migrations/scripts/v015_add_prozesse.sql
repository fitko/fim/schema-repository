CREATE TABLE prozess (
    id VARCHAR PRIMARY KEY,
    version VARCHAR,
    name VARCHAR NOT NULL,
    xml_content VARCHAR NOT NULL,
    xml_content_fts tsvector NOT NULL
);

CREATE INDEX idx_prozess_xml_content_fts ON prozess USING GIN(xml_content_fts);

CREATE FUNCTION prozess_xml_to_tsvector(xml_data varchar) RETURNS tsvector AS $$
	DECLARE
		content text;
		ns_array text[][];
BEGIN
	ns_array := ARRAY[ARRAY['xprozess', 'http://www.regierung-mv.de/xprozess/2']];
	content := array_to_string(xpath('//text()', xmlparse(DOCUMENT xml_data), ns_array), ' ');
    RETURN to_tsvector('german', content);
END;
$$ LANGUAGE plpgsql;