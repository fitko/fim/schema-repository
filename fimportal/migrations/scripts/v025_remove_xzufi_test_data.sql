-- Remove old data from the test systems
DELETE FROM leistung WHERE redaktion_id = 'TSA_TELEPORT';

-- Remove all leistungen from the Bundesredaktion once, as those are now imported
-- from the LeiKa Redaktion directly, not from the PVOG.
DELETE FROM leistung WHERE redaktion_id = 'B100019';
