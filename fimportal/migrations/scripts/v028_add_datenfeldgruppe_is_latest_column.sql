ALTER TABLE datenfeldgruppe ADD COLUMN is_latest BOOL NOT NULL DEFAULT FALSE;

-- Add unique constraint (only allow one IS_LATEST = TRUE per (namespace, fim_id) pair)
CREATE UNIQUE INDEX idx_unique_is_latest_per_namespace_fim_id_datenfeldgruppe
ON datenfeldgruppe (namespace, fim_id)
WHERE is_latest = TRUE;


-- Automatically update is_latest

CREATE FUNCTION update_latest_datenfeldgruppe(p_namespace TEXT, p_fim_id TEXT)
RETURNS void AS $$
BEGIN
    -- Set all rows for the same namespace and fim_id to FALSE
    UPDATE datenfeldgruppe
    SET is_latest = FALSE
    WHERE namespace = p_namespace 
        AND fim_id = p_fim_id;

    -- Set the row with the highest fim_version to TRUE
    UPDATE datenfeldgruppe
    SET is_latest = TRUE
    WHERE namespace = p_namespace
      AND fim_id = p_fim_id
      AND fim_version = (
          SELECT fim_version
          FROM datenfeldgruppe
          WHERE namespace = p_namespace
            AND fim_id = p_fim_id
          ORDER BY string_to_array(fim_version, '.')::int[] DESC
          LIMIT 1
      );
END;
$$ LANGUAGE plpgsql;


CREATE FUNCTION call_update_latest_datenfeldgruppe()
RETURNS TRIGGER AS $$
BEGIN

    PERFORM update_latest_datenfeldgruppe(NEW.namespace, NEW.fim_id);

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER trg_update_is_latest
AFTER INSERT ON datenfeldgruppe
FOR EACH ROW
EXECUTE FUNCTION call_update_latest_datenfeldgruppe();


-- Update on migration
DO $$
DECLARE
    rec RECORD;
BEGIN
    FOR rec IN
        SELECT DISTINCT namespace, fim_id
        FROM datenfeldgruppe
    LOOP
        PERFORM update_latest_datenfeldgruppe(rec.namespace, rec.fim_id);
    END LOOP;
END;
$$;