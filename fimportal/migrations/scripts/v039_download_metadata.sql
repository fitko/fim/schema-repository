CREATE TABLE download (
    url TEXT PRIMARY KEY,
    name TEXT NOT NULL,
    content BYTEA NOT NULL,
    source TEXT NOT NULL
);