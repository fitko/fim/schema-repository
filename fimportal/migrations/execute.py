import os, logging, pathlib
from asyncpg import Connection
from hashlib import sha256

logger = logging.getLogger(__name__)


def get_hash(value: str):
    return sha256(value.encode()).hexdigest()


async def _ensure_migration_table(connection: Connection):
    await connection.execute(
        """
        CREATE TABLE IF NOT EXISTS migrations (
            id SERIAL PRIMARY KEY,
            migration_file TEXT NOT NULL,
            migration_sha TEXT NOT NULL
        );
        """
    )


async def _insert_migration(
    index: int, migration_file: str, migration_hash: str, connection: Connection
):
    await connection.execute(
        """
        INSERT INTO migrations VALUES ($1, $2, $3)
        """,
        index,
        migration_file,
        migration_hash,
    )


async def _get_migration_content_sha(index: int, connection: Connection) -> str | None:
    return await connection.fetchval(
        """
        SELECT migration_sha FROM migrations WHERE id = $1 
        """,
        index,
    )


async def _validate_migration_filename(
    index: int, filename: str, connection: Connection
) -> bool:
    result = await connection.fetchval(
        """
        SELECT migration_file FROM migrations WHERE id = $1
        """,
        index,
    )
    if result is None:
        return True
    else:
        return result == filename


async def _migrate(migration_files: list[str], connection: Connection):
    await _ensure_migration_table(connection)
    for index, migration_filepath in enumerate(migration_files):
        with open(migration_filepath, "r") as migration:
            migration_content = migration.read()
        migration_filename = os.path.basename(migration_filepath)
        if not await _validate_migration_filename(
            index, migration_filename, connection
        ):
            raise Exception(
                f"Invalid Migration filename at index: {index} | Name: {migration_filename}"
            )
        migration_hash = get_hash(migration_content)
        existing_sha = await _get_migration_content_sha(index, connection)
        if existing_sha is None:
            await _insert_migration(
                index, migration_filename, migration_hash, connection
            )
            try:
                await connection.execute(migration_content)
            except Exception as ex:
                logger.info(f"Failed executing Migration: {migration_filename}")
                raise ex
            logger.info(f"Migration {migration_filename} executed.")
        elif existing_sha != migration_hash:
            raise Exception(
                f"Invalid Migration Content at index: {index} | Name: {migration_filename}"
            )
        else:
            logger.info(f"Migration {migration_filename} executed before.")


async def execute_migrations(connection: Connection):
    """
    This folder contains all logic pertaining to database migrations.
    The actual migration files can be found in `/scripts`.
    They are ordered by their filename and validated against a migration table when being executed.
    Whenever a migration is executed it's checked against the corresponding persisted migration and
    if there are any content changes, it's rejected. A new migration is always persisted and afterwards executed.
    """
    migrations_path = os.path.join(pathlib.Path(__file__).parent, "scripts")
    migration_files = [
        f"{migrations_path}/{f}"
        for f in os.listdir(migrations_path)
        if os.path.isfile(os.path.join(migrations_path, f))
    ]

    async with connection.transaction():
        await _migrate(sorted(migration_files), connection)
