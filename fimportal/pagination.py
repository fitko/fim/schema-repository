from __future__ import annotations
from dataclasses import dataclass
from typing import TypeVar, Generic, Callable, Annotated
import math

from fastapi import Query
from pydantic import BaseModel


class PaginationOptions(BaseModel):
    offset: int
    limit: int


T = TypeVar("T")
S = TypeVar("S")


@dataclass(slots=True)
class AvailablePages:
    current_page: int
    last_page: int
    previous_pages: list[int]
    next_pages: list[int]


class PaginatedResult(BaseModel, Generic[T]):
    items: list[T]
    offset: int
    limit: int
    count: int
    total_count: int

    @classmethod
    def create(
        cls,
        items: list[T],
        pagination_options: PaginationOptions | None,
        total_count: int,
    ) -> PaginatedResult[T]:
        if pagination_options is None:
            assert len(items) == 0
            offset = 0
            limit = 0
        else:
            offset = pagination_options.offset
            limit = pagination_options.limit

        return PaginatedResult(
            items=items,
            offset=offset,
            limit=limit,
            count=len(items),
            total_count=total_count,
        )

    def map(self, map_item: Callable[[T], S]) -> PaginatedResult[S]:
        return PaginatedResult(
            items=[map_item(item) for item in self.items],
            offset=self.offset,
            limit=self.limit,
            count=self.count,
            total_count=self.total_count,
        )

    def get_available_pages(self) -> AvailablePages:
        current_page = math.floor(self.offset / self.limit) + 1
        last_page = math.ceil(self.total_count / self.limit)

        previous_pages: list[int] = []
        next_pages: list[int] = []
        for i in range(1, 4):
            previous_page = current_page - i
            if previous_page >= 1:
                previous_pages.append(previous_page)

            next_page = current_page + i
            if next_page <= last_page:
                next_pages.append(next_page)

        previous_pages.reverse()

        return AvailablePages(
            current_page=current_page,
            last_page=last_page,
            previous_pages=previous_pages,
            next_pages=next_pages,
        )


def get_pagination_options(
    offset: Annotated[
        int,
        Query(
            ge=0,
            description="Offset within the total dataset.",
            examples=[0],
        ),
    ] = 0,
    limit: Annotated[
        int,
        Query(
            ge=1,
            le=200,
            description="Count of returned results.",
            examples=[10],
        ),
    ] = 200,
) -> PaginationOptions:
    return PaginationOptions(
        offset=offset,
        limit=limit,
    )


def get_ui_pagination_options(
    page: Annotated[int, Query(ge=1)] = 1,
    page_size: Annotated[int, Query(ge=1, le=200)] = 20,
) -> PaginationOptions:
    return PaginationOptions(
        offset=(page - 1) * page_size,
        limit=page_size,
    )
