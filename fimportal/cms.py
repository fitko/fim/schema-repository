"""
This modules contains data structures and functions to work with
the dynamic data managed in the CMS.

The data is stored in a single JSON file, which is loaded into memory
when the server starts. Saving and loading the data is done through
pydantic models to ensure the data is valid.
"""

from __future__ import annotations

import email.utils
import logging
import os
from dataclasses import dataclass
from datetime import datetime
from enum import Enum
from pathlib import Path
from typing import Any, Literal

import httpx
import lxml.etree
import pydantic

from fimportal.helpers import retry
from fimportal.routers.ui_xzufi import get_icon_url_for_redaktion
from fimportal.rss import create_rss_xml
from fimportal.xzufi.common import LABEL_TO_REDAKTION_ID


logger = logging.getLogger(__name__)


@dataclass
class ImageConfig:
    width: int | None
    height: int | None
    mode: Literal["bestFit", "fitToWidth", "thumbnail"]
    filetype: Literal["webp"]


class PatchedContentAsset(pydantic.BaseModel):
    """
    A patched asset with the final URL.
    """

    url: str
    mime: str


# Bootstrap containers grow to at most 1320px.
# Therefore, all images that will span the full range of the container
# can safely be scaled-down to 1320px.
FULL_WIDTH_IMAGE = ImageConfig(1320, None, "fitToWidth", "webp")


class CMSClient:
    """
    Client to interact with the CMS.

    All regular assets (like downloadable files) will be saved in the assets_dir.
    These files have a stable name and will be overwritten on updates.

    All content-adressable assets (like images on the web page)
    will be saved in the content_assets_dir.
    These files are named based on their hash and will therefore change on updates.

    Splitting the resources into separate directories allows for better caching
    policies.
    """

    client: httpx.Client

    max_retries: int

    assets_dir: Path
    content_assets_dir: Path

    def __init__(
        self,
        base_url: str,
        token: str,
        assets_dir: Path,
        content_assets_dir: Path,
        max_retries: int = 5,
    ):
        self.assets_dir = assets_dir
        self.content_assets_dir = content_assets_dir

        self.max_retries = max_retries
        self.client = httpx.Client(base_url=httpx.URL(base_url))
        self.client.headers["api-key"] = token

    def get_model(self, model: str) -> Any:
        response = self._get(f"/api/content/item/{model}")

        return response.json()

    def get_collection(self, collection: str) -> list[Any]:
        response = self._get(f"/api/content/items/{collection}")

        data = response.json()
        assert isinstance(data, list)

        return data

    def download_file(self, file: DownloadableFile):
        filename = file.filename
        output_file = self.assets_dir / filename

        response = self._get(f"/storage/uploads{file.file.path}")
        output_file.write_bytes(response.content)
        logger.info(f"Downloaded {filename}")

    def patch_content_asset(self, data: dict[str, Any], key: str):
        """
        Download an asset and replace the asset data with the final URL.
        """

        asset = ContentAsset.model_validate(data[key])

        data[key] = self._download_content_asset(asset)

    def _download_content_asset(self, asset: ContentAsset) -> PatchedContentAsset:
        filename = f"{asset.hash}{asset.get_extension()}"
        cache_file = self.content_assets_dir / filename

        if not cache_file.exists():
            response = self._get(f"/storage/uploads{asset.path}")

            cache_file.write_bytes(response.content)
            logger.info(f"Downloaded {filename}")

        return PatchedContentAsset(
            url=f"/content-assets/{filename}",
            mime=asset.mime,
        )

    def patch_image(self, data: dict[str, Any], key: str, config: ImageConfig):
        """
        Download an image and replace the asset data with the final URL.
        """

        asset = ContentAsset.model_validate(data[key])
        image = Image(asset, config)

        data[key] = self._download_image(image)

    def _download_image(self, image: Image) -> PatchedContentAsset:
        filename = f"{image.asset.hash}_{image.config.height or 'auto'}X{image.config.width or 'auto'}.{image.config.filetype}"
        cache_file = self.content_assets_dir / filename

        if not cache_file.exists():
            url = f"/api/assets/image/{image.asset.id}?o=1&mime={image.config.filetype}"

            match image.config.mode:
                case "fitToWidth":
                    assert image.config.height is None
                    assert image.config.width is not None

                    url += f"&m=fitToWidth&w={image.config.width}"
                case _:
                    assert image.config.height is not None
                    assert image.config.width is not None

                    url += f"&m={image.config.mode}&h={image.config.height}&w={image.config.width}"

            response = self._get(url)

            cache_file.write_bytes(response.content)
            logger.info(f"Downloaded {filename}")

        return PatchedContentAsset(
            url=f"/content-assets/{filename}",
            mime=image.config.filetype,
        )

    @retry(3, httpx.ReadTimeout)
    def _get(self, url: str) -> httpx.Response:
        response = self.client.get(url)
        response.raise_for_status()
        return response


class ContentAsset(pydantic.BaseModel):
    id: str = pydantic.Field(alias="_id")
    hash: str = pydantic.Field(alias="_hash")
    path: str
    mime: str
    title: str

    def get_extension(self) -> str:
        return os.path.splitext(self.path)[1]


@dataclass(slots=True)
class Image:
    asset: ContentAsset
    config: ImageConfig

    def get_filename(self) -> str:
        return f"{self.config.width or 'AUTO'}x{self.config.height or 'AUTO'}_{self.asset.hash}.{self.config.filetype}"


class CMSContent(pydantic.BaseModel):
    """
    The full content of the CMS.
    """

    base: BaseInfo
    fim_coaches: FimCoachSection
    articles: list[NewsArticle]
    seminare: list[Seminar]
    legal_pages: list[LegalPage]

    index: IndexPage
    was_ist_fim: WasIstFimPage
    fim_user_journey: list[FimBeispielStep]
    schulung: SchulungPage
    kataloge: KatalogePage
    arbeitsmittel: ArbeitsmittelPage
    news: NewsPage
    kontakt: KontaktPage

    @staticmethod
    def load_from_cms(client: CMSClient) -> CMSContent:
        _load_downloadable_files(client)

        return CMSContent(
            base=BaseInfo.load(client),
            fim_coaches=FimCoachSection.load(client),
            articles=NewsArticle.load(client),
            seminare=Seminar.load(client),
            index=IndexPage.load(client),
            was_ist_fim=WasIstFimPage.load(client),
            fim_user_journey=FimBeispielStep.load(client),
            schulung=SchulungPage.load(client),
            kataloge=KatalogePage.load(client),
            arbeitsmittel=ArbeitsmittelPage.load(client),
            news=NewsPage.load(client),
            kontakt=KontaktPage.load(client),
            legal_pages=LegalPage.load(client),
        )


class DownloadableFile(pydantic.BaseModel):
    filename: str
    file: ContentAsset


def _load_downloadable_files(client: CMSClient):
    results = client.get_collection("downloads")
    files = [DownloadableFile.model_validate(item) for item in results]

    for file in files:
        client.download_file(file)


def create_news_rss_feed(content: CMSContent, baseurl: str) -> str:
    items = [article.create_rss_item(baseurl) for article in content.articles]

    return create_rss_xml("FIM Portal News", "Alle News des FIM Portals", items)


class Link(pydantic.BaseModel):
    label: str
    href: str
    icon: str | None = None


class TitleSection(pydantic.BaseModel):
    """
    Reusable data structure for subpage headers.
    """

    title: str
    text: str | None
    links: list[Link]


class Section(pydantic.BaseModel):
    """
    Reusable data structure for page sections.
    """

    anchor: str
    subtitle: str | None
    title: str
    text: str | None
    link: Link | None


class BaseInfo(pydantic.BaseModel):
    """
    General information about the website.
    Used for every rendered page.
    """

    header_bg_image: PatchedContentAsset
    central_support_email: str
    footer_contact_text: str
    info_box_background_image: PatchedContentAsset

    docs_link: str = "https://docs.fitko.de/fim/docs"

    @staticmethod
    def load(client: CMSClient) -> BaseInfo:
        data = client.get_model("basedata")
        client.patch_image(
            data, "header_bg_image", ImageConfig(3500, None, "fitToWidth", "webp")
        )
        client.patch_image(data, "info_box_background_image", FULL_WIDTH_IMAGE)

        return BaseInfo.model_validate(data)


class NewsArticle(pydantic.BaseModel):
    id: str
    published_at: datetime
    title: str
    teaser: str
    content: str

    @staticmethod
    def load(client: CMSClient) -> list[NewsArticle]:
        items = client.get_collection("News")
        for item in items:
            item["id"] = item["_id"]

        articles = [NewsArticle.model_validate(item) for item in items]

        return sorted(articles, key=lambda x: x.published_at, reverse=True)

    def get_path(self) -> str:
        return f"/news/{self.id}"

    def get_ui_published_at(self) -> str:
        return f"{self.published_at.strftime('%d.%m.%Y, %H:%M')} Uhr"

    def create_rss_item(self, baseurl: str):
        item = lxml.etree.Element("item")

        lxml.etree.SubElement(item, "guid", {"isPermaLink": "false"}).text = self.id
        lxml.etree.SubElement(item, "title").text = self.title
        lxml.etree.SubElement(item, "pubDate").text = email.utils.format_datetime(
            self.published_at
        )
        lxml.etree.SubElement(item, "description").text = self.teaser
        lxml.etree.SubElement(item, "link").text = f"{baseurl}{self.get_path()}"

        # Use the recommended best-practice to encode the HTML content of the item
        # https://www.rssboard.org/rss-profile#namespace-elements-content-encoded
        lxml.etree.SubElement(
            item, "{http://purl.org/rss/1.0/modules/content/}encoded"
        ).text = lxml.etree.CDATA(self.content)

        return item


class BausteinLink(pydantic.BaseModel):
    """
    Frontpage link to the search of a specific Baustein.
    """

    count: int
    baustein_titel: str
    link_label: str
    link_url: str


class IndexPage(pydantic.BaseModel):
    title: str = "FIM Portal"

    link_leistungen: BausteinLink
    link_datenfelder: BausteinLink
    link_prozesse: BausteinLink

    was_ist_fim_titel: str
    was_ist_fim_text: str
    was_ist_fim_video_thumbnail: PatchedContentAsset
    was_ist_fim_video: PatchedContentAsset
    was_ist_fim_video_subtitles: PatchedContentAsset
    mit_fim_arbeiten_titel: str
    mit_fim_arbeiten_text: str
    mit_fim_arbeiten_kataloge: str
    mit_fim_arbeiten_arbeitsmittel: str
    mit_fim_arbeiten_dokumentation: str

    news_titel: str
    news_text: str

    schulungen_text: str
    schulungen_titel: str
    schulungen_image: PatchedContentAsset

    ansprechpartner_text_left: str
    ansprechpartner_text_right: str
    ansprechpartner_bg_image: PatchedContentAsset

    @staticmethod
    def load(client: CMSClient) -> IndexPage:
        data = client.get_model("landingpage")

        client.patch_image(
            data, "schulungen_image", ImageConfig(500, 500, "bestFit", "webp")
        )
        client.patch_image(data, "ansprechpartner_bg_image", FULL_WIDTH_IMAGE)
        client.patch_image(data, "was_ist_fim_video_thumbnail", FULL_WIDTH_IMAGE)
        client.patch_content_asset(data, "was_ist_fim_video")
        client.patch_content_asset(data, "was_ist_fim_video_subtitles")
        data["link_leistungen"] = {
            "count": 14438,
            "baustein_titel": "Leistungen",
            "link_label": "Nach Leistungen suchen",
            "link_url": "/search?resource=service",
        }
        data["link_datenfelder"] = {
            "count": 2334,
            "baustein_titel": "Datenfelder",
            "link_label": "Nach Datenfeldern suchen",
            "link_url": "/search?resource=schema",
        }
        data["link_prozesse"] = {
            "count": 334,
            "baustein_titel": "Prozesse",
            "link_label": "Nach Prozessen suchen",
            "link_url": "/search?resource=process",
        }

        page = IndexPage.model_validate(data)

        return page

    def get_mit_fim_arbeiten_section(self) -> Section:
        return Section(
            anchor="mit-fim-arbeiten",
            subtitle="Mit FIM arbeiten",
            title=self.mit_fim_arbeiten_titel,
            text=self.mit_fim_arbeiten_text,
            link=None,
        )

    def get_news_section(self) -> Section:
        return Section(
            anchor="news",
            subtitle="FIM News",
            title=self.news_titel,
            text=self.news_text,
            link=None,
        )

    def get_schulungen_section(self) -> Section:
        return Section(
            anchor="schulungen",
            subtitle="FIM Schulungen",
            title=self.schulungen_titel,
            text=self.schulungen_text,
            link=Link(
                label="Zu den Schulungsangeboten",
                href="/schulung",
            ),
        )


class Seminar(pydantic.BaseModel):
    identifier: str
    titel: str
    teaser: str
    ueberblick: str
    ziele: str
    zielgruppe: str
    teilnahmevoraussetzungen: str
    dauer: str
    teilnehmerzahl: str
    agenda: str
    link_onlineseminar: str | None = None

    @staticmethod
    def load(client: CMSClient) -> list[Seminar]:
        result = client.get_collection("Seminare")

        return [Seminar.model_validate(item) for item in result]

    def get_files(self) -> list[ArbeitsmittelLink]:
        # NOTE(Felix): This should probably be configured via the CMS.
        match self.identifier:
            case "basis":
                return [
                    ArbeitsmittelLink.download(
                        FileIdentifier.SEMINAR_BASIS, "Seminarunterlagen"
                    ),
                    ArbeitsmittelLink.download(
                        FileIdentifier.SEMINAR_BASIS_RAHMENBEDINGUNGEN_ONLINESCHULUNG,
                        "Rahmenbedingungen Online-Schulungen",
                    ),
                ]
            case "expert":
                return [
                    ArbeitsmittelLink.download(
                        FileIdentifier.SEMINAR_EXPERT, "Seminarunterlagen"
                    ),
                ]
            case _:
                raise Exception(f"Got unknown seminar identifier: {self.identifier}")

    def get_path(self) -> str:
        return f"/seminar/{self.identifier}"


class LegalPage(pydantic.BaseModel):
    """
    Model for legal pages like privacy policy and accessibility statement.
    """

    identifier: str
    title: str
    content: str

    @staticmethod
    def load(client: CMSClient) -> list[LegalPage]:
        items = client.get_collection("legal")
        return [LegalPage.model_validate(item) for item in items]


class FimCoach(pydantic.BaseModel):
    position: int
    name: str
    mail: str | None
    website: str | None
    image: PatchedContentAsset | None = None
    active: bool


class FimCoachSection(pydantic.BaseModel):
    title: str
    text: str
    coaches: list[FimCoach]

    @staticmethod
    def load(client: CMSClient) -> FimCoachSection:
        items = client.get_collection("fimcoaches")

        for item in items:
            if item.get("image"):
                client.patch_image(
                    item, "image", ImageConfig(150, 150, "thumbnail", "webp")
                )

        fim_coaches = [FimCoach.model_validate(item) for item in items]
        fim_coaches = sorted(fim_coaches, key=lambda x: x.position)

        data = {
            "title": "Ihre Ansprechpersonen",
            "text": (
                "<p>"
                "Dieses Verzeichnis wird laufend aktualisiert."
                " Die FITKO bemüht sich Änderungen zeitnah zu veröffentlichen."
                " Ihre Fragen und Anmerkungen richten Sie gerne an: <a href='mailto:fim@fitko.de'>fim@fitko.de</a>"
                "</p>"
            ),
            "coaches": fim_coaches,
        }

        return FimCoachSection.model_validate(data)

    def get_section(self) -> Section:
        return Section(
            anchor="fim-coaches",
            subtitle="FIM-Coach Verzeichnis",
            title=self.title,
            text=self.text,
            link=None,
        )


class SchulungPage(pydantic.BaseModel):
    title: str = "Schulung • FIM Portal"

    header_titel: str
    header_text: str | None

    seminare_titel: str
    seminare_text: str | None = None

    fim_coach_titel: str
    fim_coach_text: str
    fim_coach_zertifizierung_titel: str
    fim_coach_zertifizierung_text: str

    @staticmethod
    def load(client: CMSClient) -> SchulungPage:
        data = {}

        data["header_titel"] = "Schulung"
        data["header_text"] = None

        data["seminare_titel"] = "Erlernen Sie den Umgang mit FIM"

        data["fim_coach_titel"] = "Werden Sie Expert:in"
        data["fim_coach_text"] = (
            "<p>FIM-Coaches sind Vertragspartner:innen der FITKO AöR. Sie sind vertraglich berechtigt, das FIM-Logo gewerblich zu nutzen und verpflichtet, eine aktuelle, hochwertige und einheitliche Ausbildung zu gewährleisten. FIM-Coaches sind berechtigt, FIM-Basisseminare, Ausbildungen zu FIM-Methodenexpert:innen und Ausbildungen von FIM-Coaches durchzuführen.</p>"
            "<p>Die FIM-Coaches werden im öffentlichen FIM-Coach-Verzeichnis (siehe unten) gepflegt.</p>"
        )
        data["fim_coach_zertifizierung_titel"] = "Zertifizierung zum FIM-Coach"
        data["fim_coach_zertifizierung_text"] = (
            "<p>Für die Zertifizierung müssen folgende Anforderungen erfüllt sein:</p>"
            "<ol>"
            "<li>Nachweis der Teilnahmebescheinigung des FIM-Basisseminars.</li>"
            "<li>Nachweis der Teilnahmebescheinigung zur Ausbildung zu FIM-Methodenexpert:innen.</li>"
            "<li>Praktische Anwendung aller drei Bausteine: Schriftlicher Nachweis der eigenständig modellierten Stamminformationen aus jedem Baustein.</li>"
            "<li>Durchführung mindestens einer Ausbildung zu FIM-Methodenexpert:innen unter Begleitung eines bereits zertifizierten FIM-Coaches.</li>"
            "<li>Erfolgreiche Bewertung durch die FITKO.</li>"
            "</ol>"
        )

        return SchulungPage.model_validate(data)

    @property
    def fim_coach_ausbildungsrichtlinie_url(self) -> str:
        return FileIdentifier.FIM_COACH_AUSBILDUNGSRICHTLINIE.to_link()

    @property
    def fim_coach_ausbildungsrichtlinie_label(self) -> str:
        return "FIM-Coach Ausbildungsrichtlinie"

    def get_title_section(self) -> TitleSection:
        return TitleSection(
            title=self.header_titel,
            text=self.header_text,
            links=[
                Link(
                    label="Seminare",
                    href="#seminare",
                    icon="arrow-down-circle-fill",
                ),
                Link(
                    label="Ausbildung FIM-Coach",
                    href="#fim-coach",
                    icon="arrow-down-circle-fill",
                ),
                Link(
                    label="FIM-Coach Verzeichnis",
                    href="#fim-coaches",
                    icon="arrow-down-circle-fill",
                ),
            ],
        )

    def get_seminare_section(self) -> Section:
        return Section(
            anchor="seminare",
            subtitle="Seminare",
            title=self.seminare_titel,
            text=self.seminare_text,
            link=None,
        )

    def get_fim_coach_section(self) -> Section:
        return Section(
            anchor="fim-coach",
            subtitle="Ausbildung zum FIM-Coach",
            title=self.fim_coach_titel,
            text=self.fim_coach_text,
            link=None,
        )


class SeminarPage(pydantic.BaseModel):
    title: str
    seminar: Seminar

    @staticmethod
    def from_seminar(seminar: Seminar) -> SeminarPage:
        return SeminarPage(
            title=f"{seminar.titel} • FIM Portal",
            seminar=seminar,
        )

    def get_title_links(self) -> list[Link]:
        links = [
            Link(
                label="Ansprechpersonen",
                href="/fim-coaches",
                icon="arrow-down-circle-fill",
            ),
        ]

        if self.seminar.link_onlineseminar:
            links.append(
                Link(
                    label="Zum Online-Seminar",
                    href=self.seminar.link_onlineseminar,
                    icon="box-arrow-up-right",
                )
            )

        return links


class Baustein(pydantic.BaseModel):
    titel: str
    text: str
    link_label: str
    link: str


class FimBeispielStep(pydantic.BaseModel):
    position: int
    icon: str
    title: str
    content: str

    @staticmethod
    def load(client: CMSClient) -> list[FimBeispielStep]:
        items = client.get_collection("userjourney")

        steps = [FimBeispielStep.model_validate(item) for item in items]
        return sorted(steps, key=lambda x: x.position)


class WasIstFimPage(pydantic.BaseModel):
    title: str = "Über FIM • FIM Portal"

    header_titel: str

    fim_verstehen_titel: str
    fim_verstehen_text: str
    fim_verstehen_list: str
    fim_verstehen_video_thumbnail: PatchedContentAsset
    fim_verstehen_video: PatchedContentAsset
    fim_verstehen_video_subtitles: PatchedContentAsset

    fim_modell_titel: str
    fim_modell_image: PatchedContentAsset
    fim_modell_bund_text: str
    fim_modell_land_text: str
    fim_modell_kommune_text: str

    mehr_erfahren_titel: str
    mehr_erfahren_text: str

    bausteine_titel: str
    bausteine_text: str
    bausteine_text_2: str

    bausteine_leistungen: str
    bausteine_leistungen_link: str
    bausteine_datenfelder: str
    bausteine_datenfelder_link: str
    bausteine_prozesse: str
    bausteine_prozesse_link: str

    example_titel: str
    example_text: str

    @staticmethod
    def load(client: CMSClient) -> WasIstFimPage:
        data = client.get_model("wasistfim")
        client.patch_image(data, "fim_verstehen_video_thumbnail", FULL_WIDTH_IMAGE)
        client.patch_content_asset(data, "fim_verstehen_video")
        client.patch_content_asset(data, "fim_verstehen_video_subtitles")
        client.patch_content_asset(data, "fim_modell_image")

        data["header_titel"] = "Was ist FIM?"

        data["example_titel"] = "FIM am Beispiel einer Verwaltungsleistung"
        data["example_text"] = (
            "Die Nutzerreise am Beispiel der Gewerbeanmeldung zeigt die Schnittpunkte zu FIM in der Praxis."
        )

        return WasIstFimPage.model_validate(data)

    def get_title_section(self) -> TitleSection:
        return TitleSection(
            title=self.header_titel,
            text=None,
            links=[
                Link(
                    label="FIM verstehen",
                    href=f"#{self.get_fim_verstehen_section().anchor}",
                    icon="arrow-down-circle-fill",
                ),
                Link(
                    label="FIM Bausteine",
                    href=f"#{self.get_baustein_section().anchor}",
                    icon="arrow-down-circle-fill",
                ),
                Link(
                    label="Beispiel",
                    href=f"#{self.get_example_section().anchor}",
                    icon="arrow-down-circle-fill",
                ),
            ],
        )

    def get_fim_verstehen_section(self) -> Section:
        return Section(
            anchor="fim-verstehen",
            subtitle="FIM verstehen",
            title=self.fim_verstehen_titel,
            text=self.fim_verstehen_text,
            link=None,
        )

    def get_baustein_section(self) -> Section:
        return Section(
            anchor="fim-bausteine",
            subtitle="FIM Bausteine",
            title=self.bausteine_titel,
            text=self.bausteine_text,
            link=None,
        )

    def get_bausteine(self) -> list[Baustein]:
        return [
            Baustein(
                titel="Leistungen",
                text=self.bausteine_leistungen,
                link_label="XZuFi",
                link=self.bausteine_leistungen_link,
            ),
            Baustein(
                titel="Datenfelder",
                text=self.bausteine_datenfelder,
                link_label="XDatenfelder",
                link=self.bausteine_datenfelder_link,
            ),
            Baustein(
                titel="Prozesse",
                text=self.bausteine_prozesse,
                link_label="XProzess",
                link=self.bausteine_prozesse_link,
            ),
        ]

    def get_example_section(self) -> Section:
        return Section(
            anchor="beispiel",
            subtitle="Anwendungsbeispiel",
            title=self.example_titel,
            text=self.example_text,
            link=None,
        )


class KatalogePage(pydantic.BaseModel):
    title: str = "Kataloge • FIM Portal"

    header_titel: str
    header_text: str | None = None

    @staticmethod
    def load(client: CMSClient) -> KatalogePage:
        data = {}

        data["header_titel"] = "Kataloge"
        data["header_text"] = (
            "Die Kataloge strukturieren die Inhalte der Bausteine Leistungen, Prozesse und Datenfelder."
        )

        return KatalogePage.model_validate(data)

    def get_title_section(self) -> TitleSection:
        return TitleSection(
            title=self.header_titel,
            text=self.header_text,
            links=[],
        )


class FileIdentifier(Enum):
    LEISTUNGEN_QS_KRITERIEN = "leistungen_qs_kriterien.pdf"
    LEISTUNGEN_LEIKA_HANDBUCH = "leistungen_leika_handbuch.pdf"
    LEISTUNGEN_MUSTERFORLUMAR_LEISTUNGEBESCHREIBUNGEN = (
        "leistungen_musterformular_leistungsbeschreibungen.docx"
    )
    LEISTUNGEN_REFERENZLISTE_PFLICHTATTRIBUTE = (
        "leistungen_referenzliste_pflichtattribute.pdf"
    )
    LEISTUNGEN_MUSTERFORMULAR_LEISTUNGSZUSCHNITT = (
        "musterformular_leistungszuschnitt.xlsx"
    )

    DATENFELDER_QS_KRITERIEN_2_0 = "datenfelder_qs_kriterien_2_0.pdf"
    DATENFELDER_MUSTERFORMULAR_DOKUMENTSTECKBRIEF = (
        "datenfelder_musterformular_dokumentsteckbrief.docx"
    )
    DATENFELDER_FACHKONZEPT = "datenfelder_fachkonzept.pdf"
    DATENFELDER_M2M_SCHNITTSTELLE = "datenfelder_m2m_schnittstelle.pdf"

    PROZESSE_QS_KRITERIEN = "prozesse_qs_kriterien.pdf"
    PROZESSE_QS_KRITERIEN_CHECKLISTE = "prozesse_qs_kriterien_checkliste.xlsx"
    PROZESSE_MUSTERFORMULAR_PROZESSINFORMATIONEN = (
        "prozesse_musterformular_prozessinformationen.xlsx"
    )
    PROZESSE_FACHKONZEPT = "prozesse_fachkonzept.pdf"
    PROZESSE_HANDLUNGSANWEISUNG_SAMMELREPOSITORY = (
        "prozesse_handlungsanweisung_sammelrepository.zip"
    )
    PROZESSE_HANDLUNGSANWEISUNG_XPROZESS_2_0_0 = (
        "prozesse_handlungsanweisung_xprozess_2_0_0.zip"
    )
    PROZESSE_AENDERUNGSANTRAG_XPROZESS_2_0_1 = (
        "prozesse_aenderungsantrag_xprozess_2_0_1.zip"
    )
    PROZESSE_KATALOG = "prozesse_katalog.xlsx"

    FIM_PRODUKTHANDBUCH = "fim_konformitaet.pdf"
    FIM_ZUSCHNITTSINFIKATOREN = "fim_zuschnittsindikatoren.pdf"
    FIM_REDAKTIONSPROZESS_LEISTUNGSZUSCHNITT = (
        "fim_redaktionsprozess_leistungszuschnitt.pdf"
    )
    FIM_REDAKTIONSFENSTER_BUNDESREDAKTION = "fim_redaktionsfenster_bundesredaktion.xlsx"
    FIM_SDG_RELEVANTE_LEISTUNGEN = "fim_sdg_relevante_leistungen.xlsx"

    OZG_INFORMATIONSUNTERLATEN = "ozg_informationsunterlagen.pdf"
    OZG_QS_KRITERIEN_REFERENZPROZESSE = "ozg_qs_kriterien_referenzprozesse.pdf"
    OZG_EMPFEHLUNGEN_FOERDERLISTE = "ozg_empfehlungen_foerderliste.pdf"
    OZG_VERFAHREN_STAMMDATENSCHEMATA_STAMMPROZESSE = (
        "ozg_verfahren_stammdatenschemata_stammprozesse.pdf"
    )

    FIM_COACH_AUSBILDUNGSRICHTLINIE = "fim_coach_ausbildungsrichtlinie.zip"

    SEMINAR_BASIS = "basisseminar.pdf"
    SEMINAR_BASIS_RAHMENBEDINGUNGEN_ONLINESCHULUNG = (
        "basisseminar_rahmenbedingungen_onlineschulung.pdf"
    )
    SEMINAR_EXPERT = "expertenseminar.pdf"

    def to_link(self) -> str:
        # TODO: The version parameter is used to force a cache refresh, as we had a bug that
        # these files were accidentally agressively cached (for up to 1 year).
        # The parameter can safely be removed after 2026-02-12.
        return f"/assets/{self.value}?version=2"


@dataclass(slots=True)
class ArbeitsmittelLink:
    is_download: bool
    label: str
    href: str

    @staticmethod
    def download(file: FileIdentifier, label: str = "Download") -> ArbeitsmittelLink:
        return ArbeitsmittelLink(
            is_download=True,
            label=label,
            href=file.to_link(),
        )


@dataclass(slots=True)
class ArbeitsmittelCard:
    icon: str
    title: str
    description: str | None
    links: list[ArbeitsmittelLink]
    size: int = 4


@dataclass(slots=True)
class ArbeitsmittelSection:
    anchor: str
    title: str
    cards: list[ArbeitsmittelCard]
    further_links: list[ArbeitsmittelLink]


ARBEITSMITTEL_SECTIONS = [
    ArbeitsmittelSection(
        anchor="leistungszuschnitt",
        title="Arbeitsmittel für den Leistungszuschnitt",
        cards=[
            ArbeitsmittelCard(
                icon="file-earmark-text",
                title="Musterformular Leistungszuschnitt",
                description="Dieses Formular unterstützt die Leistungsklärung im Rahmen der Normenanalyse und der Katalogisierung der Verwaltungsleistung.",
                links=[
                    ArbeitsmittelLink.download(
                        FileIdentifier.LEISTUNGEN_MUSTERFORMULAR_LEISTUNGSZUSCHNITT
                    ),
                ],
            ),
            ArbeitsmittelCard(
                icon="tools",
                title="Online-Tool FIM-Normenanalyse",
                description="Die Anwendung FIM-Normenanalyse unterstützt bei Analyse von relevanten Handlungsgrundlagen der öffentlichen Verwaltung zur Identifizierung von FIM-Leistungen im Rahmen der Leistungsklärung sowie zur Erstellung von FIM-Stamminformationen.",
                links=[
                    ArbeitsmittelLink(
                        is_download=False,
                        label="Anzeigen",
                        href="http://fim-normenanalyse.bfpi.de/",
                    ),
                ],
            ),
        ],
        further_links=[
            ArbeitsmittelLink.download(
                FileIdentifier.FIM_ZUSCHNITTSINFIKATOREN,
                label="Zuschnittsindikatoren",
            ),
            ArbeitsmittelLink(
                is_download=False,
                label="Harmonisierungskonzept",
                href="https://docs.fitko.de/fim/docs/quer/leistungszuschnitt/lz_einleitung",
            ),
            ArbeitsmittelLink.download(
                FileIdentifier.FIM_REDAKTIONSPROZESS_LEISTUNGSZUSCHNITT,
                label="Redaktionsprozess Leistungszuschnitt",
            ),
        ],
    ),
    ArbeitsmittelSection(
        anchor="leistungen",
        title="Arbeitsmittel für den Baustein Leistungen",
        cards=[
            ArbeitsmittelCard(
                icon="clipboard-check",
                title="QS-Kriterien",
                description="Eine ausführliche Beschreibung aller QS-Kriterien des Bausteins Leistungen.",
                links=[
                    ArbeitsmittelLink.download(FileIdentifier.LEISTUNGEN_QS_KRITERIEN),
                ],
            ),
            ArbeitsmittelCard(
                icon="file-earmark-text",
                title="Musterformular Leistungsbeschreibungen",
                description="Dieses Formular für Stammtexte und Leistungsbeschreibungen mit Informationen über Organisationseinheiten, Formulare und Onlinedienste, wurde in der UAG Leistung abgestimmt und vom Baustein Leistungen herausgegeben.",
                links=[
                    ArbeitsmittelLink.download(
                        FileIdentifier.LEISTUNGEN_MUSTERFORLUMAR_LEISTUNGEBESCHREIBUNGEN
                    ),
                ],
            ),
            ArbeitsmittelCard(
                icon="list",
                title="Referenzliste Pflichtattribute als Anlage zum Musterformular",
                description="Eine vom Baustein in Ergänzung zum Musterformular herausgegebene tabellarische Zusammenstellung der Pflichtattribute für Steckbrief, Stammtext, Leistungsbeschreibungen, SDG und PVOG.",
                links=[
                    ArbeitsmittelLink.download(
                        FileIdentifier.LEISTUNGEN_REFERENZLISTE_PFLICHTATTRIBUTE
                    ),
                ],
            ),
        ],
        further_links=[
            ArbeitsmittelLink(
                is_download=False,
                label="Spezifikation XZuFi",
                href="https://www.xrepository.de/details/urn:xoev-de:fim:standard:xzufi",
            ),
            ArbeitsmittelLink.download(
                FileIdentifier.LEISTUNGEN_LEIKA_HANDBUCH,
                label="LeiKa-Handbuch",
            ),
            ArbeitsmittelLink(
                is_download=False,
                label="Codeliste Leistungsgruppierung",
                href="https://www.xrepository.de/details/urn:de:fim:leika:leistungsgruppierung",
            ),
            ArbeitsmittelLink(
                is_download=False,
                label="Codeliste Verrichtungskennung",
                href="https://www.xrepository.de/details/urn:de:fim:leika:verrichtung",
            ),
            ArbeitsmittelLink(
                is_download=False,
                label="Codeliste Typisierung",
                href="https://www.xrepository.de/details/urn:de:fim:leika:typisierung",
            ),
            ArbeitsmittelLink(
                is_download=False,
                label="XZuFi 2.2.0 Qualitätscheck",
                href="/tools/check-leistung",
            ),
        ],
    ),
    ArbeitsmittelSection(
        anchor="datenfelder",
        title="Arbeitsmittel für den Baustein Datenfelder",
        cards=[
            ArbeitsmittelCard(
                icon="clipboard-check",
                title="QS-Kriterien",
                description="Eine ausführliche Beschreibung aller QS-Kriterien  für XDatenfelder 2 (der bisherige Standard) und XDatenfelder 3 (der zukünftige Standard).",
                links=[
                    ArbeitsmittelLink.download(
                        FileIdentifier.DATENFELDER_QS_KRITERIEN_2_0,
                        label="XDatenfelder 2",
                    ),
                    ArbeitsmittelLink(
                        is_download=False,
                        label="XDatenfelder 3",
                        href="https://docs.fitko.de/fim/docs/datenfelder/VB_Einleitung",
                    ),
                ],
            ),
            ArbeitsmittelCard(
                icon="file-earmark-text",
                title="Musterformular Dokumentsteckbrief",
                description="Dieses Formular dient dazu einen Dokumentsteckbrief / Katalogeintrag in einem Redaktionssystem des FIM-Bausteins Datenfelder zu beantragen.",
                links=[
                    ArbeitsmittelLink.download(
                        FileIdentifier.DATENFELDER_MUSTERFORMULAR_DOKUMENTSTECKBRIEF
                    ),
                ],
            ),
        ],
        further_links=[
            ArbeitsmittelLink(
                is_download=False,
                label="Spezifikation XDatenfelder",
                href="https://www.xrepository.de/details/urn:xoev-de:fim:standard:xdatenfelder",
            ),
            ArbeitsmittelLink.download(
                FileIdentifier.DATENFELDER_FACHKONZEPT, label="Fachkonzept"
            ),
            ArbeitsmittelLink.download(
                FileIdentifier.DATENFELDER_M2M_SCHNITTSTELLE,
                label="Dokumentation M2M Schnittstelle",
            ),
            ArbeitsmittelLink(
                is_download=False,
                label="XDF2 zu XDF3 Konverter",
                href="/tools/xdf2-to-xdf3-converter",
            ),
            ArbeitsmittelLink(
                is_download=False,
                label="XDF3 zu XDF2 Konverter",
                href="/tools/xdf3-to-xdf2-converter",
            ),
            ArbeitsmittelLink(
                is_download=False,
                label="XDF2 zu JSON Schema Konverter",
                href="/tools/xdf2-json-schema-converter",
            ),
            ArbeitsmittelLink(
                is_download=False,
                label="XDF3 zu JSON Schema Konverter",
                href="/tools/xdf3-json-schema-converter",
            ),
            ArbeitsmittelLink(
                is_download=False,
                label="XDF2 zu XSD Konverter",
                href="/tools/xdf2-xsd-converter",
            ),
            ArbeitsmittelLink(
                is_download=False,
                label="XDF2 Qualitätscheck",
                href="/tools/check-xdf2",
            ),
            ArbeitsmittelLink(
                is_download=False,
                label="XDF3 Qualitätscheck",
                href="/tools/check-xdf3",
            ),
            ArbeitsmittelLink(
                is_download=False,
                label="JSON Schema Legende",
                href="/tools/fim-json-legend",
            ),
        ],
    ),
    ArbeitsmittelSection(
        anchor="prozesse",
        title="Arbeitsmittel für den Baustein Prozesse",
        cards=[
            ArbeitsmittelCard(
                icon="clipboard-check",
                title="QS-Kriterien",
                description="Eine ausführliche Beschreibung aller QS-Kriterien des Bausteins Prozesse. Die Checkliste ist eine Excel-Datei mit Kriterien für Prozesssteckbrief, Prozessmodell allgemein und die einzelnen Prozess-Elemente.",
                links=[
                    ArbeitsmittelLink.download(
                        FileIdentifier.PROZESSE_QS_KRITERIEN,
                        label="QS-Kriterien",
                    ),
                    ArbeitsmittelLink.download(
                        FileIdentifier.PROZESSE_QS_KRITERIEN_CHECKLISTE,
                        label="Checkliste",
                    ),
                ],
            ),
            ArbeitsmittelCard(
                icon="file-earmark-text",
                title="Musterformular für Prozessinformationen",
                description="Dient als Schablone, um die relevanten Prozessinformationen auch unabhängig von einem speziellen Redaktionssystem erfassen zu können.",
                links=[
                    ArbeitsmittelLink.download(
                        FileIdentifier.PROZESSE_MUSTERFORMULAR_PROZESSINFORMATIONEN
                    )
                ],
            ),
            ArbeitsmittelCard(
                icon="tools",
                title="Online-Tool Quali-XProzess",
                description="Unterstützung für Entwickler: Hier können Sie die XProzess-Nachricht, die als XML-Datei im Format des Standards XProzess vorliegt, auf Schemakonformität und FIM-Konformität prüfen lassen.",
                links=[
                    ArbeitsmittelLink(
                        is_download=False,
                        label="Anzeigen",
                        href="https://qualixprozess.fim.dvzdigital.de/",
                    ),
                ],
            ),
        ],
        further_links=[
            ArbeitsmittelLink(
                is_download=False,
                label="Spezifikation XProzess",
                href="https://www.xrepository.de/details/urn:xoev-de:mv:em:standard:xprozess",
            ),
            ArbeitsmittelLink.download(
                FileIdentifier.PROZESSE_FACHKONZEPT, label="Fachkonzept"
            ),
            ArbeitsmittelLink(
                is_download=False,
                label="Integrierter Produktrahmen (IPR)",
                href="https://www.bundesfinanzministerium.de/Web/DE/Themen/Oeffentliche_Finanzen/Bundeshaushalt/Haushaltsrecht_und_Haushaltssystematik/haushaltsrecht_systematik.html",
            ),
            ArbeitsmittelLink.download(
                FileIdentifier.PROZESSE_HANDLUNGSANWEISUNG_SAMMELREPOSITORY,
                label="Handlungsanweisung zum Sammelrepository",
            ),
            ArbeitsmittelLink(
                is_download=False,
                label="KGSt Prozesskatalog",
                href="https://www.kgst.de/dokumentdetails?path=/documents/20181/1725501/5-B-2018_KGSt-Prozesskatalog/0cc0058f-a535-89c3-cd88-50316cffee3a",
            ),
            ArbeitsmittelLink.download(
                FileIdentifier.PROZESSE_HANDLUNGSANWEISUNG_XPROZESS_2_0_0,
                label="Handlungsanweisung XProzess 2.0.0",
            ),
            ArbeitsmittelLink(
                is_download=False,
                label="Online-Datenbank des Erfüllungsaufwands (OnDEA)",
                href="https://www.ondea.de/DE/Home/home_node.html",
            ),
            ArbeitsmittelLink.download(
                FileIdentifier.PROZESSE_AENDERUNGSANTRAG_XPROZESS_2_0_1,
                label="Änderungsantrag XProzess 2.0.1 (XProzess 3.0.0)",
            ),
            ArbeitsmittelLink(
                is_download=False,
                label="Übersicht Musterprozesse",
                href="https://fimportal.de/search?resource=sampleprocess",
            ),
        ],
    ),
]

FIM_QUERSCHNITT_LINKS = [
    ArbeitsmittelLink.download(
        FileIdentifier.FIM_COACH_AUSBILDUNGSRICHTLINIE,
        label="FIM-Coach Ausbildungsrichtlinie",
    ),
    ArbeitsmittelLink.download(
        FileIdentifier.FIM_PRODUKTHANDBUCH,
        label="FIM-Konformität",
    ),
    ArbeitsmittelLink.download(
        FileIdentifier.FIM_REDAKTIONSFENSTER_BUNDESREDAKTION,
        label="Redaktionsfenster der Bundesredaktion",
    ),
]

OZG_LINKS = [
    ArbeitsmittelLink.download(
        FileIdentifier.OZG_INFORMATIONSUNTERLATEN,
        label="FIM / OZG Informationsunterlagen",
    ),
    ArbeitsmittelLink.download(
        FileIdentifier.OZG_QS_KRITERIEN_REFERENZPROZESSE,
        label="QS-Kriterien für OZG-Referenzprozesse",
    ),
    ArbeitsmittelLink.download(
        FileIdentifier.OZG_EMPFEHLUNGEN_FOERDERLISTE,
        label="Empfehlungen für Förderleistungen im OZG-Kontext",
    ),
    ArbeitsmittelLink.download(
        FileIdentifier.OZG_VERFAHREN_STAMMDATENSCHEMATA_STAMMPROZESSE,
        label="Verdeutlichung des Verfahrens zum Silber/Gold-Status bei Typ 2/3 Stammdatenschemata und Stammprozessen",
    ),
]

FIM_COACH_LINKS = [
    ArbeitsmittelLink.download(
        FileIdentifier.FIM_COACH_AUSBILDUNGSRICHTLINIE,
        label="FIM-Coach Ausbildungsrichtlinie",
    ),
]

SDG_LINKS = [
    ArbeitsmittelLink.download(
        FileIdentifier.FIM_SDG_RELEVANTE_LEISTUNGEN,
        label="Übersicht SDG-relevante Leistungen",
    ),
]


class ArbeitsmittelPage(pydantic.BaseModel):
    title: str = "Arbeitsmittel • FIM Portal"

    header_titel: str
    header_text: str | None

    @staticmethod
    def load(client: CMSClient) -> ArbeitsmittelPage:
        data = {}

        data["header_titel"] = "Arbeitsmittel"
        data["header_text"] = (
            "Diese Seite stellt die für die Arbeit im FIM-Umfeld vorhandenen Werkzeuge zusammen."
        )

        return ArbeitsmittelPage.model_validate(data)

    @property
    def sections(self) -> list[ArbeitsmittelSection]:
        return ARBEITSMITTEL_SECTIONS

    @property
    def fim_querschnitt_links(self) -> list[ArbeitsmittelLink]:
        return FIM_QUERSCHNITT_LINKS

    @property
    def ozg_links(self) -> list[ArbeitsmittelLink]:
        return OZG_LINKS

    @property
    def fim_coach_links(self) -> list[ArbeitsmittelLink]:
        return FIM_COACH_LINKS

    @property
    def sdg_links(self) -> list[ArbeitsmittelLink]:
        return SDG_LINKS

    def get_title_section(self) -> TitleSection:
        return TitleSection(
            title=self.header_titel,
            text=self.header_text,
            links=[
                Link(
                    label="Leistungen",
                    href="#leistungen",
                    icon="arrow-down-circle-fill",
                ),
                Link(
                    label="Datenfelder",
                    href="#datenfelder",
                    icon="arrow-down-circle-fill",
                ),
                Link(
                    label="Prozesse",
                    href="#prozesse",
                    icon="arrow-down-circle-fill",
                ),
                Link(
                    label="Bausteinübergreifend",
                    href="#bausteinuebergreifend",
                    icon="arrow-down-circle-fill",
                ),
            ],
        )


class NewsPage(pydantic.BaseModel):
    title: str = "News • FIM Portal"

    header_titel: str
    header_text: str | None = None

    @staticmethod
    def load(client: CMSClient) -> NewsPage:
        data = {}

        data["header_titel"] = "News"

        return NewsPage.model_validate(data)

    def get_title_section(self) -> TitleSection:
        return TitleSection(
            title=self.header_titel,
            text=self.header_text,
            links=[
                Link(href="/news.rss", label="News abonnieren", icon="rss-fill"),
            ],
        )


class ArticlePage(pydantic.BaseModel):
    title: str

    article: NewsArticle

    @staticmethod
    def from_article(article: NewsArticle) -> ArticlePage:
        return ArticlePage(
            title=f"{article.title} • FIM Portal",
            article=article,
        )


class AnsprechpartnerRedaktion(pydantic.BaseModel):
    name: str
    description: str
    email: str
    icon: str

    @staticmethod
    def load(client: CMSClient) -> list[AnsprechpartnerRedaktion]:
        items = client.get_collection("agencycontact")

        for item in items:
            redaktion_id = LABEL_TO_REDAKTION_ID[item["name"]]
            item["icon"] = get_icon_url_for_redaktion(redaktion_id)

        items = [AnsprechpartnerRedaktion.model_validate(item) for item in items]

        return sorted(items, key=lambda x: x.name)


class AnsprechpartnerFim(pydantic.BaseModel):
    name: str
    role: str
    phone: str | None
    email: str
    image: PatchedContentAsset | None = None

    @staticmethod
    def load(client: CMSClient) -> list[AnsprechpartnerFim]:
        items = client.get_collection("fimcontact")
        for item in items:
            if item.get("image"):
                client.patch_image(
                    item, "image", ImageConfig(150, 150, "thumbnail", "webp")
                )

        return [AnsprechpartnerFim.model_validate(item) for item in items]


class KontaktPage(pydantic.BaseModel):
    title: str = "Kontakt • FIM Portal"

    header_titel: str
    header_text: str | None = None

    support_text: str
    support_email: str

    postanschrift: str

    ansprechpartner_redaktionen: list[AnsprechpartnerRedaktion]
    ansprechpartner_fim: list[AnsprechpartnerFim]

    @staticmethod
    def load(client: CMSClient) -> KontaktPage:
        data = {}

        data["header_titel"] = "Kontakt"

        data["support_text"] = (
            "<p>Haben Sie Fragen zum Föderalen Informationsmanagement oder zu diesem Internetportal, dann nutzen Sie unsere Kontaktmöglichkeiten. Sie erreichen hierüber auch die Bausteine Leistungen, Prozesse und Datenfelder.</p>"
        )
        data["support_email"] = "ticket@fimportal.de"

        data["postanschrift"] = (
            "FITKO (Föderale IT-Kooperation)<br>"
            "Digitale Verwaltung. Intelligent vernetzt.<br>"
            "Zum Gottschalkhof 3<br>"
            "60594 Frankfurt am Main"
        )

        data["ansprechpartner_redaktionen"] = AnsprechpartnerRedaktion.load(client)
        data["ansprechpartner_fim"] = AnsprechpartnerFim.load(client)

        return KontaktPage.model_validate(data)

    def get_title_section(self) -> TitleSection:
        return TitleSection(
            title=self.header_titel,
            text=self.header_text,
            links=[
                Link(
                    href="#central-contact",
                    label="Zentraler Kontakt",
                    icon="arrow-down-circle-fill",
                ),
                Link(
                    href="#contact-redaktionen",
                    label="Ansprechpersonen",
                    icon="arrow-down-circle-fill",
                ),
                Link(
                    href="#contact-fim",
                    label="Ansprechpersonen FIM",
                    icon="arrow-down-circle-fill",
                ),
            ],
        )
