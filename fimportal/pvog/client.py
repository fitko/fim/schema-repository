from __future__ import annotations

from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import AsyncGenerator
from urllib.parse import urljoin
import httpx
from httpx_auth import OAuth2ClientCredentials
import orjson

from .common import PvogError

AUTH_ENDPOINT = "/auth/realms/pvog/protocol/openid-connect/token"


@dataclass(slots=True)
class BereitstelldienstBatch:
    next_update_index: int
    xzufi_xml: str


class Client(ABC):
    @abstractmethod
    async def iterate_transfer_batches(
        self, start_batch_index: int
    ) -> AsyncGenerator[tuple[int, str], None]: ...

    @abstractmethod
    async def load_batch(self, update_index: int) -> BereitstelldienstBatch | None: ...


class HttpClient(Client):
    def __init__(self, base_url: str, client_id: str, client_secret: str):
        self._base_url = base_url

        token_endpoint = urljoin(base_url, AUTH_ENDPOINT)

        credential_manager = OAuth2ClientCredentials(
            token_endpoint,
            client_id=client_id,
            client_secret=client_secret,
        )

        self._client = httpx.AsyncClient(auth=credential_manager, timeout=60)

    async def iterate_transfer_batches(
        self, start_batch_index: int
    ) -> AsyncGenerator[tuple[int, str], None]:
        offset = start_batch_index
        while True:
            batch = await self.load_batch(offset)
            if batch is None:
                return
            batch_index = offset
            offset = batch.next_update_index
            yield (batch_index, batch.xzufi_xml)

    async def load_batch(self, update_index: int) -> BereitstelldienstBatch | None:
        response = await self._client.get(
            urljoin(
                self._base_url,
                # Using `%25` for the ars (Amtlicher Regionalschluessel) is the URL encoded string for
                # just `%`, which will include all available regions.
                # See: https://ddatabox.dataport.de/public/download-shares/QNiwa1iBHYFVrc29Ka7qHdp4vAsM5VLg
                f"/bereitstelldienst/api/v2/verwaltungsobjekte?index={update_index}&ars=%25",
            )
        )

        if response.status_code != 200:
            raise PvogError(
                f"Failed to load PVOG Bereitstelldienst batch [status_code={response.status_code}]: {response.text}"
            )

        result = orjson.loads(response.text)
        assert isinstance(result, dict)

        item_count = result["anzahlObjekte"]
        assert isinstance(item_count, int)

        if item_count == 0:
            return None

        xzufi_xml = result["xzufiObjekte"]
        assert isinstance(xzufi_xml, str)
        assert len(xzufi_xml) > 0

        next_update_index = result["naechsterIndex"]
        assert isinstance(next_update_index, int)

        return BereitstelldienstBatch(
            next_update_index=next_update_index,
            xzufi_xml=xzufi_xml,
        )


class TestClient(Client):
    def __init__(self):
        self.events: list[tuple[int, str]] = []

    async def iterate_transfer_batches(
        self, start_batch_index: int
    ) -> AsyncGenerator[tuple[int, str], None]:
        for event in self.events:
            if event[0] >= start_batch_index:
                yield event

    async def load_batch(self, update_index: int) -> BereitstelldienstBatch | None:
        return None

    def add_event_batch(self, batch_id: int, batch_xml: str):
        self.events.append((batch_id, batch_xml))
