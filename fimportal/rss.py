import email.utils
import lxml.etree as ET
from fimportal import xml
from fimportal.models.prozess import ProzessOut
from fimportal.models.xdf3 import (
    DatenfeldOut,
    DatenfeldgruppeOut,
    SchemaOut,
    SteckbriefOut,
)
import urllib.parse

from fimportal.models.xzufi import LeistungssteckbriefOut


def _get_link(
    baseurl: str,
    resource: SchemaOut | SteckbriefOut | DatenfeldgruppeOut | DatenfeldOut,
) -> str:
    if isinstance(resource, SchemaOut):
        return urllib.parse.urljoin(
            baseurl, f"/schemas/{resource.fim_id}/{resource.fim_version}"
        )
    if isinstance(resource, SteckbriefOut):
        return urllib.parse.urljoin(
            baseurl,
            f"/document-profiles/{resource.fim_id}/{resource.fim_version}",
        )
    if isinstance(resource, DatenfeldgruppeOut):
        return urllib.parse.urljoin(
            baseurl,
            f"/groups/{resource.namespace}/{resource.fim_id}/{resource.fim_version}",
        )
    return urllib.parse.urljoin(
        baseurl,
        f"/fields/{resource.namespace}/{resource.fim_id}/{resource.fim_version}",
    )


def _get_type(
    resource: SchemaOut | SteckbriefOut | DatenfeldgruppeOut | DatenfeldOut,
) -> str:
    if isinstance(resource, SchemaOut):
        return "Schema"
    if isinstance(resource, SteckbriefOut):
        return "Dokumentensteckbrief"
    if isinstance(resource, DatenfeldgruppeOut):
        return "Datenfeldgruppe"
    return "Datenfeld"


def create_schema_rss_feed_item(
    baseurl: str,
    resource: SchemaOut | SteckbriefOut | DatenfeldgruppeOut | DatenfeldOut,
):
    item = ET.Element("item")

    ET.SubElement(item, "link").text = _get_link(baseurl, resource)
    ET.SubElement(
        item, "title"
    ).text = f"{_get_type(resource)} '{resource.name}' wurde aktualisiert ({resource.fim_id} V{resource.fim_version})"
    handlungsgrundlage = ", ".join(resource.bezug) if len(resource.bezug) > 0 else "-"
    ET.SubElement(
        item, "description"
    ).text = f"Handlungsgrundlage: {handlungsgrundlage} | Status: {resource.freigabe_status_label}"
    ET.SubElement(item, "pubDate").text = email.utils.format_datetime(
        resource.letzte_aenderung
    )
    ET.SubElement(item, "author").text = resource.status_gesetzt_durch

    return item


def create_service_rss_feed_item(baseurl: str, leistung: LeistungssteckbriefOut):
    item = ET.Element("item")

    assert leistung.geaendert_datum_zeit is not None
    ET.SubElement(
        item, "title"
    ).text = f"Leistungssteckbrief { leistung.title} wurde aktualisiert ({leistung.leistungsschluessel})"
    ET.SubElement(item, "link").text = urllib.parse.urljoin(
        baseurl, f"/services/{leistung.leistungsschluessel}"
    )
    status = (
        leistung.freigabestatus_katalog.to_label()
        if leistung.freigabestatus_katalog is not None
        else "-"
    )
    rechtsgrundlage = (
        leistung.rechtsgrundlagen if leistung.rechtsgrundlagen is not None else "-"
    )
    ET.SubElement(
        item, "description"
    ).text = f"Handlungsgrundlage: {rechtsgrundlage} | Status: {status}"
    ET.SubElement(item, "pubDate").text = email.utils.format_datetime(
        leistung.geaendert_datum_zeit
    )
    return item


def create_process_rss_feed_item(baseurl: str, prozess: ProzessOut):
    """
    Information for Prozessklasse and further metadata will be added later.
    """
    item = ET.Element("item")

    assert prozess.letzter_aenderungszeitpunkt is not None
    ET.SubElement(
        item, "title"
    ).text = f"Prozess {prozess.name} wurde aktualisiert ({prozess.id})"
    ET.SubElement(item, "link").text = urllib.parse.urljoin(
        baseurl, f"/processes/{prozess.id}"
    )
    # ET.SubElement(
    #     item, "description"
    # ).text = f"Handlungsgrundlage {prozess.rechtsgrundlage} | Status: {prozess.freigabe_status}"
    ET.SubElement(item, "pubDate").text = email.utils.format_datetime(
        prozess.letzter_aenderungszeitpunkt
    )

    return item


def create_rss_xml(title: str, description: str, items: list[xml.XmlElement]) -> str:
    channel = ET.Element("channel")
    ET.SubElement(channel, "title").text = title
    ET.SubElement(channel, "description").text = description
    for item in items:
        channel.append(item)
    root = ET.Element("rss", attrib={"version": "2.0"})
    root.append(channel)

    return xml.serialize(root).decode("utf-8")
