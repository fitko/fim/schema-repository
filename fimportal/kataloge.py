from enum import Enum
import logging
from abc import ABC, abstractmethod
from typing import Literal

import httpx

logger = logging.getLogger(__name__)


class XzufiKatalogIdentifier(Enum):
    """
    All the katalog files in the TSA system we care about.
    We enumerate them here explicitly to allow for a customized presentation in the UI.
    Also, we only care about unzipped files, as not every file is available as an archive,
    so we have to zip some of them anyways.

    The list was compiled from the source: {base_url}/export/leika/links.json
    """

    LEISTUNGEN_XZUFI_2_2_0 = "leika_leistungen_xzufi_2.2.0"
    VERRICHTUNGEN_XZUFI_2_2_0 = "leika_verrichtungen_xzufi_2.2.0"
    LEISTUNGEN_GELOESCHT_XZUFI_2_2_0 = "leika_leistungen_geloescht_xzufi_2.2.0"
    LEISTUNGEN_ZU_LOESCHEN_XZUFI_2_2_0 = "leika_leistungen_zu_loeschen_xzufi_2.2.0"
    ORGANISATIONSEINHEITEN_XZUFI_2_2_0 = "leika_organisationseinheiten_xzufi_2.2.0"
    ONLINEDIENSTE_XZUFI_2_2_0 = "leika_onlinedienste_xzufi_2.2.0"

    LEISTUNGEN_XZUFI_2_1_0 = "leika_leistungen_xzufi_2.1.0"
    VERRICHTUNGEN_XZUFI_2_1_0 = "leika_verrichtungen_xzufi_2.1.0"

    LEISTUNGEN_XZUFI_1_0_0 = "leika_leistungen_xzufi"
    VERRICHTUNGEN_XZUFI_1_0_0 = "leika_verrichtungen_xzufi"

    LEIKA_PLUS_DE = "leika_plus_de_DE"
    LEIKA_PLUS_EN = "leika_plus_en"
    LEIKA_PLUS_DE_LS = "leika_plus_de_DE_LS"

    LEIKA_PLUS_OHNE_HTML_DE = "leika_plus_ohnehtml_de_DE"
    LEIKA_PLUS_OHNE_HTML_EN = "leika_plus_ohnehtml_en"
    LEIKA_PLUS_OHNE_HTML_DE_LS = "leika_plus_ohnehtml_de_DE_LS"

    LEISTUNGEN_DE = "leika_leistungen_de"
    LEISTUNGEN_EN = "leika_leistungen_en"
    LEISTUNGSOBJEKTE_DE = "leika_leistungsobjekte_de"
    LEISTUNGSOBJEKTE_EN = "leika_leistungsobjekte_en"
    VERRICHUNGEN_DE = "leika_verrichtungen_de"
    VERRICHUNGEN_EN = "leika_verrichtungen_en"

    def get_filetype(self) -> Literal["xml", "csv"]:
        match self:
            case (
                XzufiKatalogIdentifier.LEISTUNGEN_XZUFI_2_2_0
                | XzufiKatalogIdentifier.VERRICHTUNGEN_XZUFI_2_2_0
                | XzufiKatalogIdentifier.LEISTUNGEN_GELOESCHT_XZUFI_2_2_0
                | XzufiKatalogIdentifier.LEISTUNGEN_ZU_LOESCHEN_XZUFI_2_2_0
                | XzufiKatalogIdentifier.ORGANISATIONSEINHEITEN_XZUFI_2_2_0
                | XzufiKatalogIdentifier.ONLINEDIENSTE_XZUFI_2_2_0
                | XzufiKatalogIdentifier.LEISTUNGEN_XZUFI_2_1_0
                | XzufiKatalogIdentifier.VERRICHTUNGEN_XZUFI_2_1_0
                | XzufiKatalogIdentifier.LEISTUNGEN_XZUFI_1_0_0
                | XzufiKatalogIdentifier.VERRICHTUNGEN_XZUFI_1_0_0
            ):
                return "xml"

            case (
                XzufiKatalogIdentifier.LEIKA_PLUS_DE
                | XzufiKatalogIdentifier.LEIKA_PLUS_EN
                | XzufiKatalogIdentifier.LEIKA_PLUS_DE_LS
                | XzufiKatalogIdentifier.LEIKA_PLUS_OHNE_HTML_DE
                | XzufiKatalogIdentifier.LEIKA_PLUS_OHNE_HTML_EN
                | XzufiKatalogIdentifier.LEIKA_PLUS_OHNE_HTML_DE_LS
                | XzufiKatalogIdentifier.LEISTUNGEN_DE
                | XzufiKatalogIdentifier.LEISTUNGEN_EN
                | XzufiKatalogIdentifier.LEISTUNGSOBJEKTE_DE
                | XzufiKatalogIdentifier.LEISTUNGSOBJEKTE_EN
                | XzufiKatalogIdentifier.VERRICHUNGEN_DE
                | XzufiKatalogIdentifier.VERRICHUNGEN_EN
            ):
                return "csv"

    def get_source_filename(self) -> str:
        """
        Return the filename of the downloadable file in the TSA system.
        """

        return f"{self.value}.{self.get_filetype()}"


class KatalogException(Exception): ...


class Client(ABC):
    @abstractmethod
    async def load_katalog_content(self, filename: str) -> bytes: ...


class HttpClient(Client):
    def __init__(self, base_url: str):
        self._client = httpx.AsyncClient(
            timeout=120,
            base_url=base_url,
        )

    async def load_katalog_content(self, filename: str) -> bytes:
        response = await self._client.get(f"/export/leika/{filename}")
        if response.status_code != 200:
            raise KatalogException(
                f"Failed to load katalog {filename} (status: {response.status_code}):\n{response.text}"
            )

        return response.read()


class FakeClient(Client):
    def __init__(self):
        self._kataloge: dict[str, bytes] = {}

    async def load_katalog_content(self, filename: str) -> bytes:
        content = self._kataloge.get(filename)
        if content is None:
            raise KatalogException(f"Katalog {filename} not found")

        return content

    def add_kataloge(self, filename: str, content: bytes):
        self._kataloge[filename] = content

    def clear(self):
        self._kataloge = {}
