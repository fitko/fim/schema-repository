const items = document.getElementsByClassName("tree-item");

for (let item of items) {
  const header = item.querySelector(".tree-header");

  const treeToggle = header.querySelector(".tree-toggle");
  if (treeToggle) {
    treeToggle.addEventListener("click", (event) => {
      event.preventDefault();

      if (item.classList.contains("open")) {
        item.classList.remove("open");
      } else {
        item.classList.add("open");
      }
    });
  }

  const bezugToggle = header.querySelector(".bezug-toggle");
  if (bezugToggle) {
    const bezugContainer = item.querySelector(":scope > .bezug-container");
    console.assert(bezugContainer !== null);

    bezugToggle.addEventListener("click", (event) => {
      event.stopPropagation();
      event.preventDefault();

      if (bezugContainer.classList.contains("open")) {
        closeBezugContainer(bezugContainer, bezugToggle);
      } else {
        openBezugContainer(bezugContainer, bezugToggle);
      }
    });

    const closeButton = bezugContainer.querySelector(".btn-close");
    console.assert(closeButton !== null);
    closeButton.addEventListener("click", (event) => {
      event.stopPropagation();
      event.preventDefault();

      closeBezugContainer(bezugContainer, bezugToggle);
    });

    function openBezugContainer(container, toggle) {
      container.classList.add("open");
      toggle.innerHTML = toggle.innerHTML.replace(
        "bi-chevron-down",
        "bi-chevron-up",
      );
    }

    function closeBezugContainer(container, toggle) {
      container.classList.remove("open");
      toggle.innerHTML = toggle.innerHTML.replace(
        "bi-chevron-up",
        "bi-chevron-down",
      );
    }
  }
}
