const copyButtons = document.getElementsByClassName("copy-link-btn");
for (let button of copyButtons) {
  const href = button.dataset.href;

  // These must be saved before the button is modified, otherwise a double-click will load the wrong
  // original values.
  const originalIcon = button.innerHTML;
  const originalAriaLabel = button.getAttribute("aria-label");
  const originalTooltipContent = button.getAttribute("data-bs-title");

  button.addEventListener("click", (event) => {
    event.preventDefault();

    const tooltip = bootstrap.Tooltip.getInstance(button);
    navigator.clipboard
      .writeText(href)
      .then(() => {
        button.innerHTML = '<i class="bi bi-check2" aria-hidden="true"></i>';
        button.setAttribute("aria-label", "In die Zwischenablage kopiert!");
        tooltip.setContent({
          ".tooltip-inner": "In die Zwischenablage kopiert!",
        });
        tooltip.show();

        setTimeout(() => {
          button.innerHTML = originalIcon;
          button.setAttribute("aria-label", originalAriaLabel);
          tooltip.setContent({
            ".tooltip-inner": originalTooltipContent,
          });
          tooltip.hide();
        }, 3000);
      })
      .catch((err) => {
        console.error("Failed to copy text: ", err);
      });
  });
}
