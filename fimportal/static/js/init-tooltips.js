function initTooltips() {
  const tooltipTriggerList = document.querySelectorAll(
    '[data-bs-toggle="tooltip"]',
  );
  [...tooltipTriggerList].map((tooltipTriggerEl) => {
    const tooltip = new bootstrap.Tooltip(tooltipTriggerEl, {
      delay: { show: 500, hide: 100 },
      trigger: "hover focus",
    });

    tooltipTriggerEl.addEventListener("click", () => {
      clearTimeout(tooltip._timeout);
      tooltip.hide();
    });
  });
}

// Initialize tooltips on page load
document.addEventListener("DOMContentLoaded", initTooltips);

// Re-initialize tooltips after HTMX content loads
document.body.addEventListener("htmx:afterSwap", initTooltips);
