"""
Utilities for XML parsing.
"""

from __future__ import annotations

import logging
from datetime import datetime, timezone
from io import BytesIO
from pathlib import Path
from typing import IO, Any, Generic, Iterator, Literal, Optional, TypeVar

import ciso8601
import lxml.etree

from fimportal import din91379

logger = logging.getLogger(__name__)


XML_CACHE_DIR = Path(__file__).parent / "xml_cache"


class ParserException(Exception):
    pass


class UnexpectedChildNodeException(ParserException):
    def __init__(self, tag: str):
        super().__init__(f"Unexpected child node [tag={tag}]")


class MissingAttribute(ParserException):
    def __init__(self, name: str):
        super().__init__(f"Missing attribute: {name}")


class MissingValue(ParserException):
    def __init__(self, value_name: str):
        super().__init__(f"Missing value: {value_name}")


class EmptyNode(ParserException):
    pass


class DuplicateValue(ParserException):
    def __init__(self):
        super().__init__("Duplicate value")


T = TypeVar("T")


class ParsedValue(Generic[T]):
    """
    Container to temporarily store a parsed value.
    Helps detecting duplicate or missing entries.
    """

    _value: Optional[T] = None
    _filled: bool = False

    def expect(self, error_message: str) -> T:
        if self._value is None:
            raise MissingValue(error_message)

        return self._value

    def get(self) -> Optional[T]:
        return self._value

    def set(self, value: Optional[T]):
        # TODO (felix): Should this throw immediately if the field is required and the
        # value is None? This may be better for error messages, but would make
        # the code a little more complex. This would fail anywhere when calling
        # `expect` later.
        if self._filled:
            raise DuplicateValue()

        self._value = value
        self._filled = True


XmlElement = lxml.etree._Element  # type: ignore reportPrivateUsage
XmlDocument = lxml.etree._ElementTree  # type: ignore reportPrivateUsage
XmlEventType = Literal["start", "end"]
XmlEvent = tuple[XmlEventType, XmlElement]

XmlDataSource = str | bytes | IO[bytes] | IO[str]

XmlSchema = lxml.etree.XMLSchema


def parse_document(data: XmlDataSource) -> XmlElement:
    parser = lxml.etree.XMLParser(
        dtd_validation=False,
        load_dtd=False,
        no_network=True,
        remove_pis=True,
        remove_comments=True,
        resolve_entities=False,
    )

    if isinstance(data, bytes):
        return lxml.etree.fromstring(data, parser=parser)
    if isinstance(data, str):
        return lxml.etree.fromstring(data.encode("utf-8"), parser=parser)
    else:
        return lxml.etree.parse(data, parser=parser).getroot()


def load_document(data: IO[bytes]) -> XmlDocument:
    parser = lxml.etree.XMLParser(
        dtd_validation=False,
        load_dtd=False,
        no_network=True,
        remove_comments=True,
        remove_pis=True,
    )

    document: XmlDocument = lxml.etree.parse(data, parser=parser)

    return document


def remove_tag(xml_content: str, tag: str) -> str:
    tree = lxml.etree.fromstring(xml_content.encode("utf-8"))
    for node in tree.iter():
        if node.tag == tag:
            parent = node.getparent()
            assert parent is not None
            parent.remove(node)

    return lxml.etree.tostring(tree).decode("utf-8")


class XmlParser:
    """
    Custom wrapper around the raw lxml event stream.
    Provides a higher-level API supporting the xdf parsers.
    """

    current_line: int = 0

    def __init__(self, data: bytes):
        self._iter: Iterator[XmlEvent] = lxml.etree.iterparse(
            BytesIO(data),
            events=("start", "end"),
            dtd_validation=False,
            load_dtd=False,
            no_network=True,
            remove_comments=True,
            remove_pis=True,
        )

    def expect_child(self, tag: str) -> XmlElement:
        child = self.next_child()

        if child is None:
            raise MissingValue(tag)

        if child.tag != tag:
            raise UnexpectedChildNodeException(child.tag)

        return child

    def expect_close(self):
        event, data = self.next()

        if event != "end":
            raise UnexpectedChildNodeException(data.tag)

    def next_child(self) -> Optional[XmlElement]:
        event, data = self.next()

        if event == "start":
            return data
        else:
            return None

    def skip_node(self):
        depths = 1

        while True:
            event, _ = self.next()
            if event == "start":
                depths += 1
            elif event == "end":
                if depths == 1:
                    return
                else:
                    depths -= 1

    def parse_code(self) -> str:
        """
        Parse the content of the `code` child node.

        For example, calling the function for the following xml data

        ```
        <outer_node>
            <!-- Function should be called at this point -->
            <code>
                RequestedValue
            </code>
        </outer_node>
        ```

        would return `"RequestedValue"`.
        """

        self.expect_child("code")
        value = self.parse_token("code")
        self.expect_close()

        return value

    def parse_string_latin(self, node_tag: str) -> din91379.StringLatin:
        return din91379.parse_string_latin(self.parse_value(node_tag))

    def parse_optional_string_latin(self) -> din91379.StringLatin | None:
        value = self.parse_optional_value()
        if value is None:
            return None
        else:
            return din91379.parse_string_latin(value)

    def parse_token(self, node_tag: str) -> str:
        return str_to_token(self.parse_value(node_tag))

    def parse_optional_token(self) -> str | None:
        value = self.parse_optional_value()
        if value is None:
            return None
        else:
            return str_to_token(value)

    def parse_value(self, node_tag: str) -> str:
        """
        Parse a single, required value.
        """

        value = self.parse_optional_value()
        if value is None:
            raise EmptyNode(f"{node_tag} should not be empty")

        return value

    def parse_optional_value(self) -> Optional[str]:
        event, element = self.next()

        if event == "start":
            raise UnexpectedChildNodeException(element.tag)
        else:
            return element.text

    def next(self) -> XmlEvent:
        try:
            event, data = next(self._iter)
        except lxml.etree.XMLSyntaxError as error:
            raise ParserException("Invalid xml document") from error

        self.current_line = data.sourceline

        return event, data


def parse_code(node: XmlElement) -> str:
    """
    Parse the content of the `code` child node.

    For example, calling the function for the following xml data

    ```
    <outer_node>
        <!-- Function should be called at this point -->
        <code>
            RequestedValue
        </code>
    </outer_node>
    ```

    would return `"RequestedValue"`.
    """

    if len(node) == 0:
        raise MissingValue("code")

    code_node = node[0]
    if code_node.tag != "code":
        raise UnexpectedChildNodeException(code_node.tag)

    return parse_token(code_node)


def str_to_token(value: str) -> str:
    """
    Parse a value as `xs:token`.
    This will remove whitespace at the edges of the string, and replace all internal
    whitespaces with a single space.

    e.g.:
        "    Some   \n Test "
    will become:
        "Some Test"

    See: https://www.w3schools.com/XML/schema_dtypes_string.asp
    """
    return " ".join(value.split())


def optional_str_to_token(value: str | None) -> str | None:
    if value is None:
        return None

    return str_to_token(value)


def parse_token(node: XmlElement) -> str:
    value = parse_value(node)
    return str_to_token(value)


def parse_value(node: XmlElement) -> str:
    value = node.text
    if value is None:
        raise EmptyNode(f"{node.tag} should not be empty")

    return value


def parse_optional_value(node: XmlElement) -> str | None:
    value = node.text

    if value == "":
        return None

    return value


def parse_int(node: XmlElement) -> int | None:
    value = parse_value(node)
    try:
        return int(value)
    except:
        raise ParserException("Expected value should be parsed to integer.")


def parse_utc_datetime(node: XmlElement) -> datetime:
    return parse_datetime(node).astimezone(timezone.utc)


def parse_datetime(node: XmlElement) -> datetime:
    value = parse_value(node)
    try:
        return ciso8601.parse_datetime(value)
    except ValueError as error:
        raise ParserException(f"Invalid datetime '{value}'") from error


def parse_html(data: str) -> XmlElement:
    parser = lxml.etree.HTMLParser()

    return lxml.etree.fromstring(data, parser)  # type: ignore


class CacheResolver(lxml.etree.Resolver):
    """
    A custom resolver for external XSD dependencies.
    """

    def __init__(self, cache_dir: Path):
        self._cache_dir = cache_dir

    def resolve(self, system_url: str, public_id: str, context: Any):  # type: ignore
        if system_url.startswith("http"):
            filename = url_to_xsd_cache_filename(system_url)
            filepath = self._cache_dir / filename

            try:
                content = filepath.read_text(encoding="utf-8")
            except FileNotFoundError:
                # Log an error, then return nothing to load the file from the network.
                # This way, we get a sentry error in case of a missing file in the cache,
                # but the schema will still be loaded.
                logger.exception("Could not find xsd file in cache: %s", system_url)
            else:
                return self.resolve_string(content, context, base_url=system_url)


def load_xsd(path: str | Path) -> lxml.etree.XMLSchema:
    parser = lxml.etree.XMLParser(dtd_validation=False, load_dtd=False, no_network=True)
    parser.resolvers.add(CacheResolver(XML_CACHE_DIR))

    document = lxml.etree.parse(path, parser=parser)

    return lxml.etree.XMLSchema(document)


def validate_schema(data: bytes, schema: lxml.etree.XMLSchema):
    parser = lxml.etree.XMLParser(dtd_validation=False, load_dtd=False, no_network=True)

    try:
        document = lxml.etree.parse(BytesIO(data), parser)

        schema.assertValid(document)
    except (lxml.etree.DocumentInvalid, lxml.etree.XMLSyntaxError) as error:
        raise ParserException(str(error)) from error


def get_local_name(tag: str) -> str:
    return lxml.etree.QName(tag).localname


def serialize(node: XmlElement) -> bytes:
    return lxml.etree.tostring(node, encoding="utf-8", xml_declaration=True)


def prettify(xml_data: str | bytes | XmlElement | XmlDocument) -> str:
    match xml_data:
        case bytes():
            input = xml_data
        case str():
            input = xml_data.encode("utf-8")
        case _:
            input = lxml.etree.tostring(xml_data)

    parser = lxml.etree.XMLParser(
        dtd_validation=False,
        load_dtd=False,
        no_network=True,
        remove_blank_text=True,
    )
    document = lxml.etree.fromstring(input, parser=parser)
    return lxml.etree.tostring(document, pretty_print=True).decode("utf-8")


def canonicalize(xml_data: str | bytes | XmlElement | XmlDocument) -> str:
    """
    Parse and print the XML data in canonical form.
    This is helpfull when comparing XML data during tests.
    """
    match xml_data:
        case bytes():
            input = xml_data
        case str():
            input = xml_data.encode("utf-8")
        case _:
            input = lxml.etree.tostring(xml_data)

    parser = lxml.etree.XMLParser(
        dtd_validation=False,
        load_dtd=False,
        no_network=True,
        remove_blank_text=True,
    )
    document = lxml.etree.fromstring(input, parser=parser)
    return lxml.etree.tostring(document, method="c14n2").decode("utf-8")


def pretty_print(xml_data: str | bytes | XmlElement):
    print(prettify(xml_data))


def url_to_xsd_cache_filename(url: str) -> str:
    return url.replace(":", "").replace("/", "_")
