from __future__ import annotations

import hashlib
import os
import logging
import pathlib
from contextlib import asynccontextmanager
from dataclasses import dataclass
from mimetypes import guess_type
from typing import Iterable, Literal

import asyncpg
import jinja2
import sentry_sdk
from sentry_sdk.integrations.asyncio import AsyncioIntegration
from pydantic import BaseModel


logger = logging.getLogger(__file__)


@dataclass(slots=True)
class SentryConfig:
    dsn: str
    environment: str
    profiles_sample_rate: float

    def init_sentry(self):
        sentry_sdk.init(
            dsn=self.dsn,
            environment=self.environment,
            integrations=[AsyncioIntegration()],
            traces_sample_rate=1.0,
            profiles_sample_rate=self.profiles_sample_rate,
        )

        logger.info("Sentry initialized.")


class AppConfig(BaseModel):
    """
    Configuration for the FIM Portal service.

    ###### Environment

    The environment is used to pass the environment to sentry.
    When on local, this also deactivates forcing the browser to only accept https.

    ###### Baseurl

    The base url of the service. Used to generate stable urls.
    """

    environment: Literal["local", "stage", "production"]

    baseurl: str
    database_url: str
    admin_password_hash: str

    immutable_base_url: str

    sentry_dsn: str | None = None
    user_tracking: bool = False
    cms: CMSConfig

    xzufi_api_key: str | None = None
    pvog: PvogConfig | None = None

    # FRED classic import sources
    import_sources: dict[str, str] | None = None

    xprozesse: XProzesseConfig | None = None

    # Legacy FIM Portal katalog import
    kataloge: KatalogeConfig | None = None

    @staticmethod
    def load_from_env() -> AppConfig:
        config_filepath = _load_config_location()

        with open(config_filepath, "r", encoding="utf-8") as file:
            config = AppConfig.model_validate_json(file.read())

        assert not config.baseurl.endswith(
            "/"
        ), f"Baseurl must not end with a slash: {config.baseurl}"

        return config

    @staticmethod
    def load_and_init_from_env() -> AppConfig:
        config = AppConfig.load_from_env()
        config.init_sentry()

        return config

    def get_sentry_config(self) -> SentryConfig | None:
        if self.sentry_dsn is None:
            return None

        return SentryConfig(
            dsn=self.sentry_dsn,
            environment=self.environment,
            profiles_sample_rate=0.0 if self.is_production() else 1.0,
        )

    def init_sentry(self):
        """
        This should be executed at the start of the asyncio event loop, so that
        async stack traces can also be collected.
        """

        config = self.get_sentry_config()
        if config is None:
            logger.warning("No sentry dsn configured, skipping sentry initialization.")
        else:
            config.init_sentry()

    def is_local(self) -> bool:
        return self.environment == "local"

    def is_production(self) -> bool:
        return self.environment == "production"

    def get_import_urls(self) -> list[str]:
        assert self.import_sources is not None

        return list(self.import_sources.values())

    def get_full_database_url(self) -> str:
        return f"postgresql://{self.database_url}"

    def create_connection_pool(
        self,
    ):
        return create_connection_pool(self.get_full_database_url())

    async def create_connection(self) -> asyncpg.Connection:
        return await asyncpg.connect(self.get_full_database_url())


def _load_config_location() -> str:
    """
    Load the location of the config file from the environment.

    If FIM_PORTAL_CONFIG_FILE is set, just use this filepath.
    This option is helpful for local development.
    Otherwise, use the default location in the CRENDETIALS_DIRECTORY,
    which is the directory provided by systemd for the decrypted credentials.
    """

    config_filepath = os.getenv("FIM_PORTAL_CONFIG_FILE")
    if config_filepath is not None:
        return config_filepath

    credentials_directory = os.getenv("CREDENTIALS_DIRECTORY")
    if credentials_directory is None:
        raise ValueError("CREDENTIALS_DIRECTORY environment variable is not set.")

    return os.path.join(credentials_directory, "config")


class CMSConfig(BaseModel):
    """
    Configuration for the CMS data.

    This config is used both for:
        - The update script to download and compile the cms data.
        - The server to locate the compiled data.

    The server expects the compiled data to already exist at startup.
    """

    # The token to authenticate against the cms.
    # Only necessary when executing the update command.
    cockpit_token: str | None = None

    # The path to a json file containing the cms data.
    # This is the only thing required to actually run the server.
    data_file: str

    # The path to a directory containing the cms assets with a stable name.
    # This must be set when serving the assets from the Python API or updating the assets from the cms.
    assets_dir: str | None = None

    # The path to the directory containing the content-adressable assets.
    # This must be set when serving the downloads from the Python API or updating the downloads from the cms.
    content_assets_dir: str | None = None

    # Optionally define a custom cache directory for cms assets.
    # This will be re-used between updates of the cms data and skip downloading already existing assets.
    # This should only be used locally to speed up development, as there is currently no way to remove old assets from the cache.
    cache_dir: str | None = None

    # Some documents (currently only the Prozesskatalog) are not part of the cms data but downloaded from an external source.
    # We therefore want to regularly load these documents from the external source and save them to an internal cache.
    # When compiling the final assets, we want to overwrite any assets with the data in this cache.
    # This way, even if the external source is down, we still have the last known good data in the cache.
    external_cache_dir: str | None = None

    # If set to true, the Python API will serve the assets from the assets_dir.
    # This is only intended for local development, as this is served
    # directly by nginx when running on the server.
    serve_assets: bool = False


class XProzesseConfig(BaseModel):
    base_url: str
    api_key: str
    certificate_filepath: str
    key_filepath: str


class PvogConfig(BaseModel):
    """
    Configuration for the pvog Bereitstelldienst.
    """

    base_url: str
    client_id: str
    client_secret: str


class KatalogeConfig(BaseModel):
    """
    Configuration for the kataloge service.

    Attributes:
        - resource_url: The base url of the kataloge service.
    """

    resource_url: str


@asynccontextmanager
async def create_connection_pool(database_url: str):
    pool = await asyncpg.create_pool(dsn=database_url)

    try:
        yield pool
    finally:
        await pool.close()


@dataclass
class Directories:
    static: str
    templates: str
    test_data: str


def get_directories() -> Directories:
    root_dir = pathlib.Path(__file__).parent

    return Directories(
        static=str(root_dir / "static"),
        templates=str(root_dir / "templates"),
        test_data=str(root_dir / "test_data"),
    )


@dataclass
class FileMetadata:
    content_type: str
    content_length: str
    etag: str

    @staticmethod
    def create(path: pathlib.Path, content: bytes) -> FileMetadata:
        etag = hashlib.md5(content, usedforsecurity=False).hexdigest()

        return FileMetadata(
            content_type=guess_type(path)[0] or "text/plain",
            content_length=str(len(content)),
            etag=etag,
        )


def load_templates(directory: pathlib.Path | str) -> jinja2.DictLoader:
    """
    Load all jinja2 template files from the file system and return a pre-populated
    loader. This way, all templates are loaded during server-startup and can be served from memory.
    This fixes a bug when loading a template file from disk while the package is being temporarily
    removed for an update.
    """
    templates: dict[str, str] = {}

    for filepath in _iterate_files(directory):
        templates[str(filepath.relative_to(directory))] = filepath.read_text(
            encoding="utf-8"
        )

    return jinja2.DictLoader(templates)


def _iterate_files(directory: pathlib.Path | str) -> Iterable[pathlib.Path]:
    for current_dir, _directories, filenames in os.walk(pathlib.Path(directory)):
        for filename in filenames:
            if filename.startswith("."):
                continue
            yield pathlib.Path(current_dir) / filename
