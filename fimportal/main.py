from __future__ import annotations

from contextlib import asynccontextmanager
from typing import Any

import asyncpg
import jinja2
from fastapi import FastAPI, HTTPException, Request, Response, status
from fastapi.responses import ORJSONResponse
from fastapi.templating import Jinja2Templates
from starlette.middleware.base import BaseHTTPMiddleware, RequestResponseEndpoint
from starlette.requests import Request as StarletteRequest
from starlette.responses import HTMLResponse
from starlette.staticfiles import StaticFiles as StarletteStaticFiles
from starlette.types import ASGIApp

from fimportal.cms import CMSContent
from fimportal.helpers import ActiveTab
from fimportal.xzufi import link_check
from fimportal.static import StaticFiles

from . import xrepository
from .config import (
    AppConfig,
    SentryConfig,
    get_directories,
    load_templates,
)
from .routers.admin import create_router as create_admin_router
from .routers.frontend import create_router as create_frontend_router
from .routers.immutable import create_router as create_immutable_router
from .routers.tools import create_router as create_tool_router
from .routers.v0 import create_router as create_v0_router
from .routers.v1 import create_router as create_v1_router
from .service import Service


class CustomCORSMiddleware(BaseHTTPMiddleware):
    async def dispatch(
        self, request: StarletteRequest, call_next: RequestResponseEndpoint
    ) -> Response:
        response = await call_next(request)
        response.headers["Access-Control-Allow-Origin"] = "*"
        response.headers["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
        response.headers["Access-Control-Allow-Headers"] = "*"
        return response


class ContentSecurityPolicyMiddleware(BaseHTTPMiddleware):
    # NOTE(Felix): Ignore the /docs page, as here dependencies could change often
    IGNORE_ROUTES = ["/docs"]

    _header: str

    def __init__(self, app: ASGIApp):
        super().__init__(app)

        self._header = (
            "default-src 'self'; "
            "style-src 'self' 'unsafe-inline'; "
            "font-src 'self'; "
            "script-src 'self' https://plausible.io; "
            "connect-src 'self' data: https://plausible.io; "
            "img-src 'self' data:; "
        )

    async def dispatch(
        self,
        request: StarletteRequest,
        call_next: RequestResponseEndpoint,
    ) -> Response:
        response = await call_next(request)

        if not request.url.path in self.IGNORE_ROUTES:
            response.headers["Content-Security-Policy"] = self._header

        return response


def create_app() -> FastAPI:
    config = AppConfig.load_from_env()
    xrepository_client = xrepository.HttpClient()
    link_checker = link_check.LinkChecker()

    database_url = config.get_full_database_url()

    with open(config.cms.data_file) as f:
        content = f.read()
        cms_content = CMSContent.model_validate_json(content)

    if config.cms.serve_assets:
        assert config.cms.assets_dir is not None
        cms_assets_dir = config.cms.assets_dir
        cms_content_assets_dir = config.cms.content_assets_dir
    else:
        cms_assets_dir = None
        cms_content_assets_dir = None

    return create_app_with_config(
        database_url=database_url,
        xrepository_client=xrepository_client,
        link_checker=link_checker,
        admin_password_hash=config.admin_password_hash,
        baseurl=config.baseurl,
        immutable_base_url=config.immutable_base_url,
        user_tracking=config.user_tracking,
        cms_content=cms_content,
        cms_assets_dir=cms_assets_dir,
        cms_content_assets_dir=cms_content_assets_dir,
        show_production_header=config.is_production(),
        sentry_config=config.get_sentry_config(),
    )


def create_app_with_config(
    database_url: str,
    xrepository_client: xrepository.Client,
    link_checker: link_check.BaseLinkChecker,
    admin_password_hash: str,
    baseurl: str,
    immutable_base_url: str,
    user_tracking: bool,
    cms_content: CMSContent,
    cms_assets_dir: str | None,
    cms_content_assets_dir: str | None,
    show_production_header: bool,
    sentry_config: SentryConfig | None,
) -> FastAPI:
    """
    Create the FastAPI app, which handles both Web and API traffic.

    TODO(Felix): This is probably not optimal, as the Web and API Routes are mixed together.
    There are notable differences between the two, i.e.
    - Error handling (HTML for Web Routes, JSON for API Routes)
    - Security headers? (Our Web Routes should only be accessible from our domain, the API Routes from everywhere.
      We are currently probably too restrictive with our API headers.)
    - Only the API Routes are necessary for the OpenAPI Spec (splitting the apps would simplify `dump_openapi_specs.py`)

    We can think about splitting the app into two separate apps, one for the Web Routes and one for the API Routes, in the future.

    https://fastapi.tiangolo.com/advanced/sub-applications/
    """

    xrepository_service = xrepository.Service(xrepository_client)
    link_checker_service = link_check.Service(link_checker)
    directories = get_directories()
    connection_pool: asyncpg.Pool | None = None

    @asynccontextmanager
    async def lifespan(
        _app: FastAPI,
    ):
        if sentry_config is not None:
            sentry_config.init_sentry()

        nonlocal connection_pool
        connection_pool = await asyncpg.create_pool(database_url)

        try:
            yield
        finally:
            await connection_pool.close()

    app = FastAPI(
        lifespan=lifespan,
        title="FIM Portal - API",
        version="0.24.0",
        summary="The REST API of the FIM Portal.",
        contact={
            "name": "OpenCoDE Repository",
            "url": "https://gitlab.opencode.de/fitko/fim/portal",
        },
        description="""
#### Overview

The FIM Portal collects data from all FIM-Bausteine in a central location, specifically
- **Datenschemata**, **Datenfeldgruppen**, **Datenfelder** and **Dokumentsteckbriefe** as defined by the 
standard [XDatenfelder](https://www.xrepository.de/details/urn:xoev-de:fim:standard:xdatenfelder)
- **Leistungen** as defined by the standard [XZuFi](https://www.xrepository.de/details/urn:xoev-de:fim:standard:xzufi).
- **Prozesse** as defined by the standard [XProzess](https://www.xrepository.de/details/urn:xoev-de:mv:em:standard:xprozess)

It offers powerful endpoints for searching and filtering as well as for upload, download, conversion and quality assurance. The code is open source and can be found on
[OpenCoDE](https://gitlab.opencode.de/fitko/fim/schema-repository). The API also functions as a backend for the new [FIM Portal Website](https://neu.fimportal.de), specifically its dynamic [search](https://neu.fimportal.de/search) component. 

Please [reach out](https://gitlab.opencode.de/fitko/fim/schema-repository) if you need any help integrating our API.


#### Domains

The FIM Portal is currently hosted on the domain `neu.fimportal.de`.
To not invalidate existing immutable URLs, the old domain `schema.fim.fitko.net` is also still available for the API.
There is no plan to deprecate the old domain.
Both domains are interchangeable and point to the same server.

Please note that this is only true for the API. The new [FIM Portal Website](https://neu.fimportal.de) is only available on the new domain.
All URLs pointing to the old webiste will be redirected to the new domain.

In the future, there will probably be another change to the final domain `fimportal.de`.
To avoid breaking changes, we will keep the old domains available and redirect them to the new domain for the website.
The API will then be available on the new domain and the old domains.


#### Definitions

All attribute names used in our API are closely related and often even identical to the original German terms used in the respective standard.
This is done itentionally to simplify correlating our data with the corresponding definitions from the standard.
In some places, however, we opted to use English terms for easier readability, i.e. in the URLs of our api.
The following list contains all used translations:

- **Datenschema**: schema
- **Datenfeldgruppe**: data group
- **Datenfeld**: data field
- **Dokumentsteckbrief**: document profile
- **Leistung**: service
- **Leistungsbeschreibung**: xzufi-service
- **Prozess**: process
- **Codelisten**: code_lists


#### Structure

The API is structured into two, mostly independent parts:

- **api**: The core REST API, used for searching, filtering and uploading.
- **tools**: Some additional useful tools. This makes our internal tools like converters and quality checks available for simple reuse.


#### Immutable URLs

Our API generates immutable resources behind constant URLs for usage in stable production scenarios (e.g. [FITConnect](https://docs.fitko.de/fit-connect/docs)).
This includes code lists, [JSON Schema](https://json-schema.org/) and [XSD](https://www.w3.org/TR/xmlschema11-1/) files.

In the past, the URLs to these resources were constructed manually. This is no longer necessary, as the full URLs are now included in our API responses.


#### Rate-Limiting

The API uses IP based rate limiting. 
If you exceed the rate limit, you will receive a 429 status code.
Simply wait a few milliseconds and try again.

Please contact us if our default rate limit is not sufficient for your use case.


#### Feedback

Our goal is to support concrete, real-world use cases with a useful and reliable service. Please feel free to
[reach out](https://gitlab.opencode.de/fitko/fim/schema-repository) if there is anything we could do better to support
your specific usage scenario.
""",
        swagger_ui_parameters={
            "defaultModelsExpandDepth": -1,
            "displayRequestDuration": 1,
        },
        openapi_tags=[
            {
                "name": "api v1",
                "description": "The stable core JSON API. There will be no breaking changes to these endpoints, only additions and bug-fixes.",
            },
            {
                "name": "api v0",
                "description": "Future additions to the stable API which are currently in beta status. There can still be breaking changes if necessary, in particular moving the route to the v1 API once it has been stabilized.",
            },
            {
                "name": "tools",
                "description": (
                    "We provide stand-alone, reusable tools that can be used independently of the contents of the FIM Portal."
                ),
            },
        ],
    )

    app.add_middleware(CustomCORSMiddleware)
    app.add_middleware(ContentSecurityPolicyMiddleware)

    static_files = StaticFiles.load(directories.static)
    static_files.add_static_paths(app)

    if cms_assets_dir:
        app.mount("/assets", StarletteStaticFiles(directory=cms_assets_dir))

        assert cms_content_assets_dir is not None
        app.mount(
            "/content-assets", StarletteStaticFiles(directory=cms_content_assets_dir)
        )

    jinja_env = jinja2.Environment(
        loader=load_templates(directories.templates),
        undefined=jinja2.StrictUndefined,
        auto_reload=False,
        autoescape=True,
    )
    jinja_env.globals["get_static_url"] = static_files.get_static_url  # type: ignore
    jinja_env.globals["show_production_header"] = show_production_header  # type: ignore
    templates = Jinja2Templates(env=jinja_env)

    def render(
        request: StarletteRequest,
        name: str,
        context: dict[str, Any],
        active_tab: ActiveTab | None = None,
        status_code: int = 200,
    ) -> HTMLResponse:
        """
        Render a jinja2 template. This will add additional metadata to the render context,
        """

        def _get_tracking_url(baseurl: str) -> str:
            try:
                return baseurl.split("://")[1]
            except ValueError:
                return baseurl

        context["language"] = "de"
        context["user_tracking"] = user_tracking
        context["baseurl"] = _get_tracking_url(baseurl)
        context["active_tab"] = active_tab
        context["base"] = cms_content.base

        if "metaDescription" not in context:
            context["metaDescription"] = "FIM Portal"

        if "titleTag" not in context:
            context["titleTag"] = "FIM Portal"

        return templates.TemplateResponse(
            request,
            name,
            context,
            status_code=status_code,
        )

    async def get_service():
        assert connection_pool is not None

        async with connection_pool.acquire() as connection:
            async with connection.transaction():
                yield Service.from_connection(
                    immutable_base_url,
                    connection,
                    xrepository_service,
                    link_checker_service,
                )

    admin_router = create_admin_router(
        static_files=static_files,
        admin_password_hash=admin_password_hash,
        get_service=get_service,
        templates=templates,
    )
    app.include_router(admin_router, prefix="/admin")

    @app.get("/robots.txt", include_in_schema=False)
    async def _robots():  # type: ignore [reportUnusedFunction]
        return Response(content="User-agent: *\nDisallow: /")

    @app.get("/api/health", include_in_schema=False)
    async def _health():  # type: ignore [reportUnusedFunction]
        Response()

    v1_router = create_v1_router(get_service)
    app.include_router(v1_router, prefix="/api/v1", tags=["api v1"])

    v0_router = create_v0_router(get_service)
    app.include_router(v0_router, prefix="/api/v0", tags=["api v0"])

    @app.get("/api/{_:path}", response_class=ORJSONResponse, include_in_schema=False)
    async def _unknown_api_route():  # type: ignore [reportUnusedFunction]
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="unknown route",
        )

    immutable_router = create_immutable_router(get_service)
    app.include_router(
        immutable_router,
        prefix="/immutable",
        tags=["immutable"],
        include_in_schema=False,
    )

    tool_router = create_tool_router(
        xrepository_service=xrepository_service,
        baseurl=baseurl,
        get_service=get_service,
        render=render,
    )
    app.include_router(tool_router, prefix="/tools", tags=["tools"])

    frontend_router = create_frontend_router(
        get_service=get_service,
        baseurl=baseurl,
        cms_content=cms_content,
        render=render,
    )
    app.include_router(frontend_router, include_in_schema=False)

    @app.exception_handler(Exception)
    async def _internal_server_error(  # type: ignore [reportUnusedFunction]
        request: Request,
        exc: Exception,
    ):
        url = str(request.url)
        if "/api/" in url or "/immutable/" in url or "/tools/" in url:
            return ORJSONResponse(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                content="Internal Server Error",
            )

        return render(
            request,
            "500.html.jinja",
            {},
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )

    return app
