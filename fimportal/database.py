from __future__ import annotations

from collections import defaultdict
from dataclasses import dataclass
from datetime import date, datetime
from enum import Enum
from itertools import chain
from typing import (
    Any,
    Callable,
    ClassVar,
    Generic,
    Iterable,
    Literal,
    Protocol,
    TypeVar,
)
from urllib.parse import urlparse

import orjson
from asyncpg import Connection, connect
from asyncpg.connection import asyncpg

from fimportal import xzufi
from fimportal.common import Anwendungsgebiet, FreigabeStatus
from fimportal.helpers import ts_query_param, utc_now
from fimportal.migrations.execute import execute_migrations
from fimportal.models.csv_export import CSVSearchResult
from fimportal.models.kataloge import NewKatalog
from fimportal.models.prozess import (
    FullProzessklasseOut,
    FullProzessOut,
    NewProzess,
    NewProzessklasse,
    ProzessklasseOut,
    ProzessOut,
)
from fimportal.models.token import ApiTokenModel
from fimportal.models.xdf_export import (
    DatenfeldExportData,
    DatenfeldgruppeExportData,
    DatenfeldgruppeMessageExport,
    DatenfeldMessageExport,
    RegelExportData,
    SchemaExportData,
    SchemaMessageExport,
)
from fimportal.models.xzufi import NewLeistung
from fimportal.pagination import PaginatedResult, PaginationOptions
from fimportal.xdatenfelder.reports import QualityReport
from fimportal.xprozesse.prozess import (
    Detaillierungsstufe,
    Handlungsform,
    OperativesZiel,
    Verfahrensart,
)
from fimportal.xzufi.common import (
    LeistungsId,
    XzufiIdentifier,
    XzufiLeistungsIdentifier,
)
from fimportal.xzufi.leistung import (
    LeistungsTyp,
    RedaktionsId,
)

from .models.imports import (
    ElementIdentifier,
    NewCodeList,
    NewField,
    NewGroup,
    NewRule,
    NewSchema,
    RuleIdentifier,
)
from .models.xdf3 import (
    ChildType,
    CodeListOut,
    DatenfeldgruppeOut,
    DatenfeldOut,
    DetailedDatenfeld,
    DetailedDatenfeldgruppe,
    DetailedSchema,
    DirectChild,
    ElementIdentifierTuple,
    FullSteckbriefOut,
    JsonSchemaFileModel,
    NewSteckbrief,
    RuleOut,
    SchemaFileModel,
    SchemaOut,
    SteckbriefOut,
    TreeRelationModel,
    XdfVersion,
    XsdFileModel,
)
from .models.xzufi import (
    FullLeistungsbeschreibungOut,
    FullLeistungssteckbriefOut,
    LeistungsbeschreibungOut,
    LeistungssteckbriefOut,
    Link,
    NewOnlinedienst,
    NewOrganisationseinheit,
    NewSpezialisierung,
    NewZustaendigkeit,
    OnlinedienstOut,
    OrganisationseinheitOut,
    PvogResourceClass,
    SpezialisierungOut,
    XzufiSource,
    ZustaendigkeitOut,
)
from .xdatenfelder import xdf3

AtomicQueryParameter = str | date | datetime | bool | int

QueryParameter = AtomicQueryParameter | list[AtomicQueryParameter]

QUERY_PARAM_LIST = list[str | date | datetime | bool | int | list[str] | list[int]]


T = TypeVar("T", covariant=True)


class SearchQueryItem(Protocol, Generic[T]):
    """
    Interface for the output of each search query item.

    The item must contain all of the queried columns as well as a method to construct
    the result from a row.
    The `fts_match` parameter is used to pass the result of a full text search match,
    if any was performed.
    """

    SQL_COLUMNS: ClassVar[str]

    @staticmethod
    def from_row(row: tuple[Any], fts_match: str | None = None) -> T: ...


class SearchQuery:
    """
    All of the data necessary to execute a search query against our data.

    This makes it easy to re-use the same search logic across different endpoints.
    """

    table: str
    params: list[QueryParameter]
    where_clauses: list[str]
    fts_query_param_index: int | None = None
    fts_headline_column: str | None = None

    def __init__(self, table: str):
        self.table = table

        self.params = []
        self.where_clauses = []

    def fts(self, search_columns: list[str], headline_column: str, search_term: str):
        assert self.fts_query_param_index is None
        assert self.fts_headline_column is None
        assert len(search_columns) > 0

        query_param = ts_query_param(search_term)
        if query_param is not None:
            self.params.append(query_param)
            self.fts_query_param_index = len(self.params)
            self.fts_headline_column = headline_column

            fts_conditions: list[str] = []
            for col in search_columns:
                fts_conditions.append(
                    f"{col} @@ to_tsquery('german', ${self.fts_query_param_index})"
                )
            self.where_clauses.append(f"({' OR '.join(fts_conditions)})")

    def gueltig_am(self, value: date):
        self.params.append(value)
        self.where_clauses.append(
            "("
            "(gueltig_ab isnull AND gueltig_bis isnull)"
            f" OR (gueltig_ab <= ${len(self.params)} AND gueltig_bis >= ${len(self.params)})"
            f" OR (gueltig_ab isnull AND gueltig_bis >= ${len(self.params)})"
            f" OR (gueltig_ab <= ${len(self.params)} AND gueltig_bis isnull)"
            ")"
        )

    def namespace(self, value: str):
        self.equal("namespace", value)

    def name(self, value: str):
        self.ilike("name", f"%{value}%")

    def freigabe_status(self, value: list[FreigabeStatus]):
        self._equal_any_array_element_clause(
            "freigabe_status", [status.value for status in value]
        )

    def nummernkreis(self, value: str):
        self.ilike("nummernkreis", f"{value}%")

    def status_gesetzt_durch(self, value: str):
        self.ilike("status_gesetzt_durch", f"%{value}%")

    def status_gesetzt_durch_fts(self, value: str):
        self.fts(["status_gesetzt_durch_fts"], "status_gesetzt_durch", value)

    def status_gesetzt_seit(self, value: date):
        self._greater_equal("status_gesetzt_am", value)

    def status_gesetzt_bis(self, value: date):
        self._lower_equal("status_gesetzt_am", value)

    def bezug(self, value: str):
        self._ilike_any_clause("bezug", f"%{value}%")

    def bezug_fts(self, value: str):
        self.fts(["bezug_fts"], "raw_bezug", value)

    def bezug_unterelemente(self, value: str):
        self._ilike_any_clause("bezug_components", f"%{value}%")

    def versionshinweis(self, value: str):
        self.ilike("versionshinweis", f"%{value}%")

    def versionshinweis_fts(self, value: str):
        self.fts(["versionshinweis_fts"], "versionshinweis", value)

    def updated_since(self, column: str, value: datetime):
        self._greater_equal(column, value)

    def xdf_version(self, value: XdfVersion):
        self.equal("xdf_version", value.value)

    def feldart(self, value: xdf3.Feldart):
        self.equal("feldart", value.value)

    def datentyp(self, value: xdf3.Datentyp):
        self.equal("datentyp", value.value)

    def is_latest(self, value: bool):
        self.equal("is_latest", value)

    def dokumentart(self, value: xdf3.Dokumentart):
        self.equal("dokumentart", value.value)

    def bezeichnung(self, value: str):
        self.ilike("bezeichnung", f"%{value}%")

    def leistungsschluessel(self, value: str):
        self._ilike_any_clause("leistungsschluessel", f"%{value}%")

    def is_musterprozess(self, value: bool):
        if value:
            self.equal(
                "detaillierungsstufe", Detaillierungsstufe.MUSTERINFORMATIONEN.value
            )
        else:
            self._not_equal_or_none(
                "detaillierungsstufe", Detaillierungsstufe.MUSTERINFORMATIONEN.value
            )

    def typisierung(self, value: xzufi.leistung.LeistungsTypisierung):
        self._equal_any_column_value_clauses("typisierung", value.value)

    def leistungstyp(self, value: LeistungsTyp):
        self.equal("leistungstyp", value.value)

    def einheitlicher_ansprechpartner(self, value: bool):
        self.equal("einheitlicher_ansprechpartner", value)

    def title(self, value: str):
        self.ilike("title", f"%{value}%")

    def leistungsbezeichnung(self, value: str):
        self.ilike("leistungsbezeichnung", f"%{value}%")

    def leistungsbezeichnung_fts(self, value: str):
        self.fts(["leistungsbezeichnung_fts"], "leistungsbezeichnung", value)

    def leistungsbezeichnung_2(self, value: str):
        self.ilike("leistungsbezeichnung_2", f"%{value}%")

    def leistungsbezeichnung_2_fts(self, value: str):
        self.fts(["leistungsbezeichnung_2_fts"], "leistungsbezeichnung_2", value)

    def rechtsgrundlagen(self, value: str):
        self.ilike("rechtsgrundlagen", f"%{value}%")

    def rechtsgrundlagen_fts(self, value: str):
        self.fts(["rechtsgrundlagen_fts"], "rechtsgrundlagen", value)

    def stichwort(self, value: str):
        self._ilike_any_clause("stichwort", f"%{value}%")

    def stichwort_fts(self, value: str):
        self.fts(["stichwort_fts"], "raw_stichwort", value)

    def detaillierungsstufe(self, value: Detaillierungsstufe):
        self.equal("detaillierungsstufe", value.value)

    def anwendungsgebiet(self, value: Anwendungsgebiet):
        self._equal_any_column_value_clauses(
            "verwaltungspolitische_kodierung", value.value
        )

    def _ilike_any_clause(self, column_name: str, value: AtomicQueryParameter):
        self.params.append(value)
        self.where_clauses.append(
            f"EXISTS (SELECT 1 FROM unnest({column_name}) AS element WHERE element ILIKE ${len(self.params)})"
        )

    def _equal_any_column_value_clauses(
        self, column_name: str, value: AtomicQueryParameter
    ):
        self.params.append(value)
        self.where_clauses.append(
            f"EXISTS (SELECT 1 FROM unnest({column_name}) AS element WHERE element = ${len(self.params)})"
        )

    def not_null(self, column_name: str):
        self.where_clauses.append(f"{column_name} IS NOT NULL")

    def is_null(self, column_name: str):
        self.where_clauses.append(f"{column_name} IS NULL")

    def equal(self, column_name: str, value: AtomicQueryParameter):
        self.params.append(value)
        self.where_clauses.append(f"{column_name} = ${len(self.params)}")

    def _not_equal(self, column_name: str, value: AtomicQueryParameter):
        self.params.append(value)
        self.where_clauses.append(f"{column_name} != ${len(self.params)}")

    def _not_equal_or_none(self, column_name: str, value: AtomicQueryParameter):
        self.params.append(value)
        self.where_clauses.append(
            f"({column_name} != ${len(self.params)} OR {column_name} IS NULL)"
        )

    def ilike(self, column_name: str, value: AtomicQueryParameter):
        self.params.append(value)
        self.where_clauses.append(f"{column_name} ILIKE ${len(self.params)}")

    def _lower_equal(self, column_name: str, value: AtomicQueryParameter):
        self.params.append(value)
        self.where_clauses.append(f"{column_name} <= ${len(self.params)}")

    def _greater_equal(self, column_name: str, value: AtomicQueryParameter):
        self.params.append(value)
        self.where_clauses.append(f"{column_name} >= ${len(self.params)}")

    def _equal_any_array_element_clause(
        self, column_name: str, value: list[AtomicQueryParameter]
    ):
        self.params.append(value)
        self.where_clauses.append(f"{column_name} = ANY(${len(self.params)})")

    def _get_where_clause(self) -> str:
        return (
            "WHERE " + " AND ".join(self.where_clauses)
            if len(self.where_clauses) != 0
            else ""
        )

    async def fetch_all(
        self,
        columns: str,
        create_item: Callable[[tuple[Any, ...]], CSVSearchResult],
        connection: Connection,
        order_by: str,
    ) -> list[CSVSearchResult]:
        where_clause = self._get_where_clause()

        result = await connection.fetch(
            f"SELECT {columns} FROM {self.table} {where_clause} ORDER BY {order_by}",
            *self.params,
        )

        return [create_item(row) for row in result]

    async def paginate(
        self,
        sql_columns: str,
        parse_results: Callable[[tuple[Any, ...], str | None], T],
        connection: Connection,
        pagination_options: PaginationOptions | None,
        order_by: str,
        use_materialized_view_count: bool = False,
    ) -> PaginatedResult[T]:
        where_clause = self._get_where_clause()

        if use_materialized_view_count:
            total_count_result = await connection.fetchval(
                f"SELECT total FROM {self.table}_count"
            )
        else:
            total_count_result = await connection.fetchval(
                f"SELECT COUNT(*) FROM {self.table} {where_clause}", *self.params
            )
        assert total_count_result is not None

        if pagination_options is None:
            # No actual result page is requested, just return the total count
            return PaginatedResult.create(
                items=[],
                pagination_options=None,
                total_count=total_count_result,
            )
        else:
            # NOTE(Felix): I tried to combine the two queries into one, but this resulted in nested composite objects
            # for some resources. This is a problem, because asyncpg can currently not parse those automatically.
            # This seems to be a long-standing limitation: https://github.com/MagicStack/asyncpg/issues/360
            columns = sql_columns
            if self.fts_query_param_index is not None:
                assert self.fts_headline_column is not None
                columns += f", ts_headline('german', {self.fts_headline_column}, to_tsquery('german', ${self.fts_query_param_index}), 'MaxFragments=1, MaxWords=15, MinWords=5, StartSel=[[[, StopSel=]]]')"

            query = f"""
                SELECT {columns}
                FROM {self.table}
                {where_clause}
                ORDER BY {order_by}
                OFFSET ${len(self.params) + 1}
                LIMIT ${len(self.params) + 2}
            """
            result = await connection.fetch(
                query,
                *self.params,
                pagination_options.offset,
                pagination_options.limit,
            )

            if self.fts_query_param_index is not None:
                items = [parse_results(row, row[-1]) for row in result]
            else:
                items = [parse_results(row, None) for row in result]

            result = PaginatedResult.create(
                items=items,
                pagination_options=pagination_options,
                total_count=total_count_result,
            )
            return result


class SchemaSearchOrder(Enum):
    AKTUALISIERT_DATUM_ZEIT_DESC = "geaendert_datum_zeit_desc"
    AKTUALISIERT_DATUM_ZEIT_ASC = "geaendert_datum_zeit_asc"
    NAME_ASC = "name_asc"
    NAME_DESC = "name_desc"
    ID_ASC = "id_asc"
    ID_DESC = "id_desc"

    def to_label(self) -> str:
        match self:
            case SchemaSearchOrder.AKTUALISIERT_DATUM_ZEIT_DESC:
                return "Geändert (Neu zuerst)"
            case SchemaSearchOrder.AKTUALISIERT_DATUM_ZEIT_ASC:
                return "Geändert (Alt zuerst)"
            case SchemaSearchOrder.NAME_ASC:
                return "Name (A-Z)"
            case SchemaSearchOrder.NAME_DESC:
                return "Name (Z-A)"
            case SchemaSearchOrder.ID_ASC:
                return "ID aufsteigend"
            case SchemaSearchOrder.ID_DESC:
                return "ID absteigend"

    def to_sql(self) -> str:
        match self:
            case SchemaSearchOrder.AKTUALISIERT_DATUM_ZEIT_DESC:
                return "letzte_aenderung DESC, name ASC, fim_id ASC, fim_version ASC"
            case SchemaSearchOrder.AKTUALISIERT_DATUM_ZEIT_ASC:
                return "letzte_aenderung ASC, name ASC, fim_id ASC, fim_version ASC"
            case SchemaSearchOrder.NAME_ASC:
                return "name ASC, fim_id ASC, fim_version ASC"
            case SchemaSearchOrder.NAME_DESC:
                return "name DESC, fim_id ASC, fim_version ASC"
            case SchemaSearchOrder.ID_ASC:
                return "fim_id ASC, fim_version ASC"
            case SchemaSearchOrder.ID_DESC:
                return "fim_id DESC, fim_version ASC"


class LeistungssteckbriefSearchOrder(Enum):
    RELEVANZ = "relevance"
    ERSTELLT_DATUM_ZEIT_DESC = "erstellt_datum_zeit_desc"
    ERSTELLT_DATUM_ZEIT_ASC = "erstellt_datum_zeit_asc"
    AKTUALISIERT_DATUM_ZEIT_DESC = "geaendert_datum_zeit_desc"
    AKTUALISIERT_DATUM_ZEIT_ASC = "geaendert_datum_zeit_asc"
    TITEL_ASC = "titel_asc"
    LEISTUNGSSCHLUESSEL_ASC = "leistungsschluessel_asc"

    def to_label(self) -> str:
        match self:
            case LeistungssteckbriefSearchOrder.RELEVANZ:
                return "Relevanz"
            case LeistungssteckbriefSearchOrder.ERSTELLT_DATUM_ZEIT_DESC:
                return "Erstellt (Neu zuerst)"
            case LeistungssteckbriefSearchOrder.ERSTELLT_DATUM_ZEIT_ASC:
                return "Erstellt (Alt zuerst)"
            case LeistungssteckbriefSearchOrder.AKTUALISIERT_DATUM_ZEIT_DESC:
                return "Geändert (Neu zuerst)"
            case LeistungssteckbriefSearchOrder.AKTUALISIERT_DATUM_ZEIT_ASC:
                return "Geändert (Alt zuerst)"
            case LeistungssteckbriefSearchOrder.TITEL_ASC:
                return "Titel (A-Z)"
            case LeistungssteckbriefSearchOrder.LEISTUNGSSCHLUESSEL_ASC:
                return "Leistungsschlüssel"

    def to_sql(self, fts_query_param_index: int | None = None) -> str:
        match self:
            case LeistungssteckbriefSearchOrder.RELEVANZ:
                if fts_query_param_index is not None:
                    return f"(leistungsbezeichnung_fts @@ to_tsquery('german', ${fts_query_param_index}))::int DESC, (leistungsbezeichnung_2_fts @@ to_tsquery('german', ${fts_query_param_index}))::int DESC, title ASC, root_for_leistungsschluessel ASC"
                return "title ASC, root_for_leistungsschluessel ASC"
            case LeistungssteckbriefSearchOrder.ERSTELLT_DATUM_ZEIT_DESC:
                return "erstellt_datum_zeit DESC NULLS LAST, title ASC"
            case LeistungssteckbriefSearchOrder.ERSTELLT_DATUM_ZEIT_ASC:
                return "erstellt_datum_zeit ASC NULLS LAST, title ASC"
            case LeistungssteckbriefSearchOrder.AKTUALISIERT_DATUM_ZEIT_DESC:
                return "geaendert_datum_zeit DESC NULLS LAST, title ASC"
            case LeistungssteckbriefSearchOrder.AKTUALISIERT_DATUM_ZEIT_ASC:
                return "geaendert_datum_zeit ASC NULLS LAST, title ASC"
            case LeistungssteckbriefSearchOrder.TITEL_ASC:
                return "title ASC, root_for_leistungsschluessel ASC"
            case LeistungssteckbriefSearchOrder.LEISTUNGSSCHLUESSEL_ASC:
                return "root_for_leistungsschluessel ASC"


class LeistungsbeschreibungSearchOrder(Enum):
    RELEVANZ = "relevance"
    ERSTELLT_DATUM_ZEIT_DESC = "erstellt_datum_zeit_desc"
    ERSTELLT_DATUM_ZEIT_ASC = "erstellt_datum_zeit_asc"
    AKTUALISIERT_DATUM_ZEIT_DESC = "geaendert_datum_zeit_desc"
    AKTUALISIERT_DATUM_ZEIT_ASC = "geaendert_datum_zeit_asc"
    TITEL_ASC = "titel_asc"
    LEISTUNGSSCHLUESSEL_ASC = "leistungsschluessel_asc"

    def to_label(self) -> str:
        match self:
            case LeistungsbeschreibungSearchOrder.RELEVANZ:
                return "Relevanz"
            case LeistungsbeschreibungSearchOrder.ERSTELLT_DATUM_ZEIT_DESC:
                return "Erstellt (Neu zuerst)"
            case LeistungsbeschreibungSearchOrder.ERSTELLT_DATUM_ZEIT_ASC:
                return "Erstellt (Alt zuerst)"
            case LeistungsbeschreibungSearchOrder.AKTUALISIERT_DATUM_ZEIT_DESC:
                return "Geändert (Neu zuerst)"
            case LeistungsbeschreibungSearchOrder.AKTUALISIERT_DATUM_ZEIT_ASC:
                return "Geändert (Alt zuerst)"
            case LeistungsbeschreibungSearchOrder.TITEL_ASC:
                return "Titel (A-Z)"
            case LeistungsbeschreibungSearchOrder.LEISTUNGSSCHLUESSEL_ASC:
                return "Leistungsschlüssel"

    def to_sql(self, fts_query_param_index: int | None = None) -> str:
        match self:
            case LeistungsbeschreibungSearchOrder.RELEVANZ:
                if fts_query_param_index is not None:
                    return f"(leistungsbezeichnung_fts @@ to_tsquery('german', ${fts_query_param_index}))::int DESC, (leistungsbezeichnung_2_fts @@ to_tsquery('german', ${fts_query_param_index}))::int DESC, title ASC, leistungsschluessel ASC"
                return "title ASC, leistungsschluessel ASC"
            case LeistungsbeschreibungSearchOrder.ERSTELLT_DATUM_ZEIT_DESC:
                return "erstellt_datum_zeit DESC NULLS LAST, title ASC, leistungsschluessel ASC"
            case LeistungsbeschreibungSearchOrder.ERSTELLT_DATUM_ZEIT_ASC:
                return "erstellt_datum_zeit ASC NULLS LAST, title ASC, leistungsschluessel ASC"
            case LeistungsbeschreibungSearchOrder.AKTUALISIERT_DATUM_ZEIT_DESC:
                return "geaendert_datum_zeit DESC NULLS LAST, title ASC, leistungsschluessel ASC"
            case LeistungsbeschreibungSearchOrder.AKTUALISIERT_DATUM_ZEIT_ASC:
                return "geaendert_datum_zeit ASC NULLS LAST, title ASC, leistungsschluessel ASC"
            case LeistungsbeschreibungSearchOrder.TITEL_ASC:
                return "title ASC, leistungsschluessel ASC"
            case LeistungsbeschreibungSearchOrder.LEISTUNGSSCHLUESSEL_ASC:
                return "leistungsschluessel ASC"


class LeistungSucheIn(Enum):
    LEISTUNGSSCHLUESSEL = "leistungsschluessel"
    RECHTSGRUNDLAGEN = "rechtsgrundlagen"
    LEISTUNGSBEZEICHNUNG = "leistungsbezeichnung"
    LEISTUNGSBEZEICHNUNG_II = "leistungsbezeichnung_II"

    def to_label(self) -> str:
        match self:
            case LeistungSucheIn.LEISTUNGSSCHLUESSEL:
                return "Leistungsschlüssel"
            case LeistungSucheIn.RECHTSGRUNDLAGEN:
                return "Handlungsgrundlage"
            case LeistungSucheIn.LEISTUNGSBEZEICHNUNG:
                return "Leistungsbezeichnung"
            case LeistungSucheIn.LEISTUNGSBEZEICHNUNG_II:
                return "Leistungsbezeichnung II"


@dataclass(slots=True)
class LeistungssteckbriefSearchOptions:
    order_by: LeistungssteckbriefSearchOrder
    title: str | None = None
    leistungsbezeichnung: str | None = None
    leistungsbezeichnung_2: str | None = None
    leistungstyp: LeistungsTyp | None = None
    typisierung: xzufi.leistung.LeistungsTypisierung | None = None
    leistungsschluessel: str | None = None
    rechtsgrundlagen: str | None = None
    freigabe_status_katalog: FreigabeStatus | None = None
    freigabe_status_bibliothek: FreigabeStatus | None = None
    einheitlicher_ansprechpartner: bool | None = None
    fts_query: str | None = None
    updated_since: datetime | None = None
    suche_nur_in: LeistungSucheIn | None = None

    pagination_options: PaginationOptions | None = None

    def create_query(self) -> SearchQuery:
        query = SearchQuery("leistung")
        query.not_null("root_for_leistungsschluessel")

        if self.updated_since is not None:
            query.updated_since("geaendert_datum_zeit", self.updated_since)

        if self.einheitlicher_ansprechpartner is not None:
            query.einheitlicher_ansprechpartner(self.einheitlicher_ansprechpartner)

        if self.leistungstyp is not None:
            query.leistungstyp(self.leistungstyp)

        if self.typisierung is not None:
            query.typisierung(self.typisierung)

        # TODO: Use root_for_leistungsschluessel here, as this must be
        # set for steckbriefe/stammtexte
        if self.leistungsschluessel is not None:
            query.leistungsschluessel(self.leistungsschluessel)

        if self.rechtsgrundlagen is not None:
            query.rechtsgrundlagen(self.rechtsgrundlagen)

        if self.title is not None:
            query.title(self.title)

        if self.leistungsbezeichnung is not None:
            query.leistungsbezeichnung(self.leistungsbezeichnung)

        if self.leistungsbezeichnung_2 is not None:
            query.leistungsbezeichnung_2(self.leistungsbezeichnung_2)

        if self.freigabe_status_katalog is not None:
            query.equal("freigabestatus_katalog", self.freigabe_status_katalog.value)

        if self.freigabe_status_bibliothek is not None:
            query.equal(
                "freigabestatus_bibliothek", self.freigabe_status_bibliothek.value
            )

        if self.fts_query is not None:
            match self.suche_nur_in:
                case None:
                    if self.order_by == LeistungssteckbriefSearchOrder.RELEVANZ:
                        search_columns = [
                            "leistungsbezeichnung_fts",
                            "leistungsbezeichnung_2_fts",
                            "raw_content_fts",
                        ]
                    else:
                        search_columns = ["raw_content_fts"]

                    query.fts(
                        search_columns=search_columns,
                        headline_column="raw_content",
                        search_term=self.fts_query,
                    )
                case LeistungSucheIn.RECHTSGRUNDLAGEN:
                    query.rechtsgrundlagen_fts(self.fts_query)
                case LeistungSucheIn.LEISTUNGSBEZEICHNUNG:
                    query.leistungsbezeichnung_fts(self.fts_query)
                case LeistungSucheIn.LEISTUNGSBEZEICHNUNG_II:
                    query.leistungsbezeichnung_2_fts(self.fts_query)
                case _:
                    pass

        return query


@dataclass(slots=True)
class LeistungsbeschreibungSearchOptions:
    order_by: LeistungsbeschreibungSearchOrder
    redaktion_id: str | None = None
    title: str | None = None
    leistungsbezeichnung: str | None = None
    leistungsbezeichnung_2: str | None = None
    leistungsschluessel: str | None = None
    rechtsgrundlagen: str | None = None
    leistungstyp: LeistungsTyp | None = None
    typisierung: xzufi.leistung.LeistungsTypisierung | None = None
    einheitlicher_ansprechpartner: bool | None = None
    updated_since: datetime | None = None
    suche_nur_in: LeistungSucheIn | None = None
    fts_query: str | None = None
    pagination_options: PaginationOptions | None = None

    def create_query(self) -> SearchQuery:
        query = SearchQuery("leistung")
        # This is set if and only if the Leistung is from the LeiKa
        # Since we want to search through Leistungsbeschreibungen here,
        # this must not be set.
        query.is_null("root_for_leistungsschluessel")

        if self.updated_since is not None:
            query.updated_since("geaendert_datum_zeit", self.updated_since)

        if self.einheitlicher_ansprechpartner is not None:
            query.einheitlicher_ansprechpartner(self.einheitlicher_ansprechpartner)

        if self.leistungsschluessel is not None:
            query.leistungsschluessel(self.leistungsschluessel)

        if self.redaktion_id is not None:
            query.equal("redaktion_id", self.redaktion_id)

        if self.rechtsgrundlagen is not None:
            query.rechtsgrundlagen(self.rechtsgrundlagen)

        if self.leistungsbezeichnung is not None:
            query.leistungsbezeichnung(self.leistungsbezeichnung)

        if self.leistungsbezeichnung_2 is not None:
            query.leistungsbezeichnung_2(self.leistungsbezeichnung_2)

        if self.leistungstyp is not None:
            query.leistungstyp(self.leistungstyp)

        if self.typisierung is not None:
            query.typisierung(self.typisierung)

        if self.title is not None:
            query.title(self.title)

        if self.fts_query is not None:
            match self.suche_nur_in:
                case None:
                    if self.order_by == LeistungsbeschreibungSearchOrder.RELEVANZ:
                        search_columns = [
                            "leistungsbezeichnung_fts",
                            "leistungsbezeichnung_2_fts",
                            "raw_content_fts",
                        ]
                    else:
                        search_columns = ["raw_content_fts"]

                    query.fts(
                        search_columns=search_columns,
                        headline_column="raw_content",
                        search_term=self.fts_query,
                    )
                case LeistungSucheIn.RECHTSGRUNDLAGEN:
                    query.rechtsgrundlagen_fts(self.fts_query)
                case LeistungSucheIn.LEISTUNGSBEZEICHNUNG:
                    query.leistungsbezeichnung_fts(self.fts_query)
                case LeistungSucheIn.LEISTUNGSBEZEICHNUNG_II:
                    query.leistungsbezeichnung_2_fts(self.fts_query)
                case _:
                    pass

        return query


@dataclass(slots=True)
class ProzessSearchOptions:
    is_musterprozess: bool | None = None
    freigabe_status: list[FreigabeStatus] | None = None
    detaillierungsstufe: Detaillierungsstufe | None = None
    anwendungsgebiet: Anwendungsgebiet | None = None
    fts_query: str | None = None
    pagination_options: PaginationOptions | None = None

    def create_query(self) -> SearchQuery:
        query = SearchQuery("prozess")

        if self.detaillierungsstufe is not None:
            query.detaillierungsstufe(self.detaillierungsstufe)

        if self.anwendungsgebiet is not None:
            query.anwendungsgebiet(self.anwendungsgebiet)

        if self.freigabe_status is not None:
            query.freigabe_status(self.freigabe_status)

        if self.is_musterprozess is not None:
            query.is_musterprozess(self.is_musterprozess)

        if self.fts_query is not None:
            query.fts(
                search_columns=["raw_content_fts"],
                headline_column="raw_content_without_visualization",
                search_term=self.fts_query,
            )

        return query


@dataclass(slots=True)
class ProzessklasseSearchOptions:
    operatives_ziel: OperativesZiel | None = None
    verfahrensart: Verfahrensart | None = None
    handlungsform: Handlungsform | None = None
    freigabe_status: list[FreigabeStatus] | None = None
    fts_query: str | None = None
    pagination_options: PaginationOptions | None = None

    def create_query(self) -> SearchQuery:
        query = SearchQuery("prozessklasse")

        if self.freigabe_status is not None:
            query.freigabe_status(self.freigabe_status)

        if self.operatives_ziel is not None:
            query.equal("operatives_ziel", self.operatives_ziel.value)

        if self.verfahrensart is not None:
            query.equal("verfahrensart", self.verfahrensart.value)

        if self.handlungsform is not None:
            query.equal("handlungsform", self.handlungsform.value)

        if self.fts_query is not None:
            query.fts(
                search_columns=["raw_content_fts"],
                headline_column="raw_content",
                search_term=self.fts_query,
            )

        return query


class SchemaSucheIn(Enum):
    RECHTSGRUNDLAGEN = "Rechtsgrundlagen"
    STATUS_GESETZT_DURCH = "Status_gesetzt_durch"
    VERSIONSHINWEIS = "Versionshinweis"
    STICHWORT = "Stichwort"

    def to_label(self) -> str:
        match self:
            case SchemaSucheIn.RECHTSGRUNDLAGEN:
                return "Handlungsgrundlage"
            case SchemaSucheIn.STATUS_GESETZT_DURCH:
                return "Status gesetzt durch"
            case SchemaSucheIn.VERSIONSHINWEIS:
                return "Versionshinweis"
            case SchemaSucheIn.STICHWORT:
                return "Stichwort"


class SteckbriefSucheIn(Enum):
    RECHTSGRUNDLAGEN = "Rechtsgrundlagen"
    STATUS_GESETZT_DURCH = "Status_gesetzt_durch"
    VERSIONSHINWEIS = "Versionshinweis"

    def to_label(self) -> str:
        match self:
            case SteckbriefSucheIn.RECHTSGRUNDLAGEN:
                return "Handlungsgrundlage"
            case SteckbriefSucheIn.STATUS_GESETZT_DURCH:
                return "Status gesetzt durch"
            case SteckbriefSucheIn.VERSIONSHINWEIS:
                return "Versionshinweis"


class GruppeSucheIn(Enum):
    RECHTSGRUNDLAGEN = "Rechtsgrundlagen"
    STATUS_GESETZT_DURCH = "Status_gesetzt_durch"
    VERSIONSHINWEIS = "Versionshinweis"

    def to_label(self) -> str:
        match self:
            case GruppeSucheIn.RECHTSGRUNDLAGEN:
                return "Handlungsgrundlage"
            case GruppeSucheIn.STATUS_GESETZT_DURCH:
                return "Status gesetzt durch"
            case GruppeSucheIn.VERSIONSHINWEIS:
                return "Versionshinweis"


class FeldSucheIn(Enum):
    RECHTSGRUNDLAGEN = "Rechtsgrundlagen"
    STATUS_GESETZT_DURCH = "Status_gesetzt_durch"
    VERSIONSHINWEIS = "Versionshinweis"

    def to_label(self) -> str:
        match self:
            case FeldSucheIn.RECHTSGRUNDLAGEN:
                return "Handlungsgrundlage"
            case FeldSucheIn.STATUS_GESETZT_DURCH:
                return "Status gesetzt durch"
            case FeldSucheIn.VERSIONSHINWEIS:
                return "Versionshinweis"


@dataclass(slots=True)
class GroupSearchOptions:
    name: str | None = None
    nummernkreis: str | None = None
    freigabe_status: list[FreigabeStatus] | None = None
    gueltig_am: date | None = None
    status_gesetzt_durch: str | None = None
    status_gesetzt_seit: date | None = None
    status_gesetzt_bis: date | None = None
    bezug: str | None = None
    versionshinweis: str | None = None
    updated_since: datetime | None = None
    xdf_version: XdfVersion | None = None
    is_latest: bool | None = None
    fts_query: str | None = None
    suche_nur_in: GruppeSucheIn | None = None
    pagination_options: PaginationOptions | None = None

    def create_query(self) -> SearchQuery:
        query = SearchQuery("datenfeldgruppe")
        query.namespace("baukasten")

        if self.name is not None:
            query.name(self.name)

        if self.freigabe_status is not None:
            query.freigabe_status(self.freigabe_status)

        if self.nummernkreis is not None:
            query.nummernkreis(self.nummernkreis)

        if self.gueltig_am is not None:
            query.gueltig_am(self.gueltig_am)

        if self.status_gesetzt_durch is not None:
            query.status_gesetzt_durch(self.status_gesetzt_durch)

        if self.status_gesetzt_seit is not None:
            query.status_gesetzt_seit(self.status_gesetzt_seit)

        if self.status_gesetzt_bis is not None:
            query.status_gesetzt_bis(self.status_gesetzt_bis)

        if self.bezug is not None:
            query.bezug(self.bezug)

        if self.versionshinweis is not None:
            query.versionshinweis(self.versionshinweis)

        if self.updated_since is not None:
            query.updated_since("last_update", self.updated_since)

        if self.xdf_version is not None:
            query.xdf_version(self.xdf_version)

        if self.is_latest is not None:
            query.is_latest(self.is_latest)

        if self.fts_query is not None:
            match self.suche_nur_in:
                case None:
                    query.fts(
                        search_columns=["raw_content_fts"],
                        headline_column="raw_content",
                        search_term=self.fts_query,
                    )
                case GruppeSucheIn.STATUS_GESETZT_DURCH:
                    query.status_gesetzt_durch_fts(self.fts_query)
                case GruppeSucheIn.RECHTSGRUNDLAGEN:
                    query.bezug_fts(self.fts_query)
                case GruppeSucheIn.VERSIONSHINWEIS:
                    query.versionshinweis_fts(self.fts_query)

        return query


@dataclass(slots=True)
class FieldSearchOptions:
    name: str | None = None
    nummernkreis: str | None = None
    freigabe_status: list[FreigabeStatus] | None = None
    gueltig_am: date | None = None
    status_gesetzt_durch: str | None = None
    status_gesetzt_seit: date | None = None
    status_gesetzt_bis: date | None = None
    bezug: str | None = None
    versionshinweis: str | None = None
    updated_since: datetime | None = None
    xdf_version: XdfVersion | None = None
    feldart: xdf3.Feldart | None = None
    datentyp: xdf3.Datentyp | None = None
    fts_query: str | None = None
    suche_nur_in: FeldSucheIn | None = None
    is_latest: bool | None = None
    pagination_options: PaginationOptions | None = None

    def create_query(self) -> SearchQuery:
        query = SearchQuery("datenfeld")
        query.namespace("baukasten")

        if self.name is not None:
            query.name(self.name)

        if self.freigabe_status is not None:
            query.freigabe_status(self.freigabe_status)

        if self.nummernkreis is not None:
            query.nummernkreis(self.nummernkreis)

        if self.gueltig_am is not None:
            query.gueltig_am(self.gueltig_am)

        if self.status_gesetzt_durch is not None:
            query.status_gesetzt_durch(self.status_gesetzt_durch)

        if self.status_gesetzt_seit is not None:
            query.status_gesetzt_seit(self.status_gesetzt_seit)

        if self.status_gesetzt_bis is not None:
            query.status_gesetzt_bis(self.status_gesetzt_bis)

        if self.bezug is not None:
            query.bezug(self.bezug)

        if self.versionshinweis is not None:
            query.versionshinweis(self.versionshinweis)

        if self.updated_since is not None:
            query.updated_since("last_update", self.updated_since)

        if self.xdf_version is not None:
            query.xdf_version(self.xdf_version)

        if self.feldart is not None:
            query.feldart(self.feldart)

        if self.datentyp is not None:
            query.datentyp(self.datentyp)

        if self.is_latest is not None:
            query.is_latest(self.is_latest)

        if self.fts_query is not None:
            match self.suche_nur_in:
                case None:
                    query.fts(
                        search_columns=["raw_content_fts"],
                        headline_column="raw_content",
                        search_term=self.fts_query,
                    )
                case FeldSucheIn.STATUS_GESETZT_DURCH:
                    query.status_gesetzt_durch_fts(self.fts_query)
                case FeldSucheIn.RECHTSGRUNDLAGEN:
                    query.bezug_fts(self.fts_query)
                case FeldSucheIn.VERSIONSHINWEIS:
                    query.versionshinweis_fts(self.fts_query)

        return query


@dataclass(slots=True)
class SteckbriefSearchOptions:
    name: str | None = None
    nummernkreis: str | None = None
    freigabe_status: list[FreigabeStatus] | None = None
    status_gesetzt_durch: str | None = None
    status_gesetzt_seit: date | None = None
    status_gesetzt_bis: date | None = None
    bezeichnung: str | None = None
    dokumentart: xdf3.Dokumentart | None = None
    bezug: str | None = None
    fts_query: str | None = None
    suche_nur_in: SteckbriefSucheIn | None = None
    versionshinweis: str | None = None
    updated_since: datetime | None = None
    xdf_version: XdfVersion | None = None
    stichwort: str | None = None
    is_latest: bool | None = None
    pagination_options: PaginationOptions | None = None

    def create_query(self) -> SearchQuery:
        query = SearchQuery("dokumentensteckbrief")

        if self.name is not None:
            query.name(self.name)

        if self.freigabe_status is not None:
            query.freigabe_status(self.freigabe_status)

        if self.dokumentart is not None:
            query.dokumentart(self.dokumentart)

        if self.nummernkreis is not None:
            query.nummernkreis(self.nummernkreis)

        if self.status_gesetzt_durch is not None:
            query.status_gesetzt_durch(self.status_gesetzt_durch)

        if self.status_gesetzt_bis is not None:
            query.status_gesetzt_bis(self.status_gesetzt_bis)

        if self.status_gesetzt_seit is not None:
            query.status_gesetzt_seit(self.status_gesetzt_seit)

        if self.bezeichnung is not None:
            query.bezeichnung(self.bezeichnung)

        if self.bezug is not None:
            query.bezug(self.bezug)

        if self.stichwort is not None:
            query.stichwort(self.stichwort)

        if self.versionshinweis:
            query.versionshinweis(self.versionshinweis)

        if self.updated_since:
            query.updated_since("last_update", self.updated_since)

        if self.xdf_version:
            query.xdf_version(self.xdf_version)

        if self.fts_query is not None:
            match self.suche_nur_in:
                case None:
                    query.fts(
                        search_columns=["raw_content_fts"],
                        headline_column="raw_content",
                        search_term=self.fts_query,
                    )
                case SteckbriefSucheIn.STATUS_GESETZT_DURCH:
                    query.status_gesetzt_durch_fts(self.fts_query)
                case SteckbriefSucheIn.RECHTSGRUNDLAGEN:
                    query.bezug_fts(self.fts_query)
                case SteckbriefSucheIn.VERSIONSHINWEIS:
                    query.versionshinweis_fts(self.fts_query)

        if self.is_latest is not None:
            query.is_latest(self.is_latest)

        return query


@dataclass(slots=True)
class SchemaSearchOptions:
    order_by: SchemaSearchOrder
    name: str | None = None
    nummernkreis: str | None = None
    freigabe_status: list[FreigabeStatus] | None = None
    gueltig_am: date | None = None
    status_gesetzt_durch: str | None = None
    status_gesetzt_seit: date | None = None
    status_gesetzt_bis: date | None = None
    bezug: str | None = None
    bezug_unterelemente: str | None = None
    versionshinweis: str | None = None
    bezeichnung: str | None = None
    updated_since: datetime | None = None
    stichwort: str | None = None
    xdf_version: XdfVersion | None = None
    fts_query: str | None = None
    suche_nur_in: SchemaSucheIn | None = None
    is_latest: bool | None = None
    pagination_options: PaginationOptions | None = None

    def create_query(self) -> SearchQuery:
        query = SearchQuery("schema")

        if self.name is not None:
            query.name(self.name)

        if self.freigabe_status is not None:
            query.freigabe_status(self.freigabe_status)

        if self.nummernkreis:
            query.nummernkreis(self.nummernkreis)

        if self.gueltig_am:
            query.gueltig_am(self.gueltig_am)

        if self.status_gesetzt_durch is not None:
            query.status_gesetzt_durch(self.status_gesetzt_durch)

        if self.status_gesetzt_seit is not None:
            query.status_gesetzt_seit(self.status_gesetzt_seit)

        if self.status_gesetzt_bis is not None:
            query.status_gesetzt_bis(self.status_gesetzt_bis)

        if self.bezug:
            query.bezug(self.bezug)

        if self.bezug_unterelemente:
            query.bezug_unterelemente(self.bezug_unterelemente)

        if self.bezeichnung:
            query.bezeichnung(self.bezeichnung)

        if self.versionshinweis:
            query.versionshinweis(self.versionshinweis)

        if self.updated_since:
            query.updated_since("last_update", self.updated_since)

        if self.stichwort:
            query.stichwort(self.stichwort)

        if self.xdf_version:
            query.xdf_version(self.xdf_version)

        if self.is_latest is not None:
            query.is_latest(self.is_latest)

        if self.fts_query is not None:
            match self.suche_nur_in:
                case None:
                    query.fts(
                        search_columns=["raw_content_fts"],
                        headline_column="raw_content",
                        search_term=self.fts_query,
                    )
                case SchemaSucheIn.STATUS_GESETZT_DURCH:
                    query.status_gesetzt_durch_fts(self.fts_query)
                case SchemaSucheIn.RECHTSGRUNDLAGEN:
                    query.bezug_fts(self.fts_query)
                case SchemaSucheIn.VERSIONSHINWEIS:
                    query.versionshinweis_fts(self.fts_query)
                case SchemaSucheIn.STICHWORT:
                    query.stichwort_fts(self.fts_query)

        return query


@dataclass(slots=True)
class ImmutableResource:
    type: Literal["schema", "field", "group", "rule"]
    namespace: str | None
    fim_id: str
    fim_version: str
    immutable_json: str
    veroeffentlichungsdatum: date | None
    freigabe_status: FreigabeStatus | None
    code_list_id: int | None


@dataclass(slots=True)
class XdfImportData:
    version: XdfVersion
    xml_content: str
    steckbrief_id: str | None
    filename: str


@dataclass(slots=True)
class XzufiContentInfo:
    source: XzufiSource
    content: str


@dataclass(slots=True)
class SchemaTreeResult:
    groups: list[DatenfeldgruppeOut]
    fields: list[DatenfeldOut]

    schema_children: list[DirectChild]
    group_to_children: dict[tuple[str, str, str], list[DirectChild]]


@dataclass(slots=True)
class SchemaTree:
    """
    All of the information to reconstruct the full tree of children in a schema.
    """

    schema_children: list[DirectChild]
    group_to_children: dict[ElementIdentifierTuple, list[DirectChild]]
    group_identifiers: set[ElementIdentifierTuple]
    field_identifiers: set[ElementIdentifierTuple]


@dataclass(slots=True)
class GroupTree:
    """
    All of the information to reconstruct the full tree of children in a group.

    The `group_identifiers` will not include the root group itself, only all (transitive) child group identifiers.
    """

    group_to_children: dict[ElementIdentifierTuple, list[DirectChild]]
    group_identifiers: set[ElementIdentifierTuple]
    field_identifiers: set[ElementIdentifierTuple]


class Database:
    def __init__(self, connection: Connection):
        self.connection = connection

    async def save_steckbrief(
        self,
        steckbrief: NewSteckbrief,
        xml_content: str,
    ):
        """
        Insert or update a Dokumentsteckbrief.

        This will:
            - Create the steckbrief or update all mutable columns
            - Delete all outdated relations
            - Create new relations where necessary
        """

        await self.connection.execute(
            """
            WITH upsert AS (
                INSERT INTO dokumentensteckbrief (
                    fim_id,
                    fim_version,
                    nummernkreis,
                    name,
                    definition,
                    bezeichnung,
                    beschreibung,
                    freigabe_status,
                    versionshinweis,
                    status_gesetzt_durch,
                    status_gesetzt_am,
                    gueltig_ab,
                    gueltig_bis,
                    bezug,
                    veroeffentlichungsdatum,
                    stichwort,
                    letzte_aenderung,
                    last_update,
                    dokumentart,
                    ist_abstrakt,
                    hilfetext,
                    xdf_version,
                    xml_content,
                    immutable_json
                ) VALUES (
                    $1,
                    $2,
                    $3,
                    $4,
                    $5,
                    $6,
                    $7,
                    $8,
                    $9,
                    $10,
                    $11,
                    $12,
                    $13,
                    $14,
                    $15,
                    $16,
                    $17,
                    $18,
                    $19,
                    $20,
                    $21,
                    $22,
                    $23,
                    $24
                ) 
                ON CONFLICT (fim_id, fim_version) 
                DO UPDATE SET
                    freigabe_status = EXCLUDED.freigabe_status,
                    status_gesetzt_am = EXCLUDED.status_gesetzt_am,
                    veroeffentlichungsdatum = EXCLUDED.veroeffentlichungsdatum,
                    letzte_aenderung = EXCLUDED.letzte_aenderung,
                    last_update = EXCLUDED.last_update,
                    xml_content = EXCLUDED.xml_content
            ), relation_targets AS (
                SELECT * FROM unnest(
                    $25::text[],
                    $26::text[],
                    $27::text[]
                ) as t(target_fim_id, target_fim_version, praedikat)
            ), insert_relations AS (
                INSERT INTO dokumentsteckbrief_relations (
                    source_fim_id,
                    source_fim_version,
                    target_fim_id,
                    target_fim_version,
                    praedikat
                ) (
                    SELECT 
                        $1, 
                        $2, 
                        target_fim_id, 
                        target_fim_version, 
                        praedikat 
                    FROM relation_targets
                ) ON CONFLICT DO NOTHING
            )

            DELETE FROM dokumentsteckbrief_relations
            WHERE source_fim_id = $1
            AND source_fim_version = $2
            AND (target_fim_id, target_fim_version, praedikat) NOT IN (
                SELECT * FROM relation_targets
            )
            """,
            steckbrief.fim_id,
            steckbrief.fim_version,
            steckbrief.nummernkreis,
            steckbrief.name,
            steckbrief.definition,
            steckbrief.bezeichnung,
            steckbrief.beschreibung,
            steckbrief.freigabe_status.value,
            steckbrief.versionshinweis,
            steckbrief.status_gesetzt_durch,
            steckbrief.status_gesetzt_am,
            steckbrief.gueltig_ab,
            steckbrief.gueltig_bis,
            steckbrief.bezug,
            steckbrief.veroeffentlichungsdatum,
            steckbrief.stichwort,
            steckbrief.letzte_aenderung,
            steckbrief.last_update,
            steckbrief.dokumentart.value,
            steckbrief.ist_abstrakt,
            steckbrief.hilfetext,
            steckbrief.xdf_version.value,
            xml_content,
            orjson.dumps(steckbrief.immutable_json).decode(),
            [relation.objekt.id for relation in steckbrief.relation],
            [relation.objekt.version for relation in steckbrief.relation],
            [relation.praedikat.value for relation in steckbrief.relation],
        )

    async def get_dokumentsteckbrief_csv_search_results(
        self, options: SteckbriefSearchOptions
    ) -> list[CSVSearchResult]:
        query = options.create_query()

        return await query.fetch_all(
            CSVSearchResult.DATENFELDER_SQL_COLUMNS,
            CSVSearchResult.from_dokumentsteckbrief,
            self.connection,
            order_by="fim_id, fim_version",
        )

    async def search_dokumentsteckbriefe(
        self, options: SteckbriefSearchOptions
    ) -> PaginatedResult[SteckbriefOut]:
        query = options.create_query()

        return await query.paginate(
            SteckbriefOut.SQL_COLUMNS,
            SteckbriefOut.from_row,
            self.connection,
            options.pagination_options,
            order_by="fim_id, fim_version",
        )

    async def load_steckbrief_versions(self, fim_id: str) -> list[SteckbriefOut]:
        result = await self.connection.fetch(
            f"""
                SELECT {SteckbriefOut.SQL_COLUMNS}
                FROM dokumentensteckbrief
                WHERE dokumentensteckbrief.fim_id = $1
                """,
            fim_id,
        )

        return [SteckbriefOut.from_row(row) for row in result]

    async def load_steckbrief_freigabestatus_for_update(
        self, steckbrief_id: str, steckbrief_version: str
    ) -> xdf3.FreigabeStatus | None:
        value = await self.connection.fetchval(
            """
            SELECT freigabe_status
            FROM dokumentensteckbrief
            WHERE fim_id = $1
            AND fim_version = $2
            FOR UPDATE;
            """,
            steckbrief_id,
            steckbrief_version,
        )

        return xdf3.FreigabeStatus(value) if value is not None else value

    async def load_steckbrief(
        self, steckbrief_id: str, steckbrief_version: str
    ) -> FullSteckbriefOut | None:
        row = await self.connection.fetchrow(
            f"""
                SELECT {FullSteckbriefOut.SQL_COLUMNS}
                FROM dokumentensteckbrief
                WHERE dokumentensteckbrief.fim_id = $1
                AND dokumentensteckbrief.fim_version = $2
                """,
            steckbrief_id,
            steckbrief_version,
        )
        if row is None:
            return None

        return FullSteckbriefOut.from_row(row)

    async def load_latest_steckbrief(
        self, steckbrief_id: str
    ) -> FullSteckbriefOut | None:
        row = await self.connection.fetchrow(
            f"""
                SELECT {FullSteckbriefOut.SQL_COLUMNS}
                FROM dokumentensteckbrief
                WHERE fim_id = $1
                AND is_latest = true
                """,
            steckbrief_id,
        )
        if row is None:
            return None

        return FullSteckbriefOut.from_row(row)

    async def load_rule(self, rule_id: str, rule_version: str) -> RuleOut | None:
        row = await self.connection.fetchrow(
            """
                SELECT 
                    fim_id,
                    fim_version,
                    nummernkreis,
                    name,
                    letzte_aenderung,
                    last_update,
                    xdf_version
                FROM regel
                WHERE regel.fim_id = $1
                AND regel.fim_version = $2
                """,
            rule_id,
            rule_version,
        )
        if row is None:
            return None

        return RuleOut.from_row(row)

    async def load_rule_xml_content(self, fim_id: str, fim_version: str) -> str | None:
        return await self.connection.fetchval(
            """
            SELECT
                xml_content
            FROM
                regel
            WHERE
                fim_id = $1 AND fim_version = $2
            """,
            fim_id,
            fim_version,
        )

    async def upsert_rules(
        self,
        rules: list[NewRule],
    ):
        await self.connection.executemany(
            """
            INSERT INTO regel (
                fim_id,
                fim_version,
                nummernkreis,
                name,
                letzte_aenderung,
                veroeffentlichungsdatum,
                last_update,
                xdf_version,
                xml_content,
                immutable_json
            ) VALUES (
                $1,
                $2,
                $3,
                $4,
                $5,
                $6,
                $7,
                $8,
                $9,
                $10
            ) 
            ON CONFLICT (fim_id, fim_version) 
            DO UPDATE SET
                name = EXCLUDED.name,
                letzte_aenderung = EXCLUDED.letzte_aenderung,
                veroeffentlichungsdatum = EXCLUDED.veroeffentlichungsdatum,
                last_update = EXCLUDED.last_update,
                xdf_version = EXCLUDED.xdf_version,
                xml_content = EXCLUDED.xml_content,
                immutable_json = EXCLUDED.immutable_json
            """,
            [
                (
                    rule.fim_id,
                    rule.fim_version,
                    rule.nummernkreis,
                    rule.name,
                    rule.letzte_aenderung,
                    rule.veroeffentlichungsdatum,
                    rule.last_update,
                    rule.xdf_version.value,
                    rule.xml_content,
                    orjson.dumps(rule.immutable_json).decode(),
                )
                for rule in rules
            ],
        )

    async def create_rules(self, rules: list[NewRule]):
        if len(rules) == 0:
            return

        await self.connection.executemany(
            """
            INSERT INTO regel (
                fim_id,
                fim_version,
                nummernkreis,
                name,
                letzte_aenderung,
                veroeffentlichungsdatum,
                last_update,
                xdf_version,
                xml_content,
                immutable_json,
                freigabe_status,
                status_gesetzt_am
            ) VALUES (
                $1,
                $2,
                $3,
                $4,
                $5,
                $6,
                $7,
                $8,
                $9,
                $10,
                $11,
                $12
            )
            """,
            [
                (
                    rule.fim_id,
                    rule.fim_version,
                    rule.nummernkreis,
                    rule.name,
                    rule.letzte_aenderung,
                    rule.veroeffentlichungsdatum,
                    rule.last_update,
                    rule.xdf_version.value,
                    rule.xml_content,
                    orjson.dumps(rule.immutable_json).decode(),
                    rule.freigabe_status.value
                    if rule.freigabe_status is not None
                    else None,
                    rule.status_gesetzt_am,
                )
                for rule in rules
            ],
        )

    async def update_rules(self, rules: list[NewRule]):
        if len(rules) == 0:
            return

        await self.connection.executemany(
            """
            UPDATE regel
            SET
                letzte_aenderung = $3,
                veroeffentlichungsdatum = $4,
                last_update = $5,
                xml_content = $6,
                freigabe_status = $7,
                status_gesetzt_am = $8
            WHERE fim_id = $1
            AND fim_version = $2
            """,
            [
                (
                    rule.fim_id,
                    rule.fim_version,
                    rule.letzte_aenderung,
                    rule.veroeffentlichungsdatum,
                    rule.last_update,
                    rule.xml_content,
                    rule.freigabe_status.value
                    if rule.freigabe_status is not None
                    else None,
                    rule.status_gesetzt_am,
                )
                for rule in rules
            ],
        )

    async def update_quality_report(
        self,
        schema_id: str,
        schema_version: str,
        report: QualityReport,
        now: datetime,
    ):
        """
        This only updates the `quality_report` and the `last_update` timestamp
        if the contents of the report have actually changed.
        """
        await self.connection.execute(
            """
            UPDATE schema
            SET 
                quality_report = $1, 
                last_update = $2
            WHERE fim_id = $3
            AND fim_version = $4
            AND quality_report != $1
            """,
            orjson.dumps(report.to_dict()).decode(),
            now,
            schema_id,
            schema_version,
        )

    async def load_schema_freigabestatus_for_update(
        self, schema_id: str, schema_version: str
    ) -> xdf3.FreigabeStatus | None:
        value = await self.connection.fetchval(
            """
            SELECT freigabe_status
            FROM schema
            WHERE fim_id = $1
            AND fim_version = $2
            FOR UPDATE;
            """,
            schema_id,
            schema_version,
        )

        return xdf3.FreigabeStatus(value) if value is not None else value

    async def load_schema(self, fim_id: str, fim_version: str, immutable_base_url: str):
        row = await self.connection.fetchrow(
            f"""
            SELECT {DetailedSchema.SQL_COLUMNS}
            FROM schema
            WHERE fim_id = $1 AND fim_version = $2
            """,
            fim_id,
            fim_version,
        )

        if row is None:
            return None

        return DetailedSchema.from_row(row, immutable_base_url)

    async def load_latest_schema(self, fim_id: str, immutable_base_url: str):
        row = await self.connection.fetchrow(
            f"""
            SELECT {DetailedSchema.SQL_COLUMNS}
            FROM schema
            WHERE fim_id = $1 AND is_latest = true
            """,
            fim_id,
        )

        if row is None:
            return None

        return DetailedSchema.from_row(row, immutable_base_url)

    async def load_latest_schema_version(self, fim_id: str) -> str | None:
        fim_version = await self.connection.fetchval(
            "SELECT fim_version FROM schema WHERE fim_id = $1 AND is_latest = true",
            fim_id,
        )
        if fim_version is None:
            return None

        return str(fim_version)

    async def load_latest_group_version(
        self, namespace: str, fim_id: str
    ) -> str | None:
        fim_version = await self.connection.fetchval(
            """
            SELECT fim_version FROM datenfeldgruppe 
            WHERE namespace = $1 AND fim_id = $2 AND is_latest = true
            """,
            namespace,
            fim_id,
        )
        if fim_version is None:
            return None

        return str(fim_version)

    async def load_latest_field_version(
        self, namespace: str, fim_id: str
    ) -> str | None:
        fim_version = await self.connection.fetchval(
            """
            SELECT fim_version FROM datenfeld 
            WHERE namespace = $1 AND fim_id = $2 AND is_latest = true
            """,
            namespace,
            fim_id,
        )
        if fim_version is None:
            return None

        return str(fim_version)

    async def load_schema_quality_report(
        self, schema_id: str, schema_version: str
    ) -> QualityReport | None:
        data = await self.connection.fetchval(
            """
            SELECT quality_report
            FROM schema
            WHERE fim_id = $1 AND fim_version = $2
            """,
            schema_id,
            schema_version,
        )

        if data is None:
            return None
        else:
            return QualityReport.from_dict(orjson.loads(data))

    async def load_all_schema_versions(self, fim_id: str) -> list[SchemaOut]:
        result = await self.connection.fetch(
            f"""
            SELECT
                {SchemaOut.SQL_COLUMNS}
            FROM schema
            WHERE fim_id = $1
            """,
            fim_id,
        )

        return [SchemaOut.from_row(row) for row in result]

    async def load_xdf_schema_message_data(
        self, fim_id: str, fim_version: str
    ) -> SchemaMessageExport | None:
        result = await self.connection.fetchrow(
            f"""
            WITH RECURSIVE tree(node_id) AS (
                    SELECT id
                    FROM tree_node
                    WHERE 
                        schema_fim_id = $1
                    AND 
                        schema_fim_version = $2
                UNION
                    SELECT tree_relation.child_id
                    FROM tree_relation, tree
                    WHERE tree_relation.parent_id = tree.node_id
            ), group_rules AS (
                SELECT fim_id, fim_version
                FROM regel
                JOIN datenfeldgruppe_regel_relation r
                ON regel.fim_id = r.regel_fim_id
                AND regel.fim_version = r.regel_fim_version
                JOIN tree_node n
                ON r.datenfeldgruppe_namespace = n.group_namespace
                AND r.datenfeldgruppe_fim_id = n.group_fim_id
                AND r.datenfeldgruppe_fim_version = n.group_fim_version
                WHERE n.id in (
                    SELECT node_id
                    FROM tree
                )
            ), field_rules AS (
                SELECT fim_id, fim_version
                FROM regel
                JOIN datenfeld_regel_relation r
                ON regel.fim_id = r.regel_fim_id
                AND regel.fim_version = r.regel_fim_version
                JOIN tree_node n
                ON r.datenfeld_namespace = n.field_namespace
                AND r.datenfeld_fim_id = n.field_fim_id
                AND r.datenfeld_fim_version = n.field_fim_version
                WHERE n.id in (
                    SELECT node_id
                    FROM tree
                )
            ), schema_rules AS (
                SELECT fim_id, fim_version
                FROM regel
                JOIN schema_regel_relation r
                ON regel.fim_id = r.regel_fim_id
                AND regel.fim_version = r.regel_fim_version
                WHERE r.schema_fim_id = $1
                AND r.schema_fim_version = $2
            )

            SELECT
                {SchemaExportData.SQL_COLUMNS},
                array(
                    SELECT ROW({DatenfeldExportData.SQL_COLUMNS})
                    FROM datenfeld
                    JOIN tree_node n
                    ON datenfeld.namespace = n.field_namespace
                    AND datenfeld.fim_id = n.field_fim_id
                    AND datenfeld.fim_version = n.field_fim_version
                    WHERE n.id in (
                        SELECT node_id
                        FROM tree
                    )
                ) as datenfelder,
                array(
                    SELECT ROW({DatenfeldgruppeExportData.SQL_COLUMNS})
                    FROM datenfeldgruppe
                    JOIN tree_node n
                    ON datenfeldgruppe.namespace = n.group_namespace
                    AND datenfeldgruppe.fim_id = n.group_fim_id
                    AND datenfeldgruppe.fim_version = n.group_fim_version
                    WHERE n.id in (
                        SELECT node_id
                        FROM tree
                    )
                ) as datenfeldgruppen,
                array(
                    SELECT ROW({RegelExportData.SQL_COLUMNS})
                    FROM regel
                    WHERE (fim_id, fim_version) IN (
                        SELECT * FROM group_rules
                        UNION
                        SELECT * FROM field_rules
                        UNION
                        SELECT * FROM schema_rules
                    )
                ) as regeln
            FROM schema
            WHERE fim_id = $1 AND fim_version = $2
            """,
            fim_id,
            fim_version,
        )
        if result is None:
            return None

        return SchemaMessageExport(
            schema=SchemaExportData.from_row(result),
            fields=[DatenfeldExportData.from_row(row) for row in result["datenfelder"]],
            groups=[
                DatenfeldgruppeExportData.from_row(row)
                for row in result["datenfeldgruppen"]
            ],
            rules=[RegelExportData.from_row(row) for row in result["regeln"]],
        )

    async def load_xdf_group_message_data(
        self, namespace: str, fim_id: str, fim_version: str
    ) -> DatenfeldgruppeMessageExport | None:
        result = await self.connection.fetchrow(
            f"""
            WITH RECURSIVE tree(node_id) AS (
                    SELECT id
                    FROM tree_node
                    WHERE 
                        group_namespace = $1
                    AND
                        group_fim_id = $2
                    AND 
                        group_fim_version = $3
                UNION
                    SELECT tree_relation.child_id
                    FROM tree_relation, tree
                    WHERE tree_relation.parent_id = tree.node_id
            ), group_rules AS (
                SELECT fim_id, fim_version
                FROM regel
                JOIN datenfeldgruppe_regel_relation r
                ON regel.fim_id = r.regel_fim_id
                AND regel.fim_version = r.regel_fim_version
                JOIN tree_node n
                ON r.datenfeldgruppe_namespace = n.group_namespace
                AND r.datenfeldgruppe_fim_id = n.group_fim_id
                AND r.datenfeldgruppe_fim_version = n.group_fim_version
                WHERE n.id in (
                    SELECT node_id
                    FROM tree
                )
            ), field_rules AS (
                SELECT fim_id, fim_version
                FROM regel
                JOIN datenfeld_regel_relation r
                ON regel.fim_id = r.regel_fim_id
                AND regel.fim_version = r.regel_fim_version
                JOIN tree_node n
                ON r.datenfeld_namespace = n.field_namespace
                AND r.datenfeld_fim_id = n.field_fim_id
                AND r.datenfeld_fim_version = n.field_fim_version
                WHERE n.id in (
                    SELECT node_id
                    FROM tree
                )
            )
            SELECT
                {DatenfeldgruppeExportData.SQL_COLUMNS},
                array(
                    SELECT ROW({DatenfeldExportData.SQL_COLUMNS})
                    FROM datenfeld
                    JOIN tree_node n
                    ON datenfeld.namespace = n.field_namespace
                    AND datenfeld.fim_id = n.field_fim_id
                    AND datenfeld.fim_version = n.field_fim_version
                    WHERE n.id in (
                        SELECT node_id
                        FROM tree
                    )
                ) as datenfelder,
                array(
                    SELECT ROW({DatenfeldgruppeExportData.SQL_COLUMNS})
                    FROM datenfeldgruppe
                    JOIN tree_node n
                    ON datenfeldgruppe.namespace = n.group_namespace
                    AND datenfeldgruppe.fim_id = n.group_fim_id
                    AND datenfeldgruppe.fim_version = n.group_fim_version
                    WHERE n.id in (
                        SELECT node_id
                        FROM tree
                    )
                ) as datenfeldgruppen,
                array(
                    SELECT ROW({RegelExportData.SQL_COLUMNS})
                    FROM regel
                    WHERE (fim_id, fim_version) IN (
                        SELECT * FROM group_rules
                        UNION
                        SELECT * FROM field_rules
                    )
                ) as regeln
            FROM datenfeldgruppe
            WHERE namespace = $1 AND fim_id = $2 AND fim_version = $3
            """,
            namespace,
            fim_id,
            fim_version,
        )
        if result is None:
            return None

        return DatenfeldgruppeMessageExport(
            group=DatenfeldgruppeExportData.from_row(result),
            fields=[DatenfeldExportData.from_row(row) for row in result["datenfelder"]],
            groups=[
                DatenfeldgruppeExportData.from_row(row)
                for row in result["datenfeldgruppen"]
            ],
            rules=[RegelExportData.from_row(row) for row in result["regeln"]],
        )

    async def load_xdf_field_message_data(
        self, namespace: str, fim_id: str, fim_version: str
    ) -> DatenfeldMessageExport | None:
        result = await self.connection.fetchrow(
            f"""
            WITH field_rules AS (
                SELECT fim_id, fim_version
                FROM regel
                JOIN datenfeld_regel_relation r
                ON regel.fim_id = r.regel_fim_id
                AND regel.fim_version = r.regel_fim_version
                WHERE r.datenfeld_namespace = $1
                AND r.datenfeld_fim_id = $2
                AND r.datenfeld_fim_version = $3
            )
            SELECT
                {DatenfeldExportData.SQL_COLUMNS},
                array(
                    SELECT ROW({RegelExportData.SQL_COLUMNS})
                    FROM regel
                    WHERE (fim_id, fim_version) IN (
                        SELECT * FROM field_rules
                    )
                ) as regeln
            FROM datenfeld
            WHERE namespace = $1 AND fim_id = $2 AND fim_version = $3
            """,
            namespace,
            fim_id,
            fim_version,
        )
        if result is None:
            return None

        return DatenfeldMessageExport(
            field=DatenfeldExportData.from_row(result),
            rules=[RegelExportData.from_row(row) for row in result["regeln"]],
        )

    async def load_schema_file(self, filename: str) -> str | None:
        return await self.connection.fetchval(
            """
            SELECT
                content
            FROM
                schema_file
            WHERE
                filename = $1
            """,
            filename,
        )

    async def load_steckbrief_xml_content(
        self, fim_id: str, fim_version: str
    ) -> tuple[XdfVersion, str] | None:
        row = await self.connection.fetchrow(
            """
            SELECT
                xdf_version,
                xml_content
            FROM
                dokumentensteckbrief
            WHERE
                fim_id = $1 AND fim_version = $2
            """,
            fim_id,
            fim_version,
        )

        if row is None:
            return None

        xdf_version = XdfVersion(row[0])
        content = row[1]
        return xdf_version, content

    async def load_json_schema_content(self, filename: str) -> str | None:
        return await self.connection.fetchval(
            """
            SELECT
                content
            FROM
                json_schema_file
            WHERE
                filename = $1
            """,
            filename,
        )

    async def load_xsd_content(self, filename: str) -> str | None:
        return await self.connection.fetchval(
            """
            SELECT content FROM xsd_file WHERE filename = $1
            """,
            filename,
        )

    async def load_latest_schema_import(
        self, schema_id: str, schema_version: str
    ) -> XdfImportData | None:
        row = await self.connection.fetchrow(
            """
            SELECT 
                xdf_version,
                content,
                steckbrief_id,
                filename
            FROM 
                schema
            JOIN 
                schema_file
            ON schema.fim_id = schema_file.schema_fim_id
            AND schema.fim_version = schema_file.schema_fim_version
            WHERE
                schema.fim_id = $1 AND schema.fim_version = $2
            ORDER BY schema_file.created_at DESC
            LIMIT 1 
            """,
            schema_id,
            schema_version,
        )
        if row is None:
            return None

        return XdfImportData(
            version=XdfVersion(row[0]),
            xml_content=row[1],
            steckbrief_id=row[2],
            filename=row[3],
        )

    async def save_json_schema_file(self, model: JsonSchemaFileModel):
        await self.connection.execute(
            """
            INSERT INTO json_schema_file(
                filename,
                content,
                created_at,
                generated_from,
                canonical_hash
            ) VALUES (
                $1, $2, $3, $4, $5
            );
        """,
            model.filename,
            model.content,
            model.created_at,
            model.generated_from,
            model.canonical_hash,
        )

    async def load_latest_json_schema_hash(
        self, schema_id: str, schema_version: str
    ) -> str | None:
        return await self.connection.fetchval(
            """
            SELECT canonical_hash
            FROM json_schema_file
            JOIN schema_file
            ON json_schema_file.generated_from = schema_file.filename
            WHERE schema_file.schema_fim_id = $1 AND schema_file.schema_fim_version = $2
            ORDER BY json_schema_file.created_at DESC
            LIMIT 1
            """,
            schema_id,
            schema_version,
        )

    async def load_latest_xsd_hash(
        self, schema_id: str, schema_version: str
    ) -> str | None:
        return await self.connection.fetchval(
            """
            SELECT canonical_hash
            FROM xsd_file
            JOIN schema_file
            ON xsd_file.generated_from = schema_file.filename
            WHERE schema_file.schema_fim_id = $1 AND schema_file.schema_fim_version = $2
            ORDER BY xsd_file.created_at DESC
            LIMIT 1
            """,
            schema_id,
            schema_version,
        )

    async def save_xsd_file(self, model: XsdFileModel):
        await self.connection.execute(
            """
            INSERT INTO xsd_file(
                filename,
                content,
                created_at,
                generated_from,
                canonical_hash
            ) VALUES (
                $1, $2, $3, $4, $5
            );
        """,
            model.filename,
            model.content,
            model.created_at,
            model.generated_from,
            model.canonical_hash,
        )

    async def load_code_list_contents_for_schema_import(
        self, schema_filename: str
    ) -> list[str]:
        rows: list[tuple[str]] = await self.connection.fetch(
            """
                    SELECT code_list.content 
                    FROM code_list 
                    JOIN schema_file_code_list
                    ON
                        schema_file_code_list.code_list_id = code_list.id
                    WHERE
                        schema_file_code_list.schema_filename = $1
                    AND
                        code_list.content IS NOT NULL
                """,
            schema_filename,
        )

        return [row[0] for row in rows]

    async def load_code_list_content(
        self,
        code_list_id: int,
    ) -> str | None:
        return await self.connection.fetchval(
            """
                    SELECT content 
                    FROM code_list 
                    WHERE id = $1
                """,
            code_list_id,
        )

    async def load_external_code_lists(
        self,
        uris: Iterable[str],
    ) -> dict[str, tuple[str, str]]:
        """
        Return a map uri -> (source, content) for all external code lists with the given URIs.
        """
        rows: list[tuple[str, str, str]] = await self.connection.fetch(
            """
                SELECT genericode_canonical_version_uri, source, content
                FROM code_list 
                WHERE genericode_canonical_version_uri = ANY($1)
                AND content IS NOT NULL
                AND is_external = true
            """,
            uris,
        )

        return {
            row[0]: (
                row[1],
                row[2],
            )
            for row in rows
        }

    async def create_fields(self, fields: list[NewField]) -> list[int]:
        """
        Create new fields in the database. Return the internal ids of all relevant code lists
        for the created fields.

        This will:
            - Create all necessary code lists first
            - Create the fields itself
            - Create all relations to other fields
            - Create all references to rules
        """

        if len(fields) == 0:
            return []

        # Note(Felix): Maybe create the code list within a CTE when creating the field?
        # When I implemented this, it was not clear to me how to only execute the INSERT conditionally, so I
        # decided to do it in two steps.
        code_lists: list[NewCodeList] = [
            field.code_list for field in fields if field.code_list
        ]
        code_list_uri_to_id = await self._save_code_lists(code_lists)

        await self.connection.executemany(
            """
                WITH insert AS (
                    INSERT INTO datenfeld (
                        namespace,
                        fim_id,
                        fim_version,
                        nummernkreis,
                        name,
                        beschreibung,
                        definition,
                        bezug,
                        freigabe_status,
                        status_gesetzt_am,
                        status_gesetzt_durch,
                        gueltig_ab,
                        gueltig_bis,
                        versionshinweis,
                        veroeffentlichungsdatum,
                        letzte_aenderung,
                        last_update,
                        feldart,
                        datentyp,
                        xdf_version,
                        xml_content,
                        immutable_json,
                        code_list_id
                    ) VALUES (
                        $1,
                        $2,
                        $3,
                        $4,
                        $5,
                        $6,
                        $7,
                        $8,
                        $9,
                        $10,
                        $11,
                        $12,
                        $13,
                        $14,
                        $15,
                        $16,
                        $17,
                        $18,
                        $19,
                        $20,
                        $21,
                        $22,
                        $23
                    )
                ), insert_rule_relations AS (
                    INSERT INTO datenfeld_regel_relation (
                        datenfeld_namespace,
                        datenfeld_fim_id,
                        datenfeld_fim_version,
                        regel_fim_id,
                        regel_fim_version
                    ) VALUES (
                        $1,
                        $2,
                        $3,
                        unnest($24::TEXT[]),
                        unnest($25::TEXT[])
                    )
                )

                INSERT INTO datenfeld_relations (
                    source_namespace,
                    source_fim_id,
                    source_fim_version,
                    target_fim_id,
                    target_fim_version,
                    praedikat
                ) VALUES (
                    $1,
                    $2,
                    $3,
                    unnest($26::TEXT[]),
                    unnest($27::TEXT[]),
                    unnest($28::TEXT[])
                )
                """,
            [
                (
                    field.identifier.namespace,
                    field.identifier.fim_id,
                    field.identifier.fim_version,
                    field.nummernkreis,
                    field.name,
                    field.beschreibung,
                    field.definition,
                    field.bezug,
                    field.freigabe_status.value,
                    field.status_gesetzt_am,
                    field.status_gesetzt_durch,
                    field.gueltig_ab,
                    field.gueltig_bis,
                    field.versionshinweis,
                    field.veroeffentlichungsdatum,
                    field.letzte_aenderung,
                    field.last_update,
                    field.feldart.value,
                    field.datentyp.value,
                    field.xdf_version.value,
                    field.xml_content,
                    orjson.dumps(field.immutable_json).decode(),
                    code_list_uri_to_id[
                        field.code_list.identifier.canonical_version_uri
                    ]
                    if field.code_list
                    else None,
                    [rule.fim_id for rule in field.regeln],
                    [rule.fim_version for rule in field.regeln],
                    [relation.objekt.id for relation in field.relation],
                    [relation.objekt.version for relation in field.relation],
                    [relation.praedikat.value for relation in field.relation],
                )
                for field in fields
            ],
        )

        return list(code_list_uri_to_id.values())

    async def update_fields(self, fields: list[NewField]):
        """
        Update existing fields in the database.

        This will:
            - Update the fields
            - Delete all outdated relations
            - Create new relations where necessary
        """

        if len(fields) == 0:
            return

        await self.connection.executemany(
            """
            WITH relation_targets AS (
                SELECT * FROM unnest(
                    $10::text[],
                    $11::text[],
                    $12::text[]
                ) as t(target_fim_id, target_fim_version, praedikat)
            ), insert_new_relations AS (
                INSERT INTO datenfeld_relations (
                    source_namespace,
                    source_fim_id,
                    source_fim_version,
                    target_fim_id,
                    target_fim_version,
                    praedikat
                ) (
                    SELECT
                        $1,
                        $2,
                        $3,
                        target_fim_id,
                        target_fim_version,
                        praedikat
                    FROM relation_targets
                ) ON CONFLICT DO NOTHING
            ), delete_old_relations AS (
                DELETE FROM datenfeld_relations
                WHERE source_namespace = $1
                AND source_fim_id = $2
                AND source_fim_version = $3
                AND (target_fim_id, target_fim_version, praedikat) NOT IN (
                    SELECT * FROM relation_targets
                )
            )

            UPDATE datenfeld
            SET
                freigabe_status = $4,
                status_gesetzt_am = $5,
                veroeffentlichungsdatum = $6,
                letzte_aenderung = $7,
                last_update = $8,
                xml_content = $9
            WHERE namespace = $1
            AND fim_id = $2
            AND fim_version = $3
                """,
            [
                (
                    field.identifier.namespace,
                    field.identifier.fim_id,
                    field.identifier.fim_version,
                    field.freigabe_status.value,
                    field.status_gesetzt_am,
                    field.veroeffentlichungsdatum,
                    field.letzte_aenderung,
                    field.last_update,
                    field.xml_content,
                    [relation.objekt.id for relation in field.relation],
                    [relation.objekt.version for relation in field.relation],
                    [relation.praedikat.value for relation in field.relation],
                )
                for field in fields
            ],
        )

    async def create_groups(self, groups: list[NewGroup]):
        """
        Create new groups in the database.
        This will
            - Create the groups itself
            - Create all relations to other groups
            - Create all references to rules
        """

        if len(groups) == 0:
            return

        await self.connection.executemany(
            """
                WITH insert_group AS (
                    INSERT INTO datenfeldgruppe (
                        namespace,
                        fim_id,
                        fim_version,
                        nummernkreis,
                        xdf_version,
                        name,
                        beschreibung,
                        definition,
                        status_gesetzt_durch,
                        bezug,
                        freigabe_status,
                        status_gesetzt_am,
                        gueltig_ab,
                        gueltig_bis,
                        versionshinweis,
                        veroeffentlichungsdatum,
                        letzte_aenderung,
                        last_update,
                        xml_content,
                        immutable_json
                    ) VALUES (
                        $1,
                        $2,
                        $3,
                        $4,
                        $5,
                        $6,
                        $7,
                        $8,
                        $9,
                        $10,
                        $11,
                        $12,
                        $13,
                        $14,
                        $15,
                        $16,
                        $17,
                        $18,
                        $19,
                        $20
                    )
                ), insert_rule_relations AS (
                    INSERT INTO datenfeldgruppe_regel_relation (
                        datenfeldgruppe_namespace,
                        datenfeldgruppe_fim_id,
                        datenfeldgruppe_fim_version,
                        regel_fim_id,
                        regel_fim_version
                    ) VALUES (
                        $1,
                        $2,
                        $3,
                        unnest($21::TEXT[]),
                        unnest($22::TEXT[])
                    )
                )

                INSERT INTO datenfeldgruppe_relations (
                    source_namespace,
                    source_fim_id,
                    source_fim_version,
                    target_fim_id,
                    target_fim_version,
                    praedikat
                ) VALUES (
                    $1,
                    $2,
                    $3,
                    unnest($23::TEXT[]),
                    unnest($24::TEXT[]),
                    unnest($25::TEXT[])
                )
                """,
            [
                (
                    group.identifier.namespace,
                    group.identifier.fim_id,
                    group.identifier.fim_version,
                    group.nummernkreis,
                    group.xdf_version.value,
                    group.name,
                    group.beschreibung,
                    group.definition,
                    group.status_gesetzt_durch,
                    group.bezug,
                    group.freigabe_status.value,
                    group.status_gesetzt_am,
                    group.gueltig_ab,
                    group.gueltig_bis,
                    group.versionshinweis,
                    group.veroeffentlichungsdatum,
                    group.letzte_aenderung,
                    group.last_update,
                    group.xml_content,
                    orjson.dumps(group.immutable_json).decode(),
                    [rule.fim_id for rule in group.regeln],
                    [rule.fim_version for rule in group.regeln],
                    [relation.objekt.id for relation in group.relation],
                    [relation.objekt.version for relation in group.relation],
                    [relation.praedikat.value for relation in group.relation],
                )
                for group in groups
            ],
        )

    async def update_groups(self, groups: list[NewGroup]):
        """
        Update existing groups in the database.

        This will:
            - Update the groups
            - Delete all outdated relations
            - Create new relations where necessary
        """

        if len(groups) == 0:
            return

        await self.connection.executemany(
            """
            WITH relation_targets AS (
                SELECT * FROM unnest(
                    $10::text[],
                    $11::text[],
                    $12::text[]
                ) as t(target_fim_id, target_fim_version, praedikat)
            ), insert_new_relations AS (
                INSERT INTO datenfeldgruppe_relations (
                    source_namespace,
                    source_fim_id,
                    source_fim_version,
                    target_fim_id,
                    target_fim_version,
                    praedikat
                ) (
                    SELECT
                        $1, 
                        $2,
                        $3,
                        target_fim_id,
                        target_fim_version,
                        praedikat
                    FROM relation_targets
                ) ON CONFLICT DO NOTHING
            ), delete_old_relations AS (
                DELETE FROM datenfeldgruppe_relations
                WHERE source_namespace = $1
                AND source_fim_id = $2
                AND source_fim_version = $3
                AND (target_fim_id, target_fim_version, praedikat) NOT IN (
                    SELECT * FROM relation_targets
                )
            )

            UPDATE datenfeldgruppe
            SET
                freigabe_status = $4,
                status_gesetzt_am = $5,
                veroeffentlichungsdatum = $6,
                letzte_aenderung = $7,
                last_update = $8,
                xml_content = $9
            WHERE namespace = $1
            AND fim_id = $2
            AND fim_version = $3
            """,
            [
                (
                    group.identifier.namespace,
                    group.identifier.fim_id,
                    group.identifier.fim_version,
                    group.freigabe_status.value,
                    group.status_gesetzt_am,
                    group.veroeffentlichungsdatum,
                    group.letzte_aenderung,
                    group.last_update,
                    group.xml_content,
                    [relation.objekt.id for relation in group.relation],
                    [relation.objekt.version for relation in group.relation],
                    [relation.praedikat.value for relation in group.relation],
                )
                for group in groups
            ],
        )

    async def save_tree_structure(self, connections: list[TreeRelationModel]):
        if len(connections) == 0:
            return

        await self.connection.executemany(
            """
                INSERT INTO tree_relation (
                    parent_id,
                    child_index,
                    child_id,
                    anzahl,
                    bezug
                ) VALUES (
                    $1,
                    $2,
                    $3,
                    $4,
                    $5
                ) ON CONFLICT DO NOTHING
                """,
            [
                (
                    tree_conn.parent_id,
                    tree_conn.child_index,
                    tree_conn.child_id,
                    tree_conn.anzahl,
                    tree_conn.bezug,
                )
                for tree_conn in connections
            ],
        )

    async def load_schema_for_update(
        self, fim_id: str, fim_version: str
    ) -> SchemaOut | None:
        # NOTE(Felix): Load the schema with FOR UPDATE
        # to signal all competing uploads for this schemas to wait until the
        # current transaction has finished.

        row = await self.connection.fetchrow(
            f"""
            SELECT
                {SchemaOut.SQL_COLUMNS}
            FROM schema
            WHERE fim_id = $1 AND fim_version = $2
            FOR UPDATE;
            """,
            fim_id,
            fim_version,
        )

        if row is None:
            return None

        return SchemaOut.from_row(row)

    async def create_schema(self, schema: NewSchema, fts_content: str):
        """
        Create a new schema in the database.

        This will:
            - Create the schema itself
            - Create all relations to other schemas
            - Create all references to rules
        """

        # When updating an existing schema, the fts_content is not written,
        # as this column only depends on immutable data.
        await self.connection.execute(
            """
            WITH insert AS (
                INSERT INTO schema (
                    fim_id,
                    fim_version,
                    nummernkreis,
                    name,
                    beschreibung,
                    definition,
                    bezug,
                    freigabe_status,
                    status_gesetzt_durch,
                    gueltig_ab,
                    gueltig_bis,
                    versionshinweis,
                    letzte_aenderung,
                    stichwort,
                    steckbrief_id,
                    xdf_version,
                    last_update,
                    bezeichnung,
                    status_gesetzt_am,
                    bezug_components,
                    veroeffentlichungsdatum,
                    quality_report,
                    immutable_json,
                    raw_content
                ) VALUES (
                    $1,
                    $2,
                    $3,
                    $4,
                    $5,
                    $6,
                    $7,
                    $8,
                    $9,
                    $10,
                    $11,
                    $12,
                    $13,
                    $14,
                    $15,
                    $16,
                    $17,
                    $18,
                    $19,
                    $20,
                    $21,
                    $22,
                    $23,
                    $24
                )
            ), insert_rule_relations AS (
                INSERT INTO schema_regel_relation (
                    schema_fim_id,
                    schema_fim_version,
                    regel_fim_id,
                    regel_fim_version
                ) VALUES (
                    $1,
                    $2,
                    unnest($25::TEXT[]),
                    unnest($26::TEXT[])
                )
            )

            INSERT INTO schema_relations (
                source_fim_id,
                source_fim_version,
                target_fim_id,
                target_fim_version,
                praedikat
            ) VALUES (
                $1,
                $2,
                unnest($27::TEXT[]),
                unnest($28::TEXT[]),
                unnest($29::TEXT[])
            )
            """,
            schema.fim_id,
            schema.fim_version,
            schema.nummernkreis,
            schema.name,
            schema.beschreibung,
            schema.definition,
            schema.bezug,
            schema.freigabe_status.value,
            schema.status_gesetzt_durch,
            schema.gueltig_ab,
            schema.gueltig_bis,
            schema.versionshinweis,
            schema.letzte_aenderung,
            schema.stichwort,
            schema.steckbrief_id,
            schema.xdf_version.value,
            schema.last_update,
            schema.bezeichnung,
            schema.status_gesetzt_am,
            schema.bezug_components,
            schema.veroeffentlichungsdatum,
            orjson.dumps(schema.quality_report.to_dict()).decode(),
            orjson.dumps(schema.immutable_json).decode(),
            fts_content,
            [rule.fim_id for rule in schema.regeln],
            [rule.fim_version for rule in schema.regeln],
            [relation.objekt.id for relation in schema.relation],
            [relation.objekt.version for relation in schema.relation],
            [relation.praedikat.value for relation in schema.relation],
        )

    async def update_schema(self, schema: NewSchema):
        """
        Update an existing schema in the database.

        This will:
            - Update all mutable columns of the schema
            - Delete all outdated relations
            - Create new relations where necessary
        """

        # When updating an existing schema, the fts_content is not written,
        # as this column only depends on immutable data.
        await self.connection.execute(
            """
            WITH relation_targets AS (
                SELECT * FROM unnest(
                    $10::text[],
                    $11::text[],
                    $12::text[]
                ) as t(target_fim_id, target_fim_version, praedikat)
            ), insert_new_relations AS (
                INSERT INTO schema_relations (
                    source_fim_id,
                    source_fim_version,
                    target_fim_id,
                    target_fim_version,
                    praedikat
                ) (
                    SELECT
                        $1, 
                        $2,
                        target_fim_id,
                        target_fim_version,
                        praedikat
                    FROM relation_targets
                ) ON CONFLICT DO NOTHING
            ), delete_old_relations AS (
                DELETE FROM schema_relations
                WHERE source_fim_id = $1
                AND source_fim_version = $2
                AND (target_fim_id, target_fim_version, praedikat) NOT IN (
                    SELECT * FROM relation_targets
                )
            )

            UPDATE schema
            SET
                freigabe_status = $3,
                status_gesetzt_am = $4,
                veroeffentlichungsdatum = $5,
                steckbrief_id = $6,
                letzte_aenderung = $7,
                last_update = $8,
                quality_report = $9
            WHERE fim_id = $1
            AND fim_version = $2
            """,
            schema.fim_id,
            schema.fim_version,
            schema.freigabe_status.value,
            schema.status_gesetzt_am,
            schema.veroeffentlichungsdatum,
            schema.steckbrief_id,
            schema.letzte_aenderung,
            schema.last_update,
            orjson.dumps(schema.quality_report.to_dict()).decode(),
            [relation.objekt.id for relation in schema.relation],
            [relation.objekt.version for relation in schema.relation],
            [relation.praedikat.value for relation in schema.relation],
        )

    async def update_schema_last_update(
        self, fim_id: str, fim_version: str, last_update: datetime
    ):
        await self.connection.execute(
            """
            UPDATE schema 
            SET last_update = $1
            WHERE fim_id = $2 AND fim_version = $3
            """,
            last_update,
            fim_id,
            fim_version,
        )

    async def upload_schema_file(
        self, schema: NewSchema, xml_content: str, code_list_ids: Iterable[int]
    ) -> str:
        file_model = SchemaFileModel.create(
            xml_content=xml_content,
            xdf_version=schema.xdf_version,
            schema_fim_id=schema.fim_id,
            schema_fim_version=schema.fim_version,
        )

        await self.connection.execute(
            """
            WITH schema AS (
                INSERT INTO schema_file (
                    filename,
                    content,
                    created_at,
                    schema_fim_id,
                    schema_fim_version
                ) VALUES (
                    $1,
                    $2,
                    $3,
                    $4,
                    $5
                )
            )

            INSERT INTO schema_file_code_list (
                schema_filename,
                code_list_id
            ) VALUES (
                $1,
                unnest($6::INT[])
            )
            """,
            file_model.filename,
            file_model.content,
            file_model.created_at,
            file_model.schema_fim_id,
            file_model.schema_fim_version,
            code_list_ids,
        )

        return file_model.filename

    async def clear_tree_connections(self, node_ids: list[int]):
        """
        Clear all outgoing tree connections from the nodes in the list.

        This will only remove connections where the parent is in the list of nodes.
        All connections that include any of the provided node as a child will not be removed.
        """

        await self.connection.execute(
            """
            DELETE FROM tree_relation 
            WHERE parent_id = ANY($1)
            """,
            node_ids,
        )

    async def load_or_create_nodes(
        self,
        schema_fim_id: str,
        schema_fim_version: str,
        field_identifiers: list[ElementIdentifier],
        group_identifiers: list[ElementIdentifier],
    ) -> tuple[int, dict[ElementIdentifier, int]]:
        """
        Load all the existing nodes relevant for a schema upload.
        If a node does not exist, create the corresponding node.

        This returns a map from the element identifier to the node model.

        Important note: this assumes, that the identifiers of schemas, fields and groups
        are always unique, which is the case here due to the `S`, `G` or `F` prefix in the FIM ID.
        """

        total_fields = len(field_identifiers)
        total_groups = len(group_identifiers)
        none_fields = [None] * total_fields
        none_groups = [None] * total_groups

        # First create all new nodes and return the rows, then select all existing nodes.
        # Since the SELECT does not contain the newly created nodes, the results must be combined at the end.
        rows = await self.connection.fetch(
            """
                WITH insert AS (
                    INSERT INTO tree_node(
                        schema_fim_id,
                        schema_fim_version,
                        group_namespace,
                        group_fim_id,
                        group_fim_version,
                        field_namespace,
                        field_fim_id,
                        field_fim_version
                    ) VALUES (
                        unnest($1::text[]),
                        unnest($2::text[]),
                        unnest($3::text[]),
                        unnest($4::text[]),
                        unnest($5::text[]),
                        unnest($6::text[]),
                        unnest($7::text[]),
                        unnest($8::text[])
                    ) ON CONFLICT DO NOTHING
                    RETURNING *
                )

                SELECT
                    id,
                    schema_fim_id,
                    schema_fim_version,
                    group_namespace,
                    group_fim_id,
                    group_fim_version,
                    field_namespace,
                    field_fim_id,
                    field_fim_version
                FROM tree_node
                WHERE (schema_fim_id = $9 AND schema_fim_version = $10)
                OR (group_namespace || group_fim_id || group_fim_version) = ANY($11)
                OR (field_namespace || field_fim_id || field_fim_version) = ANY($12)

                UNION ALL

                SELECT * FROM insert;
            """,
            list(chain([schema_fim_id], none_groups, none_fields)),
            list(chain([schema_fim_version], none_groups, none_fields)),
            list(
                chain(
                    [None],
                    [identifier.namespace for identifier in group_identifiers],
                    none_fields,
                )
            ),
            list(
                chain(
                    [None],
                    [identifier.fim_id for identifier in group_identifiers],
                    none_fields,
                )
            ),
            list(
                chain(
                    [None],
                    [identifier.fim_version for identifier in group_identifiers],
                    none_fields,
                )
            ),
            list(
                chain(
                    [None],
                    none_groups,
                    [identifier.namespace for identifier in field_identifiers],
                )
            ),
            list(
                chain(
                    [None],
                    none_groups,
                    [identifier.fim_id for identifier in field_identifiers],
                )
            ),
            list(
                chain(
                    [None],
                    none_groups,
                    [identifier.fim_version for identifier in field_identifiers],
                ),
            ),
            schema_fim_id,
            schema_fim_version,
            [
                f"{identifier.namespace}{identifier.fim_id}{identifier.fim_version}"
                for identifier in group_identifiers
            ],
            [
                f"{identifier.namespace}{identifier.fim_id}{identifier.fim_version}"
                for identifier in field_identifiers
            ],
        )

        schema_node_id: int | None = None
        identifier_to_node_id: dict[ElementIdentifier, int] = {}
        for row in rows:
            (
                node_id,
                node_schema_fim_id,
                node_schema_fim_version,
                group_namespace,
                group_fim_id,
                group_fim_version,
                field_namespace,
                field_fim_id,
                field_fim_version,
            ) = row

            if node_schema_fim_id is not None:
                assert node_schema_fim_version is not None
                schema_node_id = node_id
            elif group_namespace is not None:
                assert group_fim_id is not None
                assert group_fim_version is not None

                identifier = ElementIdentifier(
                    group_namespace, group_fim_id, group_fim_version
                )
                identifier_to_node_id[identifier] = node_id
            else:
                assert field_namespace is not None
                assert field_fim_id is not None
                assert field_fim_version is not None

                identifier = ElementIdentifier(
                    field_namespace, field_fim_id, field_fim_version
                )
                identifier_to_node_id[identifier] = node_id

        assert schema_node_id is not None
        return schema_node_id, identifier_to_node_id

    async def _save_code_lists(
        self,
        code_lists: list[NewCodeList],
    ) -> dict[str, int]:
        """
        Insert missing code lists and return a map from the genericode_canonical_version_uri to the internal code list id.
        All already existing, external code lists will be re-used.
        """
        if len(code_lists) == 0:
            return {}

        # This should insert all missing codelists and then return the IDs of all referenced codelists.
        # Effectively, this will create all internal codelists and all missing external codelists.
        result = await self.connection.fetch(
            """
            WITH inserted AS (
                INSERT INTO code_list (
                    genericode_canonical_version_uri,
                    genericode_canonical_uri,
                    genericode_version,
                    is_external,
                    content,
                    source
                ) VALUES (
                    unnest($1::TEXT[]),
                    unnest($2::TEXT[]),
                    unnest($3::TEXT[]),
                    unnest($4::BOOLEAN[]),
                    unnest($5::TEXT[]),
                    unnest($6::TEXT[])
                )
                ON CONFLICT DO NOTHING
                RETURNING genericode_canonical_version_uri, id
            ), existing AS (
                SELECT genericode_canonical_version_uri, id FROM code_list
                WHERE is_external = true
                AND genericode_canonical_version_uri = ANY($7)
            )

            SELECT * from inserted
            UNION
            SELECT * from existing
            """,
            [code_list.identifier.canonical_version_uri for code_list in code_lists],
            [code_list.identifier.canonical_uri for code_list in code_lists],
            [code_list.identifier.version for code_list in code_lists],
            [code_list.is_external for code_list in code_lists],
            [
                code_list.content.xml_content if code_list.content is not None else None
                for code_list in code_lists
            ],
            [
                code_list.content.source if code_list.content is not None else None
                for code_list in code_lists
            ],
            [
                code_list.identifier.canonical_version_uri
                for code_list in code_lists
                if code_list.is_external
            ],
        )

        return {
            genericode_canonical_version_uri: code_list_id
            for genericode_canonical_version_uri, code_list_id in result
        }

    async def save_admin_session(self, token: str, now: datetime) -> None:
        await self.connection.execute(
            """
                INSERT INTO admin_session (
                    token,
                    created_at
                ) VALUES (
                    $1,
                    $2
                )
            """,
            token,
            now,
        )

    async def load_admin_session_creation_time(self, token: str) -> datetime | None:
        return await self.connection.fetchval(
            "SELECT created_at from admin_session WHERE token = $1", token
        )

    async def remove_admin_session(self, token: str):
        await self.connection.execute(
            "DELETE FROM admin_session WHERE token = $1", token
        )

    async def load_all_api_tokens(self) -> list[ApiTokenModel]:
        rows: list[tuple[int, str, str, str]] = await self.connection.fetch(
            """
            SELECT 
                id,
                token,
                nummernkreis,
                description
            FROM access_token
            ORDER BY nummernkreis
            """
        )

        return [
            ApiTokenModel(
                id=row[0],
                token=row[1],
                nummernkreis=row[2],
                description=row[3],
            )
            for row in rows
        ]

    async def load_api_token_by_id(self, token_id: int) -> ApiTokenModel | None:
        row = await self.connection.fetchrow(
            """
            SELECT 
                token,
                nummernkreis,
                description
            FROM access_token
            WHERE id = $1
            """,
            token_id,
        )

        if row is None:
            return None

        return ApiTokenModel(
            id=token_id,
            token=row[0],
            nummernkreis=row[1],
            description=row[2],
        )

    async def load_api_token(self, token: str) -> ApiTokenModel | None:
        row = await self.connection.fetchrow(
            """
            SELECT 
                id,
                token,
                nummernkreis,
                description
            FROM access_token
            WHERE token = $1
            """,
            token,
        )
        if row is None:
            return None

        return ApiTokenModel(
            id=row[0], token=row[1], nummernkreis=row[2], description=row[3]
        )

    async def save_api_token(self, token: str, nummernkreis: str, description: str):
        await self.connection.execute(
            """
            INSERT INTO access_token (
                token,
                nummernkreis,
                description
            ) VALUES (
                $1,
                $2,
                $3
            )
            """,
            token,
            nummernkreis,
            description,
        )

    async def delete_api_token(self, token_id: int):
        await self.connection.execute(
            """
            DELETE FROM access_token 
            WHERE id = $1
            """,
            token_id,
        )

    async def get_all_schema_identifiers(self) -> list[tuple[str, str]]:
        """
        Get the list of FIM ID and version for each available schema.
        """

        return await self.connection.fetch(
            """
            SELECT 
                fim_id,
                fim_version
            FROM schema
            """
        )

    async def get_schema_csv_search_results(
        self, options: SchemaSearchOptions
    ) -> list[CSVSearchResult]:
        query = options.create_query()

        return await query.fetch_all(
            CSVSearchResult.DATENFELDER_SQL_COLUMNS,
            CSVSearchResult.from_schema,
            self.connection,
            order_by=options.order_by.to_sql(),
        )

    async def search_schemas(
        self, options: SchemaSearchOptions
    ) -> PaginatedResult[SchemaOut]:
        """
        Raises `DatabaseError` if there is a problem with the query.
        """
        query = options.create_query()

        return await query.paginate(
            SchemaOut.SQL_COLUMNS,
            SchemaOut.from_row,
            self.connection,
            options.pagination_options,
            order_by=options.order_by.to_sql(),
        )

    async def load_code_lists(
        self, pagination_options: PaginationOptions, immutable_base_url: str
    ) -> PaginatedResult[CodeListOut]:
        query = SearchQuery("code_list")

        return await query.paginate(
            CodeListOut.SQL_COLUMNS,
            lambda row, fts_match: CodeListOut.from_row(
                row, immutable_base_url, fts_match
            ),
            self.connection,
            pagination_options,
            order_by="id",
        )

    async def load_fields(
        self, identifiers: Iterable[ElementIdentifierTuple]
    ) -> list[DatenfeldOut]:
        field_result = await self.connection.fetch(
            f"""
                SELECT {DatenfeldOut.SQL_COLUMNS}
                FROM datenfeld
                WHERE (namespace || fim_id || fim_version) = ANY($1)
                """,
            [
                f"{identifier[0]}{identifier[1]}{identifier[2]}"
                for identifier in identifiers
            ],
        )

        return [DatenfeldOut.from_row(row) for row in field_result]

    async def get_field_csv_search_results(
        self, options: FieldSearchOptions
    ) -> list[CSVSearchResult]:
        query = options.create_query()

        return await query.fetch_all(
            CSVSearchResult.DATENFELDER_SQL_COLUMNS,
            CSVSearchResult.from_field,
            self.connection,
            order_by="fim_id, fim_version",
        )

    async def search_fields(
        self, options: FieldSearchOptions
    ) -> PaginatedResult[DatenfeldOut]:
        """
        Raises `DatabaseError` if there is a problem with the query.
        """
        query = options.create_query()

        return await query.paginate(
            DatenfeldOut.SQL_COLUMNS,
            DatenfeldOut.from_row,
            self.connection,
            options.pagination_options,
            order_by="fim_id, fim_version",
        )

    async def load_all_field_versions(
        self, namespace: str, fim_id: str
    ) -> list[DatenfeldOut]:
        result = await self.connection.fetch(
            f"""
                SELECT {DatenfeldOut.SQL_COLUMNS}
                FROM datenfeld
                WHERE namespace = $1 AND fim_id = $2
                """,
            namespace,
            fim_id,
        )

        return [DatenfeldOut.from_row(row) for row in result]

    async def load_field(
        self, namespace: str, fim_id: str, fim_version: str, immutable_base_url: str
    ) -> DetailedDatenfeld | None:
        row = await self.connection.fetchrow(
            f"""
                SELECT {DetailedDatenfeld.SQL_COLUMNS}
                FROM datenfeld
                WHERE namespace =$1 AND fim_id = $2 AND fim_version = $3
                """,
            namespace,
            fim_id,
            fim_version,
        )

        if row is None:
            return None

        return DetailedDatenfeld.from_row(row, immutable_base_url=immutable_base_url)

    async def load_latest_field(
        self, namespace: str, fim_id: str, immutable_base_url: str
    ) -> DetailedDatenfeld | None:
        row = await self.connection.fetchrow(
            f"""
                SELECT {DetailedDatenfeld.SQL_COLUMNS}
                FROM datenfeld
                WHERE namespace =$1 AND fim_id = $2 AND is_latest = true
                """,
            namespace,
            fim_id,
        )

        if row is None:
            return None

        return DetailedDatenfeld.from_row(row, immutable_base_url)

    async def load_field_batch_by_ids(
        self,
        fim_ids: list[str],
    ) -> list[DatenfeldOut]:
        if len(fim_ids) == 0:
            return []

        result = await self.connection.fetch(
            f"""
                SELECT {DatenfeldOut.SQL_COLUMNS}
                FROM datenfeld
                WHERE namespace = $1 AND fim_id = ANY($2)
                """,
            "baukasten",
            [fim_ids],
        )

        return [DatenfeldOut.from_row(row) for row in result]

    async def load_group_batch_by_ids(
        self,
        fim_ids: list[str],
    ) -> list[DatenfeldgruppeOut]:
        if len(fim_ids) == 0:
            return []

        result = await self.connection.fetch(
            f"""
            SELECT
                {DatenfeldgruppeOut.SQL_COLUMNS}
            FROM datenfeldgruppe
            WHERE namespace = $1 AND fim_id = ANY($2)
            """,
            "baukasten",
            [fim_ids],
        )

        return [DatenfeldgruppeOut.from_row(row) for row in result]

    async def load_groups(
        self, identifiers: Iterable[ElementIdentifierTuple]
    ) -> list[DatenfeldgruppeOut]:
        group_result = await self.connection.fetch(
            f"""
            SELECT
                {DatenfeldgruppeOut.SQL_COLUMNS}
            FROM datenfeldgruppe
            WHERE (namespace || fim_id || fim_version) = ANY($1)
            """,
            [
                f"{identifier[0]}{identifier[1]}{identifier[2]}"
                for identifier in identifiers
            ],
        )

        return [DatenfeldgruppeOut.from_row(row) for row in group_result]

    async def get_group_csv_search_results(
        self, options: GroupSearchOptions
    ) -> list[CSVSearchResult]:
        query = options.create_query()

        return await query.fetch_all(
            CSVSearchResult.DATENFELDER_SQL_COLUMNS,
            CSVSearchResult.from_group,
            self.connection,
            order_by="fim_id, fim_version",
        )

    async def search_groups(
        self, options: GroupSearchOptions
    ) -> PaginatedResult[DatenfeldgruppeOut]:
        """
        Raises `DatabaseError` if there is a problem with the query.
        """
        query = options.create_query()

        return await query.paginate(
            DatenfeldgruppeOut.SQL_COLUMNS,
            DatenfeldgruppeOut.from_row,
            self.connection,
            options.pagination_options,
            order_by="fim_id, fim_version",
        )

    async def load_all_group_versions(
        self, namespace: str, fim_id: str
    ) -> list[DatenfeldgruppeOut]:
        result = await self.connection.fetch(
            f"""
            SELECT
                {DatenfeldgruppeOut.SQL_COLUMNS}
            FROM datenfeldgruppe
            WHERE namespace=$1 AND fim_id=$2
            """,
            namespace,
            fim_id,
        )
        return [DatenfeldgruppeOut.from_row(row) for row in result]

    async def load_group(
        self, namespace: str, fim_id: str, fim_version: str
    ) -> DetailedDatenfeldgruppe | None:
        row = await self.connection.fetchrow(
            f"""
            SELECT
                {DetailedDatenfeldgruppe.SQL_COLUMNS}
            FROM datenfeldgruppe
            WHERE namespace=$1 AND fim_id=$2 AND fim_version=$3
            """,
            namespace,
            fim_id,
            fim_version,
        )
        if row is None:
            return None

        return DetailedDatenfeldgruppe.from_row(row)

    async def load_latest_group(
        self, namespace: str, fim_id: str
    ) -> DetailedDatenfeldgruppe | None:
        row = await self.connection.fetchrow(
            f"""
            SELECT {DetailedDatenfeldgruppe.SQL_COLUMNS}
            FROM datenfeldgruppe
            WHERE namespace=$1 AND fim_id=$2 AND is_latest = true
            """,
            namespace,
            fim_id,
        )
        if row is None:
            return None

        return DetailedDatenfeldgruppe.from_row(row)

    async def load_schema_tree(self, fim_id: str, fim_version: str) -> SchemaTree:
        results = await self.connection.fetch(
            """
            WITH RECURSIVE tree(node_id) AS (
                    SELECT id
                    FROM tree_node
                    WHERE 
                        schema_fim_id = $1
                    AND 
                        schema_fim_version = $2
                UNION
                    SELECT tree_relation.child_id
                    FROM tree_relation, tree
                    WHERE tree_relation.parent_id = tree.node_id
            )

            SELECT
                n1.schema_fim_id as parent_schema_id,
                n1.group_namespace as parent_group_namespace,
                n1.group_fim_id as parent_group_fim_id,
                n1.group_fim_version as parent_group_fim_version,
                n2.group_namespace as child_group_namespace,
                n2.group_fim_id as child_group_fim_id,
                n2.group_fim_version as child_group_fim_version,
                n2.field_namespace as child_field_namespace,
                n2.field_fim_id as child_field_fim_id,
                n2.field_fim_version as child_field_fim_version,
                tree_relation.anzahl,
                tree_relation.bezug

            FROM tree_relation
            JOIN tree_node n1
                ON tree_relation.parent_id = n1.id
            JOIN tree_node n2
                ON tree_relation.child_id = n2.id
            WHERE tree_relation.parent_id in (
                SELECT node_id
                FROM tree
            )
            ORDER BY tree_relation.child_index ASC
            """,
            fim_id,
            fim_version,
        )

        schema_children: list[DirectChild] = []
        group_to_children: dict[ElementIdentifierTuple, list[DirectChild]] = (
            defaultdict(list)
        )

        group_identifiers: set[ElementIdentifierTuple] = set()
        field_identifiers: set[ElementIdentifierTuple] = set()

        for row in results:
            (
                parent_schema_id,
                parent_group_namespace,
                parent_group_fim_id,
                parent_group_fim_version,
                child_group_namespace,
                child_group_fim_id,
                child_group_fim_version,
                child_field_namespace,
                child_field_fim_id,
                child_field_fim_version,
                anzahl,
                bezug,
            ) = row

            assert anzahl is not None
            assert bezug is not None

            # First, extract the child identifier
            if child_group_namespace is not None:
                assert child_group_fim_id is not None
                assert child_group_fim_version is not None

                child_identifier = (
                    child_group_namespace,
                    child_group_fim_id,
                    child_group_fim_version,
                )
                child_type = ChildType.GROUP
                group_identifiers.add(child_identifier)
            else:
                assert child_field_namespace is not None
                assert child_field_fim_id is not None
                assert child_field_fim_version is not None

                child_identifier = (
                    child_field_namespace,
                    child_field_fim_id,
                    child_field_fim_version,
                )
                child_type = ChildType.FIELD
                field_identifiers.add(child_identifier)

            child = DirectChild(
                type=child_type,
                namespace=child_identifier[0],
                fim_id=child_identifier[1],
                fim_version=child_identifier[2],
                anzahl=anzahl,
                bezug=bezug,
            )

            # Then, determine the parent and and the child relationship.
            # The resulting child list will be ordered correctly, as we
            # queried the data in order of the `child_index`.

            if parent_schema_id is not None:
                assert parent_schema_id == fim_id
                schema_children.append(child)
            else:
                assert parent_group_namespace is not None
                assert parent_group_fim_id is not None
                assert parent_group_fim_version is not None

                # We do not have to include this identifier in `group_identifier`,
                # as each group in the tree must appear at least once as a direct children,
                # so the identifier will be added anyways.
                group_identifier = (
                    parent_group_namespace,
                    parent_group_fim_id,
                    parent_group_fim_version,
                )
                group_to_children[group_identifier].append(child)

        return SchemaTree(
            schema_children=schema_children,
            group_to_children=group_to_children,
            group_identifiers=group_identifiers,
            field_identifiers=field_identifiers,
        )

    async def load_group_tree(
        self, namespace: str, fim_id: str, fim_version: str
    ) -> GroupTree:
        results = await self.connection.fetch(
            """
            WITH RECURSIVE tree(node_id) AS (
                    SELECT id
                    FROM tree_node
                    WHERE 
                        group_namespace = $1
                    AND
                        group_fim_id = $2
                    AND 
                        group_fim_version = $3
                UNION
                    SELECT tree_relation.child_id
                    FROM tree_relation, tree
                    WHERE tree_relation.parent_id = tree.node_id
            )

            SELECT
                n1.group_namespace as parent_group_namespace,
                n1.group_fim_id as parent_group_fim_id,
                n1.group_fim_version as parent_group_fim_version,
                n2.group_namespace as child_group_namespace,
                n2.group_fim_id as child_group_fim_id,
                n2.group_fim_version as child_group_fim_version,
                n2.field_namespace as child_field_namespace,
                n2.field_fim_id as child_field_fim_id,
                n2.field_fim_version as child_field_fim_version,
                tree_relation.anzahl,
                tree_relation.bezug

            FROM tree_relation
            JOIN tree_node n1
                ON tree_relation.parent_id = n1.id
            JOIN tree_node n2
                ON tree_relation.child_id = n2.id
            WHERE tree_relation.parent_id in (
                SELECT node_id
                FROM tree
            )
            ORDER BY tree_relation.child_index ASC
            """,
            namespace,
            fim_id,
            fim_version,
        )

        group_to_children: dict[ElementIdentifierTuple, list[DirectChild]] = (
            defaultdict(list)
        )

        group_identifiers: set[ElementIdentifierTuple] = set()
        field_identifiers: set[ElementIdentifierTuple] = set()

        for row in results:
            (
                parent_group_namespace,
                parent_group_fim_id,
                parent_group_fim_version,
                child_group_namespace,
                child_group_fim_id,
                child_group_fim_version,
                child_field_namespace,
                child_field_fim_id,
                child_field_fim_version,
                anzahl,
                bezug,
            ) = row

            assert anzahl is not None
            assert bezug is not None

            # First, extract the child identifier
            if child_group_namespace is not None:
                assert child_group_fim_id is not None
                assert child_group_fim_version is not None

                child_identifier = (
                    child_group_namespace,
                    child_group_fim_id,
                    child_group_fim_version,
                )
                child_type = ChildType.GROUP
                group_identifiers.add(child_identifier)
            else:
                assert child_field_namespace is not None
                assert child_field_fim_id is not None
                assert child_field_fim_version is not None

                child_identifier = (
                    child_field_namespace,
                    child_field_fim_id,
                    child_field_fim_version,
                )
                child_type = ChildType.FIELD
                field_identifiers.add(child_identifier)

            child = DirectChild(
                type=child_type,
                namespace=child_identifier[0],
                fim_id=child_identifier[1],
                fim_version=child_identifier[2],
                anzahl=anzahl,
                bezug=bezug,
            )

            assert parent_group_namespace is not None
            assert parent_group_fim_id is not None
            assert parent_group_fim_version is not None

            # We do not have to include this identifier in `group_identifier`,
            # as each group in the tree must appear at least once as a direct children,
            # so the identifier will be added anyways.
            group_identifier = (
                parent_group_namespace,
                parent_group_fim_id,
                parent_group_fim_version,
            )
            group_to_children[group_identifier].append(child)

        return GroupTree(
            group_to_children=group_to_children,
            group_identifiers=group_identifiers,
            field_identifiers=field_identifiers,
        )

    async def load_schemas_for_element(
        self, namespace: str, fim_id: str, fim_version: str
    ) -> list[SchemaOut]:
        """
        Load all the schemas in which the element is currently referenced.

        This is done within a single SQL query, which
        1. Initially selects the single node for the element
        2. Recursively collects all parent nodes of the element
        3. Selects all schemas which have a corresponding tree node in the collected list of nodes
        """
        results = await self.connection.fetch(
            f"""
                WITH RECURSIVE tree(node_id) AS (
                        SELECT id
                        FROM tree_node
                        WHERE (
                            group_namespace = $1
                            AND group_fim_id = $2
                            AND group_fim_version = $3
                        )
                        OR (
                            field_namespace = $4
                            AND field_fim_id = $5
                            AND field_fim_version = $6
                        )
                    UNION
                        SELECT tree_relation.parent_id
                        FROM tree_relation, tree
                        WHERE tree_relation.child_id = tree.node_id
                )

                SELECT 
                    {SchemaOut.SQL_COLUMNS}
                FROM schema
                INNER JOIN tree_node
                ON schema.fim_id = tree_node.schema_fim_id
                AND schema.fim_version = tree_node.schema_fim_version
                WHERE tree_node.id in (
                    SELECT node_id
                    FROM tree
                )
                ORDER BY name
                """,
            namespace,
            fim_id,
            fim_version,
            namespace,
            fim_id,
            fim_version,
        )

        return [SchemaOut.from_row(row) for row in results]

    async def load_entities_for_schema_import_checks(
        self,
        schema_id: str,
        schema_version: str,
        fields: Iterable[ElementIdentifier],
        groups: Iterable[ElementIdentifier],
        rules: Iterable[RuleIdentifier],
    ) -> list[ImmutableResource]:
        """
        Return a list of tuples:

        (
            entity_type,
            namespace,
            fim_id,
            fim_version,
            immutable_json,
            veroeffentlichungsdatum,
            freigabe_status,
            code_list_id
        )

        - Entity types are: "schema", "field", "group", "rule"
        - namespace only applies for field und group and is None for other entities.
        - xdf3 rules have no veroeffentlichungsdatum.
        - freigabe_status is only needed for schema.
        - code_list_id is only needed to know the linked code_list_ids in order
        to link them to the immutable schema_upload.

        TODO: Remove the code_list_id from the result once the schema_upload is
        not saved anymore.

        When a tuple entry is not needed or doesn't apply, then None is returned.
        """
        result = await self.connection.fetch(
            """
            SELECT * FROM (SELECT
                'schema' AS entity_type,
                NULL AS namespace,
                fim_id,
                fim_version,
                immutable_json,
                veroeffentlichungsdatum,
                freigabe_status,
                NULL::INTEGER AS code_list_id
            FROM
                schema
            WHERE 
                fim_id = $1 AND
                fim_version = $2
            FOR UPDATE) as r

            UNION ALL
            SELECT * FROM
                (SELECT
                    'field' AS entity_type,
                    datenfeld.namespace,
                    datenfeld.fim_id,
                    datenfeld.fim_version,
                    immutable_json,
                    veroeffentlichungsdatum,
                    NULL::SMALLINT AS freigabe_status,
                    code_list_id
                FROM
                    datenfeld
                JOIN unnest($3::text[], $4::text[], $5::text[]) 
                    AS params(namespace, fim_id, fim_version)
                ON 
                    datenfeld.namespace = params.namespace AND
                    datenfeld.fim_id = params.fim_id AND
                    datenfeld.fim_version = params.fim_version
                FOR UPDATE
                ) AS f

            UNION ALL
            SELECT * FROM
                (SELECT
                    'group' AS entity_type,
                    datenfeldgruppe.namespace,
                    datenfeldgruppe.fim_id,
                    datenfeldgruppe.fim_version,
                    immutable_json,
                    veroeffentlichungsdatum,
                    NULL::SMALLINT AS freigabe_status,
                    NULL::INTEGER AS code_list_id
                FROM
                    datenfeldgruppe
                JOIN unnest($6::text[], $7::text[], $8::text[]) 
                    AS params(namespace, fim_id, fim_version)
                ON 
                    datenfeldgruppe.namespace = params.namespace AND
                    datenfeldgruppe.fim_id = params.fim_id AND
                    datenfeldgruppe.fim_version = params.fim_version
                FOR UPDATE
                ) AS g

            UNION ALL
            SELECT * FROM
                (SELECT
                    'rule' AS entity_type,
                    NULL AS namespace,
                    regel.fim_id,
                    regel.fim_version,
                    immutable_json,
                    veroeffentlichungsdatum,
                    NULL::SMALLINT AS freigabe_status,
                    NULL::INTEGER AS code_list_id
                FROM
                    regel
                JOIN unnest($9::text[], $10::text[]) 
                    AS params(fim_id, fim_version)
                ON 
                    regel.fim_id = params.fim_id AND
                    regel.fim_version = params.fim_version
                FOR UPDATE
                ) AS r
            """,
            schema_id,
            schema_version,
            [field.namespace for field in fields],
            [field.fim_id for field in fields],
            [field.fim_version for field in fields],
            [group.namespace for group in groups],
            [group.fim_id for group in groups],
            [group.fim_version for group in groups],
            [rule.fim_id for rule in rules],
            [rule.fim_version for rule in rules],
        )

        return [
            ImmutableResource(
                type=row[0],
                namespace=row[1],
                fim_id=row[2],
                fim_version=row[3],
                immutable_json=row[4],
                veroeffentlichungsdatum=row[5],
                freigabe_status=FreigabeStatus(row[6]) if row[6] is not None else None,
                code_list_id=row[7],
            )
            for row in result
        ]

    async def load_steckbrief_for_import_checks(
        self, fim_id: str, fim_version: str
    ) -> tuple[str, date, FreigabeStatus] | None:
        row = await self.connection.fetchrow(
            """
            SELECT
                immutable_json, veroeffentlichungsdatum, freigabe_status
            FROM dokumentensteckbrief
            WHERE
                fim_id = $1
                AND fim_version = $2
            FOR UPDATE
            """,
            fim_id,
            fim_version,
        )
        if row is None:
            return row

        return (row[0], row[1], FreigabeStatus(row[2]))

    async def save_xzufi_leistung_batch(self, leistungen: list[NewLeistung]):
        if len(leistungen) == 0:
            return

        await self.connection.executemany(
            """
                INSERT INTO leistung (
                    redaktion_id,
                    id,
                    title,
                    leistungsbezeichnung,
                    leistungsbezeichnung_2,
                    freigabestatus_katalog,
                    freigabestatus_bibliothek,
                    hierarchy_type,
                    root_for_leistungsschluessel,
                    leistungstyp,
                    typisierung,
                    leistungsschluessel,
                    kurztext,
                    volltext,
                    rechtsgrundlagen,
                    sdg_informationsbereiche,
                    einheitlicher_ansprechpartner,
                    klassifizierung,
                    erstellt_datum_zeit,
                    geaendert_datum_zeit,
                    xml_content,
                    source
                ) VALUES (
                    $1,
                    $2,
                    $3,
                    $4,
                    $5,
                    $6,
                    $7,
                    $8,
                    $9,
                    $10,
                    $11,
                    $12,
                    $13,
                    $14,
                    $15,
                    $16,
                    $17,
                    $18,
                    $19,
                    $20,
                    $21,
                    $22
                ) 
                ON CONFLICT (redaktion_id, id) 
                DO UPDATE SET
                    title = EXCLUDED.title,
                    leistungsbezeichnung = EXCLUDED.leistungsbezeichnung,
                    leistungsbezeichnung_2 = EXCLUDED.leistungsbezeichnung_2,
                    freigabestatus_katalog = EXCLUDED.freigabestatus_katalog,
                    freigabestatus_bibliothek = EXCLUDED.freigabestatus_bibliothek,
                    hierarchy_type = EXCLUDED.hierarchy_type,
                    root_for_leistungsschluessel = EXCLUDED.root_for_leistungsschluessel,
                    leistungstyp = EXCLUDED.leistungstyp,
                    typisierung = EXCLUDED.typisierung,
                    leistungsschluessel = EXCLUDED.leistungsschluessel,
                    kurztext = EXCLUDED.kurztext,
                    volltext = EXCLUDED.volltext,
                    rechtsgrundlagen = EXCLUDED.rechtsgrundlagen,
                    sdg_informationsbereiche = EXCLUDED.sdg_informationsbereiche,
                    einheitlicher_ansprechpartner = EXCLUDED.einheitlicher_ansprechpartner,
                    klassifizierung = EXCLUDED.klassifizierung,
                    erstellt_datum_zeit = EXCLUDED.erstellt_datum_zeit,
                    geaendert_datum_zeit = EXCLUDED.geaendert_datum_zeit,
                    xml_content = EXCLUDED.xml_content,
                    source = EXCLUDED.source
                """,
            [
                (
                    leistung.redaktion_id,
                    leistung.id,
                    leistung.title,
                    leistung.leistungsbezeichnung,
                    leistung.leistungsbezeichnung_2,
                    leistung.freigabe_status_katalog,
                    leistung.freigabe_status_bibliothek,
                    leistung.hierarchy_type.value,
                    leistung.root_for_leistungsschluessel,
                    (
                        leistung.leistungstyp.value
                        if leistung.leistungstyp is not None
                        else None
                    ),
                    [typ.value for typ in leistung.typisierung],
                    leistung.leistungsschluessel,
                    leistung.kurztext,
                    leistung.volltext,
                    leistung.rechtsgrundlagen,
                    leistung.sdg_informationsbereiche,
                    leistung.einheitlicher_ansprechpartner,
                    [item.to_str() for item in leistung.klassifizierung],
                    leistung.erstellt_datum_zeit,
                    leistung.geaendert_datum_zeit,
                    leistung.xml_content,
                    leistung.source.value,
                )
                for leistung in leistungen
            ],
        )

        links = set([link for leistung in leistungen for link in leistung.links])
        if len(links) == 0:
            return

        await self.save_links(links)

        await self.connection.executemany(
            """
            INSERT INTO link_leistung_relation (
                uri,
                redaktion,
                leistung
            ) VALUES (
                $1,
                $2,
                $3
            ) ON CONFLICT (uri, redaktion, leistung) 
            DO NOTHING;
            """,
            [
                (link, leistung.redaktion_id, leistung.id)
                for leistung in leistungen
                for link in leistung.links
            ],
        )

    async def save_links(self, links: Iterable[str]):
        await self.connection.executemany(
            """
            INSERT INTO link (
                uri
            ) VALUES (
                $1
            ) ON CONFLICT (uri) 
            DO NOTHING;
            """,
            [(uri,) for uri in links],
        )

    async def get_leistungssteckbrief_csv_search_results(
        self, options: LeistungssteckbriefSearchOptions
    ) -> list[CSVSearchResult]:
        query = options.create_query()

        return await query.fetch_all(
            CSVSearchResult.LEISTUNGSSTECKBRIEF_SQL_COLUMNS,
            CSVSearchResult.from_leistungssteckbrief,
            self.connection,
            order_by=options.order_by.to_sql(),
        )

    async def search_leistungssteckbriefe(
        self,
        options: LeistungssteckbriefSearchOptions,
    ) -> PaginatedResult[LeistungssteckbriefOut]:
        query = options.create_query()

        return await query.paginate(
            LeistungssteckbriefOut.SQL_COLUMNS,
            LeistungssteckbriefOut.from_row,
            self.connection,
            options.pagination_options,
            order_by=options.order_by.to_sql(query.fts_query_param_index),
        )

    async def load_leistungssteckbrief(
        self, leistungsschluessel: str
    ) -> FullLeistungssteckbriefOut | None:
        row = await self.connection.fetchrow(
            f"""
                SELECT {FullLeistungssteckbriefOut.SQL_COLUMNS}
                FROM leistung
                WHERE root_for_leistungsschluessel = $1
                """,
            leistungsschluessel,
        )

        if row is None:
            return None

        return FullLeistungssteckbriefOut.from_row(row)

    async def load_leistungssteckbrief_content_info(
        self, leistungsschluessel: str
    ) -> XzufiContentInfo | None:
        row: tuple[str, str] | None = await self.connection.fetchrow(
            "SELECT source, xml_content FROM leistung WHERE root_for_leistungsschluessel = $1",
            leistungsschluessel,
        )

        if row is None:
            return None

        return XzufiContentInfo(
            source=XzufiSource(row[0]),
            content=row[1],
        )

    async def get_leistungsbeschreibung_csv_search_results(
        self, options: LeistungsbeschreibungSearchOptions
    ) -> list[CSVSearchResult]:
        query = options.create_query()

        return await query.fetch_all(
            CSVSearchResult.LEISTUNGSBESCHREIBUNG_SQL_COLUMNS,
            CSVSearchResult.from_leistungsbeschreibung,
            self.connection,
            order_by=options.order_by.to_sql(),
        )

    async def search_leistungsbeschreibungen(
        self,
        options: LeistungsbeschreibungSearchOptions,
    ) -> PaginatedResult[LeistungsbeschreibungOut]:
        query = options.create_query()

        return await query.paginate(
            LeistungsbeschreibungOut.SQL_COLUMNS,
            LeistungsbeschreibungOut.from_row,
            self.connection,
            options.pagination_options,
            order_by=options.order_by.to_sql(query.fts_query_param_index),
        )

    async def load_leistung_identifiers_for_redaktion(
        self, redaktion_id: RedaktionsId
    ) -> list[XzufiLeistungsIdentifier]:
        result: list[tuple[LeistungsId]] = await self.connection.fetch(
            "SELECT id FROM leistung WHERE redaktion_id = $1", redaktion_id
        )

        return [(redaktion_id, row[0]) for row in result]

    async def load_leistungsbeschreibung(
        self, identifier: XzufiLeistungsIdentifier
    ) -> FullLeistungsbeschreibungOut | None:
        redaktion_id, leistung_id = identifier

        row = await self.connection.fetchrow(
            f"""
                SELECT {FullLeistungsbeschreibungOut.SQL_COLUMNS}
                FROM leistung
                WHERE redaktion_id = $1
                AND id = $2
                AND root_for_leistungsschluessel IS NULL
                """,
            redaktion_id,
            leistung_id,
        )

        if row is None:
            return None

        return FullLeistungsbeschreibungOut.from_row(row)

    async def load_leistungsbeschreibung_content_info(
        self, identifier: XzufiLeistungsIdentifier
    ) -> XzufiContentInfo | None:
        redaktion_id, leistung_id = identifier

        row = await self.connection.fetchrow(
            """
                SELECT source, xml_content
                FROM leistung
                WHERE redaktion_id = $1
                AND id = $2
                AND root_for_leistungsschluessel IS NULL
            """,
            redaktion_id,
            leistung_id,
        )
        if row is None:
            return None

        return XzufiContentInfo(
            source=XzufiSource(row[0]),
            content=row[1],
        )

    async def delete_leistungsbeschreibung(self, identifier: XzufiLeistungsIdentifier):
        redaktion_id, leistung_id = identifier

        await self.connection.execute(
            "DELETE FROM leistung WHERE redaktion_id = $1 AND id = $2",
            redaktion_id,
            leistung_id,
        )

    async def delete_leistungsbeschreibung_batch(
        self, identifiers: list[XzufiLeistungsIdentifier]
    ):
        await self.connection.executemany(
            """
                DELETE FROM leistung WHERE redaktion_id = $1 AND id = $2
                """,
            [(id[0], id[1]) for id in identifiers],
        )

    async def load_link(self, uri: str) -> Link | None:
        row = await self.connection.fetchrow(
            "select uri, last_checked, last_online FROM link WHERE uri = $1", uri
        )
        if row is None:
            return None
        return Link(uri=row[0], last_checked=row[1], last_online=row[2])

    async def load_link_statuses(self, uris: list[str]) -> list[Link]:
        result: list[
            tuple[str, datetime | None, datetime | None]
        ] = await self.connection.fetch(
            """
                SELECT uri, last_checked, last_online 
                FROM link 
                WHERE uri = ANY($1)
            """,
            uris,
        )

        return [
            Link(uri=row[0], last_checked=row[1], last_online=row[2]) for row in result
        ]

    async def load_links(self, options: PaginationOptions) -> PaginatedResult[Link]:
        query = SearchQuery("link")

        return await query.paginate(
            Link.SQL_COLUMNS,
            Link.from_row,
            self.connection,
            options,
            order_by="uri",
        )

    async def load_oldest_links(self, batch_size: int, last_checked_before: datetime):
        result: list[
            tuple[str, datetime | None, datetime | None]
        ] = await self.connection.fetch(
            """
                SELECT uri, last_checked, last_online 
                FROM link 
                WHERE last_checked isnull OR last_checked < $1
                LIMIT $2
                FOR UPDATE;
            """,
            last_checked_before,
            batch_size,
        )

        return [
            Link(uri=row[0], last_checked=row[1], last_online=row[2]) for row in result
        ]

    async def update_link_statuses(self, links: list[Link]):
        await self.connection.executemany(
            """
                UPDATE link 
                SET
                    last_checked = $1,
                    last_online = $2
                WHERE uri = $3 
                """,
            [(link.last_checked, link.last_online, link.uri) for link in links],
        )

    async def load_links_for_leistung(
        self, leistung_identifier: XzufiLeistungsIdentifier
    ) -> list[Link]:
        redaktion, leistung = leistung_identifier
        result: list[
            tuple[str, datetime | None, datetime | None]
        ] = await self.connection.fetch(
            """
                SELECT llr.uri, last_checked, last_online 
                FROM link_leistung_relation llr JOIN link l
                ON llr.uri=l.uri 
                WHERE redaktion = $1 AND leistung = $2;
            """,
            redaktion,
            leistung,
        )

        return [
            Link(uri=row[0], last_checked=row[1], last_online=row[2]) for row in result
        ]

    async def remove_unused_links(self):
        await self.connection.execute(
            """
                WITH remaining_links AS (
                    SELECT distinct(uri) FROM link_leistung_relation
                )
                DELETE FROM link WHERE uri NOT IN (SELECT uri FROM remaining_links);
            """
        )

    async def save_prozessklasse_batch(self, prozessklassen: list[NewProzessklasse]):
        await self.connection.executemany(
            """
                INSERT INTO prozessklasse (
                    id,
                    version,
                    name,
                    freigabe_status,
                    letzter_aenderungszeitpunkt,
                    operatives_ziel,
                    verfahrensart,
                    handlungsform,
                    xml_content
                ) VALUES (
                    $1,
                    $2,
                    $3,
                    $4,
                    $5,
                    $6,
                    $7,
                    $8,
                    $9
                ) 
                ON CONFLICT (id)
                DO UPDATE SET
                    version = EXCLUDED.version,
                    name = EXCLUDED.name,
                    freigabe_status = EXCLUDED.freigabe_status,
                    letzter_aenderungszeitpunkt = EXCLUDED.letzter_aenderungszeitpunkt,
                    operatives_ziel = EXCLUDED.operatives_ziel,
                    verfahrensart=EXCLUDED.verfahrensart,
                    handlungsform=EXCLUDED.handlungsform,
                    xml_content = EXCLUDED.xml_content
                """,
            [
                (
                    prozessklasse.id,
                    prozessklasse.version,
                    prozessklasse.name,
                    prozessklasse.freigabe_status,
                    prozessklasse.letzter_aenderungszeitpunkt,
                    prozessklasse.operatives_ziel.value
                    if prozessklasse.operatives_ziel is not None
                    else None,
                    prozessklasse.verfahrensart.value
                    if prozessklasse.verfahrensart is not None
                    else None,
                    prozessklasse.handlungsform.value
                    if prozessklasse.handlungsform is not None
                    else None,
                    prozessklasse.xml_content,
                )
                for prozessklasse in prozessklassen
            ],
        )

    async def save_prozess_batch(self, prozesse: list[NewProzess]):
        """
        Upsert all Prozesse in the batch.
        Also update the references to the Dokumentsteckbriefe.
        """
        await self.connection.executemany(
            """
                INSERT INTO prozess (
                    id,
                    version,
                    name,
                    freigabe_status,
                    detaillierungsstufe,
                    prozessklasse,
                    verwaltungspolitische_kodierung,
                    letzter_aenderungszeitpunkt,
                    report_file,
                    visualization_file,
                    xml_content,
                    raw_content_without_visualization
                ) VALUES (
                    $1,
                    $2,
                    $3,
                    $4,
                    $5,
                    $6,
                    $7,
                    $8,
                    $9,
                    $10,
                    $11,
                    prozess_xml_to_raw_content($12)
                ) 
                ON CONFLICT (id)
                DO UPDATE SET
                    version = EXCLUDED.version,
                    name = EXCLUDED.name,
                    freigabe_status = EXCLUDED.freigabe_status,
                    detaillierungsstufe = EXCLUDED.detaillierungsstufe,
                    prozessklasse = EXCLUDED.prozessklasse,
                    verwaltungspolitische_kodierung = EXCLUDED.verwaltungspolitische_kodierung,
                    letzter_aenderungszeitpunkt = EXCLUDED.letzter_aenderungszeitpunkt,
                    report_file = EXCLUDED.report_file,
                    visualization_file = EXCLUDED.visualization_file,
                    xml_content = EXCLUDED.xml_content,
                    raw_content_without_visualization = EXCLUDED.raw_content_without_visualization
                """,
            [
                (
                    prozess.id,
                    prozess.version,
                    prozess.name,
                    prozess.status,
                    (
                        prozess.detaillierungsstufe.value
                        if prozess.detaillierungsstufe is not None
                        else None
                    ),
                    prozess.prozessklasse,
                    [kodierung.value for kodierung in prozess.anwendungsgebiet],
                    prozess.letzter_aenderungszeitpunkt,
                    prozess.report_file,
                    prozess.visualization_file,
                    prozess.xml_content,
                    prozess.xml_content_without_files,
                )
                for prozess in prozesse
            ],
        )

        # Clear relations first
        await self.connection.executemany(
            "DELETE FROM prozess_dokumentsteckbrief_relation WHERE prozess_id = $1",
            [(prozess.id,) for prozess in prozesse],
        )

        # Re-create new relations
        await self.connection.executemany(
            """
            INSERT INTO prozess_dokumentsteckbrief_relation (
                prozess_id,
                dokumentsteckbrief_fim_id,
                role
            ) VALUES(
                $1,
                $2,
                $3
            ) ON CONFLICT DO NOTHING;
            """,
            chain.from_iterable(
                (
                    [
                        (prozess.id, steckbrief_id, role.value)
                        for steckbrief_id, role in prozess.dokumentsteckbrief_references
                    ]
                    for prozess in prozesse
                )
            ),
        )

    async def load_prozess(self, id: str) -> FullProzessOut | None:
        row = await self.connection.fetchrow(
            f"""
            SELECT {FullProzessOut.SQL_COLUMNS}
            FROM prozess
            WHERE id = $1
            """,
            id,
        )
        if row is None:
            return None

        return FullProzessOut.from_row(row)

    async def load_prozessklasse(self, id: str) -> FullProzessklasseOut | None:
        row = await self.connection.fetchrow(
            f"""
            SELECT {FullProzessklasseOut.SQL_COLUMNS}
            FROM prozessklasse
            WHERE id = $1
            """,
            id,
        )
        if row is None:
            return None

        return FullProzessklasseOut.from_row(row)

    async def load_prozess_xml_content(self, id: str) -> str | None:
        return await self.connection.fetchval(
            """
            SELECT xml_content
            FROM prozess
            WHERE id = $1
            """,
            id,
        )

    async def load_prozess_report_file(self, id: str) -> bytes | None:
        return await self.connection.fetchval(
            """
            SELECT report_file FROM prozess WHERE id = $1
            """,
            id,
        )

    async def load_prozess_visualization_file(self, id: str) -> bytes | None:
        return await self.connection.fetchval(
            """
            SELECT visualization_file FROM prozess WHERE id = $1
            """,
            id,
        )

    async def load_prozessklasse_xml_content(self, id: str) -> str | None:
        return await self.connection.fetchval(
            """
            SELECT xml_content
            FROM prozessklasse
            WHERE id = $1
            """,
            id,
        )

    async def get_prozess_csv_search_results(
        self, options: ProzessSearchOptions
    ) -> list[CSVSearchResult]:
        query = options.create_query()

        return await query.fetch_all(
            CSVSearchResult.PROZESS_SQL_COLUMNS,
            CSVSearchResult.from_prozess,
            self.connection,
            order_by="name, id",
        )

    async def search_prozesse(
        self, options: ProzessSearchOptions
    ) -> PaginatedResult[ProzessOut]:
        query = options.create_query()

        return await query.paginate(
            ProzessOut.SQL_COLUMNS,
            ProzessOut.from_row,
            self.connection,
            options.pagination_options,
            order_by="name, id",
        )

    async def get_prozessklasse_csv_search_results(
        self, options: ProzessklasseSearchOptions
    ) -> list[CSVSearchResult]:
        query = options.create_query()

        return await query.fetch_all(
            CSVSearchResult.PROZESS_SQL_COLUMNS,
            CSVSearchResult.from_prozessklasse,
            self.connection,
            order_by="name, id",
        )

    async def search_prozessklassen(
        self, options: ProzessklasseSearchOptions
    ) -> PaginatedResult[ProzessklasseOut]:
        query = options.create_query()

        return await query.paginate(
            ProzessklasseOut.SQL_COLUMNS,
            ProzessklasseOut.from_row,
            self.connection,
            options.pagination_options,
            order_by="name, id",
        )

    async def load_prozess_ids(self) -> list[str]:
        result: list[tuple[str]] = await self.connection.fetch(
            """
            SELECT id FROM prozess;
            """
        )
        return [row[0] for row in result]

    async def load_prozessklasse_ids(self) -> list[str]:
        result: list[tuple[str]] = await self.connection.fetch(
            """
            SELECT id FROM prozessklasse;
            """
        )
        return [row[0] for row in result]

    async def delete_prozess(self, id: str):
        await self.connection.execute(
            """
            DELETE FROM prozess_dokumentsteckbrief_relation WHERE prozess_id = $1
            """,
            id,
        )
        await self.connection.execute(
            """
            DELETE FROM prozess WHERE id = $1
            """,
            id,
        )

    async def delete_prozessklasse(self, id: str):
        await self.connection.execute(
            """
            DELETE FROM prozessklasse WHERE id = $1
            """,
            id,
        )

    async def save_pvog_batch(self, batch_id: int, batch_xml: str):
        await self.connection.execute(
            """
            INSERT INTO pvog_batch (
                batch_id,
                batch_xml
            ) VALUES (
                $1, 
                $2
            ) 
            ON CONFLICT (batch_id) 
            DO UPDATE SET
                batch_xml = EXCLUDED.batch_xml;
            """,
            batch_id,
            batch_xml,
        )

    async def get_latest_pvog_batch_id(self) -> int:
        value = await self.connection.fetchval(
            """
            SELECT batch_id FROM pvog_batch ORDER BY batch_id DESC LIMIT 1;
            """
        )
        if value is None:
            return 0
        return int(value)

    async def save_raw_pvog_resource(
        self,
        identifier: XzufiLeistungsIdentifier,
        xml_content: str,
        resource_class: PvogResourceClass,
    ):
        await self.connection.execute(
            """
            INSERT INTO raw_pvog_resource (
                redaktion_id,
                id,
                xml_content,
                resource_class
            ) VALUES (
                $1,
                $2,
                $3,
                $4
            ) 
            ON CONFLICT (redaktion_id, id, resource_class)
            DO UPDATE SET
                xml_content = EXCLUDED.xml_content,
                synced = FALSE;
            """,
            identifier[0],
            identifier[1],
            xml_content,
            resource_class.value,
        )

    async def delete_raw_pvog_resource(
        self,
        identifier: XzufiLeistungsIdentifier,
        resource_class: PvogResourceClass,
    ):
        await self.connection.execute(
            """
            DELETE FROM raw_pvog_resource WHERE redaktion_id = $1 AND id = $2 AND resource_class = $3;
            """,
            identifier[0],
            identifier[1],
            resource_class.value,
        )

    async def get_raw_pvog_resource_identifiers(
        self, resource_class: PvogResourceClass
    ) -> list[XzufiLeistungsIdentifier]:
        result: list[tuple[RedaktionsId, LeistungsId]] = await self.connection.fetch(
            """
            SELECT redaktion_id, id FROM raw_pvog_resource WHERE resource_class = $1;
            """,
            resource_class.value,
        )
        return [(row[0], row[1]) for row in result]

    async def get_raw_pvog_resource_for_sync(
        self, resource_class: PvogResourceClass
    ) -> list[tuple[XzufiLeistungsIdentifier, str]]:
        result: list[
            tuple[RedaktionsId, LeistungsId, str]
        ] = await self.connection.fetch(
            """
                SELECT redaktion_id, id, xml_content
                FROM raw_pvog_resource
                WHERE NOT synced AND resource_class = $1
                LIMIT 100
                FOR UPDATE;
            """,
            resource_class.value,
        )
        return [((row[0], row[1]), row[2]) for row in result]

    async def mark_raw_pvog_resource_as_synced(
        self,
        identifiers: list[XzufiLeistungsIdentifier],
        resource_class: PvogResourceClass,
    ):
        await self.connection.executemany(
            """
                UPDATE raw_pvog_resource
                SET
                    synced = TRUE
                WHERE
                    resource_class = $1
                AND 
                    redaktion_id = $2
                AND 
                    id = $3;
                """,
            [(resource_class.value, id[0], id[1]) for id in identifiers],
        )

    async def save_spezialisierung_batch(
        self, spezialisierungen: list[NewSpezialisierung]
    ):
        if len(spezialisierungen) == 0:
            return

        await self.connection.executemany(
            """
                INSERT INTO xzufi_spezialisierung (
                    redaktion_id,
                    id,
                    leistung_redaktion_id,
                    leistung_id,
                    xml_content
                ) VALUES (
                    $1,
                    $2,
                    $3,
                    $4,
                    $5
                ) ON CONFLICT (redaktion_id, id)
                DO UPDATE SET
                    leistung_redaktion_id = EXCLUDED.leistung_redaktion_id,
                    leistung_id = EXCLUDED.leistung_id,
                    xml_content = EXCLUDED.xml_content;
                """,
            [
                (
                    spezialisierung.id.redaktion_id,
                    spezialisierung.id.id,
                    spezialisierung.leistung.redaktion_id,
                    spezialisierung.leistung.id,
                    spezialisierung.xml_content,
                )
                for spezialisierung in spezialisierungen
            ],
        )
        await self.connection.execute(
            "REFRESH MATERIALIZED VIEW xzufi_spezialisierung_count;"
        )

    async def save_organisationseinheit_batch(
        self, organisationseinheiten: list[NewOrganisationseinheit]
    ):
        if len(organisationseinheiten) == 0:
            return

        await self.connection.executemany(
            """
                INSERT INTO xzufi_organisationseinheit (
                    redaktion_id,
                    id,
                    xml_content
                ) VALUES (
                    $1,
                    $2,
                    $3
                ) ON CONFLICT (redaktion_id, id)
                DO UPDATE SET
                    xml_content = EXCLUDED.xml_content;
                """,
            [
                (
                    organisationseinheit.id.redaktion_id,
                    organisationseinheit.id.id,
                    organisationseinheit.xml_content,
                )
                for organisationseinheit in organisationseinheiten
            ],
        )
        await self.connection.execute(
            "REFRESH MATERIALIZED VIEW xzufi_organisationseinheit_count;"
        )

    async def save_onlinedienst_batch(self, onlinedienste: list[NewOnlinedienst]):
        if len(onlinedienste) == 0:
            return

        await self.connection.executemany(
            """
                INSERT INTO xzufi_onlinedienst (
                    redaktion_id,
                    id,
                    xml_content
                ) VALUES (
                    $1,
                    $2,
                    $3
                ) ON CONFLICT (redaktion_id, id)
                DO UPDATE SET
                    xml_content = EXCLUDED.xml_content;
                """,
            [
                (
                    onlinedienst.id.redaktion_id,
                    onlinedienst.id.id,
                    onlinedienst.xml_content,
                )
                for onlinedienst in onlinedienste
            ],
        )
        await self.connection.execute(
            "REFRESH MATERIALIZED VIEW xzufi_onlinedienst_count;"
        )

    async def save_zustaendigkeit_batch(
        self, zustaendigkeiten: list[NewZustaendigkeit]
    ):
        if len(zustaendigkeiten) == 0:
            return

        await self.connection.executemany(
            """
                INSERT INTO xzufi_zustaendigkeit (
                    redaktion_id,
                    id,
                    xml_content
                ) VALUES (
                    $1,
                    $2,
                    $3
                ) ON CONFLICT (redaktion_id, id)
                DO UPDATE SET
                    xml_content = EXCLUDED.xml_content;
                """,
            [
                (
                    zustaendigkeit.id.redaktion_id,
                    zustaendigkeit.id.id,
                    zustaendigkeit.xml_content,
                )
                for zustaendigkeit in zustaendigkeiten
            ],
        )
        await self.connection.execute(
            "REFRESH MATERIALIZED VIEW xzufi_zustaendigkeit_count;"
        )

    async def search_xzufi_spezialisierung(self, options: PaginationOptions):
        query = SearchQuery("xzufi_spezialisierung")

        return await query.paginate(
            SpezialisierungOut.SQL_COLUMNS,
            SpezialisierungOut.from_row,
            self.connection,
            options,
            order_by="redaktion_id ASC, id ASC",
            use_materialized_view_count=True,
        )

    async def get_xzufi_spezialisierung_xml(
        self, identifier: XzufiIdentifier
    ) -> str | None:
        return await self.connection.fetchval(
            "SELECT xml_content FROM xzufi_spezialisierung WHERE redaktion_id = $1 AND id = $2",
            identifier.redaktion_id,
            identifier.id,
        )

    async def get_xzufi_spezialisierung(
        self, identifier: XzufiIdentifier
    ) -> SpezialisierungOut | None:
        row = await self.connection.fetchrow(
            f"""
            SELECT {SpezialisierungOut.SQL_COLUMNS} FROM xzufi_spezialisierung WHERE redaktion_id = $1 AND id = $2
            """,
            identifier.redaktion_id,
            identifier.id,
        )
        if row is None:
            return None
        return SpezialisierungOut.from_row(row)

    async def search_xzufi_organisationseinheiten(
        self, options: PaginationOptions
    ) -> PaginatedResult[OrganisationseinheitOut]:
        query = SearchQuery("xzufi_organisationseinheit")

        return await query.paginate(
            OrganisationseinheitOut.SQL_COLUMNS,
            OrganisationseinheitOut.from_row,
            self.connection,
            options,
            order_by="redaktion_id ASC, id ASC",
            use_materialized_view_count=True,
        )

    async def load_xzufi_organisationseinheit(self, identifier: XzufiIdentifier):
        row = await self.connection.fetchrow(
            f"""
            SELECT {OrganisationseinheitOut.SQL_COLUMNS} FROM xzufi_organisationseinheit WHERE redaktion_id = $1 AND id = $2
            """,
            identifier.redaktion_id,
            identifier.id,
        )
        if row is None:
            return None

        return OrganisationseinheitOut.from_row(row)

    async def load_xzufi_organisationseinheit_xml(
        self, identifier: XzufiIdentifier
    ) -> str | None:
        return await self.connection.fetchval(
            "SELECT xml_content FROM xzufi_organisationseinheit WHERE redaktion_id = $1 AND id = $2",
            identifier.redaktion_id,
            identifier.id,
        )

    async def search_xzufi_onlinedienste(
        self, options: PaginationOptions
    ) -> PaginatedResult[OnlinedienstOut]:
        query = SearchQuery("xzufi_onlinedienst")

        return await query.paginate(
            OnlinedienstOut.SQL_COLUMNS,
            OnlinedienstOut.from_row,
            self.connection,
            options,
            order_by="redaktion_id ASC, id ASC",
            use_materialized_view_count=True,
        )

    async def load_xzufi_onlinedienst(self, identifier: XzufiIdentifier):
        row = await self.connection.fetchrow(
            f"""
            SELECT {OnlinedienstOut.SQL_COLUMNS} FROM xzufi_onlinedienst WHERE redaktion_id = $1 AND id = $2
            """,
            identifier.redaktion_id,
            identifier.id,
        )
        if row is None:
            return None

        return OnlinedienstOut.from_row(row)

    async def load_xzufi_onlinedienst_xml(
        self, identifier: XzufiIdentifier
    ) -> str | None:
        return await self.connection.fetchval(
            "SELECT xml_content FROM xzufi_onlinedienst WHERE redaktion_id = $1 AND id = $2",
            identifier.redaktion_id,
            identifier.id,
        )

    async def search_xzufi_zustaendigkeiten(
        self, options: PaginationOptions
    ) -> PaginatedResult[ZustaendigkeitOut]:
        query = SearchQuery("xzufi_zustaendigkeit")
        result = await query.paginate(
            ZustaendigkeitOut.SQL_COLUMNS,
            ZustaendigkeitOut.from_row,
            self.connection,
            options,
            order_by="redaktion_id ASC, id ASC",
            use_materialized_view_count=True,
        )
        return result

    async def load_xzufi_zustaendigkeit(self, identifier: XzufiIdentifier):
        row = await self.connection.fetchrow(
            f"""
            SELECT {ZustaendigkeitOut.SQL_COLUMNS} FROM xzufi_zustaendigkeit WHERE redaktion_id = $1 AND id = $2
            """,
            identifier.redaktion_id,
            identifier.id,
        )
        if row is None:
            return None

        return ZustaendigkeitOut.from_row(row)

    async def load_xzufi_zustaendigkeit_xml(
        self, identifier: XzufiIdentifier
    ) -> str | None:
        return await self.connection.fetchval(
            "SELECT xml_content FROM xzufi_zustaendigkeit WHERE redaktion_id = $1 AND id = $2",
            identifier.redaktion_id,
            identifier.id,
        )

    async def delete_removed_spezialisierungen(self):
        await self.connection.execute(
            """
            DELETE FROM xzufi_spezialisierung WHERE NOT EXISTS 
                (
                    SELECT 1 FROM raw_pvog_resource 
                    WHERE 
                        raw_pvog_resource.resource_class = $1 AND 
                        raw_pvog_resource.redaktion_id = xzufi_spezialisierung.redaktion_id AND 
                        raw_pvog_resource.id = xzufi_spezialisierung.id
                )
            """,
            PvogResourceClass.SPEZIALISIERUNG.value,
        )

    async def delete_removed_organisationseinheiten(self):
        await self.connection.execute(
            """
            DELETE FROM xzufi_organisationseinheit WHERE NOT EXISTS 
                (
                    SELECT 1 FROM raw_pvog_resource 
                    WHERE 
                        raw_pvog_resource.resource_class = $1 AND 
                        raw_pvog_resource.redaktion_id = xzufi_organisationseinheit.redaktion_id AND 
                        raw_pvog_resource.id = xzufi_organisationseinheit.id
                )
            """,
            PvogResourceClass.ORGANISATIONSEINHEIT.value,
        )

    async def delete_removed_zustaendigkeiten(self):
        await self.connection.execute(
            """
            DELETE FROM xzufi_zustaendigkeit WHERE NOT EXISTS 
                (
                    SELECT 1 FROM raw_pvog_resource 
                    WHERE 
                        raw_pvog_resource.resource_class = $1 AND 
                        raw_pvog_resource.redaktion_id = xzufi_zustaendigkeit.redaktion_id AND 
                        raw_pvog_resource.id = xzufi_zustaendigkeit.id
                )
            """,
            PvogResourceClass.ZUSTAENDIGKEIT.value,
        )

    async def delete_removed_onlinedienste(self):
        await self.connection.execute(
            """
            DELETE FROM xzufi_onlinedienst WHERE NOT EXISTS 
                (
                    SELECT 1 FROM raw_pvog_resource 
                    WHERE 
                        raw_pvog_resource.resource_class = $1 AND 
                        raw_pvog_resource.redaktion_id = xzufi_onlinedienst.redaktion_id AND 
                        raw_pvog_resource.id = xzufi_onlinedienst.id
                )
            """,
            PvogResourceClass.ONLINEDIENST.value,
        )

    async def delete_removed_pvog_leistungen(self):
        await self.connection.execute(
            """
            DELETE FROM leistung WHERE 
                source = 'pvog' AND
                NOT EXISTS 
                    (
                        SELECT 1 FROM raw_pvog_resource 
                        WHERE 
                            raw_pvog_resource.resource_class = $1 AND 
                            raw_pvog_resource.redaktion_id = leistung.redaktion_id AND 
                            raw_pvog_resource.id = leistung.id
                    )
            """,
            PvogResourceClass.LEISTUNG.value,
        )

    async def upsert_gebiet_id(
        self, gebiet_id_batch: list[tuple[str, str]], urn: str, version: date
    ):
        await self.connection.executemany(
            """
                INSERT INTO gebiet_id (
                    id,
                    label,
                    urn,
                    version
                ) VALUES (
                    $1,
                    $2,
                    $3,
                    $4
                )
                ON CONFLICT (urn, id)
                DO UPDATE SET
                    label = EXCLUDED.label,
                    version = EXCLUDED.version
                WHERE
                    gebiet_id.version < EXCLUDED.version;
                """,
            [(batch[0], batch[1], urn, version) for batch in gebiet_id_batch],
        )

    async def get_gebiet_id_label(self, id: str) -> str | None:
        return await self.connection.fetchval(
            """
            SELECT label FROM gebiet_id WHERE id = $1
            """,
            id,
        )

    async def get_gebiet_ids(self) -> list[str]:
        result = await self.connection.execute(
            """
            SELECT id FROM gebiet_id;
            """
        )
        return [row[0] for row in result]

    async def delete_removed_gebiet_ids(self, timestamp: datetime):
        await self.connection.execute(
            """
            DELETE FROM gebiet_id WHERE last_update < $1
            """,
            timestamp,
        )

    async def get_recently_updated_datenfelder_resources(
        self, updated_since: datetime
    ) -> tuple[
        list[SchemaOut],
        list[DatenfeldgruppeOut],
        list[DatenfeldOut],
        list[SteckbriefOut],
    ]:
        result = await self.connection.fetch(
            f"""
            SELECT 
                (
                    SELECT array_agg(ROW({SchemaOut.SQL_COLUMNS})) FROM
                    (
                        SELECT {SchemaOut.SQL_COLUMNS}
                        FROM schema
                        WHERE letzte_aenderung >= $1
                        ORDER BY letzte_aenderung DESC, fim_id, fim_version
                    ) AS schema_updates
                ),
                (
                    SELECT array_agg(ROW({DatenfeldgruppeOut.SQL_COLUMNS})) FROM
                    (
                        SELECT {DatenfeldgruppeOut.SQL_COLUMNS}
                        FROM datenfeldgruppe
                        WHERE letzte_aenderung >= $1
                        ORDER BY letzte_aenderung DESC, fim_id, fim_version
                    ) AS group_updates
                ),
                (
                    SELECT array_agg(ROW({DatenfeldOut.SQL_COLUMNS})) FROM
                    (
                        SELECT {DatenfeldOut.SQL_COLUMNS}
                        FROM datenfeld
                        WHERE letzte_aenderung >= $1
                        ORDER BY letzte_aenderung DESC, fim_id, fim_version
                    ) AS field_updates  
                ),
                (
                    SELECT array_agg(ROW({SteckbriefOut.SQL_COLUMNS})) FROM
                    (
                        SELECT {SteckbriefOut.SQL_COLUMNS}
                        FROM dokumentensteckbrief
                        WHERE letzte_aenderung >= $1
                        ORDER BY letzte_aenderung DESC, fim_id, fim_version
                    ) AS document_profile_updates
                )
            """,
            updated_since,
        )
        schema_results, group_results, field_results, document_profile_results = result[
            0
        ]
        return (
            (
                [SchemaOut.from_row(row) for row in schema_results]
                if schema_results is not None
                else []
            ),
            (
                [DatenfeldgruppeOut.from_row(row) for row in group_results]
                if group_results is not None
                else []
            ),
            (
                [DatenfeldOut.from_row(row) for row in field_results]
                if field_results is not None
                else []
            ),
            (
                [SteckbriefOut.from_row(row) for row in document_profile_results]
                if document_profile_results is not None
                else []
            ),
        )

    async def get_recently_updated_services(
        self, updated_since: datetime
    ) -> list[LeistungssteckbriefOut]:
        result = await self.connection.fetch(
            f"""
            SELECT {LeistungssteckbriefOut.SQL_COLUMNS}
            FROM leistung
            WHERE geaendert_datum_zeit >= $1 AND root_for_leistungsschluessel IS NOT NULL
            ORDER BY geaendert_datum_zeit DESC, redaktion_id, id;
            """,
            updated_since,
        )

        return [LeistungssteckbriefOut.from_row(row) for row in result]

    async def get_recently_updated_processes(
        self, updated_since: datetime
    ) -> list[ProzessOut]:
        result = await self.connection.fetch(
            f"""
            SELECT {ProzessOut.SQL_COLUMNS}
            FROM prozess
            WHERE letzter_aenderungszeitpunkt >= $1
            ORDER BY letzter_aenderungszeitpunkt DESC, id;
            """,
            updated_since,
        )

        return [ProzessOut.from_row(row) for row in result]

    async def save_katalog(self, katalog: NewKatalog):
        await self.connection.execute(
            """
                INSERT INTO katalog (
                    filename,
                    source,
                    content,
                    updated_at
                ) VALUES (
                    $1,
                    $2,
                    $3,
                    $4
                ) ON CONFLICT (filename)
                DO UPDATE SET
                    source = EXCLUDED.source,
                    content = EXCLUDED.content,
                    updated_at = EXCLUDED.updated_at;
            """,
            katalog.filename,
            katalog.source,
            katalog.content,
            utc_now(),
        )

    async def get_katalog_content(self, filename: str) -> bytes | None:
        result = await self.connection.fetchval(
            "SELECT content FROM katalog WHERE filename = $1",
            filename,
        )
        return result


async def get_connection(db_url: str) -> Connection:
    return await connect(db_url)


async def clear_database(connection: Connection):
    """Clear database between tests"""
    await connection.execute(
        """
        TRUNCATE
            admin_session,
            access_token, 

            tree_relation,
            tree_node,

            schema_regel_relation,
            datenfeldgruppe_regel_relation,
            datenfeld_regel_relation,

            dokumentsteckbrief_relations,
            schema_relations,
            datenfeldgruppe_relations,
            datenfeld_relations,

            schema_file_code_list, 

            json_schema_file,
            xsd_file,
            schema_file,
            schema, 
            datenfeld, 
            datenfeldgruppe, 
            dokumentensteckbrief, 
            code_list,
            leistung,
            link,
            link_leistung_relation,
            prozess,
            regel,

            pvog_batch,
            raw_pvog_resource,
            xzufi_organisationseinheit,
            xzufi_spezialisierung,
            xzufi_zustaendigkeit,
            xzufi_onlinedienst,
            gebiet_id,
            katalog,

            prozessklasse,
            prozess_dokumentsteckbrief_relation
            
            RESTART IDENTITY CASCADE;
        """
    )


async def drop_tables(connection: Connection):
    async with connection.transaction():
        await connection.execute(
            """
            DROP TABLE IF EXISTS 
                admin_session,
                access_token, 

                tree_relation,
                tree_node,

                schema_regel_relation,
                datenfeldgruppe_regel_relation,
                datenfeld_regel_relation,

                dokumentsteckbrief_relations,
                schema_relations,
                datenfeldgruppe_relations,
                datenfeld_relations,

                schema_file_code_list, 

                json_schema_file,
                xsd_file,
                schema_file,
                schema, 
                datenfeld, 
                datenfeldgruppe, 
                steckbrief_file,
                dokumentensteckbrief, 
                code_list, 
                leistung,
                link,
                link_leistung_relation,
                prozess,
                regel,

                pvog_batch,
                raw_pvog_resource,
                xzufi_organisationseinheit,
                xzufi_spezialisierung,
                xzufi_zustaendigkeit,
                xzufi_onlinedienst,
                gebiet_id,
                download,
                katalog,

                prozessklasse,
                prozess_dokumentsteckbrief_relation,
                
                migrations

                CASCADE;
            """
        )


async def drop_functions(connection: Connection):
    async with connection.transaction():
        await connection.execute(
            """
            DROP FUNCTION IF EXISTS xzufi_xml_to_tsvector(xml_data varchar);
            DROP FUNCTION IF EXISTS leistung_tsvector_trigger();
            DROP FUNCTION IF EXISTS schema_xml_to_tsvector(xml_data varchar);
            DROP FUNCTION IF EXISTS update_schema_fts_content();
            DROP FUNCTION IF EXISTS prozess_xml_to_tsvector(xml_data varchar);
            DROP FUNCTION IF EXISTS update_schema_content();
            DROP FUNCTION IF EXISTS steckbrief_xml_to_tsvector(xml_data varchar);
            DROP FUNCTION IF EXISTS dokumentensteckbrief_tsvector_trigger();
            DROP FUNCTION IF EXISTS call_update_latest_datenfeld();
            DROP FUNCTION IF EXISTS update_latest_datenfeld(p_namespace text, p_fim_id text);
            DROP FUNCTION IF EXISTS call_update_latest_dokumentensteckbrief();
            DROP FUNCTION IF EXISTS update_latest_dokumentensteckbrief(p_fim_id text);
            DROP FUNCTION IF EXISTS call_update_latest_datenfeldgruppe();
            DROP FUNCTION IF EXISTS update_latest_datenfeldgruppe(p_namespace text, p_fim_id text);
            DROP FUNCTION IF EXISTS schema_element_xml_to_tsvector(xml_data varchar);
            DROP FUNCTION IF EXISTS schema_element_tsvector_trigger();
            DROP FUNCTION IF EXISTS call_update_latest_schema();
            DROP FUNCTION IF EXISTS update_latest_schema(p_fim_id text);
            DROP FUNCTION IF EXISTS prozess_xml_to_raw_content(xml_data TEXT);
            DROP FUNCTION IF EXISTS xzufi_xml_to_raw_content(xml_data TEXT);
            DROP FUNCTION IF EXISTS leistung_raw_content_trigger();
            DROP FUNCTION IF EXISTS schema_element_xml_to_raw_content(xml_data TEXT);
            DROP FUNCTION IF EXISTS schema_element_raw_content_trigger();
            DROP FUNCTION IF EXISTS prozessklasse_xml_to_tsvector(xml_data TEXT);
            DROP FUNCTION IF EXISTS prozessklasse_xml_to_raw_content(xml_data TEXT);
            DROP FUNCTION IF EXISTS prozessklasse_xml_content_trigger();
            DROP FUNCTION IF EXISTS immutable_array_to_string(text[], text);
            """
        )


async def reset_database(connection: Connection):
    await drop_tables(connection)
    await drop_functions(connection)

    await execute_migrations(connection)


async def create_new_database(database_url: str, database_name: str) -> str:
    connection: asyncpg.Connection = await asyncpg.connect(
        database_url,
        ssl="disable",
    )

    try:
        await connection.execute(f"DROP DATABASE IF EXISTS {database_name}")
        await connection.execute(f"CREATE DATABASE {database_name}")
    finally:
        await connection.close()

    parsed_url = urlparse(database_url)
    return f"postgresql://{parsed_url.username}:{parsed_url.password}@{parsed_url.hostname}:{parsed_url.port or 5432}/{database_name}?sslmode=disable"
