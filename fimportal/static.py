"""
Module for handling static files.

This will load all files from a directory tree into memory and create a content-based filename as well
as other information for efficient HTTP-based caching for each file.
"""

from __future__ import annotations

from dataclasses import dataclass
import hashlib
from mimetypes import guess_type
import gzip
import os
import pathlib
from typing import Iterable, Mapping

from fastapi import FastAPI, Request, Response
from starlette.datastructures import Headers
from starlette.staticfiles import NotModifiedResponse


@dataclass(slots=True)
class StaticFile:
    path: str
    content_path: str

    content_type: str
    content_length: str
    content_hash: str

    content: bytes

    @staticmethod
    def load(path: pathlib.Path, content: bytes) -> StaticFile:
        content_hash = hashlib.md5(content, usedforsecurity=False).hexdigest()
        content_type = guess_type(path)[0] or "text/plain"

        extension = path.suffix
        parent = path.parent

        content_path = parent / f"{content_hash}{extension}"

        return StaticFile(
            path=f"/{path}",
            content_path=f"/{content_path}",
            content_type=content_type,
            content_length=str(len(content)),
            content_hash=content_hash,
            content=content,
        )


# Cache for up to 30 days on the client.
# This is one of several common headers for this use case
# See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Caching#cache_busting
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control
CACHE_CONTROL_HEADER = "max-age=31536000, immutable"


class StaticFiles:
    files: list[StaticFile]
    path_to_content_path: dict[str, str]

    def __init__(self, files: list[StaticFile]):
        self.files = files

        self._path_to_content_path = {
            static_file.path: static_file.content_path for static_file in files
        }

    @staticmethod
    def load(directory: pathlib.Path | str) -> StaticFiles:
        files = []
        for file_path in _iterate_files(directory):
            content = file_path.read_bytes()
            static_file = StaticFile.load(file_path.relative_to(directory), content)

            files.append(static_file)

        return StaticFiles(files)

    def get_static_url(self, path: str) -> str:
        return self._path_to_content_path[path]

    def add_static_paths(self, app: FastAPI):
        # helper function with its own scope to bind the route
        def _add_route(file: StaticFile):
            raw_headers = Headers(
                {
                    "content-type": file.content_type,
                    "content-length": file.content_length,
                    "etag": file.content_hash,
                    "cache-control": CACHE_CONTROL_HEADER,
                }
            )

            gzip_content = gzip.compress(file.content)
            gzip_headers = Headers(
                {
                    "content-type": file.content_type,
                    "content-length": str(len(gzip_content)),
                    "etag": f"{file.content_hash}-gzip",
                    "cache-control": CACHE_CONTROL_HEADER,
                    "content-encoding": "gzip",
                }
            )

            @app.get(path=file.content_path, include_in_schema=False)
            async def _get_file(request: Request) -> Response:  # type: ignore [reportUnusedFunction]
                accepted_encoding = request.headers.get("accept-encoding")
                if accepted_encoding and "gzip" in accepted_encoding:
                    headers = gzip_headers
                    content = gzip_content
                else:
                    headers = raw_headers
                    content = file.content

                if _content_has_not_changed(headers["etag"], request.headers):
                    return NotModifiedResponse(headers)

                return Response(
                    content=content,
                    status_code=200,
                    headers=headers,
                    media_type=file.content_type,
                )

        for file in self.files:
            _add_route(file)


def _iterate_files(directory: pathlib.Path | str) -> Iterable[pathlib.Path]:
    for current_dir, _directories, filenames in os.walk(pathlib.Path(directory)):
        for filename in filenames:
            # Ignore hidden files.
            if filename.startswith("."):
                continue

            yield pathlib.Path(current_dir) / filename


def _content_has_not_changed(etag: str, request_headers: Mapping[str, str]):
    if "if-none-match" in request_headers:
        return request_headers["if-none-match"] == etag
    else:
        return False
