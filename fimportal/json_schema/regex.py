import logging
import subprocess
import pathlib


logger = logging.getLogger(__name__)


class RegexException(Exception):
    pass


FILE_DIR = pathlib.Path(__file__).parent
JS_FILE_PATH = str(FILE_DIR / "check_js_regex.mjs")

TIMEOUT_SECONDS = 10


def check_js_regex(value: str) -> None:
    """
    Check whether the provided input is a valid ECMA regex, which
    is the regex dialect used in JSON Schema.
    """

    try:
        # Pass the value via stdin instead of the command line to prevent command injection
        result = subprocess.run(
            ["node", JS_FILE_PATH],
            capture_output=True,
            input=value,
            text=True,
            timeout=TIMEOUT_SECONDS,
        )
    except subprocess.TimeoutExpired as error:
        logger.exception("Subprocess timeout expired when checking regex %s", value)
        raise RegexException("Timeout expired") from error
    else:
        if result.returncode != 0:
            raise RegexException(result.stderr)
