"""
Module to convert xdf schemas into json schema.

See `/docs/json-schema-conversion.md` for more information.
"""

from .common import *
from .xdf2 import *
from .xdf3 import *
