import { stdin as input } from "node:process";
import * as readline from "node:readline";

const rl = readline.createInterface({
    input,
});

rl.on("line", (line) => {
    try {
        new RegExp(line);
    } catch (error) {
        console.error(error.message);
        process.exit(-1);
    }

    rl.close();
});
