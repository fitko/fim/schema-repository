from typing import Any
import logging

from fimportal import genericode
from fimportal.xdatenfelder import xdf2
from fimportal.xdatenfelder.common import XdfException
from fimportal.xdatenfelder.util import check_code_lists
from .regex import check_js_regex, RegexException
from .common import JsonSchemaException, get_definition_id


logger = logging.getLogger(__name__)


def from_xdf2(
    json_schema_id: str,
    message: xdf2.SchemaMessage,
    code_lists: dict[str, genericode.CodeList],
) -> dict[str, Any]:
    """
    Create a JSON schema from the provided xdf2 schema message.
    """

    try:
        check_code_lists(message, code_lists)
    except XdfException as error:
        raise JsonSchemaException(str(error)) from error

    try:
        return {
            "$schema": "https://json-schema.org/draft/2020-12/schema",
            "$id": json_schema_id,
            "title": message.schema.name,
            "type": "object",
            "$defs": {
                **_create_group_defs(message.groups, message.fields),
                **_create_field_defs(message.fields, code_lists),
            },
            **_get_properties(message.schema.struktur, message.fields),
        }
    except genericode.GenericodeException as error:
        raise JsonSchemaException(f"Cannot convert to json schema: {error}") from error
    except RegexException as error:
        raise JsonSchemaException(str(error)) from error


def _create_field_defs(
    fields: dict[xdf2.Identifier, xdf2.Datenfeld],
    code_lists: dict[str, genericode.CodeList],
) -> dict[str, Any]:
    return {
        get_definition_id(identifier): {
            "title": field.name,
            **_get_field_type(field, code_lists),
        }
        for identifier, field in fields.items()
        if field.feldart != xdf2.Feldart.LABEL
    }


def _get_field_type(
    field: xdf2.Datenfeld, code_lists: dict[str, genericode.CodeList]
) -> dict[str, Any]:
    if field.feldart == xdf2.Feldart.INPUT:
        if field.code_listen_referenz is not None:
            raise JsonSchemaException(
                f"INPUT field with code list [element={field.identifier.id}V{field.identifier.version}]"
            )

        return _get_input_properties(field)
    elif field.feldart == xdf2.Feldart.SELECT:
        if field.datentyp != xdf2.Datentyp.TEXT:
            logger.warning(
                "Ignore select data type, use string instead [type=%s, id=%s, version=%s]",
                field.datentyp.value,
                field.identifier.id,
                field.identifier.version,
            )

        properties: dict[str, Any] = {"type": "string"}

        code_list_referenz = field.code_listen_referenz
        if code_list_referenz is None:
            raise JsonSchemaException(
                f"SELECT field without code list [element={field.identifier.id}V{field.identifier.version}]"
            )

        code_list = code_lists.get(
            code_list_referenz.genericode_identifier.canonical_version_uri
        )
        if code_list is None:
            raise JsonSchemaException(
                f"Missing CodeList [canonical_version_uri={code_list_referenz.genericode_identifier.canonical_version_uri}]"
            )

        properties["enum"] = [key for key in code_list.get_key_column()]

        return properties
    else:
        assert False


def _get_input_properties(field: xdf2.Datenfeld) -> dict[str, Any]:
    if field.datentyp == xdf2.Datentyp.TEXT:
        return {
            "type": "string",
            **_get_text_constraints(field),
        }
    elif field.datentyp == xdf2.Datentyp.NUMMER:
        return {
            "type": "number",
            **_get_number_constraints(field),
        }
    elif field.datentyp == xdf2.Datentyp.GELDBETRAG:
        return {
            "type": "number",
            "multipleOf": 0.01,
            **_get_number_constraints(field),
        }
    elif field.datentyp == xdf2.Datentyp.GANZZAHL:
        return {
            "type": "integer",
            **_get_number_constraints(field),
        }
    elif field.datentyp == xdf2.Datentyp.WAHRHEITSWERT:
        return {"type": "boolean"}
    elif field.datentyp == xdf2.Datentyp.DATUM:
        return {"type": "string", "format": "date"}
    else:
        assert field.datentyp in [xdf2.Datentyp.OBJEKT, xdf2.Datentyp.ANLAGE]
        return {"type": "string"}


def _get_text_constraints(field: xdf2.Datenfeld) -> dict[str, Any]:
    constraints = _get_field_constraints(field)
    result: dict[str, Any] = {}

    if constraints.min_length is not None:
        result["minLength"] = constraints.min_length
    if constraints.max_length is not None:
        result["maxLength"] = constraints.max_length
    if constraints.pattern is not None:
        check_js_regex(constraints.pattern)
        result["pattern"] = constraints.pattern

    return result


def _get_number_constraints(field: xdf2.Datenfeld) -> dict[str, Any]:
    constraints = _get_field_constraints(field)
    result: dict[str, Any] = {}

    if constraints.min_value is not None:
        result["minimum"] = constraints.min_value
    if constraints.max_value is not None:
        result["maximum"] = constraints.max_value

    return result


def _create_group_defs(
    groups: dict[xdf2.Identifier, xdf2.Gruppe],
    fields: dict[xdf2.Identifier, xdf2.Datenfeld],
) -> dict[str, Any]:
    return {
        get_definition_id(identifier): {
            "title": group.name,
            "type": "object",
            **_get_properties(group.struktur, fields),
        }
        for identifier, group in groups.items()
    }


def _get_properties(
    struktur: list[xdf2.ElementReference],
    fields: dict[xdf2.Identifier, xdf2.Datenfeld],
) -> dict[str, Any]:
    required: list[str] = []
    properties: dict[str, Any] = {}

    for element in struktur:
        # Ignore datafields of type `label`, as they are not important for
        # the resulting data format
        if element.element_type == xdf2.ElementType.FELD:
            field = fields[element.identifier]
            if field.feldart == xdf2.Feldart.LABEL:
                continue

        definition_id = get_definition_id(element.identifier)

        if element.anzahl.is_array:
            property = properties[definition_id] = {
                "type": "array",
                "items": {"$ref": f"#/$defs/{definition_id}"},
                "minItems": element.anzahl.min,
            }

            if element.anzahl.max is not None:
                property["maxItems"] = element.anzahl.max

            properties[definition_id] = property
        else:
            properties[definition_id] = {"$ref": f"#/$defs/{definition_id}"}

        if not element.anzahl.is_optional:
            required.append(definition_id)

    return {
        "properties": properties,
        "required": required,
    }


def _get_field_constraints(field: xdf2.Datenfeld) -> xdf2.Constraints:
    try:
        return field.get_constraints()
    except xdf2.InvalidPraezisierungException:
        # Ignore invalid constraints
        return xdf2.Constraints()
