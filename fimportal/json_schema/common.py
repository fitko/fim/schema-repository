from fimportal.xdatenfelder import xdf2, xdf3


class JsonSchemaException(Exception):
    pass


def get_definition_id(identifier: xdf2.Identifier | xdf3.Identifier) -> str:
    return f"{identifier.id}V{identifier.version}"
