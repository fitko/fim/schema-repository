from typing import Any
import logging

from fimportal import genericode
from fimportal.xdatenfelder import xdf3
from fimportal.xdatenfelder.common import delocalize_number
from fimportal.xdatenfelder.common import XdfException
from fimportal.xdatenfelder.util import check_code_lists
from .regex import RegexException, check_js_regex
from .common import JsonSchemaException, get_definition_id


logger = logging.getLogger(__name__)


def from_xdf3(
    json_schema_id: str,
    message: xdf3.SchemaMessage,
    code_lists: dict[str, genericode.CodeList],
) -> dict[str, Any]:
    """
    Create a JSON schema from the provided xdf3 schema message.
    """

    try:
        check_code_lists(message, code_lists)
    except XdfException as error:
        raise JsonSchemaException(str(error)) from error

    try:
        return {
            "$schema": "https://json-schema.org/draft/2020-12/schema",
            "$id": json_schema_id,
            "title": message.schema.name,
            "type": "object",
            "$defs": {
                **_create_field_defs(message.fields, code_lists),
                **_create_group_defs(message.groups, message.fields),
            },
            **_get_properties(message.schema.struktur, message.fields),
        }
    except RegexException as error:
        raise JsonSchemaException(str(error)) from error


def _create_group_defs(
    groups: dict[xdf3.Identifier, xdf3.Gruppe],
    fields: dict[xdf3.Identifier, xdf3.Datenfeld],
) -> dict[str, Any]:
    return {
        get_definition_id(identifier): {
            "title": group.name,
            "type": "object",
            **_get_properties(group.struktur, fields),
        }
        for identifier, group in groups.items()
    }


def _create_field_defs(
    fields: dict[xdf3.Identifier, xdf3.Datenfeld],
    code_lists: dict[str, genericode.CodeList],
) -> dict[str, Any]:
    return {
        get_definition_id(identifier): {
            "title": field.name,
            **_get_field_type(field, code_lists),
        }
        for identifier, field in fields.items()
        if not _should_ignore_field(field)
    }


def _get_field_type(
    field: xdf3.Datenfeld, code_lists: dict[str, genericode.CodeList]
) -> dict[str, Any]:
    assert not _should_ignore_field(field)

    if field.feldart == xdf3.Feldart.EINGABE:
        return _get_input_properties(field)
    elif field.feldart == xdf3.Feldart.AUSWAHL:
        if field.datentyp != xdf3.Datentyp.TEXT:
            logger.warning(
                "Ignore select data type, use string instead [type=%s, id=%s, version=%s]",
                field.datentyp.value,
                field.identifier.id,
                field.identifier.version,
            )

        properties: dict[str, Any] = {"type": "string"}

        if field.codeliste_referenz is not None:
            # Felix: This is just a sanity check, the xdf3 parser checks this already
            assert field.werte is None

            code_list = code_lists.get(field.codeliste_referenz.canonical_version_uri)
            if code_list is None:
                raise JsonSchemaException(
                    f"Missing CodeList [canonical_version_uri={field.codeliste_referenz.canonical_version_uri}]"
                )

            properties["enum"] = [
                key for key in code_list.get_key_column(field.code_key)
            ]
        elif field.werte is not None:
            # Felix: This is just a sanity check, the xdf3 parser checks this already
            assert len(field.werte) > 0

            properties["enum"] = [wert.code for wert in field.werte]
        else:
            raise JsonSchemaException(
                f"Missing code list information for select field '{get_definition_id(field.identifier)}'"
            )

        return properties
    else:
        # Sanity check, this should never happen
        raise Exception(f"Datenfeld must be EINGABE or AUSWAHL, not {field.feldart}")


def _get_input_properties(field: xdf3.Datenfeld) -> dict[str, Any]:
    if field.datentyp == xdf3.Datentyp.TEXT:
        return {
            "type": "string",
            **_get_text_constraints(field),
        }
    elif field.datentyp == xdf3.Datentyp.STRING_LATIN:
        return {
            "type": "string",
            **_get_text_constraints(field),
        }
    elif field.datentyp == xdf3.Datentyp.DATUM:
        return {"type": "string", "format": "date"}
    elif field.datentyp == xdf3.Datentyp.ZEIT:
        return {"type": "string", "format": "time"}
    elif field.datentyp == xdf3.Datentyp.ZEITPUNKT:
        return {"type": "string", "format": "date-time"}
    elif field.datentyp == xdf3.Datentyp.WAHRHEITSWERT:
        return {"type": "boolean"}
    elif field.datentyp == xdf3.Datentyp.NUMMER:
        return {
            "type": "number",
            **_get_number_constraints(field),
        }
    elif field.datentyp == xdf3.Datentyp.GANZZAHL:
        return {
            "type": "integer",
            **_get_number_constraints(field),
        }
    elif field.datentyp == xdf3.Datentyp.GELDBETRAG:
        return {
            "type": "number",
            "multipleOf": 0.01,
            **_get_number_constraints(field),
        }
    else:
        assert field.datentyp in [xdf3.Datentyp.OBJEKT, xdf3.Datentyp.ANLAGE]
        return {"type": "string"}


def _get_text_constraints(field: xdf3.Datenfeld) -> dict[str, Any]:
    result: dict[str, Any] = {}

    praezisierung = field.praezisierung
    if praezisierung is None:
        return result

    if praezisierung.min_length is not None:
        result["minLength"] = praezisierung.min_length
    if praezisierung.max_length is not None:
        result["maxLength"] = praezisierung.max_length
    if praezisierung.pattern is not None:
        check_js_regex(praezisierung.pattern)
        result["pattern"] = praezisierung.pattern

    return result


def _get_number_constraints(field: xdf3.Datenfeld) -> dict[str, Any]:
    result: dict[str, Any] = {}

    praezisierung = field.praezisierung
    if praezisierung is None:
        return result

    if praezisierung.min_value is not None:
        result["minimum"] = _parse_float(field, praezisierung.min_value)
    if praezisierung.max_value is not None:
        result["maximum"] = _parse_float(field, praezisierung.max_value)

    return result


def _parse_float(field: xdf3.Datenfeld, value: str) -> float:
    try:
        return float(delocalize_number(value))
    except Exception as error:
        raise JsonSchemaException(
            f"Invalid numeric limit '{value}' in field '{get_definition_id(field.identifier)}'"
        ) from error


def _get_properties(
    struktur: list[xdf3.ElementReference],
    fields: dict[xdf3.Identifier, xdf3.Datenfeld],
) -> dict[str, Any]:
    required: list[str] = []
    properties: dict[str, Any] = {}

    for element in struktur:
        if element.element_type == xdf3.ElementType.FELD:
            field = fields[element.identifier]
            if _should_ignore_field(field):
                continue

        definition_id = get_definition_id(element.identifier)

        if element.anzahl.is_array:
            property = properties[definition_id] = {
                "type": "array",
                "items": {"$ref": f"#/$defs/{definition_id}"},
                "minItems": element.anzahl.min,
            }

            if element.anzahl.max is not None:
                property["maxItems"] = element.anzahl.max

            properties[definition_id] = property
        else:
            properties[definition_id] = {"$ref": f"#/$defs/{definition_id}"}

        if not element.anzahl.is_optional:
            required.append(definition_id)

    return {
        "properties": properties,
        "required": required,
    }


def _should_ignore_field(field: xdf3.Datenfeld) -> bool:
    return field.feldart in [
        xdf3.Feldart.GESPERRT,
        xdf3.Feldart.STATISCH,
        xdf3.Feldart.VERSTECKT,
    ]
