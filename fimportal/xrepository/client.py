from abc import ABC, abstractmethod
import logging
from pathlib import Path

import httpx

from fimportal import genericode, xml
from fimportal.genericode.code_list.serialize import serialize_code_list


logger = logging.getLogger(__name__)


class XRepositoryError(Exception):
    pass


class MissingCodeListError(XRepositoryError):
    def __init__(self, uri: str):
        super().__init__(f"Cannot find code list '{uri}' in XRepository")


class UnsupportedFeatureError(XRepositoryError):
    pass


class Client(ABC):
    """
    Interface for a XRepository client.
    Makes it easy to provide a test client during tests.
    """

    @abstractmethod
    async def load_code_list(self, uri: str) -> genericode.CodeListWithContent: ...

    @abstractmethod
    async def load_code_list_kennungen(self, uri: str) -> list[str]: ...


class HttpClient(Client):
    def __init__(self):
        self._client = httpx.AsyncClient(
            transport=httpx.AsyncHTTPTransport(
                retries=5,
            ),
            timeout=60,
        )

    async def load_code_list_kennungen(self, uri: str) -> list[str]:
        url = f"https://www.xrepository.de/api/codeliste/{uri}/metadaten"
        try:
            response = await self._client.get(url)
            response.raise_for_status()

            data = await response.aread()
        except httpx.HTTPStatusError as error:
            if error.response.status_code == 404:
                raise MissingCodeListError(uri) from error
            else:
                raise XRepositoryError(
                    f"failed to load latest code list version - {uri}"
                ) from error
        return _parse_code_list_kennungen(data)

    async def load_code_list(self, uri: str) -> genericode.CodeListWithContent:
        url = f"https://www.xrepository.de/api/version_codeliste/{uri}/genericode"

        try:
            response = await self._client.get(url)
            response.raise_for_status()

            data = await response.aread()
        except httpx.HTTPStatusError as error:
            if error.response.status_code == 404:
                raise MissingCodeListError(uri) from error
            else:
                raise XRepositoryError(f"failed to load code list {uri}") from error
        except httpx.RequestError as error:
            raise XRepositoryError(f"failed to load code list {uri}") from error

        try:
            return genericode.parse_code_list_with_content(data)
        except genericode.UnsupportedFeatureException as error:
            raise UnsupportedFeatureError(str(error)) from error
        except genericode.GenericodeException as error:
            logger.exception("Failed to parse code list %s", uri)
            raise XRepositoryError(f"failed to parse code list {uri}") from error


def _parse_code_list_kennungen(data: bytes) -> list[str]:
    node = xml.parse_document(data)
    kennungen = []
    try:
        for child in node:
            if child.tag == "referenzen":
                kennungen.append(_parse_kennung(child))
    except xml.ParserException as error:
        raise XRepositoryError(
            f"Could not parse code list kennungen: {str(error)} | {xml.serialize(node).decode('utf-8')[:1000]}"
        ) from error
    return kennungen


def _parse_kennung(node: xml.XmlElement) -> str:
    for child in node:
        if child.tag == "kennung":
            return xml.parse_value(child)
    raise XRepositoryError("Expected `referenzen` node to contain `kennung`.")


class FakeClient(Client):
    def __init__(self):
        self._codelist_cache: dict[str, genericode.CodeListWithContent] = {}
        self._kennungen_cache: dict[str, list[str]] = {}

    async def load_code_list_kennungen(self, uri: str) -> list[str]:
        kennungen = self._kennungen_cache.get(uri)
        if kennungen is None:
            return []

        return kennungen

    async def load_code_list(self, uri: str) -> genericode.CodeListWithContent:
        code_list = self._codelist_cache.get(uri)
        if code_list is None:
            raise MissingCodeListError(uri)

        return code_list

    def register_code_list(self, code_list: genericode.CodeList):
        self._codelist_cache[code_list.identifier.canonical_version_uri] = (
            genericode.CodeList(
                identifier=code_list.identifier,
                default_code_key=code_list.default_code_key,
                default_name_key=code_list.default_name_key,
                columns=code_list.columns,
            ),
            serialize_code_list(code_list).decode("utf-8"),
        )

    def register_kennungen(self, uri: str, kennungen: list[str]):
        self._kennungen_cache[uri] = kennungen

    def register_code_list_file(self, path: str | Path):
        code_list = genericode.load_code_list(path)
        self.register_code_list(code_list)

    def clear(self):
        self._codelist_cache = {}
        self._kennungen_cache = {}
