import asyncio
from typing import Iterable

from fimportal import genericode
from .client import Client, MissingCodeListError


class Service:
    def __init__(self, client: Client):
        self._client = client

    async def load_code_lists(
        self, canonical_version_uris: Iterable[str]
    ) -> list[genericode.CodeListWithContent]:
        tasks = asyncio.gather(
            *[self._client.load_code_list(uri) for uri in canonical_version_uris]
        )

        try:
            return await tasks
        except Exception:
            tasks.cancel()
            raise

    async def load_available_code_lists(
        self, canonical_verion_uris: Iterable[str]
    ) -> dict[str, genericode.CodeListWithContent | None]:
        async def _load_optional_code_list(
            canonical_version_uri: str,
        ) -> genericode.CodeListWithContent | None:
            try:
                return await self._client.load_code_list(canonical_version_uri)
            except MissingCodeListError:
                return None

        tasks = asyncio.gather(
            *[_load_optional_code_list(uri) for uri in canonical_verion_uris]
        )

        try:
            results = await tasks
        except Exception:
            tasks.cancel()
            raise

        uri_to_content = {}
        for uri, result in zip(canonical_verion_uris, results):
            if result is not None:
                assert result[0].identifier.canonical_version_uri == uri

            uri_to_content[uri] = result

        return uri_to_content
