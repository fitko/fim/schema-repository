import logging
import xml.etree.ElementTree as ET

from fimportal import genericode
from fimportal.json_schema import check_js_regex
from fimportal.json_schema.regex import RegexException
from fimportal.xdatenfelder import xdf2
from fimportal.xdatenfelder.xdf2 import Datentyp

logger = logging.getLogger(__name__)

# Mapping for the datatypes (xdf2 to xml schema)
FIELD_TYPE_MAP = {
    Datentyp.TEXT: "xs:string",
    Datentyp.NUMMER: "xs:float",
    Datentyp.GELDBETRAG: "xs:decimal",
    Datentyp.GANZZAHL: "xs:integer",
    Datentyp.WAHRHEITSWERT: "xs:boolean",
    Datentyp.DATUM: "xs:date",
    Datentyp.OBJEKT: "xs:string",
    Datentyp.ANLAGE: "xs:string",
}

# Constants for XML Schema elements
XS_SCHEMA = "xs:schema"
XS_ELEMENT = "xs:element"
XS_COMPLEX_TYPE = "xs:complexType"
XS_SIMPLE_TYPE = "xs:simpleType"
XS_SEQUENCE = "xs:sequence"
XS_ANNOTATION = "xs:annotation"
XS_DOCUMENTATION = "xs:documentation"
XS_RESTRICTION = "xs:restriction"
XS_ENUMERATION = "xs:enumeration"
XS_ANYTYPE = "xs:anyType"
XS_MIN_LENGTH = "xs:minLength"
XS_MAX_LENGTH = "xs:maxLength"
XS_PATTERN = "xs:pattern"
XS_MAX_INCLUSIVE = "xs:maxInclusive"
XS_MIN_INCLUSIVE = "xs:minInclusive"

# Map constraints to XML Schema elements
constraints_mapping = {
    "min_length": XS_MIN_LENGTH,
    "max_length": XS_MAX_LENGTH,
    "min_value": XS_MIN_INCLUSIVE,
    "max_value": XS_MAX_INCLUSIVE,
    "pattern": XS_PATTERN,
}


class XsdException(Exception): ...


def from_xdf2(
    message: xdf2.SchemaMessage,
    code_lists: dict[str, genericode.CodeList],
) -> str:
    _check_code_lists(message, code_lists)

    schema_id = message.schema.identifier.id
    schema_version = message.schema.identifier.version

    namespace = f"urn:xoev-de:xfall:standard:fim-{schema_id.lower()}_{schema_version}"
    schema_attrib = {
        "xmlns:xfd": namespace,
        "xmlns:xs": "http://www.w3.org/2001/XMLSchema",
        "attributeFormDefault": "unqualified",
        "elementFormDefault": "qualified",
        "targetNamespace": namespace,
    }

    if schema_version is not None:
        schema_attrib["version"] = schema_version

    # Combine fields and groups for ease of processing
    fields_and_groups = combine_fields_and_groups(message.groups, message.fields)

    try:
        # Clean up the references (remove the label elements)
        cleaned_references = _clean_up_structure(
            message.schema.struktur, fields_and_groups
        )
    except Exception as error:
        raise XsdException("Could not clean up xsd structure") from error

    # Create the main element for the xml schema
    schema_element = ET.Element(XS_SCHEMA, attrib=schema_attrib)
    main_element = ET.SubElement(
        schema_element,
        XS_ELEMENT,
        name=message.schema.name,
        id=create_schema_id(message.schema.identifier),
    )

    # Create the schema structure and add the complex types to the xml schema
    _create_schema_structure(
        fields_and_groups,
        code_lists,
        cleaned_references,
        main_element,
    )
    _create_complex_types(
        fields_and_groups,
        message.groups,
        code_lists,
        schema_element,
    )

    return xml_to_str(schema_element)


# Check if code lists are missing
def _check_code_lists(
    message: xdf2.SchemaMessage,
    code_lists: dict[str, genericode.CodeList],
):
    missing_code_lists = [
        uri for uri in message.get_code_list_uris() if uri not in code_lists
    ]
    if len(missing_code_lists) > 0:
        raise XsdException(f"Missing code lists: {missing_code_lists}")


def _create_schema_structure(
    fields_and_groups: dict[str, xdf2.Gruppe | xdf2.Datenfeld],
    code_lists: dict[str, genericode.CodeList],
    references: list[xdf2.ElementReference],
    parent_element: ET.Element,
):
    """
    Creating xs:element schema structure (needed for the first part of the xml schema)
    """

    sequence = ET.SubElement(
        ET.SubElement(parent_element, XS_COMPLEX_TYPE), XS_SEQUENCE
    )

    for reference in references:
        if (
            field_or_group := fields_and_groups.get(reference.identifier.id)
        ) is not None:
            _add_elements_to_schema_structure(
                field_or_group, code_lists, reference, sequence
            )


# Add elements to the schema structure
def _add_elements_to_schema_structure(
    field_or_group: xdf2.Datenfeld | xdf2.Gruppe,
    code_lists: dict[str, genericode.CodeList],
    reference: xdf2.ElementReference | None,
    parent_element: ET.Element,
):
    element = ET.SubElement(parent_element, XS_ELEMENT)

    if reference is not None:
        process_anzahl(reference.anzahl, element)
    create_element(element, field_or_group)
    _create_documentation_element(element, field_or_group)
    if isinstance(field_or_group, xdf2.Datenfeld):
        _create_restriction_element(element, field_or_group, code_lists)


# Creating xs:complexTypes (needed for the second part of the xml schema)
def _create_complex_types(
    fields_and_groups: dict[str, xdf2.Gruppe | xdf2.Datenfeld],
    groups: dict[xdf2.Identifier, xdf2.Gruppe],
    code_lists: dict[str, genericode.CodeList],
    parent_element: ET.Element,
):
    for group in groups.values():
        sequence = ET.SubElement(
            ET.SubElement(
                parent_element,
                XS_COMPLEX_TYPE,
                {"name": f"{group.identifier.id}V{group.identifier.version}"},
            ),
            XS_SEQUENCE,
        )

        for element in group.struktur:
            if (
                field_or_group := fields_and_groups.get(element.identifier.id)
            ) is not None:
                _process_fields_or_groups(field_or_group, element, code_lists, sequence)


# Add elements to the groups_and_fields dict
def _process_fields_or_groups(
    field_or_group: xdf2.Datenfeld | xdf2.Gruppe,
    reference: xdf2.ElementReference | None,
    code_lists: dict[str, genericode.CodeList],
    parent_element: ET.Element,
):
    element = ET.SubElement(parent_element, XS_ELEMENT)

    if reference is not None:
        process_anzahl(reference.anzahl, element)
    create_element(element, field_or_group)
    _create_documentation_element(element, field_or_group)
    if isinstance(field_or_group, xdf2.Datenfeld):
        _create_restriction_element(element, field_or_group, code_lists)


# Creates the xs:element
def create_element(
    element: ET.Element,
    field_or_group: xdf2.Datenfeld | xdf2.Gruppe,
) -> ET.Element:
    element.set(
        "name", f"{field_or_group.identifier.id}V{field_or_group.identifier.version}"
    )
    element.set("type", get_type_property(field_or_group))

    return element


# Adds the maxOccurs and minOccurs attributes to the xs:element
def process_anzahl(anzahl: xdf2.Anzahl, element: ET.Element) -> None:
    min_, max_ = anzahl.min, anzahl.max

    # If both min_ and max_ are 1, we don't set any attribute.
    if min_ == max_ == 1:
        return

    # If min_ is 0 and max_ is 1, we set the minOccurs attribute only.
    if min_ == 0 and max_ == 1:
        element.set("minOccurs", str(min_))
        return

    # Default: Set min and max attributes (when max is an array set it to unbounded)
    element.set("minOccurs", str(min_))
    element.set("maxOccurs", str(max_) if max_ is not None else "unbounded")


# Creates the xs:annotation and xs:documentation part of a xs:element or xs:complexType
def _create_documentation_element(
    parent_element: ET.Element,
    field_or_group: xdf2.Datenfeld | xdf2.Gruppe,
):
    name = getattr(field_or_group, "name")
    if name is not None:
        documentation_element = ET.SubElement(
            ET.SubElement(parent_element, XS_ANNOTATION), XS_DOCUMENTATION
        )
        name_element = ET.SubElement(documentation_element, "name")
        name_element.text = name


# Create the xs:restriction element containing the xs:enumeration elements
def _create_restriction_element(
    xs_element: ET.Element,
    field: xdf2.Datenfeld,
    code_lists: dict[str, genericode.CodeList],
):
    def has_code_list(field_reference: xdf2.Datenfeld) -> bool:
        """Check if a code list exists for the field."""
        if field_reference.code_listen_referenz is None:
            return False
        gen_id = field_reference.code_listen_referenz.genericode_identifier.canonical_version_uri
        return bool(code_lists.get(gen_id))

    constraints = _get_field_constraints(field)
    if constraints.is_empty() and not has_code_list(field):
        return

    # Create restriction element
    simple_type_element = ET.SubElement(xs_element, XS_SIMPLE_TYPE)
    restriction_element = ET.SubElement(simple_type_element, XS_RESTRICTION)
    restriction_element.set("base", get_type_property(field))

    add_constraints(field.datentyp, constraints, restriction_element)

    if field.code_listen_referenz:
        code_list = code_lists.get(
            field.code_listen_referenz.genericode_identifier.canonical_version_uri
        )

        if code_list is not None:
            try:
                # Add enumeration items to restriction
                for item in code_list.get_key_column():
                    item_element = ET.SubElement(restriction_element, XS_ENUMERATION)
                    item_element.set("value", str(item))
            except genericode.GenericodeException as error:
                raise XsdException(
                    f"Could not generate XSD: {str(error)} [code_list={field.code_listen_referenz.identifier.id}]"
                ) from error

    # If after all restriction_element is empty then remove it from tree
    if not len(restriction_element):
        xs_element.remove(simple_type_element)


# Getting the property of a Datenfeld or a Gruppe element
def get_type_property(field_or_group: xdf2.Datenfeld | xdf2.Gruppe) -> str:
    if isinstance(field_or_group, xdf2.Datenfeld):
        if field_or_group.feldart == xdf2.Feldart.INPUT:
            if field_or_group.datentyp not in FIELD_TYPE_MAP:
                raise XsdException(f"Data type {field_or_group.datentyp} not supported")
            return FIELD_TYPE_MAP[field_or_group.datentyp]
        elif field_or_group.feldart == xdf2.Feldart.SELECT:
            if field_or_group.datentyp != xdf2.Datentyp.TEXT:
                logger.warning(
                    "Ignore select data type, use string instead [type=%s, id=%s, version=%s]",
                    field_or_group.datentyp.value,
                    field_or_group.identifier.id,
                    field_or_group.identifier.version,
                )
            return FIELD_TYPE_MAP[xdf2.Datentyp.TEXT]
    return f"xfd:{field_or_group.identifier.id}V{field_or_group.identifier.version}"


# Convert xml to string
def xml_to_str(root: ET.Element) -> str:
    tree_str = ET.tostring(root, encoding="unicode", method="xml")
    return f'<?xml version="1.0" encoding="UTF-8"?>\n{tree_str}'


# Combining fields and groups into one dictionary (for easier lookup)
def combine_fields_and_groups(
    groups: dict[xdf2.Identifier, xdf2.Gruppe],
    fields: dict[xdf2.Identifier, xdf2.Datenfeld],
) -> dict[str, xdf2.Gruppe | xdf2.Datenfeld]:
    return {
        element.identifier.id: element
        for element in list(fields.values()) + list(groups.values())
    }


# Create correct schema id pattern
def create_schema_id(identifier: xdf2.Identifier) -> str:
    return f"fim.{identifier.id}V{identifier.version}"


# Remove the  elements which are not groups or of type input or select
def _clean_up_structure(
    structure: list[xdf2.ElementReference],
    groups_and_fields: dict[str, xdf2.Gruppe | xdf2.Datenfeld],
) -> list[xdf2.ElementReference]:
    new_structure = []
    for element in structure:
        if (field_or_group := groups_and_fields.get(element.identifier.id)) is not None:
            is_group = isinstance(field_or_group, xdf2.Gruppe)
            is_input_or_select_field = isinstance(
                field_or_group, xdf2.Datenfeld
            ) and field_or_group.feldart in [xdf2.Feldart.INPUT, xdf2.Feldart.SELECT]

            if is_group or is_input_or_select_field:
                new_structure.append(element)

    return new_structure


# Adding constraints to the restriction element
def add_constraints(
    datentyp: xdf2.Datentyp,
    constraints: xdf2.Constraints,
    restriction_element: ET.Element,
) -> None:
    constraint_attributes = ["min_length", "max_length", "min_value", "max_value"]

    if datentyp in [
        xdf2.Datentyp.NUMMER,
        xdf2.Datentyp.GANZZAHL,
        xdf2.Datentyp.GELDBETRAG,
    ]:
        constraint_attributes.remove("min_length")
        constraint_attributes.remove("max_length")
    elif datentyp != xdf2.Datentyp.TEXT:
        return

    for attr in constraint_attributes:
        value = getattr(constraints, attr, None)
        if value is not None:
            element = ET.SubElement(restriction_element, constraints_mapping[attr])

            # If the field type is integer, only use the whole number part
            if datentyp.GANZZAHL:
                value = int(value)

            element.set("value", str(value))

    if constraints.pattern is not None:
        try:
            check_js_regex(constraints.pattern)
        except RegexException as error:
            raise XsdException("Invalid regex") from error
        else:
            element = ET.SubElement(restriction_element, XS_PATTERN)
            element.set("value", str(constraints.pattern))


# Get constraints from field
def _get_field_constraints(field: xdf2.Datenfeld) -> xdf2.Constraints:
    try:
        return field.get_constraints()
    except xdf2.InvalidPraezisierungException:
        # Ignore invalid constraints
        return xdf2.Constraints()
