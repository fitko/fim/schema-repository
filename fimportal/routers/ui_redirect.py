from fastapi import APIRouter, Request, status
from fastapi.responses import HTMLResponse, RedirectResponse
from fimportal.helpers import RenderFunction


def create_redirect_router(render: RenderFunction):
    router = APIRouter()

    @router.get("/detail/L/{leistungsschluessel}", response_class=HTMLResponse)
    async def get_old_service_profile_details(  # type: ignore [reportUnusedFunction]
        request: Request,
        leistungsschluessel: str,
    ):
        return RedirectResponse(url=f"/services/{leistungsschluessel}")

    @router.get("/detail/D/{fim_id_with_version}", response_class=HTMLResponse)
    async def get_old_schema_details(  # type: ignore [reportUnusedFunction]
        request: Request,
        fim_id_with_version: str,
    ):
        def _resource_not_found(request: Request, fim_id_with_version: str):
            return render(
                request,
                "404.html.jinja",
                {
                    "message": f"Resource {fim_id_with_version} konnte nicht gefunden werden.",
                },
                status_code=status.HTTP_404_NOT_FOUND,
            )

        try:
            fim_id, fim_version = fim_id_with_version.split("V")
        except ValueError:
            return _resource_not_found(request, fim_id_with_version)

        if fim_id[0] == "S":
            return RedirectResponse(url=f"/schemas/{fim_id}/{fim_version}")

        if fim_id[0] == "D":
            return RedirectResponse(url=f"/document-profiles/{fim_id}/{fim_version}")

        return _resource_not_found(request, fim_id_with_version)

    @router.get("/detail/P/{id}", response_class=HTMLResponse)
    async def get_old_processclass_details(  # type: ignore [reportUnusedFunction]
        request: Request,
        id: str,
    ):
        return RedirectResponse(url=f"/processclasses/{id}")

    @router.get("/fim-feeds/prozesse.rss")
    async def get_old_prozesse_rss_feed(request: Request):  # type: ignore [reportUnusedFunction]
        return RedirectResponse(url="/tools/rss/processes")

    @router.get("/fim-feeds/datenfelder.rss")
    async def get_old_schemas_rss_feed(request: Request):  # type: ignore [reportUnusedFunction]
        return RedirectResponse(url="/tools/rss/schemas")

    @router.get("/fim-feeds/leistungen.rss")
    async def get_old_leistungen_rss_feed(request: Request):  # type: ignore [reportUnusedFunction]
        return RedirectResponse(url="/tools/rss/services")

    return router
