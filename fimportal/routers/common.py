from dataclasses import dataclass
from enum import Enum
from typing import Literal, TypeVar, Generic
from urllib.parse import urlencode

from fastapi import Request

from fimportal.models.xdf3 import RelationOut
from fimportal.xprozesse.prozess import Zustandsangaben


class Resource(Enum):
    SCHEMA = "schema"
    DOCUMENT_PROFILE = "document-profile"
    FIELD = "field"
    GROUP = "group"
    SERVICE = "service"
    SERVICE_DESCRIPTION = "service-description"
    PROCESS = "process"
    PROCESSCLASS = "processclass"
    SAMPLEPROCESS = "sampleprocess"

    def to_label(self) -> str:
        match self:
            case Resource.SCHEMA:
                return "Datenschemata"
            case Resource.DOCUMENT_PROFILE:
                return "Dokumentsteckbriefe"
            case Resource.GROUP:
                return "Datenfeldgruppen"
            case Resource.FIELD:
                return "Datenfelder"
            case Resource.SERVICE:
                return "Steckbriefe/Bundesstammtexte"
            case Resource.SERVICE_DESCRIPTION:
                return "Daten aus Portalverbund"
            case Resource.PROCESSCLASS:
                return "Prozessklasse"
            case Resource.PROCESS:
                return "Prozesse"
            case Resource.SAMPLEPROCESS:
                return "Musterprozesse"


# Generic type for tab IDs
TabId = TypeVar("TabId", bound=str)


@dataclass(slots=True)
class UiHeaderTab(Generic[TabId]):
    """
    Configuration for a navigation tab used by the nav_link macro.

    Generic over the type of tab ID to allow type-safe tab IDs for different resources
    while sharing the common tab functionality.
    """

    id: TabId
    icon: str
    label: str
    active_tab: TabId
    base_href: str
    search_query_string: str

    @property
    def path(self) -> str:
        return (
            f"{self.base_href}{self.search_query_string}"
            if self.id == "index"
            else f"{self.base_href}/{self.id}{self.search_query_string}"
        )

    @property
    def is_active(self) -> bool:
        return self.id == self.active_tab


@dataclass(slots=True)
class UiBausteinReferenz:
    id: str
    href: str
    label: str | None
    exists: bool = True


@dataclass(slots=True)
class UiHandlungsgrundlage:
    label: str
    href: str | None


@dataclass(slots=True)
class ProzessMetadata:
    zuletzt_geaendert_am: str | None
    anmerkungen: str | None
    methodisch_freigegeben_am: str | None
    fachlich_freigegeben_am: str | None
    fachlich_freigebende_stelle: str | None

    @staticmethod
    def create(
        zustandsangaben: Zustandsangaben | None, fachlich_freigebende_stelle: str | None
    ) -> "ProzessMetadata":
        if zustandsangaben is None:
            return ProzessMetadata(None, None, None, None, None)

        return ProzessMetadata(
            zuletzt_geaendert_am=zustandsangaben.letzter_aenderungszeitpunkt.strftime(
                "%d.%m.%Y"
            )
            if zustandsangaben.letzter_aenderungszeitpunkt is not None
            else None,
            anmerkungen=zustandsangaben.anmerkung_letzte_aenderung,
            methodisch_freigegeben_am=zustandsangaben.formeller_freigabezeitpunkt.strftime(
                "%d.%m.%Y"
            )
            if zustandsangaben.formeller_freigabezeitpunkt is not None
            else None,
            fachlich_freigegeben_am=zustandsangaben.fachlicher_freigabezeitpunkt.strftime(
                "%d.%m.%Y"
            )
            if zustandsangaben.fachlicher_freigabezeitpunkt is not None
            else None,
            fachlich_freigebende_stelle=fachlich_freigebende_stelle,
        )


@dataclass(slots=True)
class UiRelationen:
    abgeleitet: list[UiBausteinReferenz]
    ersetzt: list[UiBausteinReferenz]
    aequivalent: list[UiBausteinReferenz]
    verknuepft: list[UiBausteinReferenz]
    has_no_relation: bool

    @staticmethod
    def create(
        relations: list[RelationOut],
        resource: Literal["schema", "steckbrief", "group", "field"],
    ):
        has_no_relation = len(relations) == 0

        if has_no_relation:
            return UiRelationen(
                abgeleitet=[],
                ersetzt=[],
                aequivalent=[],
                verknuepft=[],
                has_no_relation=has_no_relation,
            )

        abgeleitet = []
        ersetzt = []
        aequivalent = []
        verknuepft = []

        sorted_relations = {
            "ABL": abgeleitet,
            "ERS": ersetzt,
            "EQU": aequivalent,
            "VKN": verknuepft,
        }

        link_base = {
            "schema": "/schemas/",
            "steckbrief": "/document-profiles/",
            "group": "/groups/baukasten/",
            "field": "/fields/baukasten/",
        }

        for relation in relations:
            link = (
                link_base[resource]
                + relation.fim_id
                + "/"
                + (relation.fim_version or "latest")
            )

            reference = UiBausteinReferenz(
                relation.fim_id,
                link,
                relation.name or relation.fim_id,
                exists=relation.name is not None,
            )

            sorted_relations[relation.typ.value].append(reference)

        return UiRelationen(
            abgeleitet=abgeleitet,
            ersetzt=ersetzt,
            aequivalent=aequivalent,
            verknuepft=verknuepft,
            has_no_relation=has_no_relation,
        )


def ui_display_eingabe_ausgabe(eingabe: str | None, ausgabe: str | None) -> list[str]:
    """Define display text of an Eingabe/Ausgabe text pair

    In group and field detail views, there are HilfetextEingabe and -Ausgabe
    as well as BezeichnungEingabe and -Ausgabe.

    We only display those present and if they are identical we only display the text itself.
    """
    eingabe = eingabe.strip() if eingabe else None
    ausgabe = ausgabe.strip() if ausgabe else None

    if eingabe and ausgabe:
        if eingabe == ausgabe:
            return [eingabe]
        else:
            return [f"Eingabe: {eingabe}", f"Ausgabe: {ausgabe}"]
    else:
        if eingabe:
            return [f"Eingabe: {eingabe}"]
        elif ausgabe:
            return [f"Ausgabe {ausgabe}"]
        else:
            return []


def create_clean_query_string(request: Request, resource: Resource) -> str:
    query_params = [
        (key, value)
        for key, value in request.query_params.multi_items()
        if key != "language_code" and value != "undefined"
    ]
    return (
        "?" + urlencode(query_params) if query_params else f"?resource={resource.value}"
    )
