from __future__ import annotations

from dataclasses import dataclass
from typing import Any, AsyncGenerator, Callable, Literal

from fastapi import APIRouter, Depends, Request, status
from starlette.responses import HTMLResponse

from fimportal.helpers import RenderFunction
from fimportal.models.imports import XdfVersion
from fimportal.models.ui_prototype import (
    AvailableVersion,
    QualityReportView,
    SchemaTree,
)
from fimportal.models.xdf3 import (
    CodeListOut,
    FullSchemaOut,
    JsonSchemaFile,
    SchemaOut,
    SchemaUpload,
    XsdFile,
)
from fimportal.routers.common import (
    Resource,
    UiHeaderTab,
    UiBausteinReferenz,
    UiHandlungsgrundlage,
    UiRelationen,
    create_clean_query_string,
)
from fimportal.service import Service
from fimportal.xdatenfelder import xdf2, xdf2_to_xdf3, xdf3
from fimportal.xdatenfelder.reports import QualityReport
from fimportal.xdatenfelder.xdf3.common import Stichwort

SchemaTabId = Literal["index", "structure", "report", "downloads"]
SchemaTab = UiHeaderTab[SchemaTabId]


@dataclass(slots=True)
class SchemaHeader:
    """
    Data for templates/components/schema_header.html.jinja
    """

    name: str
    fim_id: str
    fim_version: str
    xdf_version: str
    freigabe_status: str

    base_href: str
    back_url: str
    tabs: list[SchemaTab]
    active_tab: SchemaTabId

    @staticmethod
    def create(
        schema: FullSchemaOut, active_tab: SchemaTabId, search_query_string: str
    ) -> SchemaHeader:
        base_href = f"/schemas/{schema.fim_id}/{schema.fim_version}"

        return SchemaHeader(
            name=schema.name,
            fim_id=schema.fim_id,
            fim_version=schema.fim_version,
            xdf_version="XDatenfelder " + schema.xdf_version.value,
            freigabe_status=schema.freigabe_status.to_label(),
            base_href=base_href,
            active_tab=active_tab,
            back_url=f"/search{search_query_string}",
            tabs=[
                SchemaTab(
                    id="index",
                    label="Übersicht",
                    icon="bi-eye-fill",
                    active_tab=active_tab,
                    base_href=base_href,
                    search_query_string=search_query_string,
                ),
                SchemaTab(
                    id="structure",
                    label="Struktur",
                    icon="bi-diagram-2-fill",
                    active_tab=active_tab,
                    base_href=base_href,
                    search_query_string=search_query_string,
                ),
                SchemaTab(
                    id="report",
                    label="Technischer Report",
                    icon="bi-tools",
                    active_tab=active_tab,
                    base_href=base_href,
                    search_query_string=search_query_string,
                ),
                SchemaTab(
                    id="downloads",
                    label="Downloads",
                    icon="bi-download",
                    active_tab=active_tab,
                    base_href=base_href,
                    search_query_string=search_query_string,
                ),
            ],
        )


@dataclass(slots=True)
class UiShortDownloadLink:
    label: str
    href: str

    @staticmethod
    def from_upload(file: SchemaUpload, xdf_version: XdfVersion) -> UiShortDownloadLink:
        return UiShortDownloadLink(
            label=f"Datenschema (xDatenfelder {xdf_version.value})",
            href=file.url,
        )

    @staticmethod
    def from_json_schema_file(file: JsonSchemaFile) -> UiShortDownloadLink:
        return UiShortDownloadLink(
            label="Datenschema (JSON Schema)",
            href=file.url,
        )

    @staticmethod
    def from_xsd_file(file: XsdFile) -> UiShortDownloadLink:
        return UiShortDownloadLink(
            label="Datenschema (XSD)",
            href=file.url,
        )


@dataclass(slots=True)
class UiSchemaData:
    """
    All of the data that is displayed in xdf 3.0.0 format on the page.
    """

    bezug: list[UiHandlungsgrundlage]
    hilfetext: str | None
    stichwoerter: list[Stichwort] | None
    ableitungsmodifikationen_repraesentation: str
    ableitungsmodifikationen_struktur: str

    @staticmethod
    def from_xdf2_schema(schema: xdf2.Schema) -> UiSchemaData:
        bezug = xdf2_to_xdf3.map_bezug(schema.bezug)

        return UiSchemaData(
            bezug=[UiHandlungsgrundlage(el.text, el.link) for el in bezug],
            hilfetext=schema.hilfetext,
            stichwoerter=None,
            ableitungsmodifikationen_repraesentation=schema.ableitungsmodifikationen_repraesentation.to_label(),
            ableitungsmodifikationen_struktur=schema.ableitungsmodifikationen_struktur.to_label(),
        )

    @staticmethod
    def from_xdf3_schema(schema: xdf3.Schema) -> UiSchemaData:
        return UiSchemaData(
            bezug=[UiHandlungsgrundlage(el.text, el.link) for el in schema.bezug],
            hilfetext=schema.hilfetext,
            stichwoerter=schema.stichwort if len(schema.stichwort) > 0 else None,
            ableitungsmodifikationen_repraesentation=schema.ableitungsmodifikationen_repraesentation.to_label(),
            ableitungsmodifikationen_struktur=schema.ableitungsmodifikationen_struktur.to_label(),
        )


@dataclass(slots=True)
class SchemaIndexPage:
    """
    Data for templates/schema_detail.html.jinja
    """

    header: SchemaHeader

    definition: str | None
    bezug: list[UiHandlungsgrundlage]
    gueltig_ab: str | None
    gueltig_bis: str | None
    technische_beschreibung: str | None
    bezeichnung: str | None
    hilfetext: str | None
    stichwoerter: list[Stichwort] | None
    ableitungsmodifikationen_repraesentation: str | None
    ableitungsmodifikationen_struktur: str | None
    versionshinweis: str | None

    available_versions: list[AvailableVersion]
    status_gesetzt_durch: str | None
    status_gesetzt_am: str | None
    veroeffentlicht_am: str | None
    letzte_aenderung: str | None
    last_update: str

    downloads: list[UiShortDownloadLink]
    downloads_tab: SchemaTab
    dokumentsteckbrief: UiBausteinReferenz | None
    relations: UiRelationen

    @staticmethod
    def create(
        schema: FullSchemaOut,
        ui_schema_data: UiSchemaData,
        versions: list[SchemaOut],
        search_query_string: str,
    ) -> SchemaIndexPage:
        downloads = [
            UiShortDownloadLink.from_upload(schema.uploads[0], schema.xdf_version)
        ]
        if schema.json_schema_files:
            downloads.append(
                UiShortDownloadLink.from_json_schema_file(schema.json_schema_files[0])
            )
        if schema.xsd_files:
            downloads.append(UiShortDownloadLink.from_xsd_file(schema.xsd_files[0]))

        return SchemaIndexPage(
            header=SchemaHeader.create(
                schema, active_tab="index", search_query_string=search_query_string
            ),
            last_update=schema.last_update.strftime("%d.%m.%Y"),
            definition=schema.definition,
            technische_beschreibung=schema.beschreibung,
            bezug=ui_schema_data.bezug,
            gueltig_ab=schema.gueltig_ab.strftime("%d.%m.%Y")
            if schema.gueltig_ab is not None
            else None,
            gueltig_bis=schema.gueltig_bis.strftime("%d.%m.%Y")
            if schema.gueltig_bis is not None
            else None,
            bezeichnung=schema.bezeichnung,
            hilfetext=ui_schema_data.hilfetext,
            stichwoerter=ui_schema_data.stichwoerter,
            ableitungsmodifikationen_repraesentation=ui_schema_data.ableitungsmodifikationen_repraesentation,
            ableitungsmodifikationen_struktur=ui_schema_data.ableitungsmodifikationen_struktur,
            status_gesetzt_durch=schema.status_gesetzt_durch,
            status_gesetzt_am=(
                schema.status_gesetzt_am.strftime("%d.%m.%Y")
                if schema.status_gesetzt_am
                else None
            ),
            versionshinweis=schema.versionshinweis,
            veroeffentlicht_am=(
                schema.veroeffentlichungsdatum.strftime("%d.%m.%Y")
                if schema.veroeffentlichungsdatum
                else None
            ),
            letzte_aenderung=schema.letzte_aenderung.strftime("%d.%m.%Y"),
            available_versions=[
                AvailableVersion.from_schema(
                    version, schema.fim_version, search_query_string
                )
                for version in versions
            ][::-1],
            downloads=downloads,
            downloads_tab=SchemaTab(
                id="downloads",
                label="Downloads",
                icon="bi-download",
                active_tab="downloads",
                base_href=f"/schemas/{schema.fim_id}/{schema.fim_version}",
                search_query_string=search_query_string,
            ),
            dokumentsteckbrief=UiBausteinReferenz(
                id=schema.steckbrief_id,
                href=f"/document-profiles/{schema.steckbrief_id}/latest",
                label=schema.steckbrief_name,
            )
            if schema.steckbrief_id is not None and schema.steckbrief_name is not None
            else None,
            relations=UiRelationen.create(schema.relation, "schema"),
        )


@dataclass(slots=True)
class SchemaStructurePage:
    """
    Data for templates/schema_structure.html.jinja
    """

    header: SchemaHeader
    tree: SchemaTree

    @staticmethod
    def create(schema: FullSchemaOut, search_query_string: str) -> SchemaStructurePage:
        return SchemaStructurePage(
            header=SchemaHeader.create(
                schema, active_tab="structure", search_query_string=search_query_string
            ),
            tree=SchemaTree.create(schema),
        )


@dataclass(slots=True)
class SchemaQualityReportPage:
    """
    Data for templates/schema_quality_report.html.jinja
    """

    header: SchemaHeader
    quality_report: QualityReportView

    @staticmethod
    def create(
        schema: FullSchemaOut, quality_report: QualityReport, search_query_string: str
    ) -> SchemaQualityReportPage:
        element_mapping: dict[
            xdf2.Identifier | xdf3.Identifier,
            tuple[str, xdf3.FreigabeStatus | None],
        ] = {
            (
                xdf2.Identifier(
                    id=element.fim_id,
                    version=xdf2.parse_version(element.fim_version),
                )
                if schema.xdf_version == XdfVersion.XDF2
                else xdf3.Identifier(
                    id=element.fim_id,
                    version=xdf3.parse_version(element.fim_version),
                )
            ): (element.name, element.freigabe_status)
            for element in [*schema.datenfelder, *schema.datenfeldgruppen]
        }

        return SchemaQualityReportPage(
            header=SchemaHeader.create(
                schema, active_tab="report", search_query_string=search_query_string
            ),
            quality_report=QualityReportView.from_quality_report(
                schema_fim_id=schema.fim_id,
                schema_fim_version=schema.fim_version,
                schema_name=schema.name,
                schema_freigabe_status=schema.freigabe_status,
                quality_report=quality_report,
                element_mapping=element_mapping,
            ),
        )


@dataclass(slots=True)
class UiCodeListLink:
    label: str
    href: str | None

    @staticmethod
    def from_code_list(code_list: CodeListOut) -> UiCodeListLink:
        return UiCodeListLink(
            label=code_list.genericode_canonical_version_uri,
            href=code_list.url,
        )


@dataclass(slots=True)
class UiResourceLink:
    label: str
    href: str

    @staticmethod
    def from_upload(file: SchemaUpload, xdf_version: XdfVersion) -> UiResourceLink:
        return UiResourceLink(
            label=f"xDatenfelder-{xdf_version.value}-Datei vom {file.upload_time.strftime('%d.%m.%Y')}",
            href=file.url,
        )

    @staticmethod
    def from_json_schema_file(file: JsonSchemaFile) -> UiResourceLink:
        return UiResourceLink(
            label="JSON Schema-Datei vom " + file.upload_time.strftime("%d.%m.%Y"),
            href=file.url,
        )

    @staticmethod
    def from_xsd_file(file: XsdFile) -> UiResourceLink:
        return UiResourceLink(
            label="XSD-Datei vom " + file.upload_time.strftime("%d.%m.%Y"),
            href=file.url,
        )


@dataclass(slots=True)
class SchemaDownloadsPage:
    """
    Data for templates/schema_downloads.html.jinja
    """

    header: SchemaHeader

    xdf_uploads: list[UiResourceLink]
    json_schema_files: list[UiResourceLink]
    xsd_files: list[UiResourceLink]

    codelists: list[UiCodeListLink]

    @staticmethod
    def create(schema: FullSchemaOut, search_query_string: str) -> SchemaDownloadsPage:
        xdf_uploads = [
            UiResourceLink.from_upload(upload, schema.xdf_version)
            for upload in schema.uploads
        ]

        json_schema_files = [
            UiResourceLink.from_json_schema_file(file)
            for file in schema.json_schema_files
        ]

        xsd_files = [UiResourceLink.from_xsd_file(file) for file in schema.xsd_files]

        return SchemaDownloadsPage(
            header=SchemaHeader.create(
                schema, active_tab="downloads", search_query_string=search_query_string
            ),
            xdf_uploads=xdf_uploads,
            json_schema_files=json_schema_files,
            xsd_files=xsd_files,
            # Only include the code lists from the latest download, as we will have only one download in the future anyways.
            codelists=[
                UiCodeListLink.from_code_list(code_list)
                for code_list in schema.uploads[0].code_lists
            ],
        )


def create_schema_router(
    get_service: Callable[[], AsyncGenerator[Service, Any]],
    render: RenderFunction,
) -> APIRouter:
    router = APIRouter()

    def _schema_not_found(request: Request, fim_id: str, fim_version: str):
        return render(
            request,
            "404.html.jinja",
            {
                "message": f"Datenschema {fim_id} v{fim_version} konnte nicht gefunden werden.",
            },
            status_code=status.HTTP_404_NOT_FOUND,
        )

    @router.get(
        "/{fim_id}/{fim_version}",
        response_class=HTMLResponse,
    )
    async def _get_schema_details(  # type: ignore [reportUnusedFunction]
        request: Request,
        fim_id: str,
        fim_version: str,
        service: Service = Depends(get_service),
    ):
        schema = await service.get_schema(fim_id=fim_id, fim_version=fim_version)
        if schema is None:
            return _schema_not_found(request, fim_id, fim_version)
        schema_message = await service.export_xdf_schema_message(fim_id, fim_version)
        assert schema_message is not None

        # NOTE(Felix): This is not optimal, as we only need the actual schema here.
        # All of the children are not needed, but the full schema_message is still loaded here.
        # If displaying the schema becomes a bottleneck, we should consider changing this.
        if isinstance(schema_message, xdf2.SchemaMessage):
            ui_schema_data = UiSchemaData.from_xdf2_schema(schema_message.schema)
        else:
            ui_schema_data = UiSchemaData.from_xdf3_schema(schema_message.schema)

        versions = await service.get_schema_versions(schema.fim_id)

        search_query_string = create_clean_query_string(request, Resource.SCHEMA)

        return render(
            request,
            "schema_detail.html.jinja",
            {
                "titleTag": f"{schema.fim_id} v{schema.fim_version} • Datenschemata • FIM-Portal",
                "page": SchemaIndexPage.create(
                    schema, ui_schema_data, versions, search_query_string
                ),
            },
        )

    @router.get(
        "/{fim_id}/{fim_version}/structure",
        response_class=HTMLResponse,
    )
    async def _get_schema_structure(  # type: ignore [reportUnusedFunction]
        request: Request,
        fim_id: str,
        fim_version: str,
        service: Service = Depends(get_service),
    ):
        schema = await service.get_schema(fim_id=fim_id, fim_version=fim_version)
        if schema is None:
            return _schema_not_found(request, fim_id, fim_version)

        search_query_string = create_clean_query_string(request, Resource.SCHEMA)

        return render(
            request,
            "schema_structure.html.jinja",
            {
                "titleTag": f"{schema.fim_id} v{schema.fim_version} • Datenschemata • FIM-Portal",
                "page": SchemaStructurePage.create(
                    schema,
                    search_query_string=search_query_string,
                ),
            },
        )

    @router.get(
        "/{fim_id}/{fim_version}/report",
        response_class=HTMLResponse,
    )
    async def _get_schema_quality_report(  # type: ignore [reportUnusedFunction]
        request: Request,
        fim_id: str,
        fim_version: str,
        service: Service = Depends(get_service),
    ):
        schema = await service.get_schema(fim_id=fim_id, fim_version=fim_version)
        if schema is None:
            return _schema_not_found(request, fim_id, fim_version)

        quality_report = await service.get_schema_quality_report(
            schema.fim_id, schema.fim_version
        )
        assert quality_report is not None

        search_query_string = create_clean_query_string(request, Resource.SCHEMA)

        return render(
            request,
            "schema_quality_report.html.jinja",
            {
                "titleTag": f"{schema.fim_id} v{schema.fim_version} • Datenschemata • FIM-Portal",
                "page": SchemaQualityReportPage.create(
                    schema, quality_report, search_query_string
                ),
            },
        )

    @router.get(
        "/{fim_id}/{fim_version}/downloads",
        response_class=HTMLResponse,
    )
    async def _get_schema_downloads(  # type: ignore [reportUnusedFunction]
        request: Request,
        fim_id: str,
        fim_version: str,
        service: Service = Depends(get_service),
    ):
        schema = await service.get_schema(fim_id=fim_id, fim_version=fim_version)
        if schema is None:
            return _schema_not_found(request, fim_id, fim_version)

        search_query_string = create_clean_query_string(request, Resource.SCHEMA)

        return render(
            request,
            "schema_downloads.html.jinja",
            {
                "titleTag": f"{schema.fim_id} v{schema.fim_version} • Datenschemata • FIM-Portal",
                "page": SchemaDownloadsPage.create(schema, search_query_string),
            },
        )

    return router
