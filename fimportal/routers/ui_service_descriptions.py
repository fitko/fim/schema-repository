from __future__ import annotations

from dataclasses import dataclass
from typing import Annotated, Any, AsyncGenerator, Callable, Literal

from fastapi import APIRouter, Depends, Query, Request, status
from fastapi.responses import HTMLResponse

from fimportal.helpers import RenderFunction
from fimportal.models.xzufi import (
    FullLeistungsbeschreibungOut,
    XzufiSource,
    get_leistungs_title,
)
from fimportal.routers.common import (
    Resource,
    UiBausteinReferenz,
    UiHeaderTab,
    create_clean_query_string,
)
from fimportal.routers.ui_xzufi import (
    REDAKTION_ID_TO_ICON,
    Metadata,
    SidebarNavigationItem,
    SteckbriefInfo,
    Translation,
    UiModul,
)
from fimportal.service import Service
from fimportal.xzufi.common import (
    REDAKTION_ID_TO_LABEL,
    LanguageCode,
    LeistungsId,
    ModulTextTyp,
    RedaktionsId,
)
from fimportal.xzufi.leistung import (
    Leistung,
    parse_leistung,
)

LeistungenTabId = Literal["index", "downloads"]
LeistungenTab = UiHeaderTab[LeistungenTabId]


@dataclass(slots=True)
class LeistungHeader:
    title: str
    leistungsschluessel: str | None
    typisierung: str | None
    redaktion_label: str
    redaktion_icon: str | None

    base_href: str
    back_url: str
    active_tab: LeistungenTabId

    @staticmethod
    def create(
        leistung: Leistung,
        redaktion_id: str,
        leistungsschluessel: list[str],
        active_tab: LeistungenTabId,
        german: LanguageCode,
        search_query_string: str,
    ) -> LeistungHeader:
        if len(leistungsschluessel) > 0:
            _leistungsschluessel = ", ".join(leistungsschluessel)
        else:
            _leistungsschluessel = None

        if len(leistung.typisierung) > 0:
            typisierung = ", ".join([f"Typ {i.value}" for i in leistung.typisierung])
        else:
            typisierung = None

        return LeistungHeader(
            title=get_leistungs_title(leistung, german),
            leistungsschluessel=_leistungsschluessel,
            typisierung=typisierung,
            redaktion_label=REDAKTION_ID_TO_LABEL.get(redaktion_id, redaktion_id),
            redaktion_icon=REDAKTION_ID_TO_ICON.get(redaktion_id),
            base_href=f"/xzufi-services/{redaktion_id}/{leistung.id.value}",
            back_url=f"/search{search_query_string}",
            active_tab=active_tab,
        )


@dataclass(slots=True)
class ServiceIndexPage:
    header: LeistungHeader
    sidebar_navigation: list[SidebarNavigationItem]

    translations: list[Translation]
    steckbrief_referenz: UiBausteinReferenz | None
    download_xzufi_href: str
    metadata: Metadata
    steckbrief: SteckbriefInfo
    module: list[UiModul]

    @staticmethod
    def from_leistung(
        leistung: Leistung,
        leistung_out: FullLeistungsbeschreibungOut,
        language_code: str | None,
        source: XzufiSource,
        search_query_string: str,
    ) -> ServiceIndexPage:
        redaktion_id = leistung.unwrap_redaktions_id()

        native_language_code, active_language_code, translations = (
            Translation.extract_translations(
                leistung, language_code, search_query_string
            )
        )

        leistungsschluessel = leistung.get_leistungsschluessel()
        if len(leistungsschluessel) > 0 and leistung_out.steckbrief_name is not None:
            steckbrief_referenz = UiBausteinReferenz(
                href=f"/services/{leistungsschluessel[0]}",
                label=leistung_out.steckbrief_name,
                id=leistungsschluessel[0],
            )
        else:
            steckbrief_referenz = None

        header = LeistungHeader.create(
            leistung,
            redaktion_id=redaktion_id,
            leistungsschluessel=leistungsschluessel,
            active_tab="index",
            german=native_language_code,
            search_query_string=search_query_string,
        )

        steckbrief = SteckbriefInfo.create(
            leistung,
            header.leistungsschluessel,
            header.typisierung,
            active_language_code,
        )

        module = [
            UiModul.create_textmodul(
                leistung, ModulTextTyp.TEASER, True, active_language_code
            ),
            UiModul.create_textmodul(
                leistung, ModulTextTyp.VOLLTEXT, True, active_language_code
            ),
            UiModul.create_textmodul(
                leistung,
                ModulTextTyp.ERFORDERLICHE_UNTERLAGEN,
                True,
                active_language_code,
            ),
            UiModul.create_textmodul(
                leistung, ModulTextTyp.VORAUSSETZUNGEN, True, active_language_code
            ),
            UiModul.create_kostenmodul(leistung, True, active_language_code),
            UiModul.create_textmodul(
                leistung, ModulTextTyp.VERFAHRENSABLAUF, False, active_language_code
            ),
            UiModul.create_bearbeitungsdauer(leistung, True, active_language_code),
            UiModul.create_fristmodul(leistung, True, active_language_code),
            UiModul.create_textmodul(
                leistung,
                ModulTextTyp.WEITERFUEHRENDE_INFORMATIONEN,
                False,
                active_language_code,
            ),
            UiModul.create_textmodul(
                leistung, ModulTextTyp.HINWEISE, True, active_language_code
            ),
            UiModul.create_textmodul(
                leistung, ModulTextTyp.RECHTSBEHELF, True, active_language_code
            ),
            UiModul.create_textmodul(
                leistung, ModulTextTyp.KURZTEXT, True, active_language_code
            ),
            UiModul.create_textmodul(
                leistung, ModulTextTyp.ANSPRECHPUNKT, True, active_language_code
            ),
            UiModul.create_textmodul(
                leistung, ModulTextTyp.ZUSTAENDIGE_STELLE, True, active_language_code
            ),
            UiModul.create_textmodul(
                leistung, ModulTextTyp.FORMUALRE, True, active_language_code
            ),
            UiModul.create_ursprungsportal(leistung, True, active_language_code),
        ]

        sidebar_nav_items = [
            SidebarNavigationItem.from_ui_modul(steckbrief.rechtsgrundlagen),
        ]
        for modul in module:
            sidebar_nav_items.append(SidebarNavigationItem.from_ui_modul(modul))

        return ServiceIndexPage(
            header=header,
            sidebar_navigation=sidebar_nav_items,
            translations=translations,
            steckbrief_referenz=steckbrief_referenz,
            download_xzufi_href=f"/api/v0/xzufi-services/{redaktion_id}/{leistung.id.value}/xzufi",
            metadata=Metadata.create(
                leistung,
                header.redaktion_label,
                source,
            ),
            steckbrief=steckbrief,
            module=module,
        )


def create_service_description_router(
    get_service: Callable[[], AsyncGenerator[Service, Any]], render: RenderFunction
):
    router = APIRouter()

    def _service_not_found(request: Request, leistung_id: str):
        return render(
            request,
            "404.html.jinja",
            {
                "message": f"Leistung {leistung_id} konnte nicht gefunden werden.",
            },
            status_code=status.HTTP_404_NOT_FOUND,
        )

    @router.get("/{redaktion_id}/{leistung_id}", response_class=HTMLResponse)
    async def get_service_description_details(  # type: ignore [reportUnusedFunction]
        request: Request,
        redaktion_id: RedaktionsId,
        leistung_id: LeistungsId,
        language_code: Annotated[str | None, Query()] = None,
        service: Service = Depends(get_service),
    ):
        content_info = await service.get_leistungsbeschreibung_content_info(
            (redaktion_id, leistung_id)
        )
        if content_info is None:
            return _service_not_found(request, leistung_id=leistung_id)

        # This should never fail, as we only import leistungen which we can parse.
        # Therefore, a potential parsing error is not caught here.
        # If an error occurs, this is a clear bug, which results a 500 return value and an
        # automatic error log entry.
        leistung = parse_leistung(content_info.content)
        leistung_out = await service.get_leistungsbeschreibung(
            (redaktion_id, leistung_id)
        )
        assert leistung_out is not None

        search_query_string = create_clean_query_string(
            request, Resource.SERVICE_DESCRIPTION
        )

        service_index_page = ServiceIndexPage.from_leistung(
            leistung,
            leistung_out,
            language_code,
            content_info.source,
            search_query_string,
        )

        return render(
            request,
            "service_description_detail.html.jinja",
            {
                "titleTag": f"{service_index_page.header.title} • Leistungen • FIM Portal",
                "page": service_index_page,
            },
        )

    return router
