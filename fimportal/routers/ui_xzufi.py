"""
Common data structures to display XZuFi data
in ./templates/components/xzufi_modules.html.jinja.
"""

from __future__ import annotations

import logging
from dataclasses import dataclass
from typing import Literal

from fimportal.common import RawCode
from fimportal.helpers import remove_html_tags
from fimportal.models.xzufi import XzufiSource
from fimportal.routers.ui_help import TEXT_MODUL_TO_GLOSSARY_LINK
from fimportal.xzufi.common import (
    ALT_GERMAN,
    ENGLISH,
    GERMAN,
    LanguageCode,
    LeistungRedaktionId,
    ModulTextTyp,
    StringLocalized,
    get_translation,
)
from fimportal.xzufi.labels import (
    LEISTUNGS_GRUPPIERUNG_TO_LABEL,
    PV_LAGEN_TO_LABEL,
    SDG_INFORMATIONSBEREICH_TO_LABEL,
    VERRICHTUNGS_KENNUNG_TO_LABEL,
)
from fimportal.xzufi.leistung import (
    Amount,
    Bearbeitungsdauermodul,
    FristDauer,
    FristMitTyp,
    FristOhneTyp,
    FristStichtagWert,
    Fristmodul,
    Kosten,
    KostenFix,
    KostenFrei,
    Kostenmodul,
    Leistung,
    LeistungsgruppierungAuswahl,
    LeistungsstrukturObjekt,
    LeistungsstrukturObjektMitVerrichtung,
    LeistungsstrukturObjektMitVerrichtungUndDetail,
    LeistungsverrichtungAuswahl,
    MonatTag,
    Textmodul,
    UrsprungsportalModul,
)


logger = logging.getLogger(__name__)


def get_icon_url_for_redaktion(redaktion: LeistungRedaktionId) -> str:
    return f"/img/{_get_icon_name_for_redaktion(redaktion)}.svg"


def _get_icon_name_for_redaktion(redaktion: LeistungRedaktionId) -> str:
    match redaktion:
        case LeistungRedaktionId.SH:
            return "schleswig-holstein"
        case LeistungRedaktionId.HH:
            return "hamburg"
        case LeistungRedaktionId.NI:
            return "niedersachsen"
        case LeistungRedaktionId.HB:
            return "bremen"
        case LeistungRedaktionId.NW:
            return "nordrhein-westfalen"
        case LeistungRedaktionId.HE:
            return "hessen"
        case LeistungRedaktionId.RP:
            return "rheinland-pfalz"
        case LeistungRedaktionId.BW:
            return "baden-wuerttemberg"
        case LeistungRedaktionId.BY:
            return "bayern"
        case LeistungRedaktionId.SL:
            return "saarland"
        case LeistungRedaktionId.BE:
            return "berlin"
        case LeistungRedaktionId.BB:
            return "brandenburg"
        case LeistungRedaktionId.MV:
            return "mecklenburg-vorpommern"
        case LeistungRedaktionId.SN:
            return "sachsen"
        case LeistungRedaktionId.ST:
            return "sachsen-anhalt"
        case LeistungRedaktionId.TH:
            return "thueringen"
        case LeistungRedaktionId.BUND:
            return "bund"
        case LeistungRedaktionId.LEIKA:
            return "bund"


REDAKTION_ID_TO_ICON = {
    redaktion_id.value: get_icon_url_for_redaktion(redaktion_id)
    for redaktion_id in LeistungRedaktionId
}


@dataclass(slots=True)
class Metadata:
    leistungsadressat: str | None
    redaktion_label: str
    datenquelle: str
    erstellt_am: str | None
    # erstellt_durch: str | None
    geaendert_am: str | None
    # geaendert_durch: str | None
    version: str | None

    @staticmethod
    def create(
        leistung: Leistung,
        redaktion_label: str,
        source: XzufiSource,
    ) -> Metadata:
        if len(leistung.leistungsadressat) > 0:
            leistungsadressat = ", ".join(
                [i.to_label() for i in leistung.leistungsadressat]
            )
        else:
            leistungsadressat = None

        if leistung.versionsinformation:
            if leistung.versionsinformation.erstellt_datum_zeit:
                erstellt_am = leistung.versionsinformation.erstellt_datum_zeit.strftime(
                    "%d.%m.%Y"
                )
            else:
                erstellt_am = None

            if leistung.versionsinformation.geaendert_datum_zeit:
                geaendert_am = (
                    leistung.versionsinformation.geaendert_datum_zeit.strftime(
                        "%d.%m.%Y"
                    )
                )
            else:
                geaendert_am = None

            # erstellt_durch = leistung.versionsinformation.erstellt_durch
            # geaendert_durch = leistung.versionsinformation.geaendert_durch
            version = leistung.versionsinformation.version
        else:
            erstellt_am = None
            # erstellt_durch = None
            geaendert_am = None
            # geaendert_durch = None
            version = None

        return Metadata(
            leistungsadressat=leistungsadressat,
            redaktion_label=redaktion_label,
            datenquelle=source.to_label(),
            erstellt_am=erstellt_am,
            # erstellt_durch=erstellt_durch,
            geaendert_am=geaendert_am,
            # geaendert_durch=geaendert_durch,
            version=version,
        )


@dataclass(slots=True)
class LeistungsstrukturAttribute:
    """
    Hold information about the individual parts of the Leistungsstruktur.
    """

    title: str
    value: str
    help_link: str | None

    @staticmethod
    def from_gruppierung(
        leistungsgruppierung: LeistungsgruppierungAuswahl | None,
        language_code: LanguageCode,
    ) -> LeistungsstrukturAttribute:
        if isinstance(leistungsgruppierung, RawCode):
            return LeistungsstrukturAttribute(
                title="Leistungsgruppierung",
                value=leistungsgruppierung.get_title(LEISTUNGS_GRUPPIERUNG_TO_LABEL),
                help_link="https://docs.fitko.de/fim/docs/glossar/#leistungsgruppierung",
            )

        return LeistungsstrukturAttribute(
            title="Leistungsgruppierung",
            value=leistungsgruppierung.get_title(language_code)
            if leistungsgruppierung is not None
            else "-",
            help_link="https://docs.fitko.de/fim/docs/glossar/#leistungsgruppierung",
        )

    @staticmethod
    def from_verrichtung(
        verrichtungskennung: LeistungsverrichtungAuswahl, language_code: LanguageCode
    ) -> LeistungsstrukturAttribute:
        if isinstance(verrichtungskennung, RawCode):
            return LeistungsstrukturAttribute(
                title="Verrichtungskennung",
                value=verrichtungskennung.get_title(VERRICHTUNGS_KENNUNG_TO_LABEL),
                help_link="https://docs.fitko.de/fim/docs/glossar/#verrichtungskennung",
            )

        return LeistungsstrukturAttribute(
            title="Verrichtungskennung",
            value=verrichtungskennung.get_title(language_code),
            help_link="https://docs.fitko.de/fim/docs/glossar/#verrichtungskennung",
        )

    @staticmethod
    def from_verrichtungsdetail(
        verrichtungsdetail: list[StringLocalized], language_code: LanguageCode
    ) -> LeistungsstrukturAttribute:
        assert len(verrichtungsdetail) > 0

        for content in verrichtungsdetail:
            if content.language_code == language_code:
                return LeistungsstrukturAttribute(
                    title="Verrichtungsdetail",
                    value=content.value,
                    help_link="https://docs.fitko.de/fim/docs/glossar/#verrichtungsdetail",
                )

        return LeistungsstrukturAttribute(
            title="Verrichtungsdetail",
            value=verrichtungsdetail[0].value,
            help_link="https://docs.fitko.de/fim/docs/glossar/#verrichtungsdetail",
        )


@dataclass(slots=True)
class UiLeistungsstruktur:
    """
    All data necessary to render the information about the Leistungsstruktur
    in the frontend.

    This is used both for "Leistungen" and "XZuFi-Leistungen".
    """

    leistungstyp: str
    attribute: list[LeistungsstrukturAttribute]

    @staticmethod
    def create(
        leistung: Leistung, language_code: LanguageCode
    ) -> UiLeistungsstruktur | None:
        if leistung.struktur is None:
            return None

        match leistung.struktur:
            case LeistungsstrukturObjekt(leistungsgruppierung=gruppierung):
                leistungstyp = "Leistungsobjekt"
                attribute = [
                    LeistungsstrukturAttribute.from_gruppierung(
                        gruppierung, language_code=language_code
                    )
                ]
            case LeistungsstrukturObjektMitVerrichtung(
                leistungsgruppierung=gruppierung, verrichtung=verrichtung
            ):
                leistungstyp = "Leistungsobjekt mit Verrichtung"
                attribute = [
                    LeistungsstrukturAttribute.from_gruppierung(
                        gruppierung, language_code=language_code
                    ),
                    LeistungsstrukturAttribute.from_verrichtung(
                        verrichtung, language_code=language_code
                    ),
                ]
            case LeistungsstrukturObjektMitVerrichtungUndDetail(
                leistungsgruppierung=gruppierung,
                verrichtung=verrichtung,
                verrichtungsdetail=verrichtungsdetail,
            ):
                leistungstyp = "Leistungsobjekt mit Verrichtung und Detail"
                attribute = [
                    LeistungsstrukturAttribute.from_gruppierung(
                        gruppierung, language_code=language_code
                    ),
                    LeistungsstrukturAttribute.from_verrichtung(
                        verrichtung, language_code=language_code
                    ),
                    LeistungsstrukturAttribute.from_verrichtungsdetail(
                        verrichtungsdetail, language_code=language_code
                    ),
                ]

        return UiLeistungsstruktur(leistungstyp, attribute)


@dataclass(slots=True)
class SteckbriefInfo:
    """
    This dataclass is used both for holding the entire data of a Steckbrief
    as well as the same attributes but for a Leistungsbeschreibung.
    The data is displayed by the macro `steckbrief` in `/components/xzufi_module.html.jinja`
    """

    leistungsschluessel: str | None
    typisierung: str | None
    leistungsbezeichnung: str | None
    leistungsbezeichnung_II: str | None
    begriffe_im_kontext: str | None
    leistungsstruktur: UiLeistungsstruktur | None
    sdg_informationsbereiche: list[str]
    lagen_portalverbund: list[str]
    einheitlicher_ansprechpartner: bool | None
    fachlich_freigegeben_am: str | None
    fachlich_freigegeben_durch: str | None
    rechtsgrundlagen: UiModul

    @staticmethod
    def create(
        leistung: Leistung,
        leistungsschluessel: str | None,
        typisierung: str | None,
        language_code: LanguageCode,
    ) -> SteckbriefInfo:
        bezeichnung_modul = leistung.get_text_modul(ModulTextTyp.LEISTUNGSBEZEICHNUNG)
        if bezeichnung_modul is not None:
            bezeichnung = bezeichnung_modul.get_inhalt(language_code)
        else:
            bezeichnung = None

        bezeichnung_II_modul = leistung.get_text_modul(
            ModulTextTyp.LEISTUNGSBEZEICHNUNG_II
        )
        if bezeichnung_II_modul is not None:
            bezeichnung_2 = bezeichnung_II_modul.get_inhalt(language_code)
        else:
            bezeichnung_2 = None

        begriffe_im_kontext: list[str] = []
        for modul in leistung.modul_begriff_im_kontext:
            for begriff in modul.begriff_im_kontext:
                if begriff.begriff.language_code == language_code:
                    label = begriff.begriff.value

                    if begriff.typ is not None:
                        label += f" ({begriff.typ.to_label()})"

                    begriffe_im_kontext.append(label)

        if len(begriffe_im_kontext) > 0:
            begriffe_im_kontext_value = ", ".join(begriffe_im_kontext)
        else:
            begriffe_im_kontext_value = None

        sdg_informationsbereiche = [
            f"{SDG_INFORMATIONSBEREICH_TO_LABEL.get(i, 'Unbekannt')} ({i})"
            for i in leistung.informationsbereich_sdg
        ]

        lagen_portalverbund = [
            f"{PV_LAGEN_TO_LABEL.get(i, 'Unbekannt')} ({i})"
            for i in leistung.get_pv_lagen()
        ]

        if leistung.modul_fachliche_freigabe:
            fachlich_freigegeben_am = (
                leistung.modul_fachliche_freigabe.fachlich_freigegeben_am.strftime(
                    "%d.%m.%Y"
                )
                if leistung.modul_fachliche_freigabe.fachlich_freigegeben_am
                else None
            )
            fachlich_freigegeben_durch = get_translation(
                leistung.modul_fachliche_freigabe.fachlich_freigegeben_durch,
                language_code,
            )
            if fachlich_freigegeben_durch is not None:
                fachlich_freigegeben_durch = remove_html_tags(
                    fachlich_freigegeben_durch
                )
        else:
            fachlich_freigegeben_am = None
            fachlich_freigegeben_durch = None

        return SteckbriefInfo(
            leistungsschluessel=leistungsschluessel,
            typisierung=typisierung,
            leistungsbezeichnung=bezeichnung,
            leistungsbezeichnung_II=bezeichnung_2,
            begriffe_im_kontext=begriffe_im_kontext_value,
            leistungsstruktur=UiLeistungsstruktur.create(leistung, language_code),
            sdg_informationsbereiche=sdg_informationsbereiche,
            einheitlicher_ansprechpartner=leistung.kennzeichen_ea,
            lagen_portalverbund=lagen_portalverbund,
            fachlich_freigegeben_am=fachlich_freigegeben_am,
            fachlich_freigegeben_durch=fachlich_freigegeben_durch,
            rechtsgrundlagen=UiModul.create_textmodul(
                leistung=leistung,
                modul_typ=ModulTextTyp.RECHTSGRUNDLAGEN,
                is_required=True,
                language_code=language_code,
            ),
        )


@dataclass(slots=True)
class SidebarNavigationItem:
    label: str
    href: str
    icon: Literal["error", "empty"] | None = None

    @staticmethod
    def from_ui_modul(modul: UiModul) -> SidebarNavigationItem:
        if modul.content is None:
            if modul.is_required:
                icon = "error"
            else:
                icon = "empty"
        else:
            icon = None

        return SidebarNavigationItem(
            label=modul.title,
            href=f"#{modul.id}",
            icon=icon,
        )


@dataclass(slots=True)
class Translation:
    label: str
    href: str
    active: bool

    @staticmethod
    def extract_translations(
        leistung: Leistung, language_code: str | None, search_query_string: str
    ) -> tuple[LanguageCode, LanguageCode, list[Translation]]:
        native = leistung.get_german_language_code()
        # Every leistung in our database should have a german translation
        assert native is not None

        # Make sure german is the first shown language
        language_codes = set(
            sprachversion.language_code for sprachversion in leistung.sprachversion
        )
        language_codes.remove(native)
        language_codes = [native, *language_codes]

        # Use german as the default language code if no explicit code is provided or
        # the language_code is not supported
        active_language_code = (
            language_code if language_code in language_codes else native
        )

        translations = [
            Translation(
                label=LANGUAGE_CODE_TO_LABEL.get(language_code, language_code),
                href=f"{search_query_string}&language_code={language_code}",
                active=language_code == active_language_code,
            )
            for language_code in language_codes
        ]

        return native, active_language_code, translations


@dataclass(slots=True)
class LeistungsAttribut:
    code: str
    label: str


@dataclass(slots=True)
class UiModulLink:
    href: str
    label: str


@dataclass(slots=True)
class UiTextmodul:
    inhalt: str | None
    links: list[UiModulLink]

    type = "text"

    @staticmethod
    def create(text_modul: Textmodul, language_code: LanguageCode) -> UiTextmodul:
        return UiTextmodul(
            inhalt=text_modul.get_inhalt(language_code),
            links=[
                UiModulLink(
                    href=link.uri,
                    label=link.titel or link.uri,
                )
                for link in text_modul.weiterfuehrender_link
                if link.language_code == language_code or link.language_code is None
            ],
        )


@dataclass(slots=True)
class UiKosten:
    kostentyp: str | None
    kosten_value: str
    kosten_value_details: str | None
    beschreibung: str | None
    link_kostenbildung: str | None
    vorkasse: bool

    @staticmethod
    def create(kosten: Kosten, language_code: LanguageCode) -> UiKosten:
        if isinstance(kosten.typ, str):
            kostentyp = kosten.typ
        else:
            kostentyp = kosten.typ.to_label()

        kosten_value_details = None
        if isinstance(kosten.kostenauswahl, KostenFrei):
            kosten_value = "Es fallen keine Kosten an"
            kosten_value_details = get_translation(
                kosten.kostenauswahl.beschreibung_kostenfreiheit, language_code
            )
        elif isinstance(kosten.kostenauswahl, KostenFix):
            kosten_value = _format_currency(kosten.kostenauswahl.betrag)
        else:
            untergrenze = kosten.kostenauswahl.betrag_untergrenze
            obergrenze = kosten.kostenauswahl.betrag_obergrenze
            if untergrenze is not None and obergrenze is not None:
                kosten_value = (
                    f"{_format_currency(untergrenze)} - {_format_currency(obergrenze)}"
                )
            elif untergrenze is not None:
                kosten_value = f"Mindestens {_format_currency(untergrenze)}"
            elif obergrenze is not None:
                kosten_value = f"Bis zu {_format_currency(obergrenze)}"
            else:
                kosten_value = "Unbekannte Kosten"

            kosten_value_details = get_translation(
                kosten.kostenauswahl.beschreibung_variabilitaet, language_code
            )

        return UiKosten(
            kostentyp=kostentyp,
            kosten_value=kosten_value,
            kosten_value_details=kosten_value_details,
            beschreibung=get_translation(kosten.beschreibung, language_code),
            vorkasse=kosten.vorkasse is True,
            link_kostenbildung=kosten.link_kostenbildung,
        )


def _format_currency(amount: Amount) -> str:
    if amount.currency_code == "EUR":
        symbol = "€"
    else:
        symbol = amount.currency_code

    euros, cents = f"{amount.value:,.2f}".split(".")
    euros = euros.replace(",", ".")

    if cents == "00":
        return f"{euros}{symbol}"
    else:
        return f"{euros},{cents}{symbol}"


@dataclass(slots=True)
class UiKostenmodul:
    kosten: list[UiKosten]
    beschreibung: str | None
    links: list[UiModulLink]

    type = "kosten"

    @staticmethod
    def create(modul: Kostenmodul, language_code: LanguageCode) -> UiKostenmodul:
        kosten = [UiKosten.create(kosten, language_code) for kosten in modul.kosten]
        beschreibung = get_translation(modul.beschreibung, language_code)
        links = [
            UiModulLink(
                href=link.uri,
                label=link.titel or link.uri,
            )
            for link in modul.weiterfuehrender_link
            if link.language_code == language_code or link.language_code is None
        ]

        return UiKostenmodul(
            kosten=kosten,
            beschreibung=beschreibung,
            links=links,
        )


@dataclass(slots=True)
class UiFrist:
    typ: str
    frist_value: str
    beschreibung: str | None

    @staticmethod
    def create(frist: FristMitTyp, language_code: LanguageCode) -> UiFrist:
        if isinstance(frist.typ, str):
            typ = frist.typ
        else:
            typ = frist.typ.to_label()

        auswahl = frist.fristauswahl
        if isinstance(auswahl, FristDauer):
            if auswahl.dauer_bis is None:
                frist_value = f"{auswahl.dauer} {auswahl.einheit.to_label()}"
            else:
                if (
                    auswahl.einheit == auswahl.einheit_bis
                    or auswahl.einheit_bis is None
                ):
                    frist_value = f"{auswahl.dauer} - {auswahl.dauer_bis} {auswahl.einheit.to_label()}"
                else:
                    frist_value = f"{auswahl.dauer} {auswahl.einheit.to_label()} - {auswahl.dauer_bis} {auswahl.einheit_bis.to_label()}"
        else:
            von = auswahl.von
            bis = auswahl.bis
            if von is not None and bis is not None:
                frist_value = f"{_format_stichtag(von)} - {_format_stichtag(bis)}"
            elif von is not None:
                frist_value = f"ab {_format_stichtag(von)}"
            elif bis is not None:
                frist_value = f"bis {_format_stichtag(bis)}"
            else:
                frist_value = "Unbekannte Frist"

        return UiFrist(
            typ=typ,
            frist_value=frist_value,
            beschreibung=get_translation(frist.beschreibung, language_code),
        )


def _format_stichtag(stichtag: FristStichtagWert) -> str:
    if isinstance(stichtag, str):
        return stichtag
    elif isinstance(stichtag, MonatTag):
        return stichtag.value
    else:
        return stichtag.strftime("%d.%m.%Y")


@dataclass(slots=True)
class UiFristmodul:
    frist: list[UiFrist]
    beschreibung: str | None
    links: list[UiModulLink]

    type = "frist"

    @staticmethod
    def create(modul: Fristmodul, language_code: LanguageCode) -> UiFristmodul:
        beschreibung = get_translation(modul.beschreibung, language_code)
        links = [
            UiModulLink(
                href=link.uri,
                label=link.titel or link.uri,
            )
            for link in modul.weiterfuehrender_link
            if link.language_code == language_code or link.language_code is None
        ]

        return UiFristmodul(
            frist=[UiFrist.create(frist, language_code) for frist in modul.frist],
            beschreibung=beschreibung,
            links=links,
        )


@dataclass(slots=True)
class UiUrsprungsportalmodul:
    titel: str
    uri: str

    type = "ursprung"

    @staticmethod
    def create(
        module: list[UrsprungsportalModul], language_code: LanguageCode
    ) -> UiUrsprungsportalmodul | None:
        for modul in module:
            if modul.language_code == language_code:
                return UiUrsprungsportalmodul(
                    titel=modul.titel or modul.uri,
                    uri=modul.uri,
                )

        return None


@dataclass(slots=True)
class UiFristOhneTyp:
    frist_value: str
    beschreibung: str | None

    @staticmethod
    def create(frist: FristOhneTyp, language_code: LanguageCode) -> UiFristOhneTyp:
        auswahl = frist.fristauswahl
        if isinstance(auswahl, FristDauer):
            if auswahl.dauer_bis is None:
                frist_value = f"{auswahl.dauer} {auswahl.einheit.to_label()}"
            else:
                if (
                    auswahl.einheit == auswahl.einheit_bis
                    or auswahl.einheit_bis is None
                ):
                    frist_value = f"{auswahl.dauer} - {auswahl.dauer_bis} {auswahl.einheit.to_label()}"
                else:
                    frist_value = f"{auswahl.dauer} {auswahl.einheit.to_label()} - {auswahl.dauer_bis} {auswahl.einheit_bis.to_label()}"
        else:
            von = auswahl.von
            bis = auswahl.bis
            if von is not None and bis is not None:
                frist_value = f"{_format_stichtag(von)} - {_format_stichtag(bis)}"
            elif von is not None:
                frist_value = f"ab {_format_stichtag(von)}"
            elif bis is not None:
                frist_value = f"bis {_format_stichtag(bis)}"
            else:
                frist_value = "Unbekannte Frist"

        return UiFristOhneTyp(
            frist_value=frist_value,
            beschreibung=get_translation(frist.beschreibung, language_code),
        )


@dataclass(slots=True)
class UiBearbeitungsdauer:
    bearbeitungsdauer: list[UiFristOhneTyp]
    beschreibung: str | None
    links: list[UiModulLink]

    type = "bearbeitungsdauer"

    @staticmethod
    def create(
        modul: Bearbeitungsdauermodul, language_code: LanguageCode
    ) -> UiBearbeitungsdauer:
        return UiBearbeitungsdauer(
            bearbeitungsdauer=[
                UiFristOhneTyp.create(frist, language_code)
                for frist in modul.bearbeitungsdauer
            ],
            beschreibung=get_translation(modul.beschreibung, language_code),
            links=[
                UiModulLink(
                    href=link.uri,
                    label=link.titel or link.uri,
                )
                for link in modul.weiterfuehrender_link
                if link.language_code == language_code or link.language_code is None
            ],
        )


@dataclass(slots=True)
class UiModul:
    id: str
    title: str
    glossary_link: str | None
    is_required: bool
    content: (
        UiTextmodul
        | UiKostenmodul
        | UiFristmodul
        | UiUrsprungsportalmodul
        | UiBearbeitungsdauer
        | None
    )

    @staticmethod
    def create_textmodul(
        leistung: Leistung,
        modul_typ: ModulTextTyp,
        is_required: bool,
        language_code: LanguageCode,
    ) -> UiModul:
        modul = leistung.get_text_modul(modul_typ)
        glossary_link = TEXT_MODUL_TO_GLOSSARY_LINK.get(modul_typ)

        if modul is None:
            return UiModul(
                id=modul_typ.value,
                title=modul_typ.to_label(),
                glossary_link=glossary_link,
                is_required=is_required,
                content=None,
            )
        else:
            return UiModul(
                id=modul_typ.value,
                title=modul_typ.to_label(),
                glossary_link=glossary_link,
                is_required=is_required,
                content=UiTextmodul.create(modul, language_code),
            )

    @staticmethod
    def create_kostenmodul(
        leistung: Leistung,
        is_required: bool,
        language_code: LanguageCode,
    ) -> UiModul:
        modul = leistung.modul_kosten

        if modul is None:
            return UiModul(
                id="kosten",
                title="Kosten",
                glossary_link="https://docs.fitko.de/fim/docs/glossar/#kosten",
                is_required=is_required,
                content=None,
            )
        else:
            return UiModul(
                id="kosten",
                title="Kosten",
                glossary_link="https://docs.fitko.de/fim/docs/glossar/#kosten",
                is_required=is_required,
                content=UiKostenmodul.create(
                    modul,
                    language_code,
                ),
            )

    @staticmethod
    def create_fristmodul(
        leistung: Leistung, is_required: bool, language_code: LanguageCode
    ) -> UiModul:
        modul = leistung.modul_frist

        if modul is None:
            return UiModul(
                id="frist",
                title="Frist",
                glossary_link="https://docs.fitko.de/fim/docs/glossar/#frist",
                is_required=is_required,
                content=None,
            )
        else:
            return UiModul(
                id="frist",
                title="Frist",
                glossary_link="https://docs.fitko.de/fim/docs/glossar/#frist",
                is_required=is_required,
                content=UiFristmodul.create(modul, language_code),
            )

    @staticmethod
    def create_ursprungsportal(
        leistung: Leistung, is_required: bool, language_code: LanguageCode
    ) -> UiModul:
        return UiModul(
            id="ursprungsportal",
            title="Ursprungsportal",
            glossary_link="https://docs.fitko.de/fim/docs/glossar/#ursprungsportal",
            is_required=is_required,
            content=UiUrsprungsportalmodul.create(
                leistung.modul_ursprungsportal, language_code
            ),
        )

    @staticmethod
    def create_bearbeitungsdauer(
        leistung: Leistung, is_required: bool, language_code: LanguageCode
    ) -> UiModul:
        modul = leistung.modul_bearbeitungsdauer

        if modul is None:
            return UiModul(
                id="bearbeitungsdauer",
                title="Bearbeitungsdauer",
                glossary_link="https://docs.fitko.de/fim/docs/glossar/#bearbeitungsdauer",
                is_required=is_required,
                content=None,
            )
        else:
            return UiModul(
                id="bearbeitungsdauer",
                title="Bearbeitungsdauer",
                glossary_link="https://docs.fitko.de/fim/docs/glossar/#bearbeitungsdauer",
                is_required=is_required,
                content=UiBearbeitungsdauer.create(modul, language_code),
            )


LANGUAGE_CODE_TO_LABEL: dict[LanguageCode, str] = {
    GERMAN: "Deutsch",
    ALT_GERMAN: "Deutsch",
    ENGLISH: "Englisch",
}
