"""
The router defining all routes for the additional tools provided by the app.

This for now includes the conversion to json schema via a file upload.
"""

from datetime import timedelta
import csv
import email.utils
import urllib.parse
import zipfile
from io import BytesIO, StringIO
from typing import Annotated, Any, AsyncGenerator, Callable

import orjson
from fastapi import APIRouter, Depends, File, HTTPException, Request, Response, status
from fastapi.responses import ORJSONResponse

from fimportal import genericode, json_schema, xrepository, xsd, xzufi
from fimportal.errors import InvalidFimIdException
from fimportal.helpers import RenderFunction
from fimportal.models.csv_export import CSVSearchResult
from fimportal.models.ui_prototype import QualityReportView
from fimportal.models.xdf3 import (
    JsonLegend,
)
from fimportal.routers.parameters import ErrorMessage, SchemaId
from fimportal.routers.ui_search import Resource, UiFilters, get_ui_filters
from fimportal.rss import (
    create_process_rss_feed_item,
    create_rss_xml,
    create_schema_rss_feed_item,
    create_service_rss_feed_item,
)
from fimportal.service import Service
from fimportal.xdatenfelder import (
    reports,
    xdf2,
    xdf2_to_xdf3,
    xdf3,
    xdf3_to_xdf2,
)
from fimportal.xdatenfelder.common import XdfException
from .immutable import XmlFileResponse


class ZipFileResponse(Response):
    media_type = "application/zip"

    def __init__(
        self,
        content: bytes,
        filename: str,
        headers: dict[str, str] | None = None,
        status_code: int = 200,
    ) -> None:
        super().__init__(
            content=content,
            status_code=status_code,
            headers=headers,
            media_type="application/zip",
            background=None,
        )
        self.headers["Content-Disposition"] = f'attachment; filename="{filename}"'

    def render(self, content: bytes) -> bytes:
        return content


def create_router(
    xrepository_service: xrepository.Service,
    baseurl: str,
    get_service: Callable[[], AsyncGenerator[Service, Any]],
    render: RenderFunction,
) -> APIRouter:
    router = APIRouter()

    @router.get("/xdf2-to-xdf3-converter", include_in_schema=False)
    async def get_xdf2_to_xdf3_converter(  # type: ignore [reportUnusedFunction]
        request: Request,
    ):
        return render(
            request,
            "converter/xdf2-to-xdf3.html.jinja",
            {
                "metaDescription": "XDF2 zu XDF3 Konverter",
                "titleTag": "XDF2 zu XDF3 Konverter",
            },
        )

    @router.post(
        "/xdf2-to-xdf3-converter",
        name="Convert an xdf2 schema to xdf3",
        response_class=XmlFileResponse,
        responses={
            400: {"model": ErrorMessage, "description": "Conversion Error"},
        },
        openapi_extra={"security": [{}]},
    )
    async def convert_xdf2_to_xdf3(  # type: ignore [reportUnusedFunction]
        # NOTE: This must not be called `schema`, as this is a reserved keyword in pydantic 2.
        schema_file: Annotated[
            bytes,
            File(
                title="Schema",
                alias="schema",
                # NOTE: `validation_alias` must be set additionally, as fastapi has a bug where the alias
                # is not respected when creating the OpenAPI Docs.
                validation_alias="schema",
                description="The xdf2 `Datenschema` (must be `application/xml`).",
                media_type="application/xml",
            ),
        ],
        code_lists: Annotated[
            list[bytes],
            File(
                title="Codelists",
                description="One or multiple files containing the necessary genericode code lists (must be `application/xml`).",
                media_type="application/xml",
            ),
        ] = [],
    ):
        """
        Convert an xdf2 schema alongside all necessary code lists to xdf3.
        """
        try:
            schema_message = xdf2.parse_schema_message(schema_file)

            parsed_code_lists = [
                genericode.parse_code_list(code_list)
                for code_list in code_lists
                if code_list != b""
            ]
            code_list_map = {
                code_list.identifier: code_list for code_list in parsed_code_lists
            }

            xdf3_message = xdf2_to_xdf3.convert_message(schema_message, code_list_map)
            xdf3_content = xdf3.serialize_schema_message(xdf3_message)
        except (
            XdfException,
            genericode.GenericodeException,
            xdf2_to_xdf3.ConversionException,
        ) as exception:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(exception)
            ) from exception

        filename = f"{xdf3_message.id}V{xdf3_message.version}_xdf3.xml"

        return XmlFileResponse(
            content=xdf3_content,
            filename=filename,
        )

    @router.get("/xdf3-to-xdf2-converter", include_in_schema=False)
    async def get_xdf3_to_xdf2_converter(  # type: ignore [reportUnusedFunction]
        request: Request,
    ):
        return render(
            request,
            "converter/xdf3-to-xdf2.html.jinja",
            {
                "metaDescription": "XDF3 zu XDF2 Konverter",
                "titleTag": "XDF3 zu XDF2 Konverter",
            },
        )

    @router.post(
        "/xdf3-to-xdf2-converter",
        name="Convert an xdf3 schema to xdf2",
        response_class=ZipFileResponse,
        responses={
            400: {"model": ErrorMessage, "description": "Conversion Error"},
        },
        openapi_extra={"security": [{}]},
    )
    async def convert_xdf3_to_xdf2(  # type: ignore [reportUnusedFunction]
        # NOTE: This must not be called `schema`, as this is a reserved keyword in pydantic 2.
        schema_file: Annotated[
            bytes,
            File(
                title="Schema",
                alias="schema",
                # NOTE: `validation_alias` must be set additionally, as fastapi has a bug where the alias
                # is not respected when creating the OpenAPI Docs.
                validation_alias="schema",
                description="The xdf3 `Datenschema` (must be `application/xml`).",
                media_type="application/xml",
            ),
        ],
    ):
        """
        Convert an xdf3 schema to xdf2.
        The result will be a zip file containing the schema as well as all extraced code lists.
        """
        try:
            schema_message = xdf3.parse_schema_message(schema_file)

            result = xdf3_to_xdf2.convert_message(schema_message)

            data = BytesIO()
            with zipfile.ZipFile(data, "w") as archive:
                message = result.message
                archive.writestr(
                    f"{message.id}V{message.version}_converted.xdf2.xml",
                    xdf2.serialize_schema_message(message),
                )

                for fim_id, code_list in result.code_lists.items():
                    archive.writestr(
                        f"{fim_id}.xml",
                        genericode.serialize_code_list(code_list),
                    )

        except (
            XdfException,
            genericode.GenericodeException,
            xdf3_to_xdf2.ConversionException,
        ) as exception:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(exception)
            )

        filename = f"{result.message.id}V{result.message.version}_converted.zip"

        return ZipFileResponse(
            content=data.getvalue(),
            filename=filename,
        )

    @router.get("/xdf2-json-schema-converter", include_in_schema=False)
    async def get_json_schema_converter(  # type: ignore [reportUnusedFunction]
        request: Request,
    ):
        return render(
            request,
            "converter/json-schema.html.jinja",
            {
                "metaDescription": "XDF2 zu JSON Schema Konverter",
                "titleTag": "XDF2 zu JSON Schema Konverter",
            },
        )

    @router.post(
        "/xdf2-json-schema-converter",
        name="Convert an xdf2 schema to JSON Schema",
        responses={
            400: {"model": ErrorMessage, "description": "Conversion Error"},
        },
        openapi_extra={"security": [{}]},
    )
    async def convert_to_json_schema(  # type: ignore [reportUnusedFunction]
        # NOTE: This must not be called `schema`, as this is a reserved keyword in pydantic 2.
        schema_file: Annotated[
            bytes,
            File(
                title="Schema",
                alias="schema",
                # NOTE: `validation_alias` must be set additionally, as fastapi has a bug where the alias
                # is not respected when creating the OpenAPI Docs.
                validation_alias="schema",
                description="The `Datenschema` (must be `application/xml`).",
                media_type="application/xml",
            ),
        ],
        code_lists: Annotated[
            list[bytes],
            File(
                title="Codelists",
                description="One or multiple files containing the necessary code lists (must be `application/xml`).",
                media_type="application/xml",
            ),
        ] = [],
    ) -> ORJSONResponse:
        """
        Convert an xdf2 schema alongside all necessary code lists to JSON Schema.
        There is also a [web version](https://test.fim.uber.space/services/xdf2-json-schema-converter) available.
        """
        try:
            schema_message = xdf2.parse_schema_message(schema_file)

            parsed_code_lists = [
                genericode.parse_code_list(code_list)
                for code_list in code_lists
                if code_list != b""
            ]
            code_list_map = {
                code_list.identifier.canonical_version_uri: code_list
                for code_list in parsed_code_lists
            }

            json_schema_data = json_schema.from_xdf2(
                "anonymous-schema", schema_message, code_list_map
            )
        except (
            json_schema.JsonSchemaException,
            XdfException,
            genericode.GenericodeException,
        ) as exception:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(exception)
            )

        filename = f"{schema_message.id}V{schema_message.version}_schema.json"

        return ORJSONResponse(
            content=json_schema_data,
            headers={"Content-Disposition": f'attachment; filename="{filename}"'},
        )

    @router.get("/xdf3-json-schema-converter", include_in_schema=False)
    async def get_xdf3_json_schema_converter(  # type: ignore [reportUnusedFunction]
        request: Request,
    ):
        return render(
            request,
            "converter/xdf3-json-schema.html.jinja",
            {
                "metaDescription": "XDF3 zu JSON Schema Konverter",
                "titleTag": "XDF3 zu JSON Schema Konverter",
            },
        )

    @router.post(
        "/xdf3-json-schema-converter",
        name="Convert an xdf3 schema to JSON Schema",
        responses={
            400: {"model": ErrorMessage, "description": "Conversion Error"},
            503: {"model": ErrorMessage, "description": "XRepository Error"},
        },
        openapi_extra={"security": [{}]},
    )
    async def convert_xdf3_to_json_schema(  # type: ignore [reportUnusedFunction]
        # NOTE: This must not be called `schema`, as this is a reserved keyword in pydantic 2.
        schema_file: Annotated[
            bytes,
            File(
                title="Schema",
                alias="schema",
                # NOTE: `validation_alias` must be set additionally, as fastapi has a bug where the alias
                # is not respected when creating the OpenAPI Docs.
                validation_alias="schema",
                description="The xdf3 `Datenschema` (must be `application/xml`).",
                media_type="application/xml",
            ),
        ],
    ) -> ORJSONResponse:
        """
        Convert an xdf3 schema to JSON Schema.
        All necessary code lists are loaded dynamically from [XRepository](https://www.xrepository.de/).
        """
        try:
            schema_message = xdf3.parse_schema_message(schema_file)

            code_lists = await xrepository_service.load_code_lists(
                schema_message.get_code_list_uris()
            )
            code_list_map = {
                code_list.identifier.canonical_version_uri: code_list
                for code_list, _ in code_lists
            }

            json_schema_data = json_schema.from_xdf3(
                "anonymous-schema", schema_message, code_list_map
            )
        except (
            json_schema.JsonSchemaException,
            XdfException,
            genericode.GenericodeException,
            xrepository.MissingCodeListError,
        ) as error:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=str(error),
            ) from error
        except xrepository.XRepositoryError as error:
            raise HTTPException(
                status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                detail="Cannot load code list from XRepository",
            ) from error

        filename = f"{schema_message.id}V{schema_message.version}_schema.json"

        return ORJSONResponse(
            content=json_schema_data,
            headers={"Content-Disposition": f'attachment; filename="{filename}"'},
        )

    @router.get("/xdf2-xsd-converter", include_in_schema=False)
    async def get_xml_schema_converter(  # type: ignore [reportUnusedFunction]
        request: Request,
    ):
        return render(
            request,
            "converter/xml-schema.html.jinja",
            {
                "metaDescription": "XDF2 zu XSD Konverter",
                "titleTag": "XDF2 zu XSD Konverter",
            },
        )

    @router.post(
        "/xdf2-xsd-converter",
        name="Convert an xdf2 schema to XSD",
        response_class=XmlFileResponse,
        responses={
            400: {"model": ErrorMessage, "description": "Conversion Error"},
        },
        openapi_extra={"security": [{}]},
    )
    async def convert_to_xml_schema(  # type: ignore [reportUnusedFunction]
        # NOTE: This must not be called `schema`, as this is a reserved keyword in pydantic 2.
        schema_file: Annotated[
            bytes,
            File(
                title="Schema",
                alias="schema",
                # NOTE: `validation_alias` must be set additionally, as fastapi has a bug where the alias
                # is not respected when creating the OpenAPI Docs.
                validation_alias="schema",
                description="The xdf2 `Datenschema` (must be `application/xml`).",
                media_type="application/xml",
            ),
        ],
        code_lists: Annotated[
            list[bytes],
            File(
                title="Codelists",
                description="One or multiple files containing the necessary code lists (must be `application/xml`).",
                media_type="application/xml",
            ),
        ] = [],
    ):
        """
        Convert an xdf2 schema alongside all necessary code lists to XSD.
        """

        try:
            schema_message = xdf2.parse_schema_message(schema_file)

            parsed_code_lists = [
                genericode.parse_code_list(code_list)
                for code_list in code_lists
                if code_list != b""
            ]
            code_list_map = {
                code_list.identifier.canonical_version_uri: code_list
                for code_list in parsed_code_lists
            }

            xml_schema_data = xsd.from_xdf2(schema_message, code_list_map)
        except (
            xsd.XsdException,
            XdfException,
            genericode.GenericodeException,
        ) as exception:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(exception)
            )

        filename = f"{schema_message.id}V{schema_message.version}_schema.xml"

        return XmlFileResponse(
            content=xml_schema_data,
            filename=filename,
        )

    @router.get("/check-xdf2", include_in_schema=False)
    async def get_check_xdf2(  # type: ignore [reportUnusedFunction]
        request: Request,
    ):
        return render(
            request,
            "check-xdf2/index.html.jinja",
            {
                "metaDescription": "XDF2 Check",
                "titleTag": "XDF2 Check",
            },
        )

    @router.post("/check-xdf2/report", include_in_schema=False)
    async def check_xdf2(  # type: ignore [reportUnusedFunction]
        request: Request,
        # NOTE: This must not be called `schema`, as this is a reserved keyword in pydantic 2.
        schema_file: Annotated[
            bytes,
            File(
                title="Schema",
                alias="schema",
                # NOTE: `validation_alias` must be set additionally, as fastapi has a bug where the alias
                # is not respected when creating the OpenAPI Docs.
                validation_alias="schema",
                description="The xdf2 `Datenschema` (must be `application/xml`).",
                media_type="application/xml",
            ),
        ],
    ):
        try:
            schema_message = xdf2.parse_schema_message(schema_file)
            report = reports.check_xdf2_quality(schema_message, {})
        except XdfException as error:
            return render(
                request,
                "check-xdf2/error.html.jinja",
                {
                    "metaDescription": "XDF2 Check",
                    "titleTag": "XDF2 Check",
                    "error": error,
                },
            )
        else:
            identifier_name_mapping: dict[
                xdf2.Identifier | xdf3.Identifier,
                tuple[str, xdf3.FreigabeStatus | None],
            ] = {
                identifier: (str(element.name), None)
                for (identifier, element) in {
                    **schema_message.fields,
                    **schema_message.groups,
                }.items()
            }

            return render(
                request,
                "check-xdf2/checks.html.jinja",
                {
                    "metaDescription": "XDF2 Check",
                    "titleTag": "XDF2 Check",
                    "report": QualityReportView.from_quality_report(
                        schema_fim_id=schema_message.id,
                        schema_fim_version=schema_message.version,
                        schema_name=schema_message.schema.name,
                        schema_freigabe_status=None,
                        quality_report=report,
                        element_mapping=identifier_name_mapping,
                    ),
                    "schema": schema_message.schema,
                    "total_elements": len(schema_message.groups)
                    + len(schema_message.fields),
                    "route": "xdf2-check",
                },
            )

    @router.post(
        "/check-xdf2",
        name="Check an xdf2 schema",
        response_class=ORJSONResponse,
        responses={
            400: {"model": ErrorMessage, "description": "Could not parse schema"},
        },
        openapi_extra={"security": [{}]},
    )
    async def check_xdf2_json(  # type: ignore [reportUnusedFunction]
        # NOTE: This must not be called `schema`, as this is a reserved keyword in pydantic 2.
        schema_file: Annotated[
            bytes,
            File(
                title="Schema",
                alias="schema",
                # NOTE: `validation_alias` must be set additionally, as fastapi has a bug where the alias
                # is not respected when creating the OpenAPI Docs.
                validation_alias="schema",
                description="The xdf2 `Datenschema` (must be `application/xml`).",
                media_type="application/xml",
            ),
        ],
    ):
        """
        Create a quality report for an xdf2 schema.
        Details on the quality metrics can be found in the [user documentation](https://docs.fitko.de/fim/docs).
        """

        try:
            schema_message = xdf2.parse_schema_message(schema_file)
            return reports.check_xdf2_quality(schema_message, {})
        except XdfException as error:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(error)
            )

    @router.get("/check-xdf3", include_in_schema=False)
    async def get_check_xdf3(  # type: ignore [reportUnusedFunction]
        request: Request,
    ):
        return render(
            request,
            "check-xdf3/index.html.jinja",
            {
                "metaDescription": "XDF3 Check",
                "titleTag": "XDF3 Check",
            },
        )

    @router.post("/check-xdf3/report", include_in_schema=False)
    async def check_xdf3(  # type: ignore [reportUnusedFunction]
        request: Request,
        # NOTE: This must not be called `schema`, as this is a reserved keyword in pydantic 2.
        schema_file: Annotated[
            bytes,
            File(
                title="Schema",
                alias="schema",
                # NOTE: `validation_alias` must be set additionally, as fastapi has a bug where the alias
                # is not respected when creating the OpenAPI Docs.
                validation_alias="schema",
                description="The xdf3 `Datenschema` (must be `application/xml`).",
                media_type="application/xml",
            ),
        ],
    ):
        try:
            schema_message = xdf3.parse_schema_message(schema_file)
            report = reports.check_xdf3_quality(schema_message, {})
        except XdfException as error:
            return render(
                request,
                "check-xdf3/error.html.jinja",
                {
                    "metaDescription": "XDF3 Check",
                    "titleTag": "XDF3 Check",
                    "error": error,
                    "route": "xdf3-check",
                },
            )
        else:
            identifier_name_mapping: dict[
                xdf2.Identifier | xdf3.Identifier,
                tuple[str, xdf3.FreigabeStatus | None],
            ] = {
                identifier: (
                    str(element.name),
                    element.freigabe_status,
                )
                for (identifier, element) in {
                    **schema_message.fields,
                    **schema_message.groups,
                }.items()
            }
            return render(
                request,
                "check-xdf3/checks.html.jinja",
                {
                    "metaDescription": "XDF3 Check",
                    "titleTag": "XDF3 Check",
                    "report": QualityReportView.from_quality_report(
                        schema_fim_id=schema_message.id,
                        schema_fim_version=schema_message.version,
                        schema_name=schema_message.schema.name,
                        schema_freigabe_status=schema_message.schema.freigabe_status,
                        quality_report=report,
                        element_mapping=identifier_name_mapping,
                    ),
                    "schema": schema_message.schema,
                    "total_elements": len(schema_message.groups)
                    + len(schema_message.fields),
                    "route": "check-xdf3",
                },
            )

    @router.post(
        "/check-xdf3",
        name="Check an xdf3 schema",
        responses={
            400: {"model": ErrorMessage, "description": "Could not parse schema"},
        },
        response_class=ORJSONResponse,
        openapi_extra={"security": [{}]},
    )
    async def check_xdf3_json(  # type: ignore [reportUnusedFunction]
        # NOTE: This must not be called `schema`, as this is a reserved keyword in pydantic 2.
        schema_file: Annotated[
            bytes,
            File(
                title="Schema",
                alias="schema",
                # NOTE: `validation_alias` must be set additionally, as fastapi has a bug where the alias
                # is not respected when creating the OpenAPI Docs.
                validation_alias="schema",
                description="The `Datenschema` (must be `application/xml`).",
                media_type="application/xml",
            ),
        ],
    ):
        """
        Create a quality report for an xdf3 schema.
        Details on the quality metrics can be found in the [user documentation](https://docs.fitko.de/fim/docs).
        """

        try:
            schema_message = xdf3.parse_schema_message(schema_file)
            return reports.check_xdf3_quality(schema_message, {})
        except XdfException as error:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(error)
            )

    @router.get("/check-leistung", include_in_schema=False)
    async def get_check_leistung(  # type: ignore [reportUnusedFunction]
        request: Request,
    ):
        return render(
            request,
            "check-leistung/index.html.jinja",
            {
                "metaDescription": "Leistungs-Check",
                "titleTag": "Leistungs-Check",
            },
        )

    @router.post("/check-leistung/report", include_in_schema=False)
    async def check_leistung(  # type: ignore [reportUnusedFunction]
        request: Request,
        leistung: Annotated[
            bytes,
            File(
                title="Leistung",
                description="The `Leistung` (must be `application/xml`).",
                media_type="application/xml",
            ),
        ],
        service: Service = Depends(get_service),
    ):
        try:
            parsed_leistung = xzufi.parse_leistung(leistung)
        except xzufi.XzufiException as error:
            return render(
                request,
                "check-leistung/error.html.jinja",
                {
                    "metaDescription": "Leistungs-Check",
                    "titleTag": "Leistungs-Check",
                    "error": error,
                },
            )
        else:
            report = await service.get_leistung_failed_checks(parsed_leistung)
            return render(
                request,
                "check-leistung/checks.html.jinja",
                {
                    "metaDescription": "Leistungs-Check",
                    "titleTag": "Leistungs-Check",
                    "leistung": parsed_leistung,
                    "total_checks": len(report.failed_checks),
                    "failed_checks": report.failed_checks,
                },
            )

    @router.post(
        "/check-leistung",
        name="Check a service",
        responses={
            400: {"model": ErrorMessage, "description": "Could not parse service"},
        },
        response_class=ORJSONResponse,
        openapi_extra={"security": [{}]},
    )
    async def check_leistung_json(  # type: ignore [reportUnusedFunction]
        leistung: Annotated[
            bytes,
            File(
                title="Leistung",
                description="The `Leistung` (must be `application/xml`).",
                media_type="application/xml",
            ),
        ],
        service: Service = Depends(get_service),
    ) -> xzufi.QualityReport:
        try:
            parsed_leistung = xzufi.parse_leistung(leistung)
        except xzufi.XzufiException as error:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(error)
            )
        else:
            return await service.get_leistung_failed_checks(parsed_leistung)

    @router.get("/fim-json-legend", include_in_schema=False)
    async def _create_json_legend_page(  # type: ignore [reportUnusedFunction]
        request: Request,
    ):
        return render(
            request,
            "fim-json-legend/index.html.jinja",
            {
                "titleTag": "JSON Schema Legende",
            },
        )

    @router.post("/fim-json-legend/result", include_in_schema=False)
    async def _create_json_legend_result(  # type: ignore [reportUnusedFunction]
        request: Request,
        service: Annotated[Service, Depends(get_service)],
        data: Annotated[
            bytes,
            File(
                title="JSON Data",
                description="The JSON data",
                media_type="application/json",
            ),
        ],
    ):
        try:
            payload = orjson.loads(data)
            legend = await service.get_json_data_legend(payload)
        except (orjson.JSONDecodeError, InvalidFimIdException) as error:
            return render(
                request,
                "fim-json-legend/error.html.jinja",
                {
                    "metaDescription": "XDF3 Check",
                    "titleTag": "XDF3 Check",
                    "error": error,
                    "route": "fim-json-legend",
                },
            )
        else:
            return render(
                request,
                "fim-json-legend/result.html.jinja",
                {
                    "metaDescription": "XDF3 Check",
                    "titleTag": "XDF3 Check",
                    "input_data": orjson.dumps(
                        payload, option=orjson.OPT_INDENT_2
                    ).decode("utf-8"),
                    "groups": legend.groups,
                    "fields": legend.fields,
                },
            )

    @router.post(
        "/fim-json-legend",
        name="Create a legend for JSON data",
        response_class=ORJSONResponse,
        responses={
            400: {"model": ErrorMessage, "description": "Invalid input data"},
        },
        openapi_extra={"security": [{}]},
    )
    async def create_json_legend(  # type: ignore [reportUnusedFunction]
        service: Annotated[Service, Depends(get_service)],
        data: Annotated[
            bytes,
            File(
                title="JSON Data",
                description="The JSON data",
                media_type="application/json",
            ),
        ],
    ) -> JsonLegend:
        """
        Create a legend for the keys in the provided JSON data.
        """

        try:
            payload = orjson.loads(data)
            return await service.get_json_data_legend(payload)
        except (orjson.JSONDecodeError, InvalidFimIdException) as error:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(error)
            )

    @router.get(
        "/rss/schemas/{fim_id}",
        response_class=ORJSONResponse,
        name="Get an RSS feed for the specified schema.",
        include_in_schema=False,
    )
    async def _get_schema_rss_feed(  # type: ignore [reportUnusedFunction]
        fim_id: SchemaId, service: Service = Depends(get_service)
    ) -> Response:
        # NOTE(Felix): If the RSS support is going to be any more involved, this should probably
        # be cleaned up a bit. For now, just putting all of the code supporting RSS into
        # one place seemed the more maintanable option, as we do not know yet how long this will be used/supported.
        # So to delete all of the RSS support, only this endpoint must be removed.
        schemas = await service.get_schema_versions(fim_id)
        if len(schemas) == 0:
            return Response(status_code=status.HTTP_404_NOT_FOUND)

        schemas = sorted(schemas, key=lambda schema: schema.last_update, reverse=True)

        items = "".join(
            [
                (
                    "<item>"
                    f"<title>{schema.fim_id} v{schema.fim_version} Update</title>"
                    f"<link>{urllib.parse.urljoin(baseurl, f'/schemas/{fim_id}/{schema.fim_version}')}</link>"
                    f"<description>Das Stammdatenschema {schema.fim_id} v{schema.fim_version} wurde aktualisiert</description>"
                    f"<pubDate>{email.utils.format_datetime(schema.last_update)}</pubDate>"
                    "</item>"
                )
                for schema in schemas
            ]
        )

        # RSS 2.0 Format
        # See: https://www.w3schools.com/XML/xml_rss.asp
        rss_xml = (
            '<?xml version="1.0" encoding="UTF-8" ?>'
            '<rss version="2.0">'
            "<channel>"
            f"<title>Stammdatenschema {fim_id} Updates</title>"
            f"<link>{urllib.parse.urljoin(baseurl, f'/schemas/{fim_id}/latest')}</link>"
            f"<description>Aktuelle Updates für das Stammdatenschema {fim_id}</description>"
            f"{items}"
            "</channel>"
            "</rss>"
        )

        return Response(content=rss_xml, media_type="application/xml")

    RSS_FEED_TIMESPAN = timedelta(weeks=12)

    @router.get(
        "/rss/schemas",
        response_class=ORJSONResponse,
        name="Get an RSS feed for recently updated Schemas.",
        include_in_schema=False,
    )
    async def _get_schemas_rss_feed(  # type: ignore [reportUnusedFunction]
        service: Service = Depends(get_service),
    ):
        resources = await service.get_recently_changed_schema_resources(
            timespan=RSS_FEED_TIMESPAN
        )

        items = [
            create_schema_rss_feed_item(baseurl, resource) for resource in resources
        ]

        rss_xml = create_rss_xml(
            "Updates Baustein Datenfelder",
            "Aktuelle Updates des Bausteins Datenfelder",
            items,
        )

        return Response(content=rss_xml, media_type="application/rss+xml;charset=utf-8")

    @router.get(
        "/rss/services",
        response_class=ORJSONResponse,
        name="Get an RSS feed for recently updated Steckbriefe and Stammtexte of Leistungen.",
        include_in_schema=False,
    )
    async def _get_services_rss_feed(  # type: ignore [reportUnusedFunction]
        service: Service = Depends(get_service),
    ) -> Response:
        leistungen = await service.get_recently_changed_service_steckbriefe(
            timespan=RSS_FEED_TIMESPAN
        )

        items = [
            create_service_rss_feed_item(baseurl, leistung)
            for leistung in leistungen
            if leistung.geaendert_datum_zeit is not None
        ]

        rss_xml = create_rss_xml(
            "Leistungssteckbrief Updates",
            "Aktuelle Updates von Leistungssteckbriefen",
            items,
        )

        return Response(content=rss_xml, media_type="application/rss+xml;charset=utf-8")

    @router.get(
        "/rss/processes",
        response_class=ORJSONResponse,
        name="Get an RSS feed for recently updated Prozesse.",
        include_in_schema=False,
    )
    async def _get_processes_rss_feed(  # type: ignore [reportUnusedFunction]
        service: Service = Depends(get_service),
    ) -> Response:
        prozesse = await service.get_recently_changed_processes(
            timespan=RSS_FEED_TIMESPAN
        )

        items = [
            create_process_rss_feed_item(baseurl, prozess)
            for prozess in prozesse
            if prozess.letzter_aenderungszeitpunkt is not None
        ]

        rss_xml = create_rss_xml(
            "Prozesse Updates", "Aktuelle Updates von Prozessen", items
        )

        return Response(content=rss_xml, media_type="application/rss+xml;charset=utf-8")

    @router.get("/search-csv-download")
    async def _search_csv_download(  # type: ignore [reportUnusedFunction]
        filters: UiFilters = Depends(get_ui_filters),
        service: Service = Depends(get_service),
    ) -> Response:
        match filters.resource:
            case Resource.DOCUMENT_PROFILE:
                search_options = filters.to_steckbrief_search_options()
                results = await service.get_dokumentsteckbrief_csv_search_results(
                    search_options
                )
                filename = "dokumentsteckbrief_search_results.csv"
            case Resource.SCHEMA:
                search_options = filters.to_schema_search_options()
                results = await service.get_schema_csv_search_results(search_options)
                filename = "schema_search_results.csv"
            case Resource.GROUP:
                search_options = filters.to_group_search_options()
                results = await service.get_group_csv_search_results(search_options)
                filename = "datenfeldgruppe_search_results.csv"
            case Resource.FIELD:
                search_options = filters.to_field_search_options()
                results = await service.get_field_csv_search_results(search_options)
                filename = "datenfeld_search_results.csv"
            case Resource.SERVICE:
                search_options = filters.to_leistungssteckbrief_search_options()
                results = await service.get_leistungssteckbrief_csv_search_results(
                    search_options
                )
                filename = "leistungssteckbrief_search_results.csv"
            case Resource.SERVICE_DESCRIPTION:
                search_options = filters.to_leistungsbeschreibung_search_options()
                results = await service.get_leistungsbeschreibung_csv_search_results(
                    search_options
                )
                filename = "leistungsbeschreibung_search_results.csv"
            case Resource.PROCESS:
                search_options = filters.to_prozess_search_options()
                results = await service.get_prozess_csv_search_results(search_options)
                filename = "prozess_search_results.csv"
            case Resource.PROCESSCLASS:
                search_options = filters.to_prozessklasse_search_options()
                results = await service.get_prozessklasse_csv_search_results(
                    search_options
                )
                filename = "prozessklasse_search_results.csv"
            case Resource.SAMPLEPROCESS:
                search_options = filters.to_musterprozess_search_options()
                results = await service.get_prozess_csv_search_results(search_options)
                filename = "musterprozess_search_results.csv"

        content = StringIO()
        writer = csv.writer(content)
        writer.writerow(CSVSearchResult.CSV_HEADERS)
        for item in results:
            writer.writerow(item.to_csv_row(baseurl))

        return Response(
            content=content.getvalue(),
            headers={"Content-Disposition": f'attachment; filename="{filename}"'},
            media_type="text/csv; charset=utf-8",
        )

    return router
