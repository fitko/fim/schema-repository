from __future__ import annotations

from dataclasses import dataclass
from typing import Any, AsyncGenerator, Callable, Literal

from fastapi import APIRouter, Depends, Request, status
from starlette.responses import HTMLResponse

from fimportal.helpers import RenderFunction
from fimportal.models.ui_prototype import AvailableVersion
from fimportal.models.xdf3 import (
    FullSteckbriefOut,
    SteckbriefOut,
)
from fimportal.routers.common import (
    Resource,
    UiBausteinReferenz,
    UiHandlungsgrundlage,
    UiRelationen,
    create_clean_query_string,
)
from fimportal.service import Service


@dataclass(slots=True)
class DocumentProfileHeader:
    name: str
    fim_id: str
    fim_version: str
    freigabe_status: str
    xdf_version: str
    anzahl_referenzen: int
    abstraction_level: Literal["Abstrakt", "Konkret"]

    base_href: str
    back_url: str

    @staticmethod
    def create(
        document_profile: FullSteckbriefOut, search_query_string: str
    ) -> "DocumentProfileHeader":
        return DocumentProfileHeader(
            name=document_profile.name,
            fim_id=document_profile.fim_id,
            fim_version=document_profile.fim_version,
            freigabe_status=document_profile.freigabe_status.to_label(),
            xdf_version="XDatenfelder " + document_profile.xdf_version.value,
            anzahl_referenzen=0,  # TODO Implement
            base_href=f"/document-profiles/{document_profile.fim_id}/{document_profile.fim_version}",
            back_url=f"/search{search_query_string}",
            abstraction_level="Abstrakt"
            if document_profile.ist_abstrakt
            else "Konkret",
        )


@dataclass(slots=True)
class DocumentProfileIndexPage:
    header: DocumentProfileHeader
    search_query_string: str

    beschreibung: str | None
    definition: str | None
    dokumentart: str | None
    bezug: list[UiHandlungsgrundlage]
    gueltig_ab: str | None
    gueltig_bis: str | None
    bezeichnung: str | None
    hilfetext: str | None
    stichwoerter: str | None
    versionshinweis: str | None

    available_versions: list[AvailableVersion]
    status_gesetzt_durch: str | None
    status_gesetzt_am: str | None
    letzte_aenderung: str | None
    last_update: str
    veroeffentlicht_am: str | None

    download_label: str
    download_href: str

    schema_references: list[UiBausteinReferenz]
    process_references: list[UiBausteinReferenz]
    relations: UiRelationen

    @staticmethod
    def from_steckbrief(
        steckbrief: FullSteckbriefOut,
        versions: list[SteckbriefOut],
        search_query_string: str,
    ) -> DocumentProfileIndexPage:
        return DocumentProfileIndexPage(
            header=DocumentProfileHeader.create(steckbrief, search_query_string),
            search_query_string=search_query_string,
            beschreibung=steckbrief.beschreibung,
            definition=steckbrief.definition,
            dokumentart=steckbrief.dokumentart.to_label(),
            bezug=[
                UiHandlungsgrundlage(bezug, None) for bezug in steckbrief.bezug
            ],  # TODO: include rechtsgrundlagen hyperlinks
            hilfetext=steckbrief.hilfetext,
            stichwoerter=", ".join(steckbrief.stichwort),
            versionshinweis=steckbrief.versionshinweis,
            gueltig_ab=steckbrief.gueltig_ab.strftime("%d.%m.%Y")
            if steckbrief.gueltig_ab is not None
            else None,
            gueltig_bis=steckbrief.gueltig_bis.strftime("%d.%m.%Y")
            if steckbrief.gueltig_bis is not None
            else None,
            bezeichnung=steckbrief.bezeichnung,
            available_versions=[
                AvailableVersion.from_steckbrief(
                    version,
                    current_version=steckbrief.fim_version,
                    search_query_string=search_query_string,
                )
                for version in versions
            ][::-1],
            veroeffentlicht_am=(
                steckbrief.veroeffentlichungsdatum.strftime("%d.%m.%Y")
                if steckbrief.veroeffentlichungsdatum
                else None
            ),
            status_gesetzt_durch=steckbrief.status_gesetzt_durch,
            status_gesetzt_am=(
                steckbrief.status_gesetzt_am.strftime("%d.%m.%Y")
                if steckbrief.status_gesetzt_am
                else None
            ),
            letzte_aenderung=steckbrief.letzte_aenderung.strftime("%d.%m.%Y"),
            last_update=steckbrief.last_update.strftime("%d.%m.%Y"),
            download_href=f"/api/v1/document-profiles/{ steckbrief.fim_id }/{ steckbrief.fim_version }/xdf",
            download_label=f"Dokumentsteckbrief (XDatenfelder {steckbrief.xdf_version.value})",
            schema_references=[
                UiBausteinReferenz(
                    id=ref.fim_id,
                    href=f"/schemas/{ref.fim_id}/{ref.fim_version}",
                    label=ref.name,
                )
                for ref in steckbrief.datenschemata
            ],
            process_references=[
                UiBausteinReferenz(
                    id=ref.id, href=f"/processes/{ref.id}", label=ref.name
                )
                for ref in steckbrief.prozesse
            ],
            relations=UiRelationen.create(steckbrief.relation, "steckbrief"),
        )


def create_document_profile_router(
    get_service: Callable[[], AsyncGenerator[Service, Any]],
    render: RenderFunction,
) -> APIRouter:
    router = APIRouter()

    def _document_profile_not_found(request: Request, fim_id: str, fim_version: str):
        return render(
            request,
            "404.html.jinja",
            {
                "message": f"Dokumentsteckbrief {fim_id} v{fim_version} konnte nicht gefunden werden.",
            },
            status_code=status.HTTP_404_NOT_FOUND,
        )

    @router.get("/{fim_id}/{fim_version}", response_class=HTMLResponse)
    async def get_steckbrief_details(  # type: ignore [reportUnusedFunction]
        request: Request,
        fim_id: str,
        fim_version: str,
        service: Service = Depends(get_service),
    ):
        steckbrief = await service.get_steckbrief(
            steckbrief_id=fim_id, steckbrief_version=fim_version
        )
        if steckbrief is None:
            return _document_profile_not_found(request, fim_id, fim_version)

        versions = await service.get_steckbrief_versions(steckbrief.fim_id)
        search_query_string = create_clean_query_string(
            request, Resource.DOCUMENT_PROFILE
        )

        return render(
            request,
            "document_profile_detail.html.jinja",
            {
                "titleTag": f"{steckbrief.fim_id} v{steckbrief.fim_version} • Dokumentsteckbriefe • FIM Portal",
                "page": DocumentProfileIndexPage.from_steckbrief(
                    steckbrief,
                    versions=versions,
                    search_query_string=search_query_string,
                ),
            },
        )

    return router
