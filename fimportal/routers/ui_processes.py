from dataclasses import dataclass
from typing import Any, AsyncGenerator, Callable, Literal

from fastapi import APIRouter, Depends, Request, status
from fastapi.responses import HTMLResponse

from fimportal.helpers import RenderFunction
from fimportal.models.prozess import DokumentsteckbriefReferenz, FullProzessOut
from fimportal.routers.common import (
    Resource,
    UiHeaderTab,
    ProzessMetadata,
    UiBausteinReferenz,
    UiHandlungsgrundlage,
    create_clean_query_string,
)
from fimportal.service import Service
from fimportal.xprozesse.prozess import (
    Detaillierungsstufe,
    DokumentsteckbriefRolle,
    Prozess,
    ProzessSteckbrief,
    Prozessrolle,
    parse_prozess,
)


ProcessTabId = Literal["index", "visualization", "references", "downloads"]
ProcessTab = UiHeaderTab[ProcessTabId]


@dataclass(slots=True)
class ProzessHeader:
    id: str
    version: str
    name: str
    freigabe_status: str

    base_href: str
    back_url: str
    is_sampleprocess: bool
    active_tab: ProcessTabId
    tabs: list[ProcessTab]

    @staticmethod
    def create(
        prozess: Prozess, active_tab: ProcessTabId, search_query_string: str
    ) -> "ProzessHeader":
        base_href = f"/processes/{prozess.id}"

        return ProzessHeader(
            id=prozess.id,
            version=prozess.version or "-",
            name=prozess.name,
            freigabe_status=prozess.freigabe_status.to_label()
            if prozess.freigabe_status
            else "unbestimmter Freigabestatus",
            is_sampleprocess=prozess.detaillierungsstufe
            == Detaillierungsstufe.MUSTERINFORMATIONEN,
            base_href=base_href,
            back_url=f"/search{search_query_string}",
            active_tab=active_tab,
            tabs=[
                ProcessTab(
                    id="index",
                    label="Übersicht",
                    icon="bi-eye-fill",
                    active_tab=active_tab,
                    base_href=base_href,
                    search_query_string=search_query_string,
                ),
                ProcessTab(
                    id="visualization",
                    label="Visualisierung",
                    icon="bi-diagram-2-fill",
                    active_tab=active_tab,
                    base_href=base_href,
                    search_query_string=search_query_string,
                ),
            ],
        )


@dataclass(slots=True)
class Prozessteilnehmende:
    initiierende: list[str]
    hauptakteure: list[str]
    mitwirkende: list[str]
    ergebnis_empfaenger: list[str]

    @staticmethod
    def from_steckbrief(steckbrief: ProzessSteckbrief | None) -> "Prozessteilnehmende":
        if steckbrief is None:
            return Prozessteilnehmende(
                initiierende=[], hauptakteure=[], mitwirkende=[], ergebnis_empfaenger=[]
            )
        return Prozessteilnehmende(
            initiierende=steckbrief.get_prozessteilnehmer_typ(Prozessrolle.INITIATOR),
            hauptakteure=steckbrief.get_prozessteilnehmer_typ(Prozessrolle.HAUPTAKTEUR),
            mitwirkende=steckbrief.get_prozessteilnehmer_typ(Prozessrolle.MITWIRKENDER),
            ergebnis_empfaenger=steckbrief.get_prozessteilnehmer_typ(
                Prozessrolle.ERGEBENIS_EMPFAENGER
            ),
        )


@dataclass(slots=True)
class ProzessDokumentensteckbriefe:
    ausloeser: list[UiBausteinReferenz]
    ergebnis: list[UiBausteinReferenz]

    @staticmethod
    def from_references(references: dict[str, DokumentsteckbriefReferenz]):
        steckbrief = ProzessDokumentensteckbriefe(ausloeser=[], ergebnis=[])
        for reference in references.values():
            if (
                reference.rolle == DokumentsteckbriefRolle.ERGEBNIS
                and reference.name is not None
            ):
                steckbrief.ergebnis.append(
                    UiBausteinReferenz(
                        id=reference.fim_id,
                        href=f"/document-profiles/{reference.fim_id}/latest",
                        label=reference.name,
                    )
                )
            elif (
                reference.rolle == DokumentsteckbriefRolle.AUSLOESER
                and reference.name is not None
            ):
                steckbrief.ausloeser.append(
                    UiBausteinReferenz(
                        id=reference.fim_id,
                        href=f"/document-profiles/{reference.fim_id}/latest",
                        label=reference.name,
                    )
                )
        return steckbrief


@dataclass(slots=True)
class Anwendungsgebiet:
    value: str
    icon: str

    @staticmethod
    def from_land(land: str) -> "Anwendungsgebiet":
        return Anwendungsgebiet(
            value=land if land != "bund" else "Alle Bundesländer",
            icon=f"/img/{land.lower().replace('ü', 'ue')}.svg",
        )


@dataclass(slots=True)
class ProzessIndexPage:
    header: ProzessHeader
    prozessklasse: UiBausteinReferenz | None
    leistungen: list[UiBausteinReferenz]
    downloads: list[UiBausteinReferenz]
    metadata: ProzessMetadata
    bezeichnung: str | None
    detaillierungsstufe: str | None
    prozessteilnehmende: Prozessteilnehmende
    dokumentensteckbriefe: ProzessDokumentensteckbriefe
    anwendungsgebiet: list[Anwendungsgebiet]
    handlungsgrundlagen: dict[str, list[UiHandlungsgrundlage]]
    gueltig_ab: str | None
    gueltig_bis: str | None

    visualization_tab: ProcessTab

    @staticmethod
    def from_prozess(
        prozess: Prozess, prozess_out: FullProzessOut, search_query_string: str
    ) -> "ProzessIndexPage":
        verwaltungspolitische_kodierung = prozess.get_verwaltungspolitische_kodierung()
        if len(verwaltungspolitische_kodierung) == 16:
            anwendungsgebiet = [Anwendungsgebiet.from_land(land="bund")]
        else:
            anwendungsgebiet = [
                Anwendungsgebiet.from_land(kodierung.to_label())
                for kodierung in verwaltungspolitische_kodierung
            ]

        handlungsgrundlagen: dict[str, list[UiHandlungsgrundlage]] = {}
        if prozess.prozesssteckbrief is not None:
            for handlungsgrundlage in prozess.prozesssteckbrief.handlungsgrundlage:
                label_art = handlungsgrundlage.art.to_label()
                if handlungsgrundlagen.get(label_art) is None:
                    handlungsgrundlagen[label_art] = [
                        UiHandlungsgrundlage(
                            label=handlungsgrundlage.name, href=handlungsgrundlage.uri
                        )
                    ]
                else:
                    handlungsgrundlagen[label_art].append(
                        UiHandlungsgrundlage(
                            label=handlungsgrundlage.name, href=handlungsgrundlage.uri
                        )
                    )

        downloads = [
            UiBausteinReferenz(
                id=prozess.id,
                href=f"/api/v0/processes/{prozess.id}/xprozess",
                label="Prozess (XProzess)",
            )
        ]
        if prozess.has_report_file():
            downloads.append(
                UiBausteinReferenz(
                    id=prozess.id,
                    href=f"/api/v0/processes/{prozess.id}/report",
                    label="Prozessbeschreibung",
                )
            )
        if prozess.has_visualization_file():
            downloads.append(
                UiBausteinReferenz(
                    id=prozess.id,
                    href=f"/api/v0/processes/{prozess.id}/visualization",
                    label="Visualisierung",
                )
            )

        leistungen = (
            [
                UiBausteinReferenz(
                    id=prozess_out.leistungssteckbrief.leistungsschluessel,
                    href=f"/services/{prozess_out.leistungssteckbrief.leistungsschluessel}",
                    label=prozess_out.leistungssteckbrief.name,
                )
            ]
            if prozess_out.leistungssteckbrief is not None
            and prozess_out.leistungssteckbrief.name is not None
            else []
        )
        return ProzessIndexPage(
            header=ProzessHeader.create(prozess, "index", search_query_string),
            prozessklasse=UiBausteinReferenz(
                id=prozess_out.prozessklasse.id,
                href=f"/processclasses/{prozess_out.prozessklasse.id}",
                label=prozess_out.prozessklasse.name,
            )
            if prozess_out.prozessklasse is not None
            else None,
            leistungen=leistungen,
            metadata=ProzessMetadata.create(
                prozess.zustandsangaben, prozess.fachlich_freigebende_stelle
            ),
            downloads=downloads,
            bezeichnung=prozess.bezeichnung,
            detaillierungsstufe=prozess.detaillierungsstufe.to_label()
            if prozess.detaillierungsstufe is not None
            else None,
            prozessteilnehmende=Prozessteilnehmende.from_steckbrief(
                prozess.prozesssteckbrief
            ),
            dokumentensteckbriefe=ProzessDokumentensteckbriefe.from_references(
                prozess_out.dokumentsteckbriefe
            ),
            anwendungsgebiet=anwendungsgebiet,
            handlungsgrundlagen=handlungsgrundlagen,
            gueltig_ab=prozess.gueltig_ab.strftime("%d.%m.%Y")
            if prozess.gueltig_ab is not None
            else None,
            gueltig_bis=prozess.gueltig_bis.strftime("%d.%m.%Y")
            if prozess.gueltig_bis is not None
            else None,
            visualization_tab=ProcessTab(
                id="visualization",
                label="Visualisierung",
                icon="bi-diagram-2-fill",
                active_tab="visualization",
                base_href=f"/processes/{prozess.id}",
                search_query_string=search_query_string,
            ),
        )


@dataclass(slots=True)
class ProzessVisualizationPage:
    header: ProzessHeader
    visualisation_file_href: str | None
    baustein_datenfelder_referenzen: list[UiBausteinReferenz | str]

    @staticmethod
    def from_prozess(
        prozess: Prozess, prozess_out: FullProzessOut, search_query_string: str
    ) -> "ProzessVisualizationPage":
        baustein_datenfelder_referenzen: list[UiBausteinReferenz | str] = []
        if prozess.prozessstrukturbeschreibung:
            for gruppe in prozess.prozessstrukturbeschreibung.strukturbeschreibung_fim.aktivitaetengruppe:
                for datum in gruppe.eigehende_daten:
                    if datum.formularverweis is not None:
                        ref = prozess_out.dokumentsteckbriefe.get(
                            datum.formularverweis.formularsteckbrief_id
                        )
                        if (
                            ref is not None
                            and ref.rolle == DokumentsteckbriefRolle.EINGEHENDE_DATEN
                        ):
                            baustein_datenfelder_referenzen.append(
                                UiBausteinReferenz(
                                    id=ref.fim_id,
                                    href=f"/document-profiles/{ref.fim_id}/latest",
                                    label=ref.name or ref.fim_id,
                                )
                            )
                        else:
                            baustein_datenfelder_referenzen.append(
                                datum.formularverweis.formularsteckbrief_id
                            )
                for datum in gruppe.ausgehende_daten:
                    if datum.formularverweis is not None:
                        ref = prozess_out.dokumentsteckbriefe.get(
                            datum.formularverweis.formularsteckbrief_id
                        )
                        if (
                            ref is not None
                            and ref.rolle == DokumentsteckbriefRolle.AUSGEHENDE_DATEN
                        ):
                            baustein_datenfelder_referenzen.append(
                                UiBausteinReferenz(
                                    id=ref.fim_id,
                                    href=f"/document-profiles/{ref.fim_id}/latest",
                                    label=ref.name or ref.fim_id,
                                )
                            )
                        else:
                            baustein_datenfelder_referenzen.append(
                                datum.formularverweis.formularsteckbrief_id
                            )

        return ProzessVisualizationPage(
            header=ProzessHeader.create(
                prozess=prozess,
                active_tab="visualization",
                search_query_string=search_query_string,
            ),
            visualisation_file_href=f"/api/v0/processes/{prozess.id}/visualization_display"
            if prozess.has_visualization_file()
            else None,
            baustein_datenfelder_referenzen=baustein_datenfelder_referenzen,
        )


@dataclass(slots=True)
class ProzessReferencesPage:
    header: ProzessHeader

    @staticmethod
    def from_prozess(
        prozess: Prozess, search_query_string: str
    ) -> "ProzessReferencesPage":
        return ProzessReferencesPage(
            header=ProzessHeader.create(
                prozess=prozess,
                active_tab="references",
                search_query_string=search_query_string,
            ),
        )


def create_process_router(
    get_service: Callable[[], AsyncGenerator[Service, Any]],
    render: RenderFunction,
) -> APIRouter:
    router = APIRouter()

    def _process_not_found(request: Request, message: str):
        return render(
            request,
            "404.html.jinja",
            {
                "message": message,
            },
            status_code=status.HTTP_404_NOT_FOUND,
        )

    @router.get(
        "/{prozess_id}",
        response_class=HTMLResponse,
    )
    async def get_process_detail(  # type: ignore [reportUnusedFunction]
        request: Request,
        prozess_id: str,
        service: Service = Depends(get_service),
    ):
        prozess_content = await service.get_prozess_xml(prozess_id)
        if prozess_content is None:
            return _process_not_found(
                request,
                message=f"Prozess {prozess_id} konnte nicht gefunden werden.",
            )
        prozess = parse_prozess(prozess_content)
        prozess_out = await service.get_prozess(prozess_id)
        assert prozess_out is not None

        search_query_string = create_clean_query_string(
            request,
            Resource.SAMPLEPROCESS
            if prozess.detaillierungsstufe == Detaillierungsstufe.MUSTERINFORMATIONEN
            else Resource.PROCESS,
        )

        prozess_ui = ProzessIndexPage.from_prozess(
            prozess, prozess_out, search_query_string
        )

        return render(
            request,
            "process_detail.html.jinja",
            {
                "titleTag": f"{prozess.name} • Prozesse • FIM Portal",
                "prozess": prozess_ui,
            },
        )

    @router.get(
        "/{prozess_id}/visualization",
        response_class=HTMLResponse,
    )
    async def get_process_visualization(  # type: ignore [reportUnusedFunction]
        request: Request,
        prozess_id: str,
        service: Service = Depends(get_service),
    ):
        prozess_content = await service.get_prozess_xml(prozess_id)
        if prozess_content is None:
            return _process_not_found(
                request,
                message=f"Prozess {prozess_id} konnte nicht gefunden werden.",
            )
        prozess = parse_prozess(prozess_content)
        prozess_out = await service.get_prozess(prozess_id)
        assert prozess_out is not None

        search_query_string = create_clean_query_string(
            request,
            Resource.SAMPLEPROCESS
            if prozess.detaillierungsstufe == Detaillierungsstufe.MUSTERINFORMATIONEN
            else Resource.PROCESS,
        )

        prozess_visualization = ProzessVisualizationPage.from_prozess(
            prozess, prozess_out, search_query_string
        )

        return render(
            request,
            "process_visualization.html.jinja",
            {
                "titleTag": f"{prozess.name} • Prozesse • FIM Portal",
                "prozess": prozess_visualization,
            },
        )

    return router
