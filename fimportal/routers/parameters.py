from dataclasses import dataclass
from datetime import date, datetime
from typing import Annotated

from fastapi import Path, Query

from fimportal import xzufi
from fimportal.common import Anwendungsgebiet, FreigabeStatus
from fimportal.database import (
    FeldSucheIn,
    GruppeSucheIn,
    LeistungSucheIn,
    LeistungsbeschreibungSearchOrder,
    LeistungssteckbriefSearchOrder,
    SchemaSearchOrder,
    SchemaSucheIn,
    SteckbriefSucheIn,
)
from fimportal.models.imports import XdfVersion
from fimportal.xdatenfelder import xdf3
from fimportal.xprozesse.prozess import (
    Detaillierungsstufe,
    Handlungsform,
    OperativesZiel,
    Verfahrensart,
)
from fimportal.xzufi.common import LeistungHierarchyType


@dataclass
class ErrorMessage:
    detail: str


SchemaId = Annotated[
    str, Path(description="The ID of a Datenschema.", examples=["S07000009"])
]
SchemaVersion = Annotated[
    str, Path(description="The version of a Datenschema.", examples=["1.0"])
]
CodeListUri = Annotated[
    str,
    Path(
        description="The genericode `canonicalVersionUri` of a code list.",
        examples=[
            "urn:de:fim:codeliste:natuerliche-person-juristische-person-personenhandelsgesellschaft_2021-10-07"
        ],
    ),
]


NameFilter = Annotated[
    str | None,
    Query(
        alias="name",
        title="Name",
        description="Filter by name",
    ),
]

NummernkreisFilter = Annotated[
    str | None,
    Query(
        title="Nummernkreis",
        description=(
            "Filter by `nummernkreis`. This will match all entries where the Nummernkreis starts with the "
            "provided search pattern. A search pattern 01 or 01000 yields all entries of Nummernkreis 01000."
        ),
        examples=["01000", "01"],
    ),
]

FreigabeStatusFilter = Annotated[
    list[FreigabeStatus] | None,
    Query(
        alias="freigabe_status",
        title="Freigabestatus",
        description=(
            "Filter by multiple `freigabe_status`."
            "See the [code list](https://www.xrepository.de/details/urn:xoev-de:xprozess:codeliste:status) for a description of the available values."
        ),
        examples=[
            None,
            [xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value],
            [
                xdf3.FreigabeStatus.ENTWURF.value,
                xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER.value,
                xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value,
            ],
        ],
    ),
]

LeistungFtsSucheIn = Annotated[
    LeistungSucheIn | None,
    Query(
        alias="suche_nur_in",
        title="Suche nur in",
        description="Beschränkt die Fulltext-Search nur auf das angegebene Modul.",
        examples=[None, LeistungSucheIn.LEISTUNGSBEZEICHNUNG],
    ),
]

SteckbriefFtsSucheIn = Annotated[
    SteckbriefSucheIn | None,
    Query(
        alias="suche_nur_in",
        title="Suche nur in",
        description="Beschränkt die Fulltext-Search nur auf das angegebene Modul.",
        examples=[None, SteckbriefSucheIn.RECHTSGRUNDLAGEN],
    ),
]

SchemaFtsSucheIn = Annotated[
    SchemaSucheIn | None,
    Query(
        alias="suche_nur_in",
        title="Suche nur in",
        description="Beschränkt die Fulltext-Search nur auf das angegebene Modul.",
        examples=[None, SchemaSucheIn.RECHTSGRUNDLAGEN],
    ),
]

GruppeFtsSucheIn = Annotated[
    GruppeSucheIn | None,
    Query(
        alias="suche_nur_in",
        title="Suche nur in",
        description="Beschränkt die Fulltext-Search nur auf das angegebene Modul.",
        examples=[None, GruppeSucheIn.RECHTSGRUNDLAGEN],
    ),
]

FeldFtsSucheIn = Annotated[
    FeldSucheIn | None,
    Query(
        alias="suche_nur_in",
        title="Suche nur in",
        description="Beschränkt die Fulltext-Search nur auf das angegebene Modul.",
        examples=[None, FeldSucheIn.RECHTSGRUNDLAGEN],
    ),
]

FreigabeStatusKatalogFilter = Annotated[
    FreigabeStatus | None,
    Query(
        alias="freigabe_status_katalog",
        title="Freigabestatus",
        description=(
            "Filter by `freigabe_status_katalog`. "
            "See the [code list](https://www.xrepository.de/details/urn:xoev-de:xprozess:codeliste:status) for a description of the available values."
        ),
        examples=[None, xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value],
    ),
]

FreigabeStatusBibliothekFilter = Annotated[
    FreigabeStatus | None,
    Query(
        alias="freigabe_status_bibliothek",
        title="Freigabestatus",
        description=(
            "Filter by `freigabe_status_bibliothek`. "
            "See the [code list](https://www.xrepository.de/details/urn:xoev-de:xprozess:codeliste:status) for a description of the available values."
        ),
        examples=[None, xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value],
    ),
]

StatusGesetztSeitFilter = Annotated[
    date | None,
    Query(
        alias="status_gesetzt_seit",
        title="Status gesetzt seit",
        description="Filter by `status_gesetzt_seit`. When set only schemas where there status was set after the parameter are returned.",
        examples=[None, "2022-01-01"],
    ),
]

StatusGesetztBisFilter = Annotated[
    date | None,
    Query(
        alias="status_gesetzt_bis",
        title="Status gesetzt bis",
        description="Filter by `status_gesetzt_bis`. When set only schemas where there status was set before the parameter are returned.",
        examples=["2022-01-01"],
    ),
]

GueltigAmFilter = Annotated[
    date | None,
    Query(
        alias="gueltig_am",
        title="Gültig am",
        description="Filter by `gueltig_am`. When set only schemas that are valid on the given date are returned.",
        examples=["2022-01-01"],
    ),
]

UpdatedSinceFilter = Annotated[
    datetime | None,
    Query(
        alias="updated_since",
        title="Letzte Änderung",
        description="Filter by last update timestamp. This must be a valid `iso8601` datetime.",
        examples=["2023-10-17T16:50:40.859986"],
    ),
]

StatusGesetztDurchFilter = Annotated[
    str | None,
    Query(
        alias="status_gesetzt_durch",
        title="Status gesetzt durch",
        description="Filter by `status_gesetzt_durch` (`fachlicher_ersteller` for XDF2)",
    ),
]

BezugFilter = Annotated[
    str | None,
    Query(
        alias="bezug",
        title="Bezug",
        description="Filter by `bezug`",
        examples=["Test"],
    ),
]

BezugUnterelementeFilter = Annotated[
    str | None,
    Query(
        alias="bezug_unterelemente",
        title="Bezug Unterelemente",
        description="Filter by bezug of the subelements of a schema",
        examples=["Test"],
    ),
]

BezeichnungFilter = Annotated[
    str | None,
    Query(
        alias="bezeichnung",
        title="Bezeichnung",
        description="Filter by `bezeichnung` (`bezeichnung_eingabe` in xdf2)",
        examples=["Test"],
    ),
]

StichwortFilter = Annotated[
    str | None,
    Query(
        alias="stichwort",
        title="Stichwort",
        description="Filter by `stichwort` (XDF3 only)",
        examples=["Anwendungsgebiet::Bundesrepublik"],
    ),
]

VersionshinweisFilter = Annotated[
    str | None,
    Query(
        alias="Versionshinweis",
        description="Filter by `versionshinweis`",
        examples=["Hinweis"],
    ),
]

DokumentArtFilter = Annotated[
    xdf3.Dokumentart | None,
    Query(
        alias="dokumentart",
        title="Dokumentart",
        description="Filter by `dokumentart`. See the [code list](https://www.xrepository.de/details/urn:xoev-de:fim-datenfelder:codeliste:dokumentart) for details.",
        examples=["999"],
    ),
]

XdfVersionFilter = Annotated[
    XdfVersion | None,
    Query(
        alias="xdf_version",
        title="Xdf Version",
        description="Filter by `xdf_version`",
        examples=["3.0.0"],
    ),
]

LeistungLeistungsschluesselFilter = Annotated[
    str | None,
    Query(
        alias="leistungsschluessel",
        title="Leistungsschluessel",
        description="Filter by partial Leistungsschluessel.",
    ),
]

LeistungRedaktionsIdFilter = Annotated[
    str | None,
    Query(
        alias="redaktion_id",
        title="Redaktions-ID",
        description="Filter by a specific Redaktion.",
    ),
]

LeistungRechtsgrundlagenFilter = Annotated[
    str | None,
    Query(
        alias="rechtsgrundlagen",
        title="Rechtsgrundlagen",
        description="Filter by substring in the text module for Rechtsgrundlagen.",
    ),
]

LeistungsbezeichnungFilter = Annotated[
    str | None,
    Query(
        alias="leistungsbezeichnung",
        title="Leistungsbezeichnung",
        description="Filter by substring in the text module for Leistungsbezeichnung.",
    ),
]
Leistungsbezeichnung2Filter = Annotated[
    str | None,
    Query(
        alias="leistungsbezeichnung2",
        title="Leistungsbezeichnung II",
        description="Filter by substring in the text module for Leistungsbezeichnung II.",
    ),
]

LeistungEinheitlicherAnsprechpartnerFilter = Annotated[
    bool | None,
    Query(
        alias="einheitlicher_ansprechpartner",
        title="Einheitlicher Ansprechpartner",
        description="Filter for Einheitlicher Ansprechpartner (kennzeichen_ea)",
    ),
]


LeistungTypFilter = Annotated[
    xzufi.leistung.LeistungsTyp | None,
    Query(
        alias="leistungstyp",
        title="Leistungstyp",
        description="Filter by 'Leistungstyp'.",
    ),
]

LeistungTypisierungFilter = Annotated[
    xzufi.leistung.LeistungsTypisierung | None,
    Query(
        alias="typisierung",
        title="Typisierung",
        description="Filter by 'Leistungstypisierung'.",
    ),
]

LeistungOrderBy = Annotated[
    LeistungssteckbriefSearchOrder,
    Query(
        alias="order_by",
        title="Order By",
        description="Select the order of the results.",
    ),
]

LeistungsbeschreibungOrderBy = Annotated[
    LeistungsbeschreibungSearchOrder,
    Query(
        alias="order_by",
        title="Order By",
        description="Select the order of the results.",
    ),
]

SchemaOrderBy = Annotated[
    SchemaSearchOrder,
    Query(
        alias="order_by",
        title="Order By",
        description="Select the order of the results.",
    ),
]

MusterprozessQueryFilter = Annotated[
    bool | None,
    Query(
        alias="is_musterprozess",
        description="Filters for Musterprozesse or Non-Musterprozesse",
    ),
]
FtsQueryFilter = Annotated[
    str | None,
    Query(
        alias="fts_query",
        description="Full-Text-Search query",
        examples=["Test"],
    ),
]

LeistungTitleFilter = Annotated[
    str | None,
    Query(
        alias="title",
        title="Title",
        description="Filter by title. This value is either identical to 'Bezeichnung', or 'Bezeichnung 2' if the former does not exist. If both are empty, the title will be 'Kein Titel'.",
    ),
]

LeistungHierarchyTypeFilter = Annotated[
    LeistungHierarchyType | None,
    Query(
        alias="hierarchy_type",
        title="Hierarchy Type",
        description="Filter by `hierarchy_type`. This filters whether the xzufi service is a 'Steckbrief' or a 'Leistungsbeschreibung'.",
        examples=[None, LeistungHierarchyType.BESCHREIBUNG],
    ),
]

FeldartFilter = Annotated[
    xdf3.Feldart | None,
    Query(
        alias="feldart",
        title="Feldart",
        description=(
            "Filter by `feldart`. "
            "See the [specification](https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xdatenfelder_3.0.0:dokument:XDatenfelder_3.0.0_Spezifikation), pp. 42, 64, for a description of the available values."
        ),
        examples=[None, xdf3.Feldart.EINGABE],
    ),
]

DatentypFilter = Annotated[
    xdf3.Datentyp | None,
    Query(
        alias="datentyp",
        title="Datentyp",
        description=(
            "Filter by `datentyp`. "
            "See the [specification](https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xdatenfelder_3.0.0:dokument:XDatenfelder_3.0.0_Spezifikation), pp. 42, 65, for a description of the available values."
        ),
        examples=[None, xdf3.Datentyp.TEXT],
    ),
]

IsLatestFilter = Annotated[
    bool | None,
    Query(
        alias="is_latest",
        title="Is latest",
        description=(
            "Filter for results which are the latest `fim_version` of their kind. "
        ),
        examples=["true", "false"],
    ),
]

OperativesZielFilter = Annotated[
    OperativesZiel | None,
    Query(
        alias="operatives_ziel",
        title="Operatives Ziel",
        description=(
            "Filter by `operatives_ziel`. "
            "See the [code list](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:operativesziel) for a description of the available values."
        ),
        examples=[None, OperativesZiel.ARBEIT_DER_VERWALTUNG.value],
    ),
]

VerfahrensartFilter = Annotated[
    Verfahrensart | None,
    Query(
        alias="verfahrensart",
        title="Verfahrensart",
        description=(
            "Filter by `verfahrensart`. "
            "See the [code list](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:verfahrensart) for a description of the available values."
        ),
        examples=[None, Verfahrensart.ANFECHTUNGSKLAGEVERFAHREN_FGO.value],
    ),
]

HandlungsformFilter = Annotated[
    Handlungsform | None,
    Query(
        alias="handlungsform",
        title="Handlungsform",
        description=(
            "Filter by `handlungsform`. "
            "See the [code list](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:handlungsform) for a description of the available values."
        ),
        examples=[None, Handlungsform.BESCHLUSS.value],
    ),
]

DetaillierungsstufeFilter = Annotated[
    Detaillierungsstufe | None,
    Query(
        alias="detaillierungsstufe",
        title="Detaillierungsstufe",
        description=(
            "Filter by `detaillierungsstufe`. "
            "See the [code list](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:detaillierungsstufe) for a description of the available values."
        ),
        examples=[None, Handlungsform.BESCHLUSS.value],
    ),
]


AnwendungsgebietFilter = Annotated[
    Anwendungsgebiet | None,
    Query(
        alias="anwendungsgebiet",
        title="Anwendungsgebiet",
        description=(
            "Filter by `anwendungsgebiet`. "
            f"See the [code list](https://www.xrepository.de/details/urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:bundesland) for a description of the available values. Use value {Anwendungsgebiet.BUND.value} for the Bund."
        ),
        examples=[None, Handlungsform.BESCHLUSS.value],
    ),
]
