from __future__ import annotations

from dataclasses import dataclass
from typing import Any, AsyncGenerator, Callable

from fastapi import APIRouter, Depends, Request, Response, status
from fastapi.responses import RedirectResponse
from starlette.responses import HTMLResponse

from fimportal.cms import (
    ArticlePage,
    CMSContent,
    FileIdentifier,
    SeminarPage,
    create_news_rss_feed,
)
from fimportal.helpers import RenderFunction
from fimportal.kataloge import XzufiKatalogIdentifier
from fimportal.routers.ui_redirect import (
    create_redirect_router,
)
from fimportal.routers.ui_processclass import create_processclass_router
from fimportal.service import Service

from .ui_document_profiles import create_document_profile_router
from .ui_fields import create_field_router
from .ui_groups import create_group_router
from .ui_processes import create_process_router
from .ui_schemas import create_schema_router
from .ui_search import create_search_router
from .ui_service_profiles import create_service_profile_router
from .ui_service_descriptions import create_service_description_router


@dataclass(slots=True)
class KatalogLink:
    is_download: bool
    label: str
    href: str

    @staticmethod
    def katalog_download(label: str, katalog: XzufiKatalogIdentifier) -> KatalogLink:
        return KatalogLink(
            is_download=True,
            label=label,
            href=f"/kataloge/{katalog.get_source_filename()}.zip",
        )

    @staticmethod
    def file_download(label: str, file: FileIdentifier) -> KatalogLink:
        return KatalogLink(
            is_download=True,
            label=label,
            href=file.to_link(),
        )

    @staticmethod
    def link(label: str, href: str) -> KatalogLink:
        return KatalogLink(
            is_download=False,
            label=label,
            href=href,
        )


@dataclass(slots=True)
class KatalogEntry:
    title: str
    description: str | None
    links: list[KatalogLink]


@dataclass(slots=True)
class KatalogEntries:
    leistungen_csv: list[KatalogEntry]
    leistungen_xzufi: list[KatalogEntry]
    datenfelder: list[KatalogEntry]
    prozesse: list[KatalogEntry]


KATALOG_ENTRIES = KatalogEntries(
    leistungen_csv=[
        KatalogEntry(
            title="Alle Leistungen",
            description="Leistungsschlüssel (LSL) Bezeichnung Synonyme",
            links=[
                KatalogLink.katalog_download(
                    "Deutsch", katalog=XzufiKatalogIdentifier.LEISTUNGEN_DE
                ),
                KatalogLink.katalog_download(
                    "Englisch", katalog=XzufiKatalogIdentifier.LEISTUNGEN_EN
                ),
            ],
        ),
        KatalogEntry(
            title="LeiKa-plus mit HTML",
            description="Alle Leistungen inkl. inhaltlicher Beschreibungen mit HTML",
            links=[
                KatalogLink.katalog_download(
                    "Deutsch", katalog=XzufiKatalogIdentifier.LEIKA_PLUS_DE
                ),
                KatalogLink.katalog_download(
                    "Englisch", katalog=XzufiKatalogIdentifier.LEIKA_PLUS_EN
                ),
                KatalogLink.katalog_download(
                    "Deutsch leichte Sprache",
                    katalog=XzufiKatalogIdentifier.LEIKA_PLUS_DE_LS,
                ),
            ],
        ),
        KatalogEntry(
            title="LeiKa-plus ohne HTML",
            description="Alle Leistungen inkl. inhaltlicher Beschreibungen ohne HTML",
            links=[
                KatalogLink.katalog_download(
                    "Deutsch", katalog=XzufiKatalogIdentifier.LEIKA_PLUS_OHNE_HTML_DE
                ),
                KatalogLink.katalog_download(
                    "Englisch", katalog=XzufiKatalogIdentifier.LEIKA_PLUS_OHNE_HTML_EN
                ),
                KatalogLink.katalog_download(
                    "Deutsch leichte Sprache",
                    katalog=XzufiKatalogIdentifier.LEIKA_PLUS_OHNE_HTML_DE_LS,
                ),
            ],
        ),
        KatalogEntry(
            title="Leistungsobjekte",
            description="Leistungsschlüssel (LSL) Bezeichnung",
            links=[
                KatalogLink.katalog_download(
                    "Deutsch", katalog=XzufiKatalogIdentifier.LEISTUNGSOBJEKTE_DE
                ),
                KatalogLink.katalog_download(
                    "Englisch", katalog=XzufiKatalogIdentifier.LEISTUNGSOBJEKTE_EN
                ),
            ],
        ),
        KatalogEntry(
            title="Verrichtungskennungen",
            description=None,
            links=[
                KatalogLink.katalog_download(
                    "Deutsch", katalog=XzufiKatalogIdentifier.VERRICHUNGEN_DE
                ),
                KatalogLink.katalog_download(
                    "Englisch", katalog=XzufiKatalogIdentifier.VERRICHUNGEN_EN
                ),
            ],
        ),
    ],
    leistungen_xzufi=[
        KatalogEntry(
            title="Leika-plus",
            description="Alle Informationen zu Leistungen aus dem LeiKa (de/en)",
            links=[
                KatalogLink.katalog_download(
                    "XZuFi 2.2.0",
                    katalog=XzufiKatalogIdentifier.LEISTUNGEN_XZUFI_2_2_0,
                ),
                KatalogLink.katalog_download(
                    "XZuFi 2.1.0",
                    katalog=XzufiKatalogIdentifier.LEISTUNGEN_XZUFI_2_1_0,
                ),
                KatalogLink.katalog_download(
                    "XZuFi 1.0",
                    katalog=XzufiKatalogIdentifier.LEISTUNGEN_XZUFI_1_0_0,
                ),
            ],
        ),
        KatalogEntry(
            title="Onlinedienst",
            description="Alle Informationen zu Onlinediensten aus dem LeiKa (de/en)",
            links=[
                KatalogLink.katalog_download(
                    "XZuFi 2.2.0",
                    katalog=XzufiKatalogIdentifier.ONLINEDIENSTE_XZUFI_2_2_0,
                )
            ],
        ),
        KatalogEntry(
            title="Organisationseinheiten",
            description="Alle Informationen zu Organisationseinheiten aus dem LeiKa (de/en)",
            links=[
                KatalogLink.katalog_download(
                    "XZuFi 2.2.0",
                    katalog=XzufiKatalogIdentifier.ORGANISATIONSEINHEITEN_XZUFI_2_2_0,
                )
            ],
        ),
        KatalogEntry(
            title="Verrichtungskennungen",
            description=None,
            links=[
                KatalogLink.katalog_download(
                    "XZuFi 2.2.0",
                    katalog=XzufiKatalogIdentifier.VERRICHTUNGEN_XZUFI_2_2_0,
                ),
                KatalogLink.katalog_download(
                    "XZuFi 2.1.0",
                    katalog=XzufiKatalogIdentifier.VERRICHTUNGEN_XZUFI_2_1_0,
                ),
                KatalogLink.katalog_download(
                    "XZuFi 1.0",
                    katalog=XzufiKatalogIdentifier.VERRICHTUNGEN_XZUFI_1_0_0,
                ),
            ],
        ),
        KatalogEntry(
            title="Gelöschte Leistungen",
            description=None,
            links=[
                KatalogLink.katalog_download(
                    "XZuFi 2.2.0",
                    katalog=XzufiKatalogIdentifier.LEISTUNGEN_GELOESCHT_XZUFI_2_2_0,
                )
            ],
        ),
        KatalogEntry(
            title="Zur Löschung vorgesehene Leistungen",
            description=None,
            links=[
                KatalogLink.katalog_download(
                    "XZuFi 2.2.0",
                    katalog=XzufiKatalogIdentifier.LEISTUNGEN_ZU_LOESCHEN_XZUFI_2_2_0,
                )
            ],
        ),
    ],
    datenfelder=[
        KatalogEntry(
            title="BOB - harmonisierte Baukastenelemente",
            description=None,
            links=[
                KatalogLink.link(
                    "Anzeigen",
                    href="/schemas/S60000011/latest",
                )
            ],
        )
    ],
    prozesse=[
        KatalogEntry(
            title="FIM Prozesskatalog",
            description="Download im XLSX Format",
            links=[
                KatalogLink.file_download(
                    "Herunterladen",
                    file=FileIdentifier.PROZESSE_KATALOG,
                )
            ],
        )
    ],
)


def create_router(
    get_service: Callable[[], AsyncGenerator[Service, Any]],
    cms_content: CMSContent,
    baseurl: str,
    render: RenderFunction,
) -> APIRouter:
    router = APIRouter()

    def _not_found(request: Request, message: str):
        return render(
            request,
            "404.html.jinja",
            {
                "message": message,
            },
            status_code=status.HTTP_404_NOT_FOUND,
        )

    @router.get("/", response_class=HTMLResponse)
    async def _index(request: Request):  # type: ignore [reportUnusedFunction]
        return render(
            request,
            "index.html.jinja",
            context={
                "titleTag": cms_content.index.title,
                "news": cms_content.articles[:3],
                "page": cms_content.index,
            },
        )

    @router.get("/ueber-fim", response_class=HTMLResponse)
    async def _ueber_fim(request: Request):  # type: ignore [reportUnusedFunction]
        return render(
            request,
            "ueber_fim.html.jinja",
            context={
                "titleTag": cms_content.was_ist_fim.title,
                "example_steps": cms_content.fim_user_journey,
                "page": cms_content.was_ist_fim,
            },
            active_tab="ueber-fim",
        )

    @router.get("/kataloge", response_class=HTMLResponse)
    async def _kataloge(request: Request):  # type: ignore [reportUnusedFunction]
        return render(
            request,
            "kataloge.html.jinja",
            context={
                "titleTag": cms_content.kataloge.title,
                "page": cms_content.kataloge,
                "kataloge": KATALOG_ENTRIES,
            },
            active_tab="kataloge",
        )

    @router.get("/kataloge/{filename}", response_class=HTMLResponse)
    async def _katalog(  # type: ignore [reportUnusedFunction]
        request: Request,
        filename: str,
        service: Service = Depends(get_service),
    ):
        content = await service.get_katalog_content(filename)
        if content is None:
            return _not_found(request, "Dieser Katalog existiert nicht.")

        return Response(content=content, media_type="application/zip")

    # Support for old links to the "FIM Haus"
    @router.get("/fim-haus", response_class=HTMLResponse)
    async def _fim_haus(request: Request):  # type: ignore [reportUnusedFunction]
        return RedirectResponse(url="/arbeitsmittel")

    @router.get("/arbeitsmittel", response_class=HTMLResponse)
    async def _arbeitsmittel(request: Request):  # type: ignore [reportUnusedFunction]
        return render(
            request,
            "arbeitsmittel.html.jinja",
            context={
                "titleTag": cms_content.arbeitsmittel.title,
                "page": cms_content.arbeitsmittel,
            },
            active_tab="arbeitsmittel",
        )

    @router.get("/schulung", response_class=HTMLResponse)
    async def _schulung(request: Request):  # type: ignore [reportUnusedFunction]
        return render(
            request,
            "schulung.html.jinja",
            context={
                "titleTag": cms_content.schulung.title,
                "seminare": cms_content.seminare,
                "fim_coaches": cms_content.fim_coaches,
                "page": cms_content.schulung,
            },
            active_tab="schulung",
        )

    seminar_pages = {
        seminar.identifier: SeminarPage.from_seminar(seminar)
        for seminar in cms_content.seminare
    }

    @router.get("/seminar/{seminar_id}", response_class=HTMLResponse)
    async def _seminar(request: Request, seminar_id: str):  # type: ignore [reportUnusedFunction]
        page = seminar_pages.get(seminar_id)
        if page is None:
            return _not_found(request, "Dieses Seminar existiert nicht.")

        return render(
            request,
            "seminar.html.jinja",
            context={
                "titleTag": page.title,
                "fim_coaches": cms_content.fim_coaches,
                "page": page,
            },
            active_tab="schulung",
        )

    @router.get("/news", response_class=HTMLResponse)
    async def _news(request: Request):  # type: ignore [reportUnusedFunction]
        return render(
            request,
            "news.html.jinja",
            context={
                "titleTag": cms_content.news.title,
                "articles": cms_content.articles,
                "page": cms_content.news,
            },
            active_tab="news",
        )

    article_pages = {
        article.id: ArticlePage.from_article(article)
        for article in cms_content.articles
    }

    @router.get("/news/{article_id}", response_class=HTMLResponse)
    async def _article(request: Request, article_id: str):  # type: ignore [reportUnusedFunction]
        page = article_pages.get(article_id)
        if page is None:
            return _not_found(request, "Dieser Artikel existiert nicht.")

        return render(
            request,
            "article.html.jinja",
            context={
                "titleTag": page.title,
                "articles": cms_content.articles[:5],
                "page": page,
            },
            active_tab="news",
        )

    @router.get("/kontakt", response_class=HTMLResponse)
    async def _kontakt(request: Request):  # type: ignore [reportUnusedFunction]
        return render(
            request,
            "kontakt.html.jinja",
            context={
                "titleTag": cms_content.kontakt.title,
                "page": cms_content.kontakt,
            },
            active_tab="kontakt",
        )

    NEWS_RSS_FEED = create_news_rss_feed(cms_content, baseurl)

    @router.get("/news.rss", include_in_schema=False)
    async def render_rss_feed(request: Request):  # type: ignore [reportUnusedFunction]
        return Response(
            content=NEWS_RSS_FEED, media_type="application/rss+xml;charset=utf-8"
        )

    router.include_router(create_search_router(get_service, render), prefix="/search")
    router.include_router(create_schema_router(get_service, render), prefix="/schemas")
    router.include_router(create_group_router(get_service, render), prefix="/groups")
    router.include_router(create_field_router(get_service, render), prefix="/fields")
    router.include_router(
        create_document_profile_router(get_service, render), prefix="/document-profiles"
    )
    router.include_router(
        create_process_router(get_service, render), prefix="/processes"
    )
    router.include_router(
        create_processclass_router(get_service, render), prefix="/processclasses"
    )
    router.include_router(
        create_service_profile_router(get_service=get_service, render=render),
        prefix="/services",
    )
    router.include_router(
        create_service_description_router(get_service=get_service, render=render),
        prefix="/xzufi-services",
    )
    router.include_router(create_redirect_router(render=render))

    legal_pages = {page.identifier: page for page in cms_content.legal_pages}

    @router.get("/{legal_id}", response_class=HTMLResponse)
    async def _legal_page(request: Request, legal_id: str):  # type: ignore [reportUnusedFunction]
        legal_content = legal_pages.get(legal_id)
        if legal_content is None:
            return _not_found(request, "Diese Seite existiert nicht.")

        return render(
            request,
            "legal.html.jinja",
            context={
                "titleTag": f"{legal_content.title} • FIM Portal",
                "page": legal_content,
            },
            active_tab=None,
        )

    @router.get("{_:path}", response_class=HTMLResponse)
    async def unknown_page(request: Request):  # type: ignore [reportUnusedFunction]
        return _not_found(
            request,
            message="Diese Seite existiert nicht.",
        )

    return router
