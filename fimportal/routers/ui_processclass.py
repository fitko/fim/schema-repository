from dataclasses import dataclass
from typing import Any, AsyncGenerator, Callable

from fastapi import APIRouter, Depends, Request, status
from fastapi.responses import HTMLResponse
from fimportal.helpers import RenderFunction
from fimportal.models.prozess import FullProzessklasseOut
from fimportal.routers.common import (
    ProzessMetadata,
    Resource,
    UiBausteinReferenz,
    UiHandlungsgrundlage,
    create_clean_query_string,
)
from fimportal.service import Service
from fimportal.xprozesse.prozess import (
    Prozessklasse,
    Klassifikation,
    parse_prozessklasse,
)


@dataclass(slots=True)
class ProzessklasseHeader:
    id: str
    version: str
    name: str
    freigabe_status: str

    base_href: str
    back_url: str

    @staticmethod
    def create(
        prozessklasse: Prozessklasse, search_query_string: str
    ) -> "ProzessklasseHeader":
        return ProzessklasseHeader(
            id=prozessklasse.id,
            version=prozessklasse.version or "-",
            name=prozessklasse.name,
            freigabe_status=prozessklasse.freigabe_status.to_label()
            if prozessklasse.freigabe_status is not None
            else "unbestimmter Freigabestatus",
            base_href=f"/processclasses/{prozessklasse.id}",
            back_url=f"/search{search_query_string}",
        )


@dataclass(slots=True)
class EinordnungProzesskatalogItem:
    name: str
    id: str

    @staticmethod
    def from_klassifikation(klassifikation: Klassifikation):
        return EinordnungProzesskatalogItem(
            klassifikation.klasse_name
            if klassifikation.klasse_name is not None
            else "Unbekannt",
            klassifikation.klasse_id,
        )


@dataclass(slots=True)
class EinordnungProzesskatalog:
    first: EinordnungProzesskatalogItem | None
    second: EinordnungProzesskatalogItem | None
    third: EinordnungProzesskatalogItem | None

    @staticmethod
    def from_prozessklasse(prozessklasse: Prozessklasse) -> "EinordnungProzesskatalog":
        return EinordnungProzesskatalog(
            first=EinordnungProzesskatalogItem.from_klassifikation(
                prozessklasse.klassifikation[0]
            )
            if len(prozessklasse.klassifikation) > 0
            else None,
            second=EinordnungProzesskatalogItem.from_klassifikation(
                prozessklasse.klassifikation[1]
            )
            if len(prozessklasse.klassifikation) > 1
            else None,
            third=EinordnungProzesskatalogItem.from_klassifikation(
                prozessklasse.klassifikation[2]
            )
            if len(prozessklasse.klassifikation) > 2
            else None,
        )


@dataclass(slots=True)
class ProzessklasseIndexPage:
    header: ProzessklasseHeader

    leistungen: list[UiBausteinReferenz]
    prozesse: list[UiBausteinReferenz]

    download_prozessklasse_href: str

    metadata: ProzessMetadata

    definition: str | None
    handlungsgrundlagen: dict[str, list[UiHandlungsgrundlage]]
    gueltig_ab: str | None
    gueltig_bis: str | None
    operatives_ziel: str | None
    handlungsform: str | None
    verfahrensart: str | None
    prozessart: str | None

    einordnung_in_den_prozesskatalog: EinordnungProzesskatalog

    @staticmethod
    def from_prozessklasse(
        prozessklasse: Prozessklasse,
        prozessklasse_out: FullProzessklasseOut,
        search_query_string: str,
    ) -> "ProzessklasseIndexPage":
        leistungen = (
            [
                UiBausteinReferenz(
                    href=f"/services/{prozessklasse_out.leistungssteckbrief.leistungsschluessel}",
                    label=prozessklasse_out.leistungssteckbrief.name,
                    id=prozessklasse_out.leistungssteckbrief.leistungsschluessel,
                )
            ]
            if prozessklasse_out.leistungssteckbrief is not None
            and prozessklasse_out.leistungssteckbrief.name is not None
            else []
        )
        prozesse = [
            UiBausteinReferenz(
                id=item.id, label=item.name, href=f"/processes/{item.id}"
            )
            for item in prozessklasse_out.prozesse
        ]

        handlungsgrundlagen: dict[str, list[UiHandlungsgrundlage]] = {}
        for handlungsgrundlage in prozessklasse.handlungsgrundlage:
            label_art = handlungsgrundlage.art.to_label()
            if label_art not in handlungsgrundlagen:
                handlungsgrundlagen[label_art] = [
                    UiHandlungsgrundlage(
                        label=handlungsgrundlage.name, href=handlungsgrundlage.uri
                    )
                ]
            else:
                handlungsgrundlagen[label_art].append(
                    UiHandlungsgrundlage(
                        label=handlungsgrundlage.name, href=handlungsgrundlage.uri
                    )
                )
        return ProzessklasseIndexPage(
            header=ProzessklasseHeader.create(
                prozessklasse, search_query_string=search_query_string
            ),
            leistungen=leistungen,
            prozesse=prozesse,
            download_prozessklasse_href=f"/api/v0/processclasses/{prozessklasse.id}/xprozess",
            definition=prozessklasse.definition,
            handlungsgrundlagen=handlungsgrundlagen,
            gueltig_ab=prozessklasse.gueltig_ab.strftime("%d.%m.%Y")
            if prozessklasse.gueltig_ab is not None
            else None,
            gueltig_bis=prozessklasse.gueltig_bis.strftime("%d.%m.%Y")
            if prozessklasse.gueltig_bis is not None
            else None,
            operatives_ziel=prozessklasse.operatives_ziel.to_label()
            if prozessklasse.operatives_ziel is not None
            else None,
            handlungsform=prozessklasse.handlungsform.to_label()
            if prozessklasse.handlungsform is not None
            else None,
            verfahrensart=prozessklasse.verfahrensart.to_label()
            if prozessklasse.verfahrensart is not None
            else None,
            prozessart=prozessklasse.prozessart,
            metadata=ProzessMetadata.create(
                prozessklasse.zustandsangaben, prozessklasse.fachlich_freigebende_stelle
            ),
            einordnung_in_den_prozesskatalog=EinordnungProzesskatalog.from_prozessklasse(
                prozessklasse
            ),
        )


def create_processclass_router(
    get_service: Callable[[], AsyncGenerator[Service, Any]],
    render: RenderFunction,
) -> APIRouter:
    router = APIRouter()

    def _processclass_not_found(request: Request, message: str):
        return render(
            request,
            "404.html.jinja",
            {
                "message": message,
            },
            status_code=status.HTTP_404_NOT_FOUND,
        )

    @router.get(
        "/{prozessklasse_id}",
        response_class=HTMLResponse,
    )
    async def get_processclass_detail(  # type: ignore [reportUnusedFunction]
        request: Request,
        prozessklasse_id: str,
        service: Service = Depends(get_service),
    ):
        prozessklasse_content = await service.get_prozessklasse_xml(prozessklasse_id)
        if prozessklasse_content is None:
            return _processclass_not_found(
                request,
                message=f"Prozessklasse {prozessklasse_id} konnte nicht gefunden werden.",
            )
        prozessklasse = parse_prozessklasse(prozessklasse_content)
        prozessklasse_out = await service.get_prozessklasse(prozessklasse_id)
        assert prozessklasse_out is not None

        search_query_string = create_clean_query_string(request, Resource.PROCESSCLASS)
        prozessklasse_ui = ProzessklasseIndexPage.from_prozessklasse(
            prozessklasse, prozessklasse_out, search_query_string
        )

        return render(
            request,
            "processclass_detail.html.jinja",
            {
                "titleTag": f"{prozessklasse.name} • Prozesse • FIM Portal",
                "prozessklasse": prozessklasse_ui,
            },
        )

    return router
