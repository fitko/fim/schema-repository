import logging
from typing import Annotated, Any, AsyncGenerator, Callable

from fastapi import (
    APIRouter,
    Depends,
    File,
    Form,
    HTTPException,
    Security,
    status,
)
from fastapi.responses import ORJSONResponse
from fastapi.security import APIKeyHeader

from fimportal.database import (
    FieldSearchOptions,
    GroupSearchOptions,
    SchemaSearchOptions,
    SchemaSearchOrder,
    SteckbriefSearchOptions,
)
from fimportal.errors import (
    DatabaseDeadlockException,
    ImportException,
)
from fimportal.models.imports import (
    Xdf2SchemaImport,
    Xdf2SteckbriefImport,
    Xdf3SchemaImport,
)
from fimportal.models.xdf3 import (
    DatenfeldgruppeOut,
    DatenfeldOut,
    FullDatenfeldgruppeOut,
    FullDatenfeldOut,
    FullSchemaOut,
    FullSteckbriefOut,
    SchemaOut,
    SteckbriefOut,
)
from fimportal.pagination import (
    PaginatedResult,
    PaginationOptions,
    get_pagination_options,
)
from fimportal.routers.immutable import FileResponse, XmlFileResponse
from fimportal.xdatenfelder import xdf2, xdf3
from fimportal.xdatenfelder.reports import QualityReport
from fimportal.xdatenfelder.xdf3.steckbrief_message import parse_steckbrief_message

from ..service import Access, Service
from .parameters import (
    BezeichnungFilter,
    BezugFilter,
    BezugUnterelementeFilter,
    DatentypFilter,
    DokumentArtFilter,
    ErrorMessage,
    FeldartFilter,
    FreigabeStatusFilter,
    FtsQueryFilter,
    SteckbriefFtsSucheIn,
    SchemaFtsSucheIn,
    GruppeFtsSucheIn,
    FeldFtsSucheIn,
    GueltigAmFilter,
    IsLatestFilter,
    NameFilter,
    NummernkreisFilter,
    SchemaId,
    SchemaOrderBy,
    SchemaVersion,
    StatusGesetztBisFilter,
    StatusGesetztDurchFilter,
    StatusGesetztSeitFilter,
    StichwortFilter,
    UpdatedSinceFilter,
    VersionshinweisFilter,
    XdfVersionFilter,
)

logger = logging.getLogger(__name__)


AccessTokenHeader = APIKeyHeader(name="Access-Token", auto_error=False)


def create_router(
    get_service: Callable[[], AsyncGenerator[Service, Any]],
) -> APIRouter:
    router = APIRouter()

    async def check_access_token(
        access_token: Annotated[str | None, Depends(AccessTokenHeader)],
        service: Service = Depends(get_service),
    ) -> Access:
        if access_token is None:
            access = None
        else:
            access = await service.check_api_token(access_token)

        if access is None:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="invalid access token",
            )

        return access

    def check_access_to_upload(
        nummernkreis: xdf3.Nummernkreis,
        access: Access,
    ) -> None:
        if nummernkreis != access.nummernkreis:
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail=f"missing authorization: token can only upload for Nummernkreis {access.nummernkreis}, the schema is for Nummernkreis {nummernkreis}.",
            )

    @router.get(
        "/token/access",
        name="Check the access rights of an access token",
        response_class=ORJSONResponse,
        responses={
            401: {
                "model": ErrorMessage,
                "description": "Invalid Access Token",
            },
        },
    )
    async def check_token(  # type: ignore [reportUnusedFunction]
        access: Annotated[Access, Depends(check_access_token)],
    ) -> Access:
        return access

    @router.post(
        "/schemas/xdf2",
        name="Upload an xdf2 schema",
        response_class=ORJSONResponse,
        responses={
            400: {
                "model": ErrorMessage,
                "description": "Import Error",
            },
            401: {
                "model": ErrorMessage,
                "description": "Invalid Access Token",
            },
            403: {
                "model": ErrorMessage,
                "description": "No access to Nummernkreis",
            },
            503: {
                "model": ErrorMessage,
                "description": "Another process is currently updating this resource. Try again.",
            },
        },
    )
    async def _upload_xdf2_schema(  # type: ignore [reportUnusedFunction]
        access: Annotated[Access, Depends(check_access_token)],
        # NOTE: This must not be called `schema`, as this is a reserved keyword in pydantic 2.
        schema_file: Annotated[
            bytes,
            File(
                title="Schema",
                alias="schema",
                # NOTE: `validation_alias` must be set additionally, as fastapi has a bug where the alias
                # is not respected when creating the OpenAPI Docs.
                validation_alias="schema",
                description="The `Datenschema` (must be `application/xml`).",
                media_type="application/xml",
            ),
        ],
        steckbrief_id: Annotated[
            str,
            Form(
                title="Steckbrief Id",
                description="The FIM Id of the corresponding `Dokumentsteckbrief`.",
                examples=["D17000436"],
            ),
        ],
        freigabe_status: Annotated[
            xdf3.FreigabeStatus,
            Form(
                title="Freigabestatus",
                description="The `freigabe_status` of the `Datenschema`.",
            ),
        ],
        code_lists: Annotated[
            list[bytes],
            File(
                title="Codelists",
                description="One or multiple files containing the necessary code lists (must be `application/xml`).",
                media_type="application/xml",
            ),
        ] = [],
        service: Service = Depends(get_service),
    ) -> FullSchemaOut:
        """
        This upload needs additional parameters besides the schema itself:

        - **steckbrief_id**: The ID of the corresponding `Dokumentsteckbrief`.
        - **freigabe_status**: The `Freigabestatus` of the schema as specified in the new standard xdf3.
        - **code_lists**: The genericode file of included code lists, if any.

        Include the API Key in the Header `Access-Token`.
        """
        code_list_data = [data for data in code_lists if data != b""]

        try:
            xdf2_import = Xdf2SchemaImport.from_bytes(
                schema_file,
                code_list_data=code_list_data,
                freigabe_status=freigabe_status,
                steckbrief_id=steckbrief_id,
            )
        except ImportException as ex:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=str(ex),
            ) from ex

        # TODO(Felix): Find a better place for this conversion
        xdf2_nummernkreis = xdf2_import.schema_message.schema.identifier.nummernkreis
        xdf3_nummernkreis = xdf3.Nummernkreis(f"{xdf2_nummernkreis}000")
        check_access_to_upload(xdf3_nummernkreis, access)

        logger.info(f"Uploading new schema for Nummernkreis {access.nummernkreis}")

        try:
            await service.import_xdf2_schema(xdf2_import, strict=False)
        except ImportException as error:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=str(error),
            ) from error
        except DatabaseDeadlockException as error:
            raise HTTPException(
                status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                detail=str(error),
                headers={"Retry-After": "0"},
            ) from error

        result = await service.get_schema(xdf2_import.id, xdf2_import.version)
        assert result is not None
        return result

    @router.post(
        "/schemas/xdf3",
        name="Upload an xdf3 schema",
        response_class=ORJSONResponse,
        responses={
            400: {
                "model": ErrorMessage,
                "description": "Import Error",
            },
            401: {
                "model": ErrorMessage,
                "description": "Invalid Access Token",
            },
            403: {
                "model": ErrorMessage,
                "description": "No access to Nummernkreis",
            },
            503: {
                "model": ErrorMessage,
                "description": "Another process is currently updating this resource. Try again.",
            },
        },
    )
    async def _upload_xdf3_schema(  # type: ignore [reportUnusedFunction]
        access: Annotated[Access, Security(check_access_token)],
        # NOTE: This must not be called `schema`, as this is a reserved keyword in pydantic 2.
        schema_file: Annotated[
            bytes,
            File(
                title="Schema",
                alias="schema",
                # NOTE: `validation_alias` must be set additionally, as fastapi has a bug where the alias
                # is not respected when creating the OpenAPI Docs.
                validation_alias="schema",
                description="The `Datenschema` (must be `application/xml`).",
                media_type="application/xml",
            ),
        ],
        service: Service = Depends(get_service),
    ) -> FullSchemaOut:
        """
        Include the API Key in the Header `Access-Token`.
        """
        try:
            xdf3_import = Xdf3SchemaImport.from_bytes(schema_file)

            check_access_to_upload(
                xdf3_import.schema_message.schema.identifier.nummernkreis,
                access,
            )
        except ImportException as exception:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(exception)
            )

        logger.info(f"Uploading new schema for Nummernkreis {access.nummernkreis}")

        try:
            await service.import_xdf3_schema(xdf3_import, strict=False)
        except ImportException as error:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(error)
            ) from error
        except DatabaseDeadlockException as error:
            raise HTTPException(
                status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                detail=str(error),
                headers={"Retry-After": "0"},
            )

        result = await service.get_schema(xdf3_import.id, xdf3_import.version)
        assert result is not None

        return result

    @router.get(
        "/schemas",
        response_class=ORJSONResponse,
        name="Search schemas",
    )
    async def get_schemas(  # type: ignore [reportUnusedFunction]
        name: NameFilter = None,
        nummernkreis: NummernkreisFilter = None,
        freigabe_status: FreigabeStatusFilter = None,
        gueltig_am: GueltigAmFilter = None,
        status_gesetzt_durch: StatusGesetztDurchFilter = None,
        status_gesetzt_seit: StatusGesetztSeitFilter = None,
        status_gesetzt_bis: StatusGesetztBisFilter = None,
        bezug: BezugFilter = None,
        bezug_unterelemente: BezugUnterelementeFilter = None,
        bezeichnung: BezeichnungFilter = None,
        versionshinweis: VersionshinweisFilter = None,
        updated_since: UpdatedSinceFilter = None,
        stichwort: StichwortFilter = None,
        xdf_version: XdfVersionFilter = None,
        fts_query: FtsQueryFilter = None,
        suche_nur_in: SchemaFtsSucheIn = None,
        is_latest: IsLatestFilter = None,
        order_by: SchemaOrderBy = SchemaSearchOrder.ID_ASC,
        pagination_options: PaginationOptions = Depends(get_pagination_options),
        service: Service = Depends(get_service),
    ) -> PaginatedResult[SchemaOut]:
        return await service.search_schemas(
            options=SchemaSearchOptions(
                pagination_options=pagination_options,
                order_by=order_by,
                name=name,
                nummernkreis=nummernkreis,
                freigabe_status=freigabe_status,
                gueltig_am=gueltig_am,
                status_gesetzt_durch=status_gesetzt_durch,
                status_gesetzt_seit=status_gesetzt_seit,
                status_gesetzt_bis=status_gesetzt_bis,
                stichwort=stichwort,
                bezug=bezug,
                bezug_unterelemente=bezug_unterelemente,
                bezeichnung=bezeichnung,
                versionshinweis=versionshinweis,
                xdf_version=xdf_version,
                updated_since=updated_since,
                fts_query=fts_query,
                suche_nur_in=suche_nur_in,
                is_latest=is_latest,
            )
        )

    @router.get(
        "/schemas/{fim_id}",
        response_class=ORJSONResponse,
        name="Get all schema versions",
    )
    async def get_schema_versions(  # type: ignore [reportUnusedFunction]
        fim_id: SchemaId, service: Service = Depends(get_service)
    ) -> list[SchemaOut]:
        """
        Get a list of all available versions of a schema in ascending order.
        """

        return await service.get_schema_versions(fim_id)

    @router.get(
        "/schemas/{fim_id}/{fim_version}",
        response_class=ORJSONResponse,
        name="Get a full schema",
        responses={
            200: {"model": FullSchemaOut},
            404: {"description": "Schema not found"},
        },
    )
    async def get_full_schema(  # type: ignore [reportUnusedFunction]
        fim_id: SchemaId,
        fim_version: SchemaVersion,
        service: Service = Depends(get_service),
    ) -> FullSchemaOut:
        """
        This will return all of the available schema data including the list of referenced data groups and data fields.
        Use the special version `latest` to return the latest version for a given Id.
        """

        schema = await service.get_schema(
            fim_id=fim_id,
            fim_version=fim_version,
        )

        if schema is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find schema {fim_id}V{fim_version}.",
            )
        else:
            return schema

    @router.get(
        "/schemas/{fim_id}/{fim_version}/xdf",
        name="Download the XDatenfelder XML file for this schema.",
        response_class=FileResponse,
        responses={
            200: {"content": {"application/xml": {}}},
            404: {"model": ErrorMessage, "description": "Schema not found"},
        },
    )
    async def _get_schema_xml(  # type: ignore [reportUnusedFunction]
        fim_id: str,
        fim_version: str,
        service: Service = Depends(get_service),
    ):
        """
        Use the special version `latest` to return the latest version for a given Id.
        """

        message = await service.export_xdf_schema_message(fim_id, fim_version)
        if message is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find schema {fim_id}V{fim_version}.",
            )

        if isinstance(message, xdf2.SchemaMessage):
            content = xdf2.serialize_schema_message(message)
            filename = f"{fim_id}V{message.assert_version()}.xdf2.xml"
        else:
            content = xdf3.serialize_schema_message(message)
            filename = f"{fim_id}V{message.assert_version()}.xdf3.xml"

        return XmlFileResponse(
            content=content,
            filename=filename,
        )

    @router.get(
        "/schemas/{fim_id}/{fim_version}/quality-report",
        response_class=ORJSONResponse,
        name="Get schema quality report",
        responses={
            404: {"model": ErrorMessage, "description": "Schema not found"},
        },
    )
    async def get_quality_report(  # type: ignore [reportUnusedFunction]
        fim_id: SchemaId,
        fim_version: SchemaVersion,
        service: Service = Depends(get_service),
    ) -> QualityReport:
        """
        This will return a quality report for the specified schema.
        Details on the quality metrics can be found in the [user documentation](https://docs.fitko.de/fim/docs).
        Use the special version `latest` to return the latest version for a given Id.
        """
        report = await service.get_schema_quality_report(
            fim_id=fim_id,
            fim_version=fim_version,
        )

        if report is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find schema {fim_id}V{fim_version}.",
            )
        else:
            return report

    @router.post(
        "/document-profiles/xdf2",
        name="Upload an xdf2 document profile",
        response_class=ORJSONResponse,
        responses={
            400: {
                "model": ErrorMessage,
                "description": "Import Error",
            },
            401: {
                "model": ErrorMessage,
                "description": "Invalid Access Token",
            },
            403: {
                "model": ErrorMessage,
                "description": "No access to Nummernkreis",
            },
            503: {
                "model": ErrorMessage,
                "description": "Another process is currently updating this resource. Try again.",
            },
        },
    )
    async def upload_xdf2_steckbrief(  # type: ignore [reportUnusedFunction]
        access: Annotated[Access, Security(check_access_token)],
        document_profile: Annotated[
            bytes,
            File(
                title="Document Profile",
                description="The `Steckbrief` (must be `application/xml`).",
                media_type="application/xml",
            ),
        ],
        freigabe_status: Annotated[
            xdf3.FreigabeStatus,
            Form(
                title="Freigabestatus",
                description="The `freigabe_status` of the `Steckbrief`.",
            ),
        ],
        service: Service = Depends(get_service),
    ) -> FullSteckbriefOut:
        """
        This upload needs additional parameters besides the document profile itself:

        - **freigabe_status**: The `Freigabestatus` of the schema as specified in the new standard xdf3.

        Include the API Key in the Header `Access-Token`.
        """
        try:
            xdf2_steckbrief_import = Xdf2SteckbriefImport.from_bytes(
                document_profile,
                freigabe_status=freigabe_status,
            )
        except ImportException as ex:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=str(ex),
            )

        # TODO(Felix): Find a better place for this conversion
        xdf2_nummernkreis = xdf2_steckbrief_import.steckbrief.identifier.nummernkreis
        xdf3_nummernkreis = xdf3.Nummernkreis(f"{xdf2_nummernkreis}000")
        check_access_to_upload(xdf3_nummernkreis, access)

        logger.info(
            f"Uploading new document profile for Nummernkreis {access.nummernkreis}"
        )

        try:
            await service.import_xdf2_steckbrief(
                xdf2_steckbrief_import, check_status=False
            )
        except ImportException as error:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=str(error),
            ) from error
        except DatabaseDeadlockException as error:
            raise HTTPException(
                status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                detail=str(error),
                headers={"Retry-After": "0"},
            )

        result = await service.get_steckbrief(
            xdf2_steckbrief_import.id, xdf2_steckbrief_import.version
        )
        assert result is not None
        return result

    @router.post(
        "/document-profiles/xdf3",
        name="Upload an xdf3 document profile",
        response_class=ORJSONResponse,
        responses={
            400: {
                "model": ErrorMessage,
                "description": "Import Error",
            },
            401: {
                "model": ErrorMessage,
                "description": "Invalid Access Token",
            },
            403: {
                "model": ErrorMessage,
                "description": "No access to Nummernkreis",
            },
            503: {
                "model": ErrorMessage,
                "description": "Another process is currently updating this resource. Try again.",
            },
        },
    )
    async def upload_xdf3_steckbrief(  # type: ignore [reportUnusedFunction]
        access: Annotated[Access, Security(check_access_token)],
        document_profile: Annotated[
            bytes,
            File(
                title="Document Profile",
                description="The `Steckbrief` (must be `application/xml`).",
                media_type="application/xml",
            ),
        ],
        service: Service = Depends(get_service),
    ) -> FullSteckbriefOut:
        """
        Include the API Key in the Header `Access-Token`.
        """
        try:
            steckbrief_message = parse_steckbrief_message(document_profile)
        except xdf3.ParserException as ex:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=str(ex),
            ) from ex

        check_access_to_upload(
            steckbrief_message.steckbrief.identifier.nummernkreis, access
        )

        logger.info(
            f"Uploading new document profile for Nummernkreis {access.nummernkreis}"
        )

        try:
            await service.import_xdf3_steckbrief(
                steckbrief_message, document_profile.decode("utf-8"), check_status=False
            )
        except ImportException as error:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=str(error),
            ) from error
        except DatabaseDeadlockException as error:
            raise HTTPException(
                status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                detail=str(error),
                headers={"Retry-After": "0"},
            )

        result = await service.get_steckbrief(
            steckbrief_message.steckbrief.identifier.id,
            steckbrief_message.steckbrief.identifier.assert_version(),
        )
        assert result is not None

        return result

    @router.get(
        "/document-profiles",
        response_class=ORJSONResponse,
        name="Search document profiles",
    )
    async def search_steckbriefe(  # type: ignore [reportUnusedFunction]
        name: NameFilter = None,
        freigabe_status: FreigabeStatusFilter = None,
        status_gesetzt_durch: StatusGesetztDurchFilter = None,
        status_gesetzt_seit: StatusGesetztSeitFilter = None,
        status_gesetzt_bis: StatusGesetztBisFilter = None,
        bezeichnung: BezeichnungFilter = None,
        dokumentart: DokumentArtFilter = None,
        nummernkreis: NummernkreisFilter = None,
        bezug: BezugFilter = None,
        fts_query: FtsQueryFilter = None,
        suche_nur_in: SteckbriefFtsSucheIn = None,
        versionshinweis: VersionshinweisFilter = None,
        updated_since: UpdatedSinceFilter = None,
        xdf_version: XdfVersionFilter = None,
        stichwort: StichwortFilter = None,
        is_latest: IsLatestFilter = None,
        pagination_options: PaginationOptions = Depends(get_pagination_options),
        service: Service = Depends(get_service),
    ) -> PaginatedResult[SteckbriefOut]:
        options = SteckbriefSearchOptions(
            name=name,
            nummernkreis=nummernkreis,
            freigabe_status=freigabe_status,
            status_gesetzt_durch=status_gesetzt_durch,
            status_gesetzt_bis=status_gesetzt_bis,
            status_gesetzt_seit=status_gesetzt_seit,
            bezeichnung=bezeichnung,
            dokumentart=dokumentart,
            bezug=bezug,
            pagination_options=pagination_options,
            fts_query=fts_query,
            suche_nur_in=suche_nur_in,
            versionshinweis=versionshinweis,
            updated_since=updated_since,
            xdf_version=xdf_version,
            stichwort=stichwort,
            is_latest=is_latest,
        )
        return await service.search_steckbriefe(options)

    @router.get(
        "/document-profiles/{fim_id}",
        name="Get all versions of a document profile",
        response_class=ORJSONResponse,
    )
    async def get_steckbrief_versions(  # type: ignore [reportUnusedFunction]
        fim_id: str,
        service: Service = Depends(get_service),
    ) -> list[SteckbriefOut]:
        return await service.get_steckbrief_versions(fim_id)

    @router.get(
        "/document-profiles/{fim_id}/{fim_version}",
        name="Return a single document profile",
        response_class=ORJSONResponse,
        responses={
            404: {"model": ErrorMessage, "description": "Document profile not found"},
        },
    )
    async def get_steckbrief(  # type: ignore [reportUnusedFunction]
        fim_id: str, fim_version: str, service: Service = Depends(get_service)
    ) -> FullSteckbriefOut:
        """
        Use the special version `latest` to return the latest version for a given Id.
        """

        result = await service.get_steckbrief(fim_id, fim_version)
        if result is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find document profile {fim_id}V{fim_version}.",
            )

        return result

    @router.get(
        "/document-profiles/{fim_id}/{fim_version}/xdf",
        name="Download the XDatenfelder XML file for this document-profile.",
        response_class=FileResponse,
        responses={
            200: {"content": {"application/xml": {}}},
            404: {"model": ErrorMessage, "description": "Document profile not found"},
        },
    )
    async def get_steckbrief_xml(  # type: ignore [reportUnusedFunction]
        fim_id: str,
        fim_version: str,
        service: Service = Depends(get_service),
    ):
        """
        Use the special version `latest` to return the latest version for a given Id.
        """

        result = await service.get_steckbrief_xml_content(fim_id, fim_version)
        if result is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find document profile {fim_id}V{fim_version}.",
            )

        filename, content = result

        return XmlFileResponse(
            content=content,
            filename=filename,
        )

    @router.get(
        "/fields",
        response_class=ORJSONResponse,
        name="Search fields",
    )
    async def get_fields(  # type: ignore [reportUnusedFunction]
        fts_query: FtsQueryFilter = None,
        suche_nur_in: FeldFtsSucheIn = None,
        name: NameFilter = None,
        nummernkreis: NummernkreisFilter = None,
        freigabe_status: FreigabeStatusFilter = None,
        gueltig_am: GueltigAmFilter = None,
        status_gesetzt_durch: StatusGesetztDurchFilter = None,
        status_gesetzt_seit: StatusGesetztSeitFilter = None,
        status_gesetzt_bis: StatusGesetztBisFilter = None,
        bezug: BezugFilter = None,
        versionshinweis: VersionshinweisFilter = None,
        updated_since: UpdatedSinceFilter = None,
        xdf_version: XdfVersionFilter = None,
        feldart: FeldartFilter = None,
        datentyp: DatentypFilter = None,
        is_latest: IsLatestFilter = None,
        pagination_options: PaginationOptions = Depends(get_pagination_options),
        service: Service = Depends(get_service),
    ) -> PaginatedResult[DatenfeldOut]:
        options = FieldSearchOptions(
            fts_query=fts_query,
            suche_nur_in=suche_nur_in,
            name=name,
            pagination_options=pagination_options,
            nummernkreis=nummernkreis,
            freigabe_status=freigabe_status,
            gueltig_am=gueltig_am,
            status_gesetzt_durch=status_gesetzt_durch,
            status_gesetzt_seit=status_gesetzt_seit,
            status_gesetzt_bis=status_gesetzt_bis,
            bezug=bezug,
            versionshinweis=versionshinweis,
            xdf_version=xdf_version,
            updated_since=updated_since,
            feldart=feldart,
            datentyp=datentyp,
            is_latest=is_latest,
        )

        return await service.search_fields(options)

    @router.get(
        "/fields/{namespace}/{fim_id}",
        response_class=ORJSONResponse,
        name="Get all versions of a data field",
        description="Get a list of all available versions of a data field in ascending order.",
    )
    async def get_field_versions(  # type: ignore [reportUnusedFunction]
        namespace: str,
        fim_id: str,
        service: Service = Depends(get_service),
    ) -> list[DatenfeldOut]:
        return await service.get_field_versions(namespace, fim_id)

    @router.get(
        "/fields/{namespace}/{fim_id}/{fim_version}",
        response_class=ORJSONResponse,
        name="Get a specific data field",
        responses={
            404: {"model": ErrorMessage, "description": "Data field not found"},
        },
    )
    async def get_field(  # type: ignore [reportUnusedFunction]
        namespace: str,
        fim_id: str,
        fim_version: str,
        service: Service = Depends(get_service),
    ) -> FullDatenfeldOut:
        """
        Use the special version `latest` to return the latest version for a given Id.
        """

        field = await service.get_field(namespace, fim_id, fim_version)

        if field is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find field {fim_id}V{fim_version} in namespace {namespace}.",
            )
        else:
            return field

    @router.get(
        "/fields/{namespace}/{fim_id}/{fim_version}/xdf",
        name="Download the XDatenfelder XML file for this field.",
        response_class=FileResponse,
        responses={
            200: {"content": {"application/xml": {}}},
            404: {"model": ErrorMessage, "description": "Data field not found"},
        },
    )
    async def _get_field_xml(  # type: ignore [reportUnusedFunction]
        namespace: str,
        fim_id: str,
        fim_version: str,
        service: Service = Depends(get_service),
    ):
        """
        Use the special version `latest` to return the latest version for a given Id.
        """

        message = await service.export_xdf_datenfeld_message(
            namespace, fim_id, fim_version
        )
        if message is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find field {fim_id}V{fim_version} in namespace {namespace}.",
            )

        if isinstance(message, xdf2.DatenfeldMessage):
            content = xdf2.serialize_datenfeld_message(message)
            filename = f"{fim_id}V{message.assert_version()}.xdf2.xml"
        else:
            content = xdf3.serialize_datenfeld_message(message)
            filename = f"{fim_id}V{message.assert_version()}.xdf3.xml"

        return XmlFileResponse(
            content=content,
            filename=filename,
        )

    @router.get(
        "/groups",
        response_class=ORJSONResponse,
        name="Search groups",
    )
    async def get_groups(  # type: ignore [reportUnusedFunction]
        fts_query: FtsQueryFilter = None,
        suche_nur_in: GruppeFtsSucheIn = None,
        name: NameFilter = None,
        nummernkreis: NummernkreisFilter = None,
        freigabe_status: FreigabeStatusFilter = None,
        gueltig_am: GueltigAmFilter = None,
        status_gesetzt_durch: StatusGesetztDurchFilter = None,
        status_gesetzt_seit: StatusGesetztSeitFilter = None,
        status_gesetzt_bis: StatusGesetztBisFilter = None,
        bezug: BezugFilter = None,
        versionshinweis: VersionshinweisFilter = None,
        updated_since: UpdatedSinceFilter = None,
        xdf_version: XdfVersionFilter = None,
        is_latest: IsLatestFilter = None,
        pagination_options: PaginationOptions = Depends(get_pagination_options),
        service: Service = Depends(get_service),
    ) -> PaginatedResult[DatenfeldgruppeOut]:
        options = GroupSearchOptions(
            fts_query=fts_query,
            suche_nur_in=suche_nur_in,
            name=name,
            pagination_options=pagination_options,
            nummernkreis=nummernkreis,
            freigabe_status=freigabe_status,
            gueltig_am=gueltig_am,
            status_gesetzt_durch=status_gesetzt_durch,
            status_gesetzt_seit=status_gesetzt_seit,
            status_gesetzt_bis=status_gesetzt_bis,
            bezug=bezug,
            versionshinweis=versionshinweis,
            xdf_version=xdf_version,
            updated_since=updated_since,
            is_latest=is_latest,
        )

        return await service.search_groups(options)

    @router.get(
        "/groups/{namespace}/{fim_id}",
        response_class=ORJSONResponse,
        name="Get all versions of a data group",
        description="Get a list of all available versions of a data group in ascending order.",
    )
    async def get_group_versions(  # type: ignore [reportUnusedFunction]
        namespace: str,
        fim_id: str,
        service: Service = Depends(get_service),
    ) -> list[DatenfeldgruppeOut]:
        return await service.get_group_versions(namespace, fim_id)

    @router.get(
        "/groups/{namespace}/{fim_id}/{fim_version}",
        name="Get a specific data group",
        response_class=ORJSONResponse,
        responses={
            404: {"model": ErrorMessage, "description": "Data group not found"},
        },
    )
    async def get_group(  # type: ignore [reportUnusedFunction]
        namespace: str,
        fim_id: str,
        fim_version: str,
        service: Service = Depends(get_service),
    ) -> FullDatenfeldgruppeOut:
        """
        Use the special version `latest` to return the latest version for a given Id.
        """

        group = await service.get_group(namespace, fim_id, fim_version)

        if group is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find group {fim_id}V{fim_version} in namespace {namespace}.",
            )
        else:
            return group

    @router.get(
        "/groups/{namespace}/{fim_id}/{fim_version}/xdf",
        name="Download the XDatenfelder XML file for this group.",
        response_class=FileResponse,
        responses={
            200: {"content": {"application/xml": {}}},
            404: {"model": ErrorMessage, "description": "Data group not found"},
        },
    )
    async def _get_group_xml(  # type: ignore [reportUnusedFunction]
        namespace: str,
        fim_id: str,
        fim_version: str,
        service: Service = Depends(get_service),
    ):
        """
        Use the special version `latest` to return the latest version for a given Id.
        """

        message = await service.export_xdf_datenfeldgruppe_message(
            namespace, fim_id, fim_version
        )
        if message is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find group {fim_id}V{fim_version} in namespace {namespace}.",
            )

        if isinstance(message, xdf2.DatenfeldgruppeMessage):
            content = xdf2.serialize_datenfeldgruppe_message(message)
            filename = f"{fim_id}V{message.assert_version()}.xdf2.xml"
        else:
            content = xdf3.serialize_datenfeldgruppe_message(message)
            filename = f"{fim_id}V{message.assert_version()}.xdf3.xml"

        return XmlFileResponse(
            content=content,
            filename=filename,
        )

    return router
