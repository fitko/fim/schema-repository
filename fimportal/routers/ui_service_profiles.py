from __future__ import annotations

import logging
from dataclasses import dataclass
from typing import Annotated, Any, AsyncGenerator, Callable, Literal

from fastapi import APIRouter, Depends, Query, Request, status
from fastapi.responses import HTMLResponse

from fimportal.helpers import RenderFunction
from fimportal.models.xzufi import (
    FullLeistungssteckbriefOut,
    XzufiSource,
    get_leistung_hierarchy_info,
    get_leistungs_title,
)
from fimportal.routers.common import (
    Resource,
    UiBausteinReferenz,
    UiHeaderTab,
    create_clean_query_string,
)
from fimportal.routers.ui_xzufi import (
    REDAKTION_ID_TO_ICON,
    Metadata,
    SidebarNavigationItem,
    SteckbriefInfo,
    Translation,
    UiModul,
    get_icon_url_for_redaktion,
)
from fimportal.service import Service
from fimportal.xzufi.common import (
    REDAKTION_ID_TO_LABEL,
    LanguageCode,
    LeistungHierarchyType,
    LeistungRedaktionId,
    ModulTextTyp,
)
from fimportal.xzufi.leistung import (
    Leistung,
    can_have_stammtext,
    parse_leistung,
)

ServiceTabId = Literal["index", "leistungsbeschreibungen"]
ServiceTab = UiHeaderTab[ServiceTabId]


logger = logging.getLogger(__name__)


@dataclass(slots=True)
class ServiceHeader:
    title: str
    leistungsschluessel: str
    typisierung: str
    redaktion_label: str
    redaktion_icon: str | None

    base_href: str
    back_url: str
    active_tab: ServiceTabId
    tabs: list[ServiceTab]

    @staticmethod
    def from_xzufi(
        leistung: Leistung,
        leistungsschluessel: str,
        redaktion_id: str,
        german: LanguageCode,
        search_query_string: str,
    ) -> ServiceHeader:
        base_href = f"/services/{leistungsschluessel}"

        return ServiceHeader(
            leistungsschluessel=leistungsschluessel,
            typisierung=", ".join([f"Typ {i.value}" for i in leistung.typisierung]),
            title=get_leistungs_title(leistung, german),
            redaktion_label=REDAKTION_ID_TO_LABEL.get(redaktion_id, redaktion_id),
            redaktion_icon=REDAKTION_ID_TO_ICON.get(redaktion_id),
            base_href=base_href,
            back_url=f"/search{search_query_string}",
            active_tab="index",
            tabs=[
                ServiceTab(
                    id="index",
                    label="Übersicht",
                    icon="bi-eye-fill",
                    active_tab="index",
                    base_href=base_href,
                    search_query_string=search_query_string,
                ),
                ServiceTab(
                    id="leistungsbeschreibungen",
                    label="Leistungsbeschreibungen",
                    icon="bi-file-earmark-text",
                    active_tab="index",
                    base_href=base_href,
                    search_query_string=search_query_string,
                ),
            ],
        )

    @staticmethod
    def from_leistung(
        leistung: FullLeistungssteckbriefOut,
        search_query_string: str,
    ) -> ServiceHeader:
        base_href = f"/services/{leistung.leistungsschluessel}"

        return ServiceHeader(
            leistungsschluessel=leistung.leistungsschluessel,
            typisierung=", ".join([f"Typ {i.value}" for i in leistung.typisierung]),
            title=leistung.title,
            redaktion_label=REDAKTION_ID_TO_LABEL.get(
                leistung.redaktion_id, leistung.redaktion_id
            ),
            redaktion_icon=REDAKTION_ID_TO_ICON.get(leistung.redaktion_id),
            base_href=base_href,
            back_url=f"/search{search_query_string}",
            active_tab="leistungsbeschreibungen",
            tabs=[
                ServiceTab(
                    id="index",
                    label="Übersicht",
                    icon="bi-eye-fill",
                    active_tab="leistungsbeschreibungen",
                    base_href=base_href,
                    search_query_string=search_query_string,
                ),
                ServiceTab(
                    id="leistungsbeschreibungen",
                    label="Leistungsbeschreibungen",
                    icon="bi-file-earmark-text",
                    active_tab="leistungsbeschreibungen",
                    base_href=base_href,
                    search_query_string=search_query_string,
                ),
            ],
        )


@dataclass(slots=True)
class ServiceIndexPage:
    header: ServiceHeader
    sidebar_navigation: list[SidebarNavigationItem]

    freigabestatus_katalog: str
    freigabestatus_bibliothek: str | None

    translations: list[Translation]
    leistungsbeschreibungen: list[LeistungsBeschreibungenItem]
    prozessklasse: UiBausteinReferenz | None

    metadata: Metadata
    steckbrief: SteckbriefInfo
    stammtext: StammtextInfo | str

    @staticmethod
    def create(
        leistung: FullLeistungssteckbriefOut,
        xzufi_leistung: Leistung,
        language_code: str | None,
        source: XzufiSource,
        search_query_string: str,
    ) -> ServiceIndexPage:
        redaktion_id = xzufi_leistung.unwrap_redaktions_id()

        leistungsschluessel, hierarchy_type = get_leistung_hierarchy_info(
            xzufi_leistung, redaktion_id
        )

        # The leistungsschluesel cannot be None, as we load the Leistung by the
        # Leistungsschluessel.
        assert leistungsschluessel is not None

        native_language_code, active_language_code, translations = (
            Translation.extract_translations(
                xzufi_leistung, language_code, search_query_string
            )
        )

        header = ServiceHeader.from_xzufi(
            xzufi_leistung,
            leistungsschluessel,
            redaktion_id,
            native_language_code,
            search_query_string,
        )

        freigabestatus_katalog = xzufi_leistung.get_freigabe_status_katalog()
        if freigabestatus_katalog is None:
            freigabestatus_katalog = "unbestimmter Freigabestatus"
        else:
            freigabestatus_katalog = freigabestatus_katalog.to_label()

        if can_have_stammtext(xzufi_leistung.typisierung):
            freigabestatus_bibliothek = xzufi_leistung.get_freigabe_status_bibliothek()
            if freigabestatus_bibliothek is None:
                freigabestatus_bibliothek = "unbestimmter Freigabestatus"
            else:
                freigabestatus_bibliothek = freigabestatus_bibliothek.to_label()
        else:
            freigabestatus_bibliothek = None

        steckbrief = SteckbriefInfo.create(
            xzufi_leistung,
            header.leistungsschluessel,
            header.typisierung,
            active_language_code,
        )
        stammtext = StammtextInfo.create(
            xzufi_leistung, header, hierarchy_type, active_language_code
        )

        sidebar_nav_items = [
            SidebarNavigationItem("Steckbrief", "#steckbrief"),
            SidebarNavigationItem.from_ui_modul(steckbrief.rechtsgrundlagen),
        ]
        if isinstance(stammtext, StammtextInfo):
            for modul in stammtext.module:
                sidebar_nav_items.append(SidebarNavigationItem.from_ui_modul(modul))

        return ServiceIndexPage(
            header=header,
            freigabestatus_katalog=freigabestatus_katalog,
            freigabestatus_bibliothek=freigabestatus_bibliothek,
            translations=translations,
            leistungsbeschreibungen=LeistungsBeschreibungenItem.create(leistung),
            prozessklasse=UiBausteinReferenz(
                id=leistung.prozessklasse.id,
                href=f"/processclasses/{leistung.prozessklasse.id}",
                label=leistung.prozessklasse.name,
            )
            if leistung.prozessklasse is not None
            else None,
            metadata=Metadata.create(xzufi_leistung, header.redaktion_label, source),
            steckbrief=steckbrief,
            stammtext=stammtext,
            sidebar_navigation=sidebar_nav_items,
        )


@dataclass(slots=True)
class LeistungsBeschreibungenItem:
    icon: str
    label: str
    href: str

    @staticmethod
    def create(
        leistung: FullLeistungssteckbriefOut,
    ) -> list[LeistungsBeschreibungenItem]:
        items = []

        for leistungsbeschreibung in leistung.leistungsbeschreibungen:
            try:
                redaktion_id = LeistungRedaktionId(leistungsbeschreibung.redaktion_id)
            except ValueError:
                logger.error(
                    "Unknown redaktion_id %s", leistungsbeschreibung.redaktion_id
                )
                icon = ""
                label = leistungsbeschreibung.redaktion_id
            else:
                icon = get_icon_url_for_redaktion(redaktion_id)
                label = redaktion_id.to_label()

            items.append(
                LeistungsBeschreibungenItem(
                    icon=icon,
                    label=label,
                    href=f"/xzufi-services/{leistungsbeschreibung.redaktion_id}/{leistungsbeschreibung.id}",
                )
            )

        return sorted(items, key=lambda item: item.label)


@dataclass(slots=True)
class StammtextInfo:
    module: list[UiModul]

    @staticmethod
    def create(
        leistung: Leistung,
        header: ServiceHeader,
        hierarchy_type: LeistungHierarchyType,
        language_code: LanguageCode,
    ) -> StammtextInfo | str:
        if not can_have_stammtext(leistung.typisierung):
            return f"Stammtext nicht verfügbar für {header.typisierung} Leistung"

        if hierarchy_type is not LeistungHierarchyType.STAMMTEXT:
            return "Stammtext nicht veröffentlicht."

        return StammtextInfo(
            module=[
                UiModul.create_textmodul(
                    leistung, ModulTextTyp.TEASER, True, language_code
                ),
                UiModul.create_textmodul(
                    leistung, ModulTextTyp.VOLLTEXT, True, language_code
                ),
                UiModul.create_textmodul(
                    leistung,
                    ModulTextTyp.ERFORDERLICHE_UNTERLAGEN,
                    False,
                    language_code,
                ),
                UiModul.create_textmodul(
                    leistung, ModulTextTyp.VORAUSSETZUNGEN, False, language_code
                ),
                UiModul.create_kostenmodul(leistung, False, language_code),
                UiModul.create_textmodul(
                    leistung, ModulTextTyp.VERFAHRENSABLAUF, False, language_code
                ),
                UiModul.create_bearbeitungsdauer(leistung, False, language_code),
                UiModul.create_fristmodul(leistung, False, language_code),
                UiModul.create_textmodul(
                    leistung,
                    ModulTextTyp.WEITERFUEHRENDE_INFORMATIONEN,
                    False,
                    language_code,
                ),
                UiModul.create_textmodul(
                    leistung, ModulTextTyp.HINWEISE, False, language_code
                ),
                UiModul.create_textmodul(
                    leistung, ModulTextTyp.RECHTSBEHELF, False, language_code
                ),
                UiModul.create_textmodul(
                    leistung, ModulTextTyp.KURZTEXT, True, language_code
                ),
                UiModul.create_textmodul(
                    leistung, ModulTextTyp.ANSPRECHPUNKT, True, language_code
                ),
                UiModul.create_textmodul(
                    leistung,
                    ModulTextTyp.ZUSTAENDIGE_STELLE,
                    True,
                    language_code,
                ),
                UiModul.create_textmodul(
                    leistung, ModulTextTyp.FORMUALRE, False, language_code
                ),
                UiModul.create_ursprungsportal(leistung, False, language_code),
            ]
        )


@dataclass(slots=True)
class XzufiReferenzLink:
    title: str
    redaktion_label: str

    href: str

    @staticmethod
    def create(leistung: FullLeistungssteckbriefOut) -> XzufiReferenzLink:
        return XzufiReferenzLink(
            title=leistung.title,
            redaktion_label=REDAKTION_ID_TO_LABEL.get(
                leistung.redaktion_id, leistung.redaktion_id
            ),
            href=f"/xzufi-services/{leistung.redaktion_id}/{leistung.leistung_id}",
        )


def create_service_profile_router(
    get_service: Callable[[], AsyncGenerator[Service, Any]], render: RenderFunction
):
    router = APIRouter()

    def _service_not_found(request: Request, leistungsschluessel: str):
        return render(
            request,
            "404.html.jinja",
            {
                "message": f"Leistung {leistungsschluessel} konnte nicht gefunden werden.",
            },
            status_code=status.HTTP_404_NOT_FOUND,
        )

    @router.get("/{leistungsschluessel}", response_class=HTMLResponse)
    async def get_service_profile_details(  # type: ignore [reportUnusedFunction]
        request: Request,
        leistungsschluessel: str,
        language_code: Annotated[str | None, Query()] = None,
        service: Service = Depends(get_service),
    ):
        # TODO: Combine these requests into one
        leistungssteckbrief = await service.get_leistungssteckbrief(leistungsschluessel)
        if leistungssteckbrief is None:
            return _service_not_found(request, leistungsschluessel=leistungsschluessel)

        content_info = await service.get_leistungssteckbrief_content(
            leistungsschluessel
        )
        assert content_info is not None

        # This should never fail, as we only import leistungen which we can parse.
        # Therefore, a potential parsing error is not caught here.
        # If an error occurs, this is a clear bug, which results a 500 return value and an
        # automatic error log entry.
        xzufi_leistung = parse_leistung(content_info.content)

        search_query_string = create_clean_query_string(request, Resource.SERVICE)

        page = ServiceIndexPage.create(
            leistungssteckbrief,
            xzufi_leistung,
            language_code=language_code,
            source=content_info.source,
            search_query_string=search_query_string,
        )

        return render(
            request,
            "service_profile_detail.html.jinja",
            {
                "titleTag": f"{page.header.title} • Leistungen • FIM Portal",
                "page": page,
            },
        )

    return router
