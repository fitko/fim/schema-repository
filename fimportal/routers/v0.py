from typing import Any, AsyncGenerator, Callable

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.responses import ORJSONResponse
from starlette.responses import Response

from fimportal.database import (
    LeistungssteckbriefSearchOrder,
    ProzessklasseSearchOptions,
    LeistungsbeschreibungSearchOptions,
    ProzessSearchOptions,
    LeistungssteckbriefSearchOptions,
    LeistungsbeschreibungSearchOrder,
)
from fimportal.models.xdf3 import CodeListOut
from fimportal.models.xzufi import (
    FullLeistungssteckbriefOut,
    FullLeistungsbeschreibungOut,
    LeistungssteckbriefOut,
    LeistungsbeschreibungOut,
    OnlinedienstOut,
    OrganisationseinheitOut,
    QualityReportOut,
    SpezialisierungOut,
    ZustaendigkeitOut,
)
from fimportal.pagination import (
    PaginatedResult,
    PaginationOptions,
    get_pagination_options,
)
from fimportal.routers.immutable import FileResponse, XmlFileResponse

from fimportal.xprozesse.prozess import (
    create_xprozess_message_for_prozess,
    create_xprozess_message_for_prozessklasse,
)
from fimportal.xzufi import (
    LeistungsId,
    RedaktionsId,
)
from fimportal.xzufi.common import XzufiIdentifier

from ..service import Service
from .parameters import (
    ErrorMessage,
    FreigabeStatusFilter,
    FreigabeStatusBibliothekFilter,
    FreigabeStatusKatalogFilter,
    MusterprozessQueryFilter,
    FtsQueryFilter,
    LeistungFtsSucheIn,
    LeistungOrderBy,
    LeistungsbeschreibungOrderBy,
    LeistungTitleFilter,
    LeistungLeistungsschluesselFilter,
    LeistungRechtsgrundlagenFilter,
    LeistungRedaktionsIdFilter,
    LeistungTypFilter,
    LeistungTypisierungFilter,
    LeistungsbezeichnungFilter,
    Leistungsbezeichnung2Filter,
    LeistungEinheitlicherAnsprechpartnerFilter,
    OperativesZielFilter,
    VerfahrensartFilter,
    HandlungsformFilter,
    DetaillierungsstufeFilter,
    AnwendungsgebietFilter,
    UpdatedSinceFilter,
)


class PdfFileResponse(Response):
    media_type = "application/pdf"

    def __init__(
        self,
        content: bytes,
        filename: str,
        headers: dict[str, str] | None = None,
        status_code: int = 200,
    ) -> None:
        super().__init__(
            content=content,
            status_code=status_code,
            headers=headers,
            media_type="application/pdf",
            background=None,
        )
        self.headers["Content-Disposition"] = f'attachment; filename="{filename}"'

    def render(self, content: str | bytes) -> bytes:
        if isinstance(content, str):
            return content.encode("utf-8")
        else:
            return content


class DisplayPdfFileResponse(Response):
    media_type = "application/pdf"

    def __init__(
        self,
        content: bytes,
        filename: str,
        headers: dict[str, str] | None = None,
        status_code: int = 200,
    ) -> None:
        super().__init__(
            content=content,
            status_code=status_code,
            headers=headers,
            media_type="application/pdf",
            background=None,
        )
        self.headers["Content-Disposition"] = f"inline; filename={filename}"

    def render(self, content: str | bytes) -> bytes:
        if isinstance(content, str):
            return content.encode("utf-8")
        else:
            return content


def create_router(
    get_service: Callable[[], AsyncGenerator[Service, Any]],
) -> APIRouter:
    router = APIRouter()

    @router.get(
        "/services",
        response_class=ORJSONResponse,
        name="Get a list of available services",
    )
    async def _get_leistungen(  # type: ignore [reportUnusedFunction]
        leistungstyp: LeistungTypFilter = None,
        typisierung: LeistungTypisierungFilter = None,
        fts_query: FtsQueryFilter = None,
        suche_nur_in: LeistungFtsSucheIn = None,
        title: LeistungTitleFilter = None,
        leistungsbezeichnung: LeistungsbezeichnungFilter = None,
        leistungsbezeichnung_II: Leistungsbezeichnung2Filter = None,
        leistungsschluessel: LeistungLeistungsschluesselFilter = None,
        rechtsgrundlagen: LeistungRechtsgrundlagenFilter = None,
        freigabe_status_katalog: FreigabeStatusKatalogFilter = None,
        freigabe_status_bibliothek: FreigabeStatusBibliothekFilter = None,
        einheitlicher_ansprechpartner: LeistungEinheitlicherAnsprechpartnerFilter = None,
        updated_since: UpdatedSinceFilter = None,
        order_by: LeistungOrderBy = LeistungssteckbriefSearchOrder.ERSTELLT_DATUM_ZEIT_DESC,
        pagination_options: PaginationOptions = Depends(get_pagination_options),
        service: Service = Depends(get_service),
    ) -> PaginatedResult[LeistungssteckbriefOut]:
        options = LeistungssteckbriefSearchOptions(
            leistungstyp=leistungstyp,
            typisierung=typisierung,
            fts_query=fts_query,
            suche_nur_in=suche_nur_in,
            title=title,
            leistungsbezeichnung=leistungsbezeichnung,
            leistungsbezeichnung_2=leistungsbezeichnung_II,
            leistungsschluessel=leistungsschluessel,
            rechtsgrundlagen=rechtsgrundlagen,
            freigabe_status_katalog=freigabe_status_katalog,
            freigabe_status_bibliothek=freigabe_status_bibliothek,
            einheitlicher_ansprechpartner=einheitlicher_ansprechpartner,
            updated_since=updated_since,
            pagination_options=pagination_options,
            order_by=order_by,
        )
        return await service.search_leistungssteckbriefe(options)

    @router.get(
        "/services/{leistungsschluessel}",
        response_class=ORJSONResponse,
        name="Get a specific service",
    )
    async def _get_leistungssteckbrief(  # type: ignore [reportUnusedFunction]
        leistungsschluessel: str, service: Service = Depends(get_service)
    ) -> FullLeistungssteckbriefOut:
        leistung = await service.get_leistungssteckbrief(leistungsschluessel)
        if leistung is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find service {leistungsschluessel}.",
            )

        return leistung

    @router.get(
        "/xzufi-services",
        response_class=ORJSONResponse,
        name="Get a list of available raw xzufi services",
    )
    async def _get_leistungsbeschreibungen(  # type: ignore [reportUnusedFunction]
        leistungsschluessel: LeistungLeistungsschluesselFilter = None,
        redaktion_id: LeistungRedaktionsIdFilter = None,
        title: LeistungTitleFilter = None,
        leistungsbezeichnung: LeistungsbezeichnungFilter = None,
        leistungsbezeichnung_II: Leistungsbezeichnung2Filter = None,
        rechtsgrundlagen: LeistungRechtsgrundlagenFilter = None,
        leistungstyp: LeistungTypFilter = None,
        typisierung: LeistungTypisierungFilter = None,
        updated_since: UpdatedSinceFilter = None,
        fts_query: FtsQueryFilter = None,
        suche_nur_in: LeistungFtsSucheIn = None,
        order_by: LeistungsbeschreibungOrderBy = LeistungsbeschreibungSearchOrder.ERSTELLT_DATUM_ZEIT_DESC,
        pagination_options: PaginationOptions = Depends(get_pagination_options),
        service: Service = Depends(get_service),
    ) -> PaginatedResult[LeistungsbeschreibungOut]:
        options = LeistungsbeschreibungSearchOptions(
            redaktion_id=redaktion_id,
            leistungsschluessel=leistungsschluessel,
            rechtsgrundlagen=rechtsgrundlagen,
            leistungstyp=leistungstyp,
            typisierung=typisierung,
            title=title,
            leistungsbezeichnung=leistungsbezeichnung,
            leistungsbezeichnung_2=leistungsbezeichnung_II,
            updated_since=updated_since,
            fts_query=fts_query,
            suche_nur_in=suche_nur_in,
            pagination_options=pagination_options,
            order_by=order_by,
        )
        return await service.search_leistungsbeschreibungen(options)

    @router.get(
        "/xzufi-services/{redaktion_id}/{leistung_id}",
        response_class=ORJSONResponse,
        name="Get a specific xzufi service",
        responses={
            404: {"model": ErrorMessage, "description": "Service not found"},
        },
    )
    async def get_leistungsbeschreibung(  # type: ignore [reportUnusedFunction]
        redaktion_id: str,
        leistung_id: str,
        service: Service = Depends(get_service),
    ) -> FullLeistungsbeschreibungOut:
        leistung = await service.get_leistungsbeschreibung(
            (
                RedaktionsId(redaktion_id),
                LeistungsId(leistung_id),
            )
        )

        if leistung is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find service {leistung_id} from Redaktion {redaktion_id}.",
            )

        return leistung

    @router.get(
        "/xzufi-services/{redaktion_id}/{leistung_id}/quality-report",
        response_class=ORJSONResponse,
        name="Get the quality report of a given service",
    )
    async def get_leistung_quality_report(  # type: ignore [reportUnusedFunction]
        redaktion_id: str,
        leistung_id: str,
        service: Service = Depends(get_service),
    ) -> QualityReportOut:
        result = await service.get_leistung_quality_report(
            (
                RedaktionsId(redaktion_id),
                LeistungsId(leistung_id),
            )
        )
        if result is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find service {leistung_id} from Redaktion {redaktion_id}.",
            )
        return QualityReportOut.from_quality_report(result)

    @router.get(
        "/xzufi-services/{redaktion_id}/{leistung_id}/xzufi",
        response_class=FileResponse,
        responses={
            200: {"content": {"application/xml": {}}},
            404: {"model": ErrorMessage, "description": "Service not found"},
        },
        name="Download the XZuFi XML representation of the service.",
    )
    async def get_leistung_xml(  # type: ignore [reportUnusedFunction]
        redaktion_id: RedaktionsId,
        leistung_id: LeistungsId,
        service: Service = Depends(get_service),
    ):
        content_info = await service.get_leistungsbeschreibung_content_info(
            (redaktion_id, leistung_id)
        )

        if content_info is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find service {leistung_id} from Redaktion {redaktion_id}.",
            )

        return XmlFileResponse(
            content=content_info.content,
            filename=f"{redaktion_id}_{leistung_id}.xzufi.xml",
        )

    @router.get(
        "/organizational-unit",
        response_class=ORJSONResponse,
        name="Get a list of available Organisationeinheiten",
    )
    async def _get_leistungen(  # type: ignore [reportUnusedFunction]
        pagination_options: PaginationOptions = Depends(get_pagination_options),
        service: Service = Depends(get_service),
    ) -> PaginatedResult[OrganisationseinheitOut]:
        return await service.search_xzufi_organisationseinheiten(pagination_options)

    @router.get(
        "/organizational-unit/{redaktion_id}/{organisationseinheit_id}/xzufi",
        response_class=FileResponse,
        responses={
            200: {"content": {"application/xml": {}}},
            404: {
                "model": ErrorMessage,
                "description": "Organisationseinheit not found",
            },
        },
        name="Download the XZuFi XML representation of the Organisationseinheit.",
    )
    async def get_organisationseinheit_xml(  # type: ignore [reportUnusedFunction]
        redaktion_id: RedaktionsId,
        organisationseinheit_id: str,
        service: Service = Depends(get_service),
    ):
        xml_content = await service.get_xzufi_organisationseinheit_xml(
            XzufiIdentifier(redaktion_id, organisationseinheit_id)
        )

        if xml_content is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find Organisationseinheit {organisationseinheit_id} from Redaktion {redaktion_id}.",
            )

        return XmlFileResponse(
            content=xml_content,
            filename=f"organisationseinheit_{redaktion_id}_{organisationseinheit_id}.xzufi.xml",
        )

    @router.get(
        "/specialization",
        response_class=ORJSONResponse,
        name="Get a list of available Spezialisierungen",
    )
    async def _get_specialization(  # type: ignore [reportUnusedFunction]
        pagination_options: PaginationOptions = Depends(get_pagination_options),
        service: Service = Depends(get_service),
    ) -> PaginatedResult[SpezialisierungOut]:
        return await service.search_xzufi_spezialisierung(pagination_options)

    @router.get(
        "/specialization/{redaktion_id}/{spezialisierung_id}/xzufi",
        response_class=FileResponse,
        responses={
            200: {"content": {"application/xml": {}}},
            404: {
                "model": ErrorMessage,
                "description": "Spezialisierung not found",
            },
        },
        name="Download the XZuFi XML representation of the Spezialisierung.",
    )
    async def get_spezialisierung_xml(  # type: ignore [reportUnusedFunction]
        redaktion_id: RedaktionsId,
        spezialisierung_id: str,
        service: Service = Depends(get_service),
    ):
        xml_content = await service.get_xzufi_spezialisierung_xml(
            XzufiIdentifier(redaktion_id, spezialisierung_id)
        )

        if xml_content is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find Spezialisierung {spezialisierung_id} from Redaktion {redaktion_id}.",
            )

        return XmlFileResponse(
            content=xml_content,
            filename=f"spezialisierung_{redaktion_id}_{spezialisierung_id}.xzufi.xml",
        )

    @router.get(
        "/jurisdiction",
        response_class=ORJSONResponse,
        name="Get a list of available Zustaendigkeiten",
    )
    async def _get_zustaendigkeiten(  # type: ignore [reportUnusedFunction]
        pagination_options: PaginationOptions = Depends(get_pagination_options),
        service: Service = Depends(get_service),
    ) -> PaginatedResult[ZustaendigkeitOut]:
        return await service.search_xzufi_zustaendigkeiten(pagination_options)

    @router.get(
        "/jurisdiction/{redaktion_id}/{zustaendigkeit_id}/xzufi",
        response_class=FileResponse,
        responses={
            200: {"content": {"application/xml": {}}},
            404: {
                "model": ErrorMessage,
                "description": "Zustaendigkeit not found",
            },
        },
        name="Download the XZuFi XML representation of the Zustaendigkeit.",
    )
    async def get_zustaendigkeit_xml(  # type: ignore [reportUnusedFunction]
        redaktion_id: RedaktionsId,
        zustaendigkeit_id: str,
        service: Service = Depends(get_service),
    ):
        xml_content = await service.get_xzufi_zustaendigkeit_xml(
            XzufiIdentifier(redaktion_id, zustaendigkeit_id)
        )

        if xml_content is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find Zustaendigkeit {zustaendigkeit_id} from Redaktion {redaktion_id}.",
            )

        return XmlFileResponse(
            content=xml_content,
            filename=f"zustaendigkeit_{redaktion_id}_{zustaendigkeit_id}.xzufi.xml",
        )

    @router.get(
        "/online-service",
        response_class=ORJSONResponse,
        name="Get a list of available Onlinediensten",
    )
    async def _get_onlinedienste(  # type: ignore [reportUnusedFunction]
        pagination_options: PaginationOptions = Depends(get_pagination_options),
        service: Service = Depends(get_service),
    ) -> PaginatedResult[OnlinedienstOut]:
        return await service.search_xzufi_onlinedienste(pagination_options)

    @router.get(
        "/online-service/{redaktion_id}/{onlinedienst_id}/xzufi",
        response_class=FileResponse,
        responses={
            200: {"content": {"application/xml": {}}},
            404: {
                "model": ErrorMessage,
                "description": "Onlinedienst not found",
            },
        },
        name="Download the XZuFi XML representation of the Onlinedienst.",
    )
    async def get_onlinedienst_xml(  # type: ignore [reportUnusedFunction]
        redaktion_id: RedaktionsId,
        onlinedienst_id: str,
        service: Service = Depends(get_service),
    ):
        xml_content = await service.get_xzufi_onlinedienst_xml(
            XzufiIdentifier(redaktion_id, onlinedienst_id)
        )

        if xml_content is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find Onlinedienst {onlinedienst_id} from Redaktion {redaktion_id}.",
            )

        return XmlFileResponse(
            content=xml_content,
            filename=f"onlinedienst_{redaktion_id}_{onlinedienst_id}.xzufi.xml",
        )

    @router.get(
        "/processclasses",
        name="Search process classes.",
        response_class=ORJSONResponse,
    )
    async def get_prozessklassen(  # type: ignore [reportUnusedFunction]
        fts_query: FtsQueryFilter = None,
        pagination_options: PaginationOptions = Depends(get_pagination_options),
        freigabe_status: FreigabeStatusFilter = None,
        operatives_ziel: OperativesZielFilter = None,
        verfahrensart: VerfahrensartFilter = None,
        handlungsform: HandlungsformFilter = None,
        service: Service = Depends(get_service),
    ):
        options = ProzessklasseSearchOptions(
            fts_query=fts_query,
            freigabe_status=freigabe_status,
            operatives_ziel=operatives_ziel,
            verfahrensart=verfahrensart,
            handlungsform=handlungsform,
            pagination_options=pagination_options,
        )
        return await service.search_prozessklassen(options)

    @router.get(
        "/processclasses/{processclass_id}",
        name="Get a specific process.",
        response_class=ORJSONResponse,
        responses={
            404: {"model": ErrorMessage, "description": "Prozessklasse not found"},
        },
    )
    async def get_prozessklasse(  # type: ignore [reportUnusedFunction]
        processclass_id: str, service: Service = Depends(get_service)
    ):
        result = await service.get_prozessklasse(processclass_id)
        if result is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find process {processclass_id}.",
            )

        return result

    @router.get(
        "/processclasses/{processclass_id}/xprozess",
        name="Get a specific process.",
        response_class=ORJSONResponse,
        responses={
            404: {"model": ErrorMessage, "description": "Prozessklasse not found"},
        },
    )
    async def get_prozessklasse_xml(  # type: ignore [reportUnusedFunction]
        processclass_id: str, service: Service = Depends(get_service)
    ):
        result = await service.get_prozessklasse_xml(processclass_id)
        if result is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find processclass {processclass_id}.",
            )

        content = create_xprozess_message_for_prozessklasse(result)
        filename = f"{processclass_id}_prozessklasse.xprozess.xml"

        return XmlFileResponse(
            content=content,
            filename=filename,
        )

    @router.get(
        "/processes",
        name="Search processes.",
        response_class=ORJSONResponse,
    )
    async def get_prozesse(  # type: ignore [reportUnusedFunction]
        freigabe_status: FreigabeStatusFilter = None,
        detaillierungsstufe: DetaillierungsstufeFilter = None,
        anwendungsgebiet: AnwendungsgebietFilter = None,
        is_musterprozess: MusterprozessQueryFilter = None,
        fts_query: FtsQueryFilter = None,
        pagination_options: PaginationOptions = Depends(get_pagination_options),
        service: Service = Depends(get_service),
    ):
        options = ProzessSearchOptions(
            freigabe_status=freigabe_status,
            detaillierungsstufe=detaillierungsstufe,
            anwendungsgebiet=anwendungsgebiet,
            is_musterprozess=is_musterprozess,
            fts_query=fts_query,
            pagination_options=pagination_options,
        )
        return await service.search_prozesse(options)

    @router.get(
        "/processes/{process_id}",
        name="Get a specific process.",
        response_class=ORJSONResponse,
        responses={
            404: {"model": ErrorMessage, "description": "Prozess not found"},
        },
    )
    async def get_prozess(  # type: ignore [reportUnusedFunction]
        process_id: str,
        service: Service = Depends(get_service),
    ):
        result = await service.get_prozess(process_id)
        if result is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find process {process_id}.",
            )

        return result

    @router.get(
        "/processes/{process_id}/xprozess",
        name="Download the XProzess XML file for this process.",
        response_class=FileResponse,
        responses={
            200: {"content": {"application/xml": {}}},
            404: {"model": ErrorMessage, "description": "Process not found"},
        },
    )
    async def get_prozess_xml(  # type: ignore [reportUnusedFunction]
        process_id: str,
        service: Service = Depends(get_service),
    ):
        result = await service.get_prozess_xml(process_id)
        if result is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find process {process_id}.",
            )

        content = create_xprozess_message_for_prozess(result)
        filename = f"{process_id}_prozess.xprozess.xml"

        return XmlFileResponse(
            content=content,
            filename=filename,
        )

    @router.get(
        "/processes/{process_id}/report",
        name="Download the report file for this process.",
        response_class=FileResponse,
        responses={
            200: {"content": {"application/xml": {}}},
            404: {"model": ErrorMessage, "description": "Process not found"},
        },
    )
    async def get_prozessbeschreibung(  # type: ignore [reportUnusedFunction]
        process_id: str,
        service: Service = Depends(get_service),
    ):
        result = await service.get_prozess_report_file(process_id)
        if result is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find process {process_id}.",
            )

        content = result
        filename = f"{process_id}.report.pdf"

        return PdfFileResponse(
            content=content,
            filename=filename,
        )

    @router.get(
        "/processes/{process_id}/visualization",
        name="Download the visualization file for this process.",
        response_class=FileResponse,
        responses={
            200: {"content": {"application/xml": {}}},
            404: {"model": ErrorMessage, "description": "Process not found"},
        },
    )
    async def get_prozess_visualisierung(  # type: ignore [reportUnusedFunction]
        process_id: str,
        service: Service = Depends(get_service),
    ):
        result = await service.get_prozess_visualization_file(process_id)
        if result is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find process {process_id}.",
            )

        content = result
        filename = f"{process_id}.visualization.pdf"

        return PdfFileResponse(
            content=content,
            filename=filename,
        )

    @router.get(
        "/processes/{process_id}/visualization_display",
        name="Download the visualization file for this process.",
        response_class=FileResponse,
        responses={
            200: {"content": {"application/xml": {}}},
            404: {"model": ErrorMessage, "description": "Process not found"},
        },
    )
    async def get_prozess_visualisierung_for_display(  # type: ignore [reportUnusedFunction]
        process_id: str,
        service: Service = Depends(get_service),
    ):
        result = await service.get_prozess_visualization_file(process_id)
        if result is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find process {process_id}.",
            )

        content = result
        filename = f"{process_id}.visualization.pdf"

        return DisplayPdfFileResponse(
            content=content,
            filename=filename,
        )

    # TODO: Remove this endpoint once the new one is in use
    @router.get(
        "/document-profiles/{fim_id}/{fim_version}/xdf",
        name="Download the XDatenfelder XML file for this document-profile. Please use the new endpoint in `/v1` instead.",
        response_class=FileResponse,
        responses={
            200: {"content": {"application/xml": {}}},
            404: {"model": ErrorMessage, "description": "Document profile not found"},
        },
        deprecated=True,
    )
    async def get_steckbrief_xml(  # type: ignore [reportUnusedFunction]
        fim_id: str,
        fim_version: str,
        service: Service = Depends(get_service),
    ):
        """
        Use the special version `latest` to return the latest version for a given Id.
        """

        result = await service.get_steckbrief_xml_content(fim_id, fim_version)
        if result is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find document profile {fim_id}V{fim_version}.",
            )

        filename, content = result

        return XmlFileResponse(
            content=content,
            filename=filename,
        )

    @router.get(
        "/code-lists",
        response_class=ORJSONResponse,
        name="Get all code lists",
    )
    async def get_code_lists(  # type: ignore [reportUnusedFunction]
        pagination_options: PaginationOptions = Depends(get_pagination_options),
        service: Service = Depends(get_service),
    ) -> PaginatedResult[CodeListOut]:
        """
        This returns a list of all the code lists included by any of the imported schemas.
        """

        return await service.get_code_lists(pagination_options)

    return router
