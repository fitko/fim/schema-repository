from __future__ import annotations

from dataclasses import dataclass
from typing import Any, AsyncGenerator, Callable, Literal

from fastapi import APIRouter, Depends, Request, status
from starlette.responses import HTMLResponse

from fimportal.helpers import RenderFunction
from fimportal.models.ui_prototype import (
    AvailableVersion,
    SchemaReference,
    TreeGroup,
)
from fimportal.models.xdf3 import DatenfeldgruppeOut, FullDatenfeldgruppeOut
from fimportal.routers.common import (
    Resource,
    UiHeaderTab,
    UiHandlungsgrundlage,
    UiRelationen,
    create_clean_query_string,
    ui_display_eingabe_ausgabe,
)
from fimportal.service import Service
from fimportal.xdatenfelder import xdf2, xdf2_to_xdf3, xdf3
from fimportal.xdatenfelder.xdf3.common import Stichwort

GroupTabId = Literal["index", "structure", "references"]
GroupTab = UiHeaderTab[GroupTabId]


@dataclass(slots=True)
class GroupHeader:
    """
    Data for templates/components/group_header.html.jinja
    """

    name: str
    fim_id: str
    fim_version: str
    xdf_version: str
    freigabe_status: str

    # NOTE(Felix): Only temporary for the RegMo presentation
    is_bob: bool

    base_href: str
    anzahl_referenzen: str
    tabs: list[GroupTab]
    active_tab: GroupTabId
    back_url: str

    @staticmethod
    def create(
        group: FullDatenfeldgruppeOut, active_tab: GroupTabId, search_query_string: str
    ) -> GroupHeader:
        anzahl_referenzen = str(len(group.schemas))
        base_href = f"/groups/{group.namespace}/{group.fim_id}/{group.fim_version}"

        return GroupHeader(
            name=group.name,
            fim_id=group.fim_id,
            fim_version=group.fim_version,
            xdf_version="XDatenfelder " + group.xdf_version.value,
            freigabe_status=group.freigabe_status.to_label(),
            anzahl_referenzen=anzahl_referenzen,
            base_href=base_href,
            active_tab=active_tab,
            is_bob=group.nummernkreis == "60000"
            and group.freigabe_status == xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
            tabs=[
                GroupTab(
                    id="index",
                    label="Übersicht",
                    icon="bi-eye-fill",
                    active_tab=active_tab,
                    base_href=base_href,
                    search_query_string=search_query_string,
                ),
                GroupTab(
                    id="structure",
                    label="Struktur",
                    icon="bi-diagram-2-fill",
                    active_tab=active_tab,
                    base_href=base_href,
                    search_query_string=search_query_string,
                ),
                GroupTab(
                    id="references",
                    label="Verwendungen (" + anzahl_referenzen + ")",
                    icon="bi-arrow-left-right",
                    active_tab=active_tab,
                    base_href=base_href,
                    search_query_string=search_query_string,
                ),
            ],
            back_url=f"/search{search_query_string}",
        )


@dataclass(slots=True)
class UiGroupData:
    """
    All of the data that is displayed in xdf 3.0.0 format on the page.
    """

    bezug: list[UiHandlungsgrundlage]
    gruppenart: str | None
    hilfetext_eingabe: str | None
    hilfetext_ausgabe: str | None
    bezeichnung_eingabe: (
        str | None
    )  # NOTE(Dominik): Standard does not allow None but Fimportal does (for Xdf2)
    bezeichnung_ausgabe: str | None
    schemaelementart: str
    stichwoerter: list[Stichwort] | None

    @staticmethod
    def from_xdf2_group(group: xdf2.Gruppe) -> UiGroupData:
        bezug = xdf2_to_xdf3.map_bezug(group.bezug)

        return UiGroupData(
            bezug=[UiHandlungsgrundlage(el.text, el.link) for el in bezug],
            gruppenart=None,
            hilfetext_eingabe=group.hilfetext_eingabe,
            hilfetext_ausgabe=group.hilfetext_ausgabe,
            bezeichnung_eingabe=group.bezeichnung_eingabe,
            bezeichnung_ausgabe=group.bezeichnung_ausgabe,
            schemaelementart=group.schema_element_art.to_label(),
            stichwoerter=None,
        )

    @staticmethod
    def from_xdf3_group(group: xdf3.Gruppe) -> UiGroupData:
        return UiGroupData(
            bezug=[UiHandlungsgrundlage(el.text, el.link) for el in group.bezug],
            gruppenart=None,
            hilfetext_eingabe=group.hilfetext_eingabe,
            hilfetext_ausgabe=group.hilfetext_ausgabe,
            bezeichnung_eingabe=group.bezeichnung_eingabe,
            bezeichnung_ausgabe=group.bezeichnung_ausgabe,
            schemaelementart=group.schema_element_art.to_label(),
            stichwoerter=group.stichwort if len(group.stichwort) > 0 else None,
        )


@dataclass(slots=True)
class GroupIndexPage:
    """
    Data for templates/group_detail.html.jinja
    """

    header: GroupHeader

    definition: str | None
    beschreibung: str | None
    bezug: list[UiHandlungsgrundlage]
    versionshinweis: str | None
    gruppenart: str | None
    bezeichnung: list[str]
    hilfetext: list[str]
    schemaelementart: str
    stichwoerter: list[Stichwort] | None

    available_versions: list[AvailableVersion]

    status_gesetzt_durch: str | None
    status_gesetzt_am: str | None
    gueltig_ab: str | None
    gueltig_bis: str | None
    letzte_aenderung: str
    last_update: str
    veroeffentlicht_am: str | None

    relations: UiRelationen

    @staticmethod
    def create(
        group: FullDatenfeldgruppeOut,
        ui_group_data: UiGroupData,
        versions: list[DatenfeldgruppeOut],
        search_query_string: str,
    ) -> GroupIndexPage:
        return GroupIndexPage(
            header=GroupHeader.create(
                group, active_tab="index", search_query_string=search_query_string
            ),
            available_versions=[
                AvailableVersion.from_group(
                    version, group.fim_version, search_query_string
                )
                for version in versions
            ][::-1],
            definition=group.definition,
            beschreibung=group.beschreibung,
            bezug=ui_group_data.bezug,
            status_gesetzt_durch=group.status_gesetzt_durch,
            status_gesetzt_am=(
                group.status_gesetzt_am.strftime("%d.%m.%Y")
                if group.status_gesetzt_am
                else None
            ),
            veroeffentlicht_am=(
                group.veroeffentlichungsdatum.strftime("%d.%m.%Y")
                if group.veroeffentlichungsdatum
                else None
            ),
            gueltig_ab=(
                group.gueltig_ab.strftime("%d.%m.%Y") if group.gueltig_ab else None
            ),
            gueltig_bis=(
                group.gueltig_bis.strftime("%d.%m.%Y") if group.gueltig_bis else None
            ),
            versionshinweis=group.versionshinweis,
            gruppenart=ui_group_data.gruppenart,
            bezeichnung=ui_display_eingabe_ausgabe(
                ui_group_data.bezeichnung_eingabe, ui_group_data.bezeichnung_ausgabe
            ),
            hilfetext=ui_display_eingabe_ausgabe(
                ui_group_data.hilfetext_eingabe, ui_group_data.hilfetext_ausgabe
            ),
            schemaelementart=ui_group_data.schemaelementart,
            stichwoerter=ui_group_data.stichwoerter,
            letzte_aenderung=group.letzte_aenderung.strftime("%d.%m.%Y"),
            last_update=group.last_update.strftime("%d.%m.%Y"),
            relations=UiRelationen.create(group.relation, "group"),
        )


@dataclass(slots=True)
class GroupStructurePage:
    """
    Data for templates/group_structure.html.jinja
    """

    header: GroupHeader
    group_tree: TreeGroup

    @staticmethod
    def create(
        group: FullDatenfeldgruppeOut, search_query_string: str
    ) -> GroupStructurePage:
        return GroupStructurePage(
            header=GroupHeader.create(
                group, active_tab="structure", search_query_string=search_query_string
            ),
            group_tree=TreeGroup.create(group),
        )


@dataclass(slots=True)
class GroupReferencesPage:
    """
    Data for templates/group_references.html.jinja
    """

    header: GroupHeader
    schema_references: list[SchemaReference]

    @staticmethod
    def create(
        group: FullDatenfeldgruppeOut, search_query_string: str
    ) -> GroupReferencesPage:
        return GroupReferencesPage(
            header=GroupHeader.create(
                group, active_tab="references", search_query_string=search_query_string
            ),
            schema_references=[
                SchemaReference.from_schema_out(schema) for schema in group.schemas
            ],
        )


def create_group_router(
    get_service: Callable[[], AsyncGenerator[Service, Any]],
    render: RenderFunction,
) -> APIRouter:
    router = APIRouter()

    def _group_not_found(request: Request, fim_id: str, fim_version: str):
        return render(
            request,
            "404.html.jinja",
            {
                "message": f"Datenfeldgruppe {fim_id} v{fim_version} konnte nicht gefunden werden.",
            },
            status_code=status.HTTP_404_NOT_FOUND,
        )

    @router.get("/{namespace}/{fim_id}/{fim_version}", response_class=HTMLResponse)
    async def _get_group_page(  # type: ignore [reportUnusedFunction]
        request: Request,
        namespace: str,
        fim_id: str,
        fim_version: str,
        service: Service = Depends(get_service),
    ):
        group = await service.get_group(
            namespace=namespace, fim_id=fim_id, fim_version=fim_version
        )
        if group is None:
            return _group_not_found(request, fim_id, fim_version)

        group_message = await service.export_xdf_datenfeldgruppe_message(
            namespace, fim_id, fim_version
        )
        assert group_message is not None

        # NOTE(Dominik): This is not optimal, as we only need the actual group here.
        # See also ui_schemas._get_schema_details
        if isinstance(group_message, xdf2.DatenfeldgruppeMessage):
            ui_group_data = UiGroupData.from_xdf2_group(group_message.group)
        else:
            ui_group_data = UiGroupData.from_xdf3_group(group_message.group)

        versions = await service.get_group_versions(namespace, fim_id)

        search_query_string = create_clean_query_string(request, Resource.GROUP)

        return render(
            request,
            "group_detail.html.jinja",
            {
                "titleTag": f"{group.fim_id} v{group.fim_version} • Datenfeldgruppen • FIM-Portal",
                "page": GroupIndexPage.create(
                    group, ui_group_data, versions, search_query_string
                ),
            },
        )

    @router.get(
        "/{namespace}/{fim_id}/{fim_version}/structure", response_class=HTMLResponse
    )
    async def _get_group_structure_page(  # type: ignore [reportUnusedFunction]
        request: Request,
        namespace: str,
        fim_id: str,
        fim_version: str,
        service: Service = Depends(get_service),
    ):
        group = await service.get_group(
            namespace=namespace, fim_id=fim_id, fim_version=fim_version
        )
        if group is None:
            return _group_not_found(request, fim_id, fim_version)

        search_query_string = create_clean_query_string(request, Resource.GROUP)

        return render(
            request,
            "group_structure.html.jinja",
            {
                "titleTag": f"{group.fim_id} v{group.fim_version} • Datenfeldgruppen • FIM-Portal",
                "page": GroupStructurePage.create(group, search_query_string),
            },
        )

    @router.get(
        "/{namespace}/{fim_id}/{fim_version}/references", response_class=HTMLResponse
    )
    async def _get_group_references_page(  # type: ignore [reportUnusedFunction]
        request: Request,
        namespace: str,
        fim_id: str,
        fim_version: str,
        service: Service = Depends(get_service),
    ):
        group = await service.get_group(
            namespace=namespace, fim_id=fim_id, fim_version=fim_version
        )
        if group is None:
            return _group_not_found(request, fim_id, fim_version)

        search_query_string = create_clean_query_string(request, Resource.GROUP)

        return render(
            request,
            "group_references.html.jinja",
            {
                "titleTag": f"{group.fim_id} v{group.fim_version} • Datenfeldgruppen • FIM-Portal",
                "page": GroupReferencesPage.create(group, search_query_string),
            },
        )

    return router
