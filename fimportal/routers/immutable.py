import os
from typing import Any, AsyncGenerator, Callable, Literal

from fastapi import APIRouter, Depends, HTTPException, status
from starlette.responses import Response

from fimportal.routers.parameters import ErrorMessage

from ..service import Service


class XmlFileResponse(Response):
    media_type = "application/xml"

    def __init__(
        self,
        content: str | bytes,
        filename: str,
        headers: dict[str, str] | None = None,
        status_code: int = 200,
    ) -> None:
        super().__init__(
            content=content,
            status_code=status_code,
            headers=headers,
            media_type="application/xml",
            background=None,
        )
        self.headers["Content-Disposition"] = f'attachment; filename="{filename}"'

    def render(self, content: str | bytes) -> bytes:
        if isinstance(content, str):
            return content.encode("utf-8")
        else:
            return content


class FileResponse(Response):
    def __init__(
        self,
        content: str,
        filename: str,
        media_type: Literal["application/json", "application/xml"],
        headers: dict[str, str] | None = None,
        status_code: int = 200,
    ) -> None:
        super().__init__(
            content=content,
            status_code=status_code,
            headers=headers,
            media_type=media_type,
            background=None,
        )
        self.headers["Content-Disposition"] = f'attachment; filename="{filename}"'

    def render(self, content: str) -> bytes:
        return content.encode("utf-8")


def create_router(get_service: Callable[[], AsyncGenerator[Service, Any]]) -> APIRouter:
    router = APIRouter()

    @router.get(
        "/code-lists/{id}/genericode.xml",
        response_class=FileResponse,
        responses={
            200: {"content": {"application/xml": {}}},
            404: {
                "model": ErrorMessage,
                "description": "Not Found Error",
            },
        },
    )
    async def get_code_list(  # type: ignore [reportUnusedFunction]
        id: int,
        service: Service = Depends(get_service),
    ):
        content = await service.export_code_list(id)
        if content is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find code list {id}",
            )

        return XmlFileResponse(
            content=content,
            filename=f"code_list_{id}_genericode.xml",
        )

    @router.get(
        "/schemas/{filename}",
        summary="Download schema files",
        response_class=FileResponse,
        responses={
            200: {"content": {"application/json": {}, "application/xml": {}}},
            404: {
                "model": ErrorMessage,
                "description": "Not Found Error",
            },
        },
    )
    async def get_schema_file(  # type: ignore [reportUnusedFunction]
        filename: str,
        service: Service = Depends(get_service),
    ):
        match os.path.splitext(filename)[1]:
            case ".xml":
                media_type = "application/xml"
                content = await service.export_schema_file(filename)
            case ".json":
                media_type = "application/json"
                content = await service.export_json_schema_file(filename)
            case ".xsd":
                media_type = "application/xml"
                content = await service.export_xsd_file(filename)
            case _:
                media_type = None
                content = None

        if content is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Could not find file {filename}",
            )
        else:
            assert media_type is not None
            return FileResponse(
                content=content,
                filename=filename,
                media_type=media_type,
            )

    return router
