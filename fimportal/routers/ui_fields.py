from __future__ import annotations

from dataclasses import dataclass
from typing import Any, AsyncGenerator, Callable, Literal

from fastapi import APIRouter, Depends, Request, status
from starlette.responses import HTMLResponse

from fimportal.helpers import RenderFunction
from fimportal.models.ui_prototype import (
    AvailableVersion,
    SchemaReference,
)
from fimportal.models.xdf3 import DatenfeldOut, FullDatenfeldOut
from fimportal.routers.common import (
    Resource,
    UiHeaderTab,
    UiHandlungsgrundlage,
    UiRelationen,
    create_clean_query_string,
    ui_display_eingabe_ausgabe,
)
from fimportal.service import Service
from fimportal.xdatenfelder import xdf2, xdf2_to_xdf3, xdf3
from fimportal.xdatenfelder.xdf3.common import Praezisierung, Stichwort

FieldTabId = Literal["index", "references"]
FieldTab = UiHeaderTab[FieldTabId]


@dataclass(slots=True)
class FieldHeader:
    """
    Data for templates/components/field_header.html.jinja
    """

    name: str
    fim_id: str
    fim_version: str
    xdf_version: str
    freigabe_status: str

    # NOTE(Felix): Only temporary for the RegMo presentation
    is_bob: bool

    base_href: str
    back_url: str
    active_tab: FieldTabId
    tabs: list[FieldTab]
    anzahl_referenzen: str

    @staticmethod
    def create(
        field: FullDatenfeldOut, active_tab: FieldTabId, search_query_string: str
    ) -> FieldHeader:
        anzahl_referenzen = str(len(field.schemas))
        base_href = f"/fields/{field.namespace}/{field.fim_id}/{field.fim_version}"

        return FieldHeader(
            name=field.name,
            fim_id=field.fim_id,
            fim_version=field.fim_version,
            xdf_version="XDatenfelder " + field.xdf_version.value,
            freigabe_status=field.freigabe_status.to_label(),
            anzahl_referenzen=anzahl_referenzen,
            base_href=base_href,
            back_url=f"/search{search_query_string}",
            active_tab=active_tab,
            tabs=[
                FieldTab(
                    id="index",
                    label="Übersicht",
                    icon="bi-eye-fill",
                    active_tab=active_tab,
                    base_href=base_href,
                    search_query_string=search_query_string,
                ),
                FieldTab(
                    id="references",
                    label="Verwendungen (" + anzahl_referenzen + ")",
                    icon="bi-arrow-left-right",
                    active_tab=active_tab,
                    base_href=base_href,
                    search_query_string=search_query_string,
                ),
            ],
            is_bob=field.nummernkreis == "60000"
            and field.freigabe_status == xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        )


@dataclass(slots=True)
class UiFieldData:
    """
    All of the data that is displayed in xdf 3.0.0 format on the page.
    """

    bezug: list[UiHandlungsgrundlage]
    inhalt: str | None
    praezisierung: Praezisierung | str | None
    hilfetext_eingabe: str | None
    hilfetext_ausgabe: str | None
    bezeichnung_eingabe: (
        str | None
    )  # NOTE(Dominik): Standard does not allow None but Fimportal does (for Xdf2)
    bezeichnung_ausgabe: str | None
    schemaelementart: str
    stichwoerter: list[Stichwort] | None

    @staticmethod
    def from_xdf2_field(field: xdf2.Datenfeld) -> UiFieldData:
        bezug = xdf2_to_xdf3.map_bezug(field.bezug)

        return UiFieldData(
            bezug=[UiHandlungsgrundlage(el.text, el.link) for el in bezug],
            inhalt=field.inhalt,
            praezisierung=field.praezisierung,
            hilfetext_eingabe=field.hilfetext_eingabe,
            hilfetext_ausgabe=field.hilfetext_ausgabe,
            bezeichnung_eingabe=field.bezeichnung_eingabe,
            bezeichnung_ausgabe=field.bezeichnung_ausgabe,
            schemaelementart=field.schema_element_art.to_label(),
            stichwoerter=None,
        )

    @staticmethod
    def from_xdf3_field(field: xdf3.Datenfeld) -> UiFieldData:
        return UiFieldData(
            bezug=[UiHandlungsgrundlage(el.text, el.link) for el in field.bezug],
            inhalt=field.inhalt,
            praezisierung=field.praezisierung,
            hilfetext_eingabe=field.hilfetext_eingabe,
            hilfetext_ausgabe=field.hilfetext_ausgabe,
            bezeichnung_eingabe=field.bezeichnung_eingabe,
            bezeichnung_ausgabe=field.bezeichnung_ausgabe,
            schemaelementart=field.schema_element_art.to_label(),
            stichwoerter=field.stichwort if len(field.stichwort) > 0 else None,
        )


@dataclass(slots=True)
class FieldIndexPage:
    """
    Data for templates/field_detail.html.jinja
    """

    header: FieldHeader

    definition: str | None
    beschreibung: str | None
    bezug: list[UiHandlungsgrundlage]
    versionshinweis: str | None
    datentyp: str
    feldart: str
    inhalt: str | None
    praezisierung: list[str]
    bezeichnung: list[str]
    hilfetext: list[str]
    schemaelementart: str
    stichwoerter: list[Stichwort] | None

    available_versions: list[AvailableVersion]

    status_gesetzt_durch: str | None
    status_gesetzt_am: str | None
    gueltig_ab: str | None
    gueltig_bis: str | None
    letzte_aenderung: str
    last_update: str
    veroeffentlicht_am: str | None

    relations: UiRelationen

    # NOTE(Felix): Only temporary for the RegMo presentation
    is_bob: bool

    @staticmethod
    def create(
        field: FullDatenfeldOut,
        ui_field_data: UiFieldData,
        versions: list[DatenfeldOut],
        search_query_string: str,
    ) -> FieldIndexPage:
        return FieldIndexPage(
            header=FieldHeader.create(
                field, active_tab="index", search_query_string=search_query_string
            ),
            definition=field.definition,
            beschreibung=field.beschreibung,
            bezug=ui_field_data.bezug,
            versionshinweis=field.versionshinweis,
            datentyp=field.datentyp.value,
            feldart=field.feldart.value,
            inhalt=ui_field_data.inhalt,
            praezisierung=ui_display_praezisierung(ui_field_data.praezisierung),
            bezeichnung=ui_display_eingabe_ausgabe(
                ui_field_data.bezeichnung_eingabe, ui_field_data.bezeichnung_ausgabe
            ),
            hilfetext=ui_display_eingabe_ausgabe(
                ui_field_data.hilfetext_eingabe, ui_field_data.hilfetext_ausgabe
            ),
            schemaelementart=ui_field_data.schemaelementart,
            stichwoerter=ui_field_data.stichwoerter,
            status_gesetzt_durch=field.status_gesetzt_durch,
            available_versions=[
                AvailableVersion.from_field(
                    version, field.fim_version, search_query_string
                )
                for version in versions
            ][::-1],
            veroeffentlicht_am=(
                field.veroeffentlichungsdatum.strftime("%d.%m.%Y")
                if field.veroeffentlichungsdatum
                else None
            ),
            status_gesetzt_am=(
                field.status_gesetzt_am.strftime("%d.%m.%Y")
                if field.status_gesetzt_am
                else None
            ),
            gueltig_ab=(
                field.gueltig_ab.strftime("%d.%m.%Y") if field.gueltig_ab else None
            ),
            gueltig_bis=(
                field.gueltig_bis.strftime("%d.%m.%Y") if field.gueltig_bis else None
            ),
            letzte_aenderung=field.letzte_aenderung.strftime("%d.%m.%Y"),
            last_update=field.last_update.strftime("%d.%m.%Y"),
            relations=UiRelationen.create(field.relation, "field"),
            is_bob=field.nummernkreis == "60000"
            and field.freigabe_status == xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        )


def ui_display_praezisierung(praezisierung: Praezisierung | str | None) -> list[str]:
    """Format a praezisierung for display

    An xdf2 praezisierung is a string that ist shown as is. An xdf3 praezisierung has
    up to three lines with value ranges et cetera.
    """
    if praezisierung is None:
        return []

    if isinstance(praezisierung, str):
        return [praezisierung]

    assert isinstance(praezisierung, Praezisierung)

    display_lines = []
    # NOTE(Dominik): While handling empty strings like None in this context would be fine,
    # we do not want integer zeros to mess with our conditionals which is why I explicitly
    # ask for `is not None`.
    if praezisierung.min_length is not None or praezisierung.max_length is not None:
        length_line = "Länge: "
        if (
            praezisierung.min_length is not None
            and praezisierung.max_length is not None
        ):
            length_line += (
                f"{praezisierung.min_length} - {praezisierung.max_length} Zeichen"
            )
        elif praezisierung.min_length is not None:
            length_line += f"mindestens {praezisierung.min_length} Zeichen"
        else:
            length_line += f"maximal {praezisierung.max_length} Zeichen"

        display_lines.append(length_line)

    if praezisierung.min_value or praezisierung.max_value:
        range_line = "Wertebereich: "
        if praezisierung.min_value and praezisierung.max_value:
            range_line += f"{praezisierung.min_value} - {praezisierung.max_value}"
        elif praezisierung.min_value is not None:
            range_line += f"mindestens {praezisierung.min_value}"
        else:
            range_line += f"maximal {praezisierung.max_value}"

        display_lines.append(range_line)

    if praezisierung.pattern:
        display_lines.append(f"Pattern: {praezisierung.pattern}")

    return display_lines


@dataclass(slots=True)
class FieldReferencesPage:
    """
    Data for templates/field_references.html.jinja
    """

    header: FieldHeader
    schema_references: list[SchemaReference]

    @staticmethod
    def create(
        field: FullDatenfeldOut, search_query_string: str
    ) -> FieldReferencesPage:
        return FieldReferencesPage(
            header=FieldHeader.create(
                field, active_tab="references", search_query_string=search_query_string
            ),
            schema_references=[
                SchemaReference.from_schema_out(schema) for schema in field.schemas
            ],
        )


def create_field_router(
    get_service: Callable[[], AsyncGenerator[Service, Any]],
    render: RenderFunction,
) -> APIRouter:
    router = APIRouter()

    def _field_not_found(request: Request, fim_id: str, fim_version: str):
        return render(
            request,
            "404.html.jinja",
            {
                "message": f"Datenfeld {fim_id} v{fim_version} konnte nicht gefunden werden.",
            },
            status_code=status.HTTP_404_NOT_FOUND,
        )

    @router.get("/{namespace}/{fim_id}/{fim_version}", response_class=HTMLResponse)
    async def _get_field_page(  # type: ignore [reportUnusedFunction]
        request: Request,
        namespace: str,
        fim_id: str,
        fim_version: str,
        service: Service = Depends(get_service),
    ):
        field = await service.get_field(
            namespace=namespace, fim_id=fim_id, fim_version=fim_version
        )
        if field is None:
            return _field_not_found(request, fim_id, fim_version)

        field_message = await service.export_xdf_datenfeld_message(
            namespace, fim_id, fim_version
        )
        assert field_message is not None

        # NOTE(Dominik): This is not optimal, as we only need the actual field here.
        # See also ui_schemas._get_schema_details
        if isinstance(field_message, xdf2.DatenfeldMessage):
            ui_field_data = UiFieldData.from_xdf2_field(field_message.field)
        else:
            ui_field_data = UiFieldData.from_xdf3_field(field_message.field)

        versions = await service.get_field_versions(namespace, fim_id)

        search_query_string = create_clean_query_string(request, Resource.FIELD)

        return render(
            request,
            "field_detail.html.jinja",
            {
                "titleTag": f"{field.fim_id} v{field.fim_version} • Datenfelder • FIM-Portal",
                "page": FieldIndexPage.create(
                    field, ui_field_data, versions, search_query_string
                ),
            },
        )

    @router.get(
        "/{namespace}/{fim_id}/{fim_version}/references", response_class=HTMLResponse
    )
    async def _get_field_references_page(  # type: ignore [reportUnusedFunction]
        request: Request,
        namespace: str,
        fim_id: str,
        fim_version: str,
        service: Service = Depends(get_service),
    ):
        field = await service.get_field(
            namespace=namespace, fim_id=fim_id, fim_version=fim_version
        )
        if field is None:
            return _field_not_found(request, fim_id, fim_version)

        search_query_string = create_clean_query_string(request, Resource.FIELD)

        return render(
            request,
            "field_references.html.jinja",
            {
                "titleTag": f"{field.fim_id} v{field.fim_version} • Datenfelder • FIM-Portal",
                "page": FieldReferencesPage.create(field, search_query_string),
            },
        )

    return router
