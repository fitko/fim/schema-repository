from fimportal.xzufi.common import ModulTextTyp


TEXT_MODUL_TO_GLOSSARY_LINK = {
    ModulTextTyp.LEISTUNGSBEZEICHNUNG: "https://docs.fitko.de/fim/docs/glossar/#leistungsbezeichnung1",
    ModulTextTyp.LEISTUNGSBEZEICHNUNG_II: "https://docs.fitko.de/fim/docs/glossar/#leistungsbezeichnung2",
    ModulTextTyp.RECHTSGRUNDLAGEN: "https://docs.fitko.de/fim/docs/glossar/#handlungsgrundlage",
    ModulTextTyp.VOLLTEXT: "https://docs.fitko.de/fim/docs/glossar/#volltext",
    ModulTextTyp.ZUSTAENDIGE_STELLE: "https://docs.fitko.de/fim/docs/glossar/#zustaendige-stelle",
    ModulTextTyp.TEASER: "https://docs.fitko.de/fim/docs/glossar/#teaser",
    ModulTextTyp.KURZTEXT: "https://docs.fitko.de/fim/docs/glossar/#kurztext",
    ModulTextTyp.ERFORDERLICHE_UNTERLAGEN: "https://docs.fitko.de/fim/docs/glossar/#erforderliche-unterlagen",
    ModulTextTyp.WEITERFUEHRENDE_INFORMATIONEN: "https://docs.fitko.de/fim/docs/glossar/#weiterfuehrende-informationen",
    ModulTextTyp.HINWEISE: "https://docs.fitko.de/fim/docs/glossar/#hinweise-besonderheiten",
    ModulTextTyp.ANSPRECHPUNKT: "https://docs.fitko.de/fim/docs/glossar/#ansprechpunkt",
    ModulTextTyp.FORMUALRE: "https://docs.fitko.de/fim/docs/glossar/#formular",
    ModulTextTyp.RECHTSBEHELF: "https://docs.fitko.de/fim/docs/glossar/#rechtsbehelf",
    ModulTextTyp.VORAUSSETZUNGEN: "https://docs.fitko.de/fim/docs/glossar/#voraussetzungen",
    ModulTextTyp.VERFAHRENSABLAUF: "https://docs.fitko.de/fim/docs/glossar/#verfahrensablauf",
}
