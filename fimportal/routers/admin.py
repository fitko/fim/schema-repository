from typing import Annotated, Any, AsyncGenerator, Callable
import bcrypt
from fastapi import APIRouter, Depends, Form, Query, status
from fastapi.requests import Request
from fastapi.responses import RedirectResponse
from fastapi.security import APIKeyCookie
from fastapi.templating import Jinja2Templates

from fimportal.service import Service
from fimportal.static import StaticFiles


ADMIN_SESSION_TOKEN = "admin-session"
AdminCookie = APIKeyCookie(name=ADMIN_SESSION_TOKEN, auto_error=False)


def create_router(
    admin_password_hash: str,
    get_service: Callable[[], AsyncGenerator[Service, Any]],
    static_files: StaticFiles,
    templates: Jinja2Templates,
) -> APIRouter:
    router = APIRouter()

    def _check_admin_credentials(username: str, password: str) -> bool:
        if username != "admin":
            return False

        if not bcrypt.checkpw(
            password.encode("utf-8"), admin_password_hash.encode("utf-8")
        ):
            return False

        return True

    async def is_admin(
        admin_cookie: Annotated[str | None, Depends(AdminCookie)],
        service: Service = Depends(get_service),
    ) -> bool:
        if admin_cookie is None:
            return False
        else:
            return await service.is_valid_admin_session(admin_cookie)

    @router.get(
        "/",
        include_in_schema=False,
    )
    async def home(  # type: ignore [reportUnusedFunction]
        request: Request,
        is_admin: Annotated[bool, Depends(is_admin)],
        service: Service = Depends(get_service),
        message: Annotated[str | None, Query()] = None,
    ):
        if not is_admin:
            return RedirectResponse("/admin/login")

        api_token = await service.get_all_api_tokens()

        return templates.TemplateResponse(
            request,
            "admin/home.html.jinja",
            {
                "get_static_url": static_files.get_static_url,
                "admin": True,
                "message": message,
                "api_tokens": api_token,
            },
        )

    @router.get("/login", include_in_schema=False)
    async def login_page(  # type: ignore [reportUnusedFunction]
        request: Request, is_admin: Annotated[bool, Depends(is_admin)]
    ):
        if is_admin:
            return RedirectResponse("/admin/")

        return templates.TemplateResponse(
            request,
            "admin/login.html.jinja",
            {
                "get_static_url": static_files.get_static_url,
                "admin": False,
            },
        )

    @router.post(
        "/login",
        include_in_schema=False,
    )
    async def login(  # type: ignore [reportUnusedFunction]
        request: Request,
        username: Annotated[str, Form()],
        password: Annotated[str, Form()],
        is_admin: Annotated[bool, Depends(is_admin)],
        service: Service = Depends(get_service),
    ):
        if is_admin:
            return RedirectResponse("/admin/")

        if not _check_admin_credentials(username, password):
            return templates.TemplateResponse(
                request,
                "admin/login.html.jinja",
                {
                    "get_static_url": static_files.get_static_url,
                    "admin": False,
                    "error": "Ungültige Zugangsdaten",
                },
                status_code=status.HTTP_400_BAD_REQUEST,
            )

        token = await service.create_admin_session()

        response = RedirectResponse("/admin/", status_code=status.HTTP_302_FOUND)
        response.set_cookie(ADMIN_SESSION_TOKEN, token, secure=True, samesite="strict")
        return response

    @router.get("/logout", include_in_schema=False)
    async def logout(  # type: ignore [reportUnusedFunction]
        admin_cookie: Annotated[str | None, Depends(AdminCookie)],
        service: Service = Depends(get_service),
    ):
        if admin_cookie is not None:
            await service.remove_admin_session(admin_cookie)

        response = RedirectResponse("/admin/login")
        response.delete_cookie(ADMIN_SESSION_TOKEN, secure=True, samesite="strict")
        return response

    @router.get("/api-tokens/create", include_in_schema=False)
    async def get_api_token_create(  # type: ignore [reportUnusedFunction]
        request: Request,
        is_admin: Annotated[bool, Depends(is_admin)],
    ):
        if not is_admin:
            return RedirectResponse("/admin/login")

        return templates.TemplateResponse(
            request,
            "admin/api-token-create.html.jinja",
            {
                "get_static_url": static_files.get_static_url,
                "admin": True,
            },
        )

    @router.post("/api-tokens/create", include_in_schema=False)
    async def create_api_token(  # type: ignore [reportUnusedFunction]
        request: Request,
        is_admin: Annotated[bool, Depends(is_admin)],
        nummernkreis: Annotated[str, Form()],
        description: Annotated[str, Form()],
        service: Service = Depends(get_service),
    ):
        if not is_admin:
            return RedirectResponse("/admin/login")

        token = await service.create_api_token(nummernkreis, description)

        return templates.TemplateResponse(
            request,
            "admin/api-token-created.html.jinja",
            {
                "get_static_url": static_files.get_static_url,
                "admin": True,
                "token": token,
            },
        )

    @router.get("/api-tokens/{token_id}", include_in_schema=False)
    async def get_api_token_details(  # type: ignore [reportUnusedFunction]
        request: Request,
        is_admin: Annotated[bool, Depends(is_admin)],
        token_id: int,
        service: Service = Depends(get_service),
    ):
        # Todo: But into middleware or something
        if not is_admin:
            return RedirectResponse("/admin/login")

        api_token = await service.get_api_token(token_id)

        return templates.TemplateResponse(
            "admin/api-token-details.html.jinja",
            {
                "get_static_url": static_files.get_static_url,
                "request": request,
                "admin": True,
                "api_token": api_token,
            },
        )

    @router.get("/api-tokens/{token_id}/delete", include_in_schema=False)
    async def get_api_token_delete(  # type: ignore [reportUnusedFunction]
        request: Request,
        is_admin: Annotated[bool, Depends(is_admin)],
        token_id: int,
        service: Service = Depends(get_service),
    ):
        # Todo: But into middleware or something
        if not is_admin:
            return RedirectResponse("/admin/login")

        api_token = await service.get_api_token(token_id)

        return templates.TemplateResponse(
            "admin/api-token-delete.html.jinja",
            {
                "get_static_url": static_files.get_static_url,
                "request": request,
                "admin": True,
                "api_token": api_token,
            },
        )

    @router.post("/api-tokens/{token_id}/delete", include_in_schema=False)
    async def delete_api_token(  # type: ignore [reportUnusedFunction]
        is_admin: Annotated[bool, Depends(is_admin)],
        token_id: int,
        service: Service = Depends(get_service),
    ):
        # Todo: But into middleware or something
        if not is_admin:
            return RedirectResponse("/admin/login", status_code=status.HTTP_302_FOUND)

        await service.delete_api_token(token_id)

        return RedirectResponse(
            "/admin/?message=API Token erfolgreich gelöscht.",
            status_code=status.HTTP_302_FOUND,
        )

    return router
