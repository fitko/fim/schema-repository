from __future__ import annotations

import copy
import re
import time
import urllib.parse
from dataclasses import dataclass
from enum import Enum
from typing import Annotated, Any, AsyncGenerator, Callable, TypeVar

from fastapi import APIRouter, Depends, Query, Request
from starlette.responses import HTMLResponse

from fimportal.common import Anwendungsgebiet, FreigabeStatus
from fimportal.database import (
    FeldSucheIn,
    FieldSearchOptions,
    GroupSearchOptions,
    GruppeSucheIn,
    LeistungSucheIn,
    LeistungsbeschreibungSearchOptions,
    LeistungsbeschreibungSearchOrder,
    LeistungssteckbriefSearchOptions,
    LeistungssteckbriefSearchOrder,
    ProzessklasseSearchOptions,
    ProzessSearchOptions,
    SchemaSearchOptions,
    SchemaSearchOrder,
    SchemaSucheIn,
    SteckbriefSearchOptions,
    SteckbriefSucheIn,
)
from fimportal.helpers import RenderFunction, remove_html_tags
from fimportal.models.imports import XdfVersion
from fimportal.models.prozess import ProzessklasseOut, ProzessOut
from fimportal.models.xdf3 import (
    DatenfeldgruppeOut,
    DatenfeldOut,
    SchemaOut,
    SteckbriefOut,
)
from fimportal.models.xzufi import LeistungsbeschreibungOut, LeistungssteckbriefOut
from fimportal.pagination import (
    AvailablePages,
    PaginationOptions,
    get_ui_pagination_options,
)
from fimportal.routers.common import create_clean_query_string, Resource
from fimportal.routers.ui_xzufi import REDAKTION_ID_TO_ICON
from fimportal.service import Service
from fimportal.xdatenfelder.xdf3.common import Datentyp, Dokumentart, Feldart
from fimportal.xprozesse.prozess import (
    Detaillierungsstufe,
    Handlungsform,
    OperativesZiel,
    Verfahrensart,
)
from fimportal.xzufi.common import (
    LANDESREDAKTIONEN,
    REDAKTION_ID_TO_LABEL,
    LeistungRedaktionId,
    LeistungsTypisierung,
    SdgRelevanz,
)
from fimportal.xzufi.leistung import LeistungsTyp

E = TypeVar("E", bound=Enum)

MATCH_START_MARKER = "[[["
MATCH_STOP_MARKER = "]]]"


def _add_highlights(match: str) -> str:
    return match.replace(MATCH_START_MARKER, "<b><u>").replace(
        MATCH_STOP_MARKER, "</u></b>"
    )


def _add_fts_highlights(fts_match: str) -> str:
    cleaned_match = remove_html_tags(
        remove_html_tags(_remove_uri_from_string(fts_match))
    )
    formatted_match = f"... {_add_highlights(cleaned_match)} ..."
    return formatted_match


def _remove_uri_from_string(string: str) -> str:
    pattern = r"https?://\S+|www\.\S+"
    cleaned_text = re.sub(pattern, "", string)

    # Optionally, you may want to remove extra spaces left behind after URL removal
    cleaned_text = re.sub(r"\s+", " ", cleaned_text).strip()

    return cleaned_text


def _add_suche_in_highlights(content: str, search_string: str | None):
    if search_string is None:
        return content

    pattern = re.compile(r"\b\w*" + re.escape(search_string) + r"\w*\b", re.IGNORECASE)

    def add_markers(match: re.Match[str]):
        return f"{MATCH_START_MARKER}{match.group(0)}{MATCH_STOP_MARKER}"

    marked_content = pattern.sub(add_markers, content)
    return _add_highlights(marked_content)


def _get_header_date_string(
    leistung: LeistungssteckbriefOut | LeistungsbeschreibungOut,
    sort_order: LeistungssteckbriefSearchOrder | LeistungsbeschreibungSearchOrder,
):
    if (
        sort_order == LeistungssteckbriefSearchOrder.ERSTELLT_DATUM_ZEIT_ASC
        or sort_order == LeistungssteckbriefSearchOrder.ERSTELLT_DATUM_ZEIT_DESC
        or sort_order == LeistungsbeschreibungSearchOrder.ERSTELLT_DATUM_ZEIT_ASC
        or sort_order == LeistungsbeschreibungSearchOrder.ERSTELLT_DATUM_ZEIT_DESC
    ):
        return (
            f"Erstellt am {leistung.erstellt_datum_zeit.strftime('%d.%m.%Y')}"
            if leistung.erstellt_datum_zeit is not None
            else "Unbekannter Erstellungszeitpunkt"
        )
    if (
        sort_order == LeistungssteckbriefSearchOrder.AKTUALISIERT_DATUM_ZEIT_ASC
        or sort_order == LeistungssteckbriefSearchOrder.AKTUALISIERT_DATUM_ZEIT_DESC
        or sort_order == LeistungsbeschreibungSearchOrder.AKTUALISIERT_DATUM_ZEIT_ASC
        or sort_order == LeistungsbeschreibungSearchOrder.AKTUALISIERT_DATUM_ZEIT_DESC
    ):
        return (
            f"Geändert am {leistung.geaendert_datum_zeit.strftime('%d.%m.%Y')}"
            if leistung.geaendert_datum_zeit is not None
            else "Unbekannter Änderungszeitpunkt"
        )
    return None


@dataclass(slots=True)
class LeistungssteckbriefSearchResult:
    leistungsschluessel: str
    typ: str | None
    typisierung: str
    sdg_relevanz: bool | None
    title: str
    header_date_string: str | None
    beschreibung: str | list[str] | None

    freigabestatus_katalog: str
    freigabestatus_bibliothek: str
    redaktionen_overview: list[LeistungsbeschreibungsReferenz]

    detail_link: str

    @staticmethod
    def from_leistungssteckbrief(
        leistung: LeistungssteckbriefOut,
        sort_order: LeistungssteckbriefSearchOrder,
        suche_in: LeistungSucheIn | None,
        search_string: str | None,
        search_query_string: str,
    ) -> LeistungssteckbriefSearchResult:
        if leistung.fts_match:
            beschreibung = _add_fts_highlights(leistung.fts_match)
        elif suche_in == LeistungSucheIn.RECHTSGRUNDLAGEN:
            if leistung.rechtsgrundlagen:
                beschreibung = [
                    _add_suche_in_highlights(content, search_string)
                    for content in leistung.rechtsgrundlagen.splitlines()
                ]
            else:
                beschreibung = None
        elif suche_in == LeistungSucheIn.LEISTUNGSBEZEICHNUNG:
            if leistung.leistungsbezeichnung:
                beschreibung = _add_suche_in_highlights(
                    leistung.leistungsbezeichnung, search_string
                )
            else:
                beschreibung = None
        elif suche_in == LeistungSucheIn.LEISTUNGSBEZEICHNUNG_II:
            if leistung.leistungsbezeichnung_2:
                beschreibung = _add_suche_in_highlights(
                    leistung.leistungsbezeichnung_2, search_string
                )
            else:
                beschreibung = None
        else:
            beschreibung = None

        redaktionen_overview = _get_redaktionen_overview(
            leistung.leistungsbeschreibungen, search_query_string
        )

        freigabestatus_bibliothek = (
            leistung.freigabestatus_bibliothek.to_label()
            if leistung.freigabestatus_bibliothek
            else "unbestimmter Freigabestatus"
        )

        return LeistungssteckbriefSearchResult(
            leistungsschluessel=leistung.leistungsschluessel,
            typ=(
                leistung.leistungstyp.to_short_label()
                if leistung.leistungstyp
                else None
            ),
            typisierung=", ".join([f"Typ {i.value}" for i in leistung.typisierung]),
            sdg_relevanz=SdgRelevanz.from_sdg_informationsbereiche(
                leistung.sdg_informationsbereiche
            ).is_sdg_relevant(),
            title=leistung.title,
            header_date_string=_get_header_date_string(leistung, sort_order),
            beschreibung=beschreibung,
            freigabestatus_katalog=(
                leistung.freigabestatus_katalog.to_label()
                if leistung.freigabestatus_katalog
                else "unbestimmter Freigabestatus"
            ),
            freigabestatus_bibliothek=freigabestatus_bibliothek,
            redaktionen_overview=redaktionen_overview,
            detail_link=f"/services/{leistung.leistungsschluessel}{search_query_string}",
        )


@dataclass(slots=True)
class LeistungsModulInfo:
    name: str
    exists: bool


@dataclass(slots=True)
class LeistungsbeschreibungsReferenz:
    icon: str | None
    label: str
    href: str


def _get_redaktionen_overview(
    leistungsbeschreibungen: list[tuple[str, str]], search_query_string: str
) -> list[LeistungsbeschreibungsReferenz]:
    return sorted(
        [
            LeistungsbeschreibungsReferenz(
                icon=REDAKTION_ID_TO_ICON.get(redaktion_id),
                label=REDAKTION_ID_TO_LABEL.get(redaktion_id, redaktion_id),
                href=f"/xzufi-services/{redaktion_id}/{leistung_id}{search_query_string}",
            )
            for redaktion_id, leistung_id in leistungsbeschreibungen
        ],
        key=lambda x: x.label,
    )


@dataclass(slots=True)
class LeistungsbeschreibungSearchResult:
    redaktion_id: str
    redaktion_label: str
    redaktion_icon: str | None
    leistung_id: str
    title: str
    hierarchy_type: str
    beschreibung: str | list[str] | None
    header_date_string: str | None
    typisierung: str | None
    leistungsschluessel: str | None

    detail_link: str

    @staticmethod
    def from_leistungsbeschreibung(
        leistung: LeistungsbeschreibungOut,
        sort_order: LeistungsbeschreibungSearchOrder,
        suche_in: LeistungSucheIn | None,
        search_string: str | None,
        search_query_string: str,
    ) -> LeistungsbeschreibungSearchResult:
        if leistung.fts_match:
            beschreibung = _add_fts_highlights(leistung.fts_match)
        elif suche_in == LeistungSucheIn.RECHTSGRUNDLAGEN:
            if leistung.rechtsgrundlagen:
                beschreibung = [
                    _add_suche_in_highlights(content, search_string)
                    for content in leistung.rechtsgrundlagen.splitlines()
                ]
            else:
                beschreibung = None
        elif suche_in == LeistungSucheIn.LEISTUNGSBEZEICHNUNG:
            if leistung.leistungsbezeichnung:
                beschreibung = _add_suche_in_highlights(
                    leistung.leistungsbezeichnung, search_string
                )
            else:
                beschreibung = None
        elif suche_in == LeistungSucheIn.LEISTUNGSBEZEICHNUNG_II:
            if leistung.leistungsbezeichnung_2:
                beschreibung = _add_suche_in_highlights(
                    leistung.leistungsbezeichnung_2, search_string
                )
            else:
                beschreibung = None
        else:
            beschreibung = None

        return LeistungsbeschreibungSearchResult(
            redaktion_id=leistung.redaktion_id,
            redaktion_label=REDAKTION_ID_TO_LABEL.get(
                leistung.redaktion_id, leistung.redaktion_id
            ),
            redaktion_icon=REDAKTION_ID_TO_ICON.get(leistung.redaktion_id),
            leistung_id=leistung.id,
            title=leistung.title,
            hierarchy_type=leistung.hierarchy_type.to_label(),
            beschreibung=beschreibung,
            header_date_string=_get_header_date_string(leistung, sort_order),
            typisierung=(
                ", ".join([f"Typ {typ.value}" for typ in leistung.typisierung])
                if len(leistung.typisierung) > 0
                else None
            ),
            leistungsschluessel=(
                ", ".join(leistung.leistungsschluessel)
                if len(leistung.leistungsschluessel) > 0
                else None
            ),
            detail_link=f"/xzufi-services/{leistung.redaktion_id}/{leistung.id}{search_query_string}",
        )


@dataclass(slots=True)
class GroupSearchResult:
    fim_id: str
    fim_version: str
    name: str
    beschreibung: str | list[str] | None
    freigabe_status: str
    xdf_version: str
    last_update: str
    detail_link: str

    # NOTE(Felix): Only temporary for the RegMo presentation
    is_bob: bool

    @staticmethod
    def from_group(
        group: DatenfeldgruppeOut,
        suche_in: GruppeSucheIn | None,
        search_string: str | None,
        search_query_string: str,
    ) -> GroupSearchResult:
        if group.fts_match is not None:
            beschreibung = _add_fts_highlights(group.fts_match)
        elif suche_in == GruppeSucheIn.RECHTSGRUNDLAGEN:
            beschreibung = [
                _add_suche_in_highlights(content, search_string)
                for content in group.bezug
            ]
        elif suche_in == GruppeSucheIn.STATUS_GESETZT_DURCH:
            beschreibung = (
                "<i>Status gesetzt durch: </i>"
                + _add_suche_in_highlights(group.status_gesetzt_durch, search_string)
                if group.status_gesetzt_durch is not None
                else None
            )
        elif suche_in == GruppeSucheIn.VERSIONSHINWEIS:
            beschreibung = (
                "<i>Versionshinweis: </i>"
                + _add_suche_in_highlights(group.versionshinweis, search_string)
                if group.versionshinweis is not None
                else None
            )
        else:
            beschreibung = group.definition or group.beschreibung

        return GroupSearchResult(
            fim_id=group.fim_id,
            fim_version="v" + group.fim_version,
            name=group.name,
            beschreibung=beschreibung,
            freigabe_status=group.freigabe_status_label,
            xdf_version="xdf " + group.xdf_version.value,
            last_update=group.last_update.strftime("%d.%m.%Y"),
            detail_link=f"/groups/{group.namespace}/{group.fim_id}/{group.fim_version}{search_query_string}",
            is_bob=group.nummernkreis == "60000"
            and group.freigabe_status == FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        )


@dataclass(slots=True)
class FieldSearchResult:
    fim_id: str
    fim_version: str
    name: str
    beschreibung: str | list[str] | None
    freigabe_status: str
    xdf_version: str
    last_update: str
    detail_link: str

    # NOTE(Felix): Only temporary for the RegMo presentation
    is_bob: bool

    @staticmethod
    def from_field(
        field: DatenfeldOut,
        suche_in: FeldSucheIn | None,
        search_string: str | None,
        search_query_string: str,
    ) -> FieldSearchResult:
        if field.fts_match is not None:
            beschreibung = _add_fts_highlights(field.fts_match)
        elif suche_in == FeldSucheIn.RECHTSGRUNDLAGEN:
            beschreibung = [
                _add_suche_in_highlights(content, search_string)
                for content in field.bezug
            ]
        elif suche_in == FeldSucheIn.STATUS_GESETZT_DURCH:
            beschreibung = (
                "<i>Status gesetzt durch: </i>"
                + _add_suche_in_highlights(field.status_gesetzt_durch, search_string)
                if field.status_gesetzt_durch is not None
                else None
            )
        elif suche_in == FeldSucheIn.VERSIONSHINWEIS:
            beschreibung = (
                "<i>Versionshinweis: </i>"
                + _add_suche_in_highlights(field.versionshinweis, search_string)
                if field.versionshinweis is not None
                else None
            )
        else:
            beschreibung = field.definition or field.beschreibung

        return FieldSearchResult(
            fim_id=field.fim_id,
            fim_version="v" + field.fim_version,
            name=field.name,
            beschreibung=beschreibung,
            freigabe_status=field.freigabe_status_label,
            xdf_version="xdf " + field.xdf_version.value,
            last_update=field.last_update.strftime("%d.%m.%Y"),
            detail_link=f"/fields/{field.namespace}/{field.fim_id}/{field.fim_version}{search_query_string}",
            is_bob=field.nummernkreis == "60000"
            and field.freigabe_status == FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        )


@dataclass(slots=True)
class SteckbriefSearchResult:
    fim_id: str
    fim_version: str
    name: str
    beschreibung: str | list[str] | None
    freigabe_status: str
    status_gesetzt_durch: str
    xdf_version: str
    last_update: str

    detail_link: str

    @staticmethod
    def from_steckbrief(
        steckbrief: SteckbriefOut,
        suche_in: SteckbriefSucheIn | None,
        search_string: str | None,
        search_query_string: str,
    ) -> SteckbriefSearchResult:
        if steckbrief.fts_match:
            beschreibung = _add_fts_highlights(steckbrief.fts_match)
        elif suche_in == SteckbriefSucheIn.RECHTSGRUNDLAGEN:
            beschreibung = [
                _add_suche_in_highlights(content, search_string)
                for content in steckbrief.bezug
            ]
        elif suche_in == SteckbriefSucheIn.STATUS_GESETZT_DURCH:
            beschreibung = (
                "<i>Status gesetzt durch: </i>"
                + _add_suche_in_highlights(
                    steckbrief.status_gesetzt_durch, search_string
                )
                if steckbrief.status_gesetzt_durch is not None
                else None
            )
        elif suche_in == SteckbriefSucheIn.VERSIONSHINWEIS:
            beschreibung = (
                "<i>Versionshinweis: </i>"
                + _add_suche_in_highlights(steckbrief.versionshinweis, search_string)
                if steckbrief.versionshinweis is not None
                else None
            )
        else:
            beschreibung = steckbrief.definition or steckbrief.beschreibung

        return SteckbriefSearchResult(
            fim_id=steckbrief.fim_id,
            fim_version="v" + steckbrief.fim_version,
            name=steckbrief.name,
            beschreibung=beschreibung,
            freigabe_status=steckbrief.freigabe_status_label,
            xdf_version="xdf " + steckbrief.xdf_version.value,
            status_gesetzt_durch=(steckbrief.status_gesetzt_durch or "-"),
            last_update=steckbrief.last_update.strftime("%d.%m.%Y"),
            detail_link=f"/document-profiles/{steckbrief.fim_id}/{steckbrief.fim_version}{search_query_string}",
        )


@dataclass(slots=True)
class SchemaSearchResult:
    fim_id: str
    fim_version: str
    name: str
    beschreibung: str | list[str] | None
    freigabe_status: str
    status_gesetzt_durch: str
    xdf_version: str
    last_update: str

    detail_link: str

    @staticmethod
    def from_schema_out(
        schema: SchemaOut,
        suche_in: SchemaSucheIn | None,
        search_string: str | None,
        search_query_string: str,
    ) -> SchemaSearchResult:
        if schema.fts_match:
            beschreibung = _add_fts_highlights(schema.fts_match)
        elif suche_in == SchemaSucheIn.RECHTSGRUNDLAGEN:
            beschreibung = [
                _add_suche_in_highlights(content, search_string)
                for content in schema.bezug
            ]
        elif suche_in == SchemaSucheIn.STATUS_GESETZT_DURCH:
            beschreibung = (
                "<i>Status gesetzt durch: </i>"
                + _add_suche_in_highlights(schema.status_gesetzt_durch, search_string)
                if schema.status_gesetzt_durch is not None
                else None
            )
        elif suche_in == SchemaSucheIn.VERSIONSHINWEIS:
            beschreibung = (
                "<i>Versionshinweis: </i>"
                + _add_suche_in_highlights(schema.versionshinweis, search_string)
                if schema.versionshinweis is not None
                else None
            )
        elif suche_in == SchemaSucheIn.STICHWORT:
            beschreibung = [
                _add_suche_in_highlights(content, search_string)
                for content in schema.stichwort
            ]
        else:
            beschreibung = schema.definition or schema.beschreibung

        return SchemaSearchResult(
            fim_id=schema.fim_id,
            fim_version="v" + schema.fim_version,
            name=schema.name,
            beschreibung=beschreibung,
            freigabe_status=schema.freigabe_status_label,
            xdf_version="xdf " + schema.xdf_version.value,
            status_gesetzt_durch=(schema.status_gesetzt_durch or "-"),
            last_update=schema.last_update.strftime("%d.%m.%Y"),
            detail_link="/schemas/"
            + schema.fim_id
            + "/"
            + schema.fim_version
            + search_query_string,
        )


@dataclass(slots=True)
class ProzessSearchResult:
    id: str
    version: str
    name: str
    freigabe_status: str
    letzter_aenderungszeitpunkt: str
    beschreibung: str | None

    detail_link: str

    @staticmethod
    def from_prozess(
        prozess: ProzessOut, search_query_string: str
    ) -> ProzessSearchResult:
        beschreibung = (
            _add_fts_highlights(prozess.fts_match)
            if prozess.fts_match is not None
            else None
        )
        return ProzessSearchResult(
            id=prozess.id,
            version=prozess.version or "-",
            name=prozess.name,
            freigabe_status=(
                prozess.freigabe_status.to_label()
                if prozess.freigabe_status
                else "unbestimmter Freigabestatus"
            ),
            letzter_aenderungszeitpunkt=(
                f"Geändert am {prozess.letzter_aenderungszeitpunkt.strftime('%d.%m.%Y')}"
                if prozess.letzter_aenderungszeitpunkt is not None
                else "Unbekannter Änderungszeitpunkt"
            ),
            beschreibung=beschreibung,
            detail_link=f"/processes/{prozess.id}{search_query_string}",
        )


@dataclass(slots=True)
class ProzessklasseSearchResult:
    id: str
    version: str
    name: str
    freigabe_status: str
    letzter_aenderungszeitpunkt: str
    beschreibung: str | None

    detail_link: str

    @staticmethod
    def from_prozessklasse(
        prozessklasse: ProzessklasseOut,
        search_query_string: str,
    ) -> ProzessklasseSearchResult:
        beschreibung = (
            _add_fts_highlights(prozessklasse.fts_match)
            if prozessklasse.fts_match is not None
            else None
        )
        return ProzessklasseSearchResult(
            id=prozessklasse.id,
            version=prozessklasse.version or "-",
            name=prozessklasse.name,
            freigabe_status=(
                prozessklasse.freigabe_status.to_label()
                if prozessklasse.freigabe_status is not None
                else "unbestimmter Freigabestatus"
            ),
            letzter_aenderungszeitpunkt=(
                f"Geändert am {prozessklasse.letzter_aenderungszeitpunkt.strftime('%d.%m.%Y')}"
                if prozessklasse.letzter_aenderungszeitpunkt is not None
                else "Unbekannter Änderungszeitpunkt"
            ),
            beschreibung=beschreibung,
            detail_link=f"/processclasses/{prozessklasse.id}{search_query_string}",
        )


class Baustein(Enum):
    LEISTUNG = "leistung"
    DATENFELDER = "datenfelder"
    PROZESSE = "prozesse"


class EinheitlicherAnsprechpartner(Enum):
    TRUE = "Ja"
    FALSE = "Nein"

    def to_bool(self) -> bool:
        return self == EinheitlicherAnsprechpartner.TRUE


@dataclass(slots=True)
class UiFilters:
    resource: Resource
    term: str | None = None
    freigabe_status: list[FreigabeStatus] | None = None
    freigabe_status_katalog: FreigabeStatus | None = None
    freigabe_status_bibliothek: FreigabeStatus | None = None
    leistung_redaktion_id: LeistungRedaktionId | None = None
    leistungstyp: LeistungsTyp | None = None
    leistungstypisierung: LeistungsTypisierung | None = None
    leistung_einheitlicher_ansprechpartner: EinheitlicherAnsprechpartner | None = None
    leistung_suche_in: LeistungSucheIn | None = None
    schema_suche_in: SchemaSucheIn | None = None
    steckbrief_suche_in: SteckbriefSucheIn | None = None
    gruppe_suche_in: GruppeSucheIn | None = None
    feld_suche_in: FeldSucheIn | None = None
    xdf_version: XdfVersion | None = None
    nummernkreis: NummernkreisOption | None = None
    feldart: Feldart | None = None
    datentyp: Datentyp | None = None
    operatives_ziel: OperativesZiel | None = None
    verfahrensart: Verfahrensart | None = None
    handlungsform: Handlungsform | None = None
    detaillierungsstufe: Detaillierungsstufe | None = None
    anwendungsgebiet: Anwendungsgebiet | None = None
    dokumentart: Dokumentart | None = None

    # This parameter is called the same for all the different types of resources.
    # Since the actual set of valid options depends on the selected resource, this filter
    # is only decoded on demand when needed and therefore just saved as an unvalidated string in here.
    order_by: str | None = None

    def _get_search_params(self) -> str:
        params: dict[str, str] = {"resource": self.resource.value}

        if self.term is not None:
            params["term"] = self.term

        if self.freigabe_status is not None:
            for status in self.freigabe_status:
                params[f"freigabe_status_{status.name.lower()}"] = "on"

        if self.freigabe_status_katalog is not None:
            params["freigabe_status_katalog"] = str(self.freigabe_status_katalog.value)

        if self.freigabe_status_bibliothek is not None:
            params["freigabe_status_bibliothek"] = str(
                self.freigabe_status_bibliothek.value
            )

        if self.leistung_redaktion_id is not None:
            params["leistung_redaktion_id"] = self.leistung_redaktion_id.value

        if self.leistungstyp is not None:
            params["leistungstyp"] = self.leistungstyp.value

        if self.leistungstypisierung is not None:
            params["leistungstypisierung"] = self.leistungstypisierung.value

        if self.leistung_einheitlicher_ansprechpartner is not None:
            params["leistung_einheitlicher_ansprechpartner"] = str(
                self.leistung_einheitlicher_ansprechpartner.value
            )

        if self.leistung_suche_in is not None:
            params["leistung_suche_in"] = self.leistung_suche_in.value

        if self.schema_suche_in is not None:
            params["schema_suche_in"] = self.schema_suche_in.value

        if self.steckbrief_suche_in is not None:
            params["steckbrief_suche_in"] = self.steckbrief_suche_in.value

        if self.gruppe_suche_in is not None:
            params["gruppe_suche_in"] = self.gruppe_suche_in.value

        if self.feld_suche_in is not None:
            params["feld_suche_in"] = self.feld_suche_in.value

        if self.xdf_version is not None:
            params["xdf_version"] = self.xdf_version.value

        if self.nummernkreis is not None:
            params["nummernkreis"] = self.nummernkreis.value

        if self.feldart is not None:
            params["feldart"] = self.feldart.value

        if self.datentyp is not None:
            params["datentyp"] = self.datentyp.value

        if self.order_by is not None:
            params["order_by"] = self.order_by

        if self.operatives_ziel is not None:
            params["operatives_ziel"] = self.operatives_ziel.value

        if self.verfahrensart is not None:
            params["verfahrensart"] = self.verfahrensart.value

        if self.handlungsform is not None:
            params["handlungsform"] = self.handlungsform.value

        if self.detaillierungsstufe is not None:
            params["detaillierungsstufe"] = self.detaillierungsstufe.value

        if self.anwendungsgebiet is not None:
            params["anwendungsgebiet"] = self.anwendungsgebiet.value

        if self.dokumentart is not None:
            params["dokumentart"] = self.dokumentart.value

        return urllib.parse.urlencode(params)

    def to_search_url(self) -> str:
        return f"/search?{self._get_search_params()}"

    def to_csv_download_url(self) -> str:
        return f"/tools/search-csv-download?{self._get_search_params()}"

    def get_leistungsbeschreibung_search_order(
        self,
    ) -> LeistungsbeschreibungSearchOrder:
        try:
            return LeistungsbeschreibungSearchOrder(self.order_by)
        except ValueError:
            return LeistungsbeschreibungSearchOrder.RELEVANZ

    def get_leistungssteckbrief_search_order(self) -> LeistungssteckbriefSearchOrder:
        try:
            return LeistungssteckbriefSearchOrder(self.order_by)
        except ValueError:
            return LeistungssteckbriefSearchOrder.RELEVANZ

    def get_schema_search_order(self) -> SchemaSearchOrder:
        try:
            return SchemaSearchOrder(self.order_by)
        except ValueError:
            return SchemaSearchOrder.NAME_ASC

    def to_leistungssteckbrief_search_options(self) -> LeistungssteckbriefSearchOptions:
        return LeistungssteckbriefSearchOptions(
            fts_query=(
                self.term
                if self.leistung_suche_in != LeistungSucheIn.LEISTUNGSSCHLUESSEL
                else None
            ),
            leistungstyp=self.leistungstyp,
            typisierung=self.leistungstypisierung,
            leistungsschluessel=(
                self.term
                if self.leistung_suche_in == LeistungSucheIn.LEISTUNGSSCHLUESSEL
                else None
            ),
            suche_nur_in=self.leistung_suche_in,
            freigabe_status_katalog=self.freigabe_status_katalog,
            freigabe_status_bibliothek=self.freigabe_status_bibliothek,
            einheitlicher_ansprechpartner=self.leistung_einheitlicher_ansprechpartner.to_bool()
            if self.leistung_einheitlicher_ansprechpartner is not None
            else None,
            order_by=self.get_leistungssteckbrief_search_order(),
        )

    def to_leistungsbeschreibung_search_options(
        self,
    ) -> LeistungsbeschreibungSearchOptions:
        return LeistungsbeschreibungSearchOptions(
            redaktion_id=(
                self.leistung_redaktion_id.value
                if self.leistung_redaktion_id is not None
                else None
            ),
            fts_query=(
                self.term
                if self.leistung_suche_in != LeistungSucheIn.LEISTUNGSSCHLUESSEL
                else None
            ),
            leistungsschluessel=(
                self.term
                if self.leistung_suche_in == LeistungSucheIn.LEISTUNGSSCHLUESSEL
                else None
            ),
            suche_nur_in=self.leistung_suche_in,
            leistungstyp=self.leistungstyp,
            typisierung=self.leistungstypisierung,
            einheitlicher_ansprechpartner=self.leistung_einheitlicher_ansprechpartner.to_bool()
            if self.leistung_einheitlicher_ansprechpartner is not None
            else None,
            order_by=self.get_leistungsbeschreibung_search_order(),
        )

    def to_schema_search_options(self) -> SchemaSearchOptions:
        return SchemaSearchOptions(
            fts_query=self.term,
            nummernkreis=self.nummernkreis.value if self.nummernkreis else None,
            suche_nur_in=self.schema_suche_in,
            freigabe_status=self.freigabe_status,
            xdf_version=self.xdf_version,
            is_latest=(None if self.freigabe_status else True),
            order_by=self.get_schema_search_order(),
        )

    def to_steckbrief_search_options(self) -> SteckbriefSearchOptions:
        return SteckbriefSearchOptions(
            fts_query=self.term,
            nummernkreis=self.nummernkreis.value if self.nummernkreis else None,
            suche_nur_in=self.steckbrief_suche_in,
            freigabe_status=self.freigabe_status,
            xdf_version=self.xdf_version,
            dokumentart=self.dokumentart,
            is_latest=(None if self.freigabe_status else True),
        )

    def to_group_search_options(self) -> GroupSearchOptions:
        return GroupSearchOptions(
            fts_query=self.term,
            nummernkreis=self.nummernkreis.value if self.nummernkreis else None,
            suche_nur_in=self.gruppe_suche_in,
            freigabe_status=self.freigabe_status,
            xdf_version=self.xdf_version,
            is_latest=None if self.freigabe_status else True,
        )

    def to_field_search_options(self) -> FieldSearchOptions:
        return FieldSearchOptions(
            fts_query=self.term,
            nummernkreis=self.nummernkreis.value if self.nummernkreis else None,
            suche_nur_in=self.feld_suche_in,
            freigabe_status=self.freigabe_status,
            xdf_version=self.xdf_version,
            feldart=self.feldart,
            datentyp=self.datentyp,
            is_latest=None if self.freigabe_status else True,
        )

    def to_prozess_search_options(self) -> ProzessSearchOptions:
        return ProzessSearchOptions(
            is_musterprozess=False,
            freigabe_status=self.freigabe_status,
            detaillierungsstufe=self.detaillierungsstufe,
            anwendungsgebiet=self.anwendungsgebiet,
            fts_query=self.term,
        )

    def to_musterprozess_search_options(self) -> ProzessSearchOptions:
        return ProzessSearchOptions(
            is_musterprozess=True,
            freigabe_status=self.freigabe_status,
            detaillierungsstufe=self.detaillierungsstufe,
            anwendungsgebiet=self.anwendungsgebiet,
            fts_query=self.term,
        )

    def to_prozessklasse_search_options(self) -> ProzessklasseSearchOptions:
        return ProzessklasseSearchOptions(
            fts_query=self.term,
            freigabe_status=self.freigabe_status,
            operatives_ziel=self.operatives_ziel,
            verfahrensart=self.verfahrensart,
            handlungsform=self.handlungsform,
        )


def _parse_nullable_enum(enum_type: type[E], value: str | None) -> E | None:
    """
    Some filters use a select form input with a default "Kein Filter" option.
    De-selecting the filter requires an additional option which is interpreted as "None", as having select
    options without a value is not supported.
    This is solved by mapping any non-valid value to `None`, so that we can use a special value like
    `undefined` for the empty select option.
    """
    if value is None:
        return None

    try:
        return enum_type(value)
    except ValueError:
        return None


def _parse_multiple_freigabe_status(
    freigabe_status_in_planung: bool | None,
    freigabe_status_in_bearbeitung: bool | None,
    freigabe_status_entwurf: bool | None,
    freigabe_status_methodisch_freigegeben: bool | None,
    freigabe_status_fachlich_freigegeben_silber: bool | None,
    freigabe_status_fachlich_freigegeben_gold: bool | None,
    freigabe_status_inaktiv: bool | None,
    freigabe_status_vorgesehen_zum_loeschen: bool | None,
) -> list[FreigabeStatus] | None:
    status: list[FreigabeStatus] = []
    if freigabe_status_in_planung:
        status.append(FreigabeStatus.IN_PLANUNG)
    if freigabe_status_in_bearbeitung:
        status.append(FreigabeStatus.IN_BEARBEITUNG)
    if freigabe_status_entwurf:
        status.append(FreigabeStatus.ENTWURF)
    if freigabe_status_methodisch_freigegeben:
        status.append(FreigabeStatus.METHODISCH_FREIGEGEBEN)
    if freigabe_status_fachlich_freigegeben_silber:
        status.append(FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER)
    if freigabe_status_fachlich_freigegeben_gold:
        status.append(FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD)
    if freigabe_status_inaktiv:
        status.append(FreigabeStatus.INAKTIV)
    if freigabe_status_vorgesehen_zum_loeschen:
        status.append(FreigabeStatus.VORGESEHEN_ZUM_LOESCHEN)
    if len(status) > 0:
        return status
    return None


def _parse_nullable_int_enum(enum_type: type[E], value: str | None) -> E | None:
    if value is None:
        return None

    try:
        return enum_type(int(value))
    except ValueError:
        return None


def get_ui_filters(
    resource: Annotated[str | None, Query()] = None,
    term: Annotated[str | None, Query()] = None,
    freigabe_status_in_planung: Annotated[bool | None, Query] = None,
    freigabe_status_in_bearbeitung: Annotated[bool | None, Query] = None,
    freigabe_status_entwurf: Annotated[bool | None, Query] = None,
    freigabe_status_methodisch_freigegeben: Annotated[bool | None, Query] = None,
    freigabe_status_fachlich_freigegeben_silber: Annotated[bool | None, Query] = None,
    freigabe_status_fachlich_freigegeben_gold: Annotated[bool | None, Query] = None,
    freigabe_status_inaktiv: Annotated[bool | None, Query] = None,
    freigabe_status_vorgesehen_zum_loeschen: Annotated[bool | None, Query] = None,
    freigabe_status_katalog: Annotated[str | None, Query] = None,
    freigabe_status_bibliothek: Annotated[str | None, Query] = None,
    xdf_version: Annotated[str | None, Query] = None,
    leistung_redaktion_id: Annotated[str | None, Query] = None,
    leistungstyp: Annotated[str | None, Query] = None,
    leistungstypisierung: Annotated[str | None, Query] = None,
    leistung_einheitlicher_ansprechpartner: Annotated[str | None, Query] = None,
    leistung_suche_in: Annotated[str | None, Query] = None,
    schema_suche_in: Annotated[str | None, Query] = None,
    steckbrief_suche_in: Annotated[str | None, Query] = None,
    gruppe_suche_in: Annotated[str | None, Query] = None,
    feld_suche_in: Annotated[str | None, Query] = None,
    nummernkreis: Annotated[str | None, Query] = None,
    feldart: Annotated[str | None, Query] = None,
    datentyp: Annotated[str | None, Query] = None,
    order_by: Annotated[str | None, Query] = None,
    operatives_ziel: Annotated[str | None, Query] = None,
    verfahrensart: Annotated[str | None, Query] = None,
    handlungsform: Annotated[str | None, Query] = None,
    detaillierungsstufe: Annotated[str | None, Query] = None,
    anwendungsgebiet: Annotated[str | None, Query] = None,
    dokumentart: Annotated[str | None, Query] = None,
) -> UiFilters:
    if term == "":
        term = None

    real_resource = _parse_nullable_enum(Resource, resource) or Resource.SERVICE

    real_leistungstypisierung = _parse_nullable_enum(
        LeistungsTypisierung, leistungstypisierung
    )
    real_leistungstyp = _parse_nullable_enum(LeistungsTyp, leistungstyp)
    real_xdf_version = _parse_nullable_enum(XdfVersion, xdf_version)

    real_freigabe_status = _parse_multiple_freigabe_status(
        freigabe_status_in_planung,
        freigabe_status_in_bearbeitung,
        freigabe_status_entwurf,
        freigabe_status_methodisch_freigegeben,
        freigabe_status_fachlich_freigegeben_silber,
        freigabe_status_fachlich_freigegeben_gold,
        freigabe_status_inaktiv,
        freigabe_status_vorgesehen_zum_loeschen,
    )
    real_freigabe_status_katalog = _parse_nullable_int_enum(
        FreigabeStatus, freigabe_status_katalog
    )
    real_freigabe_status_bibliothek = _parse_nullable_int_enum(
        FreigabeStatus, freigabe_status_bibliothek
    )
    real_nummernkreis = _parse_nullable_enum(NummernkreisOption, nummernkreis)
    real_feldart = _parse_nullable_enum(Feldart, feldart)
    real_datentyp = _parse_nullable_enum(Datentyp, datentyp)
    real_leistung_redaktion_id = _parse_nullable_enum(
        LeistungRedaktionId, leistung_redaktion_id
    )
    real_schema_suche_in = _parse_nullable_enum(SchemaSucheIn, schema_suche_in)
    real_steckbrief_suche_in = _parse_nullable_enum(
        SteckbriefSucheIn, steckbrief_suche_in
    )
    real_gruppe_suche_in = _parse_nullable_enum(GruppeSucheIn, gruppe_suche_in)
    real_feld_suche_in = _parse_nullable_enum(FeldSucheIn, feld_suche_in)
    real_leistung_suche_in = _parse_nullable_enum(LeistungSucheIn, leistung_suche_in)
    real_leistung_einheitlicher_ansprechpartner = _parse_nullable_enum(
        EinheitlicherAnsprechpartner, leistung_einheitlicher_ansprechpartner
    )

    real_operatives_ziel = _parse_nullable_enum(OperativesZiel, operatives_ziel)
    real_verfahrensart = _parse_nullable_enum(Verfahrensart, verfahrensart)
    real_handlungsform = _parse_nullable_enum(Handlungsform, handlungsform)

    real_detaillierungsstufe = _parse_nullable_enum(
        Detaillierungsstufe, detaillierungsstufe
    )
    real_anwendungsgebiet = _parse_nullable_enum(Anwendungsgebiet, anwendungsgebiet)
    real_dokumentart = _parse_nullable_enum(Dokumentart, dokumentart)

    return UiFilters(
        resource=real_resource,
        term=term,
        freigabe_status=real_freigabe_status,
        freigabe_status_katalog=real_freigabe_status_katalog,
        freigabe_status_bibliothek=real_freigabe_status_bibliothek,
        xdf_version=real_xdf_version,
        leistung_redaktion_id=real_leistung_redaktion_id,
        leistungstyp=real_leistungstyp,
        leistungstypisierung=real_leistungstypisierung,
        leistung_einheitlicher_ansprechpartner=real_leistung_einheitlicher_ansprechpartner,
        leistung_suche_in=real_leistung_suche_in,
        schema_suche_in=real_schema_suche_in,
        steckbrief_suche_in=real_steckbrief_suche_in,
        gruppe_suche_in=real_gruppe_suche_in,
        feld_suche_in=real_feld_suche_in,
        nummernkreis=real_nummernkreis,
        feldart=real_feldart,
        datentyp=real_datentyp,
        order_by=order_by,
        operatives_ziel=real_operatives_ziel,
        verfahrensart=real_verfahrensart,
        handlungsform=real_handlungsform,
        detaillierungsstufe=real_detaillierungsstufe,
        anwendungsgebiet=real_anwendungsgebiet,
        dokumentart=real_dokumentart,
    )


@dataclass(slots=True)
class PageLink:
    label: str
    href: str

    @staticmethod
    def for_page(base_url: str, page_size: int, page: int) -> PageLink:
        return PageLink(
            label=str(page), href=_add_pagination_params(base_url, page_size, page)
        )


def _add_pagination_params(base_url: str, page_size: int, page: int) -> str:
    return f"{base_url}&page={page}&page_size={page_size}"


@dataclass(slots=True)
class PaginationLinks:
    first_page: PageLink | None
    previous_pages: list[PageLink]
    current_page: PageLink
    next_pages: list[PageLink]
    last_page: PageLink | None


@dataclass(slots=True)
class ResourceLink:
    resource: Resource
    is_active: bool
    count: int
    href: str

    def get_label(self) -> str:
        return self.resource.to_label()


@dataclass(slots=True)
class SingleSelectFilterOption:
    value: str
    label: str
    is_active: bool


@dataclass(slots=True)
class MultiSelectFilterOption:
    query_param_name: str
    value: str
    label: str
    is_active: bool


@dataclass(slots=True)
class SingleSelectFilter:
    parameter_name: str
    label: str
    glossary_link: str | None
    options: list[SingleSelectFilterOption]
    is_active: bool

    @staticmethod
    def create(
        parameter_name: str,
        label: str,
        glossary_link: str | None,
        current_active: E | None,
        variants: list[E],
        to_label: Callable[[E], str],
        default_label: str | None = "Kein Filter",
    ) -> SingleSelectFilter:
        if default_label is not None:
            options = [
                SingleSelectFilterOption(
                    value="undefined",
                    label=default_label,
                    is_active=current_active is None,
                ),
            ]
        else:
            options = []

        return SingleSelectFilter(
            parameter_name,
            label,
            glossary_link,
            options=options
            + [
                SingleSelectFilterOption(
                    value=variant.value,
                    label=to_label(variant),
                    is_active=variant == current_active,
                )
                for variant in variants
            ],
            is_active=current_active is not None,
        )


@dataclass(slots=True)
class MultiSelectFilter:
    parameter_name: str
    label: str
    glossary_link: str | None
    options: list[MultiSelectFilterOption]
    is_active: bool

    @staticmethod
    def create(
        parameter_name: str,
        label: str,
        glossary_link: str | None,
        current_active: list[E],
        variants: list[E],
        to_label: Callable[[E], str],
    ) -> MultiSelectFilter:
        return MultiSelectFilter(
            parameter_name,
            label,
            glossary_link,
            options=[
                MultiSelectFilterOption(
                    query_param_name=f"{parameter_name}_{variant.name.lower()}",
                    value=variant.value,
                    label=to_label(variant),
                    is_active=variant in current_active,
                )
                for variant in variants
            ],
            is_active=len(current_active) > 0,
        )


FREIGABE_STATUS_VARIANTS = [
    FreigabeStatus.IN_PLANUNG,
    FreigabeStatus.IN_BEARBEITUNG,
    FreigabeStatus.ENTWURF,
    FreigabeStatus.METHODISCH_FREIGEGEBEN,
    FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
    FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER,
    FreigabeStatus.INAKTIV,
    FreigabeStatus.VORGESEHEN_ZUM_LOESCHEN,
]

EINHEITLICHER_ANSPRECHPARTNER_VARIANTS = [
    EinheitlicherAnsprechpartner.TRUE,
    EinheitlicherAnsprechpartner.FALSE,
]


def get_einheitlicher_ansprechpartner_options(
    filters: UiFilters,
) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="leistung_einheitlicher_ansprechpartner",
        label="Einheitlicher Ansprechpartner",
        glossary_link="https://docs.fitko.de/fim/docs/glossar/#einheitlicher-ansprechpartner",
        current_active=filters.leistung_einheitlicher_ansprechpartner,
        variants=EINHEITLICHER_ANSPRECHPARTNER_VARIANTS,
        to_label=lambda x: str(x.value),
    )


def get_freigabe_status_options(filters: UiFilters) -> MultiSelectFilter:
    return MultiSelectFilter.create(
        parameter_name="freigabe_status",
        label="Freigabestatus",
        glossary_link="https://docs.fitko.de/fim/docs/glossar/#status",
        current_active=filters.freigabe_status
        if filters.freigabe_status is not None
        else [],
        variants=FREIGABE_STATUS_VARIANTS,
        to_label=FreigabeStatus.to_label,
    )


def get_freigabe_status_katalog_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="freigabe_status_katalog",
        label="Freigabestatus Katalog",
        glossary_link="https://docs.fitko.de/fim/docs/glossar/#status",
        current_active=filters.freigabe_status_katalog,
        variants=FREIGABE_STATUS_VARIANTS,
        to_label=FreigabeStatus.to_label,
    )


def get_freigabe_status_bibliothek_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="freigabe_status_bibliothek",
        label="Freigabestatus Bibliothek",
        glossary_link="https://docs.fitko.de/fim/docs/glossar/#status",
        current_active=filters.freigabe_status_bibliothek,
        variants=FREIGABE_STATUS_VARIANTS,
        to_label=FreigabeStatus.to_label,
    )


def get_xdf_version_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="xdf_version",
        label="XDF Version",
        glossary_link="https://docs.fitko.de/fim/docs/glossar/#xdfversion",
        current_active=filters.xdf_version,
        variants=[
            XdfVersion.XDF2,
            XdfVersion.XDF3,
        ],
        to_label=lambda v: v.value,
    )


def get_leistung_redaktion_id_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="leistung_redaktion_id",
        label="Quellredaktion",
        glossary_link="https://docs.fitko.de/fim/docs/glossar/#quellredaktion",
        current_active=filters.leistung_redaktion_id,
        variants=[
            *LANDESREDAKTIONEN,
            LeistungRedaktionId.BUND,
        ],
        to_label=lambda v: v.to_label(),
    )


def get_leistungstyp_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="leistungstyp",
        label="Leistungstyp",
        glossary_link="https://docs.fitko.de/fim/docs/glossar/#leistungstyp",
        current_active=filters.leistungstyp,
        variants=[
            LeistungsTyp.LEISTUNGS_OBJEKT,
            LeistungsTyp.LEISTUNGS_OBJEKT_MIT_VERRICHTUNG,
            LeistungsTyp.LEISTUNGS_OBJEKT_MIT_VERRICHTUNG_UND_DETAIL,
        ],
        to_label=lambda v: v.to_label(),
    )


def get_leistungstypisierung_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="leistungstypisierung",
        label="Leistungstypisierung",
        glossary_link="https://docs.fitko.de/fim/docs/glossar/#typisierung",
        current_active=filters.leistungstypisierung,
        variants=[
            LeistungsTypisierung.TYP_1,
            LeistungsTypisierung.TYP_2,
            LeistungsTypisierung.TYP_2A,
            LeistungsTypisierung.TYP_2B,
            LeistungsTypisierung.TYP_3,
            LeistungsTypisierung.TYP_3A,
            LeistungsTypisierung.TYP_3B,
            LeistungsTypisierung.TYP_2_3,
            LeistungsTypisierung.TYP_2_3A,
            LeistungsTypisierung.TYP_2_3B,
            LeistungsTypisierung.TYP_4,
            LeistungsTypisierung.TYP_4A,
            LeistungsTypisierung.TYP_4B,
            LeistungsTypisierung.TYP_5,
            LeistungsTypisierung.TYP_6,
            LeistungsTypisierung.TYP_7,
            LeistungsTypisierung.TYP_8,
            LeistungsTypisierung.TYP_9,
            LeistungsTypisierung.TYP_10,
            LeistungsTypisierung.TYP_11,
            LeistungsTypisierung.TYP_12,
        ],
        to_label=lambda v: v.to_label(),
    )


def get_nummernkreis_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="nummernkreis",
        label="Redaktion (Nummernkreis)",
        glossary_link="https://docs.fitko.de/fim/docs/glossar/#nummernkreis",
        current_active=filters.nummernkreis,
        variants=[
            NummernkreisOption.BUNDESREDAKTION,
            NummernkreisOption.LANDESREDAKTION_SCHLESWIG_HOLSTEIN,
            NummernkreisOption.LANDESREDAKTION_HAMBURG,
            NummernkreisOption.LANDESREDAKTION_LOWER_SAXONY,
            NummernkreisOption.LANDESREDAKTION_BREMEN,
            NummernkreisOption.LANDESREDAKTION_NORTH_RHINE_WESTPHALIA,
            NummernkreisOption.LANDESREDAKTION_HESSE,
            NummernkreisOption.LANDESREDAKTION_RHINELAND_PALATINATE,
            NummernkreisOption.LANDESREDAKTION_BADEN_WUERTTEMBERG,
            NummernkreisOption.LANDESREDAKTION_BAVARIA,
            NummernkreisOption.LANDESREDAKTION_SAARLAND,
            NummernkreisOption.LANDESREDAKTION_BERLIN,
            NummernkreisOption.LANDESREDAKTION_BRANDENBURG,
            NummernkreisOption.LANDESREDAKTION_MECKLENBURG_WESTERN_POMERANIA,
            NummernkreisOption.LANDESREDAKTION_SAXONY,
            NummernkreisOption.LANDESREDAKTION_SAXONY_ANHALT,
            NummernkreisOption.LANDESREDAKTION_THURINGIA,
            NummernkreisOption.VIRTUELLE_LANDESREDAKTION,
            NummernkreisOption.FIM_BAUSTEINE,
            NummernkreisOption.ATLANTIS,
            NummernkreisOption.TESTREPOSITORY,
            NummernkreisOption.KATE,
        ],
        to_label=lambda v: v.to_label(),
    )


def get_feldart_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="feldart",
        label="Feldart",
        glossary_link="https://docs.fitko.de/fim/docs/glossar/#feldart",
        current_active=filters.feldart,
        variants=[
            Feldart.EINGABE,
            Feldart.AUSWAHL,
            Feldart.STATISCH,
            Feldart.VERSTECKT,
            Feldart.GESPERRT,
        ],
        to_label=feldart_to_label,
    )


def get_datentyp_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="datentyp",
        label="Datentyp",
        glossary_link="https://docs.fitko.de/fim/docs/glossar/#datentyp",
        current_active=filters.datentyp,
        variants=[
            Datentyp.TEXT,
            Datentyp.STRING_LATIN,
            Datentyp.DATUM,
            Datentyp.ZEIT,
            Datentyp.ZEITPUNKT,
            Datentyp.WAHRHEITSWERT,
            Datentyp.NUMMER,
            Datentyp.GANZZAHL,
            Datentyp.GELDBETRAG,
            Datentyp.ANLAGE,
            Datentyp.OBJEKT,
        ],
        to_label=datentyp_to_label,
    )


def _get_leistung_suche_in_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="leistung_suche_in",
        label="Suche nur in",
        glossary_link=None,
        current_active=filters.leistung_suche_in,
        variants=[
            LeistungSucheIn.LEISTUNGSSCHLUESSEL,
            LeistungSucheIn.RECHTSGRUNDLAGEN,
            LeistungSucheIn.LEISTUNGSBEZEICHNUNG,
            LeistungSucheIn.LEISTUNGSBEZEICHNUNG_II,
        ],
        to_label=lambda v: v.to_label(),
        default_label="Volltextsuche",
    )


def _get_leistungssteckbrief_sort_options(
    current_active: LeistungssteckbriefSearchOrder,
) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="order_by",
        label="Sortieren nach",
        glossary_link=None,
        current_active=current_active,
        variants=[
            LeistungssteckbriefSearchOrder.RELEVANZ,
            LeistungssteckbriefSearchOrder.LEISTUNGSSCHLUESSEL_ASC,
            LeistungssteckbriefSearchOrder.TITEL_ASC,
            LeistungssteckbriefSearchOrder.AKTUALISIERT_DATUM_ZEIT_DESC,
            LeistungssteckbriefSearchOrder.AKTUALISIERT_DATUM_ZEIT_ASC,
            LeistungssteckbriefSearchOrder.ERSTELLT_DATUM_ZEIT_DESC,
            LeistungssteckbriefSearchOrder.ERSTELLT_DATUM_ZEIT_ASC,
        ],
        to_label=lambda v: v.to_label(),
        default_label=None,
    )


def _get_leistungsbeschreibung_sort_options(
    current_active: LeistungsbeschreibungSearchOrder,
) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="order_by",
        label="Sortieren nach",
        glossary_link=None,
        current_active=current_active,
        variants=[
            LeistungsbeschreibungSearchOrder.RELEVANZ,
            LeistungsbeschreibungSearchOrder.LEISTUNGSSCHLUESSEL_ASC,
            LeistungsbeschreibungSearchOrder.TITEL_ASC,
            LeistungsbeschreibungSearchOrder.AKTUALISIERT_DATUM_ZEIT_DESC,
            LeistungsbeschreibungSearchOrder.AKTUALISIERT_DATUM_ZEIT_ASC,
            LeistungsbeschreibungSearchOrder.ERSTELLT_DATUM_ZEIT_DESC,
            LeistungsbeschreibungSearchOrder.ERSTELLT_DATUM_ZEIT_ASC,
        ],
        to_label=lambda v: v.to_label(),
        default_label=None,
    )


def _get_schema_sort_options(current_active: SchemaSearchOrder):
    return SingleSelectFilter.create(
        parameter_name="order_by",
        label="Sortieren nach",
        glossary_link=None,
        current_active=current_active,
        variants=[
            SchemaSearchOrder.NAME_ASC,
            SchemaSearchOrder.NAME_DESC,
            SchemaSearchOrder.AKTUALISIERT_DATUM_ZEIT_DESC,
            SchemaSearchOrder.AKTUALISIERT_DATUM_ZEIT_ASC,
            SchemaSearchOrder.ID_ASC,
            SchemaSearchOrder.ID_DESC,
        ],
        to_label=lambda v: v.to_label(),
        default_label=None,
    )


def get_schema_suche_in_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="schema_suche_in",
        label="Suche nur in",
        glossary_link=None,
        current_active=filters.schema_suche_in,
        variants=[
            SchemaSucheIn.RECHTSGRUNDLAGEN,
            SchemaSucheIn.STATUS_GESETZT_DURCH,
            SchemaSucheIn.VERSIONSHINWEIS,
            SchemaSucheIn.STICHWORT,
        ],
        to_label=lambda v: v.to_label(),
        default_label="Volltextsuche",
    )


def get_steckbrief_suche_in_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="steckbrief_suche_in",
        label="Suche nur in",
        glossary_link=None,
        current_active=filters.steckbrief_suche_in,
        variants=[
            SteckbriefSucheIn.RECHTSGRUNDLAGEN,
            SteckbriefSucheIn.STATUS_GESETZT_DURCH,
            SteckbriefSucheIn.VERSIONSHINWEIS,
        ],
        to_label=lambda v: v.to_label(),
        default_label="Volltextsuche",
    )


def get_gruppe_suche_in_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="gruppe_suche_in",
        label="Suche nur in",
        glossary_link=None,
        current_active=filters.gruppe_suche_in,
        variants=[
            GruppeSucheIn.RECHTSGRUNDLAGEN,
            GruppeSucheIn.STATUS_GESETZT_DURCH,
            GruppeSucheIn.VERSIONSHINWEIS,
        ],
        to_label=lambda v: v.to_label(),
        default_label="Volltextsuche",
    )


def get_feld_suche_in_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="feld_suche_in",
        label="Suche nur in",
        glossary_link=None,
        current_active=filters.feld_suche_in,
        variants=[
            FeldSucheIn.RECHTSGRUNDLAGEN,
            FeldSucheIn.STATUS_GESETZT_DURCH,
            FeldSucheIn.VERSIONSHINWEIS,
        ],
        to_label=lambda v: v.to_label(),
        default_label="Volltextsuche",
    )


def get_operatives_ziel_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="operatives_ziel",
        label="Operatives Ziel",
        glossary_link="https://docs.fitko.de/fim/docs/glossar/#operatives-ziel",
        current_active=filters.operatives_ziel,
        variants=[
            OperativesZiel.GENERELL_ABSTRAKTE_REGELUNG,
            OperativesZiel.KONKRETE_REGELUNG_VERHALTEN_PERSON,
            OperativesZiel.KONKRETE_REGELUNG_RECHTSVERHAELTNIS_FESTSTELLUNG,
            OperativesZiel.KONKRETE_REGELUNG_RECHTSVERHAELTNIS_HANDLUNG,
            OperativesZiel.KONKRETE_REGELUNG_RECHTSVERHAELTNIS_VERWALTUNG,
            OperativesZiel.NORMKONKRETISIERENDE_REGELUNG,
            OperativesZiel.NORMINTERPRETIERENDE_REGELUNG,
            OperativesZiel.NORMVERTRENDE_REGELUNG,
            OperativesZiel.ORGANINTERNE_INTERORGANE_REGELUNG,
            OperativesZiel.OEFFENTLICH_RECHTLICHE_GELDFORDERUNG,
            OperativesZiel.ERZWINGUNG_DULDUNG_UNTERLASSUNG_HANDLUNG,
            OperativesZiel.GELDLEISTUNG,
            OperativesZiel.SACH_DIENSTLEISTUNG,
            OperativesZiel.KONKRETE_SACH_RECHSLAGE,
            OperativesZiel.GEMEINWESEN,
            OperativesZiel.ARBEIT_DER_VERWALTUNG,
            OperativesZiel.RECHTMAESSIGKEIT_ARBEIT_DER_VERWALTUNG,
            OperativesZiel.ZWECKMAESSIGKEIT_ARBEIT_DER_VERWALTUNG,
            OperativesZiel.SONSTIGE,
        ],
        to_label=OperativesZiel.to_label,
    )


def get_handlungsform_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="handlungsform",
        label="Handlungsform",
        glossary_link="https://docs.fitko.de/fim/docs/glossar/#handlungsform",
        current_active=filters.handlungsform,
        variants=[
            Handlungsform.NORMERLASS_EU_BESCHLUSS,
            Handlungsform.NORMERLASS_EU_RICHTLINIE,
            Handlungsform.NORMERLASS_EU_VERORDNUNG,
            Handlungsform.NORMERLASS_GESETZ,
            Handlungsform.NORMERLASS_GESETZ_EU_RICHTLINIE,
            Handlungsform.NORMERLASS_RECHTSVERORDNUNG,
            Handlungsform.NORMERLASS_SATZUNG,
            Handlungsform.NORMERLASS_VERWALTUNGSVORSCHRIFT,
            Handlungsform.NORMERLASS_GESCHAEFTSORDNUNG,
            Handlungsform.NORMERLASS_BESCHLUSS,
            Handlungsform.ENTWICKLUNG_NORM_ODER_STANDARD,
            Handlungsform.VERWALTUNGSAKT,
            Handlungsform.VERWALTUNGSAKT_ALLGEMEINVERFUEGUNG,
            Handlungsform.VERWALTUNGSAKT_RECHTSAUFSICHTLICHE_WEISUNG,
            Handlungsform.VERWALTUNGSRECHTLICHE_WILLENSERKLAERUNG_FACHAUFSICHTLICHE_WEISUNG,
            Handlungsform.VERWALTUNGSRECHTLICHE_WILLENSERKLAERUNG_INNERDIENSTLICHE_WEISUNG,
            Handlungsform.VERWALTUNGSRECHTLICHE_WILLENSERKLAERUNG,
            Handlungsform.OEFFENTLICH_RECHTLICHER_VERTRAG_KOORDINATIONSRECHTLICHER_VERTRAG,
            Handlungsform.OEFFENTLICH_RECHTLICHER_VERTRAG_SUBORDINATIONSRECHTLICHER_VERTRAG,
            Handlungsform.REALAKT_ABGABE_WISSENSERKLAERUNG,
            Handlungsform.REALAKT_ABWICKLUNG_FEHLGESCHLAGENER_LEISTUNGSBEZIEHUNGEN,
            Handlungsform.REALAKT_PLAN,
            Handlungsform.REALAKT_VOLLSTRECKUNG_VERWALTUNGSAKT,
            Handlungsform.REALAKT_VOLLZUG_VERWALTUNGSAKT,
            Handlungsform.REALAKT_SCHLICHT_HOHEITLICHES_VERWALTUNGSHANDELN,
            Handlungsform.PRIVATRECHTLICHES_HILFSGESCHAEFT,
            Handlungsform.PRIVATRECHTLICHER_VERTRAG,
            Handlungsform.VERWALTUNGSPRIVATRECHTLICHES_HANDELN,
            Handlungsform.ERWERBSWIRTSCHAFTLICHES_HANDELN,
            Handlungsform.BESCHLUSS,
            Handlungsform.VERFUEGUNG,
            Handlungsform.URTEIL,
            Handlungsform.JUSTIZVERWALTUNGSAKT,
            Handlungsform.SONSTIGE_WEITERE_HANDLUNGSFORM,
        ],
        to_label=Handlungsform.to_label,
    )


def get_verfahrensart_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="verfahrensart",
        label="Verfahrensart",
        glossary_link="https://docs.fitko.de/fim/docs/glossar/#verfahrensart",
        current_active=filters.verfahrensart,
        variants=[
            Verfahrensart.GESETZGEBUNGSVERFAHREN_FORMELLES_RECHT,
            Verfahrensart.GESETZGEBUNGSVERFAHREN_FORMELLES_UND_MATERIELLES_RECHT,
            Verfahrensart.HAUSHALTSPLANUNG_HAUSHALTSPLANFESTSTELLUNG_FORMELLES_GESETZ,
            Verfahrensart.NORMSETZUNGSVERFAHREN_OHNE_FORMELLICHE_GESETZE,
            Verfahrensart.LANDESENTWICKLUNGSPLANFESTSETZUNG,
            Verfahrensart.REGIONALENTWICKLUNGSPLANFESTSETZUNG,
            Verfahrensart.BAULEITPLANVERFAHREN,
            Verfahrensart.NICHT_FORMLICHES_VERWALTUNGSVERFAHREN_VWVFG_VWGO,
            Verfahrensart.FORMAL_VERWALTUNGSVERFAHREN_VWVFG,
            Verfahrensart.WIDERSPRUCHSVERFAHREN_VORVERFAHREN_VWGO,
            Verfahrensart.WIDERSPRUCHSVERFAHREN_ABHILFE_VERFAHREN_VWGO,
            Verfahrensart.VERWALTUNGSVERFAHREN_EINHEITLICHE_STELLE_VWVFG,
            Verfahrensart.VERWALTUNGSVERFAHREN_GENEHMIGUNGSFIKTION_VWVFG,
            Verfahrensart.PLANFESTSTELLUNGSVERFAHREN_VWVFG,
            Verfahrensart.PLANFESTSTELLUNGSVERFAHREN_BUENDELUNG_VWVFG,
            Verfahrensart.VERWALTUNGSGEBUEHREN_AUSLAGEN_FESTSETZUNG_VERFAHREN,
            Verfahrensart.BENUTZUNGSGEBUEHREN_AUSLAGEN_FESTSETZUNG_VERFAHREN,
            Verfahrensart.PRIVATRECHTLICHES_ENTGELT_FESTSETZUNG_VERFAHREN,
            Verfahrensart.BEITRAG_FESTSETZUNG_VERFAHREN,
            Verfahrensart.VOLLSTRECKUNGSVERFAHREN_VWVFG,
            Verfahrensart.BUSSGELDVERFAHREN_VWVFG,
            Verfahrensart.BESTEUERUNGSVERFAHREN_AO,
            Verfahrensart.GESONDERTE_FESTSTELLUNG_VERFAHREN_AO,
            Verfahrensart.STEUERMESSBETRAEGE_FESTSETZUNG_VERFAHREN_AO,
            Verfahrensart.EINSRUCHSVERFAHREN_VORVERFAHREN_AO,
            Verfahrensart.VOLLSTRECKUNGSVERFAHREN_AO,
            Verfahrensart.STEUERSTRAFVERFAHREN_AO,
            Verfahrensart.BUSSGELDVERFAHREN_AO,
            Verfahrensart.SOZIALVERWALTUNGSVERFAHREN_SGB_X,
            Verfahrensart.WIDERSPRUCHSVERFAHREN_VORVERFAHREN_SGB_X,
            Verfahrensart.VOLLSTRECKUNGSVERFAHREN_SGB_X,
            Verfahrensart.BUSSGELDVERFAHREN_SGB_X,
            Verfahrensart.NORMENKONTROLLVERFAHREN_VWGO,
            Verfahrensart.ANFECHTUNGSKLAGEVERFAHREN_VWGO,
            Verfahrensart.VERPFLICHTUNGSKLAGEVERFAHREN_VWGO,
            Verfahrensart.FESTSTELLUNGSKLAGEVERFAHREN_VWGO,
            Verfahrensart.LEISTUNGSKLAGEVERFAHREN_VWGO,
            Verfahrensart.ANFECHTUNGSKLAGEVERFAHREN_FGO,
            Verfahrensart.VERPFLICHTUNGSKLAGEVERFAHREN_FGO,
            Verfahrensart.FESTSTELLUNGSKLAGEVERFAHREN_FGO,
            Verfahrensart.LEISTUNGSKLAGEVERFAHREN_FGO,
            Verfahrensart.ANFECHTUNGSKLAGEVERFAHREN_SGG,
            Verfahrensart.VERPFLICHTUNGSKLAGEVERFAHREN_SGG,
            Verfahrensart.FESTSTELLUNGSKLAGEVERFAHREN_SGG,
            Verfahrensart.LEISTUNGSKLAGEVERFAHREN_SGG,
            Verfahrensart.KOSTENFESTSETZUNGSVERFAHREN_ZPO,
            Verfahrensart.JUSTIZVERWALTUNGSVERFAHREN_EGGVG,
            Verfahrensart.BEAMTENRECHTLICHES_DISZIPLINARVERFAHREN,
            Verfahrensart.ARBEITSRECHTLICHES_DISZIPLINARVERFAHREN,
            Verfahrensart.VERGABEVERFAHREN,
            Verfahrensart.KEIN_OE_RECHTLICHES_OE_RECHT_PRIVATRECHT_VERFAHREN,
            Verfahrensart.KEINE_VORGABE,
        ],
        to_label=Verfahrensart.to_label,
    )


def get_detaillierungsstufe_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="detaillierungsstufe",
        label="Detaillierungsstufe",
        glossary_link=None,
        current_active=filters.detaillierungsstufe,
        variants=[
            Detaillierungsstufe.STAMMINFORMATIONEN,
            Detaillierungsstufe.REFERENZINFORMATIONEN,
            Detaillierungsstufe.OZG_REFERENZINFORMATIONEN,
            Detaillierungsstufe.LOKALINFORMATIONEN,
        ],
        to_label=Detaillierungsstufe.to_label,
    )


def get_anwendungsgebiet_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="anwendungsgebiet",
        label="Anwendungsgebiet (Verwaltungspolitische Kodierung)",
        glossary_link=None,
        current_active=filters.anwendungsgebiet,
        variants=[
            Anwendungsgebiet.SCHLESWIG_HOLSTEIN,
            Anwendungsgebiet.HAMBURG,
            Anwendungsgebiet.NIEDERSACHSEN,
            Anwendungsgebiet.BREMEN,
            Anwendungsgebiet.NORDRHEIN_WESTFALEN,
            Anwendungsgebiet.HESSEN,
            Anwendungsgebiet.RHEINLAND_PFALZ,
            Anwendungsgebiet.BADEN_WUERTTEMBERG,
            Anwendungsgebiet.BAYERN,
            Anwendungsgebiet.SAARLAND,
            Anwendungsgebiet.BERLIN,
            Anwendungsgebiet.BRANDENBURG,
            Anwendungsgebiet.MECKLENBURG_VORPOMMERN,
            Anwendungsgebiet.SACHSEN,
            Anwendungsgebiet.SACHSEN_ANHALT,
            Anwendungsgebiet.THUERINGEN,
            Anwendungsgebiet.BUND,
        ],
        to_label=Anwendungsgebiet.to_label,
    )


def get_dokumentart_options(filters: UiFilters) -> SingleSelectFilter:
    return SingleSelectFilter.create(
        parameter_name="dokumentart",
        label="Dokumentart",
        glossary_link=None,
        current_active=filters.dokumentart,
        variants=[
            Dokumentart.ANTRAG,
            Dokumentart.ANZEIGE,
            Dokumentart.BERICHT,
            Dokumentart.BESCHEID,
            Dokumentart.ERKLAERUNG,
            Dokumentart.KARTENMATERIAL,
            Dokumentart.MITTEILUNG,
            Dokumentart.MULTIMEDIA,
            Dokumentart.REGISTERANFRAGE,
            Dokumentart.REGISTERANTWORT,
            Dokumentart.URKUNDE,
            Dokumentart.VERTRAG,
            Dokumentart.VOLLMACHT,
            Dokumentart.WILLENSERKLAERUNG,
            Dokumentart.UNBESTIMMT,
        ],
        to_label=Dokumentart.to_label,
    )


@dataclass(slots=True)
class LinkBuilder:
    filters: UiFilters
    page_size: int

    def get_resource_link(self, resource: Resource, total_count: int) -> ResourceLink:
        filters = copy.copy(self.filters)
        filters.resource = resource

        base_url = filters.to_search_url()

        return ResourceLink(
            resource=resource,
            is_active=self.filters.resource == resource,
            count=total_count,
            href=_add_pagination_params(base_url, page_size=self.page_size, page=1),
        )

    def get_pagination_options(
        self, available_pages: AvailablePages
    ) -> PaginationLinks:
        page_base_url = self.filters.to_search_url()

        return PaginationLinks(
            first_page=(
                PageLink.for_page(
                    base_url=page_base_url,
                    page_size=self.page_size,
                    page=1,
                )
                if available_pages.current_page != 1
                else None
            ),
            previous_pages=[
                PageLink.for_page(
                    base_url=page_base_url,
                    page_size=self.page_size,
                    page=page,
                )
                for page in available_pages.previous_pages
            ],
            current_page=PageLink.for_page(
                base_url=page_base_url,
                page_size=self.page_size,
                page=available_pages.current_page,
            ),
            next_pages=[
                PageLink.for_page(
                    base_url=page_base_url,
                    page_size=self.page_size,
                    page=page,
                )
                for page in available_pages.next_pages
            ],
            last_page=(
                PageLink.for_page(
                    base_url=page_base_url,
                    page_size=self.page_size,
                    page=available_pages.last_page,
                )
                if available_pages.current_page != available_pages.last_page
                else None
            ),
        )


class NummernkreisOption(Enum):
    """Nummernrkeis options for the select menu

    0 Bundesredaktion
    1-16 Landesredaktion Bundesland (refer to https://de.wikipedia.org/wiki/Amtlicher_Gemeindeschlüssel for table "Aktuelle Länderschlüssel")
    17 Virtuelle Landesredaktion
    60 FIM-Bausteine
    82 Testrepository
    99 Dokument-lokale IDs
    """

    BUNDESREDAKTION = "00000"
    LANDESREDAKTION_SCHLESWIG_HOLSTEIN = "01000"
    LANDESREDAKTION_HAMBURG = "02000"
    LANDESREDAKTION_LOWER_SAXONY = "03000"
    LANDESREDAKTION_BREMEN = "04000"
    LANDESREDAKTION_NORTH_RHINE_WESTPHALIA = "05000"
    LANDESREDAKTION_HESSE = "06000"
    LANDESREDAKTION_RHINELAND_PALATINATE = "07000"
    LANDESREDAKTION_BADEN_WUERTTEMBERG = "08000"
    LANDESREDAKTION_BAVARIA = "09000"
    LANDESREDAKTION_SAARLAND = "10000"
    LANDESREDAKTION_BERLIN = "11000"
    LANDESREDAKTION_BRANDENBURG = "12000"
    LANDESREDAKTION_MECKLENBURG_WESTERN_POMERANIA = "13000"
    LANDESREDAKTION_SAXONY = "14000"
    LANDESREDAKTION_SAXONY_ANHALT = "15000"
    LANDESREDAKTION_THURINGIA = "16000"
    VIRTUELLE_LANDESREDAKTION = "17000"
    FIM_BAUSTEINE = "60000"
    ATLANTIS = "81000"
    TESTREPOSITORY = "82000"
    KATE = "99000"

    def to_label(self) -> str:
        match self:
            case NummernkreisOption.BUNDESREDAKTION:
                return "00 - Bundesredaktion"
            case NummernkreisOption.LANDESREDAKTION_SCHLESWIG_HOLSTEIN:
                return "01 - Landesredaktion Schleswig-Holstein"
            case NummernkreisOption.LANDESREDAKTION_HAMBURG:
                return "02 - Landesredaktion Hamburg"
            case NummernkreisOption.LANDESREDAKTION_LOWER_SAXONY:
                return "03 - Landesredaktion Niedersachsen"
            case NummernkreisOption.LANDESREDAKTION_BREMEN:
                return "04 - Landesredaktion Bremen"
            case NummernkreisOption.LANDESREDAKTION_NORTH_RHINE_WESTPHALIA:
                return "05 - Landesredaktion Nordrhein-Westfalen"
            case NummernkreisOption.LANDESREDAKTION_HESSE:
                return "06 - Landesredaktion Hessen"
            case NummernkreisOption.LANDESREDAKTION_RHINELAND_PALATINATE:
                return "07 - Landesredaktion Rheinland-Pfalz"
            case NummernkreisOption.LANDESREDAKTION_BADEN_WUERTTEMBERG:
                return "08 - Landesredaktion Baden-Württemberg"
            case NummernkreisOption.LANDESREDAKTION_BAVARIA:
                return "09 - Landesredaktion Bayern"
            case NummernkreisOption.LANDESREDAKTION_SAARLAND:
                return "10 - Landesredaktion Saarland"
            case NummernkreisOption.LANDESREDAKTION_BERLIN:
                return "11 - Landesredaktion Berlin"
            case NummernkreisOption.LANDESREDAKTION_BRANDENBURG:
                return "12 - Landesredaktion Brandenburg"
            case NummernkreisOption.LANDESREDAKTION_MECKLENBURG_WESTERN_POMERANIA:
                return "13 - Landesredaktion Mecklenburg-Vorpommern"
            case NummernkreisOption.LANDESREDAKTION_SAXONY:
                return "14 - Landesredaktion Sachsen"
            case NummernkreisOption.LANDESREDAKTION_SAXONY_ANHALT:
                return "15 - Landesredaktion Sachsen-Anhalt"
            case NummernkreisOption.LANDESREDAKTION_THURINGIA:
                return "16 - Landesredaktion Thüringen"
            case NummernkreisOption.VIRTUELLE_LANDESREDAKTION:
                return "17 - Phönix (temporär für OZG)"
            case NummernkreisOption.FIM_BAUSTEINE:
                return "60 - BOB - zentraler Baukasten"
            case NummernkreisOption.ATLANTIS:
                return "81 - Atlantis - OZG-Referenzsystem"
            case NummernkreisOption.TESTREPOSITORY:
                return "82 - Testrepository"
            case NummernkreisOption.KATE:
                return "99 - KATE  - zentraler Katalog"


def feldart_to_label(feldart: Feldart) -> str:
    match feldart:
        case Feldart.EINGABE:
            return "Eingabe"
        case Feldart.AUSWAHL:
            return "Auswahl"
        case Feldart.STATISCH:
            return "Statisch"
        case Feldart.VERSTECKT:
            return "Versteckt"
        case Feldart.GESPERRT:
            return "Gesperrt"


def datentyp_to_label(datentyp: Datentyp) -> str:
    match datentyp:
        case Datentyp.TEXT:
            return "Text"
        case Datentyp.STRING_LATIN:
            return "String.Latin+ 1.2"
        case Datentyp.DATUM:
            return "Datum"
        case Datentyp.ZEIT:
            return "Zeit (Stunde und Minute)"
        case Datentyp.ZEITPUNKT:
            return "Zeitpunkt (Datum und Zeit)"
        case Datentyp.WAHRHEITSWERT:
            return "Wahrheitswert"
        case Datentyp.NUMMER:
            return "Nummer"
        case Datentyp.GANZZAHL:
            return "Ganzzahl"
        case Datentyp.GELDBETRAG:
            return "Geldbetrag"
        case Datentyp.ANLAGE:
            return "Anlage (Datei)"
        case Datentyp.OBJEKT:
            return "Objekt (Blob)"


@dataclass(slots=True)
class BausteinLink:
    label: str
    initial_resource: Resource
    is_active: bool
    search_term: str | None

    def get_href(self) -> str:
        if self.search_term:
            return f"/search?resource={self.initial_resource.value}&term={self.search_term}"
        return f"/search?resource={self.initial_resource.value}"


@dataclass(slots=True)
class SearchBar:
    """
    Data for /templates/components/search.html.jinja
    """

    value: str | None
    search_in_filter: SingleSelectFilter | None
    sort_by_filter: SingleSelectFilter | None


SCHEMA_BOB_URL = UiFilters(
    Resource.SCHEMA,
    freigabe_status=[FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD],
    nummernkreis=NummernkreisOption.FIM_BAUSTEINE,
).to_search_url()

FIELD_BOB_URL = UiFilters(
    Resource.FIELD,
    freigabe_status=[FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD],
    nummernkreis=NummernkreisOption.FIM_BAUSTEINE,
).to_search_url()

GROUP_BOB_URL = UiFilters(
    Resource.GROUP,
    freigabe_status=[FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD],
    nummernkreis=NummernkreisOption.FIM_BAUSTEINE,
).to_search_url()

STECKBRIEF_KATE_URL = UiFilters(
    Resource.DOCUMENT_PROFILE,
    nummernkreis=NummernkreisOption.KATE,
).to_search_url()

BUNDES_STAMMPROZESSE_GOLD_URL = UiFilters(
    Resource.PROCESS,
    freigabe_status=[FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD],
    anwendungsgebiet=Anwendungsgebiet.BUND,
    detaillierungsstufe=Detaillierungsstufe.STAMMINFORMATIONEN,
).to_search_url()


@dataclass(slots=True)
class PredefinedFilter:
    name: str
    link: str


@dataclass(slots=True)
class BausteinSearchData:
    baustein: Baustein
    link_builder: LinkBuilder
    resource_links_by_category: list[ResourceCategoryLinks]
    results: (
        list[SchemaSearchResult]
        | list[SteckbriefSearchResult]
        | list[GroupSearchResult]
        | list[FieldSearchResult]
        | list[LeistungssteckbriefSearchResult]
        | list[LeistungsbeschreibungSearchResult]
        | list[ProzessSearchResult]
        | list[ProzessklasseSearchResult]
    )
    elapsed_milliseconds: int
    total_count: int
    available_pages: AvailablePages
    predefined_filters: list[PredefinedFilter]
    ui_single_select_filters: list[SingleSelectFilter]
    ui_multi_select_filters: list[MultiSelectFilter]
    csv_download_url: str
    search_bar: SearchBar

    def get_total_active_filters(self) -> int:
        return len(
            [filter for filter in self.ui_single_select_filters if filter.is_active]
        ) + len([filter for filter in self.ui_multi_select_filters if filter.is_active])


@dataclass(slots=True)
class ResourceCategory:
    label: str
    glossary_link: str


CATEGORY_KATALOG = ResourceCategory(
    "Katalog", "https://docs.fitko.de/fim/docs/glossar/#katalog"
)
CATEGORY_BIBLIOTHEK = ResourceCategory(
    "Bibliothek", "https://docs.fitko.de/fim/docs/glossar/#bibliothek"
)
CATEGORY_BAUKASTEN = ResourceCategory(
    "Baukasten", "https://docs.fitko.de/fim/docs/glossar/#baukasten"
)


@dataclass(slots=True)
class ResourceCategoryLinks:
    category: ResourceCategory | None
    links: list[ResourceLink]


async def _execute_leistung_search(
    filters: UiFilters,
    pagination_options: PaginationOptions,
    service: Service,
    search_query_string: str,
) -> BausteinSearchData:
    leistungssteckbrief_search_options = filters.to_leistungssteckbrief_search_options()
    leistungsbeschreibung_search_options = (
        filters.to_leistungsbeschreibung_search_options()
    )

    match filters.resource:
        case Resource.SERVICE:
            leistungssteckbrief_search_options.pagination_options = pagination_options
        case Resource.SERVICE_DESCRIPTION:
            leistungsbeschreibung_search_options.pagination_options = pagination_options
        case _:
            assert False

    start = time.perf_counter()
    leistungsbeschreibungen = await service.search_leistungsbeschreibungen(
        leistungsbeschreibung_search_options
    )
    leistungssteckbriefe = await service.search_leistungssteckbriefe(
        leistungssteckbrief_search_options
    )
    elapsed_milliseconds = int((time.perf_counter() - start) * 1000)

    match filters.resource:
        case Resource.SERVICE:
            results = leistungssteckbriefe.map(
                lambda leistung: LeistungssteckbriefSearchResult.from_leistungssteckbrief(
                    leistung,
                    leistungssteckbrief_search_options.order_by,
                    filters.leistung_suche_in,
                    filters.term,
                    search_query_string,
                )
            ).items
            total_count = leistungssteckbriefe.total_count
            available_pages = leistungssteckbriefe.get_available_pages()
            ui_single_select_filters = [
                get_leistungstyp_options(filters),
                get_leistungstypisierung_options(filters),
                get_freigabe_status_katalog_options(filters),
                get_freigabe_status_bibliothek_options(filters),
                get_einheitlicher_ansprechpartner_options(filters),
            ]
            sort_by_filter = _get_leistungssteckbrief_sort_options(
                leistungssteckbrief_search_options.order_by
            )
        case Resource.SERVICE_DESCRIPTION:
            results = leistungsbeschreibungen.map(
                lambda leistung: LeistungsbeschreibungSearchResult.from_leistungsbeschreibung(
                    leistung,
                    leistungsbeschreibung_search_options.order_by,
                    filters.leistung_suche_in,
                    filters.term,
                    search_query_string,
                )
            ).items
            total_count = leistungsbeschreibungen.total_count
            available_pages = leistungsbeschreibungen.get_available_pages()
            ui_single_select_filters = [
                get_leistungstyp_options(filters),
                get_leistungstypisierung_options(filters),
                get_leistung_redaktion_id_options(filters),
                get_einheitlicher_ansprechpartner_options(filters),
            ]
            sort_by_filter = _get_leistungsbeschreibung_sort_options(
                leistungsbeschreibung_search_options.order_by,
            )

    link_builder = LinkBuilder(filters, page_size=pagination_options.limit)

    return BausteinSearchData(
        baustein=Baustein.LEISTUNG,
        link_builder=link_builder,
        resource_links_by_category=[
            ResourceCategoryLinks(
                None,
                [
                    link_builder.get_resource_link(
                        Resource.SERVICE, total_count=leistungssteckbriefe.total_count
                    ),
                    link_builder.get_resource_link(
                        Resource.SERVICE_DESCRIPTION,
                        total_count=leistungsbeschreibungen.total_count,
                    ),
                ],
            )
        ],
        results=results,
        elapsed_milliseconds=elapsed_milliseconds,
        total_count=total_count,
        available_pages=available_pages,
        predefined_filters=[],
        ui_single_select_filters=ui_single_select_filters,
        ui_multi_select_filters=[],
        csv_download_url=filters.to_csv_download_url(),
        search_bar=SearchBar(
            value=filters.term,
            search_in_filter=_get_leistung_suche_in_options(filters),
            sort_by_filter=sort_by_filter,
        ),
    )


async def _execute_datenfelder_search(
    filters: UiFilters,
    pagination_options: PaginationOptions,
    service: Service,
    search_query_string: str,
) -> BausteinSearchData:
    schema_search_options = filters.to_schema_search_options()
    steckbrief_search_options = filters.to_steckbrief_search_options()
    group_search_options = filters.to_group_search_options()
    field_search_options = filters.to_field_search_options()

    match filters.resource:
        case Resource.SCHEMA:
            schema_search_options.pagination_options = pagination_options
        case Resource.DOCUMENT_PROFILE:
            steckbrief_search_options.pagination_options = pagination_options
        case Resource.GROUP:
            group_search_options.pagination_options = pagination_options
        case Resource.FIELD:
            field_search_options.pagination_options = pagination_options
        case _:
            assert False

    start = time.perf_counter()
    schemas = await service.search_schemas(schema_search_options)
    steckbriefe = await service.search_steckbriefe(steckbrief_search_options)
    groups = await service.search_groups(group_search_options)
    fields = await service.search_fields(field_search_options)
    elapsed_milliseconds = int((time.perf_counter() - start) * 1000)

    match filters.resource:
        case Resource.SCHEMA:
            results = schemas.map(
                lambda schema: SchemaSearchResult.from_schema_out(
                    schema, filters.schema_suche_in, filters.term, search_query_string
                )
            ).items
            total_count = schemas.total_count
            available_pages = schemas.get_available_pages()
            predefined_filters = [PredefinedFilter("BOB", SCHEMA_BOB_URL)]
            ui_single_select_filters = [
                get_nummernkreis_options(filters),
                get_xdf_version_options(filters),
            ]
            ui_multi_select_filters = [get_freigabe_status_options(filters)]
            search_in_filter = get_schema_suche_in_options(filters)
            sort_by_filter = _get_schema_sort_options(schema_search_options.order_by)
        case Resource.DOCUMENT_PROFILE:
            results = results = steckbriefe.map(
                lambda steckbrief: SteckbriefSearchResult.from_steckbrief(
                    steckbrief,
                    filters.steckbrief_suche_in,
                    filters.term,
                    search_query_string,
                )
            ).items
            total_count = steckbriefe.total_count
            available_pages = steckbriefe.get_available_pages()
            predefined_filters = [PredefinedFilter("KATE", STECKBRIEF_KATE_URL)]
            ui_single_select_filters = [
                get_nummernkreis_options(filters),
                get_dokumentart_options(filters),
                get_xdf_version_options(filters),
            ]
            ui_multi_select_filters = [get_freigabe_status_options(filters)]
            search_in_filter = get_steckbrief_suche_in_options(filters)
            sort_by_filter = None
        case Resource.GROUP:
            results = groups.map(
                lambda group: GroupSearchResult.from_group(
                    group, filters.gruppe_suche_in, filters.term, search_query_string
                )
            ).items
            total_count = groups.total_count
            available_pages = groups.get_available_pages()
            predefined_filters = [PredefinedFilter("BOB", GROUP_BOB_URL)]
            ui_single_select_filters = [
                get_nummernkreis_options(filters),
                get_xdf_version_options(filters),
            ]
            ui_multi_select_filters = [get_freigabe_status_options(filters)]
            search_in_filter = get_gruppe_suche_in_options(filters)
            sort_by_filter = None
        case Resource.FIELD:
            results = fields.map(
                lambda field: FieldSearchResult.from_field(
                    field, filters.feld_suche_in, filters.term, search_query_string
                )
            ).items
            total_count = fields.total_count
            available_pages = fields.get_available_pages()
            predefined_filters = [PredefinedFilter("BOB", FIELD_BOB_URL)]
            ui_single_select_filters = [
                get_feldart_options(filters),
                get_datentyp_options(filters),
                get_nummernkreis_options(filters),
                get_xdf_version_options(filters),
            ]
            ui_multi_select_filters = [get_freigabe_status_options(filters)]
            search_in_filter = get_feld_suche_in_options(filters)
            sort_by_filter = None

    link_builder = LinkBuilder(filters, page_size=pagination_options.limit)

    return BausteinSearchData(
        baustein=Baustein.DATENFELDER,
        link_builder=link_builder,
        resource_links_by_category=[
            ResourceCategoryLinks(
                CATEGORY_KATALOG,
                [
                    link_builder.get_resource_link(
                        Resource.DOCUMENT_PROFILE, total_count=steckbriefe.total_count
                    )
                ],
            ),
            ResourceCategoryLinks(
                CATEGORY_BIBLIOTHEK,
                [
                    link_builder.get_resource_link(
                        Resource.SCHEMA, total_count=schemas.total_count
                    )
                ],
            ),
            ResourceCategoryLinks(
                CATEGORY_BAUKASTEN,
                [
                    link_builder.get_resource_link(
                        Resource.GROUP, total_count=groups.total_count
                    ),
                    link_builder.get_resource_link(
                        Resource.FIELD, total_count=fields.total_count
                    ),
                ],
            ),
        ],
        results=results,
        elapsed_milliseconds=elapsed_milliseconds,
        total_count=total_count,
        available_pages=available_pages,
        predefined_filters=predefined_filters,
        ui_single_select_filters=ui_single_select_filters,
        ui_multi_select_filters=ui_multi_select_filters,
        csv_download_url=filters.to_csv_download_url(),
        search_bar=SearchBar(
            value=filters.term,
            search_in_filter=search_in_filter,
            sort_by_filter=sort_by_filter,
        ),
    )


async def _execute_prozesse_search(
    filters: UiFilters,
    pagination_options: PaginationOptions,
    service: Service,
    search_query_string: str,
) -> BausteinSearchData:
    prozess_search_options = filters.to_prozess_search_options()
    musterprozess_search_options = filters.to_musterprozess_search_options()
    prozessklasse_search_options = filters.to_prozessklasse_search_options()

    match filters.resource:
        case Resource.PROCESSCLASS:
            prozessklasse_search_options.pagination_options = pagination_options
        case Resource.PROCESS:
            prozess_search_options.pagination_options = pagination_options
        case Resource.SAMPLEPROCESS:
            musterprozess_search_options.pagination_options = pagination_options
        case _:
            assert False

    start = time.perf_counter()
    prozessklassen = await service.search_prozessklassen(prozessklasse_search_options)
    prozesse = await service.search_prozesse(prozess_search_options)
    musterprozesse = await service.search_prozesse(musterprozess_search_options)
    elapsed_milliseconds = int((time.perf_counter() - start) * 1000)

    match filters.resource:
        case Resource.PROCESSCLASS:
            results = prozessklassen.map(
                lambda prozessklasse: ProzessklasseSearchResult.from_prozessklasse(
                    prozessklasse, search_query_string
                )
            ).items
            total_count = prozessklassen.total_count
            available_pages = prozessklassen.get_available_pages()
            predefined_filters = []
            ui_single_select_filters = [
                get_operatives_ziel_options(filters),
                get_handlungsform_options(filters),
                get_verfahrensart_options(filters),
            ]
        case Resource.PROCESS:
            results = prozesse.map(
                lambda prozess: ProzessSearchResult.from_prozess(
                    prozess, search_query_string
                )
            ).items
            total_count = prozesse.total_count
            available_pages = prozesse.get_available_pages()
            predefined_filters = [
                PredefinedFilter(
                    "Bundes-Stammprozesse Gold", BUNDES_STAMMPROZESSE_GOLD_URL
                )
            ]
            ui_single_select_filters = [
                get_detaillierungsstufe_options(filters),
                get_anwendungsgebiet_options(filters),
            ]
        case Resource.SAMPLEPROCESS:
            results = musterprozesse.map(
                lambda musterprozess: ProzessSearchResult.from_prozess(
                    musterprozess, search_query_string
                )
            ).items
            total_count = musterprozesse.total_count
            available_pages = musterprozesse.get_available_pages()
            predefined_filters = []
            ui_single_select_filters = [
                get_anwendungsgebiet_options(filters),
            ]

    ui_multi_select_filters = [get_freigabe_status_options(filters)]
    link_builder = LinkBuilder(filters, page_size=pagination_options.limit)

    return BausteinSearchData(
        baustein=Baustein.PROZESSE,
        link_builder=link_builder,
        resource_links_by_category=[
            ResourceCategoryLinks(
                CATEGORY_KATALOG,
                [
                    link_builder.get_resource_link(
                        Resource.PROCESSCLASS, total_count=prozessklassen.total_count
                    )
                ],
            ),
            ResourceCategoryLinks(
                CATEGORY_BIBLIOTHEK,
                [
                    link_builder.get_resource_link(
                        Resource.PROCESS, total_count=prozesse.total_count
                    )
                ],
            ),
            ResourceCategoryLinks(
                CATEGORY_BAUKASTEN,
                [
                    link_builder.get_resource_link(
                        Resource.SAMPLEPROCESS, total_count=musterprozesse.total_count
                    ),
                ],
            ),
        ],
        results=results,
        elapsed_milliseconds=elapsed_milliseconds,
        total_count=total_count,
        available_pages=available_pages,
        predefined_filters=predefined_filters,
        ui_single_select_filters=ui_single_select_filters,
        ui_multi_select_filters=ui_multi_select_filters,
        csv_download_url=filters.to_csv_download_url(),
        search_bar=SearchBar(
            value=filters.term,
            search_in_filter=None,
            sort_by_filter=None,
        ),
    )


def create_search_router(
    get_service: Callable[[], AsyncGenerator[Service, Any]],
    render: RenderFunction,
) -> APIRouter:
    router = APIRouter()

    @router.get("", response_class=HTMLResponse)
    async def _search(  # type: ignore [reportUnusedFunction]
        request: Request,
        filters: UiFilters = Depends(get_ui_filters),
        pagination_options: PaginationOptions = Depends(get_ui_pagination_options),
        service: Service = Depends(get_service),
    ):
        search_query_string = create_clean_query_string(request, filters.resource)

        match filters.resource:
            case (
                Resource.DOCUMENT_PROFILE
                | Resource.SCHEMA
                | Resource.GROUP
                | Resource.FIELD
            ):
                search_result = await _execute_datenfelder_search(
                    filters, pagination_options, service, search_query_string
                )
            case Resource.SERVICE | Resource.SERVICE_DESCRIPTION:
                search_result = await _execute_leistung_search(
                    filters, pagination_options, service, search_query_string
                )
            case Resource.PROCESS | Resource.PROCESSCLASS | Resource.SAMPLEPROCESS:
                search_result = await _execute_prozesse_search(
                    filters, pagination_options, service, search_query_string
                )

        return render(
            request,
            "search.html.jinja",
            {
                "titleTag": "Suche • FIM Portal",
                "pagination": search_result.link_builder.get_pagination_options(
                    search_result.available_pages
                ),
                "active_filters": search_result.get_total_active_filters(),
                "remove_filters_link": (
                    f"/search?resource={filters.resource.value}&term={search_result.search_bar.value}"
                    if search_result.search_bar.value is not None
                    else f"/search?resource={filters.resource.value}"
                ),
                "filters": filters,
                "baustein_links": [
                    BausteinLink(
                        "Leistungen",
                        initial_resource=Resource.SERVICE,
                        is_active=search_result.baustein == Baustein.LEISTUNG,
                        search_term=search_result.search_bar.value,
                    ),
                    BausteinLink(
                        "Datenfelder",
                        initial_resource=Resource.DOCUMENT_PROFILE,
                        is_active=search_result.baustein == Baustein.DATENFELDER,
                        search_term=search_result.search_bar.value,
                    ),
                    BausteinLink(
                        "Prozesse",
                        initial_resource=Resource.PROCESSCLASS,
                        is_active=search_result.baustein == Baustein.PROZESSE,
                        search_term=search_result.search_bar.value,
                    ),
                ],
                "search_result": search_result,
                "page_size": pagination_options.limit,
            },
            active_tab="bausteinsuche",
        )

    return router
