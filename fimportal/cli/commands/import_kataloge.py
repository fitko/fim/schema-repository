import asyncio
import io
import logging
import zipfile
from zlib import Z_BEST_COMPRESSION

import click
from asyncpg import Pool

from fimportal import kataloge
from fimportal.cli.commands.util import create_service
from fimportal.config import AppConfig
from fimportal.models.kataloge import NewKatalog

logger = logging.getLogger(__name__)


@click.command()
def run_kataloge_import():
    asyncio.run(_execute_import())


async def _execute_import():
    config = AppConfig.load_and_init_from_env()
    assert config.kataloge is not None

    client = kataloge.HttpClient(config.kataloge.resource_url)

    async with config.create_connection_pool() as pool:
        await sync_kataloge(client, pool, config.immutable_base_url)


async def sync_kataloge(client: kataloge.Client, pool: Pool, immutable_base_url: str):
    for katalog in kataloge.XzufiKatalogIdentifier:
        filename = katalog.get_source_filename()

        try:
            content = await client.load_katalog_content(filename)
        except kataloge.KatalogException as error:
            logger.error("Skip Katalog: %s", error)
            continue

        content = zip_file(filename, content)

        async with create_service(immutable_base_url, pool) as service:
            await service.import_katalog(
                NewKatalog(
                    source="XZUFI_KATALOG", filename=f"{filename}.zip", content=content
                )
            )


def zip_file(filename: str, content: bytes):
    zip_buffer = io.BytesIO()
    with zipfile.ZipFile(
        zip_buffer,
        "w",
        # The usual compression method for ZIP files.
        compression=zipfile.ZIP_DEFLATED,
        compresslevel=Z_BEST_COMPRESSION,
    ) as zip_file:
        zip_file.writestr(filename, content)

    return zip_buffer.getvalue()
