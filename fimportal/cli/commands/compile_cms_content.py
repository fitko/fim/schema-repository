import contextlib
import logging
import os
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Generator

import click
import httpx

from fimportal import cms
from fimportal.config import AppConfig

logger = logging.getLogger(__name__)

BASE_URL = "https://cms.fim.fitko.net"


EXTERNAL_FILES: list[tuple[str, str]] = [
    (
        cms.FileIdentifier.PROZESSE_KATALOG.value,
        "https://gitlab.opencode.de/fitko/fim/docs/-/raw/main/docs/prozesse/FIM_Prozesskatalog.xlsx",
    )
]


@click.command()
def compile_cms_content():
    """
    Download, validate and combine all data from the CMS.
    Also try to download and update any external files (i.e. from the docs) into the external_cache_dir.

    Directly writing to the final output location could result in an inconsistent state in case of an error during the download.
    Therefore, all of the final data is first assembled into intermediate directories.
    Only after all data has been successfully downloaded and validated, the final data is written to the actual output locations.
    """

    config = AppConfig.load_and_init_from_env()
    assert config.cms.cockpit_token is not None

    with _get_target_locations(config) as (
        cache_dir,
        content_file_path,
        assets_dir,
        content_assets_dir,
        external_cache_dir,
    ):
        content_assets_cache_dir = cache_dir / "content_assets"
        content_assets_cache_dir.mkdir(exist_ok=True)
        assets_cache_dir = cache_dir / "assets"
        assets_cache_dir.mkdir(exist_ok=True)

        # Step 1: Update the external files in the external_cache_dir
        _update_external_files(external_cache_dir)

        # Step 2: download and validate the cms data
        client = cms.CMSClient(
            base_url=BASE_URL,
            token=config.cms.cockpit_token,
            assets_dir=assets_cache_dir,
            content_assets_dir=content_assets_cache_dir,
        )
        content = cms.CMSContent.load_from_cms(client)

        # Step 3: Overwrite the assets with the external files from the cache
        for _, _, filenames in os.walk(external_cache_dir):
            for filename in filenames:
                source = external_cache_dir / filename
                target = assets_cache_dir / filename

                target.write_bytes(source.read_bytes())
                logger.info(f"Overwrite {target}")

        # Step 4: At this point, all data has been successfully downloaded and validated.
        # Also, the intermediate cache directories contain the final state of the data, which can now be synced to the actual target directories.
        _sync_dir(content_assets_cache_dir, content_assets_dir)
        _sync_dir(assets_cache_dir, assets_dir)

        content_file_path.write_text(content.model_dump_json())
        logger.info(f"Updated {content_file_path}")


def _update_external_files(external_cache_dir: Path):
    client = httpx.Client()
    for filename, url in EXTERNAL_FILES:
        try:
            response = client.get(url)
            response.raise_for_status()
        except:
            logger.exception(f"Failed to download {filename}: {url}")
        else:
            target = external_cache_dir / filename
            target.write_bytes(response.content)
            logger.info(f"Imported {target}")


def _sync_dir(source_dir: Path, target_dir: Path):
    # Copy new files
    for _, _, filenames in os.walk(source_dir):
        for filename in filenames:
            source = source_dir / filename
            target = target_dir / filename

            content = source.read_bytes()
            target.write_bytes(content)
            logger.info(f"Created {target}")

    # Remove old files
    for _, _, filenames in os.walk(target_dir):
        for filename in filenames:
            source = source_dir / filename
            target = target_dir / filename

            if not source.exists():
                target.unlink()
                logger.info(f"Removed {target}")


@contextlib.contextmanager
def _get_target_locations(
    config: AppConfig,
) -> Generator[tuple[Path, Path, Path, Path, Path], None, None]:
    assert config.cms.assets_dir is not None
    assert config.cms.content_assets_dir is not None
    assert config.cms.external_cache_dir is not None

    assets_dir = Path(config.cms.assets_dir)
    assets_dir.mkdir(parents=True, exist_ok=True)

    content_assets_dir = Path(config.cms.content_assets_dir)
    content_assets_dir.mkdir(parents=True, exist_ok=True)

    external_cache_dir = Path(config.cms.external_cache_dir)
    external_cache_dir.mkdir(parents=True, exist_ok=True)

    content_file_path = Path(config.cms.data_file)

    if config.cms.cache_dir is None:
        with TemporaryDirectory() as cache_dir:
            yield (
                Path(cache_dir),
                content_file_path,
                assets_dir,
                content_assets_dir,
                external_cache_dir,
            )
    else:
        cache_dir = Path(config.cms.cache_dir)
        cache_dir.mkdir(parents=True, exist_ok=True)

        yield (
            cache_dir,
            content_file_path,
            assets_dir,
            content_assets_dir,
            external_cache_dir,
        )
