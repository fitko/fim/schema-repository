import base64
import random

import bcrypt
import click


@click.command()
def generate_admin_password():
    """
    Select a random password and hash it via bcrypt.
    The resulting hash can then be used in the app config.
    """
    password = base64.b64encode(random.randbytes(32))
    hash = bcrypt.hashpw(password, bcrypt.gensalt())

    print("Password:", password.decode("utf-8"))
    print("Hash:", hash.decode("utf-8"))
