import click
import orjson
from fimportal import xrepository
from fimportal.cms import CMSContent

from fimportal.main import create_app_with_config
from fimportal.xzufi import link_check


@click.command()
@click.argument("filepath")
def dump_openapi_specs(filepath: str):
    # The construction of the app does not actually connect to the database.
    # In order to dump the openapi specs, create an app instance with a dummy url,
    # as a valid app instance is needed to generate the docs.
    dummy_db_url = "postgresql://test_user:abcd1234@postgres:5432/test_db"

    with open("example_content.json") as f:
        cms_content = CMSContent.model_validate_json(f.read())

    app = create_app_with_config(
        database_url=dummy_db_url,
        xrepository_client=xrepository.FakeClient(),
        link_checker=link_check.FakeLinkChecker(),
        admin_password_hash="test",
        baseurl="test",
        immutable_base_url="test",
        user_tracking=False,
        cms_content=cms_content,
        cms_assets_dir=None,
        cms_content_assets_dir=None,
        show_production_header=False,
        sentry_config=None,
    )

    openapi_specs = app.openapi()

    with open(filepath, "wb") as file:
        file.write(orjson.dumps(openapi_specs))
