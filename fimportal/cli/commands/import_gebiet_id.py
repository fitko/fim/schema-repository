import asyncio
import logging

import click
from asyncpg import Pool

from fimportal import xrepository
from fimportal.cli.commands.util import create_service
from fimportal.config import AppConfig


GEBIET_ID_REGIONALSCHLUESSEL_URI = (
    "urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:rs"
)
GEBIET_ID_KREIS_URI = "urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:kreis"
GEBIET_ID_GEMEINDEVERBAND_URI = (
    "urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:gemeindeverband"
)
GEBIET_ID_BEZIRK_URI = "urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:bezirk"

GEBIET_ID_URIs = [
    GEBIET_ID_REGIONALSCHLUESSEL_URI,
    GEBIET_ID_KREIS_URI,
    GEBIET_ID_GEMEINDEVERBAND_URI,
    GEBIET_ID_BEZIRK_URI,
]

logger = logging.getLogger(__name__)


@click.command()
def run_gebiet_id_import():
    asyncio.run(_execute())


async def _execute():
    config = AppConfig.load_and_init_from_env()
    client = xrepository.HttpClient()

    async with config.create_connection_pool() as pool:
        await sync_gebiet_id(pool, config.immutable_base_url, client)


async def sync_gebiet_id(
    pool: Pool,
    immutable_base_url: str,
    xrepository_client: xrepository.Client,
):
    for uri in GEBIET_ID_URIs:
        kennungen = await xrepository_client.load_code_list_kennungen(uri)
        logger.info("found %d kennungen for codelist %s", len(kennungen), uri)
        for kennung in kennungen:
            code_list, _ = await xrepository_client.load_code_list(kennung)
            async with create_service(immutable_base_url, pool) as service:
                await service.import_gebiet_id(code_list)
