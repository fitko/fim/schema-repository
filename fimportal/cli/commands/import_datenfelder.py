import asyncio
import logging
import os
from typing import Iterator

import click
import httpx
from asyncpg import Pool

from fimportal.config import AppConfig
from fimportal.crawler import load_schema_imports
from fimportal.errors import (
    DatabaseDeadlockException,
    ImportException,
    StatusHasNotChangedException,
)
from fimportal.helpers import retry
from fimportal.models.imports import (
    Xdf2SchemaImport,
    Xdf2SteckbriefImport,
    Xdf3SchemaImport,
)
from fimportal.service import Service
from fimportal.xdatenfelder import xdf2, xdf3

from .util import create_service

HTTP_RETRIES = 5
HTTP_TIMEOUT = 60


logger = logging.getLogger(__name__)


@click.command()
def run_datenfeld_import():
    """
    Import all changed files from the external repositories.
    """

    asyncio.run(_command())


async def _command():
    config = AppConfig.load_and_init_from_env()
    await execute_schema_import(config)


async def execute_schema_import(config: AppConfig):
    async with config.create_connection_pool() as pool:
        await _import_xdf2_schemas(config.immutable_base_url, pool)
        await _import_xdf3_schemas(config.immutable_base_url, pool)

        repo_urls = config.get_import_urls()
        async with httpx.AsyncClient(
            timeout=HTTP_TIMEOUT,
            transport=httpx.AsyncHTTPTransport(
                retries=HTTP_RETRIES,
            ),
        ) as client:
            async with asyncio.TaskGroup() as tg:
                for url in repo_urls:
                    tg.create_task(
                        _import_repository(config.immutable_base_url, pool, client, url)
                    )


async def _import_xdf2_schemas(immutable_base_url: str, pool: Pool):
    for xdf2_content in _iterate_xdf_files("./xdf2-schemas"):
        message = xdf2.parse_schema_message(xdf2_content)

        async with create_service(immutable_base_url, pool) as service:
            if (
                await service.get_schema(
                    message.id,
                    message.assert_version(),
                )
                is not None
            ):
                continue

            xdf2_import = Xdf2SchemaImport(
                schema_text=xdf2_content,
                schema_message=message,
                freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
                steckbrief_id=None,
                code_list_imports=[],
            )

            await _import_xdf2_schema(service, xdf2_import)


async def _import_xdf3_schemas(immutable_base_url: str, pool: Pool):
    for xdf3_content in _iterate_xdf_files("./xdf3-schemas"):
        message = xdf3.parse_schema_message(xdf3_content)

        async with create_service(immutable_base_url, pool) as service:
            if (
                await service.get_schema(
                    message.id, message.schema.identifier.assert_version()
                )
                is not None
            ):
                continue

            xdf3_import = Xdf3SchemaImport(
                schema_text=xdf3_content,
                schema_message=message,
            )

            await _import_xdf3_schema(service, xdf3_import)


def _iterate_xdf_files(dirpath: str) -> Iterator[str]:
    try:
        filenames = os.listdir(dirpath)
    except FileNotFoundError:
        logger.info(f"Skip importing files from {dirpath}: Directory not found")
        return

    for filename in filenames:
        filepath = os.path.join(dirpath, filename)

        _, ext = os.path.splitext(filename)
        if ext == ".xml":
            with open(filepath, "r", encoding="utf-8") as file:
                yield file.read()
        else:
            continue


async def _import_repository(
    immutable_base_url: str,
    pool: Pool,
    client: httpx.AsyncClient,
    url: str,
):
    async for xdf2_imports in load_schema_imports(client, url):
        async with create_service(immutable_base_url, pool) as service:
            await _import_xdf2_schema(service, xdf2_imports)


@retry(3, DatabaseDeadlockException)
async def _import_xdf2_schema(
    service: Service, xdf2_import: Xdf2SchemaImport | Xdf2SteckbriefImport
):
    try:
        if isinstance(xdf2_import, Xdf2SteckbriefImport):
            await service.import_xdf2_steckbrief(xdf2_import, check_status=True)
        else:
            # NOTE(Felix): Import as not strict, as the legacy data contains
            # elements from other repositories, which are not guaranteed to be
            # saved already for the initial import.
            await service.import_xdf2_schema(
                xdf2_import, strict=False, check_status=True
            )
    except StatusHasNotChangedException:
        logger.info(
            "Skip schema: freigabe_status has not changed [id=%s, version=%s]",
            xdf2_import.id,
            xdf2_import.version,
        )
    except ImportException:
        logger.warning(
            'Invalid freigabe_status "%s" [id=%s, version=%s]',
            xdf2_import.freigabe_status,
            xdf2_import.id,
            xdf2_import.version,
        )
    except Exception:
        logger.exception(
            "Failed to import schema [id=%s, version=%s]",
            xdf2_import.id,
            xdf2_import.version,
        )


@retry(3, DatabaseDeadlockException)
async def _import_xdf3_schema(service: Service, xdf3_import: Xdf3SchemaImport):
    try:
        await service.import_xdf3_schema(xdf3_import, strict=False, check_status=True)
    except StatusHasNotChangedException:
        logger.info(
            "Skip schema: freigabe_status has not changed [id=%s, version=%s]",
            xdf3_import.id,
            xdf3_import.version,
        )
    except ImportException:
        logger.warning(
            'Invalid freigabe_status "%s" [id=%s, version=%s]',
            xdf3_import.freigabe_status,
            xdf3_import.id,
            xdf3_import.version,
        )
    except Exception:
        logger.exception(
            "Failed to import schema [id=%s, version=%s]",
            xdf3_import.id,
            xdf3_import.version,
        )
