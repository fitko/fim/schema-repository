import asyncio
import logging

import click

from fimportal.config import AppConfig
from fimportal.database import get_connection
from fimportal.migrations.execute import execute_migrations

logger = logging.getLogger(__name__)


@click.command()
def migrate():
    """
    Execute all new database migrations and
    import all new schemas from "./xdf2-schemas" and "./xdf3-schemas".
    """

    asyncio.run(_command())


async def _command():
    config = AppConfig.load_and_init_from_env()
    await execute_migrate(config)


async def execute_migrate(config: AppConfig):
    await execute_migrations(await get_connection(config.get_full_database_url()))
