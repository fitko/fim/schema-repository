import logging, asyncio

import click
from asyncpg import Pool

from fimportal import xprozesse
from fimportal.cli.commands.util import create_service
from fimportal.config import AppConfig


logger = logging.getLogger(__name__)


@click.command()
def run_prozesse_import():
    asyncio.run(_execute_prozesse_sync())


async def _execute_prozesse_sync():
    config = AppConfig.load_and_init_from_env()
    assert config.xprozesse is not None

    client = xprozesse.HttpClient(
        config.xprozesse.base_url,
        config.xprozesse.api_key,
        config.xprozesse.certificate_filepath,
        config.xprozesse.key_filepath,
    )

    async with config.create_connection_pool() as pool:
        await sync_prozesse(client, config.immutable_base_url, pool)


async def sync_prozesse(
    client: xprozesse.Client,
    immutable_base_url: str,
    pool: Pool,
):
    logger.info("Started Prozess Sync")
    search_result = await client.get_prozess_data()

    logger.info(f"Received {len(search_result.prozesse)} Prozesse")
    logger.info(f"Received {len(search_result.prozessklassen)} Prozesseklassen")

    async with create_service(immutable_base_url, pool) as service:
        prozess_ids = await service.import_prozess_batch(search_result.prozesse)
        logger.info("Imported Prozesse")
        prozessklasse_ids = await service.import_prozessklasse_batch(
            search_result.prozessklassen
        )
        logger.info("Imported Prozesseklassen")

    async with create_service(immutable_base_url, pool) as service:
        existing_process_ids = await service.get_prozess_ids()

        for id in existing_process_ids:
            if id not in prozess_ids:
                await service.delete_prozess(id)
    logger.info("Deleted old Prozesse")

    async with create_service(immutable_base_url, pool) as service:
        existing_prozessklassen_ids = await service.get_prozessklassen_ids()

        for id in existing_prozessklassen_ids:
            if id not in prozessklasse_ids:
                await service.delete_prozessklasse(id)
    logger.info("Deleted old Prozessklassen")
