import asyncio
import logging
import click
from asyncpg import Pool

from fimportal import pvog, xml, xzufi
from fimportal.cli.commands.util import create_service
from fimportal.config import AppConfig
from fimportal.models.xzufi import PvogResourceClass, XzufiSource
from fimportal.service import Service
from fimportal.xzufi.common import (
    XZUFI_ID,
    XZUFI_ZUSTAENDIGKEIT,
    Identifikator,
    LeistungsId,
    LeistungsTypisierung,
    RedaktionsId,
    XzufiLeistungsIdentifier,
    parse_identifikator,
)
from fimportal.xzufi.transfer import (
    LoeschEvent,
    LoeschKlasse,
    SchreibEvent,
    SchreibKlasse,
    iter_transfer_events,
)

logger = logging.getLogger(__name__)


ALLOWED_LEIKA_API_TYPISIERUNGEN = frozenset(
    [
        LeistungsTypisierung.TYP_2,
        LeistungsTypisierung.TYP_2A,
        LeistungsTypisierung.TYP_2B,
        LeistungsTypisierung.TYP_3,
        LeistungsTypisierung.TYP_3A,
        LeistungsTypisierung.TYP_3B,
        LeistungsTypisierung.TYP_2_3,
        LeistungsTypisierung.TYP_2_3A,
        LeistungsTypisierung.TYP_2_3B,
    ]
)


@click.command()
def run_leistung_import():
    asyncio.run(_command())


async def _command():
    config = AppConfig.load_and_init_from_env()
    assert config.pvog is not None
    assert config.xzufi_api_key is not None

    pvog_client = pvog.HttpClient(
        base_url=config.pvog.base_url,
        client_id=config.pvog.client_id,
        client_secret=config.pvog.client_secret,
    )

    xzufi_client = xzufi.HttpClient.create_baustein_client(config.xzufi_api_key)

    await sync_leistungen(pvog_client, xzufi_client, config)


async def sync_leistungen(
    pvog_client: pvog.Client, xzufi_client: xzufi.Client, config: AppConfig
):
    async with config.create_connection_pool() as pool:
        await sync_xzufi_leistungen(xzufi_client, config.immutable_base_url, pool)
        await sync_pvog_resources(pvog_client, config.immutable_base_url, pool)


async def sync_pvog_resources(client: pvog.Client, immutable_base_url: str, pool: Pool):
    async with create_service(immutable_base_url, pool) as service:
        start_batch_index = await service.get_latest_pvog_batch_id()

    logger.info(f"Extracted previous last batch index: {start_batch_index}")
    batch_counter = 0
    async for batch in client.iterate_transfer_batches(start_batch_index):
        async with create_service(immutable_base_url, pool) as service:
            await service.save_pvog_batch(batch[0], batch[1])
            await sync_raw_pvog_resources(service, batch[1])
            if batch_counter % 10 == 0:
                logger.info(f"Current_batch: {batch[0]}")
            batch_counter += 1

    logger.info("Parsed PVOG events")

    async with create_service(immutable_base_url, pool) as service:
        await service.delete_removed_pvog_resources()
    logger.info("Deleted removed PVOG resources")

    while True:
        async with create_service(immutable_base_url, pool) as service:
            more_to_sync = await service.sync_pvog_leistungen()
            if not more_to_sync:
                break

    while True:
        async with create_service(immutable_base_url, pool) as service:
            more_to_sync = await service.sync_pvog_spezialisierung()
            if not more_to_sync:
                break

    while True:
        async with create_service(immutable_base_url, pool) as service:
            more_to_sync = await service.sync_pvog_organisationseinheit()
            if not more_to_sync:
                break

    while True:
        async with create_service(immutable_base_url, pool) as service:
            more_to_sync = await service.sync_pvog_zustaendigkeit()
            if not more_to_sync:
                break

    while True:
        async with create_service(immutable_base_url, pool) as service:
            more_to_sync = await service.sync_pvog_onlinedienst()
            if not more_to_sync:
                break


async def sync_xzufi_leistungen(
    xzufi_client: xzufi.Client,
    immutable_base_url: str,
    pool: Pool,
):
    """
    Sync the `Leistungen` from the xzufi system with our own database.

    IMPORTANT: This will add all new `Leistungen` and remove all Leistungen that are not longer part of
    the imported set of Leistungen. Deleting old Leistungen is just a temporary solution to get the system going until we switched to the
    production XZuFi System. This way, after the switch, all existing Leistungen will be removed and all new Leistungen from the production
    system will be inserted. After the switch is ready, we can think of a better deletion strategy (if any).
    """
    synced_leistungen = set()

    async for batch in xzufi_client.iterate_leistung_batches():
        async with create_service(immutable_base_url, pool) as service:
            imported_leistung_identifiers = await service.import_leistung_batch(
                batch,
                source=XzufiSource.XZUFI,
            )

        for identifier in imported_leistung_identifiers:
            synced_leistungen.add(identifier)

    # Also add the RedaktionsId from the client in case no Leistungen were actually imported
    # from the client.
    redaktions_ids = set([redaktions_id for redaktions_id, _ in synced_leistungen])
    redaktions_ids.add(xzufi_client.get_redaktions_id())

    # TODO(Felix): Rethink how we want to delete old Leistungen. Right now, valid data within our database
    # would be removed, if the latest version from the xzufi service has an error while importing, as it is then
    # not added to the set of synced leistungen.
    for redaktion_id in redaktions_ids:
        async with create_service(immutable_base_url, pool) as service:
            existing_identifiers = await service.get_leistung_identifiers_for_redaktion(
                redaktion_id
            )
            for identifier in existing_identifiers:
                if identifier not in synced_leistungen:
                    await service.delete_leistungsbeschreibung(identifier)
    async with create_service(immutable_base_url, pool) as service:
        await service.sync_links()


def _identifikator_to_xzufi_leistungs_identifier(
    identifikator: Identifikator,
) -> XzufiLeistungsIdentifier | None:
    if identifikator.scheme_agency_id is None:
        return None

    redaktion = RedaktionsId(identifikator.scheme_agency_id)
    id = LeistungsId(identifikator.value)

    return redaktion, id


def _get_identifier(node: xml.XmlElement) -> XzufiLeistungsIdentifier:
    for child in node:
        if child.tag == XZUFI_ID:
            xzufi_leistungs_identifier = _identifikator_to_xzufi_leistungs_identifier(
                parse_identifikator(child)
            )
            assert xzufi_leistungs_identifier is not None
            return xzufi_leistungs_identifier
    assert False


async def sync_raw_zustaendigkeit_transfer_event(
    service: Service, node: xml.XmlElement
):
    for child in node:
        if child.tag == XZUFI_ZUSTAENDIGKEIT:
            await service.save_raw_pvog_resource(
                _get_identifier(child),
                xml.serialize(child).decode("utf-8"),
                PvogResourceClass.ZUSTAENDIGKEIT,
            )


async def sync_raw_pvog_resources(service: Service, event_xml: str):
    event = xml.parse_document(event_xml)
    for transfer_event in iter_transfer_events(event):
        match transfer_event:
            case SchreibEvent(klasse=SchreibKlasse.LEISTUNG, node=node):
                await service.save_raw_pvog_resource(
                    _get_identifier(node),
                    xml.serialize(node).decode("utf-8"),
                    PvogResourceClass.LEISTUNG,
                )
            case SchreibEvent(klasse=SchreibKlasse.SPEZIALISIERUNG, node=node):
                await service.save_raw_pvog_resource(
                    _get_identifier(node),
                    xml.serialize(node).decode("utf-8"),
                    PvogResourceClass.SPEZIALISIERUNG,
                )
            case SchreibEvent(klasse=SchreibKlasse.ORGANISATIONSEINHEIT, node=node):
                await service.save_raw_pvog_resource(
                    _get_identifier(node),
                    xml.serialize(node).decode("utf-8"),
                    PvogResourceClass.ORGANISATIONSEINHEIT,
                )
            case SchreibEvent(
                klasse=SchreibKlasse.ZUSTAENDIGKEIT_TRANSFER_OBJEKT, node=node
            ):
                await sync_raw_zustaendigkeit_transfer_event(service, node)
            case SchreibEvent(klasse=SchreibKlasse.ONLINEDIENST, node=node):
                await service.save_raw_pvog_resource(
                    _get_identifier(node),
                    xml.serialize(node).decode("utf-8"),
                    PvogResourceClass.ONLINEDIENST,
                )
            case LoeschEvent(klasse=LoeschKlasse.LEISTUNG, id=id):
                identifier = _identifikator_to_xzufi_leistungs_identifier(id)
                if identifier is not None:
                    await service.delete_raw_pvog_resource(
                        identifier, PvogResourceClass.LEISTUNG
                    )
            case LoeschEvent(klasse=LoeschKlasse.ORGANISATIONSEINHEIT, id=id):
                identifier = _identifikator_to_xzufi_leistungs_identifier(id)
                if identifier is not None:
                    await service.delete_raw_pvog_resource(
                        identifier, PvogResourceClass.ORGANISATIONSEINHEIT
                    )
            case LoeschEvent(klasse=LoeschKlasse.SPEZIALISIERUNG, id=id):
                identifier = _identifikator_to_xzufi_leistungs_identifier(id)
                if identifier is not None:
                    await service.delete_raw_pvog_resource(
                        identifier, PvogResourceClass.SPEZIALISIERUNG
                    )
            case LoeschEvent(klasse=LoeschKlasse.ZUSTAENGIKEIT, id=id):
                identifier = _identifikator_to_xzufi_leistungs_identifier(id)
                if identifier is not None:
                    await service.delete_raw_pvog_resource(
                        identifier, PvogResourceClass.ZUSTAENDIGKEIT
                    )
            case LoeschEvent(klasse=LoeschKlasse.ONLINEDIENST, id=id):
                identifier = _identifikator_to_xzufi_leistungs_identifier(id)
                if identifier is not None:
                    await service.delete_raw_pvog_resource(
                        identifier, PvogResourceClass.ONLINEDIENST
                    )
            case _:
                pass
