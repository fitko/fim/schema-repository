import asyncio

import click
from asyncpg import Pool

from fimportal import xml, xprozesse, xzufi
from fimportal.config import AppConfig
from fimportal.database import reset_database as reset_db
from fimportal.models.imports import (
    Xdf2SchemaImport,
    Xdf2SteckbriefImport,
    Xdf3SchemaImport,
)
from fimportal.models.xzufi import XzufiSource
from fimportal.xdatenfelder import xdf3

from .util import create_service


@click.command()
@click.option(
    "--no-test-data", is_flag=True, default=False, help="Skip importing test data"
)
def resetdb(no_test_data: bool):
    """
    Reset and seed the database
    """

    asyncio.run(_command(no_test_data))


async def _command(no_test_data: bool):
    config = AppConfig.load_and_init_from_env()
    await reset_database(config, not no_test_data)


async def reset_database(config: AppConfig, import_test_data: bool):
    async with config.create_connection_pool() as pool:
        async with pool.acquire() as connection:
            await reset_db(connection)
        print("Database successfully reset.")

        if import_test_data:
            await _seed_database(config, pool)
        else:
            print("Skip importing test data.")


async def _seed_database(config: AppConfig, pool: Pool):
    from tests.data import XDF2_DATA, XDF3_DATA, XPROZESS2_DATA, XZUFI_2_2_0_DATA

    await _create_dev_api_tokens(pool, config)

    for schema_path, code_list_paths in [
        (
            XDF2_DATA / "schema.xml",
            [
                XDF2_DATA / "code_list_a.xml",
                XDF2_DATA / "code_list_b.xml",
            ],
        ),
    ]:
        async with create_service(config.immutable_base_url, pool) as service:
            await service.import_xdf2_schema(
                Xdf2SchemaImport.from_bytes(
                    schema_data=schema_path.read_bytes(),
                    code_list_data=[path.read_bytes() for path in code_list_paths],
                    freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
                    steckbrief_id=None,
                ),
                strict=False,
            )

    for steckbrief_path in [
        XDF2_DATA / "steckbrief.xdf2.xml",
        XDF2_DATA / "steckbrief_v1_1.xdf2.xml",
    ]:
        async with create_service(config.immutable_base_url, pool) as service:
            await service.import_xdf2_steckbrief(
                Xdf2SteckbriefImport.from_bytes(
                    steckbrief_path.read_bytes(),
                    freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
                ),
                check_status=False,
            )

    for schema_path in [
        XDF3_DATA / "hundesteuer.xdf3.xml",
        XDF3_DATA / "schema.xml",
    ]:
        async with create_service(config.immutable_base_url, pool) as service:
            await service.import_xdf3_schema(
                Xdf3SchemaImport.from_bytes(schema_path.read_bytes()), strict=False
            )

    for steckbrief_path in [
        XDF3_DATA / "steckbrief.xdf3.xml",
    ]:
        content = steckbrief_path.read_text()
        async with create_service(config.immutable_base_url, pool) as service:
            await service.import_xdf3_steckbrief(
                xdf3.parse_steckbrief_message(content), content, check_status=False
            )

    leistung_batch = []
    for leistung_filename in [
        "leistung_minimal.xml",
        "leistung_full.xml",
        "leistung_bearbeitungsdauer_empty.xml",
        "leistung_bearbeitungsdauer_frist_dauer.xml",
        "leistung_frist_dauer.xml",
        "leistung_frist_stichtag_datum.xml",
        "leistung_frist_stichtag_monat_tag.xml",
        "leistung_kosten_fix.xml",
        "leistung_kosten_frei.xml",
        "leistung_kosten_ungelisteter_typ.xml",
        "leistung_kosten_variabel.xml",
        "leistung_struktur_lo_leika.xml",
        "leistung_struktur_lo_individuell.xml",
        "leistung_struktur_lov.xml",
        "leistung_struktur_lovd.xml",
        "leistung_begriff_im_kontext.xml",
    ]:
        content = (XZUFI_2_2_0_DATA / leistung_filename).read_text()
        leistung = xzufi.parse_leistung(content)

        leistung_batch.append((leistung, content))

    async with create_service(config.immutable_base_url, pool) as service:
        await service.import_leistung_batch(
            leistungen=leistung_batch,
            source=XzufiSource.PVOG,
        )

    steckbrief_batch = []
    for leistung_filename in [
        "leistung_steckbrief.xml",
    ]:
        content = (XZUFI_2_2_0_DATA / leistung_filename).read_text()
        leistung = xzufi.parse_leistung(content)

        steckbrief_batch.append((leistung, content))

    async with create_service(config.immutable_base_url, pool) as service:
        await service.import_leistung_batch(
            leistungen=steckbrief_batch,
            source=XzufiSource.XZUFI,
        )

    for prozess_path in [
        XPROZESS2_DATA / "prozess_with_files.xml",
        XPROZESS2_DATA / "musterprozess.xml",
    ]:
        content = prozess_path.read_text()
        # TODO: This is a little hacky, as we also need the xml of a process here,
        # `xprozesse.parse_message` however only returns the parsed Prozess.
        # So we just reuse the `parse_search_result` method, which is technically not
        # for this use case.
        batch = xprozesse.parse_search_result(xml.parse_document(content))

        async with create_service(config.immutable_base_url, pool) as service:
            await service.import_prozess_batch(batch.prozesse)
            await service.import_prozessklasse_batch(batch.prozessklassen)


async def _create_dev_api_tokens(pool: Pool, config: AppConfig):
    async with create_service(config.immutable_base_url, pool) as auth:
        for id in range(0, 100):
            if id == 18 or id == 19:
                # NOTE(Felix): Those Nummernkreise are not used,
                # see: https://gitlab.opencode.de/fitko/fim/schema-repository/-/issues/211
                continue

            # XX000
            nummernkreis = str(id).rjust(2, "0") + "000"
            await auth.create_api_token(
                nummernkreis, description="test", token=f"upload_token_{nummernkreis}"
            )
