import asyncio
import zipfile
from zlib import Z_BEST_COMPRESSION

import click

from fimportal.config import AppConfig


@click.command()
@click.argument("archive")
def dump_pvog_transfer_log(archive: str):
    """
    Dump all pvog transfer messages into the specified zip archive.
    """

    asyncio.run(_execute_command(archive))


async def _execute_command(archive: str):
    config = AppConfig.load_and_init_from_env()
    connection = await config.create_connection()

    with zipfile.ZipFile(
        archive,
        "w",
        # The usual compression method for ZIP files.
        compression=zipfile.ZIP_DEFLATED,
        compresslevel=Z_BEST_COMPRESSION,
    ) as output_archive:
        current_batch = -1
        while True:
            row = await connection.fetchrow(
                """
                SELECT batch_id, batch_xml
                FROM pvog_batch
                WHERE batch_id > $1
                ORDER BY batch_id ASC
                LIMIT 1
                """,
                current_batch,
            )
            if row is None:
                return

            current_batch, batch_xml = row
            print("Processing batch", current_batch)

            output_archive.writestr(f"{current_batch}.xml", batch_xml)
