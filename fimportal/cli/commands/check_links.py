import asyncio
import logging
import click

from fimportal.cli.commands.util import create_service
from fimportal.config import AppConfig
from fimportal.helpers import utc_now


logger = logging.getLogger(__name__)


@click.command()
def check_links():
    asyncio.run(_execute_link_check())


async def _execute_link_check():
    config = AppConfig.load_and_init_from_env()

    now = utc_now()
    has_more = True
    async with config.create_connection_pool() as pool:
        while has_more:
            async with create_service(config.immutable_base_url, pool) as service:
                updated_links = await service.check_link_status(now, 20)
                has_more = updated_links > 0
