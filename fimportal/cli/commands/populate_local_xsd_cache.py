import os
from pathlib import Path
from typing import Any

import click
import httpx
import lxml.etree

from fimportal import xml, xzufi
from fimportal.xdatenfelder import xdf3

XSD_FILES = [
    xzufi.client.XSD_SCHEMA_PATH,
    xdf3.XSD_SCHEMA_PATH,
]


class CacheResolver(lxml.etree.Resolver):
    def __init__(self, cache_dir: Path, root_schema: str):
        self._cache_dir = cache_dir
        self._root_schema = root_schema

    def resolve(self, system_url: str, public_id: str, context: Any):  # type: ignore
        if system_url.startswith("http"):
            filename = xml.url_to_xsd_cache_filename(system_url)
            filepath = self._cache_dir / filename

            print(f"[{self._root_schema}]: Importing {system_url} into {filename}")

            response = httpx.get(system_url)
            response.raise_for_status()

            content = response.text
            filepath.write_text(content)

            return self.resolve_string(content, context, base_url=system_url)


@click.command()
def populate_local_xsd_cache():
    # Create the directory if not already present
    xml.XML_CACHE_DIR.mkdir(exist_ok=True)

    # Clear the cache
    for filename in os.listdir(xml.XML_CACHE_DIR):
        filepath = xml.XML_CACHE_DIR / filename
        if filepath.is_file():
            os.remove(filepath)

    # Load all included files for each root XSD file
    for xsd_file in XSD_FILES:
        root_schema = os.path.basename(xsd_file)
        resolver = CacheResolver(xml.XML_CACHE_DIR, root_schema)

        parser = lxml.etree.XMLParser(
            dtd_validation=False, load_dtd=False, no_network=True
        )
        parser.resolvers.add(resolver)

        document = lxml.etree.parse(xsd_file, parser=parser)
        lxml.etree.XMLSchema(document)
