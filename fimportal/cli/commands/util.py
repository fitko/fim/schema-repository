from contextlib import asynccontextmanager

from asyncpg import Pool

from fimportal import xrepository
from fimportal.service import Service
from fimportal.xzufi import link_check


@asynccontextmanager
async def create_service(immutable_base_url: str, pool: Pool):
    xrepository_service = xrepository.Service(xrepository.HttpClient())
    link_checker_service = link_check.Service(link_check.LinkChecker())

    async with Service.from_pool(
        immutable_base_url=immutable_base_url,
        pool=pool,
        xrepository_service=xrepository_service,
        link_checker=link_checker_service,
    ) as service:
        yield service
