import asyncio

import click

from fimportal.config import AppConfig

from .util import create_service


@click.command()
def check_generated_data():
    """
    Check if the generated data for each schema must be updated. This includes:

    - JSON Schema files
    - XSD files

    This command is run after every deployment.

    """
    config = AppConfig.load_from_env()

    asyncio.run(_check(config))


async def _check(config: AppConfig):
    async with config.create_connection_pool() as pool:
        async with create_service(config.immutable_base_url, pool) as service:
            identifiers = await service.get_all_schema_identifiers()

        for schema_id, schema_version in identifiers:
            async with create_service(config.immutable_base_url, pool) as service:
                await service.update_generated_data(schema_id, schema_version)
