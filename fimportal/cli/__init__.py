import logging
import logging.config
import subprocess

import click
import orjson

from fimportal.config import AppConfig

from .commands.check_generated_data import check_generated_data
from .commands.check_links import check_links
from .commands.compile_cms_content import compile_cms_content
from .commands.dump_openapi_specs import dump_openapi_specs
from .commands.dump_pvog_transfer_log import dump_pvog_transfer_log
from .commands.generate_admin_password import generate_admin_password
from .commands.import_datenfelder import run_datenfeld_import
from .commands.import_gebiet_id import run_gebiet_id_import
from .commands.import_kataloge import run_kataloge_import
from .commands.import_leistungen import run_leistung_import
from .commands.import_prozesse import run_prozesse_import
from .commands.migrate import migrate
from .commands.populate_local_xsd_cache import populate_local_xsd_cache
from .commands.reset_db_command import resetdb


@click.group()
@click.option("--log-config", type=click.Path(exists=True))
def cli(log_config: str | None):
    if log_config:
        with open(log_config, "rb") as file:
            content = orjson.loads(file.read())

        logging.config.dictConfig(content)
    else:
        logging.basicConfig(level=logging.INFO)
        logging.getLogger("httpx").setLevel(logging.WARNING)


@click.command()
def print_sql_schema():
    config = AppConfig.load_and_init_from_env()

    subprocess.run(
        [
            "pg_dump",
            "--format=plain",
            "--encoding=UTF8",
            "--schema-only",
            "--no-privileges",
            "--no-owner",
            f"postgresql://{config.database_url}",
        ],
    )


cli.add_command(resetdb)
cli.add_command(migrate)
cli.add_command(run_datenfeld_import)
cli.add_command(check_generated_data)
cli.add_command(generate_admin_password)
cli.add_command(dump_openapi_specs)
cli.add_command(check_links)
cli.add_command(print_sql_schema)
cli.add_command(populate_local_xsd_cache)
cli.add_command(run_leistung_import)
cli.add_command(run_prozesse_import)
cli.add_command(run_gebiet_id_import)
cli.add_command(run_kataloge_import)
cli.add_command(compile_cms_content)
cli.add_command(dump_pvog_transfer_log)
