"""
A crawler collecting all available xdf2 schemas including the corresponding
code lists from the available repositories.

See `/docs/xdf2-import.py` for further information.
"""

from __future__ import annotations

import json
import logging
from dataclasses import dataclass
from typing import Any, Optional

import httpx

from fimportal import xml
from fimportal.errors import ImportException
from fimportal.models.imports import Xdf2SchemaImport, Xdf2SteckbriefImport
from fimportal.xdatenfelder import xdf2, xdf3
from fimportal.xdatenfelder.common import XdfException

logger = logging.getLogger(__name__)


STECKBRIEF_LIST_INDEX = 3
SCHEMA_LIST_INDEX = 4


class CrawlerException(Exception):
    pass


@dataclass(slots=True)
class SchemaMetadata:
    """
    The Metadata of a schema, containing the information to actually
    import the schema and the corresponding code lists.

    See `/docs/xdf2-import.py` for further information.
    """

    schema_url: str
    freigabe_status: xdf3.FreigabeStatus
    steckbrief_id: str | None

    code_lists: dict[str, str]


@dataclass(slots=True)
class SteckbriefMetadata:
    """
    The Metadata of a steckbrief, containing the information to actually
    import the steckbrief

    See `/docs/xdf2-import.py` for further information.
    """

    steckbrief_url: str
    freigabe_status: xdf3.FreigabeStatus


async def load_schema_imports(client: httpx.AsyncClient, repository_url: str):
    """
    Load all imports from a Schema Repository.
    Returns an async generator, yielding one import at a time.
    """

    steckbrief_urls, schema_urls = await _load_urls(client, repository_url)

    # NOTE: Do not do this in parallel, as the server apparently cannot reliably handle
    # more than one request at a time.
    for url in steckbrief_urls:
        try:
            yield await import_steckbrief(client, url)
        except CrawlerException as error:
            logger.warning("Failed to crawl steckbrief [url=%s]: %s", url, str(error))
        except Exception:
            logger.exception("Failed to crawl steckbrief [url=%s]", url)

    for url in schema_urls:
        try:
            yield await import_schema(client, url)
        except CrawlerException as error:
            logger.warning("Failed to crawl schema [url=%s]: %s", url, str(error))
        except Exception:
            logger.exception("Failed to crawl schema [url=%s]", url)


async def _load_urls(
    client: httpx.AsyncClient, repository_url: str
) -> tuple[list[str], list[str]]:
    data = await _fetch(client, repository_url)

    try:
        root = xml.parse_html(data.decode("utf-8"))
    except Exception as error:
        # TODO: Make this more narrow instead of catching all exceptions
        raise CrawlerException(
            f"Could not parse content of {repository_url}: {data.decode('utf-8')}"
        ) from error

    steckbrief_urls = _extract_links(root, repository_url, STECKBRIEF_LIST_INDEX)
    schema_urls = _extract_links(root, repository_url, SCHEMA_LIST_INDEX)

    return steckbrief_urls, schema_urls


def _extract_links(
    root: xml.XmlElement,
    repository_url: str,
    index: int,
) -> list[str]:
    # There is not specific ID to get to a list.
    # Just navigate via the index of the <ul> element in the document.
    items: xml.XmlElement = root.findall(".//ul", namespaces=None)[index]

    # Return all links within the list.
    links: list[xml.XmlElement] = items.findall(".//a", namespaces=None)

    urls: list[str] = []
    for link in links:
        url: Optional[str] = link.get("href", None)
        if url is None:
            raise CrawlerException(f"Empty link in {repository_url}")

        urls.append(url)

    return urls


async def import_steckbrief(
    client: httpx.AsyncClient, url: str
) -> Xdf2SteckbriefImport:
    data = await _fetch_json(client, url)
    metadata = parse_steckbrief_metadata(data)

    xml_content = await _fetch(client, metadata.steckbrief_url)

    try:
        return Xdf2SteckbriefImport.from_bytes(
            xml_content,
            freigabe_status=metadata.freigabe_status,
        )
    except ImportException as error:
        raise CrawlerException(str(error)) from error


def parse_steckbrief_metadata(data: dict[str, Any]) -> SteckbriefMetadata:
    freigabe_status = xdf3.FreigabeStatus.from_label(
        _get_field_entry(data["characteristics"], "Status")
    )

    xml_links = [link for link in data["documentLinks"] if link["docFileType"] == "XML"]
    if len(xml_links) != 1:
        raise CrawlerException("Could not find the steckbrief url.")

    return SteckbriefMetadata(
        freigabe_status=freigabe_status,
        steckbrief_url=xml_links[0]["docUrl"],
    )


async def import_schema(client: httpx.AsyncClient, url: str) -> Xdf2SchemaImport:
    data = await _fetch_json(client, url)
    metadata = parse_schema_metadata(data)

    return await _load_schema_files(client, metadata)


def parse_schema_metadata(data: dict[str, Any]) -> SchemaMetadata:
    freigabe_status = xdf3.FreigabeStatus.from_label(
        _get_field_entry(data["registryOfInformation"], "Status")
    )

    steckbrief_id_str = _get_optional_field_entry(
        data["characteristics"], "ID des Dokumentsteckbriefs"
    )
    if steckbrief_id_str is None:
        steckbrief_id = None
    else:
        steckbrief_id = parse_steckbrief_id(steckbrief_id_str)

    schema_link, code_list_links = _parse_document_links(data["documentLinks"])

    return SchemaMetadata(
        schema_link,
        freigabe_status=freigabe_status,
        steckbrief_id=steckbrief_id,
        code_lists=code_list_links,
    )


def _parse_document_links(items: list[dict[str, str]]) -> tuple[str, dict[str, str]]:
    schema_link: Optional[str] = None
    code_list_links: dict[str, str] = {}

    for item in items:
        if item["docFileType"] != "XML":
            # Ignore other files like SXXXXXX_archiv.zip
            continue

        name = item["docName"]
        if name.startswith("S"):
            if "xdatenfelder" in name:
                assert schema_link is None
                schema_link = str(item["docUrl"])
            else:
                # Ignore SXXXXXX_xfall.xml
                pass
        elif name.startswith("C"):
            code_list_links[name] = item["docUrl"]
        else:
            # Ignore other xml files
            pass

    if schema_link is None:
        raise CrawlerException("Could not find the schema url.")

    return schema_link, code_list_links


async def _load_schema_files(
    client: httpx.AsyncClient, schema_data: SchemaMetadata
) -> Xdf2SchemaImport:
    data = await _fetch(client, schema_data.schema_url)

    code_list_data = [
        await _load_code_list(client, url) for url in schema_data.code_lists.values()
    ]

    try:
        return Xdf2SchemaImport.from_bytes(
            data,
            code_list_data=code_list_data,
            freigabe_status=schema_data.freigabe_status,
            steckbrief_id=schema_data.steckbrief_id,
        )
    except ImportException as error:
        raise CrawlerException(str(error)) from error


async def _load_code_list(client: httpx.AsyncClient, url: str) -> bytes:
    try:
        return await _fetch(client, url)
    except Exception as error:
        raise CrawlerException(f"Could not import code list [url={url}]") from error


def parse_steckbrief_id(value: str) -> str:
    if "V" in value:
        identifier = _parse_identifier(value)
        return identifier.id
    else:
        return value


def _parse_identifier(value: str) -> xdf2.Identifier:
    parts = value.split("V")
    if len(parts) != 2:
        raise CrawlerException(f"Invalid identifier {value}")

    try:
        version = xdf2.parse_version(parts[1])
    except XdfException as error:
        raise CrawlerException(f"Invalid identifier {value}") from error

    return xdf2.Identifier(parts[0], version)


def _get_field_entry(fields: list[dict[str, str]], key: str) -> str:
    result = _get_optional_field_entry(fields, key)

    if result is None:
        raise CrawlerException(f"Could not find item with fieldName {key}")

    return result


def _get_optional_field_entry(fields: list[dict[str, str]], key: str) -> str | None:
    for field in fields:
        if field["fieldName"] == key:
            return field["fieldValue"]

    return None


async def _fetch_json(client: httpx.AsyncClient, url: str) -> dict[str, Any]:
    data = await _fetch(client, url)

    try:
        return json.loads(data)
    except json.JSONDecodeError as error:
        raise CrawlerException(
            f"Could not parse content of {url}: {data.decode('utf-8')[:100]}"
        ) from error


async def _fetch(client: httpx.AsyncClient, url: str) -> bytes:
    try:
        response = await client.get(url)
        response.raise_for_status()

        return await response.aread()
    except httpx.HTTPError as error:
        raise CrawlerException(f"Request failed [url={url}]") from error
