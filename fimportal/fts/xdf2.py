from fimportal.xdatenfelder.xdf2 import (
    AllgemeineAngaben,
    Datenfeld,
    ElementReference,
    Gruppe,
    Regel,
    Schema,
    SchemaMessage,
)


def create_fts_doc(message: SchemaMessage) -> str:
    """
    Create a FTS document from a SchemaMessage.

    The document will only include each field/group/rule once instead of representing
    the full schema tree with potentially redundant information.
    The structure is completely flat, i.e. all attributes are simply divided by a `\n`.

    Also, information irrelevant for the FTS (like the numerical representation of the `FreigabeStatus`, which is part
    of the original xdf schema file, or timestamps) is omitted.
    """
    lines = []
    _append_schema(message.schema, lines)
    for group in message.groups.values():
        _append_group(group, lines)
    for field in message.fields.values():
        _append_field(field, lines)
    for rule in message.rules.values():
        _append_rule(rule, lines)

    return "\n".join(lines)


def _append_schema(schema: Schema, lines: list[str]):
    _append_allgemeine_angaben(schema, lines)
    _append_struktur(schema.struktur, lines)

    if schema.hilfetext:
        lines.append(schema.hilfetext)


def _append_group(group: Gruppe, lines: list[str]):
    _append_allgemeine_angaben(group, lines)
    _append_struktur(group.struktur, lines)


def _append_field(field: Datenfeld, lines: list[str]):
    _append_allgemeine_angaben(field, lines)

    if field.inhalt:
        lines.append(field.inhalt)
    if field.code_listen_referenz:
        lines.append(field.code_listen_referenz.identifier.id)
        if field.code_listen_referenz.identifier.version:
            lines.append(field.code_listen_referenz.identifier.version)

        genericode = field.code_listen_referenz.genericode_identifier
        lines.append(genericode.canonical_uri)
        if genericode.version:
            lines.append(genericode.version)
        if genericode.canonical_version_uri:
            lines.append(genericode.canonical_version_uri)


def _append_rule(rule: Regel, lines: list[str]):
    _append_allgemeine_angaben(rule, lines)


def _append_struktur(struktur: list[ElementReference], lines: list[str]):
    for ref in struktur:
        if ref.bezug:
            lines.append(ref.bezug)


def _append_allgemeine_angaben(data: AllgemeineAngaben, lines: list[str]):
    """
    Omitted fields:
    - status
    - gueltig_ab
    - gueltig_bis
    - freigabedatum
    - veroeffentlichungsdatum
    """

    lines.append(data.identifier.id)
    if data.identifier.version:
        lines.append(data.identifier.version)

    lines.append(data.name)
    if data.bezeichnung_eingabe:
        lines.append(data.bezeichnung_eingabe)
    if data.bezeichnung_ausgabe:
        lines.append(data.bezeichnung_ausgabe)
    if data.beschreibung:
        lines.append(data.beschreibung)
    if data.definition:
        lines.append(data.definition)
    if data.bezug:
        lines.append(data.bezug)
    if data.versionshinweis:
        lines.append(data.versionshinweis)
    if data.fachlicher_ersteller:
        lines.append(data.fachlicher_ersteller)
