from fimportal.xdatenfelder.xdf3.common import (
    AllgemeineAngaben,
    Datenfeld,
    Element,
    ElementReference,
    Gruppe,
    Rechtsbezug,
    Regel,
)
from fimportal.xdatenfelder.xdf3.schema_message import Schema, SchemaMessage


def create_fts_doc(message: SchemaMessage) -> str:
    """
    Create a FTS document from a SchemaMessage.

    The document will only include each field/group/rule once instead of representing
    the full schema tree with potentially redundant information.
    The structure is completely flat, i.e. all attributes are simply divided by a `\n`.

    Also, information irrelevant for the FTS (like the numerical representation of the `FreigabeStatus`, which is part
    of the original xdf schema file, or timestamps) is omitted.
    """
    lines = []
    _append_schema(message.schema, lines)
    for group in message.groups.values():
        _append_group(group, lines)
    for field in message.fields.values():
        _append_field(field, lines)
    for rule in message.rules.values():
        _append_rule(rule, lines)

    return "\n".join(lines)


def _append_schema(schema: Schema, lines: list[str]):
    _append_allgemeine_angaben(schema, lines)
    _append_struktur(schema.struktur, lines)

    lines.append(schema.bezeichnung)
    if schema.hilfetext:
        lines.append(schema.hilfetext)
    lines.append(schema.dokumentsteckbrief.id)


def _append_group(group: Gruppe, lines: list[str]):
    _append_element(group, lines)
    _append_struktur(group.struktur, lines)


def _append_field(field: Datenfeld, lines: list[str]):
    _append_element(field, lines)

    if field.inhalt:
        lines.append(field.inhalt)

    if field.werte:
        for wert in field.werte:
            lines.append(wert.code)
            lines.append(wert.name)
            if wert.hilfe:
                lines.append(wert.hilfe)

    if field.codeliste_referenz:
        genericode = field.codeliste_referenz
        lines.append(genericode.canonical_uri)
        if genericode.version:
            lines.append(genericode.version)
        if genericode.canonical_version_uri:
            lines.append(genericode.canonical_version_uri)


def _append_rule(rule: Regel, lines: list[str]):
    lines.append(rule.identifier.id)
    if rule.identifier.version:
        lines.append(rule.identifier.version)

    lines.append(rule.name)
    if rule.beschreibung:
        lines.append(rule.beschreibung)
    if rule.freitext_regel:
        lines.append(rule.freitext_regel)
    _append_bezug(rule.bezug, lines)
    if rule.fachlicher_ersteller:
        lines.append(rule.fachlicher_ersteller)
    for stichwort in rule.stichwort:
        lines.append(stichwort.uri)
        lines.append(stichwort.value)

    for param in rule.param:
        lines.append(param.name)
    for ziel in rule.ziel:
        lines.append(ziel.name)
    for fehler in rule.fehler:
        lines.append(fehler.message)


def _append_struktur(struktur: list[ElementReference], lines: list[str]):
    for ref in struktur:
        _append_bezug(ref.bezug, lines)


def _append_element(data: Element, lines: list[str]):
    _append_allgemeine_angaben(data, lines)

    lines.append(data.bezeichnung_eingabe)
    if data.bezeichnung_ausgabe:
        lines.append(data.bezeichnung_ausgabe)
    if data.hilfetext_eingabe:
        lines.append(data.hilfetext_eingabe)
    if data.hilfetext_ausgabe:
        lines.append(data.hilfetext_ausgabe)


def _append_allgemeine_angaben(data: AllgemeineAngaben, lines: list[str]):
    """
    Omitted fields:
    - freigabe_status
    - status_gesetzt_am
    - gueltig_ab
    - gueltig_bis
    - veroeffentlichungsdatum
    - letzte_aenderung
    - relation
    """

    lines.append(data.identifier.id)
    if data.identifier.version:
        lines.append(data.identifier.version)

    lines.append(data.name)
    if data.beschreibung:
        lines.append(data.beschreibung)
    if data.definition:
        lines.append(data.definition)
    _append_bezug(data.bezug, lines)
    if data.status_gesetzt_durch:
        lines.append(data.status_gesetzt_durch)
    if data.versionshinweis:
        lines.append(data.versionshinweis)
    for stichwort in data.stichwort:
        lines.append(stichwort.uri)
        lines.append(stichwort.value)


def _append_bezug(bezug: list[Rechtsbezug], lines: list[str]):
    for item in bezug:
        lines.append(item.text)
        if item.link:
            lines.append(item.link)
