from __future__ import annotations

from dataclasses import dataclass
from datetime import datetime
from typing import Any, ClassVar, Literal

from pydantic import BaseModel

from fimportal.common import Anwendungsgebiet, Bundesland, FreigabeStatus
from fimportal.xml import remove_tag
from fimportal.xprozesse.prozess import (
    XPROZESS_NAMESPACE,
    Detaillierungsstufe,
    DokumentsteckbriefRolle,
    Handlungsform,
    OperativesZiel,
    Prozess,
    Prozessklasse,
    Verfahrensart,
)

XPROZESS_INHALT = f"{{{XPROZESS_NAMESPACE}}}inhalt"


@dataclass(slots=True)
class NewProzessklasse:
    id: str
    name: str
    version: str | None
    letzter_aenderungszeitpunkt: datetime | None
    operatives_ziel: OperativesZiel | None
    verfahrensart: Verfahrensart | None
    handlungsform: Handlungsform | None
    freigabe_status: FreigabeStatus | None

    xml_content: str

    @staticmethod
    def from_prozessklasse(
        prozessklasse: Prozessklasse, xml_content: str
    ) -> "NewProzessklasse":
        assert len(prozessklasse.klassifikation) == 3  # Expected for Frontend
        return NewProzessklasse(
            id=prozessklasse.id,
            name=prozessklasse.name,
            version=prozessklasse.version,
            letzter_aenderungszeitpunkt=prozessklasse.letzter_aenderungszeitpunkt,
            freigabe_status=prozessklasse.freigabe_status,
            operatives_ziel=prozessklasse.operatives_ziel,
            verfahrensart=prozessklasse.verfahrensart,
            handlungsform=prozessklasse.handlungsform,
            xml_content=xml_content,
        )


class ProzessklasseOut(BaseModel):
    id: str
    version: str | None
    name: str
    letzter_aenderungszeitpunkt: datetime | None
    freigabe_status: FreigabeStatus | None

    fts_match: str | None

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "id",
            "version",
            "name",
            "letzter_aenderungszeitpunkt",
            "freigabe_status",
        ]
    )

    @staticmethod
    def from_row(
        row: tuple[Any, ...], fts_match: str | None = None
    ) -> "ProzessklasseOut":
        return ProzessklasseOut(
            id=row[0],
            version=row[1],
            name=row[2],
            letzter_aenderungszeitpunkt=row[3],
            freigabe_status=FreigabeStatus(row[4]) if row[4] is not None else None,
            fts_match=fts_match,
        )


class FullProzessklasseOut(BaseModel):
    id: str
    version: str | None
    name: str
    letzter_aenderungszeitpunkt: datetime | None
    freigabe_status: FreigabeStatus | None

    leistungssteckbrief: LeistungssteckbriefReferenz | None
    prozesse: list[ProzessReferenz]

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "id",
            "version",
            "name",
            "letzter_aenderungszeitpunkt",
            "freigabe_status",
            """
                (
                    SELECT l.title
                    FROM leistung l
                    WHERE l.root_for_leistungsschluessel = prozessklasse.id
                ) as leistungssteckbrief_name
            """,
            """
                array(
                    SELECT ROW(id, name)
                    FROM prozess
                    WHERE prozessklasse.id = prozess.prozessklasse
                )
            """,
        ]
    )

    @staticmethod
    def from_row(row: tuple[Any, ...]) -> FullProzessklasseOut:
        prozess_klasse_id = row[0]

        return FullProzessklasseOut(
            id=prozess_klasse_id,
            version=row[1],
            name=row[2],
            letzter_aenderungszeitpunkt=row[3],
            freigabe_status=FreigabeStatus(row[4]) if row[4] is not None else None,
            leistungssteckbrief=LeistungssteckbriefReferenz(
                leistungsschluessel=prozess_klasse_id,
                name=row[5],
            ),
            prozesse=[ProzessReferenz.from_row(r) for r in row[6]],
        )


@dataclass(slots=True)
class Lokalisation:
    value: Bundesland | Literal["Bund"]

    def to_label(self) -> str:
        if self.value == "Bund":
            return "Bund"
        return self.value.to_label()

    @staticmethod
    def from_label(value: str) -> "Lokalisation":
        if value == "Bund":
            return Lokalisation(value="Bund")
        return Lokalisation(value=Bundesland.from_label(value))


@dataclass(slots=True)
class NewProzess:
    id: str
    version: str | None
    name: str
    status: FreigabeStatus | None
    detaillierungsstufe: Detaillierungsstufe | None
    prozessklasse: str
    anwendungsgebiet: list[Anwendungsgebiet]
    letzter_aenderungszeitpunkt: datetime | None

    report_file: bytes | None
    visualization_file: bytes | None
    xml_content: str
    xml_content_without_files: str

    dokumentsteckbrief_references: list[tuple[str, DokumentsteckbriefRolle]]

    @staticmethod
    def from_prozess(prozess: Prozess, xml_content: str) -> "NewProzess":
        assert len(prozess.klassifikation) == 1
        visualisierungsdatei = prozess.get_visualisierungsdatei()
        anwendungsgebiet = [
            item.to_anwendungsgebiet()
            for item in prozess.get_verwaltungspolitische_kodierung()
        ]
        return NewProzess(
            id=prozess.id,
            version=prozess.version,
            name=prozess.name,
            status=prozess.freigabe_status,
            detaillierungsstufe=prozess.detaillierungsstufe,
            prozessklasse=prozess.klassifikation[0].klasse_id,
            anwendungsgebiet=[Anwendungsgebiet.BUND]
            if len(anwendungsgebiet) == 16
            else anwendungsgebiet,
            letzter_aenderungszeitpunkt=prozess.letzter_aenderungszeitpunkt,
            report_file=prozess.get_report_file(),
            visualization_file=visualisierungsdatei[1]
            if visualisierungsdatei is not None
            else None,
            xml_content=xml_content,
            xml_content_without_files=remove_tag(xml_content, XPROZESS_INHALT),
            dokumentsteckbrief_references=prozess.get_dokumentsteckbrief_references(),
        )


class ProzessOut(BaseModel):
    id: str
    version: str | None
    name: str
    letzter_aenderungszeitpunkt: datetime | None
    freigabe_status: FreigabeStatus | None

    fts_match: str | None

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "id",
            "version",
            "name",
            "letzter_aenderungszeitpunkt",
            "freigabe_status",
        ]
    )

    @staticmethod
    def from_row(row: tuple[Any, ...], fts_match: str | None = None) -> "ProzessOut":
        return ProzessOut(
            id=row[0],
            version=row[1],
            name=row[2],
            letzter_aenderungszeitpunkt=row[3],
            freigabe_status=FreigabeStatus(row[4]) if row[4] is not None else None,
            fts_match=fts_match,
        )


class DokumentsteckbriefReferenz(BaseModel):
    fim_id: str
    rolle: DokumentsteckbriefRolle
    name: str | None

    @staticmethod
    def from_row(row: tuple[Any, ...]) -> "DokumentsteckbriefReferenz":
        return DokumentsteckbriefReferenz(
            fim_id=row[0],
            rolle=DokumentsteckbriefRolle(row[1]),
            name=row[2],
        )


class LeistungssteckbriefReferenz(BaseModel):
    leistungsschluessel: str
    name: str | None

    @staticmethod
    def from_row(
        title: str | None, leistungsschluessel: str
    ) -> LeistungssteckbriefReferenz:
        return LeistungssteckbriefReferenz(
            leistungsschluessel=leistungsschluessel,
            name=title,
        )


class ProzessReferenz(BaseModel):
    id: str
    name: str

    @staticmethod
    def create(id: str, name: str):
        return ProzessReferenz(id=id, name=name)

    @staticmethod
    def from_row(row: tuple[Any, ...]):
        return ProzessReferenz(id=row[0], name=row[1])


class FullProzessOut(BaseModel):
    id: str
    version: str | None
    name: str
    letzter_aenderungszeitpunkt: datetime | None
    freigabe_status: FreigabeStatus | None

    prozessklasse: ProzessReferenz | None
    dokumentsteckbriefe: dict[str, DokumentsteckbriefReferenz]
    leistungssteckbrief: LeistungssteckbriefReferenz | None

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "id",
            "version",
            "name",
            "letzter_aenderungszeitpunkt",
            "freigabe_status",
            "prozessklasse",
            """
                (
                    SELECT pk.name
                    FROM prozessklasse pk
                    WHERE prozess.prozessklasse = pk.id
                ) as prozessklasse_name
            """,
            """
                array(
                    SELECT ROW(l.dokumentsteckbrief_fim_id, l.role, r.name)
                    FROM prozess_dokumentsteckbrief_relation l
                    LEFT OUTER JOIN dokumentensteckbrief r
                    ON l.dokumentsteckbrief_fim_id = r.fim_id
                    AND r.is_latest = true
                    WHERE l.prozess_id = prozess.id
                    ORDER BY l.dokumentsteckbrief_fim_id
                ) as dokumentsteckbriefe
            """,
            """
                (
                    SELECT l.title
                    FROM leistung l
                    WHERE l.root_for_leistungsschluessel = prozess.id
                ) as leistungssteckbrief_name
            """,
        ]
    )

    @staticmethod
    def from_row(row: tuple[Any, ...]) -> "FullProzessOut":
        prozess_id = row[0]

        return FullProzessOut(
            id=prozess_id,
            version=row[1],
            name=row[2],
            letzter_aenderungszeitpunkt=row[3],
            freigabe_status=FreigabeStatus(row[4]) if row[4] is not None else None,
            prozessklasse=ProzessReferenz.create(row[5], row[6])
            if row[5] is not None and row[6] is not None
            else None,
            dokumentsteckbriefe={
                r[0]: DokumentsteckbriefReferenz.from_row(r) for r in row[7]
            },
            leistungssteckbrief=LeistungssteckbriefReferenz(
                leistungsschluessel=prozess_id,
                name=row[8],
            ),
        )
