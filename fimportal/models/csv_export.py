from __future__ import annotations

from dataclasses import dataclass
from datetime import datetime
from typing import Any, ClassVar, Literal

from fimportal.common import FreigabeStatus


@dataclass(slots=True)
class CSVSearchResult:
    id: str
    baustein_typ: Literal["L", "D", "P"]
    name: str
    typ: str
    status_katalog: str
    status_bibliothek: str
    geaendert_am: str
    url_zur_detailansicht: str

    CSV_HEADERS: ClassVar[list[str]] = [
        "Schlüssel / ID",
        "Baustein Typ",
        "Name",
        "Typ",
        "Status Katalog",
        "Status Bibliothek",
        "geändert am",
        "URL zur Detailansicht",
    ]

    def to_csv_row(self, baseurl: str) -> list[str]:
        return [
            self.id,
            self.baustein_typ,
            self.name,
            self.typ,
            self.status_katalog,
            self.status_bibliothek,
            self.geaendert_am,
            f"{baseurl}{self.url_zur_detailansicht}",
        ]

    LEISTUNGSSTECKBRIEF_SQL_COLUMNS: ClassVar[str] = ",".join(
        [
            "root_for_leistungsschluessel",
            "title",
            "typisierung",
            "freigabestatus_katalog",
            "freigabestatus_bibliothek",
            "geaendert_datum_zeit",
        ]
    )

    @staticmethod
    def from_leistungssteckbrief(row: tuple[Any, ...]) -> CSVSearchResult:
        leistungsschluessel = row[0]
        letzte_aederung: datetime | None = row[5]

        return CSVSearchResult(
            id=leistungsschluessel,
            baustein_typ="L",
            name=row[1],
            typ=",".join(row[2]),
            status_katalog=FreigabeStatus(row[3]).to_label() if row[3] else "-",
            status_bibliothek=FreigabeStatus(row[4]).to_label() if row[4] else "-",
            geaendert_am=letzte_aederung.isoformat() if letzte_aederung else "-",
            url_zur_detailansicht=f"/services/{leistungsschluessel}",
        )

    LEISTUNGSBESCHREIBUNG_SQL_COLUMNS: ClassVar[str] = ",".join(
        [
            "redaktion_id",
            "id",
            "title",
            "typisierung",
            "freigabestatus_katalog",
            "freigabestatus_bibliothek",
            "geaendert_datum_zeit",
        ]
    )

    @staticmethod
    def from_leistungsbeschreibung(row: tuple[Any, ...]) -> CSVSearchResult:
        redaktion_id = row[0]
        leistung_id = row[1]
        letzte_aederung: datetime | None = row[6]

        return CSVSearchResult(
            id=f"{redaktion_id}/{leistung_id}",
            baustein_typ="L",
            name=row[2],
            typ=",".join(row[3]) if row[3] else "-",
            status_katalog=FreigabeStatus(row[4]).to_label() if row[4] else "-",
            status_bibliothek=FreigabeStatus(row[5]).to_label() if row[5] else "-",
            geaendert_am=letzte_aederung.isoformat() if letzte_aederung else "-",
            url_zur_detailansicht=f"/xzufi-services/{redaktion_id}/{leistung_id}",
        )

    DATENFELDER_SQL_COLUMNS: ClassVar[str] = ",".join(
        [
            "fim_id",
            "fim_version",
            "name",
            "freigabe_status",
            "letzte_aenderung",
        ]
    )

    @staticmethod
    def from_dokumentsteckbrief(row: tuple[Any, ...]) -> CSVSearchResult:
        fim_id = row[0]
        fim_version = row[1]
        letzte_aederung: datetime = row[4]

        return CSVSearchResult(
            id=f"{fim_id}V{fim_version}",
            baustein_typ="D",
            name=row[2],
            typ="-",
            status_katalog="-",
            status_bibliothek=FreigabeStatus(row[3]).to_label(),
            geaendert_am=letzte_aederung.isoformat(),
            url_zur_detailansicht=f"/document-profiles/{fim_id}/{fim_version}",
        )

    @staticmethod
    def from_schema(row: tuple[Any, ...]) -> CSVSearchResult:
        fim_id = row[0]
        fim_version = row[1]
        letzte_aederung: datetime = row[4]

        return CSVSearchResult(
            id=f"{fim_id}V{fim_version}",
            baustein_typ="D",
            name=row[2],
            typ="-",
            status_katalog="-",
            status_bibliothek=FreigabeStatus(row[3]).to_label(),
            geaendert_am=letzte_aederung.isoformat(),
            url_zur_detailansicht=f"/schemas/{fim_id}/{fim_version}",
        )

    @staticmethod
    def from_field(row: tuple[Any, ...]) -> CSVSearchResult:
        fim_id = row[0]
        fim_version = row[1]
        letzte_aederung: datetime = row[4]

        return CSVSearchResult(
            id=f"{fim_id}V{fim_version}",
            baustein_typ="D",
            name=row[2],
            typ="-",
            status_katalog="-",
            status_bibliothek=FreigabeStatus(row[3]).to_label(),
            geaendert_am=letzte_aederung.isoformat(),
            url_zur_detailansicht=f"/fields/baukasten/{fim_id}/{fim_version}",
        )

    @staticmethod
    def from_group(row: tuple[Any, ...]) -> CSVSearchResult:
        fim_id = row[0]
        fim_version = row[1]
        letzte_aederung: datetime = row[4]

        return CSVSearchResult(
            id=f"{fim_id}V{fim_version}",
            baustein_typ="D",
            name=row[2],
            typ="-",
            status_katalog="-",
            status_bibliothek=FreigabeStatus(row[3]).to_label(),
            geaendert_am=letzte_aederung.isoformat(),
            url_zur_detailansicht=f"/groups/baukasten/{fim_id}/{fim_version}",
        )

    PROZESS_SQL_COLUMNS: ClassVar[str] = ",".join(
        [
            "id",
            "name",
            "freigabe_status",
            "letzter_aenderungszeitpunkt",
        ]
    )

    @staticmethod
    def from_prozessklasse(row: tuple[Any, ...]) -> CSVSearchResult:
        id = row[0]
        letzter_aenderungszeitpunkt: datetime | None = row[3]

        return CSVSearchResult(
            id=id,
            baustein_typ="P",
            name=row[1],
            typ="-",
            status_katalog=FreigabeStatus(row[2]).to_label() if row[2] else "-",
            status_bibliothek="-",
            geaendert_am=letzter_aenderungszeitpunkt.isoformat()
            if letzter_aenderungszeitpunkt
            else "-",
            url_zur_detailansicht=f"/processclasses/{id}",
        )

    @staticmethod
    def from_prozess(row: tuple[Any, ...]) -> CSVSearchResult:
        id = row[0]
        letzter_aenderungszeitpunkt: datetime | None = row[3]

        return CSVSearchResult(
            id=id,
            baustein_typ="P",
            name=row[1],
            typ="-",
            status_katalog="-",
            status_bibliothek=FreigabeStatus(row[2]).to_label() if row[2] else "-",
            geaendert_am=letzter_aenderungszeitpunkt.isoformat()
            if letzter_aenderungszeitpunkt
            else "-",
            url_zur_detailansicht=f"/processes/{id}",
        )
