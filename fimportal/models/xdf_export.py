from __future__ import annotations

from dataclasses import dataclass
from datetime import date, datetime
from typing import Any, ClassVar

from fimportal.din91379 import parse_optional_string_latin
from fimportal.helpers import utc_now
from fimportal.xdatenfelder import xdf2, xdf3, xdf3_to_xdf2

from .imports import XdfVersion


@dataclass(slots=True)
class SchemaMessageExport:
    """
    All of the data necessary to reconstruct a schema message.
    """

    schema: SchemaExportData
    groups: list[DatenfeldgruppeExportData]
    fields: list[DatenfeldExportData]
    rules: list[RegelExportData]

    def to_message(self) -> xdf2.SchemaMessage | xdf3.SchemaMessage:
        if self.schema.xdf_version == XdfVersion.XDF2:
            return self._to_xdf2_message()
        else:
            return self._to_xdf3_message()

    def _to_xdf2_message(self) -> xdf2.SchemaMessage:
        schema = self.schema.to_xdf2()
        groups = [group.to_xdf2() for group in self.groups]
        fields = [field.to_xdf2() for field in self.fields]
        rules = [rule.to_xdf2() for rule in self.rules]

        return xdf2.SchemaMessage(
            header=xdf2.MessageHeader(
                nachricht_id="fimportal-export",
                erstellungs_zeitpunkt=utc_now(),
                referenz_id=None,
            ),
            schema=schema,
            groups={group.identifier: group for group in groups},
            fields={field.identifier: field for field in fields},
            rules={rule.identifier: rule for rule in rules},
        )

    def _to_xdf3_message(self) -> xdf3.SchemaMessage:
        schema = self.schema.to_xdf3()
        groups = [group.to_xdf3() for group in self.groups]
        fields = [field.to_xdf3() for field in self.fields]
        rules = [rule.to_xdf3() for rule in self.rules]

        return xdf3.SchemaMessage(
            header=xdf3.MessageHeader(
                nachricht_id="fimportal-export",
                erstellungs_zeitpunkt=utc_now(),
            ),
            schema=schema,
            groups={group.identifier: group for group in groups},
            fields={field.identifier: field for field in fields},
            rules={rule.identifier: rule for rule in rules},
        )


@dataclass(slots=True)
class SchemaExportData:
    xdf_version: XdfVersion
    freigabe_status: xdf3.FreigabeStatus
    status_gesetzt_am: date | None
    veroeffentlichungsdatum: date | None
    letzte_aenderung: datetime

    immutable_json: str
    steckbrief_id: str | None
    relation: list[xdf3.Relation]

    SQL_COLUMNS: ClassVar[str] = ",".join(
        [
            "xdf_version",
            "freigabe_status",
            "status_gesetzt_am",
            "veroeffentlichungsdatum",
            "letzte_aenderung",
            "immutable_json",
            "steckbrief_id",
            """
            array(
                SELECT ROW(target_fim_id, target_fim_version, praedikat)
                FROM schema_relations
                WHERE source_fim_id = schema.fim_id
                AND source_fim_version = schema.fim_version
                ORDER BY target_fim_id, target_fim_version, praedikat
            )
            """,
        ]
    )

    @staticmethod
    def from_row(row: tuple[Any, ...]) -> SchemaExportData:
        return SchemaExportData(
            xdf_version=XdfVersion(row[0]),
            freigabe_status=xdf3.FreigabeStatus(row[1]),
            status_gesetzt_am=row[2],
            veroeffentlichungsdatum=row[3],
            letzte_aenderung=row[4],
            immutable_json=row[5],
            steckbrief_id=row[6],
            relation=[_parse_relation(relation) for relation in row[7]],
        )

    def to_xdf2(self) -> xdf2.Schema:
        assert self.xdf_version == XdfVersion.XDF2

        immutable = xdf2.ImmutableSchema.model_validate_json(self.immutable_json)

        return xdf2.Schema(
            identifier=immutable.identifier,
            name=immutable.name,
            bezeichnung_eingabe=immutable.bezeichnung_eingabe,
            bezeichnung_ausgabe=immutable.bezeichnung_ausgabe,
            beschreibung=immutable.beschreibung,
            definition=immutable.definition,
            bezug=immutable.bezug,
            status=xdf3_to_xdf2.map_status(self.freigabe_status),
            versionshinweis=immutable.versionshinweis,
            gueltig_ab=immutable.gueltig_ab,
            gueltig_bis=immutable.gueltig_bis,
            fachlicher_ersteller=immutable.fachlicher_ersteller,
            freigabedatum=self.status_gesetzt_am,
            veroeffentlichungsdatum=self.veroeffentlichungsdatum,
            hilfetext=immutable.hilfetext,
            ableitungsmodifikationen_struktur=immutable.ableitungsmodifikationen_struktur,
            ableitungsmodifikationen_repraesentation=immutable.ableitungsmodifikationen_repraesentation,
            regeln=immutable.regeln,
            struktur=[
                struktur.to_element_reference() for struktur in immutable.struktur
            ],
        )

    def to_xdf3(self) -> xdf3.Schema:
        assert self.xdf_version == XdfVersion.XDF3
        assert self.steckbrief_id is not None

        immutable = xdf3.ImmutableSchema.model_validate_json(self.immutable_json)

        return xdf3.Schema(
            identifier=immutable.identifier,
            name=immutable.name,
            dokumentsteckbrief=xdf3.Identifier(self.steckbrief_id, None),
            bezeichnung=immutable.bezeichnung,
            beschreibung=immutable.beschreibung,
            definition=immutable.definition,
            bezug=immutable.bezug,
            freigabe_status=self.freigabe_status,
            versionshinweis=immutable.versionshinweis,
            gueltig_ab=immutable.gueltig_ab,
            gueltig_bis=immutable.gueltig_bis,
            status_gesetzt_durch=parse_optional_string_latin(
                immutable.status_gesetzt_durch
            ),
            status_gesetzt_am=self.status_gesetzt_am,
            veroeffentlichungsdatum=self.veroeffentlichungsdatum,
            letzte_aenderung=self.letzte_aenderung,
            stichwort=immutable.stichwort,
            relation=self.relation,
            hilfetext=immutable.hilfetext,
            ableitungsmodifikationen_struktur=immutable.ableitungsmodifikationen_struktur,
            ableitungsmodifikationen_repraesentation=immutable.ableitungsmodifikationen_repraesentation,
            regeln=immutable.regeln,
            struktur=[
                struktur.to_element_reference() for struktur in immutable.struktur
            ],
        )


@dataclass(slots=True)
class DatenfeldgruppeMessageExport:
    """
    All of the data necessary to reconstruct a datenfeldgruppe message.
    """

    group: DatenfeldgruppeExportData
    groups: list[DatenfeldgruppeExportData]
    fields: list[DatenfeldExportData]
    rules: list[RegelExportData]

    def to_message(self) -> xdf2.DatenfeldgruppeMessage | xdf3.DatenfeldgruppeMessage:
        if self.group.xdf_version == XdfVersion.XDF2:
            return self._to_xdf2_message()
        else:
            return self._to_xdf3_message()

    def _to_xdf2_message(self) -> xdf2.DatenfeldgruppeMessage:
        group = self.group.to_xdf2()
        groups = [group.to_xdf2() for group in self.groups]
        fields = [field.to_xdf2() for field in self.fields]
        rules = [rule.to_xdf2() for rule in self.rules]

        message = xdf2.DatenfeldgruppeMessage(
            header=xdf2.MessageHeader(
                nachricht_id="fimportal-export",
                erstellungs_zeitpunkt=utc_now(),
                referenz_id=None,
            ),
            group=group,
            groups={group.identifier: group for group in groups},
            fields={field.identifier: field for field in fields},
            rules={rule.identifier: rule for rule in rules},
        )
        if group.identifier in message.groups:
            del message.groups[group.identifier]

        return message

    def _to_xdf3_message(self) -> xdf3.DatenfeldgruppeMessage:
        group = self.group.to_xdf3()
        groups = [group.to_xdf3() for group in self.groups]
        fields = [field.to_xdf3() for field in self.fields]
        rules = [rule.to_xdf3() for rule in self.rules]

        message = xdf3.DatenfeldgruppeMessage(
            header=xdf3.MessageHeader(
                nachricht_id="fimportal-export",
                erstellungs_zeitpunkt=utc_now(),
            ),
            group=group,
            groups={group.identifier: group for group in groups},
            fields={field.identifier: field for field in fields},
            rules={rule.identifier: rule for rule in rules},
        )
        if group.identifier in message.groups:
            del message.groups[group.identifier]

        return message


@dataclass(slots=True)
class DatenfeldgruppeExportData:
    xdf_version: XdfVersion
    freigabe_status: xdf3.FreigabeStatus
    status_gesetzt_am: date | None
    veroeffentlichungsdatum: date | None
    letzte_aenderung: datetime
    relation: list[xdf3.Relation]

    immutable_json: str

    SQL_COLUMNS: ClassVar[str] = ",".join(
        [
            "xdf_version",
            "freigabe_status",
            "status_gesetzt_am",
            "veroeffentlichungsdatum",
            "letzte_aenderung",
            """
            array(
                SELECT ROW(target_fim_id, target_fim_version, praedikat)
                FROM datenfeldgruppe_relations
                WHERE source_namespace = datenfeldgruppe.namespace
                AND source_fim_id = datenfeldgruppe.fim_id
                AND source_fim_version = datenfeldgruppe.fim_version
                ORDER BY target_fim_id, target_fim_version, praedikat
            )
            """,
            "immutable_json",
        ]
    )

    @staticmethod
    def from_row(row: tuple[Any, ...]) -> DatenfeldgruppeExportData:
        return DatenfeldgruppeExportData(
            xdf_version=XdfVersion(row[0]),
            freigabe_status=xdf3.FreigabeStatus(row[1]),
            status_gesetzt_am=row[2],
            veroeffentlichungsdatum=row[3],
            letzte_aenderung=row[4],
            relation=[_parse_relation(relation) for relation in row[5]],
            immutable_json=row[6],
        )

    def to_xdf2(self) -> xdf2.Gruppe:
        assert self.xdf_version == XdfVersion.XDF2

        immutable = xdf2.ImmutableGruppe.model_validate_json(self.immutable_json)

        return xdf2.Gruppe(
            identifier=immutable.identifier,
            name=immutable.name,
            bezeichnung_eingabe=immutable.bezeichnung_eingabe,
            bezeichnung_ausgabe=immutable.bezeichnung_ausgabe,
            beschreibung=immutable.beschreibung,
            definition=immutable.definition,
            bezug=immutable.bezug,
            status=xdf3_to_xdf2.map_status(self.freigabe_status),
            versionshinweis=immutable.versionshinweis,
            gueltig_ab=immutable.gueltig_ab,
            gueltig_bis=immutable.gueltig_bis,
            fachlicher_ersteller=immutable.fachlicher_ersteller,
            freigabedatum=self.status_gesetzt_am,
            veroeffentlichungsdatum=self.veroeffentlichungsdatum,
            hilfetext_eingabe=immutable.hilfetext_eingabe,
            hilfetext_ausgabe=immutable.hilfetext_ausgabe,
            schema_element_art=immutable.schema_element_art,
            regeln=immutable.regeln,
            struktur=[
                struktur.to_element_reference() for struktur in immutable.struktur
            ],
        )

    def to_xdf3(self) -> xdf3.Gruppe:
        assert self.xdf_version == XdfVersion.XDF3

        immutable = xdf3.ImmutableGruppe.model_validate_json(self.immutable_json)

        return xdf3.Gruppe(
            identifier=immutable.identifier,
            name=immutable.name,
            beschreibung=immutable.beschreibung,
            definition=immutable.definition,
            bezug=immutable.bezug,
            freigabe_status=self.freigabe_status,
            versionshinweis=immutable.versionshinweis,
            gueltig_ab=immutable.gueltig_ab,
            gueltig_bis=immutable.gueltig_bis,
            status_gesetzt_durch=parse_optional_string_latin(
                immutable.status_gesetzt_durch
            ),
            status_gesetzt_am=self.status_gesetzt_am,
            veroeffentlichungsdatum=self.veroeffentlichungsdatum,
            letzte_aenderung=self.letzte_aenderung,
            stichwort=immutable.stichwort,
            relation=self.relation,
            schema_element_art=immutable.schema_element_art,
            bezeichnung_eingabe=immutable.bezeichnung_eingabe,
            bezeichnung_ausgabe=immutable.bezeichnung_ausgabe,
            hilfetext_eingabe=immutable.hilfetext_eingabe,
            hilfetext_ausgabe=immutable.hilfetext_ausgabe,
            art=immutable.art,
            regeln=immutable.regeln,
            struktur=[
                struktur.to_element_reference() for struktur in immutable.struktur
            ],
        )


@dataclass(slots=True)
class DatenfeldMessageExport:
    """
    All of the data necessary to reconstruct a datenfeld message.
    """

    field: DatenfeldExportData
    rules: list[RegelExportData]

    def to_message(self) -> xdf2.DatenfeldMessage | xdf3.DatenfeldMessage:
        if self.field.xdf_version == XdfVersion.XDF2:
            return self._to_xdf2_message()
        else:
            return self._to_xdf3_message()

    def _to_xdf2_message(self) -> xdf2.DatenfeldMessage:
        field = self.field.to_xdf2()
        rules = [rule.to_xdf2() for rule in self.rules]

        return xdf2.DatenfeldMessage(
            header=xdf2.MessageHeader(
                nachricht_id="fimportal-export",
                erstellungs_zeitpunkt=utc_now(),
                referenz_id=None,
            ),
            field=field,
            rules={rule.identifier: rule for rule in rules},
        )

    def _to_xdf3_message(self) -> xdf3.DatenfeldMessage:
        field = self.field.to_xdf3()
        rules = [rule.to_xdf3() for rule in self.rules]

        return xdf3.DatenfeldMessage(
            header=xdf3.MessageHeader(
                nachricht_id="fimportal-export",
                erstellungs_zeitpunkt=utc_now(),
            ),
            field=field,
            rules={rule.identifier: rule for rule in rules},
        )


@dataclass(slots=True)
class DatenfeldExportData:
    xdf_version: XdfVersion
    freigabe_status: xdf3.FreigabeStatus
    status_gesetzt_am: date | None
    veroeffentlichungsdatum: date | None
    letzte_aenderung: datetime
    relation: list[xdf3.Relation]

    immutable_json: str

    SQL_COLUMNS: ClassVar[str] = ",".join(
        [
            "xdf_version",
            "freigabe_status",
            "status_gesetzt_am",
            "veroeffentlichungsdatum",
            "letzte_aenderung",
            """
            array(
                SELECT ROW(target_fim_id, target_fim_version, praedikat)
                FROM datenfeld_relations
                WHERE source_namespace = datenfeld.namespace
                AND source_fim_id = datenfeld.fim_id
                AND source_fim_version = datenfeld.fim_version
                ORDER BY target_fim_id, target_fim_version, praedikat
            )
            """,
            "immutable_json",
        ]
    )

    @staticmethod
    def from_row(row: tuple[Any, ...]) -> DatenfeldExportData:
        return DatenfeldExportData(
            xdf_version=XdfVersion(row[0]),
            freigabe_status=xdf3.FreigabeStatus(row[1]),
            status_gesetzt_am=row[2],
            veroeffentlichungsdatum=row[3],
            letzte_aenderung=row[4],
            relation=[_parse_relation(relation) for relation in row[5]],
            immutable_json=row[6],
        )

    def to_xdf2(self) -> xdf2.Datenfeld:
        assert self.xdf_version == XdfVersion.XDF2

        immutable = xdf2.ImmutableFeld.model_validate_json(self.immutable_json)

        return xdf2.Datenfeld(
            identifier=immutable.identifier,
            name=immutable.name,
            bezeichnung_eingabe=immutable.bezeichnung_eingabe,
            bezeichnung_ausgabe=immutable.bezeichnung_ausgabe,
            beschreibung=immutable.beschreibung,
            definition=immutable.definition,
            bezug=immutable.bezug,
            status=xdf3_to_xdf2.map_status(self.freigabe_status),
            versionshinweis=immutable.versionshinweis,
            gueltig_ab=immutable.gueltig_ab,
            gueltig_bis=immutable.gueltig_bis,
            fachlicher_ersteller=immutable.fachlicher_ersteller,
            freigabedatum=self.status_gesetzt_am,
            veroeffentlichungsdatum=self.veroeffentlichungsdatum,
            hilfetext_eingabe=immutable.hilfetext_eingabe,
            hilfetext_ausgabe=immutable.hilfetext_ausgabe,
            schema_element_art=immutable.schema_element_art,
            regeln=immutable.regeln,
            feldart=immutable.feldart,
            datentyp=immutable.datentyp,
            praezisierung=immutable.praezisierung,
            inhalt=immutable.inhalt,
            code_listen_referenz=immutable.code_listen_referenz,
        )

    def to_xdf3(self) -> xdf3.Datenfeld:
        assert self.xdf_version == XdfVersion.XDF3

        immutable = xdf3.ImmutableFeld.model_validate_json(self.immutable_json)

        return xdf3.Datenfeld(
            identifier=immutable.identifier,
            name=immutable.name,
            beschreibung=immutable.beschreibung,
            definition=immutable.definition,
            bezug=immutable.bezug,
            freigabe_status=self.freigabe_status,
            versionshinweis=immutable.versionshinweis,
            gueltig_ab=immutable.gueltig_ab,
            gueltig_bis=immutable.gueltig_bis,
            status_gesetzt_durch=parse_optional_string_latin(
                immutable.status_gesetzt_durch
            ),
            status_gesetzt_am=self.status_gesetzt_am,
            veroeffentlichungsdatum=self.veroeffentlichungsdatum,
            letzte_aenderung=self.letzte_aenderung,
            stichwort=immutable.stichwort,
            relation=self.relation,
            schema_element_art=immutable.schema_element_art,
            bezeichnung_eingabe=immutable.bezeichnung_eingabe,
            bezeichnung_ausgabe=immutable.bezeichnung_ausgabe,
            hilfetext_eingabe=immutable.hilfetext_eingabe,
            hilfetext_ausgabe=immutable.hilfetext_ausgabe,
            regeln=immutable.regeln,
            feldart=immutable.feldart,
            datentyp=immutable.datentyp,
            praezisierung=immutable.praezisierung,
            inhalt=immutable.inhalt,
            vorbefuellung=immutable.vorbefuellung,
            werte=immutable.werte,
            codeliste_referenz=immutable.codeliste_referenz,
            code_key=immutable.code_key,
            name_key=immutable.name_key,
            help_key=immutable.help_key,
            max_size=immutable.max_size,
            media_type=immutable.media_type,
        )


@dataclass(slots=True)
class RegelExportData:
    xdf_version: XdfVersion
    freigabe_status: xdf3.FreigabeStatus | None
    status_gesetzt_am: date | None
    veroeffentlichungsdatum: date | None
    letzte_aenderung: datetime

    immutable_json: str

    SQL_COLUMNS: ClassVar[str] = ",".join(
        [
            "xdf_version",
            "freigabe_status",
            "status_gesetzt_am",
            "veroeffentlichungsdatum",
            "letzte_aenderung",
            "immutable_json",
        ]
    )

    @staticmethod
    def from_row(row: tuple[Any, ...]) -> RegelExportData:
        return RegelExportData(
            xdf_version=XdfVersion(row[0]),
            freigabe_status=xdf3.FreigabeStatus(row[1]) if row[1] is not None else None,
            status_gesetzt_am=row[2],
            veroeffentlichungsdatum=row[3],
            letzte_aenderung=row[4],
            immutable_json=row[5],
        )

    def to_xdf2(self) -> xdf2.Regel:
        assert self.xdf_version == XdfVersion.XDF2
        assert self.freigabe_status is not None

        immutable = xdf2.ImmutableRegel.model_validate_json(self.immutable_json)

        return xdf2.Regel(
            identifier=immutable.identifier,
            name=immutable.name,
            bezeichnung_eingabe=immutable.bezeichnung_eingabe,
            bezeichnung_ausgabe=immutable.bezeichnung_ausgabe,
            beschreibung=immutable.beschreibung,
            definition=immutable.definition,
            bezug=immutable.bezug,
            status=xdf3_to_xdf2.map_status(self.freigabe_status),
            versionshinweis=immutable.versionshinweis,
            gueltig_ab=immutable.gueltig_ab,
            gueltig_bis=immutable.gueltig_bis,
            fachlicher_ersteller=immutable.fachlicher_ersteller,
            freigabedatum=self.status_gesetzt_am,
            veroeffentlichungsdatum=self.veroeffentlichungsdatum,
            script=immutable.script,
        )

    def to_xdf3(self) -> xdf3.Regel:
        assert self.xdf_version == XdfVersion.XDF3

        immutable = xdf3.ImmutableRegel.model_validate_json(self.immutable_json)

        return xdf3.Regel(
            identifier=immutable.identifier,
            name=immutable.name,
            beschreibung=immutable.beschreibung,
            bezug=immutable.bezug,
            letzte_aenderung=self.letzte_aenderung,
            stichwort=immutable.stichwort,
            freitext_regel=immutable.freitext_regel,
            typ=immutable.typ,
            fachlicher_ersteller=immutable.fachlicher_ersteller,
            param=immutable.param,
            ziel=immutable.ziel,
            fehler=immutable.fehler,
            skript=immutable.skript,
        )


def _parse_relation(row: tuple[Any, ...]) -> xdf3.Relation:
    return xdf3.Relation(
        objekt=xdf3.Identifier(
            id=row[0],
            version=row[1],
        ),
        praedikat=xdf3.RelationTyp(row[2]),
    )
