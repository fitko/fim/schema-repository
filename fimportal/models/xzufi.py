from __future__ import annotations

from enum import Enum
import logging
from dataclasses import dataclass
from datetime import datetime
from typing import Any, ClassVar

from pydantic import BaseModel

from fimportal import xzufi
from fimportal.common import FreigabeStatus
from fimportal.errors import CannotImportLeistung
from fimportal.helpers import (
    extract_xzufi_rechtsgrundlagen_lines,
    remove_html_tags,
)
from fimportal.xzufi import (
    Leistung,
    LeistungsTypisierung,
    RedaktionsId,
    XzufiException,
)
from fimportal.xzufi.common import (
    LanguageCode,
    LeistungHierarchyType,
    LeistungRedaktionId,
    LeistungsId,
    ModulTextTyp,
    XzufiIdentifier,
)
from fimportal.xzufi.leistung import (
    Klassifizierung,
    LeistungsTyp,
    can_have_stammtext,
    get_struktur_typ,
)
from fimportal.xzufi.onlinedienst import Onlinedienst
from fimportal.xzufi.organisationseinheit import Organisationseinheit
from fimportal.xzufi.reports import LinkStatus, QualityReport
from fimportal.xzufi.spezialisierung import Spezialisierung
from fimportal.xzufi.zustaendigkeit import Zustaendigkeit

logger = logging.getLogger(__name__)


ALLOWED_BIBLIOTHEK_STATUS = [
    FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER,
    FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
]


class XzufiSource(Enum):
    XZUFI = "xzufi"
    PVOG = "pvog"

    def to_label(self) -> str:
        match self:
            case XzufiSource.XZUFI:
                return "Baustein Leistungen"
            case XzufiSource.PVOG:
                return "Portalverbund"


def get_leistungs_title(leistung: Leistung, german: LanguageCode) -> str:
    bezeichnung_modul = leistung.get_text_modul(ModulTextTyp.LEISTUNGSBEZEICHNUNG)
    bezeichnung_2_modul = leistung.get_text_modul(ModulTextTyp.LEISTUNGSBEZEICHNUNG_II)

    if bezeichnung_modul is not None:
        return bezeichnung_modul.expect_inhalt(german)
    elif bezeichnung_2_modul is not None:
        return bezeichnung_2_modul.expect_inhalt(german)
    else:
        return "Kein Titel"


class NewLeistung(BaseModel):
    redaktion_id: RedaktionsId
    id: LeistungsId
    title: str
    leistungsbezeichnung: str | None
    leistungsbezeichnung_2: str | None

    freigabe_status_katalog: FreigabeStatus | None
    freigabe_status_bibliothek: FreigabeStatus | None

    hierarchy_type: LeistungHierarchyType
    root_for_leistungsschluessel: str | None
    leistungstyp: LeistungsTyp | None
    typisierung: list[LeistungsTypisierung]
    leistungsschluessel: list[str]
    kurztext: str | None
    volltext: str | None
    rechtsgrundlagen: str | None
    sdg_informationsbereiche: list[str]
    einheitlicher_ansprechpartner: bool | None
    klassifizierung: list[Klassifizierung]
    links: list[str]

    erstellt_datum_zeit: datetime | None
    geaendert_datum_zeit: datetime | None

    xml_content: str

    source: XzufiSource

    @staticmethod
    def from_leistung(
        leistung: Leistung,
        xml_content: str,
        source: XzufiSource,
    ) -> NewLeistung:
        """
        `katalog_redaktions_id`: The id of the central Redaktion where the LeiKa and the Steckbriefe/Stammtexte are managed.
        This is necessary to determine whether the Leistung is a Steckbrief or Stammtext.
        """

        try:
            identifier = leistung.get_identifier()
            if identifier is None:
                raise CannotImportLeistung("Missing schemaAgencyId")

            redaktion_id, leistung_id = identifier

            root_for_leistungsschluessel, hierarchy_type = get_leistung_hierarchy_info(
                leistung, redaktion_id
            )

            # Just a sanity check, as the root_for_leistunschluessel is only given for
            # a Steckbrief/Stammtext from the Bundesredaktion.
            if root_for_leistungsschluessel is None:
                assert hierarchy_type is LeistungHierarchyType.BESCHREIBUNG
            else:
                assert hierarchy_type is not LeistungHierarchyType.BESCHREIBUNG

            # Just a sanity check, as all steckbriefe should currently from out of the
            # leika, and all Leistungsbeschreibungen should come out of the PVOG.
            if root_for_leistungsschluessel is not None:
                assert source == XzufiSource.XZUFI
            else:
                assert source == XzufiSource.PVOG

            german = leistung.get_german_language_code()
            if german is None:
                raise CannotImportLeistung("Missing german language code")

            bezeichnung_modul = leistung.get_text_modul(
                ModulTextTyp.LEISTUNGSBEZEICHNUNG
            )
            if bezeichnung_modul is None:
                bezeichnung = None
            else:
                bezeichnung = bezeichnung_modul.get_inhalt(german)

            bezeichnung_2_modul = leistung.get_text_modul(
                ModulTextTyp.LEISTUNGSBEZEICHNUNG_II
            )
            if bezeichnung_2_modul is None:
                bezeichnung_2 = None
            else:
                bezeichnung_2 = bezeichnung_2_modul.get_inhalt(german)

            kurztext_modul = leistung.get_text_modul(ModulTextTyp.KURZTEXT)
            if kurztext_modul is None:
                kurztext = None
            else:
                kurztext = kurztext_modul.get_inhalt(german)

            volltext_modul = leistung.get_text_modul(ModulTextTyp.VOLLTEXT)
            if volltext_modul is None:
                volltext = None
            else:
                volltext = volltext_modul.get_inhalt(german)

            rechtsgrundlagen_modul = leistung.get_text_modul(
                ModulTextTyp.RECHTSGRUNDLAGEN
            )
            if rechtsgrundlagen_modul is None:
                rechtsgrundlagen = None
            else:
                # NOTE(Felix): We want to make the complete module searchable, this means both the
                # text content as well as the link titles.
                # Both sources are combined into a single text field, separated by new-lines.
                inhalt = rechtsgrundlagen_modul.get_inhalt(german)
                if inhalt:
                    lines = extract_xzufi_rechtsgrundlagen_lines(inhalt)
                else:
                    lines = []

                for link in rechtsgrundlagen_modul.weiterfuehrender_link:
                    titel = remove_html_tags(link.titel) if link.titel else None
                    beschreibung = (
                        remove_html_tags(link.beschreibung)
                        if link.beschreibung
                        else None
                    )

                    if titel and beschreibung:
                        line = f"{titel}: {beschreibung}"
                    else:
                        line = titel or beschreibung or None

                    if line:
                        lines.append(line)

                if len(lines) > 0:
                    rechtsgrundlagen = "\n".join(lines)
                else:
                    rechtsgrundlagen = None

            if leistung.versionsinformation is not None:
                erstellt_datum_zeit = leistung.versionsinformation.erstellt_datum_zeit
                geaendert_datum_zeit = leistung.versionsinformation.geaendert_datum_zeit
            else:
                erstellt_datum_zeit = None
                geaendert_datum_zeit = None

            freigabe_status_katalog = leistung.get_freigabe_status_katalog()
            freigabe_status_bibliothek = leistung.get_freigabe_status_bibliothek()
        except XzufiException as error:
            raise CannotImportLeistung(str(error)) from error

        return NewLeistung(
            redaktion_id=redaktion_id,
            id=leistung_id,
            title=get_leistungs_title(leistung, german),
            leistungsbezeichnung=bezeichnung,
            leistungsbezeichnung_2=bezeichnung_2,
            freigabe_status_katalog=freigabe_status_katalog,
            freigabe_status_bibliothek=freigabe_status_bibliothek,
            hierarchy_type=hierarchy_type,
            root_for_leistungsschluessel=root_for_leistungsschluessel,
            leistungstyp=(
                get_struktur_typ(leistung.struktur)
                if leistung.struktur is not None
                else None
            ),
            typisierung=leistung.typisierung,
            leistungsschluessel=leistung.get_leistungsschluessel(),
            kurztext=kurztext,
            volltext=volltext,
            rechtsgrundlagen=rechtsgrundlagen,
            sdg_informationsbereiche=leistung.informationsbereich_sdg,
            einheitlicher_ansprechpartner=leistung.kennzeichen_ea,
            klassifizierung=leistung.klassifizierung,
            erstellt_datum_zeit=erstellt_datum_zeit,
            geaendert_datum_zeit=geaendert_datum_zeit,
            xml_content=xml_content,
            links=leistung.get_links(),
            source=source,
        )


def get_leistung_hierarchy_info(
    leistung: Leistung, redaktion_id: RedaktionsId
) -> tuple[str | None, LeistungHierarchyType]:
    """
    Based on the source Redaktion and the Leistungstypisierung, determine the hierarchy type and whether or
    not this leistung is the root for a particular Leistungsschluessel.
    """

    if redaktion_id != LeistungRedaktionId.LEIKA.value:
        return None, LeistungHierarchyType.BESCHREIBUNG

    # NOTE(Felix): Each Leistung from the LeiKa should belong to a single Leistungschluessel.
    leistungsschluessel = leistung.get_leistungsschluessel()
    if len(leistungsschluessel) != 1:
        raise CannotImportLeistung(
            f"Expected a single Leistungsschluessel for a root-Leistung, got: {leistungsschluessel}"
        )

    if (
        can_have_stammtext(leistung.typisierung)
        and leistung.get_freigabe_status_bibliothek() in ALLOWED_BIBLIOTHEK_STATUS
    ):
        hierarchy_type = LeistungHierarchyType.STAMMTEXT
    else:
        hierarchy_type = LeistungHierarchyType.STECKBRIEF

    return leistungsschluessel[0], hierarchy_type


class LeistungsbeschreibungsReferenz(BaseModel):
    redaktion_id: str
    id: str
    title: str

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "redaktion_id",
            "id",
            "title",
        ]
    )

    @staticmethod
    def from_composite(comp: tuple[Any, ...]) -> LeistungsbeschreibungsReferenz:
        return LeistungsbeschreibungsReferenz(
            redaktion_id=comp[0],
            id=comp[1],
            title=comp[2],
        )


class LeistungssteckbriefOut(BaseModel):
    # xzufi
    redaktion_id: str
    leistung_id: str
    title: str
    leistungsbezeichnung: str | None
    leistungsbezeichnung_2: str | None

    freigabestatus_katalog: FreigabeStatus | None
    freigabestatus_bibliothek: FreigabeStatus | None

    erstellt_datum_zeit: datetime | None
    geaendert_datum_zeit: datetime | None

    # Steckbrief
    leistungsschluessel: str
    typisierung: list[LeistungsTypisierung]
    leistungstyp: LeistungsTyp | None
    sdg_informationsbereiche: list[str]
    rechtsgrundlagen: str | None

    # Stammtext
    klassifizierung: list[Klassifizierung]

    # references
    leistungsbeschreibungen: list[tuple[str, str]]

    fts_match: str | None

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "redaktion_id",
            "id",
            "title",
            "leistungsbezeichnung",
            "leistungsbezeichnung_2",
            "freigabestatus_katalog",
            "freigabestatus_bibliothek",
            "root_for_leistungsschluessel",
            "typisierung",
            "leistungstyp",
            "sdg_informationsbereiche",
            "klassifizierung",
            "rechtsgrundlagen",
            """
                array(
                    SELECT ROW(s.redaktion_id, s.id)
                    FROM leistung s
                    WHERE ARRAY[leistung.root_for_leistungsschluessel]::text[] <@ s.leistungsschluessel
                    AND s.root_for_leistungsschluessel IS NULL
                ) as leistungsbeschreibungen
            """,
            "erstellt_datum_zeit",
            "geaendert_datum_zeit",
        ]
    )

    @staticmethod
    def from_row(
        row: tuple[Any, ...], fts_match: str | None = None
    ) -> LeistungssteckbriefOut:
        # NOTE(Felix): Enforced during import
        leistungsschluessel: str | None = row[7]
        assert leistungsschluessel is not None

        return LeistungssteckbriefOut(
            redaktion_id=row[0],
            leistung_id=row[1],
            title=row[2],
            leistungsbezeichnung=row[3],
            leistungsbezeichnung_2=row[4],
            freigabestatus_katalog=(
                FreigabeStatus(row[5]) if row[5] is not None else None
            ),
            freigabestatus_bibliothek=(
                FreigabeStatus(row[6]) if row[6] is not None else None
            ),
            leistungsschluessel=leistungsschluessel,
            typisierung=[xzufi.LeistungsTypisierung(item) for item in row[8]],
            leistungstyp=(
                xzufi.leistung.LeistungsTyp(row[9]) if row[9] is not None else None
            ),
            sdg_informationsbereiche=row[10],
            klassifizierung=[Klassifizierung.from_str(item) for item in row[11]],
            rechtsgrundlagen=row[12],
            leistungsbeschreibungen=row[13],
            erstellt_datum_zeit=row[14],
            geaendert_datum_zeit=row[15],
            fts_match=fts_match,
        )


@dataclass
class ProzessklassenReferenz:
    id: str
    name: str

    @staticmethod
    def from_row(row: tuple[Any, ...]) -> ProzessklassenReferenz:
        return ProzessklassenReferenz(
            id=row[0],
            name=row[1],
        )


class FullLeistungssteckbriefOut(BaseModel):
    # xzufi
    redaktion_id: str
    leistung_id: str
    title: str
    leistungsbezeichnung: str | None
    leistungsbezeichnung_2: str | None
    freigabestatus_katalog: FreigabeStatus | None
    freigabestatus_bibliothek: FreigabeStatus | None

    erstellt_datum_zeit: datetime | None
    geaendert_datum_zeit: datetime | None

    has_stammtext: bool

    # Steckbrief
    leistungsschluessel: str
    typisierung: list[LeistungsTypisierung]
    leistungstyp: LeistungsTyp | None
    sdg_informationsbereiche: list[str]

    # Stammtext
    klassifizierung: list[Klassifizierung]

    # references
    leistungsbeschreibungen: list[LeistungsbeschreibungsReferenz]
    prozessklasse: ProzessklassenReferenz | None

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "redaktion_id",
            "id",
            "title",
            "leistungsbezeichnung",
            "leistungsbezeichnung_2",
            "root_for_leistungsschluessel",
            "typisierung",
            "leistungstyp",
            "sdg_informationsbereiche",
            "klassifizierung",
            " ".join(
                [
                    "array(",
                    f"SELECT ROW({LeistungsbeschreibungsReferenz.SQL_COLUMNS})",
                    "FROM leistung s",
                    "WHERE ARRAY[leistung.root_for_leistungsschluessel]::text[] <@ s.leistungsschluessel",
                    "AND s.hierarchy_type = 'beschreibung'",
                    ") as leistungsbeschreibungen",
                ]
            ),
            "freigabestatus_katalog",
            "freigabestatus_bibliothek",
            "erstellt_datum_zeit",
            "geaendert_datum_zeit",
            "hierarchy_type = 'stammtext' as has_stammtext",
            """
                (
                    SELECT pk.name
                    FROM prozessklasse pk
                    WHERE pk.id = leistung.root_for_leistungsschluessel
                ) as prozessklasse_name
            """,
        ]
    )

    @staticmethod
    def from_row(row: tuple[Any, ...]) -> FullLeistungssteckbriefOut:
        # NOTE(Felix): Enforced during import
        leistungsschluessel: str | None = row[5]
        assert leistungsschluessel is not None

        return FullLeistungssteckbriefOut(
            redaktion_id=row[0],
            leistung_id=row[1],
            title=row[2],
            leistungsbezeichnung=row[3],
            leistungsbezeichnung_2=row[4],
            leistungsschluessel=leistungsschluessel,
            typisierung=[xzufi.LeistungsTypisierung(item) for item in row[6]],
            leistungstyp=(
                xzufi.leistung.LeistungsTyp(row[7]) if row[7] is not None else None
            ),
            sdg_informationsbereiche=row[8],
            klassifizierung=[Klassifizierung.from_str(item) for item in row[9]],
            leistungsbeschreibungen=[
                LeistungsbeschreibungsReferenz.from_composite(array)
                for array in row[10]
            ],
            freigabestatus_katalog=(
                FreigabeStatus(row[11]) if row[11] is not None else None
            ),
            freigabestatus_bibliothek=(
                FreigabeStatus(row[12]) if row[12] is not None else None
            ),
            erstellt_datum_zeit=row[13],
            geaendert_datum_zeit=row[14],
            has_stammtext=row[15],
            prozessklasse=ProzessklassenReferenz(id=leistungsschluessel, name=row[16])
            if row[16] is not None
            else None,
        )


class LeistungsbeschreibungOut(BaseModel):
    redaktion_id: str
    id: str
    title: str
    leistungsbezeichnung: str | None
    leistungsbezeichnung_2: str | None
    freigabestatus_katalog: FreigabeStatus | None
    freigabestatus_bibliothek: FreigabeStatus | None
    hierarchy_type: LeistungHierarchyType
    root_for_leistungsschluessel: str | None
    leistungstyp: LeistungsTyp | None
    typisierung: list[LeistungsTypisierung]
    leistungsschluessel: list[str]
    kurztext: str | None
    volltext: str | None
    rechtsgrundlagen: str | None
    erstellt_datum_zeit: datetime | None
    geaendert_datum_zeit: datetime | None
    fts_match: str | None

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "redaktion_id",
            "id",
            "title",
            "leistungsbezeichnung",
            "leistungsbezeichnung_2",
            "hierarchy_type",
            "root_for_leistungsschluessel",
            "leistungstyp",
            "typisierung",
            "leistungsschluessel",
            "kurztext",
            "volltext",
            "rechtsgrundlagen",
            "freigabestatus_katalog",
            "freigabestatus_bibliothek",
            "erstellt_datum_zeit",
            "geaendert_datum_zeit",
        ]
    )

    @staticmethod
    def from_row(
        row: tuple[Any, ...], fts_match: str | None = None
    ) -> LeistungsbeschreibungOut:
        return LeistungsbeschreibungOut(
            redaktion_id=row[0],
            id=row[1],
            title=row[2],
            leistungsbezeichnung=row[3],
            leistungsbezeichnung_2=row[4],
            hierarchy_type=LeistungHierarchyType(row[5]),
            root_for_leistungsschluessel=row[6],
            leistungstyp=(
                xzufi.leistung.LeistungsTyp(row[7]) if row[7] is not None else None
            ),
            typisierung=[xzufi.leistung.LeistungsTypisierung(v) for v in row[8]],
            leistungsschluessel=row[9],
            kurztext=row[10],
            volltext=row[11],
            rechtsgrundlagen=row[12],
            freigabestatus_katalog=(
                FreigabeStatus(row[13]) if row[13] is not None else None
            ),
            freigabestatus_bibliothek=(
                FreigabeStatus(row[14]) if row[14] is not None else None
            ),
            erstellt_datum_zeit=row[15],
            geaendert_datum_zeit=row[16],
            fts_match=fts_match,
        )


class FullLeistungsbeschreibungOut(BaseModel):
    redaktion_id: str
    id: str
    title: str
    leistungsbezeichnung: str | None
    leistungsbezeichnung_2: str | None
    freigabestatus_katalog: FreigabeStatus | None
    freigabestatus_bibliothek: FreigabeStatus | None
    hierarchy_type: LeistungHierarchyType
    root_for_leistungsschluessel: str | None
    leistungstyp: LeistungsTyp | None
    typisierung: list[LeistungsTypisierung]
    leistungsschluessel: list[str]
    kurztext: str | None
    volltext: str | None
    rechtsgrundlagen: str | None
    erstellt_datum_zeit: datetime | None
    geaendert_datum_zeit: datetime | None
    steckbrief_name: str | None

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "redaktion_id",
            "id",
            "title",
            "leistungsbezeichnung",
            "leistungsbezeichnung_2",
            "hierarchy_type",
            "root_for_leistungsschluessel",
            "leistungstyp",
            "typisierung",
            "leistungsschluessel",
            "kurztext",
            "volltext",
            "rechtsgrundlagen",
            "freigabestatus_katalog",
            "freigabestatus_bibliothek",
            "erstellt_datum_zeit",
            "geaendert_datum_zeit",
            """
                (
                    SELECT l2.title
                    FROM leistung l2
                    WHERE l2.root_for_leistungsschluessel = leistung.leistungsschluessel[1]
                )
            """,
        ]
    )

    @staticmethod
    def from_row(row: tuple[Any, ...]) -> FullLeistungsbeschreibungOut:
        return FullLeistungsbeschreibungOut(
            redaktion_id=row[0],
            id=row[1],
            title=row[2],
            leistungsbezeichnung=row[3],
            leistungsbezeichnung_2=row[4],
            hierarchy_type=LeistungHierarchyType(row[5]),
            root_for_leistungsschluessel=row[6],
            leistungstyp=(
                xzufi.leistung.LeistungsTyp(row[7]) if row[7] is not None else None
            ),
            typisierung=[xzufi.leistung.LeistungsTypisierung(v) for v in row[8]],
            leistungsschluessel=row[9],
            kurztext=row[10],
            volltext=row[11],
            rechtsgrundlagen=row[12],
            freigabestatus_katalog=(
                FreigabeStatus(row[13]) if row[13] is not None else None
            ),
            freigabestatus_bibliothek=(
                FreigabeStatus(row[14]) if row[14] is not None else None
            ),
            erstellt_datum_zeit=row[15],
            geaendert_datum_zeit=row[16],
            steckbrief_name=row[17],
        )


class SpezialisierungOut(BaseModel):
    redaktion_id: str
    id: str
    leistung_redaktion_id: str
    leistung_id: str

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        ["redaktion_id", "id", "leistung_redaktion_id", "leistung_id"]
    )

    @staticmethod
    def from_row(
        row: tuple[Any, ...], fts_match: str | None = None
    ) -> SpezialisierungOut:
        return SpezialisierungOut(
            redaktion_id=row[0],
            id=row[1],
            leistung_redaktion_id=row[2],
            leistung_id=row[3],
        )


class ZustaendigkeitOut(BaseModel):
    redaktion_id: str
    id: str

    SQL_COLUMNS: ClassVar[str] = ", ".join(["redaktion_id", "id"])

    @staticmethod
    def from_row(
        row: tuple[Any, ...], fts_match: str | None = None
    ) -> ZustaendigkeitOut:
        return ZustaendigkeitOut(
            redaktion_id=row[0],
            id=row[1],
        )


class OrganisationseinheitOut(BaseModel):
    redaktion_id: str
    id: str

    SQL_COLUMNS: ClassVar[str] = ", ".join(["redaktion_id", "id"])

    @staticmethod
    def from_row(
        row: tuple[Any, ...], fts_match: str | None = None
    ) -> OrganisationseinheitOut:
        return OrganisationseinheitOut(
            redaktion_id=row[0],
            id=row[1],
        )


class OnlinedienstOut(BaseModel):
    redaktion_id: str
    id: str

    SQL_COLUMNS: ClassVar[str] = ", ".join(["redaktion_id", "id"])

    @staticmethod
    def from_row(row: tuple[Any, ...], fts_match: str | None = None) -> OnlinedienstOut:
        return OnlinedienstOut(
            redaktion_id=row[0],
            id=row[1],
        )


@dataclass(slots=True)
class Link:
    uri: str
    last_checked: datetime | None
    last_online: datetime | None

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "uri",
            "last_checked",
            "last_online",
        ]
    )

    @staticmethod
    def from_row(row: tuple[Any, ...], fts_match: str | None = None) -> Link:
        return Link(
            uri=row[0],
            last_checked=row[1],
            last_online=row[2],
        )

    def is_online(self):
        return not (self.last_online is None or self.last_checked != self.last_online)


class QualityReportOut(BaseModel):
    redaktion_id: str | None
    leistung_id: str
    link_status: list[LinkStatus]

    @staticmethod
    def from_quality_report(quality_report: QualityReport):
        return QualityReportOut(
            redaktion_id=quality_report.redaktion_id,
            leistung_id=quality_report.leistung_id,
            link_status=quality_report.link_statuses,
        )


class PvogResourceClass(Enum):
    LEISTUNG = "leistung"
    SPEZIALISIERUNG = "spezialisierung"
    ORGANISATIONSEINHEIT = "organisationseinheit"
    ZUSTAENDIGKEIT = "zustaendigkeit"
    ONLINEDIENST = "onlinedienst"


@dataclass(slots=True)
class NewSpezialisierung:
    id: XzufiIdentifier
    leistung: XzufiIdentifier
    xml_content: str

    @staticmethod
    def from_spezialisierung(spezialisierung: Spezialisierung, xml_content: str):
        identifier = spezialisierung.get_identifier()
        if identifier is None:
            raise CannotImportLeistung("Missing Identifier")

        leistung = spezialisierung.id_leistung.to_xzufi_identifier()
        if leistung is None:
            raise CannotImportLeistung("Missing Leistung Identifier")
        return NewSpezialisierung(
            id=identifier, leistung=leistung, xml_content=xml_content
        )


@dataclass(slots=True)
class NewOrganisationseinheit:
    id: XzufiIdentifier
    xml_content: str

    @staticmethod
    def from_organisationseinheit(
        organisationseinheit: Organisationseinheit, xml_content: str
    ):
        identifier = organisationseinheit.id.to_xzufi_identifier()
        if identifier is None:
            raise CannotImportLeistung("Missing schemaAgencyId")
        return NewOrganisationseinheit(identifier, xml_content)


@dataclass(slots=True)
class NewZustaendigkeit:
    id: XzufiIdentifier
    xml_content: str

    @staticmethod
    def from_zustaendigkeit(zustaendigkeit: Zustaendigkeit, xml_content: str):
        identifier = zustaendigkeit.id.to_xzufi_identifier()
        if identifier is None:
            raise CannotImportLeistung("Missing schemaAgencyId")
        return NewZustaendigkeit(identifier, xml_content)


@dataclass(slots=True)
class NewOnlinedienst:
    id: XzufiIdentifier
    xml_content: str

    @staticmethod
    def from_onlinedienst(onlinedienst: Onlinedienst, xml_content: str):
        identifier = onlinedienst.id.to_xzufi_identifier()
        if identifier is None:
            raise CannotImportLeistung("Missing schemaAgencyId")
        return NewOnlinedienst(identifier, xml_content)
