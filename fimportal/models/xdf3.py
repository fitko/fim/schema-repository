from __future__ import annotations

from collections import defaultdict
from dataclasses import dataclass
from datetime import date, datetime
from typing import Any, ClassVar, Sequence, TypeVar

from pydantic import BaseModel
from fimportal.common import FreigabeStatus
from fimportal.errors import ImportException
from fimportal.helpers import get_latest_version, utc_now, generate_json

from fimportal.xdatenfelder import xdf3, xdf2
from fimportal.xdatenfelder.xdf3.common import Dokumentart, Relation, RelationTyp
from .imports import (
    ChildType,
    XdfVersion,
)


ElementIdentifierTuple = tuple[str, str, str]
"""
A simple identifier for xdf groups and fields. The content is `namespace`, `fim_id` and `fim_version`.
"""


class RelationOut(BaseModel):
    typ: RelationTyp
    fim_id: str
    fim_version: str | None
    name: str | None

    @staticmethod
    def from_row(row: tuple[Any, ...]) -> RelationOut:
        return RelationOut(
            typ=RelationTyp(row[0]), fim_id=row[1], fim_version=row[2], name=row[3]
        )


@dataclass(slots=True)
class TreeRelationModel:
    """
    Many-to-many relationship describing the internal tree structure of a Stammdatenschema.

    A child could be included multiple times in the same group or schema.
    """

    parent_id: int
    child_index: int
    child_id: int
    anzahl: str
    bezug: list[str]


@dataclass(slots=True)
class SchemaFileModel:
    filename: str
    created_at: datetime
    schema_fim_id: str
    schema_fim_version: str
    content: str | None = None

    @staticmethod
    def create(
        xml_content: str,
        xdf_version: XdfVersion,
        schema_fim_id: str,
        schema_fim_version: str,
    ) -> SchemaFileModel:
        match xdf_version:
            case XdfVersion.XDF2:
                version_identifier = "xdf2"
            case XdfVersion.XDF3:
                version_identifier = "xdf3"

        now = utc_now()

        # NOTE(Felix): Use both the current date as well as the
        # UNIX timestamp in milliseconds for the filename.
        # The timestamp alone is enough to uniquely identify the upload.
        # The date is only there for better readability.
        date_string = now.date().isoformat()
        epochmillis = int(now.timestamp() * 1_000)
        filename = f"{schema_fim_id}V{schema_fim_version}_{date_string}-{epochmillis}.{version_identifier}.xml"

        return SchemaFileModel(
            filename=filename,
            content=xml_content,
            created_at=now,
            schema_fim_id=schema_fim_id,
            schema_fim_version=schema_fim_version,
        )


@dataclass(slots=True)
class JsonSchemaFileModel:
    filename: str
    created_at: datetime
    generated_from: str
    canonical_hash: str
    content: str

    @staticmethod
    def create_filename(
        schema_fim_id: str, schema_fim_version: str, now: datetime
    ) -> str:
        # NOTE(Felix): Use both the current date as well as the
        # UNIX timestamp in milliseconds for the filename.
        # The timestamp alone is enough to uniquely identify the upload.
        # The date is only there for better readability.
        date_string = now.date().isoformat()
        epochmillis = int(now.timestamp() * 1_000)
        return f"{schema_fim_id}V{schema_fim_version}_{date_string}-{epochmillis}.schema.json"


@dataclass(slots=True)
class XsdFileModel:
    filename: str
    created_at: datetime
    generated_from: str
    content: str
    canonical_hash: str

    @staticmethod
    def create(
        schema_fim_id: str,
        schema_fim_version: str,
        content: str,
        generated_from: str,
        canonical_hash: str,
    ) -> XsdFileModel:
        now = utc_now()

        # NOTE(Felix): Use both the current date as well as the
        # UNIX timestamp in milliseconds for the filename.
        # The timestamp alone is enough to uniquely identify the upload.
        # The date is only there for better readability.
        date_string = now.date().isoformat()
        epochmillis = int(now.timestamp() * 1_000)
        filename = (
            f"{schema_fim_id}V{schema_fim_version}_{date_string}-{epochmillis}.xsd"
        )

        return XsdFileModel(
            filename=filename,
            content=content,
            created_at=now,
            generated_from=generated_from,
            canonical_hash=canonical_hash,
        )


@dataclass(slots=True)
class NewSteckbrief:
    fim_id: str
    fim_version: str
    nummernkreis: str
    name: str
    definition: str | None
    bezeichnung: str | None
    beschreibung: str | None
    freigabe_status: FreigabeStatus
    status_gesetzt_am: date | None
    versionshinweis: str | None
    status_gesetzt_durch: str | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    bezug: list[str]
    veroeffentlichungsdatum: date | None
    relation: list[Relation]
    letzte_aenderung: datetime
    last_update: datetime
    ist_abstrakt: bool
    dokumentart: Dokumentart
    hilfetext: str | None
    stichwort: list[str]
    xdf_version: XdfVersion
    immutable_json: dict[str, Any]

    @staticmethod
    def from_xdf2(
        steckbrief: xdf2.Steckbrief,
        freigabe_status: FreigabeStatus,
        letzte_aenderung: datetime,
        last_update: datetime,
    ) -> NewSteckbrief:
        version = steckbrief.identifier.version
        if version is None:
            raise ImportException(
                f"Cannot import rule without a version [id={steckbrief.identifier.id}]"
            )

        # Convert to xdf3 nummernkreis
        nummernkreis = f"{steckbrief.identifier.nummernkreis}000"
        bezug = [steckbrief.bezug] if steckbrief.bezug is not None else []

        immutable_json = generate_json(
            xdf2.ImmutableSteckbrief.from_steckbrief(steckbrief)
        )

        return NewSteckbrief(
            fim_id=steckbrief.identifier.id,
            fim_version=version,
            nummernkreis=nummernkreis,
            name=steckbrief.name,
            definition=steckbrief.definition,
            bezeichnung=steckbrief.bezeichnung_eingabe,
            beschreibung=steckbrief.beschreibung,
            freigabe_status=freigabe_status,
            status_gesetzt_am=steckbrief.freigabedatum,
            versionshinweis=steckbrief.versionshinweis,
            status_gesetzt_durch=steckbrief.fachlicher_ersteller,
            bezug=bezug,
            veroeffentlichungsdatum=steckbrief.veroeffentlichungsdatum,
            relation=[],
            letzte_aenderung=letzte_aenderung,
            last_update=last_update,
            gueltig_ab=steckbrief.gueltig_ab,
            gueltig_bis=steckbrief.gueltig_bis,
            ist_abstrakt=steckbrief.is_referenz,
            dokumentart=Dokumentart.from_label(steckbrief.dokumentart),
            hilfetext=steckbrief.hilfetext,
            stichwort=[],
            xdf_version=XdfVersion.XDF2,
            immutable_json=immutable_json,
        )

    @staticmethod
    def from_xdf3(
        steckbrief: xdf3.Steckbrief,
        letzte_aenderung: datetime,
        last_update: datetime,
    ) -> NewSteckbrief:
        version = steckbrief.identifier.version
        if version is None:
            raise ImportException(
                f"Cannot import rule without a version [id={steckbrief.identifier.id}]"
            )

        bezug = [str(bezug.text) for bezug in steckbrief.bezug]

        immutable_json = generate_json(
            xdf3.ImmutableSteckbrief.from_steckbrief(steckbrief)
        )

        return NewSteckbrief(
            fim_id=steckbrief.identifier.id,
            fim_version=version,
            nummernkreis=steckbrief.identifier.nummernkreis,
            name=steckbrief.name,
            definition=steckbrief.definition,
            bezeichnung=steckbrief.bezeichnung,
            beschreibung=steckbrief.beschreibung,
            freigabe_status=steckbrief.freigabe_status,
            status_gesetzt_am=steckbrief.status_gesetzt_am,
            versionshinweis=steckbrief.versionshinweis,
            status_gesetzt_durch=steckbrief.status_gesetzt_durch,
            bezug=bezug,
            veroeffentlichungsdatum=steckbrief.veroeffentlichungsdatum,
            relation=steckbrief.relation,
            letzte_aenderung=letzte_aenderung,
            last_update=last_update,
            gueltig_ab=steckbrief.gueltig_ab,
            gueltig_bis=steckbrief.gueltig_bis,
            ist_abstrakt=steckbrief.ist_abstrakt,
            dokumentart=steckbrief.dokumentart,
            hilfetext=steckbrief.hilfetext,
            stichwort=[stichwort.value for stichwort in steckbrief.stichwort],
            xdf_version=XdfVersion.XDF3,
            immutable_json=immutable_json,
        )


class DirectChild(BaseModel):
    namespace: str
    fim_id: str
    fim_version: str
    type: ChildType
    anzahl: str
    bezug: list[str]


class SteckbriefOut(BaseModel):
    fim_id: str
    fim_version: str
    nummernkreis: str
    name: str
    definition: str | None
    bezeichnung: str | None
    beschreibung: str | None
    freigabe_status: xdf3.FreigabeStatus
    freigabe_status_label: str
    status_gesetzt_durch: str | None
    status_gesetzt_am: date | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    bezug: list[str]
    versionshinweis: str | None
    veroeffentlichungsdatum: date | None
    letzte_aenderung: datetime
    last_update: datetime
    ist_abstrakt: bool
    dokumentart: Dokumentart
    hilfetext: str | None
    stichwort: list[str]
    xdf_version: XdfVersion
    is_latest: bool

    fts_match: str | None

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "fim_id",
            "fim_version",
            "nummernkreis",
            "name",
            "definition",
            "bezeichnung",
            "beschreibung",
            "freigabe_status",
            "versionshinweis",
            "status_gesetzt_durch",
            "status_gesetzt_am",
            "gueltig_ab",
            "gueltig_bis",
            "bezug",
            "stichwort",
            "veroeffentlichungsdatum",
            "letzte_aenderung",
            "last_update",
            "dokumentart",
            "ist_abstrakt",
            "hilfetext",
            "xdf_version",
            "is_latest",
        ]
    )

    @staticmethod
    def from_row(row: tuple[Any, ...], fts_match: str | None = None) -> SteckbriefOut:
        freigabe_status = FreigabeStatus(row[7])
        return SteckbriefOut(
            fim_id=row[0],
            fim_version=row[1],
            nummernkreis=row[2],
            name=row[3],
            definition=row[4],
            bezeichnung=row[5],
            beschreibung=row[6],
            freigabe_status=freigabe_status,
            freigabe_status_label=freigabe_status.to_label(),
            versionshinweis=row[8],
            status_gesetzt_durch=row[9],
            status_gesetzt_am=row[10],
            gueltig_ab=row[11],
            gueltig_bis=row[12],
            bezug=row[13],
            stichwort=row[14],
            veroeffentlichungsdatum=row[15],
            letzte_aenderung=row[16],
            last_update=row[17],
            dokumentart=Dokumentart(row[18]),
            ist_abstrakt=row[19],
            hilfetext=row[20],
            xdf_version=XdfVersion(row[21]),
            is_latest=row[22],
            fts_match=fts_match,
        )


class ProzessReferenz(BaseModel):
    id: str
    name: str

    @staticmethod
    def from_row(row: tuple[Any, ...]) -> ProzessReferenz:
        return ProzessReferenz(
            id=row[0],
            name=row[1],
        )


class DatenschemaReferenz(BaseModel):
    fim_id: str
    fim_version: str
    name: str

    @staticmethod
    def from_row(row: tuple[Any, ...]) -> DatenschemaReferenz:
        return DatenschemaReferenz(
            fim_id=row[0],
            fim_version=row[1],
            name=row[2],
        )


class FullSteckbriefOut(BaseModel):
    fim_id: str
    fim_version: str
    nummernkreis: str
    name: str
    definition: str | None
    bezeichnung: str | None
    beschreibung: str | None
    freigabe_status: xdf3.FreigabeStatus
    freigabe_status_label: str
    status_gesetzt_durch: str | None
    status_gesetzt_am: date | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    bezug: list[str]
    versionshinweis: str | None
    veroeffentlichungsdatum: date | None
    letzte_aenderung: datetime
    last_update: datetime
    ist_abstrakt: bool
    dokumentart: Dokumentart
    hilfetext: str | None
    stichwort: list[str]
    xdf_version: XdfVersion
    is_latest: bool
    prozesse: list[ProzessReferenz]
    datenschemata: list[DatenschemaReferenz]
    relation: list[RelationOut]

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "fim_id",
            "fim_version",
            "nummernkreis",
            "name",
            "definition",
            "bezeichnung",
            "beschreibung",
            "freigabe_status",
            "versionshinweis",
            "status_gesetzt_durch",
            "status_gesetzt_am",
            "gueltig_ab",
            "gueltig_bis",
            "bezug",
            "stichwort",
            "veroeffentlichungsdatum",
            "letzte_aenderung",
            "last_update",
            "dokumentart",
            "ist_abstrakt",
            "hilfetext",
            "xdf_version",
            "is_latest",
            """
                array(
                    SELECT ROW(p.id, p.name)
                    FROM prozess_dokumentsteckbrief_relation l
                    JOIN prozess p
                    ON l.prozess_id = p.id
                    WHERE l.dokumentsteckbrief_fim_id = dokumentensteckbrief.fim_id
                    ORDER BY p.id
                ) as prozesse
            """,
            """
                array(
                    SELECT ROW(s.fim_id, s.fim_version, s.name)
                    FROM schema s
                    WHERE s.steckbrief_id = dokumentensteckbrief.fim_id
                ) as datenschemata
            """,
            """
                array(
                    SELECT ROW(ds_rel.praedikat, ds_rel.target_fim_id, ds_rel.target_fim_version, ds.name) 
                    FROM dokumentsteckbrief_relations ds_rel 
                    LEFT OUTER JOIN dokumentensteckbrief ds 
                    ON ds.fim_id = ds_rel.target_fim_id 
                    AND (
                        (ds.fim_version = ds_rel.target_fim_version AND ds_rel.target_fim_version IS NOT NULL) 
                        OR 
                        (ds_rel.target_fim_version IS NULL AND ds.is_latest)
                    )
                    WHERE ds_rel.source_fim_id = dokumentensteckbrief.fim_id AND ds_rel.source_fim_version = dokumentensteckbrief.fim_version
                ) as relations
            """,
        ]
    )

    @staticmethod
    def from_row(row: tuple[Any, ...]) -> FullSteckbriefOut:
        freigabe_status = FreigabeStatus(row[7])
        return FullSteckbriefOut(
            fim_id=row[0],
            fim_version=row[1],
            nummernkreis=row[2],
            name=row[3],
            definition=row[4],
            bezeichnung=row[5],
            beschreibung=row[6],
            freigabe_status=freigabe_status,
            freigabe_status_label=freigabe_status.to_label(),
            versionshinweis=row[8],
            status_gesetzt_durch=row[9],
            status_gesetzt_am=row[10],
            gueltig_ab=row[11],
            gueltig_bis=row[12],
            bezug=row[13],
            stichwort=row[14],
            veroeffentlichungsdatum=row[15],
            letzte_aenderung=row[16],
            last_update=row[17],
            dokumentart=Dokumentart(row[18]),
            ist_abstrakt=row[19],
            hilfetext=row[20],
            xdf_version=XdfVersion(row[21]),
            is_latest=row[22],
            prozesse=[ProzessReferenz.from_row(r) for r in row[23]],
            datenschemata=[DatenschemaReferenz.from_row(r) for r in row[24]],
            relation=[RelationOut.from_row(r) for r in row[25]],
        )


class SchemaOut(BaseModel):
    fim_id: str
    fim_version: str
    nummernkreis: str
    name: str
    beschreibung: str | None
    definition: str | None
    freigabe_status: xdf3.FreigabeStatus
    freigabe_status_label: str
    status_gesetzt_am: date | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    status_gesetzt_durch: str | None
    steckbrief_id: str | None
    xdf_version: XdfVersion
    bezug: list[str]
    versionshinweis: str | None
    stichwort: list[str]
    letzte_aenderung: datetime
    last_update: datetime
    bezeichnung: str | None
    bezug_components: list[str]
    veroeffentlichungsdatum: date | None
    is_latest: bool

    fts_match: str | None

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "fim_id",
            "fim_version",
            "nummernkreis",
            "name",
            "beschreibung",
            "definition",
            "bezug",
            "freigabe_status",
            "status_gesetzt_durch",
            "gueltig_ab",
            "gueltig_bis",
            "versionshinweis",
            "letzte_aenderung",
            "stichwort",
            "steckbrief_id",
            "xdf_version",
            "last_update",
            "bezeichnung",
            "status_gesetzt_am",
            "bezug_components",
            "veroeffentlichungsdatum",
            "is_latest",
        ]
    )

    @staticmethod
    def from_row(row: tuple[Any, ...], fts_match: str | None = None) -> SchemaOut:
        freigabe_status = FreigabeStatus(row[7])

        return SchemaOut(
            fim_id=row[0],
            fim_version=row[1],
            nummernkreis=row[2],
            name=row[3],
            beschreibung=row[4],
            definition=row[5],
            bezug=row[6],
            freigabe_status=freigabe_status,
            freigabe_status_label=freigabe_status.to_label(),
            status_gesetzt_durch=row[8],
            gueltig_ab=row[9],
            gueltig_bis=row[10],
            versionshinweis=row[11],
            letzte_aenderung=row[12],
            stichwort=row[13],
            steckbrief_id=row[14],
            xdf_version=XdfVersion(row[15]),
            last_update=row[16],
            bezeichnung=row[17],
            status_gesetzt_am=row[18],
            bezug_components=row[19],
            veroeffentlichungsdatum=row[20],
            is_latest=row[21],
            fts_match=fts_match,
        )


class DatenfeldOut(BaseModel):
    namespace: str
    fim_id: str
    fim_version: str
    nummernkreis: str
    name: str
    beschreibung: str | None
    definition: str | None
    bezug: list[str]
    freigabe_status: xdf3.FreigabeStatus
    freigabe_status_label: str
    status_gesetzt_am: date | None
    status_gesetzt_durch: str | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    versionshinweis: str | None
    veroeffentlichungsdatum: date | None
    letzte_aenderung: datetime
    last_update: datetime
    feldart: xdf3.Feldart
    datentyp: xdf3.Datentyp

    xdf_version: XdfVersion
    is_latest: bool

    fts_match: str | None

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "namespace",
            "fim_id",
            "fim_version",
            "nummernkreis",
            "name",
            "beschreibung",
            "definition",
            "bezug",
            "freigabe_status",
            "status_gesetzt_am",
            "status_gesetzt_durch",
            "gueltig_ab",
            "gueltig_bis",
            "versionshinweis",
            "veroeffentlichungsdatum",
            "letzte_aenderung",
            "last_update",
            "feldart",
            "datentyp",
            "xdf_version",
            "is_latest",
        ]
    )

    @staticmethod
    def from_row(row: tuple[Any, ...], fts_match: str | None = None) -> DatenfeldOut:
        freigabe_status = FreigabeStatus(row[8])

        return DatenfeldOut(
            namespace=row[0],
            fim_id=row[1],
            fim_version=row[2],
            nummernkreis=row[3],
            name=row[4],
            beschreibung=row[5],
            definition=row[6],
            bezug=row[7],
            freigabe_status=freigabe_status,
            freigabe_status_label=freigabe_status.to_label(),
            status_gesetzt_am=row[9],
            status_gesetzt_durch=row[10],
            gueltig_ab=row[11],
            gueltig_bis=row[12],
            versionshinweis=row[13],
            veroeffentlichungsdatum=row[14],
            letzte_aenderung=row[15],
            last_update=row[16],
            feldart=xdf3.Feldart(row[17]),
            datentyp=xdf3.Datentyp(row[18]),
            xdf_version=XdfVersion(row[19]),
            is_latest=row[20],
            fts_match=fts_match,
        )


class CodeListReferenceOut(BaseModel):
    id: int
    genericode_canonical_version_uri: str
    source: str | None
    url: str | None

    @staticmethod
    def from_row(row: tuple[Any, ...], immutable_base_url: str) -> CodeListReferenceOut:
        id = row[0]
        source = row[2]

        if source is None:
            url = None
        else:
            url = f"{immutable_base_url}/immutable/code-lists/{id}/genericode.xml"

        return CodeListReferenceOut(
            id=id,
            genericode_canonical_version_uri=row[1],
            source=source,
            url=url,
        )


class DetailedDatenfeld(BaseModel):
    """
    Base data for FullDatenfeldOut.
    """

    namespace: str
    fim_id: str
    fim_version: str
    nummernkreis: str
    xdf_version: XdfVersion
    name: str
    beschreibung: str | None
    definition: str | None
    bezug: list[str]
    freigabe_status: xdf3.FreigabeStatus
    freigabe_status_label: str
    status_gesetzt_am: date | None
    status_gesetzt_durch: str | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    versionshinweis: str | None
    veroeffentlichungsdatum: date | None
    letzte_aenderung: datetime
    last_update: datetime
    feldart: xdf3.Feldart
    datentyp: xdf3.Datentyp
    is_latest: bool

    regeln: list[RegelReferenz]
    relation: list[RelationOut]
    code_list: CodeListReferenceOut | None

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "namespace",
            "fim_id",
            "fim_version",
            "nummernkreis",
            "name",
            "beschreibung",
            "definition",
            "bezug",
            "freigabe_status",
            "status_gesetzt_am",
            "status_gesetzt_durch",
            "gueltig_ab",
            "gueltig_bis",
            "versionshinweis",
            "veroeffentlichungsdatum",
            "letzte_aenderung",
            "last_update",
            "feldart",
            "datentyp",
            "xdf_version",
            "is_latest",
            """
                array(
                    SELECT ROW(r.regel_fim_id, r.regel_fim_version)
                    FROM datenfeld_regel_relation r
                    WHERE r.datenfeld_namespace = namespace
                    AND r.datenfeld_fim_id = fim_id
                    AND r.datenfeld_fim_version = fim_version
                ) as regeln
            """,
            """
                array(
                    SELECT ROW(rel.praedikat, rel.target_fim_id, rel.target_fim_version, d.name)
                    FROM datenfeld_relations rel
                    LEFT OUTER JOIN datenfeld d
                    ON rel.target_fim_id = d.fim_id
                    AND (
                        (rel.target_fim_version IS NOT NULL AND rel.target_fim_version = d.fim_version)
                        OR
                        (rel.target_fim_version IS NULL AND d.is_latest)
                    )
                    WHERE rel.source_fim_id = datenfeld.fim_id
                    AND rel.source_namespace = datenfeld.namespace
                    AND rel.source_fim_version = datenfeld.fim_version
                ) as relations
            """,
            """
                (
                    SELECT Row(c.id, c.genericode_canonical_version_uri, c.source)
                    FROM code_list c
                    WHERE c.id = datenfeld.code_list_id
                ) as code_list
            """,
        ]
    )

    @staticmethod
    def from_row(row: tuple[Any, ...], immutable_base_url: str) -> DetailedDatenfeld:
        freigabe_status = FreigabeStatus(row[8])

        code_list = row[23]
        if code_list is not None:
            code_list = CodeListReferenceOut.from_row(
                code_list, immutable_base_url=immutable_base_url
            )

        return DetailedDatenfeld(
            namespace=row[0],
            fim_id=row[1],
            fim_version=row[2],
            nummernkreis=row[3],
            name=row[4],
            beschreibung=row[5],
            definition=row[6],
            bezug=row[7],
            freigabe_status=freigabe_status,
            freigabe_status_label=freigabe_status.to_label(),
            status_gesetzt_am=row[9],
            status_gesetzt_durch=row[10],
            gueltig_ab=row[11],
            gueltig_bis=row[12],
            versionshinweis=row[13],
            veroeffentlichungsdatum=row[14],
            letzte_aenderung=row[15],
            last_update=row[16],
            feldart=xdf3.Feldart(row[17]),
            datentyp=xdf3.Datentyp(row[18]),
            xdf_version=XdfVersion(row[19]),
            is_latest=row[20],
            regeln=[RegelReferenz.from_row(r) for r in row[21]],
            relation=[RelationOut.from_row(r) for r in row[22]],
            code_list=code_list,
        )


class FullDatenfeldOut(BaseModel):
    namespace: str
    fim_id: str
    fim_version: str
    nummernkreis: str
    xdf_version: XdfVersion
    name: str
    beschreibung: str | None
    definition: str | None
    bezug: list[str]
    freigabe_status: xdf3.FreigabeStatus
    freigabe_status_label: str
    status_gesetzt_am: date | None
    status_gesetzt_durch: str | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    versionshinweis: str | None
    veroeffentlichungsdatum: date | None
    letzte_aenderung: datetime
    last_update: datetime
    feldart: xdf3.Feldart
    datentyp: xdf3.Datentyp
    is_latest: bool

    regeln: list[RegelReferenz]
    schemas: list[SchemaOut]
    relation: list[RelationOut]
    code_list: CodeListReferenceOut | None

    @staticmethod
    def create(field: DetailedDatenfeld, schemas: list[SchemaOut]) -> FullDatenfeldOut:
        return FullDatenfeldOut(
            namespace=field.namespace,
            fim_id=field.fim_id,
            fim_version=field.fim_version,
            nummernkreis=field.nummernkreis,
            xdf_version=field.xdf_version,
            name=field.name,
            beschreibung=field.beschreibung,
            definition=field.definition,
            bezug=field.bezug,
            freigabe_status=field.freigabe_status,
            freigabe_status_label=field.freigabe_status_label,
            status_gesetzt_am=field.status_gesetzt_am,
            status_gesetzt_durch=field.status_gesetzt_durch,
            gueltig_ab=field.gueltig_ab,
            gueltig_bis=field.gueltig_bis,
            versionshinweis=field.versionshinweis,
            veroeffentlichungsdatum=field.veroeffentlichungsdatum,
            letzte_aenderung=field.letzte_aenderung,
            last_update=field.last_update,
            feldart=field.feldart,
            datentyp=field.datentyp,
            is_latest=field.is_latest,
            regeln=field.regeln,
            schemas=schemas,
            relation=field.relation,
            code_list=field.code_list,
        )


class DatenfeldgruppeOut(BaseModel):
    namespace: str
    fim_id: str
    fim_version: str
    nummernkreis: str
    xdf_version: XdfVersion
    name: str
    beschreibung: str | None
    definition: str | None
    freigabe_status: xdf3.FreigabeStatus
    freigabe_status_label: str
    status_gesetzt_durch: str | None
    bezug: list[str]
    status_gesetzt_am: date | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    versionshinweis: str | None
    veroeffentlichungsdatum: date | None
    letzte_aenderung: datetime
    last_update: datetime
    is_latest: bool

    fts_match: str | None

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "namespace",
            "fim_id",
            "fim_version",
            "nummernkreis",
            "xdf_version",
            "name",
            "beschreibung",
            "definition",
            "status_gesetzt_durch",
            "bezug",
            "freigabe_status",
            "status_gesetzt_am",
            "gueltig_ab",
            "gueltig_bis",
            "versionshinweis",
            "veroeffentlichungsdatum",
            "letzte_aenderung",
            "last_update",
            "is_latest",
        ]
    )

    @staticmethod
    def from_row(
        row: tuple[Any, ...], fts_match: str | None = None
    ) -> DatenfeldgruppeOut:
        freigabe_status = FreigabeStatus(row[10])

        return DatenfeldgruppeOut(
            namespace=row[0],
            fim_id=row[1],
            fim_version=row[2],
            nummernkreis=row[3],
            xdf_version=XdfVersion(row[4]),
            name=row[5],
            beschreibung=row[6],
            definition=row[7],
            status_gesetzt_durch=row[8],
            bezug=row[9],
            freigabe_status=freigabe_status,
            freigabe_status_label=freigabe_status.to_label(),
            status_gesetzt_am=row[11],
            gueltig_ab=row[12],
            gueltig_bis=row[13],
            versionshinweis=row[14],
            veroeffentlichungsdatum=row[15],
            letzte_aenderung=row[16],
            last_update=row[17],
            is_latest=row[18],
            fts_match=fts_match,
        )


class DatenfeldgruppeOutWithDirectChildren(BaseModel):
    namespace: str
    fim_id: str
    fim_version: str
    nummernkreis: str
    xdf_version: XdfVersion
    name: str
    beschreibung: str | None
    definition: str | None
    freigabe_status: xdf3.FreigabeStatus
    freigabe_status_label: str
    status_gesetzt_durch: str | None
    bezug: list[str]
    children: list[DirectChild]

    @staticmethod
    def map(
        groups: Sequence[DatenfeldgruppeOut],
        group_identifier_to_children: dict[ElementIdentifierTuple, list[DirectChild]],
    ) -> list[DatenfeldgruppeOutWithDirectChildren]:
        return [
            DatenfeldgruppeOutWithDirectChildren.create(
                group,
                group_identifier_to_children[
                    (group.namespace, group.fim_id, group.fim_version)
                ],
            )
            for group in groups
        ]

    @staticmethod
    def create(
        group: DatenfeldgruppeOut, children: list[DirectChild]
    ) -> DatenfeldgruppeOutWithDirectChildren:
        return DatenfeldgruppeOutWithDirectChildren(
            namespace=group.namespace,
            fim_id=group.fim_id,
            fim_version=group.fim_version,
            nummernkreis=group.nummernkreis,
            xdf_version=group.xdf_version,
            name=group.name,
            beschreibung=group.beschreibung,
            definition=group.definition,
            freigabe_status=group.freigabe_status,
            freigabe_status_label=group.freigabe_status.to_label(),
            status_gesetzt_durch=group.status_gesetzt_durch,
            bezug=group.bezug,
            children=children,
        )


class DetailedDatenfeldgruppe(BaseModel):
    """
    Base data for FullDatenfeldgruppeOut.
    """

    namespace: str
    fim_id: str
    fim_version: str
    nummernkreis: str
    xdf_version: XdfVersion
    name: str
    beschreibung: str | None
    definition: str | None
    bezug: list[str]
    freigabe_status: xdf3.FreigabeStatus
    freigabe_status_label: str
    status_gesetzt_am: date | None
    status_gesetzt_durch: str | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    versionshinweis: str | None
    veroeffentlichungsdatum: date | None
    letzte_aenderung: datetime
    last_update: datetime
    is_latest: bool

    regeln: list[RegelReferenz]
    relation: list[RelationOut]

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "namespace",
            "fim_id",
            "fim_version",
            "nummernkreis",
            "xdf_version",
            "name",
            "beschreibung",
            "definition",
            "status_gesetzt_durch",
            "bezug",
            "freigabe_status",
            "status_gesetzt_am",
            "gueltig_ab",
            "gueltig_bis",
            "versionshinweis",
            "veroeffentlichungsdatum",
            "letzte_aenderung",
            "last_update",
            "is_latest",
            """
                array(
                    SELECT ROW(r.regel_fim_id, r.regel_fim_version)
                    FROM datenfeldgruppe_regel_relation r
                    WHERE r.datenfeldgruppe_namespace = namespace
                    AND r.datenfeldgruppe_fim_id = fim_id
                    AND r.datenfeldgruppe_fim_version = fim_version
                ) as regeln
            """,
            """
                array(
                    SELECT ROW(rel.praedikat, rel.target_fim_id, rel.target_fim_version, g.name)
                    FROM datenfeldgruppe_relations rel
                    LEFT OUTER JOIN datenfeldgruppe g
                    ON rel.target_fim_id = g.fim_id
                    AND (
                        (rel.target_fim_version IS NOT NULL AND rel.target_fim_version = g.fim_version)
                        OR
                        (rel.target_fim_version IS NULL AND g.is_latest)
                    )
                    WHERE rel.source_fim_id = datenfeldgruppe.fim_id
                    AND rel.source_namespace = datenfeldgruppe.namespace
                    AND rel.source_fim_version = datenfeldgruppe.fim_version
                ) as relations
            """,
        ]
    )

    @staticmethod
    def from_row(row: tuple[Any, ...]) -> DetailedDatenfeldgruppe:
        freigabe_status = FreigabeStatus(row[10])

        return DetailedDatenfeldgruppe(
            namespace=row[0],
            fim_id=row[1],
            fim_version=row[2],
            nummernkreis=row[3],
            xdf_version=XdfVersion(row[4]),
            name=row[5],
            beschreibung=row[6],
            definition=row[7],
            status_gesetzt_durch=row[8],
            bezug=row[9],
            freigabe_status=freigabe_status,
            freigabe_status_label=freigabe_status.to_label(),
            status_gesetzt_am=row[11],
            gueltig_ab=row[12],
            gueltig_bis=row[13],
            versionshinweis=row[14],
            veroeffentlichungsdatum=row[15],
            letzte_aenderung=row[16],
            last_update=row[17],
            is_latest=row[18],
            regeln=[RegelReferenz.from_row(r) for r in row[19]],
            relation=[RelationOut.from_row(r) for r in row[20]],
        )


class FullDatenfeldgruppeOut(BaseModel):
    namespace: str
    fim_id: str
    fim_version: str
    nummernkreis: str
    xdf_version: XdfVersion
    name: str
    beschreibung: str | None
    definition: str | None
    bezug: list[str]
    freigabe_status: xdf3.FreigabeStatus
    freigabe_status_label: str
    status_gesetzt_am: date | None
    status_gesetzt_durch: str | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    versionshinweis: str | None
    veroeffentlichungsdatum: date | None
    letzte_aenderung: datetime
    last_update: datetime
    is_latest: bool

    regeln: list[RegelReferenz]
    schemas: list[SchemaOut]
    relation: list[RelationOut]

    datenfelder: list[DatenfeldOut]
    datenfeldgruppen: list[DatenfeldgruppeOutWithDirectChildren]
    children: list[DirectChild]

    @staticmethod
    def create(
        group: DetailedDatenfeldgruppe,
        schemas: list[SchemaOut],
        fields: list[DatenfeldOut],
        groups: list[DatenfeldgruppeOutWithDirectChildren],
        children: list[DirectChild],
    ) -> FullDatenfeldgruppeOut:
        return FullDatenfeldgruppeOut(
            namespace=group.namespace,
            fim_id=group.fim_id,
            fim_version=group.fim_version,
            nummernkreis=group.nummernkreis,
            xdf_version=group.xdf_version,
            name=group.name,
            beschreibung=group.beschreibung,
            definition=group.definition,
            bezug=group.bezug,
            freigabe_status=group.freigabe_status,
            freigabe_status_label=group.freigabe_status_label,
            status_gesetzt_am=group.status_gesetzt_am,
            status_gesetzt_durch=group.status_gesetzt_durch,
            gueltig_ab=group.gueltig_ab,
            gueltig_bis=group.gueltig_bis,
            versionshinweis=group.versionshinweis,
            veroeffentlichungsdatum=group.veroeffentlichungsdatum,
            letzte_aenderung=group.letzte_aenderung,
            last_update=group.last_update,
            is_latest=group.is_latest,
            regeln=group.regeln,
            schemas=schemas,
            datenfelder=fields,
            datenfeldgruppen=groups,
            children=children,
            relation=group.relation,
        )


class RuleOut(BaseModel):
    fim_id: str
    fim_version: str
    nummernkreis: str

    name: str
    letzte_aenderung: datetime
    last_update: datetime

    xdf_version: XdfVersion

    @staticmethod
    def from_row(row: tuple[Any, ...]) -> RuleOut:
        return RuleOut(
            fim_id=row[0],
            fim_version=row[1],
            nummernkreis=row[2],
            name=row[3],
            letzte_aenderung=row[4],
            last_update=row[5],
            xdf_version=XdfVersion(row[6]),
        )


class CodeListOut(BaseModel):
    id: int
    url: str | None
    genericode_canonical_version_uri: str
    genericode_canonical_uri: str
    genericode_version: str | None
    is_external: bool

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "id",
            "genericode_canonical_version_uri",
            "genericode_canonical_uri",
            "genericode_version",
            "is_external",
        ]
    )

    @staticmethod
    def from_row(
        row: tuple[Any, ...],
        immutable_base_url: str,
        fts_match: str | None,
    ) -> CodeListOut:
        id = row[0]
        is_external = row[4]

        if is_external:
            url = None
        else:
            url = f"{immutable_base_url}/immutable/code-lists/{id}/genericode.xml"

        return CodeListOut(
            id=id,
            url=url,
            genericode_canonical_version_uri=row[1],
            genericode_canonical_uri=row[2],
            genericode_version=row[3],
            is_external=is_external,
        )


class SchemaUpload(BaseModel):
    filename: str
    url: str
    upload_time: datetime
    code_lists: list[CodeListOut]

    @staticmethod
    def from_row(row: tuple[Any, ...], immutable_base_url: str) -> SchemaUpload:
        return SchemaUpload(
            filename=row[0],
            url=f"{immutable_base_url}/immutable/schemas/{row[0]}",
            upload_time=row[1],
            code_lists=[
                CodeListOut.from_row(
                    r, immutable_base_url=immutable_base_url, fts_match=None
                )
                for r in row[2]
            ],
        )


class JsonSchemaFile(BaseModel):
    filename: str
    url: str
    upload_time: datetime
    generated_from: str

    @staticmethod
    def from_row(row: tuple[Any, ...], immutable_base_url: str) -> JsonSchemaFile:
        return JsonSchemaFile(
            filename=row[0],
            url=f"{immutable_base_url}/immutable/schemas/{row[0]}",
            upload_time=row[1],
            generated_from=row[2],
        )


class XsdFile(BaseModel):
    filename: str
    url: str
    upload_time: datetime
    generated_from: str

    @staticmethod
    def from_row(row: tuple[Any, ...], immutable_base_url: str) -> XsdFile:
        return XsdFile(
            filename=row[0],
            url=f"{immutable_base_url}/immutable/schemas/{row[0]}",
            upload_time=row[1],
            generated_from=row[2],
        )


class RegelReferenz(BaseModel):
    fim_id: str
    fim_version: str

    @staticmethod
    def from_row(row: tuple[Any, ...]) -> RegelReferenz:
        return RegelReferenz(
            fim_id=row[0],
            fim_version=row[1],
        )


class DetailedSchema(BaseModel):
    """
    The base of the data in FullSchemaOut.
    """

    fim_id: str
    fim_version: str
    nummernkreis: str
    name: str
    beschreibung: str | None
    definition: str | None
    freigabe_status: xdf3.FreigabeStatus
    freigabe_status_label: str
    status_gesetzt_am: date | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    status_gesetzt_durch: str | None
    steckbrief_id: str | None
    xdf_version: XdfVersion
    bezug: list[str]
    versionshinweis: str | None
    stichwort: list[str]
    letzte_aenderung: datetime
    last_update: datetime
    bezeichnung: str | None
    bezug_components: list[str]
    veroeffentlichungsdatum: date | None
    is_latest: bool
    steckbrief_name: str | None

    regeln: list[RegelReferenz]
    uploads: list[SchemaUpload]
    json_schema_files: list[JsonSchemaFile]
    xsd_files: list[XsdFile]
    relation: list[RelationOut]

    SQL_COLUMNS: ClassVar[str] = ", ".join(
        [
            "fim_id",
            "fim_version",
            "nummernkreis",
            "name",
            "beschreibung",
            "definition",
            "bezug",
            "freigabe_status",
            "status_gesetzt_durch",
            "gueltig_ab",
            "gueltig_bis",
            "versionshinweis",
            "letzte_aenderung",
            "stichwort",
            "steckbrief_id",
            "xdf_version",
            "last_update",
            "bezeichnung",
            "status_gesetzt_am",
            "bezug_components",
            "veroeffentlichungsdatum",
            "is_latest",
            """
                (
                    SELECT d.name
                    FROM dokumentensteckbrief d
                    WHERE d.fim_id = steckbrief_id
                    AND d.is_latest = true
                    LIMIT 1
                ) as steckbrief_name
            """,
            """
                array(
                    SELECT ROW(r.regel_fim_id, r.regel_fim_version)
                    FROM schema_regel_relation r
                    WHERE r.schema_fim_id = fim_id
                    AND r.schema_fim_version = fim_version
                ) as regeln
            """,
            """
                array(
                    SELECT ROW(
                        f.filename,
                        f.created_at,
                        array(
                            SELECT ROW(c.id, c.genericode_canonical_version_uri, c.genericode_canonical_uri, c.genericode_version, c.is_external)
                            FROM schema_file_code_list 
                            JOIN code_list c
                            ON schema_file_code_list.code_list_id = c.id
                            WHERE schema_file_code_list.schema_filename = f.filename
                        )
                    )
                    FROM schema_file f
                    WHERE f.schema_fim_id = fim_id
                    AND f.schema_fim_version = fim_version
                    ORDER BY created_at DESC
                ) as uploads
            """,
            """
                array(
                    SELECT ROW(json.filename, json.created_at, json.generated_from)
                    FROM json_schema_file json
                    JOIN schema_file f
                    ON json.generated_from = f.filename
                    WHERE f.schema_fim_id = fim_id
                    AND f.schema_fim_version = fim_version
                    ORDER BY json.created_at DESC
                ) as json_schema_files
            """,
            """
                array(
                    SELECT ROW(xsd.filename, xsd.created_at, xsd.generated_from)
                    FROM xsd_file xsd
                    JOIN schema_file f
                    ON xsd.generated_from = f.filename
                    WHERE f.schema_fim_id = fim_id
                    AND f.schema_fim_version = fim_version
                    ORDER BY xsd.created_at DESC
                ) as xsd_files
            """,
            """
                array(
                    SELECT ROW(rel.praedikat, rel.target_fim_id, rel.target_fim_version, s.name)
                    FROM schema_relations rel
                    LEFT OUTER JOIN schema s
                    ON rel.target_fim_id = s.fim_id
                    AND (
                        (rel.target_fim_version IS NOT NULL AND rel.target_fim_version = s.fim_version)
                        OR
                        (rel.target_fim_version IS NULL AND s.is_latest)
                    )
                    WHERE rel.source_fim_id = schema.fim_id
                    AND rel.source_fim_version = schema.fim_version
                ) as relations
            """,
        ]
    )

    @staticmethod
    def from_row(row: tuple[Any, ...], immutable_base_url: str) -> DetailedSchema:
        freigabe_status = FreigabeStatus(row[7])

        return DetailedSchema(
            fim_id=row[0],
            fim_version=row[1],
            nummernkreis=row[2],
            name=row[3],
            beschreibung=row[4],
            definition=row[5],
            bezug=row[6],
            freigabe_status=freigabe_status,
            freigabe_status_label=freigabe_status.to_label(),
            status_gesetzt_durch=row[8],
            gueltig_ab=row[9],
            gueltig_bis=row[10],
            versionshinweis=row[11],
            letzte_aenderung=row[12],
            stichwort=row[13],
            steckbrief_id=row[14],
            xdf_version=XdfVersion(row[15]),
            last_update=row[16],
            bezeichnung=row[17],
            status_gesetzt_am=row[18],
            bezug_components=row[19],
            veroeffentlichungsdatum=row[20],
            is_latest=row[21],
            steckbrief_name=row[22],
            regeln=[RegelReferenz.from_row(r) for r in row[23]],
            uploads=[SchemaUpload.from_row(r, immutable_base_url) for r in row[24]],
            json_schema_files=[
                JsonSchemaFile.from_row(r, immutable_base_url) for r in row[25]
            ],
            xsd_files=[XsdFile.from_row(r, immutable_base_url) for r in row[26]],
            relation=[RelationOut.from_row(r) for r in row[27]],
        )


class FullSchemaOut(BaseModel):
    fim_id: str
    fim_version: str
    nummernkreis: str
    name: str
    beschreibung: str | None
    definition: str | None
    freigabe_status: xdf3.FreigabeStatus
    freigabe_status_label: str
    status_gesetzt_am: date | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    status_gesetzt_durch: str | None
    bezug: list[str]
    versionshinweis: str | None
    veroeffentlichungsdatum: date | None
    stichwort: list[str]
    bezeichnung: str | None
    letzte_aenderung: datetime
    last_update: datetime
    is_latest: bool

    steckbrief_id: str | None
    steckbrief_name: str | None

    regeln: list[RegelReferenz]

    xdf_version: XdfVersion

    datenfelder: list[DatenfeldOut]
    datenfeldgruppen: list[DatenfeldgruppeOutWithDirectChildren]
    children: list[DirectChild]
    relation: list[RelationOut]

    uploads: list[SchemaUpload]
    json_schema_files: list[JsonSchemaFile]
    xsd_files: list[XsdFile]

    @staticmethod
    def from_model(
        schema: DetailedSchema,
        fields: list[DatenfeldOut],
        groups: list[DatenfeldgruppeOutWithDirectChildren],
        children: list[DirectChild],
    ) -> FullSchemaOut:
        return FullSchemaOut(
            fim_id=schema.fim_id,
            fim_version=schema.fim_version,
            nummernkreis=schema.nummernkreis,
            name=schema.name,
            beschreibung=schema.beschreibung,
            definition=schema.definition,
            freigabe_status=schema.freigabe_status,
            freigabe_status_label=schema.freigabe_status_label,
            status_gesetzt_am=schema.status_gesetzt_am,
            gueltig_ab=schema.gueltig_ab,
            gueltig_bis=schema.gueltig_bis,
            status_gesetzt_durch=schema.status_gesetzt_durch,
            bezug=schema.bezug,
            bezeichnung=schema.bezeichnung,
            versionshinweis=schema.versionshinweis,
            veroeffentlichungsdatum=schema.veroeffentlichungsdatum,
            stichwort=schema.stichwort,
            letzte_aenderung=schema.letzte_aenderung,
            last_update=schema.last_update,
            steckbrief_id=schema.steckbrief_id,
            steckbrief_name=schema.steckbrief_name,
            regeln=schema.regeln,
            datenfelder=fields,
            datenfeldgruppen=groups,
            children=children,
            uploads=schema.uploads,
            json_schema_files=schema.json_schema_files,
            xsd_files=schema.xsd_files,
            xdf_version=schema.xdf_version,
            is_latest=schema.is_latest,
            relation=schema.relation,
        )


class JsonLegend(BaseModel):
    groups: dict[str, DatenfeldgruppeOut | None]
    fields: dict[str, DatenfeldOut | None]


T = TypeVar("T", bound=DatenfeldOut | DatenfeldgruppeOut)


def map_identifier_to_element(elements: list[T]) -> dict[tuple[str, str], T]:
    """
    Create a map from the full identifier (fim_id and fim_version) to the element.
    This will also add (fim_id, "latest") as an additional identifier to the latest element
    for each fim_id.
    """
    identifier_to_element = {}

    fim_id_to_versions: dict[str, list[str]] = defaultdict(list)
    for element in elements:
        identifier_to_element[(element.fim_id, element.fim_version)] = element
        fim_id_to_versions[element.fim_id].append(element.fim_version)

    for fim_id, versions in fim_id_to_versions.items():
        latest_version = get_latest_version(versions)
        identifier_to_element[(fim_id, "latest")] = identifier_to_element[
            (fim_id, latest_version)
        ]

    return identifier_to_element
