from dataclasses import dataclass
from typing import Literal


KatalogSource = Literal["XZUFI_KATALOG"]


@dataclass(slots=True)
class NewKatalog:
    source: KatalogSource
    filename: str
    content: bytes
