"""Define the input that the UI Prototype jinja template expects."""

from __future__ import annotations

from dataclasses import dataclass
from typing import Any, Mapping

from fimportal.helpers import remove_whitespace
from fimportal.models.imports import ChildType
from fimportal.models.xdf3 import (
    DatenfeldgruppeOut,
    DatenfeldgruppeOutWithDirectChildren,
    DatenfeldOut,
    DirectChild,
    FullDatenfeldgruppeOut,
    FullSchemaOut,
    SchemaOut,
    SteckbriefOut,
)
from fimportal.xdatenfelder import xdf2, xdf3
from fimportal.xdatenfelder.checks import FailingCheck
from fimportal.xdatenfelder.reports import QualityReport


@dataclass(slots=True)
class SchemaTree:
    """
    The main data structure holding the necessary information to render the schema tree.
    This includes basic metadata per element as well as a summary of the quality report
    per element.
    """

    fim_id: str
    fim_version: str
    name: str
    freigabe_status: str
    schema_errors: int
    schema_warnings: int
    children: list[TreeChild]

    @staticmethod
    def create(schema: FullSchemaOut) -> SchemaTree:
        identifier_to_group = {
            (group.fim_id, group.fim_version): group
            for group in schema.datenfeldgruppen
        }
        identifier_to_field = {
            (field.fim_id, field.fim_version): field for field in schema.datenfelder
        }

        return SchemaTree(
            fim_id=schema.fim_id,
            fim_version=schema.fim_version,
            name=schema.name,
            freigabe_status=schema.freigabe_status_label,
            schema_errors=0,
            schema_warnings=0,
            children=TreeChild.from_direct_children(
                schema.children,
                None,
                identifier_to_group,
                identifier_to_field,
            ),
        )


_ElementIdentifier = tuple[str, str]  # fim_id, fim_version


@dataclass(slots=True)
class SchemaTreeQualityData:
    """
    Preprocessed quality report to inclusion in the `SchemaTree`.
    """

    schema_errors: int
    schema_warnings: int

    identifier_to_group: dict[tuple[str, str | None], tuple[int, int]]
    identifier_to_field: dict[tuple[str, str | None], tuple[int, int]]

    @staticmethod
    def from_quality_report(report: QualityReport) -> SchemaTreeQualityData:
        schema_errors, schema_warnings = _summarize_error_types(report.schema_checks)

        identifier_to_group = {
            (
                item.identifier.id,
                _str_or_none(item.identifier.version),
            ): _summarize_error_types(item.failing_checks)
            for item in report.group_reports
        }

        identifier_to_field = {
            (
                item.identifier.id,
                _str_or_none(item.identifier.version),
            ): _summarize_error_types(item.failing_checks)
            for item in report.field_reports
        }

        return SchemaTreeQualityData(
            schema_errors=schema_errors,
            schema_warnings=schema_warnings,
            identifier_to_group=identifier_to_group,
            identifier_to_field=identifier_to_field,
        )


def _str_or_none(value: Any | None) -> str | None:
    if value is None:
        return value

    return str(value)


def _summarize_error_types(checks: list[FailingCheck]) -> tuple[int, int]:
    errors = 0
    warnings = 0

    for check in checks:
        if check.error_type == "critical":
            errors += 1
        elif check.error_type == "method":
            errors += 1
        elif check.error_type == "warning":
            warnings += 1

    return errors, warnings


@dataclass(slots=True)
class TreeChild:
    anzahl: str
    bezug: list[str]
    element: TreeField | TreeGroup

    def get_anzahl_label(self) -> str | None:
        if self.anzahl == "1:1":
            return None
        elif self.anzahl == "0:1":
            return "Optional (0:1)"
        elif self.anzahl == "1:*":
            return "Mindestens einmal (1:*)"
        elif self.anzahl == "0:*":
            return "Beliebig häufig (0:*)"
        else:
            return f"Mehrmals ({self.anzahl})"

    @staticmethod
    def from_direct_children(
        children: list[DirectChild],
        report: SchemaTreeQualityData | None,
        identifier_to_group: dict[
            _ElementIdentifier, DatenfeldgruppeOutWithDirectChildren
        ],
        identifier_to_field: dict[_ElementIdentifier, DatenfeldOut],
    ) -> list[TreeChild]:
        tree_children = []

        for child in children:
            identifier = (child.fim_id, child.fim_version)

            if child.type == ChildType.GROUP:
                group = identifier_to_group[identifier]

                tree_children.append(
                    TreeChild(
                        anzahl=child.anzahl,
                        bezug=remove_whitespace(child.bezug),
                        element=TreeGroup.from_group(
                            group,
                            report,
                            identifier_to_group,
                            identifier_to_field,
                        ),
                    )
                )
            else:
                field = identifier_to_field[identifier]

                tree_children.append(
                    TreeChild(
                        anzahl=child.anzahl,
                        bezug=remove_whitespace(child.bezug),
                        element=TreeField.from_field(
                            field,
                            report,
                        ),
                    )
                )

        return tree_children


@dataclass(slots=True)
class TreeField:
    child_type = "field"
    namespace: str
    fim_id: str
    fim_version: str
    name: str
    freigabe_status: str
    is_bob: bool

    href: str
    total_errors: int
    total_warnings: int

    @staticmethod
    def from_field(
        field: DatenfeldOut, report: SchemaTreeQualityData | None
    ) -> TreeField:
        if report is not None:
            total_errors, total_warnings = report.identifier_to_field.get(
                (field.fim_id, field.fim_version), (0, 0)
            )
        else:
            total_errors = 0
            total_warnings = 0

        return TreeField(
            namespace=field.name,
            fim_id=field.fim_id,
            fim_version=field.fim_version,
            name=field.name,
            freigabe_status=field.freigabe_status_label,
            is_bob=field.nummernkreis == "60000"
            and field.freigabe_status == xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
            href=f"/fields/{field.namespace}/{field.fim_id}/{field.fim_version}",
            total_errors=total_errors,
            total_warnings=total_warnings,
        )


@dataclass(slots=True)
class TreeGroup:
    child_type = "group"
    namespace: str
    fim_id: str
    fim_version: str
    name: str
    freigabe_status: str
    is_bob: bool
    children: list[TreeChild]

    href: str
    total_errors: int
    total_warnings: int

    @staticmethod
    def create(group: FullDatenfeldgruppeOut) -> TreeGroup:
        identifier_to_group = {
            (group.fim_id, group.fim_version): group for group in group.datenfeldgruppen
        }
        identifier_to_field = {
            (field.fim_id, field.fim_version): field for field in group.datenfelder
        }

        return TreeGroup(
            namespace=group.name,
            fim_id=group.fim_id,
            fim_version=group.fim_version,
            name=group.name,
            freigabe_status=group.freigabe_status_label,
            is_bob=group.nummernkreis == "60000"
            and group.freigabe_status == xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
            children=TreeChild.from_direct_children(
                group.children,
                None,
                identifier_to_group,
                identifier_to_field,
            ),
            href=f"/groups/{group.namespace}/{group.fim_id}/{group.fim_version}",
            total_errors=0,
            total_warnings=0,
        )

    @staticmethod
    def from_group(
        group: DatenfeldgruppeOutWithDirectChildren,
        report: SchemaTreeQualityData | None,
        identifier_to_group: dict[
            _ElementIdentifier, DatenfeldgruppeOutWithDirectChildren
        ],
        identifier_to_field: dict[_ElementIdentifier, DatenfeldOut],
    ) -> TreeGroup:
        if report is not None:
            total_errors, total_warnings = report.identifier_to_group.get(
                (group.fim_id, group.fim_version), (0, 0)
            )
        else:
            total_errors = 0
            total_warnings = 0

        return TreeGroup(
            namespace=group.name,
            fim_id=group.fim_id,
            fim_version=group.fim_version,
            name=group.name,
            freigabe_status=group.freigabe_status_label,
            is_bob=group.nummernkreis == "60000"
            and group.freigabe_status == xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
            children=TreeChild.from_direct_children(
                group.children,
                report,
                identifier_to_group,
                identifier_to_field,
            ),
            href=f"/groups/{group.namespace}/{group.fim_id}/{group.fim_version}",
            total_errors=total_errors,
            total_warnings=total_warnings,
        )


@dataclass(slots=True)
class AvailableVersion:
    is_active: bool
    label: str
    freigabe_status: str
    xdf_version: str
    link: str

    @staticmethod
    def from_steckbrief(
        steckbrief: SteckbriefOut, current_version: str, search_query_string: str
    ) -> AvailableVersion:
        return AvailableVersion(
            is_active=steckbrief.fim_version == current_version,
            label=f"Version {steckbrief.fim_version}",
            freigabe_status=steckbrief.freigabe_status_label,
            xdf_version=f"xdf {steckbrief.xdf_version.value}",
            link=f"/document-profiles/{steckbrief.fim_id}/{steckbrief.fim_version}{search_query_string}",
        )

    @staticmethod
    def from_schema(
        schema: SchemaOut, current_version: str, search_query_string: str
    ) -> AvailableVersion:
        return AvailableVersion(
            is_active=schema.fim_version == current_version,
            label="Version " + schema.fim_version,
            freigabe_status=schema.freigabe_status_label,
            xdf_version="xdf " + schema.xdf_version.value,
            link=f"/schemas/{schema.fim_id}/{schema.fim_version}{search_query_string}",
        )

    @staticmethod
    def from_group(
        group: DatenfeldgruppeOut, current_version: str, search_query_string: str
    ) -> AvailableVersion:
        return AvailableVersion(
            is_active=group.fim_version == current_version,
            label="Version " + group.fim_version,
            freigabe_status=group.freigabe_status_label,
            xdf_version=group.xdf_version.value,
            link=f"/groups/{group.namespace}/{group.fim_id}/{group.fim_version}{search_query_string}",
        )

    @staticmethod
    def from_field(
        field: DatenfeldOut, current_version: str, search_query_string: str
    ) -> AvailableVersion:
        return AvailableVersion(
            is_active=field.fim_version == current_version,
            label="Version " + field.fim_version,
            freigabe_status=field.freigabe_status_label,
            xdf_version="xdf " + field.xdf_version.value,
            link=f"/fields/{field.namespace}/{field.fim_id}/{field.fim_version}{search_query_string}",
        )


@dataclass(slots=True)
class SchemaReference:
    fim_id: str
    fim_version: str
    name: str
    freigabe_status: str
    xdf_version: str
    uri: str

    @staticmethod
    def from_schema_out(schema: SchemaOut) -> SchemaReference:
        return SchemaReference(
            fim_id=schema.fim_id,
            fim_version="Version " + schema.fim_version,
            name=schema.name,
            freigabe_status=schema.freigabe_status_label,
            xdf_version="XDatenfelder " + schema.xdf_version.value,
            uri=f"/schemas/{schema.fim_id}/{schema.fim_version}",
        )


@dataclass(slots=True)
class FailedCheckView:
    code: int
    message: str


@dataclass(slots=True)
class FailedChecksView:
    critical: list[FailedCheckView]
    method: list[FailedCheckView]
    warning: list[FailedCheckView]
    info: list[FailedCheckView]

    @staticmethod
    def from_failing_checks(failing_checks: list[FailingCheck]) -> FailedChecksView:
        return FailedChecksView(
            info=[
                FailedCheckView(code=quality_check.code, message=quality_check.message)
                for quality_check in failing_checks
                if quality_check.error_type == "info"
            ],
            warning=[
                FailedCheckView(code=quality_check.code, message=quality_check.message)
                for quality_check in failing_checks
                if quality_check.error_type == "warning"
            ],
            method=[
                FailedCheckView(code=quality_check.code, message=quality_check.message)
                for quality_check in failing_checks
                if quality_check.error_type == "method"
            ],
            critical=[
                FailedCheckView(code=quality_check.code, message=quality_check.message)
                for quality_check in failing_checks
                if quality_check.error_type == "critical"
            ],
        )


@dataclass(slots=True)
class ElementReportView(FailedChecksView):
    name: str
    fim_id: str
    fim_version: str | None
    freigabe_status: str | None

    href: str

    critical: list[FailedCheckView]
    method: list[FailedCheckView]
    warning: list[FailedCheckView]
    info: list[FailedCheckView]

    @staticmethod
    def create(
        name: str,
        fim_id: str,
        fim_version: str | None,
        freigabe_status: str | None,
        href: str,
        checks: list[FailingCheck],
    ) -> ElementReportView:
        return ElementReportView(
            name=name,
            fim_id=fim_id,
            fim_version=fim_version,
            freigabe_status=freigabe_status,
            href=href,
            critical=[
                FailedCheckView(code=quality_check.code, message=quality_check.message)
                for quality_check in checks
                if quality_check.error_type == "critical"
            ],
            method=[
                FailedCheckView(code=quality_check.code, message=quality_check.message)
                for quality_check in checks
                if quality_check.error_type == "method"
            ],
            warning=[
                FailedCheckView(code=quality_check.code, message=quality_check.message)
                for quality_check in checks
                if quality_check.error_type == "warning"
            ],
            info=[
                FailedCheckView(code=quality_check.code, message=quality_check.message)
                for quality_check in checks
                if quality_check.error_type == "info"
            ],
        )


@dataclass(slots=True)
class QualityReportView:
    schema_report: ElementReportView | None
    group_reports: list[ElementReportView]
    field_reports: list[ElementReportView]

    @staticmethod
    def from_quality_report(
        schema_fim_id: str,
        schema_fim_version: str | None,
        schema_name: str,
        schema_freigabe_status: xdf3.FreigabeStatus | None,
        quality_report: QualityReport,
        element_mapping: Mapping[
            xdf2.Identifier | xdf3.Identifier, tuple[str, xdf3.FreigabeStatus | None]
        ],
    ) -> QualityReportView:
        if len(quality_report.schema_checks) > 0:
            schema_report = ElementReportView.create(
                name=schema_name,
                fim_id=schema_fim_id,
                fim_version=schema_fim_version,
                freigabe_status=(
                    schema_freigabe_status.to_label()
                    if schema_freigabe_status is not None
                    else None
                ),
                href=f"/schemas/{schema_fim_id}/{schema_fim_version}",
                checks=quality_report.schema_checks,
            )
        else:
            schema_report = None

        group_reports = []
        for group_report in quality_report.group_reports:
            if len(group_report.failing_checks) > 0:
                fim_id = group_report.identifier.id
                fim_version = group_report.identifier.version
                name, freigabe_status = element_mapping[group_report.identifier]

                if group_report.identifier.is_local():
                    namespace = schema_fim_id
                else:
                    namespace = "baukasten"

                group_reports.append(
                    ElementReportView.create(
                        name=name,
                        fim_id=fim_id,
                        fim_version=fim_version,
                        freigabe_status=(
                            freigabe_status.to_label()
                            if freigabe_status is not None
                            else None
                        ),
                        href=f"/groups/{namespace}/{fim_id}/{fim_version}",
                        checks=group_report.failing_checks,
                    )
                )

        field_reports = []
        for field_report in quality_report.field_reports:
            if len(field_report.failing_checks) > 0:
                fim_id = field_report.identifier.id
                fim_version = field_report.identifier.version
                name, freigabe_status = element_mapping[field_report.identifier]

                if field_report.identifier.is_local():
                    namespace = schema_fim_id
                else:
                    namespace = "baukasten"

                field_reports.append(
                    ElementReportView.create(
                        name=name,
                        fim_id=fim_id,
                        fim_version=fim_version,
                        freigabe_status=(
                            freigabe_status.to_label()
                            if freigabe_status is not None
                            else None
                        ),
                        href=f"/fields/{namespace}/{fim_id}/{fim_version}",
                        checks=field_report.failing_checks,
                    )
                )

        return QualityReportView(
            schema_report=schema_report,
            group_reports=group_reports,
            field_reports=field_reports,
        )
