from dataclasses import dataclass

from fimportal.xdatenfelder.common import get_nummerkreis_description


@dataclass(slots=True)
class ApiTokenModel:
    id: int
    token: str
    nummernkreis: str
    description: str

    def get_nummernkreis_description(self) -> str:
        return get_nummerkreis_description(self.nummernkreis)
