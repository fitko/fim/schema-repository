"""
Plain dataclasses for all the data necessary to import a new schema, regardless of the underlying schema version.
This way, the same logic can be reused to import both xdf2 and xdf3 after a simple mapping to these classes.
"""

from __future__ import annotations

from dataclasses import dataclass
from dataclasses import field as dataclass_field
from datetime import date, datetime
from enum import Enum
from itertools import chain
from typing import Any

from fimportal import genericode
from fimportal.errors import ImportException
from fimportal.fts.xdf2 import create_fts_doc as create_xdf2_fts_doc
from fimportal.fts.xdf3 import create_fts_doc as create_xdf3_fts_doc
from fimportal.genericode.code_list.parse import parse_code_list
from fimportal.helpers import generate_json, utc_now
from fimportal.xdatenfelder import xdf2, xdf2_to_xdf3, xdf3
from fimportal.xdatenfelder.common import XdfException
from fimportal.xdatenfelder.reports import (
    QualityReport,
    check_xdf2_quality,
    check_xdf3_quality,
)
from fimportal.xdatenfelder.xdf2_to_xdf3 import map_status
from fimportal.xdatenfelder.xdf3.common import Relation
from fimportal.xsd import xml_to_str


class XdfVersion(Enum):
    XDF2 = "2.0"
    XDF3 = "3.0.0"


@dataclass(slots=True)
class CodeListImport:
    """
    Holds all of the imported data for a code list.
    """

    code_list_text: str
    code_list: genericode.CodeList

    @staticmethod
    def from_bytes(data: bytes) -> CodeListImport:
        try:
            code_list = genericode.parse_code_list(data)
        except genericode.GenericodeException as error:
            raise ImportException(str(error)) from error

        return CodeListImport(
            code_list_text=data.decode("utf-8"),
            code_list=code_list,
        )

    @property
    def identifier(self) -> genericode.Identifier:
        return self.code_list.identifier


def deduplicate_code_list_imports(
    code_list_imports: list[CodeListImport],
) -> list[CodeListImport]:
    """
    Some code lists with an identical canonical_version_uri are imported twice with different
    FIM Ids.

    This removes any duplicate code lists and checks, whether two duplicates are actually the same.
    """

    code_list_import_map: dict[genericode.Identifier, CodeListImport] = {}
    for code_list_import in code_list_imports:
        existing_code_list_import = code_list_import_map.get(
            code_list_import.identifier
        )

        if existing_code_list_import is not None:
            if (
                existing_code_list_import.code_list_text
                != code_list_import.code_list_text
            ):
                raise ImportException(
                    f"Could not import schema: inconsistent code list data [canonical_version_uri={code_list_import.identifier.canonical_version_uri}]"
                )
        else:
            code_list_import_map[code_list_import.identifier] = code_list_import

    return list(code_list_import_map.values())


@dataclass(slots=True)
class Xdf2SteckbriefImport:
    freigabe_status: xdf3.FreigabeStatus
    steckbrief: xdf2.Steckbrief
    letzte_aenderung: datetime
    xml_content: str

    @property
    def id(self) -> str:
        return self.steckbrief.identifier.id

    @staticmethod
    def from_bytes(
        steckbrief_data: bytes,
        freigabe_status: xdf3.FreigabeStatus,
    ):
        try:
            message = xdf2.parse_steckbrief_message(steckbrief_data)
        except XdfException as ex:
            raise ImportException(str(ex)) from ex

        if message.steckbrief.identifier.version is None:
            raise ImportException(
                f"Cannot import steckbrief without a version [id={message.steckbrief.identifier.id}]"
            )

        return Xdf2SteckbriefImport(
            steckbrief=message.steckbrief,
            freigabe_status=freigabe_status,
            letzte_aenderung=message.header.erstellungs_zeitpunkt,
            xml_content=steckbrief_data.decode("utf-8"),
        )

    @property
    def version(self) -> xdf2.Version:
        # This is safe, as we satisfy in the constructor that the version cannot be empty.
        return self.steckbrief.identifier.assert_version()


# NOTE(Felix): This should be moved elsewhere is now only here for consistency.
# However, this is never used in the actual crawler.
@dataclass(slots=True)
class Xdf3SteckbriefImport:
    steckbrief: xdf3.Steckbrief
    xml_content: str

    @property
    def id(self) -> str:
        return self.steckbrief.identifier.id

    @staticmethod
    def from_bytes(steckbrief_data: bytes):
        try:
            message = xdf3.parse_steckbrief_message(steckbrief_data)
        except XdfException as ex:
            raise ImportException(str(ex)) from ex

        if message.steckbrief.identifier.version is None:
            raise ImportException(
                f"Cannot import steckbrief without a version [id={message.steckbrief.identifier.id}]"
            )

        return Xdf3SteckbriefImport(
            steckbrief=message.steckbrief,
            xml_content=steckbrief_data.decode("utf-8"),
        )

    @property
    def version(self) -> xdf3.Version:
        # This is safe, as we satisfy in the constructor that the version cannot be empty.
        return self.steckbrief.identifier.assert_version()


@dataclass(slots=True)
class Xdf2SchemaImport:
    """
    Holds all of the imported data for a schema.
    """

    schema_text: str
    schema_message: xdf2.SchemaMessage
    freigabe_status: xdf3.FreigabeStatus
    steckbrief_id: str | None

    code_list_imports: list[CodeListImport]

    group_contents: dict[xdf2.Identifier, str] = dataclass_field(init=False)
    field_contents: dict[xdf2.Identifier, str] = dataclass_field(init=False)
    rule_contents: dict[xdf2.Identifier, str] = dataclass_field(init=False)

    def __post_init__(self):
        # Sanity check, that all of the imported code lists are also actually referenced in
        # the schema message
        referenced_code_lists = self.schema_message.get_code_list_identifiers()

        field_contents: dict[xdf2.Identifier, str] = {}
        for field_id in self.schema_message.fields.keys():
            field = self.schema_message.fields[field_id]
            field_contents[field_id] = xml_to_str(
                xdf2.serialize_field(
                    field,
                    rule_map=self.schema_message.rules,
                )
            )
        self.field_contents = field_contents

        group_contents: dict[xdf2.Identifier, str] = {}
        for group_id in self.schema_message.groups.keys():
            group = self.schema_message.groups[group_id]
            group_contents[group_id] = xml_to_str(
                xdf2.serialize_group(
                    group,
                    group_map=self.schema_message.groups,
                    field_map=self.schema_message.fields,
                    rule_map=self.schema_message.rules,
                )
            )
        self.group_contents = group_contents

        rule_contents: dict[xdf2.Identifier, str] = {}
        for rule_id in self.schema_message.rules.keys():
            rule = self.schema_message.rules[rule_id]
            rule_contents[rule_id] = xml_to_str(xdf2.serialize_rule(rule))
        self.rule_contents = rule_contents

        for code_list_import in self.code_list_imports:
            if code_list_import.code_list.identifier not in referenced_code_lists:
                raise ImportException(
                    f"Imported CodeList {code_list_import.code_list.identifier.canonical_version_uri} not referenced in schema {self.schema_message.id}V{self.schema_message.version}"
                )

    @staticmethod
    def from_message(message: xdf2.SchemaMessage) -> Xdf2SchemaImport:
        return Xdf2SchemaImport(
            schema_text=xdf2.serialize_schema_message(message).decode("utf-8"),
            schema_message=message,
            freigabe_status=map_status(
                message.schema.status, message.schema.freigabedatum
            ),
            steckbrief_id=None,
            code_list_imports=[],
        )

    @staticmethod
    def from_bytes(
        schema_data: bytes,
        code_list_data: list[bytes],
        freigabe_status: xdf3.FreigabeStatus,
        steckbrief_id: str | None,
    ) -> Xdf2SchemaImport:
        try:
            schema_message = xdf2.parse_schema_message(schema_data)
        except XdfException as ex:
            raise ImportException(str(ex)) from ex

        if schema_message.version is None:
            assert ImportException(
                f"Cannot import schema without a version [id={schema_message.version}]"
            )

        code_list_imports = [CodeListImport.from_bytes(data) for data in code_list_data]

        code_list_imports = deduplicate_code_list_imports(code_list_imports)

        return Xdf2SchemaImport(
            schema_text=schema_data.decode("utf-8"),
            schema_message=schema_message,
            freigabe_status=freigabe_status,
            steckbrief_id=steckbrief_id,
            code_list_imports=code_list_imports,
        )

    def get_code_list_import(self, identifier: genericode.Identifier) -> CodeListImport:
        for code_list_import in self.code_list_imports:
            if code_list_import.code_list.identifier == identifier:
                return code_list_import

        raise ImportException(
            f"Could not find code list [uri={identifier.canonical_version_uri}]"
        )

    def get_external_code_lists(self) -> list[genericode.Identifier]:
        imported_code_list_identifiers = [
            code_list_import.code_list.identifier
            for code_list_import in self.code_list_imports
        ]

        return [
            identifier
            for identifier in self.schema_message.get_code_list_identifiers()
            if identifier not in imported_code_list_identifiers
        ]

    @property
    def id(self) -> str:
        return self.schema_message.schema.identifier.id

    @property
    def version(self) -> str:
        # This is safe, because we force the version to exist in the constructor.
        return self.schema_message.schema.identifier.assert_version()


@dataclass(slots=True)
class Xdf3SchemaImport:
    """
    Holds all of the imported data for a schema.
    """

    schema_text: str
    schema_message: xdf3.SchemaMessage
    group_contents: dict[xdf3.Identifier, str] = dataclass_field(init=False)
    field_contents: dict[xdf3.Identifier, str] = dataclass_field(init=False)
    rule_contents: dict[xdf3.Identifier, str] = dataclass_field(init=False)

    def __post_init__(self):
        field_contents: dict[xdf3.Identifier, str] = {}
        for field_id in self.schema_message.fields.keys():
            field = self.schema_message.fields[field_id]
            field_contents[field_id] = xml_to_str(
                xdf3.serialize_field(field, rule_map=self.schema_message.rules)
            )
        self.field_contents = field_contents

        group_contents: dict[xdf3.Identifier, str] = {}
        for group_id in self.schema_message.groups.keys():
            group = self.schema_message.groups[group_id]
            group_contents[group_id] = xml_to_str(
                xdf3.serialize_group(
                    group,
                    group_map=self.schema_message.groups,
                    field_map=self.schema_message.fields,
                    rule_map=self.schema_message.rules,
                )
            )
        self.group_contents = group_contents

        rule_contents: dict[xdf3.Identifier, str] = {}
        for rule_id in self.schema_message.rules.keys():
            rule = self.schema_message.rules[rule_id]
            rule_contents[rule_id] = xml_to_str(xdf3.serialize_rule(rule))
        self.rule_contents = rule_contents

    @staticmethod
    def from_message(message: xdf3.SchemaMessage) -> Xdf3SchemaImport:
        return Xdf3SchemaImport(
            schema_text=xdf3.serialize_schema_message(message).decode("utf-8"),
            schema_message=message,
        )

    @staticmethod
    def from_bytes(
        schema_data: bytes,
    ) -> Xdf3SchemaImport:
        try:
            schema_message = xdf3.parse_schema_message(schema_data)
        except XdfException as error:
            raise ImportException(str(error)) from error

        if schema_message.version is None:
            raise ImportException(
                f"Cannot import schema without a version [id={schema_message.id}]"
            )

        return Xdf3SchemaImport(
            schema_text=schema_data.decode("utf-8"),
            schema_message=schema_message,
        )

    @property
    def id(self) -> str:
        return self.schema_message.schema.identifier.id

    @property
    def version(self) -> str:
        # This is safe, as we satisfy in the constructor that the version cannot be empty.
        return self.schema_message.assert_version()

    @property
    def freigabe_status(self) -> xdf3.FreigabeStatus:
        return self.schema_message.schema.freigabe_status


@dataclass(slots=True)
class NewImport:
    schema: NewSchema
    groups: list[NewGroup]
    fields: list[NewField]
    rules: list[NewRule]

    xdf_content: str
    fts_content: str

    @staticmethod
    def from_xdf2_import(
        xdf2_import: Xdf2SchemaImport,
        external_code_lists: dict[str, NewCodeList],
    ) -> NewImport:
        message = xdf2_import.schema_message

        code_lists: dict[str, NewCodeList] = {
            code_list_import.code_list.identifier.canonical_version_uri: NewCodeList.from_import(
                code_list_import
            )
            for code_list_import in xdf2_import.code_list_imports
        }

        code_lists.update(external_code_lists)

        # Just a sanity check: At this point, all code lists should be either
        # imported via the `xdf2_import`, or passed in via `external_code_lists`.
        for code_list_uri in message.get_code_list_uris():
            assert code_list_uri in code_lists

        groups = [
            NewGroup.from_xdf2(
                group,
                local_namespace=message.id,
                letzte_aenderung=message.header.erstellungs_zeitpunkt,
                xml_content=xdf2_import.group_contents[identifier],
            )
            for identifier, group in message.groups.items()
        ]
        fields = [
            NewField.from_xdf2(
                field,
                local_namespace=message.id,
                letzte_aenderung=message.header.erstellungs_zeitpunkt,
                xml_content=xdf2_import.field_contents[identifier],
                code_lists=code_lists,
            )
            for identifier, field in message.fields.items()
        ]
        rules = [
            NewRule.from_xdf2(
                rule,
                letzte_aenderung=message.header.erstellungs_zeitpunkt,
                xml_content=xdf2_import.rule_contents[identifier],
            )
            for identifier, rule in message.rules.items()
        ]

        bezug_groups = list(chain.from_iterable([group.bezug for group in groups]))
        bezug_fields = list(chain.from_iterable([field.bezug for field in fields]))
        bezug_element_reference = [
            ref.bezug for ref in message.schema.struktur if ref.bezug is not None
        ]
        bezug_element_reference_groups = [
            rechtsbezug
            for rechtsbezug in chain.from_iterable(
                [child.bezug for group in groups for child in group.children]
            )
        ]

        schema = NewSchema.from_xdf2(
            message.schema,
            freigabe_status=xdf2_import.freigabe_status,
            steckbrief_id=xdf2_import.steckbrief_id,
            letzte_aenderung=message.header.erstellungs_zeitpunkt,
            bezug_components=list(
                chain.from_iterable(
                    [
                        bezug_groups,
                        bezug_fields,
                        bezug_element_reference,
                        bezug_element_reference_groups,
                    ]
                )
            ),
            quality_report=check_xdf2_quality(
                message,
                code_lists={
                    code_list.identifier.canonical_version_uri: code_list.content.genericode_code_list
                    for code_list in code_lists.values()
                    if code_list.content is not None
                },
            ),
        )

        return NewImport(
            schema,
            groups=groups,
            fields=fields,
            rules=rules,
            xdf_content=xdf2_import.schema_text,
            fts_content=create_xdf2_fts_doc(message),
        )

    @staticmethod
    def from_xdf3_import(
        xdf3_import: Xdf3SchemaImport,
        external_code_lists: dict[str, NewCodeList],
    ) -> NewImport:
        message = xdf3_import.schema_message

        groups = [
            NewGroup.from_xdf3(
                group,
                local_namespace=message.id,
                xml_content=xdf3_import.group_contents[identifier],
            )
            for identifier, group in message.groups.items()
        ]
        fields = [
            NewField.from_xdf3(
                field,
                local_namespace=message.id,
                xml_content=xdf3_import.field_contents[identifier],
                code_lists=external_code_lists,
            )
            for identifier, field in message.fields.items()
        ]
        rules = [
            NewRule.from_xdf3(
                rule,
                xml_content=xdf3_import.rule_contents[identifier],
            )
            for identifier, rule in message.rules.items()
        ]

        bezug_groups = list(chain.from_iterable([group.bezug for group in groups]))
        bezug_fields = list(chain.from_iterable([field.bezug for field in fields]))
        bezug_element_reference = [
            str(rechtsbezug.text)
            for rechtsbezug in chain.from_iterable(
                [ref.bezug for ref in message.schema.struktur]
            )
        ]
        bezug_element_reference_groups = [
            rechtsbezug
            for rechtsbezug in chain.from_iterable(
                [child.bezug for group in groups for child in group.children]
            )
        ]

        schema = NewSchema.from_xdf3(
            message.schema,
            list(
                chain.from_iterable(
                    [
                        bezug_groups,
                        bezug_fields,
                        bezug_element_reference,
                        bezug_element_reference_groups,
                    ]
                )
            ),
            quality_report=check_xdf3_quality(
                message,
                code_lists={
                    code_list.identifier.canonical_version_uri: code_list.content.genericode_code_list
                    for code_list in external_code_lists.values()
                    if code_list.content is not None
                },
            ),
        )

        return NewImport(
            schema,
            groups=groups,
            fields=fields,
            rules=rules,
            xdf_content=xdf3_import.schema_text,
            fts_content=create_xdf3_fts_doc(message),
        )

    def get_all_loaded_code_lists(self) -> dict[str, genericode.CodeList]:
        code_lists = {}
        for field in self.fields:
            if field.code_list is not None:
                if field.code_list.content is not None:
                    code_lists[field.code_list.identifier.canonical_version_uri] = (
                        field.code_list.content.genericode_code_list
                    )

        return code_lists


@dataclass(slots=True, frozen=True)
class ElementIdentifier:
    namespace: str
    fim_id: str
    fim_version: str

    @staticmethod
    def from_xdf2(
        identifier: xdf2.Identifier, local_namespace: str
    ) -> ElementIdentifier:
        namespace = local_namespace if identifier.is_local() else "baukasten"

        version = identifier.version
        if version is None:
            raise ImportException(
                f"Cannot import element without a version [id={identifier.id}]"
            )

        return ElementIdentifier(
            namespace=namespace,
            fim_id=identifier.id,
            fim_version=version,
        )

    @staticmethod
    def from_xdf3(
        identifier: xdf3.Identifier, local_namespace: str
    ) -> ElementIdentifier:
        namespace = local_namespace if identifier.is_local() else "baukasten"

        version = identifier.version
        if version is None:
            raise ImportException(
                f"Cannot import element without a version [id={identifier.id}]"
            )

        return ElementIdentifier(
            namespace=namespace,
            fim_id=identifier.id,
            fim_version=version,
        )

    def is_local(self) -> bool:
        return self.namespace != "baukasten"


@dataclass(slots=True, frozen=True, eq=True)
class RuleIdentifier:
    fim_id: str
    fim_version: str

    @staticmethod
    def from_xdf2(identifier: xdf2.Identifier) -> RuleIdentifier:
        version = identifier.version
        if version is None:
            raise ImportException(
                f"Cannot import rule without a version [id={identifier.id}]"
            )

        return RuleIdentifier(
            fim_id=identifier.id,
            fim_version=version,
        )

    @staticmethod
    def from_xdf3(identifier: xdf3.Identifier) -> RuleIdentifier:
        version = identifier.version
        if version is None:
            raise ImportException(
                f"Cannot import rule without a version [id={identifier.id}]"
            )

        return RuleIdentifier(
            fim_id=identifier.id,
            fim_version=version,
        )


class ChildType(Enum):
    FIELD = "Feld"
    GROUP = "Gruppe"


@dataclass
class NewElementReference:
    namespace: str
    fim_id: str
    fim_version: str
    anzahl: str
    bezug: list[str]
    enthaelt: ChildType

    @staticmethod
    def from_xdf2(
        element_reference: xdf2.ElementReference, local_namespace: str
    ) -> NewElementReference:
        namespace = (
            local_namespace if element_reference.identifier.is_local() else "baukasten"
        )

        version = element_reference.identifier.version
        if version is None:
            raise ImportException(
                f"Cannot reference element without version [id={element_reference.identifier.version}]"
            )

        return NewElementReference(
            namespace=namespace,
            fim_id=element_reference.identifier.id,
            fim_version=version,
            anzahl=str(element_reference.anzahl),
            bezug=(
                [element_reference.bezug] if element_reference.bezug is not None else []
            ),
            enthaelt=(
                ChildType.FIELD
                if element_reference.element_type == xdf2.ElementType.FELD
                else ChildType.GROUP
            ),
        )

    @staticmethod
    def from_xdf3(
        element_reference: xdf3.ElementReference, local_namespace: str
    ) -> NewElementReference:
        namespace = (
            local_namespace if element_reference.identifier.is_local() else "baukasten"
        )

        return NewElementReference(
            namespace=namespace,
            fim_id=element_reference.identifier.id,
            fim_version=element_reference.identifier.assert_version(),
            anzahl=str(element_reference.anzahl),
            bezug=[bezug.text for bezug in element_reference.bezug],
            enthaelt=(
                ChildType.FIELD
                if element_reference.element_type == xdf2.ElementType.FELD
                else ChildType.GROUP
            ),
        )

    def to_identifier(self):
        return ElementIdentifier(self.namespace, self.fim_id, self.fim_version)


@dataclass(slots=True)
class NewSchema:
    fim_id: str
    fim_version: str
    nummernkreis: str
    name: str
    beschreibung: str | None
    definition: str | None
    bezug: list[str]
    bezug_components: list[str]
    freigabe_status: xdf3.FreigabeStatus
    status_gesetzt_am: date | None
    status_gesetzt_durch: str | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    versionshinweis: str | None
    veroeffentlichungsdatum: date | None
    bezeichnung: str | None
    relation: list[Relation]
    letzte_aenderung: datetime
    last_update: datetime
    stichwort: list[str]
    steckbrief_id: str | None

    xdf_version: XdfVersion

    regeln: set[RuleIdentifier]
    children: list[NewElementReference]
    quality_report: QualityReport
    immutable_json: dict[str, Any]

    @staticmethod
    def from_xdf2(
        schema: xdf2.Schema,
        freigabe_status: xdf3.FreigabeStatus,
        steckbrief_id: str | None,
        letzte_aenderung: datetime,
        bezug_components: list[str],
        quality_report: QualityReport,
    ) -> NewSchema:
        version = schema.identifier.version
        if version is None:
            raise ImportException(
                f"Cannot import schema without a version [id={schema.identifier.id}]"
            )

        # Convert to xdf3 nummernkreis
        nummernkreis = f"{schema.identifier.nummernkreis}000"

        bezug = [schema.bezug] if schema.bezug is not None else []
        immutable_json = generate_json(xdf2.ImmutableSchema.from_schema(schema))

        return NewSchema(
            fim_id=schema.identifier.id,
            fim_version=version,
            nummernkreis=nummernkreis,
            name=schema.name,
            beschreibung=schema.beschreibung,
            definition=schema.definition,
            bezug=bezug,
            bezug_components=bezug_components,
            freigabe_status=freigabe_status,
            status_gesetzt_am=schema.freigabedatum,
            status_gesetzt_durch=schema.fachlicher_ersteller,
            gueltig_ab=schema.gueltig_ab,
            gueltig_bis=schema.gueltig_bis,
            versionshinweis=schema.versionshinweis,
            veroeffentlichungsdatum=schema.veroeffentlichungsdatum,
            letzte_aenderung=letzte_aenderung,
            last_update=utc_now(),
            bezeichnung=schema.bezeichnung_eingabe,
            relation=[],
            stichwort=[],
            steckbrief_id=steckbrief_id,
            xdf_version=XdfVersion.XDF2,
            regeln=set([RuleIdentifier.from_xdf2(rule) for rule in schema.regeln]),
            children=[
                NewElementReference.from_xdf2(
                    child, local_namespace=schema.identifier.id
                )
                for child in schema.struktur
            ],
            quality_report=quality_report,
            immutable_json=immutable_json,
        )

    @staticmethod
    def from_xdf3(
        schema: xdf3.Schema, bezug_components: list[str], quality_report: QualityReport
    ) -> NewSchema:
        version = schema.identifier.version
        if version is None:
            raise ImportException(
                f"Cannot import schema without a version [id={schema.identifier.id}]"
            )

        bezug = [str(bezug.text) for bezug in schema.bezug]
        stichwort = [
            f"{stichwort.uri}::{stichwort.value}" for stichwort in schema.stichwort
        ]

        immutable_json = generate_json(xdf3.ImmutableSchema.from_schema(schema))

        return NewSchema(
            fim_id=schema.identifier.id,
            fim_version=version,
            nummernkreis=schema.identifier.nummernkreis,
            name=schema.name,
            beschreibung=schema.beschreibung,
            definition=schema.definition,
            bezug=bezug,
            bezug_components=bezug_components,
            freigabe_status=schema.freigabe_status,
            status_gesetzt_durch=schema.status_gesetzt_durch,
            status_gesetzt_am=schema.status_gesetzt_am,
            gueltig_ab=schema.gueltig_ab,
            gueltig_bis=schema.gueltig_bis,
            versionshinweis=schema.versionshinweis,
            veroeffentlichungsdatum=schema.veroeffentlichungsdatum,
            relation=schema.relation,
            letzte_aenderung=schema.letzte_aenderung,
            last_update=utc_now(),
            stichwort=stichwort,
            steckbrief_id=schema.dokumentsteckbrief.id,
            bezeichnung=schema.bezeichnung,
            xdf_version=XdfVersion.XDF3,
            regeln=set([RuleIdentifier.from_xdf3(rule) for rule in schema.regeln]),
            children=[
                NewElementReference.from_xdf3(
                    child, local_namespace=schema.identifier.id
                )
                for child in schema.struktur
            ],
            quality_report=quality_report,
            immutable_json=immutable_json,
        )


@dataclass(slots=True)
class NewGroup:
    identifier: ElementIdentifier
    nummernkreis: str
    name: str
    beschreibung: str | None
    definition: str | None
    bezug: list[str]
    freigabe_status: xdf3.FreigabeStatus
    status_gesetzt_am: date | None
    status_gesetzt_durch: str | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    versionshinweis: str | None
    veroeffentlichungsdatum: date | None
    relation: list[Relation]
    letzte_aenderung: datetime
    last_update: datetime

    xdf_version: XdfVersion

    regeln: set[RuleIdentifier]
    children: list[NewElementReference]

    xml_content: str
    immutable_json: dict[str, Any]

    @staticmethod
    def from_xdf2(
        group: xdf2.Gruppe,
        local_namespace: str,
        letzte_aenderung: datetime,
        xml_content: str,
    ) -> NewGroup:
        nummernkreis = f"{group.identifier.nummernkreis}000"

        immutable_json = generate_json(xdf2.ImmutableGruppe.from_gruppe(group))

        return NewGroup(
            identifier=ElementIdentifier.from_xdf2(
                group.identifier, local_namespace=local_namespace
            ),
            nummernkreis=nummernkreis,
            name=group.name,
            beschreibung=group.beschreibung,
            definition=group.definition,
            bezug=[group.bezug] if group.bezug is not None else [],
            freigabe_status=xdf2_to_xdf3.map_status(
                group.status,
                freigabedatum=group.freigabedatum,
            ),
            status_gesetzt_am=group.freigabedatum,
            status_gesetzt_durch=group.fachlicher_ersteller,
            gueltig_ab=group.gueltig_ab,
            gueltig_bis=group.gueltig_bis,
            versionshinweis=group.versionshinweis,
            veroeffentlichungsdatum=group.veroeffentlichungsdatum,
            relation=[],
            letzte_aenderung=letzte_aenderung,
            last_update=utc_now(),
            xdf_version=XdfVersion.XDF2,
            regeln=set([RuleIdentifier.from_xdf2(rule) for rule in group.regeln]),
            children=[
                NewElementReference.from_xdf2(child, local_namespace=local_namespace)
                for child in group.struktur
            ],
            xml_content=xml_content,
            immutable_json=immutable_json,
        )

    @staticmethod
    def from_xdf3(
        group: xdf3.Gruppe, local_namespace: str, xml_content: str
    ) -> NewGroup:
        immutable_json = generate_json(xdf3.ImmutableGruppe.from_gruppe(group))

        return NewGroup(
            identifier=ElementIdentifier.from_xdf3(
                group.identifier, local_namespace=local_namespace
            ),
            nummernkreis=group.identifier.nummernkreis,
            xdf_version=XdfVersion.XDF3,
            name=group.name,
            beschreibung=group.beschreibung,
            definition=group.definition,
            bezug=[rechtsbezug.text for rechtsbezug in group.bezug],
            freigabe_status=group.freigabe_status,
            status_gesetzt_durch=group.status_gesetzt_durch,
            status_gesetzt_am=group.status_gesetzt_am,
            gueltig_ab=group.gueltig_ab,
            gueltig_bis=group.gueltig_bis,
            versionshinweis=group.versionshinweis,
            relation=group.relation,
            veroeffentlichungsdatum=group.veroeffentlichungsdatum,
            letzte_aenderung=group.letzte_aenderung,
            last_update=utc_now(),
            regeln=set([RuleIdentifier.from_xdf3(rule) for rule in group.regeln]),
            children=[
                NewElementReference.from_xdf3(child, local_namespace=local_namespace)
                for child in group.struktur
            ],
            xml_content=xml_content,
            immutable_json=immutable_json,
        )


@dataclass(slots=True)
class NewField:
    identifier: ElementIdentifier
    nummernkreis: str
    name: str
    beschreibung: str | None
    definition: str | None
    bezug: list[str]
    freigabe_status: xdf3.FreigabeStatus
    status_gesetzt_am: date | None
    status_gesetzt_durch: str | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    versionshinweis: str | None
    veroeffentlichungsdatum: date | None
    relation: list[Relation]
    letzte_aenderung: datetime
    last_update: datetime
    code_list: NewCodeList | None

    feldart: xdf3.Feldart
    datentyp: xdf3.Datentyp
    regeln: set[RuleIdentifier]

    xdf_version: XdfVersion

    xml_content: str
    immutable_json: dict[str, Any]

    @staticmethod
    def from_xdf2(
        field: xdf2.Datenfeld,
        local_namespace: str,
        letzte_aenderung: datetime,
        xml_content: str,
        code_lists: dict[str, NewCodeList],
    ) -> NewField:
        nummernkreis = f"{field.identifier.nummernkreis}000"

        immutable_json = generate_json(xdf2.ImmutableFeld.from_datenfeld(field))

        if field.code_listen_referenz is not None:
            code_list = code_lists[
                field.code_listen_referenz.genericode_identifier.canonical_version_uri
            ]
        else:
            code_list = None

        return NewField(
            identifier=ElementIdentifier.from_xdf2(
                field.identifier, local_namespace=local_namespace
            ),
            nummernkreis=nummernkreis,
            xdf_version=XdfVersion.XDF2,
            name=field.name,
            beschreibung=field.beschreibung,
            definition=field.definition,
            bezug=[field.bezug] if field.bezug is not None else [],
            freigabe_status=xdf2_to_xdf3.map_status(
                field.status,
                freigabedatum=field.freigabedatum,
            ),
            status_gesetzt_am=field.freigabedatum,
            status_gesetzt_durch=field.fachlicher_ersteller,
            gueltig_ab=field.gueltig_ab,
            gueltig_bis=field.gueltig_bis,
            versionshinweis=field.versionshinweis,
            veroeffentlichungsdatum=field.veroeffentlichungsdatum,
            relation=[],
            letzte_aenderung=letzte_aenderung,
            last_update=utc_now(),
            feldart=xdf2_to_xdf3.map_feldart(field.feldart),
            datentyp=xdf2_to_xdf3.map_datentyp(field.datentyp),
            regeln=set([RuleIdentifier.from_xdf2(rule) for rule in field.regeln]),
            xml_content=xml_content,
            immutable_json=immutable_json,
            code_list=code_list,
        )

    @staticmethod
    def from_xdf3(
        field: xdf3.Datenfeld,
        local_namespace: str,
        xml_content: str,
        code_lists: dict[str, NewCodeList],
    ) -> NewField:
        immutable_json = generate_json(xdf3.ImmutableFeld.from_datenfeld(field))

        if field.codeliste_referenz:
            code_list = code_lists[field.codeliste_referenz.canonical_version_uri]
        else:
            code_list = None

        return NewField(
            identifier=ElementIdentifier.from_xdf3(
                field.identifier, local_namespace=local_namespace
            ),
            nummernkreis=field.identifier.nummernkreis,
            xdf_version=XdfVersion.XDF3,
            name=field.name,
            beschreibung=field.beschreibung,
            definition=field.definition,
            bezug=[rechtsbezug.text for rechtsbezug in field.bezug],
            freigabe_status=field.freigabe_status,
            status_gesetzt_durch=field.status_gesetzt_durch,
            status_gesetzt_am=field.status_gesetzt_am,
            gueltig_ab=field.gueltig_ab,
            gueltig_bis=field.gueltig_bis,
            versionshinweis=field.versionshinweis,
            veroeffentlichungsdatum=field.veroeffentlichungsdatum,
            letzte_aenderung=field.letzte_aenderung,
            relation=field.relation,
            last_update=utc_now(),
            feldart=field.feldart,
            datentyp=field.datentyp,
            regeln=set([RuleIdentifier.from_xdf3(rule) for rule in field.regeln]),
            xml_content=xml_content,
            immutable_json=immutable_json,
            code_list=code_list,
        )


@dataclass(slots=True)
class CodeListContent:
    source: str
    xml_content: str
    genericode_code_list: genericode.CodeList


@dataclass(slots=True)
class NewCodeList:
    identifier: genericode.Identifier
    is_external: bool
    content: CodeListContent | None

    @staticmethod
    def from_code_list_with_content(
        code_list_with_content: genericode.CodeListWithContent, source: str
    ):
        content = CodeListContent(
            source=source,
            xml_content=code_list_with_content[1],
            genericode_code_list=code_list_with_content[0],
        )

        return NewCodeList(
            identifier=code_list_with_content[0].identifier,
            is_external=True,
            content=content,
        )

    @staticmethod
    def external(identifier: genericode.Identifier):
        return NewCodeList(
            identifier=identifier,
            is_external=True,
            content=None,
        )

    @staticmethod
    def from_import(code_list_import: CodeListImport):
        content = CodeListContent(
            source="internal",
            xml_content=code_list_import.code_list_text,
            genericode_code_list=parse_code_list(code_list_import.code_list_text),
        )

        return NewCodeList(
            identifier=code_list_import.code_list.identifier,
            is_external=False,
            content=content,
        )


@dataclass(slots=True)
class NewRule:
    fim_id: str
    fim_version: str
    nummernkreis: str
    name: str

    # These attributes are only present in xdf2 rules, as they were dropped in xdf3.
    # They are kept for compatibility reasons when exporting data in the xdf2 format.
    # For all other purposes (e.g. displaying the rule in the UI), these should probably be ignored.
    freigabe_status: xdf3.FreigabeStatus | None
    status_gesetzt_am: date | None
    status_gesetzt_durch: str | None

    letzte_aenderung: datetime
    last_update: datetime
    veroeffentlichungsdatum: date | None

    xdf_version: XdfVersion

    xml_content: str
    immutable_json: dict[str, Any]

    def to_identifier(self) -> RuleIdentifier:
        return RuleIdentifier(self.fim_id, self.fim_version)

    @staticmethod
    def from_xdf2(
        rule: xdf2.Regel, letzte_aenderung: datetime, xml_content: str
    ) -> NewRule:
        version = rule.identifier.version
        if version is None:
            raise ImportException(
                f"Cannot import rule without a version [id={rule.identifier.id}]"
            )

        immutable_json = generate_json(xdf2.ImmutableRegel.from_regel(rule))

        return NewRule(
            fim_id=rule.identifier.id,
            fim_version=version,
            nummernkreis=rule.identifier.get_xdf3_nummernkreis(),
            name=rule.name,
            freigabe_status=xdf2_to_xdf3.map_status(
                rule.status,
                freigabedatum=rule.freigabedatum,
            ),
            status_gesetzt_durch=rule.fachlicher_ersteller,
            status_gesetzt_am=rule.freigabedatum,
            letzte_aenderung=letzte_aenderung,
            last_update=utc_now(),
            veroeffentlichungsdatum=rule.veroeffentlichungsdatum,
            xdf_version=XdfVersion.XDF2,
            xml_content=xml_content,
            immutable_json=immutable_json,
        )

    @staticmethod
    def from_xdf3(rule: xdf3.Regel, xml_content: str) -> NewRule:
        version = rule.identifier.version
        if version is None:
            raise ImportException(
                f"Cannot import rule without a version [id={rule.identifier.id}]"
            )

        immutable_json = generate_json(xdf3.ImmutableRegel.from_regel(rule))

        return NewRule(
            fim_id=rule.identifier.id,
            fim_version=version,
            nummernkreis=rule.identifier.nummernkreis,
            name=rule.name,
            freigabe_status=None,
            status_gesetzt_durch=None,
            status_gesetzt_am=None,
            letzte_aenderung=rule.letzte_aenderung,
            last_update=utc_now(),
            veroeffentlichungsdatum=None,
            xdf_version=XdfVersion.XDF3,
            xml_content=xml_content,
            immutable_json=immutable_json,
        )
