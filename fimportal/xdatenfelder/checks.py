"""
Module containing all the available checks for fields, groups and rules.
Checks can be found here: https://docs.fitko.de/fim/docs/datenfelder/QS_Bericht/
"""

from __future__ import annotations

import re
from dataclasses import dataclass
from typing import Any, Callable, Literal, TypeVar

from fimportal import genericode

from . import xdf2, xdf3


@dataclass(slots=True)
class FailingCheck:
    code: int
    error_type: Literal["critical", "warning", "method", "info"]
    message: str


@dataclass(slots=True)
class ElementReport:
    identifier: xdf2.Identifier | xdf3.Identifier
    failing_checks: list[FailingCheck]

    @staticmethod
    def from_dict(data: dict[str, Any]) -> ElementReport:
        return ElementReport(
            identifier=_deserialize_identifier(data["identifier"]),
            failing_checks=[
                FailingCheck(
                    failing_check["code"],
                    failing_check["error_type"],
                    failing_check["message"],
                )
                for failing_check in data["failing_checks"]
            ],
        )

    def to_dict(self) -> dict[str, Any]:
        return {
            "identifier": _serialize_identifier(self.identifier),
            "failing_checks": [
                {
                    "code": quality_check.code,
                    "error_type": quality_check.error_type,
                    "message": quality_check.message,
                }
                for quality_check in self.failing_checks
            ],
        }


def _serialize_identifier(identifier: xdf2.Identifier | xdf3.Identifier) -> str:
    if isinstance(identifier, xdf2.Identifier):
        return f"2:{identifier.id}:{identifier.version}"
    else:
        return f"3:{identifier.id}:{identifier.assert_version()}"


def _deserialize_identifier(data: str) -> xdf2.Identifier | xdf3.Identifier:
    xdf_version, fim_id, fim_version = data.split(":")

    if xdf_version == "2":
        return xdf2.Identifier(fim_id, xdf2.parse_version(fim_version))
    else:
        assert xdf_version == "3"
        return xdf3.Identifier(fim_id, xdf3.parse_version(fim_version))


T = TypeVar(
    "T",
    xdf2.Datenfeld,
    xdf2.Gruppe,
    xdf2.Regel,
    xdf3.Datenfeld,
    xdf3.Gruppe,
    xdf3.Regel,
    xdf3.SchemaMessage,
)


Check = Callable[[list[FailingCheck], T], None]


def process_checks(element: T, checks: list[Check[T]]) -> ElementReport:
    """
    Execute all the provided checks and collect the failing_checks in an
    `ElementReport`.
    """
    failing_checks: list[FailingCheck] = []

    for check in checks:
        check(failing_checks, element)

    return ElementReport(
        identifier=element.identifier,
        failing_checks=failing_checks,
    )


def check_element_versions_in_schema(
    failing_checks: list[FailingCheck], schema: xdf3.SchemaMessage
):
    """
    E1003 - warning: In einem Datenschema darf ein Baukastenelement nicht mehrfach in unterschiedlichen Versionen enthalten sein.
    """
    id_to_version: dict[str, str | None] = {}

    for ref in schema.schema.struktur:
        id = ref.identifier.id
        version = ref.identifier.version
        element_type = ref.element_type

        if not _set_and_check_version(id, version, id_to_version):
            failing_checks.append(
                FailingCheck(
                    1003,
                    "warning",
                    f"{element_type} {id} existiert mit version {id_to_version[id]} und {version}",
                )
            )

    for rule in schema.rules:
        identifier = rule.id
        version = rule.version
        if not _set_and_check_version(identifier, version, id_to_version):
            failing_checks.append(
                FailingCheck(
                    1003,
                    "warning",
                    f"Regel {identifier} existiert mit version {id_to_version[identifier]} und {version}",
                )
            )


def check_element_versions_in_group(
    failing_checks: list[FailingCheck], group: xdf3.Gruppe
):
    """
    E1004 - warning: In einer Datenfeldgruppe darf ein Baukastenelement nicht mehrfach in derselben Version enthalten sein..
    """
    id_to_version: dict[str, str | None] = {}
    group_id = group.identifier.id
    for rule in group.regeln:
        id = rule.id
        version = rule.version
        if not _set_and_check_version(id, version, id_to_version):
            failing_checks.append(
                FailingCheck(
                    1004,
                    "warning",
                    f"Regel {id} in Gruppe {group_id} existiert mit version {id_to_version[id]} und {version}",
                )
            )

    for reference in group.struktur:
        id = reference.identifier.id
        version = reference.identifier.version
        element_type = reference.element_type
        if not _set_and_check_version(id, version, id_to_version):
            failing_checks.append(
                FailingCheck(
                    1004,
                    "warning",
                    f"{element_type} {id} in Gruppe {group_id} existiert mit version {id_to_version[id]} und {version}",
                )
            )


def check_bezeichnung_eingabe(
    failing_checks: list[FailingCheck], element: xdf2.Datenfeld | xdf2.Gruppe
):
    """
    1009 - critical: Die 'Bezeichnung Eingabe' muss befuellt werden.
    """
    if element.bezeichnung_eingabe is None:
        failing_checks.append(
            FailingCheck(
                1009, "critical", "`bezeichnungEingabe` sollte nicht leer sein."
            )
        )


def check_select_datentyp(failing_checks: list[FailingCheck], field: xdf2.Datenfeld):
    """
    1011 - critical: Bei Datenfeldern mit der Feldart 'Auswahl' sollte der Datentyp 'Text' sein.
    """
    if field.feldart == xdf2.Feldart.SELECT and field.datentyp != xdf2.Datentyp.TEXT:
        failing_checks.append(
            FailingCheck(
                1011,
                "critical",
                f"SELECT Felder sollten den Datentyp `text` haben, nicht `{field.datentyp.value}`.",
            )
        )


def check_unused_praezisierung_for_select(
    failing_checks: list[FailingCheck], field: xdf2.Datenfeld | xdf3.Datenfeld
):
    """
    1012 - method: Bei Datenfeldern mit der Feldart 'Auswahlfeld' oder 'Statisches, read-only Feld' dürfen weder die minimale noch die maximale Feldl‰nge angegeben werden.
    """

    if (
        field.feldart == xdf2.Feldart.LABEL
        or field.feldart == xdf2.Feldart.SELECT
        or field.feldart == xdf3.Feldart.AUSWAHL
    ):
        if field.praezisierung is not None:
            failing_checks.append(
                FailingCheck(
                    1012,
                    "method",
                    "`praezisierung` wird ignoriert bei LABEL und SELECT Feldern",
                )
            )


def check_struktur_elementart(
    failing_checks: list[FailingCheck], schema: xdf3.SchemaMessage
):
    """
    1013 - method: Innerhalb von Datenschemata und Datenfeldgruppen dürfen nur Elemente mit der
    Strukturelementart harmonisiert oder rechtsnormgebunden verwendet werden.
    """
    for field in schema.fields.values():
        if field.schema_element_art == xdf3.SchemaElementArt.ABSTRAKT:
            failing_checks.append(
                FailingCheck(
                    1013,
                    "method",
                    f"Element {field.identifier.id} mit Strukturelementart, die nicht harmonisiert oder rechtsnormgebunden ist.",
                )
            )

    for group in schema.groups.values():
        if group.schema_element_art == xdf3.SchemaElementArt.ABSTRAKT:
            failing_checks.append(
                FailingCheck(
                    1013,
                    "method",
                    f"Element {group.identifier.id} mit Strukturelementart, die nicht harmonisiert oder rechtsnormgebunden ist.",
                )
            )


def check_unused_code_listen_referenz(
    failing_checks: list[FailingCheck], field: xdf2.Datenfeld | xdf3.Datenfeld
):
    """
    1017 - critical: Ist eine Code- oder Werteliste zugeordnet, muss die Feldart 'Auswahl' sein.
    """
    if isinstance(field, xdf2.Datenfeld):
        if field.feldart == xdf2.Feldart.LABEL or field.feldart == xdf2.Feldart.INPUT:
            if field.code_listen_referenz is not None:
                failing_checks.append(
                    FailingCheck(
                        1017,
                        "critical",
                        "`codelisteReferenz` wird ignoriert bei LABEL und INPUT Feldern.",
                    )
                )
    else:
        if (
            field.codeliste_referenz is not None or field.werte is not None
        ) and field.feldart != xdf3.Feldart.AUSWAHL:
            failing_checks.append(
                FailingCheck(
                    1017,
                    "critical",
                    "Codelist oder Werteliste definiert, aber Feldart nicht `AUSWAHL`.",
                )
            )


def check_missing_code_list_reference(
    failing_checks: list[FailingCheck], field: xdf2.Datenfeld | xdf3.Datenfeld
):
    """
    1018 - warning: Wenn ein Datenfeld die Feldart 'Auswahl' hat, muss entweder eine Code- oder eine Werteliste
    zugeordnet sein.
    """
    if isinstance(field, xdf2.Datenfeld):
        if field.feldart == xdf2.Feldart.SELECT and field.code_listen_referenz is None:
            failing_checks.append(
                FailingCheck(
                    1018,
                    "warning",
                    f"Auswahlfeld {field.identifier.id} hat keine zugeordnete Codeliste.",
                )
            )
    else:
        if (
            field.feldart == xdf3.Feldart.AUSWAHL
            and field.codeliste_referenz is None
            and (field.werte is None or len(field.werte) == 0)
        ):
            failing_checks.append(
                FailingCheck(
                    1018,
                    "warning",
                    f"Auswahlfeld {field.identifier.id} hat keine zugeordnete Codeliste oder Wertelist.",
                )
            )


def check_handlungsgrundlage(
    failing_checks: list[FailingCheck], schema: xdf3.SchemaMessage
):
    """
    1022 - method: Bei Verwendung von Elementen mit der Strukturelementart 'abstrakt' oder 'harmonisiert' innerhalb von
    Datenfeldgruppen mit der Strukturelementart 'rechtsnormgebunden' muss zur Multiplizität ein Bezug zu einer
    Handlungsgrundlage angegeben werden.
    """

    for group in schema.groups.values():
        if group.schema_element_art == xdf3.SchemaElementArt.RECHTSNORMGEBUNDEN:
            for ref in group.struktur:
                element = schema.fields.get(ref.identifier) or schema.groups.get(
                    ref.identifier
                )
                assert element is not None
                if (
                    element.schema_element_art == xdf3.SchemaElementArt.ABSTRAKT
                    or element.schema_element_art == xdf3.SchemaElementArt.HARMONISIERT
                ) and len(group.bezug) == 0:
                    failing_checks.append(
                        FailingCheck(
                            1022,
                            "method",
                            f"Element {element.identifier.id} ({element.schema_element_art.to_label()}) in Gruppe {group.identifier.id} (Rechtsnormgebunden) hat keine Handlungsgrundlage.",
                        )
                    )


def check_no_child_elements(failing_checks: list[FailingCheck], group: xdf2.Gruppe):
    """
    1023 - critical: Datenfeldgruppe hat keine Unterelemente.
    """
    if len(group.struktur) == 0:
        failing_checks.append(
            FailingCheck(1023, "critical", "Datenfeldgruppe hat keine Unterelemente.")
        )


def check_child_versions(
    failing_checks: list[FailingCheck], schema: xdf3.SchemaMessage
):
    """
    1025 - method: In einem Datenschema dürfen Baukastenelement auf oberster Ebene nicht mehrfach in derselben Version enthalten sein.
    """

    id_to_version: dict[str, str | None] = {}

    for ref in schema.schema.struktur:
        id = ref.identifier.id
        version = ref.identifier.version
        element_type = ref.element_type

        if not _set_and_check_version(id, version, id_to_version):
            failing_checks.append(
                FailingCheck(
                    1025,
                    "method",
                    f"Direktes Kind {id} des Types {element_type} existiert mit version {id_to_version[id]} und {version}",
                )
            )


def check_code_list_versions(
    failing_checks: list[FailingCheck], schema: xdf3.SchemaMessage
):
    """
    1037 - warning: In einem Datenschema darf eine Codeliste nicht in mehreren Versionen verwendet werden.
    """
    id_to_version: dict[str, str | None] = {}

    for field in schema.fields.values():
        codelist_ref = field.codeliste_referenz
        if codelist_ref is not None:
            uri = codelist_ref.canonical_uri
            version = codelist_ref.version
            if not _set_and_check_version(uri, version, id_to_version):
                failing_checks.append(
                    FailingCheck(
                        1037,
                        "warning",
                        f"Codeliste {uri} existiert mit Version {id_to_version[uri]} und {version}",
                    )
                )


def check_auswahl_group(failing_checks: list[FailingCheck], group: xdf3.Gruppe):
    """
    1043 - method: Eine Auswahlgruppe muss mindestens zwei Unterelemente haben.
    """

    if group.art == xdf3.Gruppenart.AUSWAHL and len(group.struktur) < 2:
        failing_checks.append(
            FailingCheck(
                1043,
                "method",
                f"Auswahlgruppe {group.identifier.id} hat weniger als zwei Elemente.",
            )
        )


def check_entries_wertelist(failing_checks: list[FailingCheck], field: xdf3.Datenfeld):
    """
    1044 - method: Eine Werteliste muss mindestens zwei Einträge haben.
    """
    if field.werte is not None and len(field.werte) < 2:
        failing_checks.append(
            FailingCheck(
                1044,
                "method",
                f"Die Werteliste in Feld {field.identifier.id} hält weniger als zwei Einträge.",
            )
        )


def check_code_list_identification_invalid_chars(
    failing_checks: list[FailingCheck], field: xdf2.Datenfeld
):
    """
    1048 - critical: Die URN-Kennung einer Codeliste entspricht nicht den Formatvorgaben.
    """
    if field.code_listen_referenz is not None:
        canonical_uri = field.code_listen_referenz.genericode_identifier.canonical_uri
        if _contains_invalid_char(
            canonical_uri.lower(),
            ["ä", "ö", "ü"],
        ):
            failing_checks.append(
                FailingCheck(
                    1048,
                    "critical",
                    f"Codelistenreferenzen dürfen keine Umlaute enthalten: {canonical_uri}.",
                )
            )


def check_column_definition(failing_checks: list[FailingCheck], field: xdf3.Datenfeld):
    """
    1058 - method: Spaltendefinitionen dürfen nur angegeben werden, wenn im Datenfeld eine referenzierte Codeliste enthalten ist.
    """

    if (
        field.code_key is not None
        or field.name_key is not None
        or field.help_key is not None
    ) and field.codeliste_referenz is None:
        failing_checks.append(
            FailingCheck(
                1058,
                "method",
                f"Feld {field.identifier.id} gibt Spaltendefinitionen an, aber referenziert keine Codeliste",
            )
        )


def check_inhalt_codeliste(
    failing_checks: list[FailingCheck],
    field: xdf2.Datenfeld | xdf3.Datenfeld,
    code_lists: dict[str, genericode.CodeList],
):
    """
    1076 - method: Der Inhalt stimmt nicht mit der zugeordneten Codeliste überein.
    """
    code_list_identifier = None
    code_key = None
    if isinstance(field, xdf2.Datenfeld):
        if field.code_listen_referenz:
            code_list_identifier = field.code_listen_referenz.genericode_identifier

    if isinstance(field, xdf3.Datenfeld):
        code_list_identifier = field.codeliste_referenz
        code_key = field.code_key

    if code_list_identifier is None:
        return

    # TODO: Only use the canonical_version_uri here?
    if code_list_identifier.canonical_version_uri not in code_lists:
        # NOTE: If the referenced code list is missing, then we can not
        # tell if this check would fail in case we had it. Thus, we let
        # the check succeed.
        # Check 1115 will fail in this case.
        return

    code_list = code_lists[code_list_identifier.canonical_version_uri]

    try:
        key_column = code_list.get_key_column(code_key)
    except genericode.GenericodeException:
        # In this case, something is wrong with the code list. As this is
        # not a fault of the field that is being checked, this is a pass.
        return
    if field.inhalt is not None and field.inhalt not in key_column:
        failing_checks.append(
            FailingCheck(
                1076,
                "method",
                f"Inhalt des Feldes {field.identifier.id} entspricht keinem Code der Codeliste.",
            )
        )


def check_inhalt_werteliste(failing_checks: list[FailingCheck], field: xdf3.Datenfeld):
    """
    1085 - method: Der Inhalt muss ein Code-Wert der Werteliste sein.
    """
    if field.feldart == xdf3.Feldart.AUSWAHL and field.codeliste_referenz is None:
        werte = field.werte if field.werte is not None else []
        werteliste_codes = [wert.code for wert in werte]
        if field.inhalt is not None and field.inhalt not in werteliste_codes:
            failing_checks.append(
                FailingCheck(
                    1085,
                    "method",
                    f"Inhalt des Feldes {field.identifier.id} entspricht keinem Code der Werteliste.",
                )
            )


def check_relation_target(
    failing_checks: list[FailingCheck],
    element: xdf3.SchemaMessage | xdf3.Gruppe | xdf3.Datenfeld,
):
    """
    1087 - method: Relation zu einem unzulässigen Objekt.
    see https://docs.fitko.de/fim/docs/datenfelder/EA_Relationen/ for requirements for relations
    """

    element_to_check = (
        element.schema if isinstance(element, xdf3.SchemaMessage) else element
    )
    for relation in element_to_check.relation:
        relation_target = relation.objekt.id[0].lower()
        relation_origin = element_to_check.identifier.id[0].lower()
        if relation_origin == "s" and relation_target != "s":
            failing_checks.append(
                FailingCheck(1087, "method", "Schema beinhaltet unzulässige Relation")
            )

        elif relation_origin == "g" and relation_target == "s":
            failing_checks.append(
                FailingCheck(
                    1087,
                    "method",
                    f"Gruppe {element_to_check.identifier.id} beinhaltet unzulässige Relation.",
                )
            )
        elif relation_origin == "f" and relation_target == "s":
            failing_checks.append(
                FailingCheck(
                    1087,
                    "method",
                    f"Feld {element_to_check.identifier.id} beinhaltet unzulässige Relation.",
                )
            )


def check_relation_type(
    failing_checks: list[FailingCheck],
    element: xdf3.SchemaMessage | xdf3.Gruppe | xdf3.Datenfeld,
):
    """
    1088 - method: Diese Relation darf in diesem Objekt nicht verwendet werden.
    see https://docs.fitko.de/fim/docs/datenfelder/EA_Relationen/ for requirements for relations
    """

    element_to_check = (
        element.schema if isinstance(element, xdf3.SchemaMessage) else element
    )
    for relation in element_to_check.relation:
        relation_type = relation.praedikat
        relation_origin = element_to_check.identifier.id[0].lower()
        if (
            relation_origin in ["s", "g", "f"]
            and relation_type == xdf3.RelationTyp.IST_VERKNUEPFT_MIT
        ):
            failing_checks.append(
                FailingCheck(
                    1088,
                    "method",
                    f"Relationstyp darf in Element {element_to_check.identifier.id} nicht verwendet werden.",
                )
            )


def check_bezug_of_field_harmonisiert(
    failing_checks: list[FailingCheck], field: xdf3.Datenfeld
):
    """
    1101 - warning: Der Bezug zur Handlungsgrundlage sollte bei Elementen mit der Strukturelementart 'harmonisiert' möglichst nicht leer sein.
    """
    if (
        field.schema_element_art == xdf3.SchemaElementArt.HARMONISIERT
        and len(field.bezug) == 0
    ):
        failing_checks.append(
            FailingCheck(
                1101,
                "warning",
                f"Leerer Bezug bei Feld {field.identifier.id} mit Typ `harmonisert`.",
            )
        )


def check_praezisierung_of_text_field(
    failing_checks: list[FailingCheck], field: xdf3.Datenfeld
):
    """
    1102 - warning: Bei Eingabedatenfeldern mit dem Datentyp 'Text' oder 'String.Latin+' sollten, wenn möglich, die minimale und maximale Feldlänge angegeben werden.
    """
    if field.feldart == xdf3.Feldart.EINGABE and (
        field.datentyp == xdf3.Datentyp.TEXT
        or field.datentyp == xdf3.Datentyp.STRING_LATIN
    ):
        if (
            field.praezisierung is None
            or field.praezisierung.max_length is None
            or field.praezisierung.min_length is None
        ):
            failing_checks.append(
                FailingCheck(
                    1102,
                    "warning",
                    f"Keine minimale oder maximale Länge bei Feld {field.identifier.id} angegeben.",
                )
            )


def check_praezisierung_of_range_field(
    failing_checks: list[FailingCheck], field: xdf3.Datenfeld
):
    """
    1103 - warning: Bei Datenfeldern mit dem Datentyp ‘Nummer', 'Ganzzahl', 'Geldbetrag' oder 'Datum’ sollte, wenn möglich, ein Wertebereich angegeben werden.
    """
    if (
        field.datentyp == xdf3.Datentyp.NUMMER
        or field.datentyp == xdf3.Datentyp.GANZZAHL
        or field.datentyp == xdf3.Datentyp.GELDBETRAG
        or field.datentyp == xdf3.Datentyp.DATUM
    ):
        if (
            field.praezisierung is None
            or field.praezisierung.max_value is None
            or field.praezisierung.min_value is None
        ):
            failing_checks.append(
                FailingCheck(
                    1103,
                    "warning",
                    f"Kein Wertebereich bei Feld {field.identifier.id} angegeben.",
                )
            )


def check_codelist_version(failing_checks: list[FailingCheck], field: xdf3.Datenfeld):
    """
    1105 - warning: Die Version einer Codeliste sollte im Datumsformat in folgender Form angegeben werden, z. B. 2018-03-01.
    """
    if field.codeliste_referenz is not None and (
        field.codeliste_referenz.version is None
        or not re.match(r"^\d{4}-\d{2}-\d{2}$", field.codeliste_referenz.version)
    ):
        failing_checks.append(
            FailingCheck(
                1105,
                "warning",
                f"Version der Codelist im Feld {field.identifier.id} folgt nicht dem Format `YYYY-MM-DD`.",
            )
        )


def check_number_of_children(failing_checks: list[FailingCheck], group: xdf3.Gruppe):
    """
    1106 - warning: Eine Datenfeldgruppe sollte mehr als ein Unterelement haben.
    """
    if len(group.struktur) <= 1:
        failing_checks.append(
            FailingCheck(
                1106,
                "warning",
                f"Gruppe {group.identifier.id} hat {len(group.struktur)} Unterelemente. Es sollten mehr als 1 sein.",
            )
        )


def check_datatype_auswahl_field(
    failing_checks: list[FailingCheck], field: xdf3.Datenfeld
):
    """
    1108 - warning: Wenn ein Datenfeld die Feldart 'Auswahl' hat, sollte der Datentyp i. d. R. vom Typ 'Text' sein.
    """

    if field.feldart == xdf3.Feldart.AUSWAHL and field.datentyp != xdf3.Datentyp.TEXT:
        failing_checks.append(
            FailingCheck(
                1108,
                "warning",
                f"Auswahl Feld {field.identifier.id} sollte vom Typ Text sein.",
            )
        )


def check_static_field_vorbefuellung(
    failing_checks: list[FailingCheck], field: xdf3.Datenfeld
):
    """
    1109 - warning: Bei einem Feld mit nur lesendem Zugriff, der Feldart 'Statisch' wird i. d. R. der Inhalt mit einem Text befüllt.
    """

    if field.feldart == xdf3.Feldart.STATISCH and (
        field.datentyp != xdf3.Datentyp.TEXT or field.inhalt == None
    ):
        failing_checks.append(
            FailingCheck(
                1109,
                "warning",
                f"Feld {field.identifier.id} ist statisch, hat aber keine zwingende Vorbefüllung.",
            )
        )


def check_code_list_availability(
    failing_checks: list[FailingCheck],
    field: xdf2.Datenfeld | xdf3.Datenfeld,
    code_lists: dict[str, genericode.CodeList],
):
    """
    1115 - warning: Auf die Codeliste kann nicht im XRepository zugegriffen werden. Dies könnte auf einen Fehler in der URN oder der Version der Codeliste zurückzuführen sein.
    """

    code_list_identifier = None
    if isinstance(field, xdf2.Datenfeld):
        if field.code_listen_referenz:
            code_list_identifier = field.code_listen_referenz.genericode_identifier

    if isinstance(field, xdf3.Datenfeld):
        code_list_identifier = field.codeliste_referenz

    if code_list_identifier is None:
        return

    # TODO: Only use the canonical_version_uri here?
    if code_list_identifier.canonical_version_uri not in code_lists:
        failing_checks.append(
            FailingCheck(
                1115,
                "warning",
                f"Auf die Codeliste {code_list_identifier.canonical_version_uri} kann "
                + "nicht im XRepository zugegriffen werden. Dies könnte auf einen Fehler "
                + "in der URN oder der Version der Codeliste zurückzuführen sein.",
            )
        )


def check_element_status(
    failing_checks: list[FailingCheck],
    element: xdf3.SchemaMessage | xdf3.Gruppe | xdf3.Datenfeld,
):
    """
    1116 - warning: Baukastenelemente im Status 'inaktiv' sollten nur in Ausnahmefällen verwendet werden.
    """

    status = (
        element.freigabe_status
        if not isinstance(element, xdf3.SchemaMessage)
        else element.schema.freigabe_status
    )
    if status == xdf3.FreigabeStatus.INAKTIV:
        failing_checks.append(
            FailingCheck(
                1116,
                "warning",
                f"Baukastenelement {element.identifier.id} hat Status 'inaktiv'. Dies sollte nur in Ausnahmefällen verwendet werden.",
            )
        )


def check_fachlicher_ersteller(
    failing_checks: list[FailingCheck], schema: xdf3.SchemaMessage
):
    """
    1117 - Hinweis: Das Feld 'Fachlicher Ersteller' darf zur Versionierung oder Veröffentlichung nicht leer sein.
    """

    if schema.schema.status_gesetzt_durch is None:
        failing_checks.append(
            FailingCheck(
                1117,
                "info",
                "Das Feld 'Fachlicher Ersteller' darf zur Versionierung oder Veröffentlichung nicht leer sein.",
            )
        )


def _contains_invalid_char(value: str, invalid_chars: list[str]) -> bool:
    for char in invalid_chars:
        if char in value:
            return True

    return False


def _set_and_check_version(
    id: str, version: str | None, id_to_version: dict[str, str | None]
):
    if id in id_to_version:
        return version == id_to_version[id]
    else:
        id_to_version[id] = version
        return True
