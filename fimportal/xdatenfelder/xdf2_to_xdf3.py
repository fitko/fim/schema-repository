"""
Conversion from xdf2 to xdf3.

The conversion is done according to the official guide:
https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xdatenfelder_3.0.0:dokument:Transformationsregeln_XDatenfelder_2_zu_3
"""

import logging
from datetime import date, datetime

from fimportal import genericode
from fimportal.din91379 import parse_string_latin, parse_optional_string_latin
from . import xdf2, xdf3
from .common import ConversionException


logger = logging.getLogger(__name__)


DEFAULT_STECKBRIEF = xdf3.Identifier("D00000000001", version=None)


def convert_message(
    message: xdf2.SchemaMessage,
    code_lists: dict[genericode.Identifier, genericode.CodeList],
    steckbrief: xdf3.Identifier = DEFAULT_STECKBRIEF,
) -> xdf3.SchemaMessage:
    fields = (
        convert_field(
            field,
            letzte_aenderung=message.header.erstellungs_zeitpunkt,
            code_lists=code_lists,
        )
        for field in message.fields.values()
    )

    groups = (
        convert_group(
            group,
            letzte_aenderung=message.header.erstellungs_zeitpunkt,
        )
        for group in message.groups.values()
    )

    rules = (
        convert_rule(
            rule,
            letzte_aenderung=message.header.erstellungs_zeitpunkt,
        )
        for rule in message.rules.values()
    )

    return xdf3.SchemaMessage(
        header=xdf3.MessageHeader(
            nachricht_id=message.header.nachricht_id,
            erstellungs_zeitpunkt=message.header.erstellungs_zeitpunkt,
        ),
        schema=convert_schema(
            message.schema,
            steckbrief=steckbrief,
            letzte_aenderung=message.header.erstellungs_zeitpunkt,
        ),
        groups={group.identifier: group for group in groups},
        fields={field.identifier: field for field in fields},
        rules={rule.identifier: rule for rule in rules},
    )


def convert_schema(
    schema: xdf2.Schema,
    steckbrief: xdf3.Identifier,
    letzte_aenderung: datetime,
) -> xdf3.Schema:
    if len(schema.struktur) == 0:
        raise ConversionException(
            f"Cannot convert to xdf3: Empty 'struktur' in schema '{schema.identifier.id}V{schema.identifier.version}'"
        )

    return xdf3.Schema(
        identifier=convert_identifier(schema.identifier),
        name=parse_string_latin(schema.name),
        beschreibung=parse_optional_string_latin(schema.beschreibung),
        definition=parse_optional_string_latin(schema.definition),
        bezug=map_bezug(schema.bezug),
        freigabe_status=map_status(
            schema.status,
            freigabedatum=schema.freigabedatum,
        ),
        status_gesetzt_am=schema.freigabedatum,
        status_gesetzt_durch=parse_optional_string_latin(schema.fachlicher_ersteller),
        gueltig_ab=schema.gueltig_ab,
        gueltig_bis=schema.gueltig_bis,
        versionshinweis=parse_optional_string_latin(schema.versionshinweis),
        veroeffentlichungsdatum=schema.veroeffentlichungsdatum,
        letzte_aenderung=letzte_aenderung,
        relation=[],
        stichwort=[],
        bezeichnung=parse_string_latin(schema.bezeichnung_eingabe or schema.name),
        hilfetext=parse_optional_string_latin(schema.hilfetext),
        ableitungsmodifikationen_struktur=schema.ableitungsmodifikationen_struktur,
        ableitungsmodifikationen_repraesentation=schema.ableitungsmodifikationen_repraesentation,
        dokumentsteckbrief=steckbrief,
        regeln=_map_rules(schema.regeln),
        struktur=_map_struktur(schema.struktur),
    )


def convert_group(
    group: xdf2.Gruppe,
    letzte_aenderung: datetime,
) -> xdf3.Gruppe:
    if len(group.struktur) == 0:
        raise ConversionException(
            f"Cannot convert to xdf3: Empty 'struktur' in group '{group.identifier.id}V{group.identifier.version}'"
        )

    return xdf3.Gruppe(
        identifier=convert_identifier(group.identifier),
        name=parse_string_latin(group.name),
        beschreibung=parse_optional_string_latin(group.beschreibung),
        definition=parse_optional_string_latin(group.definition),
        bezug=map_bezug(group.bezug),
        freigabe_status=map_status(
            group.status,
            freigabedatum=group.freigabedatum,
        ),
        status_gesetzt_am=group.freigabedatum,
        status_gesetzt_durch=parse_optional_string_latin(group.fachlicher_ersteller),
        gueltig_ab=group.gueltig_ab,
        gueltig_bis=group.gueltig_bis,
        versionshinweis=parse_optional_string_latin(group.versionshinweis),
        veroeffentlichungsdatum=group.veroeffentlichungsdatum,
        letzte_aenderung=letzte_aenderung,
        relation=[],
        stichwort=[],
        bezeichnung_eingabe=parse_string_latin(group.bezeichnung_eingabe or group.name),
        bezeichnung_ausgabe=parse_optional_string_latin(group.bezeichnung_ausgabe),
        schema_element_art=group.schema_element_art,
        hilfetext_eingabe=parse_optional_string_latin(group.hilfetext_eingabe),
        hilfetext_ausgabe=parse_optional_string_latin(group.hilfetext_ausgabe),
        art=None,
        regeln=_map_rules(group.regeln),
        struktur=_map_struktur(group.struktur),
    )


def convert_field(
    field: xdf2.Datenfeld,
    letzte_aenderung: datetime,
    code_lists: dict[genericode.Identifier, genericode.CodeList],
) -> xdf3.Datenfeld:
    werte = _map_code_list(field.code_listen_referenz, code_lists)
    codeliste_referenz = None if werte is not None else field.code_listen_referenz
    return xdf3.Datenfeld(
        identifier=convert_identifier(field.identifier),
        name=parse_string_latin(field.name),
        beschreibung=parse_optional_string_latin(field.beschreibung),
        definition=parse_optional_string_latin(field.definition),
        bezug=map_bezug(field.bezug),
        freigabe_status=map_status(
            field.status,
            freigabedatum=field.freigabedatum,
        ),
        status_gesetzt_am=field.freigabedatum,
        status_gesetzt_durch=parse_optional_string_latin(field.fachlicher_ersteller),
        gueltig_ab=field.gueltig_ab,
        gueltig_bis=field.gueltig_bis,
        versionshinweis=parse_optional_string_latin(field.versionshinweis),
        veroeffentlichungsdatum=field.veroeffentlichungsdatum,
        letzte_aenderung=letzte_aenderung,
        relation=[],
        stichwort=[],
        bezeichnung_eingabe=parse_string_latin(field.bezeichnung_eingabe or field.name),
        bezeichnung_ausgabe=parse_optional_string_latin(field.bezeichnung_ausgabe),
        schema_element_art=field.schema_element_art,
        hilfetext_eingabe=parse_optional_string_latin(field.hilfetext_eingabe),
        hilfetext_ausgabe=parse_optional_string_latin(field.hilfetext_ausgabe),
        feldart=map_feldart(field.feldart),
        datentyp=map_datentyp(field.datentyp),
        praezisierung=_map_praezisierung(field),
        inhalt=parse_optional_string_latin(field.inhalt),
        vorbefuellung=xdf3.Vorbefuellung.KEINE,
        werte=werte,
        codeliste_referenz=codeliste_referenz.genericode_identifier
        if codeliste_referenz is not None
        else None,
        code_key=None,
        name_key=None,
        help_key=None,
        regeln=_map_rules(field.regeln),
        max_size=None,
        media_type=[],
    )


def convert_rule(rule: xdf2.Regel, letzte_aenderung: datetime) -> xdf3.Regel:
    return xdf3.Regel(
        identifier=convert_identifier(rule.identifier),
        name=parse_string_latin(rule.name),
        beschreibung=parse_optional_string_latin(rule.beschreibung),
        freitext_regel=rule.definition,
        bezug=map_bezug(rule.bezug),
        stichwort=[],
        fachlicher_ersteller=rule.fachlicher_ersteller,
        letzte_aenderung=letzte_aenderung,
        typ=xdf3.Regeltyp.KOMPLEX,
        param=[],
        ziel=[],
        skript=rule.script,
        fehler=[],
    )


def _map_praezisierung(field: xdf2.Datenfeld) -> xdf3.Praezisierung | None:
    if field.praezisierung is None:
        return None

    try:
        constraints = field.get_constraints()
    except xdf3.InvalidPraezisierungException:
        logger.warning("Ignore invalid praezisierung: %s", field.praezisierung)
        return None
    else:
        return xdf3.Praezisierung(
            min_length=constraints.min_length,
            max_length=constraints.max_length,
            min_value=(
                str(constraints.min_value)
                if constraints.min_value is not None
                else None
            ),
            max_value=(
                str(constraints.max_value)
                if constraints.max_value is not None
                else None
            ),
            pattern=constraints.pattern,
        )


def _map_rules(regeln: list[xdf2.Identifier]) -> list[xdf3.Identifier]:
    return [convert_identifier(identifier) for identifier in regeln]


def _map_code_list(
    referenz: xdf2.CodeListenReferenz | None,
    code_lists: dict[genericode.Identifier, genericode.CodeList],
) -> list[xdf3.ListenWert] | None:
    if referenz is None:
        return None

    code_list = code_lists.get(referenz.genericode_identifier)
    if code_list is None:
        return None

    try:
        key_column = code_list.get_key_column()
        label_column = code_list.select_name_column()
    except genericode.GenericodeException as error:
        raise ConversionException(f"Cannot convert to xdf3: {str(error)}") from error

    return [
        xdf3.ListenWert(key, label, None)
        for key, label in zip(key_column, label_column)
    ]


def convert_identifier(identifier: xdf2.Identifier) -> xdf3.Identifier:
    """
    Convert an xdf2 identifier into an xdf3 identifier by:

    1. Adding the default Unternummernkreis `000`.
    2. By extending the version by a patch version of `0`.
    """

    id_start = identifier.id[:3]
    id_end = identifier.id[3:]

    if identifier.version is None:
        version = None
    else:
        version = xdf3.parse_version(f"{identifier.version}.0")

    return xdf3.Identifier(
        id=f"{id_start}000{id_end}",
        version=version,
    )


def map_feldart(feldart: xdf2.Feldart) -> xdf3.Feldart:
    match feldart:
        case xdf2.Feldart.INPUT:
            return xdf3.Feldart.EINGABE
        case xdf2.Feldart.SELECT:
            return xdf3.Feldart.AUSWAHL
        case xdf2.Feldart.LABEL:
            return xdf3.Feldart.STATISCH


def map_datentyp(datentyp: xdf2.Datentyp) -> xdf3.Datentyp:
    match datentyp:
        case xdf2.Datentyp.TEXT:
            return xdf3.Datentyp.TEXT
        case xdf2.Datentyp.DATUM:
            return xdf3.Datentyp.DATUM
        case xdf2.Datentyp.WAHRHEITSWERT:
            return xdf3.Datentyp.WAHRHEITSWERT
        case xdf2.Datentyp.NUMMER:
            return xdf3.Datentyp.NUMMER
        case xdf2.Datentyp.GANZZAHL:
            return xdf3.Datentyp.GANZZAHL
        case xdf2.Datentyp.GELDBETRAG:
            return xdf3.Datentyp.GELDBETRAG
        case xdf2.Datentyp.ANLAGE:
            return xdf3.Datentyp.ANLAGE
        case xdf2.Datentyp.OBJEKT:
            return xdf3.Datentyp.OBJEKT


def map_status(
    status: xdf2.Status,
    freigabedatum: date | None,
) -> xdf3.FreigabeStatus:
    match status:
        case xdf2.Status.INAKTIV:
            return xdf3.FreigabeStatus.INAKTIV
        case xdf2.Status.IN_VORBEREITUNG:
            return xdf3.FreigabeStatus.IN_BEARBEITUNG
        case xdf2.Status.AKTIV:
            if freigabedatum is not None:
                return xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
            else:
                return xdf3.FreigabeStatus.IN_BEARBEITUNG


def _map_struktur(struktur: list[xdf2.ElementReference]) -> list[xdf3.ElementReference]:
    # This should be checked already
    assert len(struktur) > 0, "xdf3 struktur is empty"

    return [_map_element_reference(ref) for ref in struktur]


def _map_element_reference(ref: xdf2.ElementReference) -> xdf3.ElementReference:
    return xdf3.ElementReference(
        anzahl=ref.anzahl,
        bezug=map_bezug(ref.bezug),
        element_type=ref.element_type,
        identifier=convert_identifier(ref.identifier),
    )


def map_bezug(value: str | None) -> list[xdf3.Rechtsbezug]:
    return (
        [xdf3.Rechtsbezug(text=parse_string_latin(value), link=None)]
        if value is not None
        else []
    )
