from __future__ import annotations
from enum import Enum, IntEnum, auto
from dataclasses import dataclass
from datetime import datetime, date

import ciso8601


class XdfException(Exception):
    pass


class ConversionException(XdfException):
    pass


class ParserException(XdfException):
    def __init__(self, message: str, line: int):
        super().__init__(f"{message}, line {line}")


class InvalidVersionException(XdfException):
    pass


class InvalidPraezisierungException(XdfException):
    pass


class InternalParserException(XdfException):
    pass


class ElementType(IntEnum):
    FELD = auto()
    GRUPPE = auto()


SCHEMA_ELEMENT_ART_CODE_LIST_URI = (
    "urn:xoev-de:fim:codeliste:xdatenfelder.schemaelementart"
)
SCHEMA_ELEMENT_ART_CODE_LIST_VERSION = "1.0"


class SchemaElementArt(Enum):
    ABSTRAKT = "ABS"
    HARMONISIERT = "HAR"
    RECHTSNORMGEBUNDEN = "RNG"

    def to_label(self) -> str:
        match self:
            case SchemaElementArt.ABSTRAKT:
                return "Abstrakt"
            case SchemaElementArt.HARMONISIERT:
                return "Harmonisiert"
            case SchemaElementArt.RECHTSNORMGEBUNDEN:
                return "Rechtsnormgebunden"


def parse_schema_element_art(value: str) -> SchemaElementArt:
    try:
        return SchemaElementArt(value)
    except ValueError as error:
        raise InternalParserException(f"Invalid SchemaElementArt '{value}'") from error


@dataclass(slots=True)
class Anzahl:
    """
    A value describing the cardinality of a child element.

    See: https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xdatenfelder_2.0:dokument:XDatenfelder_Spezifikation
    Section: II.2.1 AnzahlString
    Page: 15

    See: https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xdatenfelder_3.0.0:dokument:XDatenfelder_3.0.0_Spezifikation
    Section: II.2.1 AnzahlString
    Page: 15
    """

    min: int
    max: int | None

    @staticmethod
    def from_string(value: str) -> Anzahl:
        parts = value.split(":")

        if len(parts) != 2:
            raise InternalParserException(f"Invalid `anzahl`: {value}")

        min_str, max_str = parts

        try:
            min_value = int(min_str)

            if max_str == "*":
                max_value = None
            else:
                max_value = int(max_str)
        except ValueError as error:
            raise InternalParserException(f"Invalid `anzahl`: {value}") from error
        else:
            return Anzahl(min_value, max_value)

    def __str__(self):
        return f"{self.min}:{self.max}" if self.max is not None else f"{self.min}:*"

    @property
    def is_optional(self) -> bool:
        return self.min == 0 and self.max == 1

    @property
    def is_array(self) -> bool:
        return self.max == None or self.max > 1


def parse_datetime(value: str) -> datetime:
    try:
        return ciso8601.parse_datetime(value)
    except ValueError as error:
        raise InternalParserException(f"Invalid datetime '{value}'") from error


def parse_optional_date(value: str | None) -> date | None:
    if value is None:
        return None

    try:
        return date.fromisoformat(value)
    except ValueError as error:
        raise InternalParserException(f"Invalid date '{value}'") from error


NUMMERNKREIS_DESCRIPTIONS = {
    0: "Bundesredaktion",
    1: "Landesredaktion Schleswig-Holstein",
    2: "Landesredaktion Freie und Hansestadt Hamburg",
    3: "Landesredaktion Niedersachsen",
    4: "Landesredaktion Freie Hansestadt Bremen",
    5: "Landesredaktion Nordrhein-Westfalen",
    6: "Landesredaktion Hessen",
    7: "Landesredaktion Rheinland-Pfalz",
    8: "Landesredaktion Baden-Württemberg",
    9: "Landesredaktion Freistaat Bayern",
    10: "Landesredaktion Saarland",
    11: "Landesredaktion Berlin",
    12: "Landesredaktion Brandenburg",
    13: "Landesredaktion Mecklenburg-Vorpommern",
    14: "Landesredaktion Freistaat Sachsen",
    15: "Landesredaktion Sachsen-Anhalt",
    16: "Landesredaktion Freistaat Thüringen",
    17: "Virtuelle Landesredaktion für den Aufbau der Inhalte (Bottom-Up)",
    60: "FIM-Bausteine",
    80: "Schulungssystem",
    81: "OZG-Referenzsystem",
    82: "Testrepository",
    98: "Server-lokale IDs",
    99: "Dokument-lokale IDs",
}


def get_nummerkreis_description(nummernkreis_str: str) -> str:
    nummernkreis = int(nummernkreis_str[:2])

    assert 0 <= nummernkreis <= 99

    if nummernkreis in NUMMERNKREIS_DESCRIPTIONS:
        return NUMMERNKREIS_DESCRIPTIONS[nummernkreis]
    elif 20 <= nummernkreis <= 59:
        return "Reserviert"
    elif 61 <= nummernkreis <= 69:
        return "Repositories mit spezieller Bedeutung"
    elif 70 <= nummernkreis <= 79:
        return "Reserviert"
    elif 83 <= nummernkreis <= 89:
        return "Testsysteme"
    elif 90 <= nummernkreis <= 97:
        return "Reserviert für Anwendungen außerhalb des FIM-Kontextes"
    else:
        raise Exception(f"Cannot find description for nummernkreis {nummernkreis}")


ABLEITUNGS_REPR_CODE_LIST_URI = (
    "urn:xoev-de:fim:codeliste:xdatenfelder.ableitungsmodifikationenRepraesentation"
)
ABLEITUNGS_REPR_CODE_LIST_VERSION = "1.0"


class AbleitungsmodifikationenRepraesentation(IntEnum):
    NICHT_MODIFIZIERBAR = 0
    MODIFIZIERBAR = 1

    def to_label(self) -> str:
        return ABLEITUNGSMODIFIKATIONEN_REPRAESENTATION_TO_LABEL[self]


ABLEITUNGSMODIFIKATIONEN_REPRAESENTATION_TO_LABEL: dict[
    AbleitungsmodifikationenRepraesentation, str
] = {
    AbleitungsmodifikationenRepraesentation.NICHT_MODIFIZIERBAR: "Nicht modifizierbar",
    AbleitungsmodifikationenRepraesentation.MODIFIZIERBAR: "Modifizierbar",
}


def parse_ableitungsmodifikationen_repraesentation(
    value: str,
) -> AbleitungsmodifikationenRepraesentation:
    try:
        return AbleitungsmodifikationenRepraesentation(int(value))
    except ValueError as error:
        raise InternalParserException(
            f"Invalid AbleitungsmodifikationenRepraesentation '{value}'"
        ) from error


ABLEITUNGS_STRUKTUR_CODE_LIST_URI = (
    "urn:xoev-de:fim:codeliste:xdatenfelder.ableitungsmodifikationenStruktur"
)
ABLEITUNGS_STRUKTUR_CODE_LIST_VERSION = "1.0"


class AbleitungsmodifikationenStruktur(IntEnum):
    NICHT_MODIFIZIERBAR = 0
    NUR_EINSCHRAENKBAR = 1
    NUR_ERWEITERBAR = 2
    ALLES_MODIFIZIERBAR = 3

    def to_label(self) -> str:
        return ABLEITUNGSMODIFIKATIONEN_STRUKTUR_TO_LABEL[self]


ABLEITUNGSMODIFIKATIONEN_STRUKTUR_TO_LABEL: dict[
    AbleitungsmodifikationenStruktur, str
] = {
    AbleitungsmodifikationenStruktur.NICHT_MODIFIZIERBAR: "Nicht modifizierbar",
    AbleitungsmodifikationenStruktur.NUR_EINSCHRAENKBAR: "Nur einschränkbar",
    AbleitungsmodifikationenStruktur.NUR_ERWEITERBAR: "Nur erweiterbar",
    AbleitungsmodifikationenStruktur.ALLES_MODIFIZIERBAR: "Alles modifizierbar",
}


def parse_ableitungsmodifikationen_struktur(
    value: str,
) -> AbleitungsmodifikationenStruktur:
    try:
        return AbleitungsmodifikationenStruktur(int(value))
    except ValueError as error:
        raise InternalParserException(
            f"Invalid AbleitungsmodifikationenStruktur '{value}'"
        ) from error


def delocalize_number(value: str) -> str:
    """
    Format a string with german separators into a parsable format by:

    1. Removing the thousand separators
    2. Replacing the decimal point with a dot

    Example: 9.999,99 -> 9999.99
    """

    return value.replace(".", "").replace(",", ".")
