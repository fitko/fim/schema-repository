from .common import XdfException
from . import xdf2, xdf3
from fimportal import genericode


def check_code_lists(
    message: xdf2.SchemaMessage | xdf3.SchemaMessage,
    code_lists: dict[str, genericode.CodeList],
):
    """
    Check, that the map of code lists includes all lists required by the schema message.
    """
    missing_code_lists = [
        identifier.canonical_version_uri
        for identifier in message.get_code_list_identifiers()
        if identifier.canonical_version_uri not in code_lists
    ]

    if len(missing_code_lists) > 0:
        raise XdfException(f"Missing code lists: {missing_code_lists}")
