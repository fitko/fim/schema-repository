"""
Create quality reports for xdatenfelder schemas.
The checks itself can be found in `checks.py`.
"""

from __future__ import annotations

from dataclasses import dataclass
from typing import Any, Literal

from . import checks, xdf2, xdf3
from fimportal import genericode

XDF2_ELEMENT_CHECKS = [
    checks.check_bezeichnung_eingabe,
]

XDF2_GROUP_CHECKS: list[checks.Check[xdf2.Gruppe]] = [
    *XDF2_ELEMENT_CHECKS,
    checks.check_no_child_elements,
]

XDF2_FIELD_CHECKS: list[checks.Check[xdf2.Datenfeld]] = [
    *XDF2_ELEMENT_CHECKS,
    checks.check_code_list_identification_invalid_chars,
    checks.check_missing_code_list_reference,
    checks.check_select_datentyp,
    checks.check_unused_praezisierung_for_select,
    checks.check_unused_code_listen_referenz,
]

XDF2_RULE_CHECKS: list[checks.Check[xdf2.Regel]] = []

XDF3_SCHEMA_CHECKS: list[checks.Check[xdf3.SchemaMessage]] = [
    checks.check_element_versions_in_schema,
    checks.check_code_list_versions,
    checks.check_element_status,
    checks.check_fachlicher_ersteller,
    checks.check_struktur_elementart,
    checks.check_handlungsgrundlage,
    checks.check_child_versions,
    checks.check_relation_target,
    checks.check_relation_type,
]
XDF3_GROUP_CHECKS: list[checks.Check[xdf3.Gruppe]] = [
    checks.check_element_versions_in_group,
    checks.check_number_of_children,
    checks.check_element_status,
    checks.check_auswahl_group,
    checks.check_relation_target,
    checks.check_relation_type,
]
XDF3_FIELD_CHECKS: list[checks.Check[xdf3.Datenfeld]] = [
    checks.check_bezug_of_field_harmonisiert,
    checks.check_praezisierung_of_text_field,
    checks.check_praezisierung_of_range_field,
    checks.check_codelist_version,
    checks.check_datatype_auswahl_field,
    checks.check_static_field_vorbefuellung,
    checks.check_element_status,
    checks.check_unused_praezisierung_for_select,
    checks.check_entries_wertelist,
    checks.check_column_definition,
    checks.check_inhalt_werteliste,
    checks.check_missing_code_list_reference,
    checks.check_unused_code_listen_referenz,
    checks.check_relation_target,
    checks.check_relation_type,
]
XDF3_RULE_CHECKS: list[checks.Check[xdf3.Regel]] = []


@dataclass(slots=True)
class QualityReport:
    schema_checks: list[checks.FailingCheck]
    group_reports: list[checks.ElementReport]
    field_reports: list[checks.ElementReport]
    rule_reports: list[checks.ElementReport]
    total_checks: int
    total_group_checks: int
    total_field_checks: int
    total_rule_checks: int

    @staticmethod
    def create(
        schema_checks: list[checks.FailingCheck],
        group_reports: list[checks.ElementReport],
        field_reports: list[checks.ElementReport],
        rule_reports: list[checks.ElementReport],
    ) -> QualityReport:
        total_group_checks = _count_warnings(group_reports)
        total_field_checks = _count_warnings(field_reports)
        total_rule_checks = _count_warnings(rule_reports)
        total_checks = (
            len(schema_checks)
            + total_group_checks
            + total_field_checks
            + total_rule_checks
        )

        return QualityReport(
            schema_checks=schema_checks,
            group_reports=group_reports,
            field_reports=field_reports,
            rule_reports=rule_reports,
            total_checks=total_checks,
            total_group_checks=total_group_checks,
            total_field_checks=total_field_checks,
            total_rule_checks=total_rule_checks,
        )

    @staticmethod
    def from_dict(data: dict[str, Any]) -> QualityReport:
        # NOTE(Felix): This can probably be removed in favor of using the built-in
        # `model_dump` function from pydantic.

        try:
            return QualityReport.create(
                schema_checks=[
                    checks.FailingCheck(
                        check["code"], check["error_type"], check["message"]
                    )
                    for check in data["schema_checks"]
                ],
                group_reports=[
                    checks.ElementReport.from_dict(item)
                    for item in data["group_reports"]
                ],
                field_reports=[
                    checks.ElementReport.from_dict(item)
                    for item in data["field_reports"]
                ],
                rule_reports=[
                    checks.ElementReport.from_dict(item)
                    for item in data["rule_reports"]
                ],
            )
        except Exception as error:
            # NOTE(Felix): This should never happen, as the report is
            # regenerated after every deployment to include potential new warnings.
            # So even if the structure of the report changes, the data in the database
            # should always be valid for current structure.
            # Therefore, if an exception happens, just throw a general exception
            # with a helpful error message. This will then lead correctly to a 500-Response,
            # as we then clearly have a bug somewhere in the code.
            raise Exception(f"Could not parse QualityReport: {data}") from error

    def to_dict(self) -> dict[str, Any]:
        return {
            "schema_checks": [
                {
                    "code": check.code,
                    "error_type": check.error_type,
                    "message": check.message,
                }
                for check in self.schema_checks
            ],
            "group_reports": [report.to_dict() for report in self.group_reports],
            "field_reports": [report.to_dict() for report in self.field_reports],
            "rule_reports": [report.to_dict() for report in self.rule_reports],
        }

    def count_error_type(
        self, error_type: Literal["critical", "warning", "method", "info"]
    ) -> int:
        counter = 0
        for check in self.schema_checks:
            if check.error_type == error_type:
                counter += 1
        for element_report in self.group_reports:
            for check in element_report.failing_checks:
                if check.error_type == error_type:
                    counter += 1
        for element_report in self.field_reports:
            for check in element_report.failing_checks:
                if check.error_type == error_type:
                    counter += 1
        for element_report in self.rule_reports:
            for check in element_report.failing_checks:
                if check.error_type == error_type:
                    counter += 1

        return counter


def _count_warnings(reports: list[checks.ElementReport]) -> int:
    return sum([len(report.failing_checks) for report in reports])


def check_xdf2_quality(
    message: xdf2.SchemaMessage,
    code_lists: dict[str, genericode.CodeList],
) -> QualityReport:
    """
    Create a quality report for an xdf2 schema message.
    """
    schema_checks: list[checks.FailingCheck] = []

    field_reports = [
        check_xdf2_field(field, code_lists) for field in message.fields.values()
    ]
    group_reports = [check_xdf2_group(group) for group in message.groups.values()]
    rule_reports = [check_xdf2_rule(rule) for rule in message.rules.values()]

    return QualityReport.create(
        schema_checks=schema_checks,
        field_reports=field_reports,
        group_reports=group_reports,
        rule_reports=rule_reports,
    )


def check_xdf2_group(group: xdf2.Gruppe) -> checks.ElementReport:
    return checks.process_checks(group, XDF2_GROUP_CHECKS)


def check_xdf2_field(
    field: xdf2.Datenfeld, code_lists: dict[str, genericode.CodeList]
) -> checks.ElementReport:
    failing_checks: list[checks.FailingCheck] = []

    for check in XDF2_FIELD_CHECKS:
        check(failing_checks, field)

    checks.check_inhalt_codeliste(failing_checks, field, code_lists)
    checks.check_code_list_availability(failing_checks, field, code_lists)

    return checks.ElementReport(
        identifier=field.identifier,
        failing_checks=failing_checks,
    )


def check_xdf2_rule(rule: xdf2.Regel) -> checks.ElementReport:
    return checks.process_checks(rule, XDF2_RULE_CHECKS)


def check_xdf3_quality(
    message: xdf3.SchemaMessage,
    code_lists: dict[str, genericode.CodeList],
) -> QualityReport:
    """
    Create a quality report for an xdf3 schema message.
    """
    schema_report = checks.process_checks(message, XDF3_SCHEMA_CHECKS)
    field_reports = [
        check_xdf3_field(field, code_lists) for field in message.fields.values()
    ]
    group_reports = [check_xdf3_group(group) for group in message.groups.values()]
    rule_reports = [check_xdf3_rule(rule) for rule in message.rules.values()]

    return QualityReport.create(
        schema_checks=schema_report.failing_checks,
        field_reports=field_reports,
        group_reports=group_reports,
        rule_reports=rule_reports,
    )


def check_xdf3_group(group: xdf3.Gruppe) -> checks.ElementReport:
    return checks.process_checks(group, XDF3_GROUP_CHECKS)


def check_xdf3_field(
    field: xdf3.Datenfeld, code_lists: dict[str, genericode.CodeList]
) -> checks.ElementReport:
    failing_checks: list[checks.FailingCheck] = []

    for check in XDF3_FIELD_CHECKS:
        check(failing_checks, field)

    checks.check_inhalt_codeliste(failing_checks, field, code_lists)
    checks.check_code_list_availability(failing_checks, field, code_lists)

    return checks.ElementReport(
        identifier=field.identifier,
        failing_checks=failing_checks,
    )


def check_xdf3_rule(rule: xdf3.Regel) -> checks.ElementReport:
    return checks.process_checks(rule, XDF3_RULE_CHECKS)
