import xml.etree.ElementTree as ET
from dataclasses import dataclass
from pathlib import Path

from fimportal import genericode, xml
from fimportal.xdatenfelder.common import (
    InternalParserException,
    InvalidVersionException,
    ParserException,
)
from fimportal.xdatenfelder.xdf2.common import (
    Datenfeld,
    Identifier,
    MessageHeader,
    ParseContext,
    Regel,
    parse_datenfeld,
    parse_message_header,
    serialize_field,
    serialize_header,
)
from fimportal.xdatenfelder.xdf2.constants import (
    XDF2_DATENFELD,
    XDF2_DATENFELD_MESSAGE,
    XDF2_MESSAGE_HEADER,
)


@dataclass(slots=True)
class DatenfeldMessage:
    """
    The top-level data structure containing all the information
    of a parsed xdf2 Datenfeld message.
    """

    header: MessageHeader

    field: Datenfeld
    rules: dict[Identifier, Regel]

    @property
    def id(self) -> str:
        return self.field.identifier.id

    @property
    def version(self) -> str | None:
        return self.field.identifier.version

    def assert_version(self) -> str:
        return self.field.identifier.assert_version()


def load_datenfeld_message(path: str | Path) -> DatenfeldMessage:
    """
    Load and parse an xdf2 Datenfeld message from a file.
    """

    with open(path, "rb") as file:
        return parse_datenfeld_message(file.read())


def parse_datenfeld_message(input: bytes | str) -> DatenfeldMessage:
    """
    Parse an xdf2 Datenfeld message from the provided input.
    """

    if isinstance(input, str):
        input = input.encode("utf-8")

    parser = xml.XmlParser(input)
    context = ParseContext()

    try:
        parser.expect_child(XDF2_DATENFELD_MESSAGE)

        header = xml.ParsedValue[MessageHeader]()
        field = xml.ParsedValue[Datenfeld]()

        while (child := parser.next_child()) is not None:
            if child.tag == XDF2_MESSAGE_HEADER:
                header.set(parse_message_header(parser))
            elif child.tag == XDF2_DATENFELD:
                field.set(parse_datenfeld(parser, context))
            else:
                parser.skip_node()

        return DatenfeldMessage(
            header=header.expect(XDF2_MESSAGE_HEADER),
            field=field.expect(XDF2_DATENFELD),
            rules=context.rules,
        )
    except (
        xml.ParserException,
        genericode.GenericodeException,
        InternalParserException,
        InvalidVersionException,
    ) as error:
        raise ParserException(
            f"Could not parse Datenfeld: {str(error)}", parser.current_line
        ) from error


def serialize_datenfeld_message(message: DatenfeldMessage) -> bytes:
    """
    Serialize the xdf2 Datenfeld message into an xml string.
    """

    root = ET.Element(XDF2_DATENFELD_MESSAGE)
    root.append(serialize_header(message.header))
    root.append(serialize_field(message.field, rule_map=message.rules))

    return ET.tostring(root, encoding="utf-8", xml_declaration=True)
