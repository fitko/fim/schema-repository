"""Immutable XDF2 Datenfelder

When a schema (gruppe, feld, steckbrief) is imported again (same id, same version),
the XDatenfelder standard considers most data as immutable, meaning it cannot be
changed once defined. Only certain attributes are allowed to change, and the
import gets declined if the corresponding immutable classes of both variants
are not the same.

Explicitly mutable xdf3 attributes are:
    - freigabestatus
    - statusGesetztAm
    - veroeffentlichungsdatum - with a caveat (can be set once and then becomes immutable)
    - letzteAenderung
    - relation
    - dokumentsteckbrief (only Schema)

They correspond to these xdf2 attributes (in order):
    - status
    - freigabedatum
    - veroeffentlichungsdatum
    - none (letzteAenderung does not exist in xdf2)
    - none (relation does not exist in xdf2)
    - none (schema-dokumentsteckbrief does not exist in xdf2)

The mutable xdf2 attributes are part of:
    - status: AllgemeineAngaben
    - freigabedatum: AllgemeineAngaben
    - veroeffentlichungsdatum: AllgemeineAngaben
"""

from __future__ import annotations

from datetime import date

from pydantic import BaseModel

from fimportal import din91379
from fimportal.xdatenfelder.common import (
    AbleitungsmodifikationenRepraesentation,
    AbleitungsmodifikationenStruktur,
    Anzahl,
    ElementType,
    SchemaElementArt,
)
from fimportal.xdatenfelder.xdf2.common import (
    AllgemeineAngaben,
    CodeListenReferenz,
    Datenfeld,
    Datentyp,
    ElementReference,
    Feldart,
    Gruppe,
    Identifier,
    Regel,
    SchemaElementAngaben,
)
from fimportal.xdatenfelder.xdf2.schema_message import Schema
from fimportal.xdatenfelder.xdf2.steckbrief_message import Steckbrief


class ImmutableAllgemeineAngaben(BaseModel):
    """Immutable xdf2 Allgemeine Angaben

    Explicitly mutable attributes are `status`, `freigabedatum`,
    `veroeffentlichungsdatum.

    `veroeffentlichungsdatum` is mutable while it is `None`. Once assigned a date value, it
    becomes immutable. We manage its immutability seperately. For the purpose of these
    classes, `veroeffentlichungsdatum` is treated as mutable.
    """

    identifier: Identifier
    name: str
    bezeichnung_eingabe: str | None
    bezeichnung_ausgabe: str | None
    beschreibung: str | None
    definition: str | None
    bezug: str | None
    versionshinweis: str | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    fachlicher_ersteller: str | None

    @staticmethod
    def from_allgemeine_angaben(general_information: AllgemeineAngaben):
        return ImmutableAllgemeineAngaben(
            identifier=general_information.identifier,
            name=general_information.name,
            bezeichnung_eingabe=general_information.bezeichnung_eingabe,
            bezeichnung_ausgabe=general_information.bezeichnung_ausgabe,
            beschreibung=general_information.beschreibung,
            definition=general_information.definition,
            bezug=general_information.bezug,
            versionshinweis=general_information.versionshinweis,
            gueltig_ab=general_information.gueltig_ab,
            gueltig_bis=general_information.gueltig_bis,
            fachlicher_ersteller=general_information.fachlicher_ersteller,
        )


class ImmutableStrukturElement(BaseModel):
    anzahl: Anzahl
    bezug: str | None
    element_type: ElementType
    enthaelt: Identifier

    @staticmethod
    def from_element_reference(element_reference: ElementReference):
        return ImmutableStrukturElement(
            anzahl=element_reference.anzahl,
            bezug=element_reference.bezug,
            element_type=element_reference.element_type,
            enthaelt=element_reference.identifier,
        )

    def to_element_reference(self):
        return ElementReference(
            anzahl=self.anzahl,
            bezug=self.bezug,
            element_type=self.element_type,
            identifier=self.enthaelt,
        )


class ImmutableRegel(ImmutableAllgemeineAngaben):
    script: str | None

    @staticmethod
    def from_regel(rule: Regel):
        return ImmutableRegel(
            **dict(ImmutableAllgemeineAngaben.from_allgemeine_angaben(rule)),
            script=rule.script,
        )


class ImmutableSchema(ImmutableAllgemeineAngaben):
    hilfetext: din91379.StringLatin | None
    ableitungsmodifikationen_struktur: AbleitungsmodifikationenStruktur
    ableitungsmodifikationen_repraesentation: AbleitungsmodifikationenRepraesentation
    regeln: list[Identifier]
    struktur: list[ImmutableStrukturElement]

    @staticmethod
    def from_schema(
        schema: Schema,
    ):
        return ImmutableSchema(
            **dict(ImmutableAllgemeineAngaben.from_allgemeine_angaben(schema)),
            hilfetext=schema.hilfetext,
            ableitungsmodifikationen_struktur=schema.ableitungsmodifikationen_struktur,
            ableitungsmodifikationen_repraesentation=schema.ableitungsmodifikationen_repraesentation,
            regeln=schema.regeln,
            struktur=[
                ImmutableStrukturElement.from_element_reference(i)
                for i in schema.struktur
            ],
        )


class ImmutableSchemaElementAngaben(ImmutableAllgemeineAngaben):
    hilfetext_eingabe: str | None
    hilfetext_ausgabe: str | None
    schema_element_art: SchemaElementArt

    @staticmethod
    def from_schema_element_angaben(element_information: SchemaElementAngaben):
        return ImmutableSchemaElementAngaben(
            **dict(
                ImmutableAllgemeineAngaben.from_allgemeine_angaben(element_information)
            ),
            hilfetext_eingabe=element_information.hilfetext_eingabe,
            hilfetext_ausgabe=element_information.hilfetext_ausgabe,
            schema_element_art=element_information.schema_element_art,
        )


class ImmutableGruppe(ImmutableSchemaElementAngaben):
    regeln: list[Identifier]
    struktur: list[ImmutableStrukturElement]

    @staticmethod
    def from_gruppe(
        group: Gruppe,
    ):
        return ImmutableGruppe(
            **dict(ImmutableSchemaElementAngaben.from_schema_element_angaben(group)),
            regeln=group.regeln,
            struktur=[
                ImmutableStrukturElement.from_element_reference(i)
                for i in group.struktur
            ],
        )


class ImmutableFeld(ImmutableSchemaElementAngaben):
    feldart: Feldart
    datentyp: Datentyp
    praezisierung: str | None
    inhalt: str | None
    code_listen_referenz: CodeListenReferenz | None
    regeln: list[Identifier]

    @staticmethod
    def from_datenfeld(
        field: Datenfeld,
    ):
        return ImmutableFeld(
            **dict(ImmutableSchemaElementAngaben.from_schema_element_angaben(field)),
            feldart=field.feldart,
            datentyp=field.datentyp,
            praezisierung=field.praezisierung,
            inhalt=field.inhalt,
            code_listen_referenz=field.code_listen_referenz,
            regeln=field.regeln,
        )


class ImmutableSteckbrief(ImmutableAllgemeineAngaben):
    is_referenz: bool
    dokumentart: str
    hilfetext: str | None

    @staticmethod
    def from_steckbrief(steckbrief: Steckbrief):
        return ImmutableSteckbrief(
            **dict(ImmutableAllgemeineAngaben.from_allgemeine_angaben(steckbrief)),
            is_referenz=steckbrief.is_referenz,
            dokumentart=steckbrief.dokumentart,
            hilfetext=steckbrief.hilfetext,
        )
