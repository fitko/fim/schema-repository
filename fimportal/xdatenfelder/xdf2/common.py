from __future__ import annotations

import re
import xml.etree.ElementTree as ET
from dataclasses import dataclass, field
from datetime import date, datetime
from enum import Enum
from typing import NewType

import orjson

from fimportal import genericode, xml
from fimportal.xdatenfelder.common import (
    SCHEMA_ELEMENT_ART_CODE_LIST_URI,
    SCHEMA_ELEMENT_ART_CODE_LIST_VERSION,
    Anzahl,
    ElementType,
    InternalParserException,
    InvalidPraezisierungException,
    InvalidVersionException,
    SchemaElementArt,
    XdfException,
    delocalize_number,
    parse_datetime,
    parse_optional_date,
    parse_schema_element_art,
)
from fimportal.xdatenfelder.xdf2.constants import (
    DATENTYP_CODE_LIST_URI,
    DATENTYP_CODE_LIST_VERSION,
    FELDART_CODE_LIST_URI,
    FELDART_CODE_LIST_VERSION,
    STATUS_CODE_LIST_URI,
    STATUS_CODE_LIST_VERSION,
    XDF2_ANZAHL,
    XDF2_BESCHREIBUNG,
    XDF2_BEZEICHNUNG_AUSGABE,
    XDF2_BEZEICHNUNG_EINGABE,
    XDF2_BEZUG,
    XDF2_CANONICAL_IDENTIFICATION,
    XDF2_CANONICAL_VERSION_URI,
    XDF2_CODELISTE_REFERENZ,
    XDF2_CREATION_TIME,
    XDF2_DATENFELD,
    XDF2_DATENFELDGRUPPE,
    XDF2_DATENTYP,
    XDF2_DEFINITION,
    XDF2_ENTHAELT,
    XDF2_FACHLICHER_ERSTELLER,
    XDF2_FELDART,
    XDF2_FREIGABEDATUM,
    XDF2_GENERICODE_IDENTIFIKATION,
    XDF2_GUELTIG_AB,
    XDF2_GUELTIG_BIS,
    XDF2_HILFETEXT_AUSGABE,
    XDF2_HILFETEXT_EINGABE,
    XDF2_ID,
    XDF2_IDENTIFIKATION,
    XDF2_INHALT,
    XDF2_MESSAGE_HEADER,
    XDF2_MESSAGE_ID,
    XDF2_NAME,
    XDF2_PRAEZISIERUNG,
    XDF2_REFERENZ_ID,
    XDF2_REGEL,
    XDF2_SCHEMA_ELEMENT_ART,
    XDF2_SCRIPT,
    XDF2_STATUS,
    XDF2_STRUKTUR,
    XDF2_VEROEFFENTLICHUNGSDATUM,
    XDF2_VERSION,
    XDF2_VERSIONSHINWEIS,
)


@dataclass(slots=True, frozen=True)
class MessageHeader:
    """
    The header of an xdf2 message.
    """

    nachricht_id: str
    erstellungs_zeitpunkt: datetime
    referenz_id: str | None


def parse_message_header(parser: xml.XmlParser) -> MessageHeader:
    nachricht_id: xml.ParsedValue[str] = xml.ParsedValue()
    erstellungs_zeitpunt: xml.ParsedValue[datetime] = xml.ParsedValue()
    referenz_id: xml.ParsedValue[str] = xml.ParsedValue()

    while (child := parser.next_child()) is not None:
        if child.tag == XDF2_MESSAGE_ID:
            nachricht_id.set(parser.parse_token(XDF2_MESSAGE_ID))
        elif child.tag == XDF2_CREATION_TIME:
            erstellungs_zeitpunt.set(
                parse_datetime(parser.parse_value(XDF2_CREATION_TIME))
            )
        elif child.tag == XDF2_REFERENZ_ID:
            referenz_id.set(parser.parse_optional_token())
        else:
            parser.skip_node()

    return MessageHeader(
        nachricht_id=nachricht_id.expect(XDF2_MESSAGE_ID),
        erstellungs_zeitpunkt=erstellungs_zeitpunt.expect(XDF2_CREATION_TIME),
        referenz_id=referenz_id.get(),
    )


VERSION_REGEX = re.compile(r"^\d+\.\d+$")
Version = NewType("Version", str)


def parse_version(value: str) -> Version:
    if VERSION_REGEX.match(value) is not None:
        return Version(value)
    else:
        raise InvalidVersionException(f"Invalid version: {value}")


Nummernkreis = NewType("Nummernkreis", str)


@dataclass(slots=True, unsafe_hash=True)
class Identifier:
    """
    Uniquely identifies a schema, data group, data field, or rule.
    """

    id: str
    version: Version | None
    nummernkreis: Nummernkreis = field(init=False)

    def __post_init__(self):
        # TODO(Felix): Validate, that the id is formatted correctly
        if len(self.id) < 3:
            raise InternalParserException(f"invalid id {self.id}")

        part = self.id[1:3]
        try:
            int(part)
        except Exception as error:
            raise InternalParserException(f"invalid id {self.id}") from error

        self.nummernkreis = Nummernkreis(part)

    def get_xdf3_nummernkreis(self) -> str:
        """
        Expand the nummernkreis to the corresponding default xdf3 nummernkreis
        """

        return self.nummernkreis + "000"

    def assert_version(self) -> Version:
        assert self.version is not None
        return self.version

    def is_local(self) -> bool:
        """
        Returns whether or not the element should be in the schema local namespace.
        """

        return self.nummernkreis.startswith("99")


class Status(Enum):
    IN_VORBEREITUNG = "inVorbereitung"
    AKTIV = "aktiv"
    INAKTIV = "inaktiv"


def parse_status(value: str) -> Status:
    try:
        return Status(value)
    except ValueError as error:
        raise InternalParserException(f"Invalid Status '{value}'") from error


@dataclass(slots=True)
class AllgemeineAngaben:
    """XDF2 Allgemeine Angaben

    This implementation differs from the XDF2 standard: we allow
    `bezeichnung_eingabe` to be `None`.

    This is because many schema files, in practice, do not include this
    attribute, and we want them to be importable to the FIM-Portal.
    """

    identifier: Identifier
    name: str
    bezeichnung_eingabe: str | None
    bezeichnung_ausgabe: str | None
    beschreibung: str | None
    definition: str | None
    bezug: str | None
    status: Status
    versionshinweis: str | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    fachlicher_ersteller: str | None
    freigabedatum: date | None
    veroeffentlichungsdatum: date | None


def _parse_identifier(parser: xml.XmlParser) -> Identifier:
    id: xml.ParsedValue[str] = xml.ParsedValue()
    version: xml.ParsedValue[Version] = xml.ParsedValue()

    while (child := parser.next_child()) is not None:
        if child.tag == XDF2_ID:
            id.set(parser.parse_token(XDF2_ID))
        elif child.tag == XDF2_VERSION:
            version_value = parser.parse_optional_value()
            if version_value is not None:
                version_value = parse_version(version_value)
            version.set(version_value)
        else:
            parser.skip_node()

    return Identifier(
        id=id.expect(XDF2_ID),
        version=version.get(),
    )


class AllgemeineAngabenContainer:
    def __init__(self):
        self.identifier: xml.ParsedValue[Identifier] = xml.ParsedValue()
        self.name: xml.ParsedValue[str] = xml.ParsedValue()
        self.bezeichnung_eingabe: xml.ParsedValue[str] = xml.ParsedValue()
        self.bezeichnung_ausgabe: xml.ParsedValue[str] = xml.ParsedValue()
        self.beschreibung: xml.ParsedValue[str] = xml.ParsedValue()
        self.definition: xml.ParsedValue[str] = xml.ParsedValue()
        self.bezug: xml.ParsedValue[str] = xml.ParsedValue()
        self.status: xml.ParsedValue[Status] = xml.ParsedValue()
        self.gueltig_ab: xml.ParsedValue[date] = xml.ParsedValue()
        self.gueltig_bis: xml.ParsedValue[date] = xml.ParsedValue()
        self.fachlicher_ersteller: xml.ParsedValue[str] = xml.ParsedValue()
        self.versionshinweis: xml.ParsedValue[str] = xml.ParsedValue()
        self.freigabedatum: xml.ParsedValue[date] = xml.ParsedValue()
        self.veroeffentlichungsdatum: xml.ParsedValue[date] = xml.ParsedValue()

    def handle_child(self, parser: xml.XmlParser, child: xml.XmlElement) -> bool:  # type: ignore
        if child.tag == XDF2_IDENTIFIKATION:
            self.identifier.set(_parse_identifier(parser))
        elif child.tag == XDF2_NAME:
            self.name.set(parser.parse_value(XDF2_NAME))
        elif child.tag == XDF2_FACHLICHER_ERSTELLER:
            self.fachlicher_ersteller.set(parser.parse_optional_value())
        elif child.tag == XDF2_BEZEICHNUNG_EINGABE:
            self.bezeichnung_eingabe.set(parser.parse_optional_value())
        elif child.tag == XDF2_BEZEICHNUNG_AUSGABE:
            self.bezeichnung_ausgabe.set(parser.parse_optional_value())
        elif child.tag == XDF2_BESCHREIBUNG:
            self.beschreibung.set(parser.parse_optional_value())
        elif child.tag == XDF2_DEFINITION:
            self.definition.set(parser.parse_optional_value())
        elif child.tag == XDF2_BEZUG:
            self.bezug.set(parser.parse_optional_value())
        elif child.tag == XDF2_STATUS:
            self.status.set(parse_status(parser.parse_code()))
        elif child.tag == XDF2_VERSIONSHINWEIS:
            self.versionshinweis.set(parser.parse_optional_value())
        elif child.tag == XDF2_GUELTIG_AB:
            self.gueltig_ab.set(parse_optional_date(parser.parse_optional_value()))
        elif child.tag == XDF2_GUELTIG_BIS:
            self.gueltig_bis.set(parse_optional_date(parser.parse_optional_value()))
        elif child.tag == XDF2_FREIGABEDATUM:
            self.freigabedatum.set(
                parse_optional_date(parser.parse_value(XDF2_FREIGABEDATUM))
            )
        elif child.tag == XDF2_VEROEFFENTLICHUNGSDATUM:
            self.veroeffentlichungsdatum.set(
                parse_optional_date(parser.parse_value(XDF2_VEROEFFENTLICHUNGSDATUM))
            )
        else:
            return False

        return True

    def finish(self) -> AllgemeineAngaben:
        return AllgemeineAngaben(
            identifier=self.identifier.expect(XDF2_IDENTIFIKATION),
            name=self.name.expect(XDF2_NAME),
            fachlicher_ersteller=self.fachlicher_ersteller.get(),
            bezeichnung_eingabe=self.bezeichnung_eingabe.get(),
            bezeichnung_ausgabe=self.bezeichnung_ausgabe.get(),
            beschreibung=self.beschreibung.get(),
            definition=self.definition.get(),
            bezug=self.bezug.get(),
            status=self.status.expect(XDF2_STATUS),
            versionshinweis=self.versionshinweis.get(),
            gueltig_ab=self.gueltig_ab.get(),
            gueltig_bis=self.gueltig_bis.get(),
            freigabedatum=self.freigabedatum.get(),
            veroeffentlichungsdatum=self.veroeffentlichungsdatum.get(),
        )


@dataclass(slots=True)
class ParseContext:
    groups: dict[Identifier, Gruppe] = field(default_factory=dict)
    fields: dict[Identifier, Datenfeld] = field(default_factory=dict)
    rules: dict[Identifier, Regel] = field(default_factory=dict)

    def add_group(self, group: Gruppe):
        saved_group = self.groups.get(group.identifier)
        if saved_group is not None:
            if saved_group != group:
                raise InternalParserException(
                    f"inconsistent Datenfeldgruppe [id={group.identifier.id}, version={group.identifier.version}, old={saved_group}, new={group}]"
                )
        else:
            self.groups[group.identifier] = group

    def add_field(self, field: Datenfeld):
        saved_field = self.fields.get(field.identifier)
        if saved_field is not None:
            if saved_field != field:
                raise InternalParserException(
                    f"inconsistent Datenfeld [id={field.identifier.id}, version={field.identifier.version}, old={saved_field}, new={field}]"
                )
        else:
            self.fields[field.identifier] = field

    def add_rule(self, rule: Regel):
        saved_rule = self.rules.get(rule.identifier)
        if saved_rule is not None:
            if saved_rule != rule:
                raise InternalParserException(
                    f"inconsistent Regel [id={rule.identifier.id}, version={rule.identifier.version}, old={saved_rule}, new={rule}]"
                )
        else:
            self.rules[rule.identifier] = rule


@dataclass(slots=True)
class Constraints:
    min_length: int | None = None
    max_length: int | None = None
    min_value: float | None = None
    max_value: float | None = None
    pattern: str | None = None

    @staticmethod
    def parse(praezisierung: str | None) -> Constraints:
        constraints = Constraints()

        if praezisierung is None:
            return constraints

        try:
            value_dict = orjson.loads(praezisierung)

            if not isinstance(value_dict, dict):
                raise InvalidPraezisierungException(
                    f"Cannot parse Praezisierung: {praezisierung} with type {type(value_dict)}"
                )

            for key, value in value_dict.items():
                value = str(value)
                if value == "":
                    continue

                if key == "minLength":
                    constraints.min_length = int(delocalize_number(value))
                elif key == "maxLength":
                    constraints.max_length = int(delocalize_number(value))
                elif key == "minValue":
                    constraints.min_value = float(delocalize_number(value))
                elif key == "maxValue":
                    constraints.max_value = float(delocalize_number(value))
                elif key == "pattern":
                    constraints.pattern = value
                else:
                    # Ignore unknown keys
                    pass
        except InvalidPraezisierungException as error:
            raise error
        except Exception as error:
            raise InvalidPraezisierungException(
                f"Cannot parse Praezisierung: {praezisierung}"
            ) from error
        else:
            return constraints

    def is_empty(self):
        return all(
            val is None
            for val in [
                self.min_length,
                self.max_length,
                self.min_value,
                self.max_value,
                self.pattern,
            ]
        )


@dataclass(slots=True)
class ElementReference:
    anzahl: Anzahl
    bezug: str | None
    element_type: ElementType
    identifier: Identifier  # Identifier of the reference child


@dataclass(slots=True)
class SchemaElementAngaben(AllgemeineAngaben):
    hilfetext_eingabe: str | None
    hilfetext_ausgabe: str | None
    schema_element_art: SchemaElementArt


@dataclass(slots=True)
class Gruppe(SchemaElementAngaben):
    regeln: list[Identifier]
    struktur: list[ElementReference]


@dataclass(slots=True)
class Datenfeld(SchemaElementAngaben):
    feldart: Feldart
    datentyp: Datentyp
    praezisierung: str | None
    inhalt: str | None
    code_listen_referenz: CodeListenReferenz | None
    regeln: list[Identifier]

    def get_constraints(self) -> Constraints:
        return Constraints.parse(self.praezisierung)


@dataclass(slots=True, frozen=True)
class CodeListenReferenz:
    identifier: CodeListIdentifier
    genericode_identifier: genericode.Identifier


@dataclass(slots=True, frozen=True)
class CodeListIdentifier:
    id: str
    version: str | None


class Feldart(Enum):
    INPUT = "input"
    SELECT = "select"
    LABEL = "label"


def _parse_feldart(value: str) -> Feldart:
    try:
        return Feldart(value)
    except ValueError as error:
        raise InternalParserException(f"Invalid Feldart '{value}'") from error


class Datentyp(Enum):
    TEXT = "text"
    DATUM = "date"
    WAHRHEITSWERT = "bool"
    NUMMER = "num"
    GANZZAHL = "num_int"
    GELDBETRAG = "num_currency"
    ANLAGE = "file"
    OBJEKT = "obj"


def _parse_datentyp(value: str) -> Datentyp:
    try:
        return Datentyp(value)
    except ValueError as error:
        raise InternalParserException(f"Invalid Datentyp '{value}'") from error


@dataclass(slots=True)
class Regel(AllgemeineAngaben):
    script: str | None


def parse_structure(parser: xml.XmlParser, context: ParseContext) -> ElementReference:
    anzahl: xml.ParsedValue[Anzahl] = xml.ParsedValue()
    bezug: xml.ParsedValue[str] = xml.ParsedValue()
    reference: xml.ParsedValue[tuple[ElementType, Identifier]] = xml.ParsedValue()

    while (child := parser.next_child()) is not None:
        if child.tag == XDF2_ENTHAELT:
            reference.set(_parse_contains(parser, context))
        elif child.tag == XDF2_ANZAHL:
            anzahl.set(Anzahl.from_string(parser.parse_value(XDF2_ANZAHL)))
        elif child.tag == XDF2_BEZUG:
            bezug.set(parser.parse_optional_value())
        else:
            parser.skip_node()

    element_type, identifier = reference.expect(XDF2_ENTHAELT)

    return ElementReference(
        anzahl=anzahl.expect(XDF2_ANZAHL),
        bezug=bezug.get(),
        element_type=element_type,
        identifier=identifier,
    )


def _parse_contains(
    parser: xml.XmlParser, context: ParseContext
) -> tuple[ElementType, Identifier]:
    reference: xml.ParsedValue[tuple[ElementType, Identifier]] = xml.ParsedValue()

    while (child := parser.next_child()) is not None:
        if child.tag == XDF2_DATENFELDGRUPPE:
            group = parse_datenfeldgruppe(parser, context)
            context.add_group(group)
            reference.set((ElementType.GRUPPE, group.identifier))
        elif child.tag == XDF2_DATENFELD:
            field = parse_datenfeld(parser, context)
            context.add_field(field)
            reference.set((ElementType.FELD, field.identifier))
        else:
            parser.skip_node()

    return reference.expect(f"{XDF2_DATENFELDGRUPPE} or {XDF2_DATENFELD}")


def parse_datenfeldgruppe(parser: xml.XmlParser, context: ParseContext) -> Gruppe:
    container = SchemaElementAngabenContainer()
    struktur: list[ElementReference] = []
    regeln: list[Identifier] = []

    while (child := parser.next_child()) is not None:
        if container.handle_child(parser, child):
            continue

        if child.tag == XDF2_STRUKTUR:
            struktur.append(parse_structure(parser, context))
        elif child.tag == XDF2_REGEL:
            regeln.append(parse_rule(parser, context))
        else:
            parser.skip_node()

    element_angaben = container.finish()

    return Gruppe(
        identifier=element_angaben.identifier,
        name=element_angaben.name,
        fachlicher_ersteller=element_angaben.fachlicher_ersteller,
        bezeichnung_eingabe=element_angaben.bezeichnung_eingabe,
        bezeichnung_ausgabe=element_angaben.bezeichnung_ausgabe,
        beschreibung=element_angaben.beschreibung,
        definition=element_angaben.definition,
        bezug=element_angaben.bezug,
        status=element_angaben.status,
        versionshinweis=element_angaben.versionshinweis,
        gueltig_ab=element_angaben.gueltig_ab,
        gueltig_bis=element_angaben.gueltig_bis,
        freigabedatum=element_angaben.freigabedatum,
        veroeffentlichungsdatum=element_angaben.veroeffentlichungsdatum,
        hilfetext_eingabe=element_angaben.hilfetext_eingabe,
        hilfetext_ausgabe=element_angaben.hilfetext_ausgabe,
        schema_element_art=element_angaben.schema_element_art,
        struktur=struktur,
        regeln=regeln,
    )


def parse_datenfeld(parser: xml.XmlParser, context: ParseContext) -> Datenfeld:
    container = SchemaElementAngabenContainer()
    feldart: xml.ParsedValue[Feldart] = xml.ParsedValue()
    datentyp: xml.ParsedValue[Datentyp] = xml.ParsedValue()
    praezisierung: xml.ParsedValue[str] = xml.ParsedValue()
    inhalt: xml.ParsedValue[str] = xml.ParsedValue()
    code_listen_referenz: xml.ParsedValue[CodeListenReferenz] = xml.ParsedValue()
    regeln: list[Identifier] = []

    while (child := parser.next_child()) is not None:
        if container.handle_child(parser, child):
            continue

        if child.tag == XDF2_FELDART:
            feldart.set(_parse_feldart(parser.parse_code()))
        elif child.tag == XDF2_DATENTYP:
            datentyp.set(_parse_datentyp(parser.parse_code()))
        elif child.tag == XDF2_PRAEZISIERUNG:
            praezisierung.set(parser.parse_optional_value())
        elif child.tag == XDF2_INHALT:
            inhalt.set(parser.parse_optional_value())
        elif child.tag == XDF2_CODELISTE_REFERENZ:
            code_listen_referenz.set(_parse_code_listen_referenz(parser))
        elif child.tag == XDF2_REGEL:
            regeln.append(parse_rule(parser, context))
        else:
            parser.skip_node()

    element_angaben = container.finish()

    return Datenfeld(
        identifier=element_angaben.identifier,
        name=element_angaben.name,
        fachlicher_ersteller=element_angaben.fachlicher_ersteller,
        bezeichnung_eingabe=element_angaben.bezeichnung_eingabe,
        bezeichnung_ausgabe=element_angaben.bezeichnung_ausgabe,
        beschreibung=element_angaben.beschreibung,
        definition=element_angaben.definition,
        bezug=element_angaben.bezug,
        status=element_angaben.status,
        versionshinweis=element_angaben.versionshinweis,
        gueltig_ab=element_angaben.gueltig_ab,
        gueltig_bis=element_angaben.gueltig_bis,
        freigabedatum=element_angaben.freigabedatum,
        veroeffentlichungsdatum=element_angaben.veroeffentlichungsdatum,
        hilfetext_eingabe=element_angaben.hilfetext_eingabe,
        hilfetext_ausgabe=element_angaben.hilfetext_ausgabe,
        schema_element_art=element_angaben.schema_element_art,
        feldart=feldart.expect(XDF2_FELDART),
        datentyp=datentyp.expect(XDF2_DATENTYP),
        praezisierung=praezisierung.get(),
        inhalt=inhalt.get(),
        code_listen_referenz=code_listen_referenz.get(),
        regeln=regeln,
    )


def parse_rule(parser: xml.XmlParser, context: ParseContext) -> Identifier:
    allgemeine_angaben_container = AllgemeineAngabenContainer()
    script: xml.ParsedValue[str] = xml.ParsedValue()

    while (child := parser.next_child()) is not None:
        if allgemeine_angaben_container.handle_child(parser, child):
            continue

        if child.tag == XDF2_SCRIPT:
            script.set(parser.parse_optional_value())
        else:
            parser.skip_node()

    allgemeine_angaben = allgemeine_angaben_container.finish()

    rule = Regel(
        identifier=allgemeine_angaben.identifier,
        name=allgemeine_angaben.name,
        fachlicher_ersteller=allgemeine_angaben.fachlicher_ersteller,
        bezeichnung_eingabe=allgemeine_angaben.bezeichnung_eingabe,
        bezeichnung_ausgabe=allgemeine_angaben.bezeichnung_ausgabe,
        beschreibung=allgemeine_angaben.beschreibung,
        definition=allgemeine_angaben.definition,
        bezug=allgemeine_angaben.bezug,
        status=allgemeine_angaben.status,
        versionshinweis=allgemeine_angaben.versionshinweis,
        gueltig_ab=allgemeine_angaben.gueltig_ab,
        gueltig_bis=allgemeine_angaben.gueltig_bis,
        freigabedatum=allgemeine_angaben.freigabedatum,
        veroeffentlichungsdatum=allgemeine_angaben.veroeffentlichungsdatum,
        script=script.get(),
    )

    context.add_rule(rule)

    return rule.identifier


def _parse_code_listen_referenz(parser: xml.XmlParser) -> CodeListenReferenz:
    identifier: xml.ParsedValue[CodeListIdentifier] = xml.ParsedValue()
    genericode_identifier: xml.ParsedValue[genericode.Identifier] = xml.ParsedValue()

    while (child := parser.next_child()) is not None:
        if child.tag == XDF2_IDENTIFIKATION:
            identifier.set(_parse_code_list_identifier(parser))
        elif child.tag == XDF2_GENERICODE_IDENTIFIKATION:
            genericode_identifier.set(_parse_genericode_identifier(parser))
        else:
            parser.skip_node()

    return CodeListenReferenz(
        identifier=identifier.expect(XDF2_IDENTIFIKATION),
        genericode_identifier=genericode_identifier.expect(
            XDF2_GENERICODE_IDENTIFIKATION
        ),
    )


def _parse_genericode_identifier(parser: xml.XmlParser) -> genericode.Identifier:
    canonical_id: xml.ParsedValue[str] = xml.ParsedValue()
    version: xml.ParsedValue[str] = xml.ParsedValue()
    canonical_version_uri: xml.ParsedValue[str] = xml.ParsedValue()

    # According to the standard, all of the attributes should be parsed as tokens.
    # However, this results in inconsistent genericode identifiers, as the canonical identification
    # can somtimes include trailing whitespace.

    while (child := parser.next_child()) is not None:
        if child.tag == XDF2_CANONICAL_IDENTIFICATION:
            canonical_id.set(parser.parse_value(XDF2_CANONICAL_IDENTIFICATION))
        elif child.tag == XDF2_VERSION:
            version.set(parser.parse_optional_value())
        elif child.tag == XDF2_CANONICAL_VERSION_URI:
            canonical_version_uri.set(parser.parse_value(XDF2_CANONICAL_VERSION_URI))
        else:
            parser.skip_node()

    try:
        return genericode.Identifier(
            canonical_uri=canonical_id.expect(XDF2_CANONICAL_IDENTIFICATION),
            version=version.get(),
            canonical_version_uri=canonical_version_uri.expect(
                XDF2_CANONICAL_VERSION_URI
            ),
        )
    except genericode.GenericodeException as error:
        raise InternalParserException(
            f"invalid genericode identification: {error}"
        ) from error


def _parse_code_list_identifier(parser: xml.XmlParser) -> CodeListIdentifier:
    id: xml.ParsedValue[str] = xml.ParsedValue()
    version: xml.ParsedValue[str] = xml.ParsedValue()

    while (child := parser.next_child()) is not None:
        if child.tag == XDF2_ID:
            id.set(parser.parse_value(XDF2_ID))
        elif child.tag == XDF2_VERSION:
            version.set(parser.parse_optional_value())
        else:
            parser.skip_node()

    return CodeListIdentifier(
        id=id.expect(XDF2_ID),
        version=version.get(),
    )


class SchemaElementAngabenContainer:
    def __init__(self):
        self.allgemein = AllgemeineAngabenContainer()
        self.hilfetext_eingabe: xml.ParsedValue[str] = xml.ParsedValue()
        self.hilfetext_ausgabe: xml.ParsedValue[str] = xml.ParsedValue()
        self.schema_element_art: xml.ParsedValue[SchemaElementArt] = xml.ParsedValue()

    def handle_child(self, parser: xml.XmlParser, child: xml.XmlElement) -> bool:  # type: ignore
        if child.tag == XDF2_HILFETEXT_EINGABE:
            self.hilfetext_eingabe.set(parser.parse_optional_value())
        elif child.tag == XDF2_HILFETEXT_AUSGABE:
            self.hilfetext_ausgabe.set(parser.parse_optional_value())
        elif child.tag == XDF2_SCHEMA_ELEMENT_ART:
            self.schema_element_art.set(parse_schema_element_art(parser.parse_code()))
        else:
            return self.allgemein.handle_child(parser, child)

        return True

    def finish(self) -> SchemaElementAngaben:
        return SchemaElementAngaben(
            identifier=self.allgemein.identifier.expect(XDF2_IDENTIFIKATION),
            name=self.allgemein.name.expect(XDF2_NAME),
            fachlicher_ersteller=self.allgemein.fachlicher_ersteller.get(),
            bezeichnung_eingabe=self.allgemein.bezeichnung_eingabe.get(),
            bezeichnung_ausgabe=self.allgemein.bezeichnung_ausgabe.get(),
            beschreibung=self.allgemein.beschreibung.get(),
            definition=self.allgemein.definition.get(),
            bezug=self.allgemein.bezug.get(),
            status=self.allgemein.status.expect(XDF2_STATUS),
            versionshinweis=self.allgemein.versionshinweis.get(),
            gueltig_ab=self.allgemein.gueltig_ab.get(),
            gueltig_bis=self.allgemein.gueltig_bis.get(),
            freigabedatum=self.allgemein.freigabedatum.get(),
            veroeffentlichungsdatum=self.allgemein.veroeffentlichungsdatum.get(),
            hilfetext_eingabe=self.hilfetext_eingabe.get(),
            hilfetext_ausgabe=self.hilfetext_ausgabe.get(),
            schema_element_art=self.schema_element_art.expect(XDF2_SCHEMA_ELEMENT_ART),
        )


def serialize_header(header: MessageHeader) -> ET.Element:
    element = ET.Element(XDF2_MESSAGE_HEADER)

    ET.SubElement(element, XDF2_MESSAGE_ID).text = header.nachricht_id
    ET.SubElement(
        element, XDF2_CREATION_TIME
    ).text = header.erstellungs_zeitpunkt.isoformat()

    return element


def attach_allgemeine_angaben(element: ET.Element, data: AllgemeineAngaben):
    element.append(_serialize_identifier(data.identifier))
    ET.SubElement(element, XDF2_NAME).text = data.name

    add_value_if_not_none(element, XDF2_BEZEICHNUNG_EINGABE, data.bezeichnung_eingabe)
    add_value_if_not_none(element, XDF2_BEZEICHNUNG_AUSGABE, data.bezeichnung_ausgabe)
    add_value_if_not_none(element, XDF2_BESCHREIBUNG, data.beschreibung)
    add_value_if_not_none(element, XDF2_DEFINITION, data.definition)
    add_value_if_not_none(element, XDF2_BEZUG, data.bezug)

    element.append(
        serialize_code(
            XDF2_STATUS,
            data.status.value,
            list_uri=STATUS_CODE_LIST_URI,
            list_version=STATUS_CODE_LIST_VERSION,
        )
    )

    add_value_if_not_none(element, XDF2_VERSIONSHINWEIS, data.versionshinweis)

    if data.gueltig_ab is not None:
        ET.SubElement(element, XDF2_GUELTIG_AB).text = data.gueltig_ab.isoformat()

    if data.gueltig_bis is not None:
        ET.SubElement(element, XDF2_GUELTIG_BIS).text = data.gueltig_bis.isoformat()

    add_value_if_not_none(element, XDF2_FACHLICHER_ERSTELLER, data.fachlicher_ersteller)

    if data.freigabedatum is not None:
        ET.SubElement(element, XDF2_FREIGABEDATUM).text = data.freigabedatum.isoformat()

    if data.veroeffentlichungsdatum is not None:
        ET.SubElement(
            element, XDF2_VEROEFFENTLICHUNGSDATUM
        ).text = data.veroeffentlichungsdatum.isoformat()


def attach_rules(
    parent: ET.Element,
    regel_identifiers: list[Identifier],
    rule_map: dict[Identifier, Regel],
):
    for identifier in regel_identifiers:
        rule = rule_map.get(identifier)
        if rule is None:
            raise XdfException(
                f"Cannot serialize xdf2 message: Regel {identifier.id}V{identifier.version} is unknown"
            )

        parent.append(serialize_rule(rule))


def serialize_rule(rule: Regel) -> ET.Element:
    rule_node = ET.Element(XDF2_REGEL)
    attach_allgemeine_angaben(rule_node, rule)
    add_value_if_not_none(rule_node, XDF2_SCRIPT, rule.script)

    return rule_node


def attach_struktur(
    element: ET.Element,
    struktur_list: list[ElementReference],
    group_map: dict[Identifier, Gruppe],
    field_map: dict[Identifier, Datenfeld],
    rule_map: dict[Identifier, Regel],
):
    for reference in struktur_list:
        struktur = ET.SubElement(element, XDF2_STRUKTUR)
        ET.SubElement(struktur, XDF2_ANZAHL).text = _anzahl_to_string(reference.anzahl)

        add_value_if_not_none(struktur, XDF2_BEZUG, reference.bezug)

        enthaelt = ET.SubElement(struktur, XDF2_ENTHAELT)

        match reference.element_type:
            case ElementType.GRUPPE:
                group = group_map.get(reference.identifier)
                if group is None:
                    raise XdfException(
                        f"Cannot serialize xdf2 message: Gruppe {reference.identifier.id}V{reference.identifier.version} is unknown"
                    )

                child = serialize_group(
                    group, group_map=group_map, field_map=field_map, rule_map=rule_map
                )
            case ElementType.FELD:
                field = field_map.get(reference.identifier)
                if field is None:
                    raise XdfException(
                        f"Cannot serialize xdf2 message: Datenfeld {reference.identifier.id}V{reference.identifier.version} is unknown"
                    )

                child = serialize_field(field, rule_map=rule_map)

        enthaelt.append(child)


def _anzahl_to_string(value: Anzahl) -> str:
    left = str(value.min)
    right = str(value.max) if value.max is not None else "*"

    return f"{left}:{right}"


def serialize_group(
    group: Gruppe,
    group_map: dict[Identifier, Gruppe],
    field_map: dict[Identifier, Datenfeld],
    rule_map: dict[Identifier, Regel],
) -> ET.Element:
    element = ET.Element(XDF2_DATENFELDGRUPPE)
    attach_allgemeine_angaben(element, group)
    _attach_element_angaben(element, group)
    attach_rules(element, group.regeln, rule_map)
    attach_struktur(element, group.struktur, group_map, field_map, rule_map)

    return element


def serialize_field(field: Datenfeld, rule_map: dict[Identifier, Regel]) -> ET.Element:
    element = ET.Element(XDF2_DATENFELD)

    attach_allgemeine_angaben(element, field)
    _attach_element_angaben(element, field)

    element.append(
        serialize_code(
            XDF2_FELDART,
            field.feldart.value,
            list_uri=FELDART_CODE_LIST_URI,
            list_version=FELDART_CODE_LIST_VERSION,
        )
    )

    element.append(
        serialize_code(
            XDF2_DATENTYP,
            field.datentyp.value,
            list_uri=DATENTYP_CODE_LIST_URI,
            list_version=DATENTYP_CODE_LIST_VERSION,
        )
    )

    add_value_if_not_none(element, XDF2_PRAEZISIERUNG, field.praezisierung)
    add_value_if_not_none(element, XDF2_INHALT, field.inhalt)

    if field.code_listen_referenz is not None:
        element.append(_serialize_code_list_reference(field.code_listen_referenz))

    attach_rules(element, field.regeln, rule_map)

    return element


def _serialize_code_list_reference(reference: CodeListenReferenz) -> ET.Element:
    element = ET.Element(XDF2_CODELISTE_REFERENZ)

    element.append(_serialize_identifier(reference.identifier, XDF2_IDENTIFIKATION))

    genericode_identifikation = ET.SubElement(element, XDF2_GENERICODE_IDENTIFIKATION)

    ET.SubElement(
        genericode_identifikation, XDF2_CANONICAL_IDENTIFICATION
    ).text = reference.genericode_identifier.canonical_uri

    add_value_if_not_none(
        genericode_identifikation,
        XDF2_VERSION,
        reference.genericode_identifier.version,
    )

    ET.SubElement(
        genericode_identifikation, XDF2_CANONICAL_VERSION_URI
    ).text = reference.genericode_identifier.canonical_version_uri

    return element


def _attach_element_angaben(element: ET.Element, data: SchemaElementAngaben):
    add_value_if_not_none(element, XDF2_HILFETEXT_EINGABE, data.hilfetext_eingabe)
    add_value_if_not_none(element, XDF2_HILFETEXT_AUSGABE, data.hilfetext_ausgabe)

    element.append(
        serialize_code(
            XDF2_SCHEMA_ELEMENT_ART,
            data.schema_element_art.value,
            list_uri=SCHEMA_ELEMENT_ART_CODE_LIST_URI,
            list_version=SCHEMA_ELEMENT_ART_CODE_LIST_VERSION,
        )
    )


def _serialize_identifier(
    identifier: Identifier | CodeListIdentifier, root_tag: str = XDF2_IDENTIFIKATION
) -> ET.Element:
    root = ET.Element(root_tag)

    ET.SubElement(root, XDF2_ID).text = identifier.id

    add_value_if_not_none(root, XDF2_VERSION, identifier.version)

    return root


def add_value_if_not_none(parent: ET.Element, tag: str, value: str | int | None):
    if value is not None:
        ET.SubElement(parent, tag).text = str(value)


def serialize_code(
    tag: str,
    value: str | int,
    list_uri: str,
    list_version: str | None = None,
) -> ET.Element:
    element = ET.Element(tag)
    element.attrib["listURI"] = list_uri
    if list_version is not None:
        element.attrib["listVersionID"] = list_version

    ET.SubElement(element, "code").text = str(value)

    return element
