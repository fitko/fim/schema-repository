XDF2_NS = "{urn:xoev-de:fim:standard:xdatenfelder_2}"

XDF2_MESSAGE_HEADER = f"{XDF2_NS}header"
XDF2_MESSAGE_ID = f"{XDF2_NS}nachrichtID"

XDF2_CREATION_TIME = f"{XDF2_NS}erstellungszeitpunkt"
XDF2_REFERENZ_ID = f"{XDF2_NS}referenzID"

XDF2_IDENTIFIKATION = f"{XDF2_NS}identifikation"
XDF2_ID = f"{XDF2_NS}id"
XDF2_VERSION = f"{XDF2_NS}version"
XDF2_NAME = f"{XDF2_NS}name"
XDF2_BEZEICHNUNG_EINGABE = f"{XDF2_NS}bezeichnungEingabe"
XDF2_BEZEICHNUNG_AUSGABE = f"{XDF2_NS}bezeichnungAusgabe"
XDF2_BESCHREIBUNG = f"{XDF2_NS}beschreibung"
XDF2_VERSIONSHINWEIS = f"{XDF2_NS}versionshinweis"
XDF2_FACHLICHER_ERSTELLER = f"{XDF2_NS}fachlicherErsteller"
XDF2_GUELTIG_AB = f"{XDF2_NS}gueltigAb"
XDF2_GUELTIG_BIS = f"{XDF2_NS}gueltigBis"
XDF2_STATUS = f"{XDF2_NS}status"
XDF2_FREIGABEDATUM = f"{XDF2_NS}freigabedatum"
XDF2_VEROEFFENTLICHUNGSDATUM = f"{XDF2_NS}veroeffentlichungsdatum"


XDF2_HILFETEXT = f"{XDF2_NS}hilfetext"

XDF2_SCHEMA_MESSAGE = f"{XDF2_NS}xdatenfelder.stammdatenschema.0102"
XDF2_DATENFELDGRUPPE_MESSAGE = f"{XDF2_NS}xdatenfelder.datenfeldgruppe.0103"
XDF2_DATENFELD_MESSAGE = f"{XDF2_NS}xdatenfelder.datenfeld.0104"
XDF2_ABLEITUNGSMODIFIKATIONEN_STRUKTUR = f"{XDF2_NS}ableitungsmodifikationenStruktur"
XDF2_ABLEITUNGSMODIFIKATIONEN_REPRAESENTATION = (
    f"{XDF2_NS}ableitungsmodifikationenRepraesentation"
)
XDF2_REGEL = f"{XDF2_NS}regel"
XDF2_SCRIPT = f"{XDF2_NS}script"
XDF2_CODELISTE_REFERENZ = f"{XDF2_NS}codelisteReferenz"
XDF2_GENERICODE_IDENTIFIKATION = f"{XDF2_NS}genericodeIdentification"
XDF2_CANONICAL_IDENTIFICATION = f"{XDF2_NS}canonicalIdentification"
XDF2_CANONICAL_VERSION_URI = f"{XDF2_NS}canonicalVersionUri"
XDF2_DATENTYP = f"{XDF2_NS}datentyp"
XDF2_PRAEZISIERUNG = f"{XDF2_NS}praezisierung"
XDF2_INHALT = f"{XDF2_NS}inhalt"
XDF2_ENTHAELT = f"{XDF2_NS}enthaelt"
XDF2_DATENFELDGRUPPE = f"{XDF2_NS}datenfeldgruppe"
XDF2_DATENFELD = f"{XDF2_NS}datenfeld"
XDF2_FELDART = f"{XDF2_NS}feldart"
XDF2_HILFETEXT_EINGABE = f"{XDF2_NS}hilfetextEingabe"
XDF2_HILFETEXT_AUSGABE = f"{XDF2_NS}hilfetextAusgabe"
XDF2_SCHEMA_ELEMENT_ART = f"{XDF2_NS}schemaelementart"
XDF2_STRUKTUR = f"{XDF2_NS}struktur"
XDF2_ANZAHL = f"{XDF2_NS}anzahl"
XDF2_BEZUG = f"{XDF2_NS}bezug"
XDF2_DEFINITION = f"{XDF2_NS}definition"
XDF2_SCHEMA = f"{XDF2_NS}stammdatenschema"

XDF2_STECKBRIEF_TITLE = f"{XDF2_NS}xdatenfelder.dokumentsteckbrief.0101"
XDF2_STECKBRIEF_MESSAGE = f"{XDF2_NS}dokumentensteckbrief"

XDF2_STECKBRIEF_IS_REFERENZ = f"{XDF2_NS}isReferenz"
XDF2_STECKBRIEF_DOKUMENTART = f"{XDF2_NS}dokumentart"

STATUS_CODE_LIST_URI = "urn:xoev-de:fim:codeliste:xdatenfelder.status"
STATUS_CODE_LIST_VERSION = "1.0"

DATENTYP_CODE_LIST_URI = "urn:xoev-de:fim:codeliste:xdatenfelder.datentyp"
DATENTYP_CODE_LIST_VERSION = "1.0"

FELDART_CODE_LIST_URI = "urn:xoev-de:fim:codeliste:xdatenfelder.feldart"
FELDART_CODE_LIST_VERSION = "1.0"
