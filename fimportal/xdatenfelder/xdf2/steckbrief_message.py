from dataclasses import dataclass
from pathlib import Path

from fimportal import genericode, xml
from fimportal.xdatenfelder.common import (
    InternalParserException,
    InvalidVersionException,
    ParserException,
)
from fimportal.xdatenfelder.xdf2.common import (
    AllgemeineAngaben,
    AllgemeineAngabenContainer,
    MessageHeader,
    parse_message_header,
)
from fimportal.xdatenfelder.xdf2.constants import (
    XDF2_HILFETEXT,
    XDF2_MESSAGE_HEADER,
    XDF2_STECKBRIEF_DOKUMENTART,
    XDF2_STECKBRIEF_IS_REFERENZ,
    XDF2_STECKBRIEF_MESSAGE,
    XDF2_STECKBRIEF_TITLE,
)


@dataclass(slots=True)
class Steckbrief(AllgemeineAngaben):
    """
    The actual steckbrief contained in the message.

    Most of the attributes are already inherited from `AllgemeineAngaben`.
    See: https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xdatenfelder_2.0:dokument:XDatenfelder_Spezifikation#%5B%7B%22num%22%3A276%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C70.866%2C654.495%2Cnull%5D
    """

    is_referenz: bool
    dokumentart: str
    hilfetext: (
        str | None
    )  # technically din91379.StringLatin, but this was not enforced in the xdf editor, so we ignore this contraint


@dataclass(slots=True)
class SteckbriefMessage:
    """
    similar to `SchemaMessage` from xdf2/parse.py.
    See: https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xdatenfelder_2.0:dokument:XDatenfelder_Spezifikation#%5B%7B%22num%22%3A388%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C70.866%2C415.429%2Cnull%5D
    """

    header: MessageHeader  # Identical to the header in `SchemaMessage`
    steckbrief: Steckbrief


def _parse_steckbrief(parser: xml.XmlParser) -> Steckbrief:
    hilfetext: xml.ParsedValue[str] = xml.ParsedValue()
    dokumentart: xml.ParsedValue[str] = xml.ParsedValue()
    is_referenz: xml.ParsedValue[str] = xml.ParsedValue()
    container = AllgemeineAngabenContainer()

    while (child := parser.next_child()) is not None:
        if container.handle_child(parser, child):
            continue
        if child.tag == XDF2_STECKBRIEF_DOKUMENTART:
            dokumentart.set(parser.parse_value(child.tag))
        elif child.tag == XDF2_HILFETEXT:
            hilfetext.set(parser.parse_string_latin(child.tag))
        elif child.tag == XDF2_STECKBRIEF_IS_REFERENZ:
            is_referenz.set(parser.parse_string_latin(child.tag))
        else:
            parser.skip_node()

    allgemeine_angaben = container.finish()

    return Steckbrief(
        identifier=allgemeine_angaben.identifier,
        name=allgemeine_angaben.name,
        fachlicher_ersteller=allgemeine_angaben.fachlicher_ersteller,
        bezeichnung_eingabe=allgemeine_angaben.bezeichnung_eingabe,
        bezeichnung_ausgabe=allgemeine_angaben.bezeichnung_ausgabe,
        beschreibung=allgemeine_angaben.beschreibung,
        definition=allgemeine_angaben.definition,
        bezug=allgemeine_angaben.bezug,
        status=allgemeine_angaben.status,
        versionshinweis=allgemeine_angaben.versionshinweis,
        gueltig_ab=allgemeine_angaben.gueltig_ab,
        gueltig_bis=allgemeine_angaben.gueltig_bis,
        freigabedatum=allgemeine_angaben.freigabedatum,
        veroeffentlichungsdatum=allgemeine_angaben.veroeffentlichungsdatum,
        hilfetext=hilfetext.get(),
        dokumentart=dokumentart.expect(XDF2_STECKBRIEF_DOKUMENTART),
        is_referenz=is_referenz.get() == "true",
    )


def load_steckbrief_message(path: str | Path) -> SteckbriefMessage:
    """
    Load and parse an xdf2 document profile from a file.
    """

    with open(path, "rb") as file:
        return parse_steckbrief_message(file.read())


def parse_steckbrief_message(input: bytes | str):
    if isinstance(input, str):
        input = input.encode("utf-8")

    parser = xml.XmlParser(input)

    try:
        parser.expect_child(XDF2_STECKBRIEF_TITLE)

        header = xml.ParsedValue[MessageHeader]()
        steckbrief = xml.ParsedValue[Steckbrief]()

        while (child := parser.next_child()) is not None:
            if child.tag == XDF2_MESSAGE_HEADER:
                header.set(parse_message_header(parser))
            elif child.tag == XDF2_STECKBRIEF_MESSAGE:
                steckbrief.set(_parse_steckbrief(parser))
            else:
                parser.skip_node()

        return SteckbriefMessage(
            header.expect(XDF2_MESSAGE_HEADER),
            steckbrief.expect(XDF2_STECKBRIEF_MESSAGE),
        )
    except (
        xml.ParserException,
        genericode.GenericodeException,
        InternalParserException,
        InvalidVersionException,
    ) as error:
        raise ParserException(
            f"Could not parse document profile: {str(error)}", parser.current_line
        ) from error
