import xml.etree.ElementTree as ET
from dataclasses import dataclass
from pathlib import Path

from fimportal import din91379, genericode, xml
from fimportal.xdatenfelder.common import (
    ABLEITUNGS_REPR_CODE_LIST_URI,
    ABLEITUNGS_REPR_CODE_LIST_VERSION,
    ABLEITUNGS_STRUKTUR_CODE_LIST_URI,
    AbleitungsmodifikationenRepraesentation,
    AbleitungsmodifikationenStruktur,
    InternalParserException,
    InvalidVersionException,
    ParserException,
    parse_ableitungsmodifikationen_repraesentation,
    parse_ableitungsmodifikationen_struktur,
)
from fimportal.xdatenfelder.xdf2.common import (
    AllgemeineAngaben,
    AllgemeineAngabenContainer,
    Datenfeld,
    ElementReference,
    Gruppe,
    Identifier,
    MessageHeader,
    ParseContext,
    Regel,
    add_value_if_not_none,
    attach_allgemeine_angaben,
    attach_rules,
    attach_struktur,
    parse_message_header,
    parse_rule,
    parse_structure,
    serialize_code,
    serialize_header,
)
from fimportal.xdatenfelder.xdf2.constants import (
    XDF2_ABLEITUNGSMODIFIKATIONEN_REPRAESENTATION,
    XDF2_ABLEITUNGSMODIFIKATIONEN_STRUKTUR,
    XDF2_HILFETEXT,
    XDF2_MESSAGE_HEADER,
    XDF2_REGEL,
    XDF2_SCHEMA,
    XDF2_SCHEMA_MESSAGE,
    XDF2_STRUKTUR,
)


@dataclass(slots=True)
class SchemaMessage:
    """
    The top-level data structure containing all the information
    of a parsed xdf2 schema file.
    """

    header: MessageHeader
    schema: "Schema"

    groups: dict[Identifier, Gruppe]
    fields: dict[Identifier, Datenfeld]
    rules: dict[Identifier, Regel]

    @property
    def id(self) -> str:
        return self.schema.identifier.id

    @property
    def version(self) -> str | None:
        return self.schema.identifier.version

    def assert_version(self) -> str:
        return self.schema.identifier.assert_version()

    def get_code_list_identifiers(self) -> set[genericode.Identifier]:
        """
        Get a set of all code list `GenericodeIdentifier`s within the schema.
        """

        identifiers: set[genericode.Identifier] = set()

        for data_field in self.fields.values():
            if data_field.code_listen_referenz is None:
                continue

            identifiers.add(data_field.code_listen_referenz.genericode_identifier)

        return identifiers

    def get_code_list_uris(self) -> set[str]:
        """
        Get a set of all code list `CanonicalVersionUri`s within the schema.
        """
        return {
            identifier.canonical_version_uri
            for identifier in self.get_code_list_identifiers()
        }


@dataclass(slots=True)
class Schema(AllgemeineAngaben):
    hilfetext: din91379.StringLatin | None
    ableitungsmodifikationen_struktur: AbleitungsmodifikationenStruktur
    ableitungsmodifikationen_repraesentation: AbleitungsmodifikationenRepraesentation
    regeln: list[Identifier]
    struktur: list[ElementReference]


def load_schema_message(path: str | Path) -> SchemaMessage:
    """
    Load and parse an xdf2 schema message from a file.
    """

    with open(path, "rb") as file:
        return parse_schema_message(file.read())


def parse_schema_message(input: bytes | str) -> SchemaMessage:
    """
    Parse an xdf2 schema message from the provided input.
    """

    context = ParseContext()

    if isinstance(input, str):
        input = input.encode("utf-8")

    # Felix: Not all existing Schemas (notably: BOB) are valid according to the XSD Schema.
    # Therefore, we ignore the XSD until it is clear, how to deal with this.
    # try:
    #     xml.validate_schema(input, SCHEMA)
    # except xml.ParserException as error:
    #     raise XdfException(f"Cannot parse schema: {error}") from error

    parser = xml.XmlParser(input)

    try:
        parser.expect_child(XDF2_SCHEMA_MESSAGE)

        header = xml.ParsedValue[MessageHeader]()
        schema = xml.ParsedValue[Schema]()

        while (child := parser.next_child()) is not None:
            if child.tag == XDF2_MESSAGE_HEADER:
                header.set(parse_message_header(parser))
            elif child.tag == XDF2_SCHEMA:
                schema.set(_parse_schema(parser, context))
            else:
                parser.skip_node()

        return SchemaMessage(
            header=header.expect(XDF2_MESSAGE_HEADER),
            schema=schema.expect(XDF2_SCHEMA),
            groups=context.groups,
            fields=context.fields,
            rules=context.rules,
        )
    except (
        xml.ParserException,
        genericode.GenericodeException,
        InternalParserException,
        InvalidVersionException,
    ) as error:
        raise ParserException(
            f"Could not parse schema: {str(error)}", parser.current_line
        ) from error


def _parse_schema(parser: xml.XmlParser, context: ParseContext) -> Schema:
    hilfetext: xml.ParsedValue[din91379.StringLatin] = xml.ParsedValue()
    ableitungsmodifikationen_struktur: xml.ParsedValue[
        AbleitungsmodifikationenStruktur
    ] = xml.ParsedValue()
    ableitungsmodifikationen_repraesentation: xml.ParsedValue[
        AbleitungsmodifikationenRepraesentation
    ] = xml.ParsedValue()
    container = AllgemeineAngabenContainer()
    struktur: list[ElementReference] = []
    regeln: list[Identifier] = []

    while (child := parser.next_child()) is not None:
        if container.handle_child(parser, child):
            continue

        if child.tag == XDF2_HILFETEXT:
            hilfetext.set(parser.parse_optional_string_latin())
        elif child.tag == XDF2_ABLEITUNGSMODIFIKATIONEN_STRUKTUR:
            ableitungsmodifikationen_struktur.set(
                parse_ableitungsmodifikationen_struktur(parser.parse_code())
            )
        elif child.tag == XDF2_ABLEITUNGSMODIFIKATIONEN_REPRAESENTATION:
            ableitungsmodifikationen_repraesentation.set(
                parse_ableitungsmodifikationen_repraesentation(parser.parse_code())
            )
        elif child.tag == XDF2_STRUKTUR:
            struktur.append(parse_structure(parser, context))
        elif child.tag == XDF2_REGEL:
            regeln.append(parse_rule(parser, context))
        else:
            parser.skip_node()

    allgemeine_angaben = container.finish()

    return Schema(
        identifier=allgemeine_angaben.identifier,
        name=allgemeine_angaben.name,
        fachlicher_ersteller=allgemeine_angaben.fachlicher_ersteller,
        bezeichnung_eingabe=allgemeine_angaben.bezeichnung_eingabe,
        bezeichnung_ausgabe=allgemeine_angaben.bezeichnung_ausgabe,
        beschreibung=allgemeine_angaben.beschreibung,
        definition=allgemeine_angaben.definition,
        bezug=allgemeine_angaben.bezug,
        status=allgemeine_angaben.status,
        versionshinweis=allgemeine_angaben.versionshinweis,
        gueltig_ab=allgemeine_angaben.gueltig_ab,
        gueltig_bis=allgemeine_angaben.gueltig_bis,
        freigabedatum=allgemeine_angaben.freigabedatum,
        veroeffentlichungsdatum=allgemeine_angaben.veroeffentlichungsdatum,
        hilfetext=hilfetext.get(),
        ableitungsmodifikationen_struktur=ableitungsmodifikationen_struktur.expect(
            XDF2_ABLEITUNGSMODIFIKATIONEN_STRUKTUR
        ),
        ableitungsmodifikationen_repraesentation=ableitungsmodifikationen_repraesentation.expect(
            XDF2_ABLEITUNGSMODIFIKATIONEN_REPRAESENTATION
        ),
        struktur=struktur,
        regeln=regeln,
    )


def serialize_schema_message(message: SchemaMessage) -> bytes:
    """
    Serialize the xdf2 schema message into an xml string.
    """

    root = ET.Element(XDF2_SCHEMA_MESSAGE)
    root.append(serialize_header(message.header))
    root.append(_serialize_schema(message))

    return ET.tostring(root, encoding="utf-8", xml_declaration=True)


def _serialize_schema(message: SchemaMessage) -> ET.Element:
    schema = message.schema

    element = ET.Element(XDF2_SCHEMA)
    attach_allgemeine_angaben(element, schema)
    add_value_if_not_none(element, XDF2_HILFETEXT, schema.hilfetext)

    element.append(
        serialize_code(
            XDF2_ABLEITUNGSMODIFIKATIONEN_STRUKTUR,
            schema.ableitungsmodifikationen_struktur.value,
            list_uri=ABLEITUNGS_STRUKTUR_CODE_LIST_URI,
            list_version=ABLEITUNGS_REPR_CODE_LIST_VERSION,
        )
    )

    element.append(
        serialize_code(
            XDF2_ABLEITUNGSMODIFIKATIONEN_REPRAESENTATION,
            schema.ableitungsmodifikationen_repraesentation.value,
            list_uri=ABLEITUNGS_REPR_CODE_LIST_URI,
            list_version=ABLEITUNGS_REPR_CODE_LIST_VERSION,
        )
    )

    attach_rules(element, schema.regeln, rule_map=message.rules)
    attach_struktur(
        element,
        schema.struktur,
        group_map=message.groups,
        field_map=message.fields,
        rule_map=message.rules,
    )

    return element
