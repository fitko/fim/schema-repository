"""
Map from xdf3 to xdf2 according to the official guide:

https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xdatenfelder_3.0.0:dokument:Transformationsregeln_XDatenfelder_3_zu_2
"""

from dataclasses import dataclass
import orjson


from fimportal import genericode
from . import xdf2, xdf3
from .common import ConversionException


class ConverterContext:
    """
    Saves all code lists created during the conversion, and generates unique code list identifiers.

    The created FIM ids will be in the schema-local nummernkreis `99`.
    """

    id_counter: int
    code_lists: dict[str, genericode.CodeList]

    def __init__(self):
        self.id_counter = 0
        self.code_lists = {}

    def create_reference(
        self, identifier: genericode.Identifier
    ) -> xdf2.CodeListenReferenz:
        self.id_counter += 1
        code_list_id = self.id_counter

        fim_id = f"C99{code_list_id:06d}"

        return xdf2.CodeListenReferenz(
            identifier=xdf2.CodeListIdentifier(fim_id, version=None),
            genericode_identifier=identifier,
        )

    def create_code_list(self, werte: list[xdf3.ListenWert]) -> xdf2.CodeListenReferenz:
        self.id_counter += 1
        code_list_id = self.id_counter

        fim_id = f"C99{code_list_id:06d}"
        canonical_uri = f"de:fitko:sammelrepository:xdf3-to-xdf2-converter:{fim_id}"

        code_list = genericode.CodeList(
            identifier=genericode.Identifier(
                canonical_uri=canonical_uri,
                version="1",
                canonical_version_uri=f"{canonical_uri}_1",
            ),
            default_code_key="code",
            default_name_key="name",
            columns={
                "code": [wert.code for wert in werte],
                "name": [wert.name for wert in werte],
            },
        )

        self.code_lists[fim_id] = code_list

        return xdf2.CodeListenReferenz(
            identifier=xdf2.CodeListIdentifier(fim_id, version=None),
            genericode_identifier=code_list.identifier,
        )


@dataclass(slots=True)
class ConversionResult:
    message: xdf2.SchemaMessage
    code_lists: dict[str, genericode.CodeList]


def convert_message(message: xdf3.SchemaMessage) -> ConversionResult:
    context = ConverterContext()

    xdf2_message = xdf2.SchemaMessage(
        header=xdf2.MessageHeader(
            nachricht_id=message.header.nachricht_id,
            erstellungs_zeitpunkt=message.header.erstellungs_zeitpunkt,
            referenz_id=None,
        ),
        schema=convert_schema(message.schema),
        groups={
            convert_identifier(identifier): convert_group(group)
            for identifier, group in message.groups.items()
        },
        fields={
            convert_identifier(identifier): convert_field(field, context)
            for identifier, field in message.fields.items()
        },
        rules={
            convert_identifier(identifier): convert_rule(rule)
            for identifier, rule in message.rules.items()
        },
    )

    return ConversionResult(
        message=xdf2_message,
        code_lists=context.code_lists,
    )


def convert_identifier(xdf3_identfier: xdf3.Identifier) -> xdf2.Identifier:
    xdf3_id = xdf3_identfier.id
    assert len(xdf3_id) >= 6

    mid = xdf3_id[3:6]
    if mid != "000":
        raise ConversionException(
            f"Cannot convert fim-id {xdf3_id}: The Unternummernkreis must be 000, but got {mid}"
        )

    start = xdf3_id[:3]
    end = xdf3_id[6:]
    xdf2_id = f"{start}{end}"

    if xdf3_identfier.version is not None:
        major, minor, patch = xdf3_identfier.version.split(".")

        if int(patch) == 0:
            xdf2_version = xdf2.parse_version(f"{major}.{minor}")
        else:
            if len(minor) > 3 or len(patch) > 3:
                raise ConversionException(
                    f"Cannot convert element {xdf3_id}V{xdf3_identfier.version}: version number too large"
                )

            minor = minor.rjust(3, "0")
            patch = patch.rjust(3, "0")
            xdf2_version = xdf2.parse_version(f"{major}.{minor}{patch}")
    else:
        xdf2_version = None

    return xdf2.Identifier(xdf2_id, xdf2_version)


def convert_rule(rule: xdf3.Regel) -> xdf2.Regel:
    return xdf2.Regel(
        identifier=convert_identifier(rule.identifier),
        name=rule.name,
        bezeichnung_eingabe=None,
        bezeichnung_ausgabe=None,
        beschreibung=rule.beschreibung,
        definition=rule.freitext_regel,
        bezug=_map_bezug(rule.bezug),
        # NOTE(Felix): According to the conversion guidelines, this should be the status of the
        # parent element.
        # However, there is no guarantee, that the rule is only used in one element,
        # so there could be potenttially multiple parents.
        # I therefore decided to just use `active`, as the information in the created xdf2 Document
        # is best-effort and anyways.
        status=xdf2.Status.AKTIV,
        versionshinweis=None,
        gueltig_ab=None,
        gueltig_bis=None,
        fachlicher_ersteller=rule.fachlicher_ersteller,
        freigabedatum=None,
        veroeffentlichungsdatum=None,
        script=rule.skript,
    )


def convert_field(field: xdf3.Datenfeld, context: ConverterContext) -> xdf2.Datenfeld:
    if field.praezisierung:
        praezisierung = orjson.dumps(
            {
                "minValue": field.praezisierung.min_value,
                "maxValue": field.praezisierung.max_value,
                "minLength": field.praezisierung.min_length,
                "maxLength": field.praezisierung.max_length,
                "pattern": field.praezisierung.pattern,
            }
        ).decode("utf-8")
    else:
        praezisierung = None

    if field.werte is not None:
        assert len(field.werte) > 0
        code_listen_referenz = context.create_code_list(field.werte)
    elif field.codeliste_referenz is not None:
        code_listen_referenz = context.create_reference(field.codeliste_referenz)
    else:
        code_listen_referenz = None

    return xdf2.Datenfeld(
        identifier=convert_identifier(field.identifier),
        name=field.name,
        bezeichnung_eingabe=field.bezeichnung_eingabe,
        bezeichnung_ausgabe=field.bezeichnung_ausgabe,
        beschreibung=field.beschreibung,
        definition=field.definition,
        bezug=_map_bezug(field.bezug),
        status=map_status(field.freigabe_status),
        versionshinweis=field.versionshinweis,
        gueltig_ab=field.gueltig_ab,
        gueltig_bis=field.gueltig_bis,
        fachlicher_ersteller=field.status_gesetzt_durch,
        freigabedatum=field.status_gesetzt_am,
        veroeffentlichungsdatum=None,
        schema_element_art=field.schema_element_art,
        hilfetext_eingabe=field.hilfetext_eingabe,
        hilfetext_ausgabe=field.hilfetext_ausgabe,
        feldart=_map_feldart(field.feldart),
        datentyp=_map_datentyp(field.datentyp),
        praezisierung=praezisierung,
        inhalt=field.inhalt,
        code_listen_referenz=code_listen_referenz,
        regeln=[convert_identifier(identifier) for identifier in field.regeln],
    )


def convert_group(group: xdf3.Gruppe) -> xdf2.Gruppe:
    return xdf2.Gruppe(
        identifier=convert_identifier(group.identifier),
        name=group.name,
        bezeichnung_eingabe=group.bezeichnung_eingabe,
        bezeichnung_ausgabe=group.bezeichnung_ausgabe,
        beschreibung=group.beschreibung,
        definition=group.definition,
        bezug=_map_bezug(group.bezug),
        status=map_status(group.freigabe_status),
        versionshinweis=group.versionshinweis,
        gueltig_ab=group.gueltig_ab,
        gueltig_bis=group.gueltig_bis,
        fachlicher_ersteller=group.status_gesetzt_durch,
        freigabedatum=group.status_gesetzt_am,
        veroeffentlichungsdatum=None,
        schema_element_art=group.schema_element_art,
        hilfetext_eingabe=group.hilfetext_eingabe,
        hilfetext_ausgabe=group.hilfetext_ausgabe,
        regeln=[convert_identifier(identifier) for identifier in group.regeln],
        struktur=_map_struktur(group.struktur),
    )


def convert_schema(schema: xdf3.Schema) -> xdf2.Schema:
    return xdf2.Schema(
        identifier=convert_identifier(schema.identifier),
        name=schema.name,
        bezeichnung_eingabe=schema.bezeichnung,
        bezeichnung_ausgabe=None,
        beschreibung=schema.beschreibung,
        definition=schema.definition,
        bezug=_map_bezug(schema.bezug),
        status=map_status(schema.freigabe_status),
        versionshinweis=schema.versionshinweis,
        gueltig_ab=schema.gueltig_ab,
        gueltig_bis=schema.gueltig_bis,
        fachlicher_ersteller=schema.status_gesetzt_durch,
        freigabedatum=schema.status_gesetzt_am,
        veroeffentlichungsdatum=None,
        hilfetext=schema.hilfetext,
        ableitungsmodifikationen_struktur=schema.ableitungsmodifikationen_struktur,
        ableitungsmodifikationen_repraesentation=schema.ableitungsmodifikationen_repraesentation,
        regeln=[convert_identifier(identifier) for identifier in schema.regeln],
        struktur=_map_struktur(schema.struktur),
    )


def _map_bezug(bezug: list[xdf3.Rechtsbezug]) -> str | None:
    if len(bezug) == 0:
        return None
    else:
        return ";".join([item.text for item in bezug])


def map_status(status: xdf3.FreigabeStatus) -> xdf2.Status:
    if status == xdf3.FreigabeStatus.INAKTIV:
        return xdf2.Status.INAKTIV
    else:
        return xdf2.Status.AKTIV


def _map_struktur(
    references: list[xdf3.ElementReference],
) -> list[xdf2.ElementReference]:
    return [
        xdf2.ElementReference(
            anzahl=ref.anzahl,
            bezug=_map_bezug(ref.bezug),
            element_type=ref.element_type,
            identifier=convert_identifier(ref.identifier),
        )
        for ref in references
    ]


def _map_feldart(feldart: xdf3.Feldart) -> xdf2.Feldart:
    match feldart:
        case xdf3.Feldart.EINGABE:
            return xdf2.Feldart.INPUT
        case xdf3.Feldart.AUSWAHL:
            return xdf2.Feldart.SELECT
        case xdf3.Feldart.STATISCH | xdf3.Feldart.GESPERRT | xdf3.Feldart.VERSTECKT:
            return xdf2.Feldart.LABEL


def _map_datentyp(datentyp: xdf3.Datentyp) -> xdf2.Datentyp:
    match datentyp:
        case (
            xdf3.Datentyp.TEXT
            | xdf3.Datentyp.STRING_LATIN
            | xdf3.Datentyp.ZEIT
            | xdf3.Datentyp.ZEITPUNKT
        ):
            return xdf2.Datentyp.TEXT
        case xdf3.Datentyp.DATUM:
            return xdf2.Datentyp.DATUM
        case xdf3.Datentyp.WAHRHEITSWERT:
            return xdf2.Datentyp.WAHRHEITSWERT
        case xdf3.Datentyp.NUMMER:
            return xdf2.Datentyp.NUMMER
        case xdf3.Datentyp.GANZZAHL:
            return xdf2.Datentyp.GANZZAHL
        case xdf3.Datentyp.GELDBETRAG:
            return xdf2.Datentyp.GELDBETRAG
        case xdf3.Datentyp.ANLAGE:
            return xdf2.Datentyp.ANLAGE
        case xdf3.Datentyp.OBJEKT:
            return xdf2.Datentyp.OBJEKT
