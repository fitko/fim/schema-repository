from __future__ import annotations

import logging
import re
import xml.etree.ElementTree as ET
from dataclasses import dataclass, field
from datetime import date, datetime
from enum import Enum
from typing import NewType

from fimportal import din91379, genericode, xml
from fimportal.common import (
    FREIGABE_STATUS_CODE_LIST_URI,
    FREIGABE_STATUS_CODE_LIST_VERSION,
    FreigabeStatus,
)
from fimportal.xdatenfelder.common import (
    SCHEMA_ELEMENT_ART_CODE_LIST_URI,
    SCHEMA_ELEMENT_ART_CODE_LIST_VERSION,
    Anzahl,
    ElementType,
    InternalParserException,
    InvalidPraezisierungException,
    InvalidVersionException,
    SchemaElementArt,
    XdfException,
    parse_datetime,
    parse_optional_date,
    parse_schema_element_art,
)
from fimportal.xdatenfelder.xdf3.constants import (
    DATENTYP_CODE_LIST_URI,
    DATENTYP_CODE_LIST_VERSION,
    FELDART_CODE_LIST_URI,
    FELDART_CODE_LIST_VERSION,
    GRUPPENART_CODE_LIST_URI,
    GRUPPENART_CODE_LIST_VERSION,
    REGELTYP_CODE_LIST_URI,
    REGELTYP_CODE_LIST_VERSION,
    RELATION_TYP_CODE_LIST_URI,
    RELATION_TYP_CODE_LIST_VERSION,
    VORBEFUELLUNG_CODE_LIST_URI,
    VORBEFUELLUNG_CODE_LIST_VERSION,
    XDF3_ANZAHL,
    XDF3_ART,
    XDF3_BESCHREIBUNG,
    XDF3_BEZEICHNUNG_AUSGABE,
    XDF3_BEZEICHNUNG_EINGABE,
    XDF3_BEZUG,
    XDF3_CANONICAL_IDENTIFICATION,
    XDF3_CANONICAL_VERSION_URI,
    XDF3_CODE,
    XDF3_CODE_KEY,
    XDF3_CODELISTE_REFERENZ,
    XDF3_CREATION_TIME,
    XDF3_DATENFELD,
    XDF3_DATENFELDGRUPPE,
    XDF3_DATENTYP,
    XDF3_DEFINITION,
    XDF3_ENTHAELT,
    XDF3_FACHLICHER_ERSTELLER,
    XDF3_FEHLER,
    XDF3_FELDART,
    XDF3_FREIGABE_STATUS,
    XDF3_FREITEXT_REGEL,
    XDF3_GUELTIG_AB,
    XDF3_GUELTIG_BIS,
    XDF3_HELP_KEY,
    XDF3_HILFE,
    XDF3_HILFETEXT_AUSGABE,
    XDF3_HILFETEXT_EINGABE,
    XDF3_ID,
    XDF3_IDENTIFIKATION,
    XDF3_INHALT,
    XDF3_LETZTE_AENDERUNG,
    XDF3_MAX_SIZE,
    XDF3_MEDIA_TYPE,
    XDF3_MESSAGE_HEADER,
    XDF3_MESSAGE_ID,
    XDF3_NAME,
    XDF3_NAME_KEY,
    XDF3_OBJEKT,
    XDF3_PARAM,
    XDF3_PRAEDIKAT,
    XDF3_PRAEZISIERUNG,
    XDF3_REGEL,
    XDF3_RELATION,
    XDF3_SCHEMA_ELEMENT_ART,
    XDF3_SKRIPT,
    XDF3_STATUS_GESETZT_AM,
    XDF3_STATUS_GESETZT_DURCH,
    XDF3_STICHWORT,
    XDF3_STRUKTUR,
    XDF3_TYP,
    XDF3_VEROEFFENTLICHUNGSDATUM,
    XDF3_VERSION,
    XDF3_VERSIONSHINWEIS,
    XDF3_VORBEFUELLUNG,
    XDF3_WERT,
    XDF3_WERTE,
    XDF3_ZIEL,
)


@dataclass(slots=True, frozen=True)
class MessageHeader:
    """
    The header of an xdf3 message.
    """

    nachricht_id: str
    erstellungs_zeitpunkt: datetime


def _parse_freigabe_status(value: str) -> FreigabeStatus:
    try:
        return FreigabeStatus(int(value))
    except ValueError as error:
        raise InternalParserException(f"Invalid FreigabeStatus '{value}'") from error


Nummernkreis = NewType("Nummernkreis", str)
VERSION_REGEX = re.compile(r"^\d+\.\d+\.\d+$")
Version = NewType("Version", str)


def parse_version(value: str | None) -> Version | None:
    if value is None:
        return None
    elif VERSION_REGEX.match(value) is not None:
        return Version(value)
    else:
        raise InvalidVersionException(f"Invalid version: {value}")


@dataclass(slots=True, unsafe_hash=True)
class Identifier:
    """
    Uniquely identifies a schema, data group, data field, or rule.
    """

    id: str
    version: Version | None
    nummernkreis: Nummernkreis = field(init=False)

    def assert_version(self) -> Version:
        assert self.version is not None
        return self.version

    def __post_init__(self):
        # TODO(Felix): Validate, that the id is formatted correctly
        if len(self.id) < 6:
            raise InternalParserException(f"invalid id {self.id}")

        part = self.id[1:6]
        try:
            int(part)
        except Exception as error:
            raise InternalParserException(f"invalid id {self.id}") from error

        self.nummernkreis = Nummernkreis(part)

    def is_local(self) -> bool:
        """
        Returns whether or not the element should be in the schema local namespace.
        """

        return self.nummernkreis.startswith("99")


def parse_identifier(parser: xml.XmlParser) -> Identifier:
    id = xml.ParsedValue[str]()
    version = xml.ParsedValue[Version]()

    while (child := parser.next_child()) is not None:
        if child.tag == XDF3_ID:
            id.set(parser.parse_token(XDF3_ID))
        elif child.tag == XDF3_VERSION:
            version.set(parse_version(parser.parse_optional_value()))
        else:
            parser.skip_node()

    return Identifier(
        id=id.expect(XDF3_ID),
        version=version.get(),
    )


class RelationTyp(Enum):
    IST_ABGELEITET_VON = "ABL"
    ERSETZT = "ERS"
    IST_AEQUIVALENT_ZU = "EQU"
    IST_VERKNUEPFT_MIT = "VKN"


@dataclass(slots=True)
class Relation:
    praedikat: RelationTyp
    objekt: Identifier


@dataclass(slots=True)
class Stichwort:
    uri: str
    value: str


@dataclass(slots=True)
class Rechtsbezug:
    text: din91379.StringLatin
    link: str | None


@dataclass(slots=True)
class AllgemeineAngaben:
    identifier: Identifier
    name: din91379.StringLatin
    beschreibung: din91379.StringLatin | None
    definition: din91379.StringLatin | None
    bezug: list[Rechtsbezug]
    freigabe_status: FreigabeStatus
    status_gesetzt_am: date | None
    status_gesetzt_durch: din91379.StringLatin | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    versionshinweis: din91379.StringLatin | None
    veroeffentlichungsdatum: date | None
    letzte_aenderung: datetime
    relation: list[Relation]
    stichwort: list[Stichwort]


class Dokumentart(Enum):
    """
    See: https://www.xrepository.de/details/urn:xoev-de:fim-datenfelder:codeliste:dokumentart
    """

    ANTRAG = "001"
    ANZEIGE = "002"
    BERICHT = "003"
    BESCHEID = "004"
    ERKLAERUNG = "005"
    KARTENMATERIAL = "006"
    MITTEILUNG = "007"
    MULTIMEDIA = "008"
    REGISTERANFRAGE = "009"
    REGISTERANTWORT = "010"
    URKUNDE = "011"
    VERTRAG = "012"
    VOLLMACHT = "013"
    WILLENSERKLAERUNG = "014"
    UNBESTIMMT = "999"

    @staticmethod
    def from_label(value: str) -> "Dokumentart":
        try:
            return STRING_TO_DOKUMENTART[value]
        except KeyError:
            return Dokumentart.UNBESTIMMT

    def to_label(self) -> str:
        return DOKUMENTART_TO_STRING[self]


def parse_dokumentart(value: str) -> Dokumentart:
    try:
        return Dokumentart(value)
    except ValueError as error:
        raise InternalParserException(f"Invalid Dokumentart '{value}'") from error


STRING_TO_DOKUMENTART: dict[str, Dokumentart] = {
    "Antrag": Dokumentart.ANTRAG,
    "Anzeige": Dokumentart.ANZEIGE,
    "Bericht": Dokumentart.BERICHT,
    "Bescheid": Dokumentart.BESCHEID,
    "Erklärung": Dokumentart.ERKLAERUNG,
    "Kartenmaterial": Dokumentart.KARTENMATERIAL,
    "Mitteilung": Dokumentart.MITTEILUNG,
    "Multimedia": Dokumentart.MULTIMEDIA,
    "Registeranfrage": Dokumentart.REGISTERANFRAGE,
    "Registerantwort": Dokumentart.REGISTERANTWORT,
    "Urkunde": Dokumentart.URKUNDE,
    "Vertrag": Dokumentart.VERTRAG,
    "Vollmacht": Dokumentart.VOLLMACHT,
    "Willenserklärung": Dokumentart.WILLENSERKLAERUNG,
    "unbestimmt": Dokumentart.UNBESTIMMT,
}

DOKUMENTART_TO_STRING: dict[Dokumentart, str] = {
    status: label for label, status in STRING_TO_DOKUMENTART.items()
}


class AllgemeineAngabenContainer:
    def __init__(self):
        self.identifier = xml.ParsedValue[Identifier]()
        self.name = xml.ParsedValue[din91379.StringLatin]()
        self.beschreibung = xml.ParsedValue[din91379.StringLatin]()
        self.definition = xml.ParsedValue[din91379.StringLatin]()
        self.bezug: list[Rechtsbezug] = []
        self.freigabe_status = xml.ParsedValue[FreigabeStatus]()
        self.status_gesetzt_am = xml.ParsedValue[date]()
        self.status_gesetzt_durch = xml.ParsedValue[din91379.StringLatin]()
        self.gueltig_ab = xml.ParsedValue[date]()
        self.gueltig_bis = xml.ParsedValue[date]()
        self.versionshinweis = xml.ParsedValue[din91379.StringLatin]()
        self.veroeffentlichungsdatum = xml.ParsedValue[date]()
        self.letzte_aenderung = xml.ParsedValue[datetime]()
        self.relation: list[Relation] = []
        self.stichwort: list[Stichwort] = []

    def handle_child(self, parser: xml.XmlParser, child: xml.XmlElement) -> bool:  # type: ignore
        if child.tag == XDF3_IDENTIFIKATION:
            self.identifier.set(parse_identifier(parser))
        elif child.tag == XDF3_NAME:
            self.name.set(parser.parse_string_latin(XDF3_NAME))
        elif child.tag == XDF3_BESCHREIBUNG:
            self.beschreibung.set(parser.parse_optional_string_latin())
        elif child.tag == XDF3_DEFINITION:
            self.definition.set(parser.parse_optional_string_latin())
        elif child.tag == XDF3_BEZUG:
            self.bezug.append(parse_bezug(parser, link=child.get("link", None)))
        elif child.tag == XDF3_FREIGABE_STATUS:
            self.freigabe_status.set(_parse_freigabe_status(parser.parse_code()))
        elif child.tag == XDF3_STATUS_GESETZT_AM:
            self.status_gesetzt_am.set(
                parse_optional_date(parser.parse_optional_value())
            )
        elif child.tag == XDF3_STATUS_GESETZT_DURCH:
            self.status_gesetzt_durch.set(parser.parse_optional_string_latin())
        elif child.tag == XDF3_GUELTIG_AB:
            self.gueltig_ab.set(parse_optional_date(parser.parse_optional_value()))
        elif child.tag == XDF3_GUELTIG_BIS:
            self.gueltig_bis.set(parse_optional_date(parser.parse_optional_value()))
        elif child.tag == XDF3_VERSIONSHINWEIS:
            self.versionshinweis.set(parser.parse_optional_string_latin())
        elif child.tag == XDF3_VEROEFFENTLICHUNGSDATUM:
            self.veroeffentlichungsdatum.set(
                parse_optional_date(parser.parse_optional_value())
            )
        elif child.tag == XDF3_LETZTE_AENDERUNG:
            self.letzte_aenderung.set(
                parse_datetime(parser.parse_value(XDF3_LETZTE_AENDERUNG))
            )
        elif child.tag == XDF3_RELATION:
            self.relation.append(_parse_relation(parser))
        elif child.tag == XDF3_STICHWORT:
            self.stichwort.append(parse_stichwort(parser, uri=child.get("uri", None)))
        else:
            return False

        return True

    def finish(self) -> AllgemeineAngaben:
        return AllgemeineAngaben(
            identifier=self.identifier.expect(XDF3_IDENTIFIKATION),
            name=self.name.expect(XDF3_NAME),
            beschreibung=self.beschreibung.get(),
            definition=self.definition.get(),
            bezug=self.bezug,
            freigabe_status=self.freigabe_status.expect(XDF3_FREIGABE_STATUS),
            status_gesetzt_am=self.status_gesetzt_am.get(),
            status_gesetzt_durch=self.status_gesetzt_durch.get(),
            gueltig_ab=self.gueltig_ab.get(),
            gueltig_bis=self.gueltig_bis.get(),
            versionshinweis=self.versionshinweis.get(),
            veroeffentlichungsdatum=self.veroeffentlichungsdatum.get(),
            letzte_aenderung=self.letzte_aenderung.expect(XDF3_LETZTE_AENDERUNG),
            relation=self.relation,
            stichwort=self.stichwort,
        )


def ensure_not_none(node_name: str, attribute_name: str, value: str | None) -> str:
    if value is None:
        raise InternalParserException(
            f"{node_name } attribute '{attribute_name}' must be set"
        )

    return value


def parse_stichwort(parser: xml.XmlParser, uri: str | None) -> Stichwort:
    uri = ensure_not_none("Stichwort", "uri", uri)
    value = parser.parse_token(XDF3_STICHWORT)

    return Stichwort(uri, value)


def _parse_relation_typ(value: str) -> RelationTyp:
    try:
        return RelationTyp(value)
    except ValueError as error:
        raise InternalParserException(f"Invalid Relation '{value}'") from error


def _parse_relation(parser: xml.XmlParser) -> Relation:
    praedikat = xml.ParsedValue[RelationTyp]()
    objekt = xml.ParsedValue[Identifier]()

    while (child := parser.next_child()) is not None:
        if child.tag == XDF3_PRAEDIKAT:
            praedikat.set(_parse_relation_typ(parser.parse_code()))
        elif child.tag == XDF3_OBJEKT:
            objekt.set(parse_identifier(parser))
        else:
            parser.skip_node()

    return Relation(
        praedikat=praedikat.expect(XDF3_PRAEDIKAT),
        objekt=objekt.expect(XDF3_OBJEKT),
    )


def parse_bezug(parser: xml.XmlParser, link: str | None) -> Rechtsbezug:
    text = parser.parse_string_latin(XDF3_BEZUG)

    return Rechtsbezug(text, link)


def parse_message_header(parser: xml.XmlParser) -> MessageHeader:
    nachrichtId = xml.ParsedValue[str]()
    erstellungsZeitpunt = xml.ParsedValue[datetime]()

    while (child := parser.next_child()) is not None:
        if child.tag == XDF3_MESSAGE_ID:
            nachrichtId.set(parser.parse_token(XDF3_MESSAGE_ID))
        elif child.tag == XDF3_CREATION_TIME:
            erstellungsZeitpunt.set(
                parse_datetime(parser.parse_value(XDF3_CREATION_TIME))
            )
        else:
            parser.skip_node()

    return MessageHeader(
        nachricht_id=nachrichtId.expect(XDF3_MESSAGE_ID),
        erstellungs_zeitpunkt=erstellungsZeitpunt.expect(XDF3_CREATION_TIME),
    )


class Regeltyp(Enum):
    KOMPLEX = "K"
    MULTIPLIZITAET = "M"
    VALIDIERUNG = "V"
    BERECHNUNG = "B"


def _parse_regeltyp(value: str) -> Regeltyp:
    try:
        return Regeltyp(value)
    except ValueError as error:
        raise InternalParserException(f"Invalid Regeltyp '{value}'") from error


class Feldart(Enum):
    EINGABE = "input"
    AUSWAHL = "select"
    STATISCH = "label"
    VERSTECKT = "hidden"
    GESPERRT = "locked"


def _parse_feldart(value: str) -> Feldart:
    try:
        return Feldart(value)
    except ValueError as error:
        raise InternalParserException(f"Invalid Feldart '{value}'") from error


class Datentyp(Enum):
    TEXT = "text"
    STRING_LATIN = "text_latin"  # DIN 91379 Type C
    DATUM = "date"
    ZEIT = "time"
    ZEITPUNKT = "datetime"
    WAHRHEITSWERT = "bool"
    NUMMER = "num"
    GANZZAHL = "num_int"
    GELDBETRAG = "num_currency"
    ANLAGE = "file"
    OBJEKT = "obj"


def _parse_datentyp(value: str) -> Datentyp:
    try:
        return Datentyp(value)
    except ValueError as error:
        raise InternalParserException(f"Invalid Datentyp '{value}'") from error


class Gruppenart(Enum):
    AUSWAHL = "X"


def _parse_grupppenart(value: str) -> Gruppenart:
    try:
        return Gruppenart(value)
    except ValueError as error:
        raise InternalParserException(f"Invalid Gruppenart '{value}'") from error


class RegelParamTyp(Enum):
    WERT = "wert"
    ANZAHL = "anzahl"
    SELEKTOR = "selektor"
    OBJEKT = "objekt"


def _parse_regel_param_typ(value: str) -> RegelParamTyp:
    try:
        return RegelParamTyp(value)
    except ValueError as error:
        raise InternalParserException(f"Invalid RegelParamTyp '{value}'") from error


class Vorbefuellung(Enum):
    KEINE = "keine"
    OPTIONAL = "optional"
    VERPFLICHTEND = "verpflichtend"


def _parse_vorbefuellung(value: str) -> Vorbefuellung:
    try:
        return Vorbefuellung(value)
    except ValueError as error:
        raise InternalParserException(f"Invalid Vorbefuellung '{value}'") from error


@dataclass(slots=True)
class Praezisierung:
    min_length: int | None = None
    max_length: int | None = None
    min_value: str | None = None
    max_value: str | None = None
    pattern: str | None = None

    def __post_init__(self):
        if self.min_length is not None:
            if self.min_length < 0:
                raise InvalidPraezisierungException(
                    f"Invalid min_length: {self.min_length}"
                )

        if self.max_length is not None:
            if self.max_length <= 0:
                raise InvalidPraezisierungException(
                    f"Invalid max_length: {self.max_length}"
                )


@dataclass(slots=True)
class ElementReference:
    anzahl: Anzahl
    bezug: list[Rechtsbezug]
    element_type: ElementType
    identifier: Identifier  # Identifier of the reference child


@dataclass(slots=True)
class Element(AllgemeineAngaben):
    bezeichnung_eingabe: din91379.StringLatin
    bezeichnung_ausgabe: din91379.StringLatin | None
    schema_element_art: SchemaElementArt
    hilfetext_eingabe: din91379.StringLatin | None
    hilfetext_ausgabe: din91379.StringLatin | None


@dataclass(slots=True)
class Gruppe(Element):
    art: Gruppenart | None
    regeln: list[Identifier]
    struktur: list[ElementReference]


@dataclass(slots=True)
class Datenfeld(Element):
    feldart: Feldart
    datentyp: Datentyp
    praezisierung: Praezisierung | None
    inhalt: din91379.StringLatin | None
    vorbefuellung: Vorbefuellung
    werte: list[ListenWert] | None
    codeliste_referenz: genericode.Identifier | None
    code_key: str | None
    name_key: str | None
    help_key: str | None
    regeln: list[Identifier]
    max_size: int | None
    media_type: list[str]


@dataclass(slots=True)
class ListenWert:
    code: str
    name: str
    hilfe: str | None


@dataclass(slots=True)
class Regel:
    identifier: Identifier
    name: str
    beschreibung: str | None
    freitext_regel: str | None
    bezug: list[Rechtsbezug]
    stichwort: list[Stichwort]
    fachlicher_ersteller: str | None
    letzte_aenderung: datetime
    typ: Regeltyp
    param: list[RegelParameter]
    ziel: list[RegelZiel]
    skript: str | None
    fehler: list[Fehlermeldung]


@dataclass(slots=True)
class RegelParameter:
    element: str
    name: str
    typ: RegelParamTyp


@dataclass(slots=True)
class RegelZiel:
    element: str
    name: str


@dataclass(slots=True)
class Fehlermeldung:
    code: int
    message: str


@dataclass(slots=True)
class ParseContext:
    groups: dict[Identifier, Gruppe] = field(default_factory=dict)
    fields: dict[Identifier, Datenfeld] = field(default_factory=dict)
    rules: dict[Identifier, Regel] = field(default_factory=dict)

    def add_group(self, group: Gruppe):
        saved_group = self.groups.get(group.identifier)
        if saved_group is not None:
            if saved_group != group:
                raise InternalParserException(
                    f"inconsistent Datenfeldgruppe [id={group.identifier.id}, version={group.identifier.version}]"
                )
        else:
            self.groups[group.identifier] = group

    def add_field(self, field: Datenfeld):
        saved_field = self.fields.get(field.identifier)
        if saved_field is not None:
            if saved_field != field:
                raise InternalParserException(
                    f"inconsistent Datenfeld [id={field.identifier.id}, version={field.identifier.version}]"
                )
        else:
            self.fields[field.identifier] = field

    def add_rule(self, rule: Regel):
        saved_rule = self.rules.get(rule.identifier)
        if saved_rule is not None:
            if saved_rule != rule:
                raise InternalParserException(
                    f"inconsistent Regel [id={rule.identifier.id}, version={rule.identifier.version}]"
                )
        else:
            self.rules[rule.identifier] = rule


def parse_rule(parser: xml.XmlParser, context: ParseContext) -> Identifier:
    identifier = xml.ParsedValue[Identifier]()
    name = xml.ParsedValue[str]()
    beschreibung = xml.ParsedValue[str]()
    freitext_regel = xml.ParsedValue[str]()
    bezug: list[Rechtsbezug] = []
    stichwort: list[Stichwort] = []
    fachlicher_ersteller = xml.ParsedValue[str]()
    letzte_aenderung = xml.ParsedValue[datetime]()
    typ = xml.ParsedValue[Regeltyp]()
    param: list[RegelParameter] = []
    ziel: list[RegelZiel] = []
    skript = xml.ParsedValue[str]()
    fehler: list[Fehlermeldung] = []

    while (child := parser.next_child()) is not None:
        if child.tag == XDF3_IDENTIFIKATION:
            identifier.set(parse_identifier(parser))
        elif child.tag == XDF3_NAME:
            name.set(parser.parse_value(XDF3_NAME))
        elif child.tag == XDF3_BESCHREIBUNG:
            beschreibung.set(parser.parse_optional_value())
        elif child.tag == XDF3_FREITEXT_REGEL:
            freitext_regel.set(parser.parse_optional_value())
        elif child.tag == XDF3_BEZUG:
            bezug.append(parse_bezug(parser, child.get("link", None)))
        elif child.tag == XDF3_STICHWORT:
            stichwort.append(parse_stichwort(parser, child.get("uri", None)))
        elif child.tag == XDF3_FACHLICHER_ERSTELLER:
            fachlicher_ersteller.set(parser.parse_optional_value())
        elif child.tag == XDF3_LETZTE_AENDERUNG:
            letzte_aenderung.set(
                parse_datetime(parser.parse_value(XDF3_LETZTE_AENDERUNG))
            )
        elif child.tag == XDF3_TYP:
            typ.set(_parse_regeltyp(parser.parse_code()))
        elif child.tag == XDF3_PARAM:
            param.append(
                _parse_regel_parameter(
                    parser,
                    element=child.get("element", None),
                    name=child.get("name", None),
                )
            )
        elif child.tag == XDF3_ZIEL:
            element = child.get("element", None)
            element = ensure_not_none("RegelZiel", "element", element)

            target_name = child.get("name", None)
            target_name = ensure_not_none("RegelZiel", "name", target_name)

            # The node has only attributes and no content
            parser.skip_node()

            ziel.append(
                RegelZiel(
                    element=element,
                    name=target_name,
                )
            )
        elif child.tag == XDF3_SKRIPT:
            skript.set(parser.parse_optional_value())
        elif child.tag == XDF3_FEHLER:
            code = child.get("code", None)
            code = ensure_not_none("Fehler", "code", code)

            try:
                code = int(code)
            except ValueError as error:
                raise InternalParserException(f"Invalid Fehlercode `{code}`") from error

            message = parser.parse_value(XDF3_FEHLER)
            fehler.append(Fehlermeldung(code=code, message=message))
        else:
            parser.skip_node()

    rule = Regel(
        identifier=identifier.expect(XDF3_IDENTIFIKATION),
        name=name.expect(XDF3_NAME),
        beschreibung=beschreibung.get(),
        freitext_regel=freitext_regel.get(),
        bezug=bezug,
        stichwort=stichwort,
        fachlicher_ersteller=fachlicher_ersteller.get(),
        letzte_aenderung=letzte_aenderung.expect(XDF3_LETZTE_AENDERUNG),
        typ=typ.expect(XDF3_TYP),
        param=param,
        ziel=ziel,
        skript=skript.get(),
        fehler=fehler,
    )

    context.add_rule(rule)

    return rule.identifier


def _parse_regel_parameter(
    parser: xml.XmlParser, element: str | None, name: str | None
) -> RegelParameter:
    element = ensure_not_none("RegelParameter", "element", element)
    name = ensure_not_none("RegelParameter", "name", name)

    typ = xml.ParsedValue[RegelParamTyp]()

    while (child := parser.next_child()) is not None:
        if child.tag == XDF3_TYP:
            typ.set(_parse_regel_param_typ(parser.parse_token(XDF3_TYP)))
        else:
            parser.skip_node()

    return RegelParameter(element=element, name=name, typ=typ.expect(XDF3_TYP))


def parse_structure(parser: xml.XmlParser, context: ParseContext) -> ElementReference:
    anzahl = xml.ParsedValue[Anzahl]()
    bezug: list[Rechtsbezug] = []
    reference = xml.ParsedValue[tuple[ElementType, Identifier]]()

    while (child := parser.next_child()) is not None:
        if child.tag == XDF3_ENTHAELT:
            reference.set(_parse_contains(parser, context))
        elif child.tag == XDF3_ANZAHL:
            anzahl.set(Anzahl.from_string(parser.parse_value(XDF3_ANZAHL)))
        elif child.tag == XDF3_BEZUG:
            bezug.append(parse_bezug(parser, link=child.get("link", None)))
        else:
            parser.skip_node()

    element_type, identifier = reference.expect(XDF3_ENTHAELT)
    return ElementReference(
        anzahl=anzahl.expect(XDF3_ANZAHL),
        bezug=bezug,
        element_type=element_type,
        identifier=identifier,
    )


def _parse_contains(
    parser: xml.XmlParser, context: ParseContext
) -> tuple[ElementType, Identifier]:
    reference = xml.ParsedValue[tuple[ElementType, Identifier]]()

    while (child := parser.next_child()) is not None:
        if child.tag == XDF3_DATENFELDGRUPPE:
            group = parse_datenfeldgruppe(parser, context)
            context.add_group(group)
            reference.set((ElementType.GRUPPE, group.identifier))
        elif child.tag == XDF3_DATENFELD:
            field = parse_datenfeld(parser, context)
            context.add_field(field)
            reference.set((ElementType.FELD, field.identifier))
        else:
            parser.skip_node()

    return reference.expect(f"{XDF3_DATENFELDGRUPPE} or {XDF3_DATENFELD}")


def parse_datenfeldgruppe(parser: xml.XmlParser, context: ParseContext) -> Gruppe:
    container = ElementContainer()
    art = xml.ParsedValue[Gruppenart]()
    regeln: list[Identifier] = []
    struktur: list[ElementReference] = []

    while (child := parser.next_child()) is not None:
        if container.handle_child(parser, child):
            continue

        if child.tag == XDF3_ART:
            art.set(_parse_grupppenart(parser.parse_code()))
        elif child.tag == XDF3_REGEL:
            regeln.append(parse_rule(parser, context))
        elif child.tag == XDF3_STRUKTUR:
            struktur.append(parse_structure(parser, context))
        else:
            parser.skip_node()

    element_angaben = container.finish()

    # This is already checked by the XSD file, this is just a sanity check
    assert len(struktur) > 0

    return Gruppe(
        identifier=element_angaben.identifier,
        name=element_angaben.name,
        beschreibung=element_angaben.beschreibung,
        definition=element_angaben.definition,
        bezug=element_angaben.bezug,
        freigabe_status=element_angaben.freigabe_status,
        status_gesetzt_am=element_angaben.status_gesetzt_am,
        status_gesetzt_durch=element_angaben.status_gesetzt_durch,
        gueltig_ab=element_angaben.gueltig_ab,
        gueltig_bis=element_angaben.gueltig_bis,
        versionshinweis=element_angaben.versionshinweis,
        veroeffentlichungsdatum=element_angaben.veroeffentlichungsdatum,
        letzte_aenderung=element_angaben.letzte_aenderung,
        relation=element_angaben.relation,
        stichwort=element_angaben.stichwort,
        bezeichnung_eingabe=element_angaben.bezeichnung_eingabe,
        bezeichnung_ausgabe=element_angaben.bezeichnung_ausgabe,
        schema_element_art=element_angaben.schema_element_art,
        hilfetext_eingabe=element_angaben.hilfetext_eingabe,
        hilfetext_ausgabe=element_angaben.hilfetext_ausgabe,
        art=art.get(),
        regeln=regeln,
        struktur=struktur,
    )


def parse_datenfeld(parser: xml.XmlParser, context: ParseContext) -> Datenfeld:
    container = ElementContainer()
    feldart = xml.ParsedValue[Feldart]()
    datentyp = xml.ParsedValue[Datentyp]()
    praezisierung = xml.ParsedValue[Praezisierung]()
    inhalt = xml.ParsedValue[din91379.StringLatin]()
    vorbefuellung = xml.ParsedValue[Vorbefuellung]()
    werte = xml.ParsedValue[list[ListenWert]]()
    codeliste_referenz = xml.ParsedValue[genericode.Identifier]()
    code_key = xml.ParsedValue[str]()
    name_key = xml.ParsedValue[str]()
    help_key = xml.ParsedValue[str]()
    regeln: list[Identifier] = []
    max_size = xml.ParsedValue[int]()
    media_type: list[str] = []

    while (child := parser.next_child()) is not None:
        if container.handle_child(parser, child):
            continue

        if child.tag == XDF3_FELDART:
            feldart.set(_parse_feldart(parser.parse_code()))
        elif child.tag == XDF3_DATENTYP:
            datentyp.set(_parse_datentyp(parser.parse_code()))
        elif child.tag == XDF3_PRAEZISIERUNG:
            praezisierung.set(_parse_praezisierung(parser, child))
        elif child.tag == XDF3_INHALT:
            inhalt.set(parser.parse_optional_string_latin())
        elif child.tag == XDF3_VORBEFUELLUNG:
            vorbefuellung.set(_parse_vorbefuellung(parser.parse_code()))
        elif child.tag == XDF3_WERTE:
            werte.set(_parse_werte(parser))
        elif child.tag == XDF3_CODELISTE_REFERENZ:
            codeliste_referenz.set(_parse_genericode_identifier(parser))
        elif child.tag == XDF3_CODE_KEY:
            code_key.set(parser.parse_optional_token())
        elif child.tag == XDF3_NAME_KEY:
            name_key.set(parser.parse_optional_token())
        elif child.tag == XDF3_HELP_KEY:
            help_key.set(parser.parse_optional_token())
        elif child.tag == XDF3_REGEL:
            regeln.append(parse_rule(parser, context))
        elif child.tag == XDF3_MAX_SIZE:
            value = parser.parse_optional_value()
            if value is not None:
                # This should always succeed, as the XSD validation is already testing max_size to be
                # a positive integer
                value = int(value)
                assert value > 0

            max_size.set(value)
        elif child.tag == XDF3_MEDIA_TYPE:
            # XSD already validates that this is a valid pattern string
            media_type.append(parser.parse_token(XDF3_MEDIA_TYPE))
        else:
            parser.skip_node()

    element_angaben = container.finish()

    codeliste_referenz = codeliste_referenz.get()
    werte = werte.get()
    if codeliste_referenz is not None and werte is not None:
        raise InternalParserException(
            "Datenfeld cannot include both a code list reference and an embedded code list"
        )

    datentyp = datentyp.expect(XDF3_DATENTYP)
    max_size = max_size.get()

    if max_size is not None or len(media_type) != 0:
        if datentyp != Datentyp.ANLAGE:
            # This combination is explicitly forbidden by the standard.
            raise InternalParserException(
                "maxSize or mediaType must only be set for file inputs"
            )

    return Datenfeld(
        identifier=element_angaben.identifier,
        name=element_angaben.name,
        beschreibung=element_angaben.beschreibung,
        definition=element_angaben.definition,
        bezug=element_angaben.bezug,
        freigabe_status=element_angaben.freigabe_status,
        status_gesetzt_am=element_angaben.status_gesetzt_am,
        status_gesetzt_durch=element_angaben.status_gesetzt_durch,
        gueltig_ab=element_angaben.gueltig_ab,
        gueltig_bis=element_angaben.gueltig_bis,
        versionshinweis=element_angaben.versionshinweis,
        veroeffentlichungsdatum=element_angaben.veroeffentlichungsdatum,
        letzte_aenderung=element_angaben.letzte_aenderung,
        relation=element_angaben.relation,
        stichwort=element_angaben.stichwort,
        bezeichnung_eingabe=element_angaben.bezeichnung_eingabe,
        bezeichnung_ausgabe=element_angaben.bezeichnung_ausgabe,
        schema_element_art=element_angaben.schema_element_art,
        hilfetext_eingabe=element_angaben.hilfetext_eingabe,
        hilfetext_ausgabe=element_angaben.hilfetext_ausgabe,
        feldart=feldart.expect(XDF3_FELDART),
        datentyp=datentyp,
        praezisierung=praezisierung.get(),
        inhalt=inhalt.get(),
        vorbefuellung=vorbefuellung.expect(XDF3_VORBEFUELLUNG),
        werte=werte,
        codeliste_referenz=codeliste_referenz,
        code_key=code_key.get(),
        name_key=name_key.get(),
        help_key=help_key.get(),
        regeln=regeln,
        max_size=max_size,
        media_type=media_type,
    )


def _parse_werte(parser: xml.XmlParser) -> list[ListenWert]:
    keys: set[str] = set()
    werte = []

    while (child := parser.next_child()) is not None:
        if child.tag == XDF3_WERT:
            wert = _parse_wert(parser)

            # Each code must be unique according to the standard.
            if wert.code in keys:
                raise InternalParserException(
                    f"Duplicate key `{wert.code}` in embedded code list"
                )
            keys.add(wert.code)
            werte.append(wert)
        else:
            parser.skip_node()

    if len(werte) == 0:
        raise InternalParserException("embedded code list must not be empty")

    return werte


def _parse_wert(parser: xml.XmlParser) -> ListenWert:
    code = xml.ParsedValue[str]()
    name = xml.ParsedValue[str]()
    hilfe = xml.ParsedValue[str]()

    while (child := parser.next_child()) is not None:
        if child.tag == XDF3_CODE:
            code.set(parser.parse_token(XDF3_CODE))
        elif child.tag == XDF3_NAME:
            name.set(parser.parse_token(XDF3_NAME))
        elif child.tag == XDF3_HILFE:
            hilfe.set(parser.parse_optional_token())
        else:
            parser.skip_node()

    return ListenWert(
        code=code.expect(XDF3_CODE), name=name.expect(XDF3_NAME), hilfe=hilfe.get()
    )


def _parse_praezisierung(
    parser: xml.XmlParser,
    child: xml.XmlElement,  # type: ignore
) -> Praezisierung | None:
    try:
        min_length = _to_optional_int(child.get("minLength", None))
        max_length = _to_optional_int(child.get("maxLength", None))
    except ValueError as error:
        raise InternalParserException("Invalid praezisierung") from error

    min_value = child.get("minValue", None)
    max_value = child.get("maxValue", None)
    pattern = child.get("pattern", None)

    parser.skip_node()

    return Praezisierung(
        min_length=min_length,
        max_length=max_length,
        min_value=min_value,
        max_value=max_value,
        pattern=pattern,
    )


def _to_optional_int(value: str | None) -> int | None:
    if value is None:
        return None
    else:
        return int(value)


class ElementContainer:
    def __init__(self):
        self.allgemein = AllgemeineAngabenContainer()
        self.bezeichnung_eingabe = xml.ParsedValue[din91379.StringLatin]()
        self.bezeichnung_ausgabe = xml.ParsedValue[din91379.StringLatin]()
        self.schema_element_art = xml.ParsedValue[SchemaElementArt]()
        self.hilfetext_eingabe = xml.ParsedValue[din91379.StringLatin]()
        self.hilfetext_ausgabe = xml.ParsedValue[din91379.StringLatin]()

    def handle_child(self, parser: xml.XmlParser, child: xml.XmlElement) -> bool:  # type: ignore
        if child.tag == XDF3_BEZEICHNUNG_EINGABE:
            self.bezeichnung_eingabe.set(
                parser.parse_string_latin(XDF3_BEZEICHNUNG_EINGABE)
            )
        elif child.tag == XDF3_BEZEICHNUNG_AUSGABE:
            self.bezeichnung_ausgabe.set(parser.parse_optional_string_latin())
        elif child.tag == XDF3_SCHEMA_ELEMENT_ART:
            self.schema_element_art.set(parse_schema_element_art(parser.parse_code()))
        elif child.tag == XDF3_HILFETEXT_EINGABE:
            self.hilfetext_eingabe.set(parser.parse_optional_string_latin())
        elif child.tag == XDF3_HILFETEXT_AUSGABE:
            self.hilfetext_ausgabe.set(parser.parse_optional_string_latin())
        else:
            return self.allgemein.handle_child(parser, child)

        return True

    def finish(self) -> Element:
        allgemein = self.allgemein.finish()

        return Element(
            identifier=allgemein.identifier,
            name=allgemein.name,
            beschreibung=allgemein.beschreibung,
            definition=allgemein.definition,
            bezug=allgemein.bezug,
            freigabe_status=allgemein.freigabe_status,
            status_gesetzt_am=allgemein.status_gesetzt_am,
            status_gesetzt_durch=allgemein.status_gesetzt_durch,
            gueltig_ab=allgemein.gueltig_ab,
            gueltig_bis=allgemein.gueltig_bis,
            versionshinweis=allgemein.versionshinweis,
            veroeffentlichungsdatum=allgemein.veroeffentlichungsdatum,
            letzte_aenderung=allgemein.letzte_aenderung,
            relation=allgemein.relation,
            stichwort=allgemein.stichwort,
            bezeichnung_eingabe=self.bezeichnung_eingabe.expect(
                XDF3_BEZEICHNUNG_EINGABE
            ),
            bezeichnung_ausgabe=self.bezeichnung_ausgabe.get(),
            schema_element_art=self.schema_element_art.expect(XDF3_SCHEMA_ELEMENT_ART),
            hilfetext_eingabe=self.hilfetext_eingabe.get(),
            hilfetext_ausgabe=self.hilfetext_ausgabe.get(),
        )


def _parse_genericode_identifier(parser: xml.XmlParser) -> genericode.Identifier:
    canonical_id = xml.ParsedValue[str]()
    version = xml.ParsedValue[str]()
    canonical_version_uri = xml.ParsedValue[str]()

    while (child := parser.next_child()) is not None:
        if child.tag == XDF3_CANONICAL_IDENTIFICATION:
            canonical_id.set(parser.parse_token(XDF3_CANONICAL_IDENTIFICATION))
        elif child.tag == XDF3_VERSION:
            version.set(parser.parse_optional_token())
        elif child.tag == XDF3_CANONICAL_VERSION_URI:
            canonical_version_uri.set(parser.parse_token(XDF3_CANONICAL_VERSION_URI))
        else:
            parser.skip_node()

    try:
        return genericode.Identifier(
            canonical_uri=canonical_id.expect(XDF3_CANONICAL_IDENTIFICATION),
            version=version.get(),
            canonical_version_uri=canonical_version_uri.expect(
                XDF3_CANONICAL_VERSION_URI
            ),
        )
    except genericode.GenericodeException as error:
        raise InternalParserException(
            f"invalid genericode identification: {error}"
        ) from error


logger = logging.getLogger(__name__)


def serialize_header(header: MessageHeader) -> ET.Element:
    element = ET.Element(XDF3_MESSAGE_HEADER)

    ET.SubElement(element, XDF3_MESSAGE_ID).text = header.nachricht_id
    ET.SubElement(
        element, XDF3_CREATION_TIME
    ).text = header.erstellungs_zeitpunkt.isoformat()

    return element


def attach_allgemeine_angaben(element: ET.Element, data: AllgemeineAngaben):
    element.append(serialize_identifier(data.identifier))
    ET.SubElement(element, XDF3_NAME).text = data.name

    add_value_if_not_none(element, XDF3_BESCHREIBUNG, data.beschreibung)
    add_value_if_not_none(element, XDF3_DEFINITION, data.definition)
    _attach_bezug(element, data.bezug)

    element.append(
        serialize_code(
            XDF3_FREIGABE_STATUS,
            data.freigabe_status.value,
            list_uri=FREIGABE_STATUS_CODE_LIST_URI,
            list_version=FREIGABE_STATUS_CODE_LIST_VERSION,
        )
    )

    if data.status_gesetzt_am is not None:
        ET.SubElement(
            element, XDF3_STATUS_GESETZT_AM
        ).text = data.status_gesetzt_am.isoformat()

    add_value_if_not_none(element, XDF3_STATUS_GESETZT_DURCH, data.status_gesetzt_durch)

    if data.gueltig_ab is not None:
        ET.SubElement(element, XDF3_GUELTIG_AB).text = data.gueltig_ab.isoformat()

    if data.gueltig_bis is not None:
        ET.SubElement(element, XDF3_GUELTIG_BIS).text = data.gueltig_bis.isoformat()

    add_value_if_not_none(element, XDF3_VERSIONSHINWEIS, data.versionshinweis)

    if data.veroeffentlichungsdatum is not None:
        ET.SubElement(
            element, XDF3_VEROEFFENTLICHUNGSDATUM
        ).text = data.veroeffentlichungsdatum.isoformat()

    ET.SubElement(
        element, XDF3_LETZTE_AENDERUNG
    ).text = data.letzte_aenderung.isoformat()

    _attach_relation(element, data.relation)
    _attach_stichwort(element, data.stichwort)


def attach_rules(
    parent: ET.Element,
    regel_identifiers: list[Identifier],
    rule_map: dict[Identifier, Regel],
):
    for identifier in regel_identifiers:
        rule = rule_map.get(identifier)
        if rule is None:
            raise XdfException(
                f"Cannot serialize xdf3 message: Regel {identifier.id}V{identifier.version} is unknown"
            )

        parent.append(serialize_rule(rule))


def serialize_rule(rule: Regel) -> ET.Element:
    rule_node = ET.Element(XDF3_REGEL)

    rule_node.append(serialize_identifier(rule.identifier))
    ET.SubElement(rule_node, XDF3_NAME).text = rule.name

    add_value_if_not_none(rule_node, XDF3_BESCHREIBUNG, rule.beschreibung)
    add_value_if_not_none(rule_node, XDF3_FREITEXT_REGEL, rule.freitext_regel)
    _attach_bezug(rule_node, rule.bezug)
    _attach_stichwort(rule_node, rule.stichwort)

    add_value_if_not_none(
        rule_node, XDF3_FACHLICHER_ERSTELLER, rule.fachlicher_ersteller
    )

    ET.SubElement(
        rule_node, XDF3_LETZTE_AENDERUNG
    ).text = rule.letzte_aenderung.isoformat()

    rule_node.append(
        serialize_code(
            XDF3_TYP,
            rule.typ.value,
            list_uri=REGELTYP_CODE_LIST_URI,
            list_version=REGELTYP_CODE_LIST_VERSION,
        )
    )

    for param in rule.param:
        param_element = ET.SubElement(
            rule_node, XDF3_PARAM, element=param.element, name=param.name
        )
        ET.SubElement(param_element, XDF3_TYP).text = param.typ.value

    for ziel in rule.ziel:
        ET.SubElement(rule_node, XDF3_ZIEL, element=ziel.element, name=ziel.name)

    if rule.skript is not None:
        ET.SubElement(rule_node, XDF3_SKRIPT).text = rule.skript

    for fehler in rule.fehler:
        ET.SubElement(
            rule_node, XDF3_FEHLER, code=str(fehler.code)
        ).text = fehler.message

    return rule_node


def attach_struktur(
    element: ET.Element,
    struktur_list: list[ElementReference],
    group_map: dict[Identifier, Gruppe],
    field_map: dict[Identifier, Datenfeld],
    rule_map: dict[Identifier, Regel],
):
    for reference in struktur_list:
        struktur = ET.SubElement(element, XDF3_STRUKTUR)
        ET.SubElement(struktur, XDF3_ANZAHL).text = _anzahl_to_string(reference.anzahl)

        _attach_bezug(struktur, reference.bezug)

        enthaelt = ET.SubElement(struktur, XDF3_ENTHAELT)

        match reference.element_type:
            case ElementType.GRUPPE:
                group = group_map.get(reference.identifier)
                if group is None:
                    raise XdfException(
                        f"Cannot serialize xdf3 message: Gruppe {reference.identifier.id}V{reference.identifier.version} is unknown"
                    )

                child = serialize_group(
                    group, group_map=group_map, field_map=field_map, rule_map=rule_map
                )
            case ElementType.FELD:
                field = field_map.get(reference.identifier)
                if field is None:
                    raise XdfException(
                        f"Cannot serialize xdf3 message: Datenfeld {reference.identifier.id}V{reference.identifier.version} is unknown"
                    )

                child = serialize_field(field, rule_map=rule_map)

        enthaelt.append(child)


def _anzahl_to_string(value: Anzahl) -> str:
    left = str(value.min)
    right = str(value.max) if value.max is not None else "*"

    return f"{left}:{right}"


def serialize_group(
    group: Gruppe,
    group_map: dict[Identifier, Gruppe],
    field_map: dict[Identifier, Datenfeld],
    rule_map: dict[Identifier, Regel],
) -> ET.Element:
    element = ET.Element(XDF3_DATENFELDGRUPPE)

    attach_allgemeine_angaben(element, group)
    _attach_element_angaben(element, group)

    if group.art is not None:
        element.append(
            serialize_code(
                XDF3_ART,
                group.art.value,
                list_uri=GRUPPENART_CODE_LIST_URI,
                list_version=GRUPPENART_CODE_LIST_VERSION,
            )
        )

    attach_rules(element, group.regeln, rule_map)
    attach_struktur(element, group.struktur, group_map, field_map, rule_map)

    return element


def serialize_field(field: Datenfeld, rule_map: dict[Identifier, Regel]) -> ET.Element:
    element = ET.Element(XDF3_DATENFELD)

    attach_allgemeine_angaben(element, field)
    _attach_element_angaben(element, field)

    element.append(
        serialize_code(
            XDF3_FELDART,
            field.feldart.value,
            list_uri=FELDART_CODE_LIST_URI,
            list_version=FELDART_CODE_LIST_VERSION,
        )
    )

    element.append(
        serialize_code(
            XDF3_DATENTYP,
            field.datentyp.value,
            list_uri=DATENTYP_CODE_LIST_URI,
            list_version=DATENTYP_CODE_LIST_VERSION,
        )
    )

    if field.praezisierung is not None:
        praezisierung = ET.SubElement(element, XDF3_PRAEZISIERUNG)
        if field.praezisierung.min_length is not None:
            praezisierung.attrib["minLength"] = str(field.praezisierung.min_length)
        if field.praezisierung.max_length is not None:
            praezisierung.attrib["maxLength"] = str(field.praezisierung.max_length)
        if field.praezisierung.min_value is not None:
            praezisierung.attrib["minValue"] = str(field.praezisierung.min_value)
        if field.praezisierung.max_value is not None:
            praezisierung.attrib["maxValue"] = str(field.praezisierung.max_value)
        if field.praezisierung.pattern is not None:
            praezisierung.attrib["pattern"] = str(field.praezisierung.pattern)

    add_value_if_not_none(element, XDF3_INHALT, field.inhalt)

    element.append(
        serialize_code(
            XDF3_VORBEFUELLUNG,
            field.vorbefuellung.value,
            list_uri=VORBEFUELLUNG_CODE_LIST_URI,
            list_version=VORBEFUELLUNG_CODE_LIST_VERSION,
        )
    )

    if field.werte is not None:
        element.append(_serialize_embedded_code_list(field.werte))

    if field.codeliste_referenz is not None:
        element.append(_serialize_code_list_reference(field.codeliste_referenz))

    add_value_if_not_none(element, XDF3_CODE_KEY, field.code_key)
    add_value_if_not_none(element, XDF3_NAME_KEY, field.name_key)
    add_value_if_not_none(element, XDF3_HELP_KEY, field.help_key)
    attach_rules(element, field.regeln, rule_map)
    add_value_if_not_none(element, XDF3_MAX_SIZE, field.max_size)

    for media_type in field.media_type:
        ET.SubElement(element, XDF3_MEDIA_TYPE).text = media_type

    return element


def _serialize_embedded_code_list(werte: list[ListenWert]) -> ET.Element:
    element = ET.Element(XDF3_WERTE)

    for wert in werte:
        wert_element = ET.SubElement(element, XDF3_WERT)
        ET.SubElement(wert_element, XDF3_CODE).text = wert.code
        ET.SubElement(wert_element, XDF3_NAME).text = wert.name

        if wert.hilfe is not None:
            ET.SubElement(wert_element, XDF3_HILFE).text = wert.hilfe

    return element


def _serialize_code_list_reference(reference: genericode.Identifier) -> ET.Element:
    element = ET.Element(XDF3_CODELISTE_REFERENZ)

    ET.SubElement(element, XDF3_CANONICAL_IDENTIFICATION).text = reference.canonical_uri

    add_value_if_not_none(element, XDF3_VERSION, reference.version)

    ET.SubElement(
        element, XDF3_CANONICAL_VERSION_URI
    ).text = reference.canonical_version_uri

    return element


def _attach_element_angaben(element: ET.Element, data: Element):
    ET.SubElement(element, XDF3_BEZEICHNUNG_EINGABE).text = data.bezeichnung_eingabe

    add_value_if_not_none(element, XDF3_BEZEICHNUNG_AUSGABE, data.bezeichnung_ausgabe)

    element.append(
        serialize_code(
            XDF3_SCHEMA_ELEMENT_ART,
            data.schema_element_art.value,
            list_uri=SCHEMA_ELEMENT_ART_CODE_LIST_URI,
            list_version=SCHEMA_ELEMENT_ART_CODE_LIST_VERSION,
        )
    )

    add_value_if_not_none(element, XDF3_HILFETEXT_EINGABE, data.hilfetext_eingabe)
    add_value_if_not_none(element, XDF3_HILFETEXT_AUSGABE, data.hilfetext_ausgabe)


def _attach_bezug(element: ET.Element, bezug_list: list[Rechtsbezug]):
    for bezug in bezug_list:
        bezug_element = ET.SubElement(element, XDF3_BEZUG)
        bezug_element.text = bezug.text
        if bezug.link is not None:
            bezug_element.attrib["link"] = bezug.link


def _attach_relation(element: ET.Element, relation_list: list[Relation]):
    for relation in relation_list:
        relation_element = ET.SubElement(element, XDF3_RELATION)

        relation_element.append(
            serialize_code(
                XDF3_PRAEDIKAT,
                relation.praedikat.value,
                list_uri=RELATION_TYP_CODE_LIST_URI,
                list_version=RELATION_TYP_CODE_LIST_VERSION,
            )
        )

        relation_element.append(
            serialize_identifier(relation.objekt, root_tag=XDF3_OBJEKT)
        )


def _attach_stichwort(element: ET.Element, stichwort_list: list[Stichwort]):
    for stichwort in stichwort_list:
        ET.SubElement(element, XDF3_STICHWORT, uri=stichwort.uri).text = stichwort.value


def serialize_identifier(
    identifier: Identifier, root_tag: str = XDF3_IDENTIFIKATION
) -> ET.Element:
    root = ET.Element(root_tag)

    ET.SubElement(root, XDF3_ID).text = identifier.id
    add_value_if_not_none(root, XDF3_VERSION, identifier.version)

    return root


def add_value_if_not_none(parent: ET.Element, tag: str, value: str | int | None):
    if value is not None:
        ET.SubElement(parent, tag).text = str(value)


def serialize_code(
    tag: str,
    value: str | int,
    list_uri: str,
    list_version: str | None = None,
) -> ET.Element:
    element = ET.Element(tag)
    element.attrib["listURI"] = list_uri
    if list_version is not None:
        element.attrib["listVersionID"] = list_version

    ET.SubElement(element, "code").text = str(value)

    return element
