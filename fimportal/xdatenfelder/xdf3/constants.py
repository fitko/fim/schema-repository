from __future__ import annotations

from pathlib import Path

from fimportal import xml
from fimportal.common import FreigabeStatus

XDF3_NS = "{urn:xoev-de:fim:standard:xdatenfelder_3.0.0}"

XDF3_MESSAGE_ID = f"{XDF3_NS}nachrichtID"
XDF3_CREATION_TIME = f"{XDF3_NS}erstellungszeitpunkt"

XDF3_IDENTIFIKATION = f"{XDF3_NS}identifikation"
XDF3_ID = f"{XDF3_NS}id"
XDF3_VERSION = f"{XDF3_NS}version"
XDF3_NAME = f"{XDF3_NS}name"
XDF3_BESCHREIBUNG = f"{XDF3_NS}beschreibung"
XDF3_DEFINITION = f"{XDF3_NS}definition"
XDF3_BEZUG = f"{XDF3_NS}bezug"
XDF3_STICHWORT = f"{XDF3_NS}stichwort"
XDF3_FREIGABE_STATUS = f"{XDF3_NS}freigabestatus"
XDF3_VERSIONSHINWEIS = f"{XDF3_NS}versionshinweis"
XDF3_STATUS_GESETZT_AM = f"{XDF3_NS}statusGesetztAm"
XDF3_STATUS_GESETZT_DURCH = f"{XDF3_NS}statusGesetztDurch"
XDF3_GUELTIG_AB = f"{XDF3_NS}gueltigAb"
XDF3_GUELTIG_BIS = f"{XDF3_NS}gueltigBis"
XDF3_LETZTE_AENDERUNG = f"{XDF3_NS}letzteAenderung"
XDF3_RELATION = f"{XDF3_NS}relation"
XDF3_VEROEFFENTLICHUNGSDATUM = f"{XDF3_NS}veroeffentlichungsdatum"

XDF3_PRAEDIKAT = f"{XDF3_NS}praedikat"
XDF3_OBJEKT = f"{XDF3_NS}objekt"

XSD_SCHEMA_PATH = Path(__file__).parent / "xsd" / "xdatenfelder.xsd"
SCHEMA = xml.load_xsd(XSD_SCHEMA_PATH)

XDF3_MESSAGE_HEADER = f"{XDF3_NS}header"

XDF3_SCHEMA_MESSAGE = f"{XDF3_NS}xdatenfelder.stammdatenschema.0102"
XDF3_DATENFELDGRUPPE_MESSAGE = f"{XDF3_NS}xdatenfelder.datenfeldgruppe.0103"
XDF3_DATENFELD_MESSAGE = f"{XDF3_NS}xdatenfelder.datenfeld.0104"

XDF3_SCHEMA = f"{XDF3_NS}stammdatenschema"
XDF3_BEZEICHNUNG = f"{XDF3_NS}bezeichnung"
XDF3_HILFETEXT = f"{XDF3_NS}hilfetext"
XDF3_DOKUMENTSTECKBRIEF = f"{XDF3_NS}dokumentsteckbrief"
XDF3_ABLEITUNGSMODIFIKATIONEN_STRUKTUR = f"{XDF3_NS}ableitungsmodifikationenStruktur"
XDF3_ABLEITUNGSMODIFIKATIONEN_REPRAESENTATION = (
    f"{XDF3_NS}ableitungsmodifikationenRepraesentation"
)

XDF3_REGEL = f"{XDF3_NS}regel"
XDF3_STRUKTUR = f"{XDF3_NS}struktur"
XDF3_ANZAHL = f"{XDF3_NS}anzahl"
XDF3_ENTHAELT = f"{XDF3_NS}enthaelt"
XDF3_DATENFELDGRUPPE = f"{XDF3_NS}datenfeldgruppe"
XDF3_DATENFELD = f"{XDF3_NS}datenfeld"
XDF3_ART = f"{XDF3_NS}art"
XDF3_BEZEICHNUNG_EINGABE = f"{XDF3_NS}bezeichnungEingabe"
XDF3_BEZEICHNUNG_AUSGABE = f"{XDF3_NS}bezeichnungAusgabe"
XDF3_SCHEMA_ELEMENT_ART = f"{XDF3_NS}schemaelementart"
XDF3_HILFETEXT_EINGABE = f"{XDF3_NS}hilfetextEingabe"
XDF3_HILFETEXT_AUSGABE = f"{XDF3_NS}hilfetextAusgabe"
XDF3_FELDART = f"{XDF3_NS}feldart"
XDF3_DATENTYP = f"{XDF3_NS}datentyp"
XDF3_PRAEZISIERUNG = f"{XDF3_NS}praezisierung"
XDF3_INHALT = f"{XDF3_NS}inhalt"
XDF3_VORBEFUELLUNG = f"{XDF3_NS}vorbefuellung"
XDF3_WERTE = f"{XDF3_NS}werte"
XDF3_WERT = f"{XDF3_NS}wert"
XDF3_CODE = f"{XDF3_NS}code"
XDF3_HILFE = f"{XDF3_NS}hilfe"
XDF3_CODELISTE_REFERENZ = f"{XDF3_NS}codelisteReferenz"
XDF3_CODE_KEY = f"{XDF3_NS}codeKey"
XDF3_NAME_KEY = f"{XDF3_NS}nameKey"
XDF3_HELP_KEY = f"{XDF3_NS}helpKey"
XDF3_MAX_SIZE = f"{XDF3_NS}maxSize"
XDF3_MEDIA_TYPE = f"{XDF3_NS}mediaType"

XDF3_FREITEXT_REGEL = f"{XDF3_NS}freitextRegel"
XDF3_FACHLICHER_ERSTELLER = f"{XDF3_NS}fachlicherErsteller"
XDF3_TYP = f"{XDF3_NS}typ"
XDF3_PARAM = f"{XDF3_NS}param"
XDF3_ZIEL = f"{XDF3_NS}ziel"
XDF3_SKRIPT = f"{XDF3_NS}skript"
XDF3_FEHLER = f"{XDF3_NS}fehler"

XDF3_GENERICODE_IDENTIFIKATION = f"{XDF3_NS}genericodeIdentification"
XDF3_CANONICAL_IDENTIFICATION = f"{XDF3_NS}canonicalIdentification"
XDF3_CANONICAL_VERSION_URI = f"{XDF3_NS}canonicalVersionUri"

REGELTYP_CODE_LIST_URI = "urn:xoev-de:fim:codeliste:xdatenfelder.regeltyp"
REGELTYP_CODE_LIST_VERSION = "1.0"

FELDART_CODE_LIST_URI = "urn:xoev-de:fim:codeliste:xdatenfelder.feldart"
FELDART_CODE_LIST_VERSION = "2.0"

DATENTYP_CODE_LIST_URI = "urn:xoev-de:fim:codeliste:xdatenfelder.datentyp"
DATENTYP_CODE_LIST_VERSION = "2.0"

GRUPPENART_CODE_LIST_URI = "urn:xoev-de:fim:codeliste:datenfelder.gruppenart"
GRUPPENART_CODE_LIST_VERSION = "1.0"

VORBEFUELLUNG_CODE_LIST_URI = "urn:xoev-de:fim:codeliste:xdatenfelder.vorbefuellung"
VORBEFUELLUNG_CODE_LIST_VERSION = "1.0"

RELATION_TYP_CODE_LIST_URI = "urn:xoev-de:fim-datenfelder:codeliste:relation"
RELATION_TYP_CODE_LIST_VERSION = "1.2"

FREIGABE_STATUS_VALID_FOR_UPLOAD: list[FreigabeStatus] = [
    FreigabeStatus.ENTWURF,
    FreigabeStatus.METHODISCH_FREIGEGEBEN,
    FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER,
    FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
    FreigabeStatus.INAKTIV,
    FreigabeStatus.VORGESEHEN_ZUM_LOESCHEN,
]

XDF3_STECKBRIEF_DOKUMENTART = f"{XDF3_NS}dokumentart"
XDF3_STECKBRIEF_IST_ABSTRAKT = f"{XDF3_NS}istAbstrakt"
XDF3_STECKBRIEF_BEZEICHNUNG = f"{XDF3_NS}bezeichnung"
XDF3_STECKBRIEF_TITLE = f"{XDF3_NS}xdatenfelder.dokumentsteckbrief.0101"
XDF3_STECKBRIEF_MESSAGE = f"{XDF3_NS}dokumentsteckbrief"
