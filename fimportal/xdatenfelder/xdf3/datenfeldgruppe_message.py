import xml.etree.ElementTree as ET
from dataclasses import dataclass
from pathlib import Path

from fimportal import genericode, xml
from fimportal.xdatenfelder.common import (
    InternalParserException,
    InvalidVersionException,
    ParserException,
)
from fimportal.xdatenfelder.xdf3.common import (
    Datenfeld,
    Gruppe,
    Identifier,
    MessageHeader,
    ParseContext,
    Regel,
    parse_datenfeldgruppe,
    parse_message_header,
    serialize_group,
    serialize_header,
)
from fimportal.xdatenfelder.xdf3.constants import (
    XDF3_DATENFELDGRUPPE,
    XDF3_DATENFELDGRUPPE_MESSAGE,
    XDF3_MESSAGE_HEADER,
)


@dataclass(slots=True)
class DatenfeldgruppeMessage:
    """
    The top-level data structure containing all the information
    of a parsed xdf3 Datenfeld message.
    """

    header: MessageHeader
    group: Gruppe

    groups: dict[Identifier, Gruppe]
    fields: dict[Identifier, Datenfeld]
    rules: dict[Identifier, Regel]

    @property
    def id(self) -> str:
        return self.group.identifier.id

    @property
    def version(self) -> str | None:
        return self.group.identifier.version

    def assert_version(self) -> str:
        return self.group.identifier.assert_version()


def load_datenfeldgruppe_message(path: str | Path) -> DatenfeldgruppeMessage:
    """
    Load and parse an xdf3 Datenfeldgruppe message from a file.
    """

    with open(path, "rb") as file:
        return parse_datenfeldgruppe_message(file.read())


def parse_datenfeldgruppe_message(input: bytes | str) -> DatenfeldgruppeMessage:
    """
    Parse an xdf3 Datenfeldgruppe message from the provided input.
    """

    if isinstance(input, str):
        input = input.encode("utf-8")

    parser = xml.XmlParser(input)
    context = ParseContext()

    try:
        parser.expect_child(XDF3_DATENFELDGRUPPE_MESSAGE)

        header = xml.ParsedValue[MessageHeader]()
        group = xml.ParsedValue[Gruppe]()

        while (child := parser.next_child()) is not None:
            if child.tag == XDF3_MESSAGE_HEADER:
                header.set(parse_message_header(parser))
            elif child.tag == XDF3_DATENFELDGRUPPE:
                group.set(parse_datenfeldgruppe(parser, context))
            else:
                parser.skip_node()

        return DatenfeldgruppeMessage(
            header=header.expect(XDF3_MESSAGE_HEADER),
            group=group.expect(XDF3_DATENFELDGRUPPE),
            groups=context.groups,
            fields=context.fields,
            rules=context.rules,
        )
    except (
        xml.ParserException,
        genericode.GenericodeException,
        InternalParserException,
        InvalidVersionException,
    ) as error:
        raise ParserException(
            f"Could not parse Datenfeld: {str(error)}", parser.current_line
        ) from error


def serialize_datenfeldgruppe_message(message: DatenfeldgruppeMessage) -> bytes:
    """
    Serialize the xdf2 Datenfeld message into an xml string.
    """

    root = ET.Element(XDF3_DATENFELDGRUPPE_MESSAGE)
    root.append(serialize_header(message.header))
    root.append(
        serialize_group(
            message.group,
            group_map=message.groups,
            field_map=message.fields,
            rule_map=message.rules,
        )
    )

    return ET.tostring(root, encoding="utf-8", xml_declaration=True)
