from __future__ import annotations

import logging
import xml.etree.ElementTree as ET
from dataclasses import dataclass
from pathlib import Path

from fimportal import din91379, genericode, xml
from fimportal.xdatenfelder.common import (
    ABLEITUNGS_REPR_CODE_LIST_URI,
    ABLEITUNGS_REPR_CODE_LIST_VERSION,
    ABLEITUNGS_STRUKTUR_CODE_LIST_URI,
    ABLEITUNGS_STRUKTUR_CODE_LIST_VERSION,
    AbleitungsmodifikationenRepraesentation,
    AbleitungsmodifikationenStruktur,
    InternalParserException,
    InvalidVersionException,
    ParserException,
    XdfException,
    parse_ableitungsmodifikationen_repraesentation,
    parse_ableitungsmodifikationen_struktur,
)
from fimportal.xdatenfelder.xdf3.common import (
    AllgemeineAngaben,
    AllgemeineAngabenContainer,
    Datenfeld,
    ElementReference,
    Gruppe,
    Identifier,
    MessageHeader,
    ParseContext,
    Regel,
    Version,
    add_value_if_not_none,
    attach_allgemeine_angaben,
    attach_rules,
    attach_struktur,
    parse_identifier,
    parse_message_header,
    parse_rule,
    parse_structure,
    serialize_code,
    serialize_header,
    serialize_identifier,
)
from fimportal.xdatenfelder.xdf3.constants import (
    SCHEMA,
    XDF3_ABLEITUNGSMODIFIKATIONEN_REPRAESENTATION,
    XDF3_ABLEITUNGSMODIFIKATIONEN_STRUKTUR,
    XDF3_BEZEICHNUNG,
    XDF3_DOKUMENTSTECKBRIEF,
    XDF3_HILFETEXT,
    XDF3_MESSAGE_HEADER,
    XDF3_REGEL,
    XDF3_SCHEMA,
    XDF3_SCHEMA_MESSAGE,
    XDF3_STRUKTUR,
)

logger = logging.getLogger(__name__)


@dataclass(slots=True)
class SchemaMessage:
    """
    The top-level data structure containing all the information
    of a parsed xdf3 schema file.
    """

    header: MessageHeader
    schema: "Schema"

    groups: dict[Identifier, Gruppe]
    fields: dict[Identifier, Datenfeld]
    rules: dict[Identifier, Regel]

    @property
    def identifier(self) -> Identifier:
        return self.schema.identifier

    @property
    def id(self) -> str:
        return self.schema.identifier.id

    @property
    def version(self) -> Version | None:
        return self.schema.identifier.version

    def assert_version(self) -> Version:
        return self.schema.identifier.assert_version()

    def get_code_list_identifiers(self) -> set[genericode.Identifier]:
        """
        Get a set of all code list `GenericodeIdentifier`s within the schema.
        """

        identifiers: set[genericode.Identifier] = set()

        for data_field in self.fields.values():
            if data_field.codeliste_referenz is None:
                continue

            identifiers.add(data_field.codeliste_referenz)

        return identifiers

    def get_code_list_uris(self) -> set[str]:
        return {
            identifier.canonical_version_uri
            for identifier in self.get_code_list_identifiers()
        }


@dataclass(slots=True)
class Schema(AllgemeineAngaben):
    bezeichnung: din91379.StringLatin
    hilfetext: din91379.StringLatin | None
    ableitungsmodifikationen_struktur: AbleitungsmodifikationenStruktur
    ableitungsmodifikationen_repraesentation: AbleitungsmodifikationenRepraesentation
    dokumentsteckbrief: Identifier
    regeln: list[Identifier]
    struktur: list[ElementReference]


def load_schema_message(path: str | Path) -> SchemaMessage:
    """
    Load and parse an xdf3 schema message from a file.
    """

    with open(path, "rb") as file:
        return parse_schema_message(file.read())


def parse_schema_message(input: bytes | str) -> SchemaMessage:
    """
    Parse an xdf3 schema message from the provided input.
    """

    context = ParseContext()

    if isinstance(input, str):
        input = input.encode("utf-8")

    try:
        xml.validate_schema(input, SCHEMA)
    except xml.ParserException as error:
        raise XdfException(f"Cannot parse schema: {error}") from error

    parser = xml.XmlParser(input)

    try:
        parser.expect_child(XDF3_SCHEMA_MESSAGE)

        header = xml.ParsedValue[MessageHeader]()
        schema = xml.ParsedValue[Schema]()

        while (child := parser.next_child()) is not None:
            if child.tag == XDF3_MESSAGE_HEADER:
                header.set(parse_message_header(parser))
            elif child.tag == XDF3_SCHEMA:
                schema.set(_parse_schema(parser, context))
            else:
                parser.skip_node()

        return SchemaMessage(
            header=header.expect(XDF3_MESSAGE_HEADER),
            schema=schema.expect(XDF3_SCHEMA),
            groups=context.groups,
            fields=context.fields,
            rules=context.rules,
        )
    except (
        xml.ParserException,
        genericode.GenericodeException,
        InvalidVersionException,
        InternalParserException,
    ) as error:
        raise ParserException(
            f"Could not parse schema: {str(error)}", parser.current_line
        ) from error


def _parse_schema(parser: xml.XmlParser, context: ParseContext) -> Schema:
    container = AllgemeineAngabenContainer()
    bezeichnung = xml.ParsedValue[din91379.StringLatin]()
    hilfetext = xml.ParsedValue[din91379.StringLatin]()
    ableitungsmodifikationen_struktur: xml.ParsedValue[
        AbleitungsmodifikationenStruktur
    ] = xml.ParsedValue()
    ableitungsmodifikationen_repraesentation: xml.ParsedValue[
        AbleitungsmodifikationenRepraesentation
    ] = xml.ParsedValue()
    dokumentsteckbrief = xml.ParsedValue[Identifier]()
    struktur: list[ElementReference] = []
    regeln: list[Identifier] = []

    while (child := parser.next_child()) is not None:
        if container.handle_child(parser, child):
            continue

        if child.tag == XDF3_BEZEICHNUNG:
            bezeichnung.set(parser.parse_string_latin(XDF3_BEZEICHNUNG))
        elif child.tag == XDF3_HILFETEXT:
            hilfetext.set(parser.parse_string_latin(XDF3_HILFETEXT))
        elif child.tag == XDF3_ABLEITUNGSMODIFIKATIONEN_STRUKTUR:
            ableitungsmodifikationen_struktur.set(
                parse_ableitungsmodifikationen_struktur(parser.parse_code())
            )
        elif child.tag == XDF3_ABLEITUNGSMODIFIKATIONEN_REPRAESENTATION:
            ableitungsmodifikationen_repraesentation.set(
                parse_ableitungsmodifikationen_repraesentation(parser.parse_code())
            )
        elif child.tag == XDF3_DOKUMENTSTECKBRIEF:
            dokumentsteckbrief.set(parse_identifier(parser))
        elif child.tag == XDF3_REGEL:
            regeln.append(parse_rule(parser, context))
        elif child.tag == XDF3_STRUKTUR:
            struktur.append(parse_structure(parser, context))
        else:
            parser.skip_node()

    # This is already checked by the XSD file, this is just a sanity check
    assert len(struktur) > 0

    allgemeine_angaben = container.finish()

    return Schema(
        identifier=allgemeine_angaben.identifier,
        name=allgemeine_angaben.name,
        beschreibung=allgemeine_angaben.beschreibung,
        definition=allgemeine_angaben.definition,
        bezug=allgemeine_angaben.bezug,
        freigabe_status=allgemeine_angaben.freigabe_status,
        status_gesetzt_am=allgemeine_angaben.status_gesetzt_am,
        status_gesetzt_durch=allgemeine_angaben.status_gesetzt_durch,
        gueltig_ab=allgemeine_angaben.gueltig_ab,
        gueltig_bis=allgemeine_angaben.gueltig_bis,
        versionshinweis=allgemeine_angaben.versionshinweis,
        veroeffentlichungsdatum=allgemeine_angaben.veroeffentlichungsdatum,
        letzte_aenderung=allgemeine_angaben.letzte_aenderung,
        relation=allgemeine_angaben.relation,
        stichwort=allgemeine_angaben.stichwort,
        bezeichnung=bezeichnung.expect(XDF3_BEZEICHNUNG),
        hilfetext=hilfetext.get(),
        ableitungsmodifikationen_struktur=ableitungsmodifikationen_struktur.expect(
            XDF3_ABLEITUNGSMODIFIKATIONEN_STRUKTUR
        ),
        ableitungsmodifikationen_repraesentation=ableitungsmodifikationen_repraesentation.expect(
            XDF3_ABLEITUNGSMODIFIKATIONEN_REPRAESENTATION
        ),
        dokumentsteckbrief=dokumentsteckbrief.expect(XDF3_DOKUMENTSTECKBRIEF),
        regeln=regeln,
        struktur=struktur,
    )


def serialize_schema_message(message: SchemaMessage) -> bytes:
    """
    Serialize the xdf3 schema message into an xml string.
    """
    root = _serialize_message(message)

    xml_content = ET.tostring(
        root,
        encoding="utf-8",
        xml_declaration=True,
    )

    return xml_content


def _serialize_message(message: SchemaMessage) -> ET.Element:
    root = ET.Element(XDF3_SCHEMA_MESSAGE)

    root.append(serialize_header(message.header))
    root.append(_serialize_schema(message))

    return root


def _serialize_schema(message: SchemaMessage) -> ET.Element:
    schema = message.schema

    element = ET.Element(XDF3_SCHEMA)
    attach_allgemeine_angaben(element, schema)

    ET.SubElement(element, XDF3_BEZEICHNUNG).text = schema.bezeichnung

    add_value_if_not_none(element, XDF3_HILFETEXT, schema.hilfetext)

    element.append(
        serialize_code(
            XDF3_ABLEITUNGSMODIFIKATIONEN_STRUKTUR,
            schema.ableitungsmodifikationen_struktur.value,
            list_uri=ABLEITUNGS_STRUKTUR_CODE_LIST_URI,
            list_version=ABLEITUNGS_STRUKTUR_CODE_LIST_VERSION,
        )
    )

    element.append(
        serialize_code(
            XDF3_ABLEITUNGSMODIFIKATIONEN_REPRAESENTATION,
            schema.ableitungsmodifikationen_repraesentation.value,
            list_uri=ABLEITUNGS_REPR_CODE_LIST_URI,
            list_version=ABLEITUNGS_REPR_CODE_LIST_VERSION,
        )
    )

    element.append(
        serialize_identifier(
            schema.dokumentsteckbrief, root_tag=XDF3_DOKUMENTSTECKBRIEF
        )
    )

    attach_rules(element, schema.regeln, rule_map=message.rules)
    attach_struktur(
        element,
        schema.struktur,
        group_map=message.groups,
        field_map=message.fields,
        rule_map=message.rules,
    )

    return element
