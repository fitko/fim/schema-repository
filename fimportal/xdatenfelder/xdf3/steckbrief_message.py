from dataclasses import dataclass
from pathlib import Path

from fimportal import din91379, genericode, xml
from fimportal.xdatenfelder.common import (
    InternalParserException,
    InvalidVersionException,
    ParserException,
)
from fimportal.xdatenfelder.xdf3.common import (
    AllgemeineAngaben,
    AllgemeineAngabenContainer,
    Dokumentart,
    MessageHeader,
    parse_dokumentart,
    parse_message_header,
)
from fimportal.xdatenfelder.xdf3.constants import (
    XDF3_HILFETEXT,
    XDF3_MESSAGE_HEADER,
    XDF3_STECKBRIEF_BEZEICHNUNG,
    XDF3_STECKBRIEF_DOKUMENTART,
    XDF3_STECKBRIEF_IST_ABSTRAKT,
    XDF3_STECKBRIEF_MESSAGE,
    XDF3_STECKBRIEF_TITLE,
)


@dataclass(slots=True)
class Steckbrief(AllgemeineAngaben):
    ist_abstrakt: bool
    dokumentart: Dokumentart
    bezeichnung: din91379.StringLatin
    hilfetext: din91379.StringLatin | None


@dataclass(slots=True)
class SteckbriefMessage:
    header: MessageHeader  # Identical to the header in `SchemaMessage`
    steckbrief: Steckbrief

    def fim_id(self) -> str:
        return self.steckbrief.identifier.id

    def assert_fim_version(self) -> str:
        assert self.steckbrief.identifier.version is not None
        return self.steckbrief.identifier.version


def _parse_steckbrief(parser: xml.XmlParser) -> Steckbrief:
    hilfetext: xml.ParsedValue[din91379.StringLatin] = xml.ParsedValue()
    bezeichnung: xml.ParsedValue[din91379.StringLatin] = xml.ParsedValue()
    dokumentart: xml.ParsedValue[Dokumentart] = xml.ParsedValue()
    ist_abstrakt: xml.ParsedValue[str] = xml.ParsedValue()
    container = AllgemeineAngabenContainer()

    while (child := parser.next_child()) is not None:
        if container.handle_child(parser, child):
            continue
        if child.tag == XDF3_STECKBRIEF_BEZEICHNUNG:
            bezeichnung.set(parser.parse_string_latin(child.tag))
        elif child.tag == XDF3_STECKBRIEF_DOKUMENTART:
            dokumentart.set(parse_dokumentart(parser.parse_code()))
        elif child.tag == XDF3_HILFETEXT:
            hilfetext.set(parser.parse_string_latin(child.tag))
        elif child.tag == XDF3_STECKBRIEF_IST_ABSTRAKT:
            ist_abstrakt.set(parser.parse_value(child.tag))
        else:
            parser.skip_node()

    allgemeine_angaben = container.finish()

    return Steckbrief(
        identifier=allgemeine_angaben.identifier,
        name=allgemeine_angaben.name,
        beschreibung=allgemeine_angaben.beschreibung,
        definition=allgemeine_angaben.definition,
        bezug=allgemeine_angaben.bezug,
        freigabe_status=allgemeine_angaben.freigabe_status,
        status_gesetzt_am=allgemeine_angaben.status_gesetzt_am,
        status_gesetzt_durch=allgemeine_angaben.status_gesetzt_durch,
        gueltig_ab=allgemeine_angaben.gueltig_ab,
        gueltig_bis=allgemeine_angaben.gueltig_bis,
        versionshinweis=allgemeine_angaben.versionshinweis,
        veroeffentlichungsdatum=allgemeine_angaben.veroeffentlichungsdatum,
        letzte_aenderung=allgemeine_angaben.letzte_aenderung,
        relation=allgemeine_angaben.relation,
        stichwort=allgemeine_angaben.stichwort,
        bezeichnung=bezeichnung.expect(XDF3_STECKBRIEF_BEZEICHNUNG),
        hilfetext=hilfetext.get(),
        dokumentart=dokumentart.expect(XDF3_STECKBRIEF_DOKUMENTART),
        ist_abstrakt=ist_abstrakt.expect(XDF3_STECKBRIEF_IST_ABSTRAKT) == "true",
    )


def load_steckbrief_message(path: str | Path) -> SteckbriefMessage:
    """
    Load and parse an xdf3 document profile from a file.
    """

    with open(path, "rb") as file:
        return parse_steckbrief_message(file.read())


def parse_steckbrief_message(input: bytes | str):
    if isinstance(input, str):
        input = input.encode("utf-8")

    parser = xml.XmlParser(input)

    try:
        parser.expect_child(XDF3_STECKBRIEF_TITLE)

        header = xml.ParsedValue[MessageHeader]()
        steckbrief = xml.ParsedValue[Steckbrief]()

        while (child := parser.next_child()) is not None:
            if child.tag == XDF3_MESSAGE_HEADER:
                header.set(parse_message_header(parser))
            elif child.tag == XDF3_STECKBRIEF_MESSAGE:
                steckbrief.set(_parse_steckbrief(parser))
            else:
                parser.skip_node()

        return SteckbriefMessage(
            header.expect(XDF3_MESSAGE_HEADER),
            steckbrief.expect(XDF3_STECKBRIEF_MESSAGE),
        )
    except (
        xml.ParserException,
        genericode.GenericodeException,
        InternalParserException,
        InvalidVersionException,
    ) as error:
        raise ParserException(
            f"Could not parse document profile: {str(error)}", parser.current_line
        ) from error
