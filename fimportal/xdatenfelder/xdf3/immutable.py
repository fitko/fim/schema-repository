"""Immutable XDF3 Datenfelder

When a schema (gruppe, feld, steckbrief) is imported again (same id, same version),
the XDatenfelder standard considers most data as immutable, meaning it cannot be
changed once defined. Only certain attributes are allowed to change, and the
import gets declined if the corresponding immutable classes of both variants
are not the same.

Explicitly mutable xdf3 attributes are:
    - freigabestatus
    - statusGesetztAm
    - veroeffentlichungsdatum - with a caveat (can be set once and then becomes immutable)
    - letzteAenderung
    - relation
    - dokumentsteckbrief (only Schema)

The mutable xdf3 attributes are part of:
    - freigabestatus: AllgemeineAngaben
    - statusGesetztAm: AllgemeineAngaben
    - veroeffentlichungsdatum: AllgemeineAngaben
    - letzteAenderung: AllgemeineAngaben, Regel
    - relation: AllgemeineAngaben
    - dokumentsteckbrief (only Schema): Schema
"""

from __future__ import annotations

from datetime import date

from pydantic import BaseModel

from fimportal import din91379, genericode
from fimportal.xdatenfelder.common import (
    AbleitungsmodifikationenRepraesentation,
    AbleitungsmodifikationenStruktur,
    Anzahl,
    ElementType,
    SchemaElementArt,
)
from fimportal.xdatenfelder.xdf3.common import (
    AllgemeineAngaben,
    Datenfeld,
    Datentyp,
    Dokumentart,
    Element,
    ElementReference,
    Fehlermeldung,
    Feldart,
    Gruppe,
    Gruppenart,
    Identifier,
    ListenWert,
    Praezisierung,
    Rechtsbezug,
    Regel,
    RegelParameter,
    Regeltyp,
    RegelZiel,
    Stichwort,
    Vorbefuellung,
)
from fimportal.xdatenfelder.xdf3.schema_message import Schema
from fimportal.xdatenfelder.xdf3.steckbrief_message import Steckbrief


class ImmutableAllgemeineAngaben(BaseModel):
    """Immutable xdf3 Allgemeine Angaben

    Explicitly mutable attributes are `freigabestatus`, `statusGesetztAm`,
    `veroeffentlichungsdatum`, `letzteAenderung`, `relation`.

    `veroeffentlichungsdatum` is mutable while it is `None`. Once assigned a date value, it
    becomes immutable. We manage its immutability seperately. For the purpose of these
    classes, `veroeffentlichungsdatum` is treated as mutable.
    """

    identifier: Identifier
    name: din91379.StringLatin
    beschreibung: din91379.StringLatin | None
    definition: din91379.StringLatin | None
    bezug: list[Rechtsbezug]
    status_gesetzt_durch: din91379.StringLatin | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    versionshinweis: din91379.StringLatin | None
    stichwort: list[Stichwort]

    @staticmethod
    def from_allgemeine_angaben(general_information: AllgemeineAngaben):
        return ImmutableAllgemeineAngaben(
            identifier=general_information.identifier,
            name=general_information.name,
            beschreibung=general_information.beschreibung,
            definition=general_information.definition,
            bezug=general_information.bezug,
            status_gesetzt_durch=general_information.status_gesetzt_durch,
            gueltig_ab=general_information.gueltig_ab,
            gueltig_bis=general_information.gueltig_bis,
            versionshinweis=general_information.versionshinweis,
            stichwort=general_information.stichwort,
        )


class ImmutableStrukturElement(BaseModel):
    anzahl: Anzahl
    bezug: list[Rechtsbezug]
    element_type: ElementType
    enthaelt: Identifier

    @staticmethod
    def from_element_reference(element_reference: ElementReference):
        return ImmutableStrukturElement(
            anzahl=element_reference.anzahl,
            bezug=element_reference.bezug,
            element_type=element_reference.element_type,
            enthaelt=element_reference.identifier,
        )

    def to_element_reference(self):
        return ElementReference(
            anzahl=self.anzahl,
            bezug=self.bezug,
            element_type=self.element_type,
            identifier=self.enthaelt,
        )


class ImmutableRegel(BaseModel):
    """Immutable xdf3 Regel

    Explicitly mutable attribute: `letzteAenderung`.
    """

    identifier: Identifier
    name: str
    beschreibung: str | None
    freitext_regel: str | None
    bezug: list[Rechtsbezug]
    stichwort: list[Stichwort]
    fachlicher_ersteller: str | None
    typ: Regeltyp
    param: list[RegelParameter]
    ziel: list[RegelZiel]
    skript: str | None
    fehler: list[Fehlermeldung]

    @staticmethod
    def from_regel(rule: Regel):
        return ImmutableRegel(
            identifier=rule.identifier,
            name=rule.name,
            beschreibung=rule.beschreibung,
            freitext_regel=rule.freitext_regel,
            bezug=rule.bezug,
            stichwort=rule.stichwort,
            fachlicher_ersteller=rule.fachlicher_ersteller,
            typ=rule.typ,
            param=rule.param,
            ziel=rule.ziel,
            skript=rule.skript,
            fehler=rule.fehler,
        )


class ImmutableSchema(ImmutableAllgemeineAngaben):
    """Immutable xdf3 Stammdatenschema

    Explicitly mutable attribute: `dokumentsteckbrief`.
    """

    bezeichnung: din91379.StringLatin
    hilfetext: din91379.StringLatin | None
    ableitungsmodifikationen_struktur: AbleitungsmodifikationenStruktur
    ableitungsmodifikationen_repraesentation: AbleitungsmodifikationenRepraesentation
    regeln: list[Identifier]
    struktur: list[ImmutableStrukturElement]

    @staticmethod
    def from_schema(
        schema: Schema,
    ):
        return ImmutableSchema(
            **dict(ImmutableAllgemeineAngaben.from_allgemeine_angaben(schema)),
            bezeichnung=schema.bezeichnung,
            hilfetext=schema.hilfetext,
            ableitungsmodifikationen_struktur=schema.ableitungsmodifikationen_struktur,
            ableitungsmodifikationen_repraesentation=schema.ableitungsmodifikationen_repraesentation,
            regeln=schema.regeln,
            struktur=[
                ImmutableStrukturElement.from_element_reference(ref)
                for ref in schema.struktur
            ],
        )


class ImmutableElement(ImmutableAllgemeineAngaben):
    bezeichnung_eingabe: din91379.StringLatin
    bezeichnung_ausgabe: din91379.StringLatin | None
    schema_element_art: SchemaElementArt
    hilfetext_eingabe: din91379.StringLatin | None
    hilfetext_ausgabe: din91379.StringLatin | None

    @staticmethod
    def from_element(element: Element):
        return ImmutableElement(
            **dict(ImmutableAllgemeineAngaben.from_allgemeine_angaben(element)),
            bezeichnung_eingabe=element.bezeichnung_eingabe,
            bezeichnung_ausgabe=element.bezeichnung_ausgabe,
            schema_element_art=element.schema_element_art,
            hilfetext_eingabe=element.hilfetext_eingabe,
            hilfetext_ausgabe=element.hilfetext_ausgabe,
        )


class ImmutableGruppe(ImmutableElement):
    art: Gruppenart | None
    regeln: list[Identifier]
    struktur: list[ImmutableStrukturElement]

    @staticmethod
    def from_gruppe(
        group: Gruppe,
    ):
        return ImmutableGruppe(
            **dict(ImmutableElement.from_element(group)),
            art=group.art,
            regeln=group.regeln,
            struktur=[
                ImmutableStrukturElement.from_element_reference(ref)
                for ref in group.struktur
            ],
        )


class ImmutableFeld(ImmutableElement):
    feldart: Feldart
    datentyp: Datentyp
    praezisierung: Praezisierung | None
    inhalt: din91379.StringLatin | None
    vorbefuellung: Vorbefuellung
    werte: list[ListenWert] | None
    codeliste_referenz: genericode.Identifier | None
    code_key: str | None
    name_key: str | None
    help_key: str | None
    regeln: list[Identifier]
    max_size: int | None
    media_type: list[str]

    @staticmethod
    def from_datenfeld(
        field: Datenfeld,
    ):
        return ImmutableFeld(
            **dict(ImmutableElement.from_element(field)),
            feldart=field.feldart,
            datentyp=field.datentyp,
            praezisierung=field.praezisierung,
            inhalt=field.inhalt,
            vorbefuellung=field.vorbefuellung,
            werte=field.werte,
            codeliste_referenz=field.codeliste_referenz,
            code_key=field.code_key,
            name_key=field.name_key,
            help_key=field.help_key,
            regeln=field.regeln,
            max_size=field.max_size,
            media_type=field.media_type,
        )


class ImmutableSteckbrief(ImmutableAllgemeineAngaben):
    ist_abstrakt: bool
    dokumentart: Dokumentart
    bezeichnung: din91379.StringLatin
    hilfetext: din91379.StringLatin | None

    @staticmethod
    def from_steckbrief(steckbrief: Steckbrief):
        return ImmutableSteckbrief(
            **dict(ImmutableAllgemeineAngaben.from_allgemeine_angaben(steckbrief)),
            ist_abstrakt=steckbrief.ist_abstrakt,
            dokumentart=steckbrief.dokumentart,
            bezeichnung=steckbrief.bezeichnung,
            hilfetext=steckbrief.hilfetext,
        )
