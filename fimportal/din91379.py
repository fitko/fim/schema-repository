from typing import NewType


StringLatin = NewType("StringLatin", str)


def parse_string_latin(value: str) -> StringLatin:
    """
    This should at some point check, whether the string is actually a valid DIN 91379 Type C string.
    This is currently done implicitly by the xdf3 XSD validation.

    As soon as xdf3 documents can be created by other means than being imported via XML (and there also checked by the XSD),
    this should programmatically check for valid string types.
    """
    return StringLatin(value)


def parse_optional_string_latin(value: str | None) -> StringLatin | None:
    return parse_string_latin(value) if value is not None else None
