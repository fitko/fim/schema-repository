import re
from datetime import date, datetime, timezone
from typing import Any, Callable, Literal, ParamSpec, Protocol, TypeVar

import ciso8601
import lxml.html
from pydantic import BaseModel
from starlette.requests import Request
from starlette.responses import HTMLResponse

from fimportal.errors import InvalidFimIdException

ActiveTab = Literal[
    "ueber-fim",
    "schulung",
    "arbeitsmittel",
    "kataloge",
    "news",
    "kontakt",
    "bausteinsuche",
]


class RenderFunction(Protocol):
    def __call__(
        self,
        request: Request,
        name: str,
        context: dict[str, Any],
        active_tab: ActiveTab | None = None,
        status_code: int = 200,
    ) -> HTMLResponse: ...


def get_latest_version(versions: list[str]) -> str:
    """
    Get a list of semver versions and return the latest version.
    """

    return sort_versions(versions)[-1]


def sort_versions(versions: list[str]) -> list[str]:
    """
    Sort a list of semver versions into ascending order.
    """

    versions.sort(key=lambda version: [int(x) for x in version.split(".")])

    return versions


def optional_date_to_string(date: date | None) -> str | None:
    return date.isoformat() if date is not None else None


def format_iso8601(value: str | datetime) -> str:
    """
    Pydantic formats iso8601 datetimes with a trailing "Z" for UTC timestamps.
    The python default function `datetime.isoformat` however uses `+00:00` instead.
    Both variants are valid.

    This helper function forces the "Z" variant to simplify testing api results, where
    the strings must be equal.
    """
    if isinstance(value, str):
        value = ciso8601.parse_datetime(value)

    return value.isoformat().replace("+00:00", "Z")


def utc_now() -> datetime:
    """
    Return the current utc datetime.

    The default `datetime.now()` does not include timezone information, and is therefore
    ambiguous. This helper function should be used everywhere when the current timestamp
    is needed to reduce the risk of creating datetimes without timezone information.
    """
    return datetime.now(timezone.utc)


def collect_element_identifiers_from_json_keys(
    data: Any,
) -> tuple[list[tuple[str, str]], list[tuple[str, str]]]:
    """
    Collect all keys from the input data and parse them into a list of fim ids and versions.
    This does not validate that the fim_id or fim_version are correctly formatted.
    If no version is supplied, use "latest" instead
    """
    group_identifiers: set[tuple[str, str]] = set()
    field_identifiers: set[tuple[str, str]] = set()

    values: list[Any] = [data]
    while len(values):
        value = values.pop()

        if isinstance(value, dict):
            for key, child_value in value.items():
                values.append(child_value)

                element_type, fim_id, fim_version = _parse_identifier(key)
                if element_type == "group":
                    group_identifiers.add((fim_id, fim_version))
                else:
                    field_identifiers.add((fim_id, fim_version))
        elif isinstance(value, list):
            values.extend(value)
        else:
            pass

    return sorted(group_identifiers), sorted(field_identifiers)


def _parse_identifier(key: str) -> tuple[Literal["group", "field"], str, str]:
    parts = key.split("V", maxsplit=1)
    if len(parts) == 1:
        fim_id = parts[0]
        fim_version = "latest"
    else:
        fim_id, fim_version = parts

    if fim_id.startswith("G"):
        return "group", fim_id, fim_version
    elif fim_id.startswith("F"):
        return "field", fim_id, fim_version
    else:
        raise InvalidFimIdException(f"The key '{key}' is not a valid FIM identifier")


def remove_html_tags(content: str) -> str:
    node = lxml.html.fragment_fromstring(content, create_parent="div")
    return node.text_content().strip()


def extract_xzufi_rechtsgrundlagen_lines(content: str) -> list[str]:
    content = remove_html_tags(content)

    lines = []
    for line in content.splitlines():
        line = line.strip()
        if len(line) > 0:
            lines.append(line)

    return lines


TSQUERY_SPECIAL_CHARACTERS = r"[\\&|!():*<']"
# Single quote (apostrophe) is also a special
# character but
# a) needs to be escaped differently (' -> '', instead of ' -> \')
# b) is escaped by asyncpg parameter input


def ts_query_param(fts_term: str) -> str | None:
    """
    Escapes all special characters of the string, splits it in words and makes it a prefix search with :*.

    The split into words takes quotation marks into account. Normally, words are combined
    via &, matching anything that includes all search terms.
    Using a " starts matching with <-> (followed by) and disables prefix search
    for those words to more closely match behaviour from popular search engines.

    Notes on special characters:

    Most special characters will be forgotten by to_tsquery, whether they are escaped
    or not. But some hold special meaning.

    - https://www.postgresql.org/docs/current/textsearch-controls.html#TEXTSEARCH-PARSING-QUERIES
    - https://www.postgresql.org/docs/current/datatype-textsearch.html#DATATYPE-TSQUERY
    - https://www.postgresql.org/docs/current/datatype-textsearch.html#DATATYPE-TSVECTOR

    Operators between so-called lexemes are:

    & (AND), | (OR), ! (NOT), <-> (FOLLOWED BY) and <N> (FOLLOWED BY) where N is an
    integer constant.

    Parentheses () can be used for grouping. A colon : after a lexeme starts a label.
    tsquery can be labeled with an asterisk * . (Example: 'super':*)

    Single quotes and white space determine where lexemes start and end. A backslash
    escapes the following character. Quotes and backslashes must be doubled in order
    to be escaped.

    Examples can be seen in the tests:
    tests/test_helpers.py::test_should_correctly_create_ts_query_parameters
    tests/test_helpers.py::test_should_create_correct_ts_query
    """

    fts_term = fts_term.strip()
    if len(fts_term) == 0:
        return None

    # Each " starts the followed-by mode, which is closed by the next ".
    # So after splitting into parts between ", each even part is in AND mode and
    # each  odd part is in FOLLOWED BY mode.
    parts = fts_term.split('"')
    result_parts = []
    for index, part in enumerate(parts):
        part = part.strip()
        if len(part) == 0:
            continue

        is_in_and_mode = index % 2 == 0

        # r"\\\g<0>" is specific to pythons re module. \\ adds a single \ before the matched term \g<0>
        part = re.sub(TSQUERY_SPECIAL_CHARACTERS, r"\\\g<0>", part)

        if is_in_and_mode:
            result_parts.append(" & ".join([f"{word}:*" for word in part.split()]))
        else:
            result_part = " <-> ".join(part.split())
            # Put result in paranthesis to make operator order explicit.
            result_parts.append(f"({result_part})")

    if len(result_parts) == 0:
        return None

    return " & ".join(result_parts)


def generate_json(object: BaseModel) -> dict[str, Any]:
    # NOTE: mode="json" serializes
    # date and datetime instances. While orjson can handle datetime and date, it
    # cannot deal with freezegun's FakeDate which we use in our tests.
    return object.model_dump(mode="json")


def remove_whitespace(items: list[str]) -> list[str]:
    out = []
    for item in items:
        item = item.strip()
        if item:
            out.append(item)

    return out


Param = ParamSpec("Param")
RetType = TypeVar("RetType")


def retry(
    max_retries: int = 5,
    exception: type[Exception] = Exception,
) -> Callable[[Callable[Param, RetType]], Callable[Param, RetType]]:
    """
    A decorator to retry a function if it raises an exception.
    """

    def decorator(inner: Callable[Param, RetType]) -> Callable[Param, RetType]:
        def wrapper(*args: Param.args, **kwargs: Param.kwargs) -> RetType:
            retries = max_retries
            while True:
                try:
                    return inner(*args, **kwargs)
                except exception:
                    retries -= 1
                    if retries == 0:
                        raise

        return wrapper

    return decorator
