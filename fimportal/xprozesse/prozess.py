from dataclasses import dataclass
from datetime import datetime
import re

import lxml.etree as ET
import logging
from typing import Annotated, Any
import zipfile
import base64
from io import BytesIO
from enum import Enum

from fimportal import xmlstruct, xml
from fimportal.helpers import utc_now
from fimportal.xmlstruct import (
    Value as XmlValue,
)
from fimportal.common import (
    Bundesland,
    FreigabeStatus,
    VerwaltungspolitischeKodierung,
    create_code_encoding,
)
from fimportal.xzufi.common import Zeitraum

"""
The parser follows the specification that can be downloaded with this link.
https://www.xrepository.de/api/xrepository/urn:xoev-de:mv:em:standard:xprozess_2.0:dokument:XProzess_Spezifikation_2.0_

However it can happen that breaking changes are introduced without the release of a new version. Such changes are documented in the `baukasten`.
The latest version can be downloaded as part of a zip following this link.
https://www.xrepository.de/api/xrepository/urn:xoev-de:mv:em:standard:xprozess_2.0:dokument:Handlungsanweisung_zu_XProzess_2.0.0
"""


class XProzessException(Exception):
    pass


class CustomListEncoding(xmlstruct.ListEncoding[xmlstruct.T]):
    def __init__(self, inner_encoding: xmlstruct.OptionalValueEncoding[xmlstruct.T]):
        super().__init__(inner_encoding)

    def parse(
        self, current_value: list[xmlstruct.T], node: xml.XmlElement
    ) -> list[xmlstruct.T]:
        inner_value: Any = self._inner_encoding.create_empty_value()
        inner_value = self._inner_encoding.parse(inner_value, node)
        if inner_value is not None:
            current_value.append(inner_value)

        return current_value


class ModellierungsmethodeType(Enum):
    BPMN = "1"
    BPMN_SILVER = "2"
    CMMN = "3"
    DFD = "4"
    E3_VALUE = "5"
    ECH = "6"
    EEPK = "7"
    EPK = "8"
    FAMOS = "9"
    FIM = "10"
    FLEXNETA = "11"
    GENO_BPMN = "12"
    HIPO = "13"
    IBO = "14"
    KMDL = "15"
    PAP = "16"
    PETRI = "17"
    PICTURE_BPMN = "18"
    PICTURE_CLASSIC = "19"
    SCOR = "20"
    UML = "21"
    WKD = "22"
    SONSTIGE = "999"


@dataclass(slots=True)
class Modellierungsmethode:
    code: Annotated[
        ModellierungsmethodeType, create_code_encoding(ModellierungsmethodeType)
    ]
    freitext: str | None


class MimeType(Enum):
    """
    See: https://www.xrepository.de/details/urn:xoev-de:xprozess:codeliste:mimetype
    Codelist not listed specifically in standard.
    """

    PDF = "1"
    XML_GENERIC = "2"
    XML_XPROZESS = "2a"
    XML_BPMNDI = "2b"
    XML_ADONIS = "2c"
    PNG = "3"
    SVG_XML = "4"
    MSWORD = "5a"
    DOCX = "5b"
    MSEXCEL = "6a"
    XLSX = "6b"
    HTML = "7"
    OCTET_STREAM = "999"
    PLAIN = "999a"


@dataclass(slots=True)
class Datei:
    dateiname: str
    modellierungsmethode: Modellierungsmethode
    mime_type: Annotated[MimeType, create_code_encoding(MimeType)]
    inhalt: str


@dataclass(slots=True)
class ProzessModell:
    prozessmodelldatei: list[Datei]
    visualisierungsdatei: list[Datei]
    beschreibungsdatei: list[Datei]


@dataclass(slots=True)
class AusloeserErgebnis:
    formular_id: Annotated[str | None, XmlValue("formularID")]
    prozess_id: Annotated[str | None, XmlValue("prozessID")]
    textuelle_beschreibung: str | None


class Prozessrolle(Enum):
    """
    See: https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:prozessrolle
    """

    INITIATOR = "1"
    HAUPTAKTEUR = "2"
    MITWIRKENDER = "3"
    ERGEBENIS_EMPFAENGER = "4"


@dataclass(slots=True)
class Prozessteilnehmer:
    name: str
    rolle: list[Annotated[Prozessrolle, create_code_encoding(Prozessrolle)]]


class Handlungsgrundlagenart(Enum):
    """
    See: https://www.xrepository.de/details/urn:xoev-de:mv:em:codeliste:xprozess:handlungsgrundlagenart
    Codelist not listed specifically in standard.
    """

    EU_BESCHLUSS = "101"
    EU_VERORDNUNG = "103"
    GESETZ = "104"
    EU_RICHTLINIE_UMSETZUNG_NATIONALES_RECHT = "105"
    RECHTSVERORDNUNG = "111"
    SATZUNG = "112"
    VERWALTUNGSVORSCHRIFT = "113"
    GESCHAEFTSORDNUNG = "114"
    BESCHLUSS = "115"
    STANDARDNORM = "121"
    VERWALTUNGSAKT = "201"
    ALLGEMEINVERFUEGUNG = "202"
    RECHTSAUFSICHTLICHE_WEISUNG = "203"
    FACHAUFSICHTLICHE_WEISUNG = "204"
    INNERDIENSTLICHE_WEISUNG = "205"
    VERWALTUNGSRECHTLICHE_WILLENSKLAERUNG = "210"
    VERTRAG_OEFFENTLICH_RECHTLICH = "220"
    VERTRAG_PRIVATRECHTLICH = "420"
    RICHTERRECHT = "800"
    SONSTIGE = "999"

    def to_label(self) -> str:
        return HANDLUNGSGRUNDLAGENART_TO_STRING[self]


STRING_TO_HANDLUNGSGRUNDLAGENART: dict[str, Handlungsgrundlagenart] = {
    "EU-Beschluss": Handlungsgrundlagenart.EU_BESCHLUSS,
    "EU-Verordnung": Handlungsgrundlagenart.EU_VERORDNUNG,
    "Gesetz": Handlungsgrundlagenart.GESETZ,
    "EU-Richtlinie Umsetzung Nationales Recht": Handlungsgrundlagenart.EU_RICHTLINIE_UMSETZUNG_NATIONALES_RECHT,
    "Rechtsverordnung": Handlungsgrundlagenart.RECHTSVERORDNUNG,
    "Satzung": Handlungsgrundlagenart.SATZUNG,
    "Verwaltungsvorschrift": Handlungsgrundlagenart.VERWALTUNGSVORSCHRIFT,
    "Geschaeftsordnung": Handlungsgrundlagenart.GESCHAEFTSORDNUNG,
    "Beschluss": Handlungsgrundlagenart.BESCHLUSS,
    "Standardnorm": Handlungsgrundlagenart.STANDARDNORM,
    "Verwaltungsakt": Handlungsgrundlagenart.VERWALTUNGSAKT,
    "Allgemeinverfuegung": Handlungsgrundlagenart.ALLGEMEINVERFUEGUNG,
    "Rechtsaufsichtliche Weisung": Handlungsgrundlagenart.RECHTSAUFSICHTLICHE_WEISUNG,
    "Fachaufsichtliche Weisung": Handlungsgrundlagenart.FACHAUFSICHTLICHE_WEISUNG,
    "Innerdienstliche Weisung": Handlungsgrundlagenart.INNERDIENSTLICHE_WEISUNG,
    "Verwaltungsrechtliche Willensklaerung": Handlungsgrundlagenart.VERWALTUNGSRECHTLICHE_WILLENSKLAERUNG,
    "Vertrag Oeffentlich Rechtlich": Handlungsgrundlagenart.VERTRAG_OEFFENTLICH_RECHTLICH,
    "Vertrag Privatrechtlich": Handlungsgrundlagenart.VERTRAG_PRIVATRECHTLICH,
    "Richterrecht": Handlungsgrundlagenart.RICHTERRECHT,
    "Sonstige": Handlungsgrundlagenart.SONSTIGE,
}

HANDLUNGSGRUNDLAGENART_TO_STRING: dict[Handlungsgrundlagenart, str] = {
    art: label for label, art in STRING_TO_HANDLUNGSGRUNDLAGENART.items()
}


@dataclass(slots=True)
class Zustandsangaben:
    erstellungszeitpunkt: datetime | None
    letzter_aenderungszeitpunkt: datetime | None
    letzter_bearbeiter: str | None
    status: Annotated[FreigabeStatus, create_code_encoding(FreigabeStatus)] | None
    anmerkung_letzte_aenderung: str | None
    gueltigkeitszeitraum: Zeitraum | None
    fachlicher_freigabezeitpunkt: datetime | None
    formeller_freigabezeitpunkt: datetime | None


@dataclass(slots=True)
class Handlungsgrundlage:
    name: str
    art: Annotated[Handlungsgrundlagenart, create_code_encoding(Handlungsgrundlagenart)]
    uri: str | None
    gueltigkeitszeitraum: Zeitraum | None


class Detaillierungsstufe(Enum):
    """
    See:
    https://www.xrepository.de/details/urn:xoev-de:xprozess:codeliste:detaillierungsstufe_2022-03-15#version
    and
    https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:detaillierungsstufe
    """

    STAMMINFORMATIONEN = "101"
    REFERENZINFORMATIONEN = "102"
    OZG_REFERENZINFORMATIONEN = "103"
    LOKALINFORMATIONEN = "104"
    MUSTERINFORMATIONEN = "105"

    @staticmethod
    def from_label(value: str) -> "Detaillierungsstufe":
        return STRING_TO_DETAILLIERUNGSSTUFE[value]

    def to_label(self) -> str:
        return DETAILLIERUNGSSTUFE_TO_STRING[self]


STRING_TO_DETAILLIERUNGSSTUFE: dict[str, Detaillierungsstufe] = {
    "Stamminformationen": Detaillierungsstufe.STAMMINFORMATIONEN,
    "Referenzinformationen": Detaillierungsstufe.REFERENZINFORMATIONEN,
    "OzgReferenzinformationen": Detaillierungsstufe.OZG_REFERENZINFORMATIONEN,
    "Lokalinformationen": Detaillierungsstufe.LOKALINFORMATIONEN,
    "Musterinformationen": Detaillierungsstufe.MUSTERINFORMATIONEN,
}

DETAILLIERUNGSSTUFE_TO_STRING: dict[Detaillierungsstufe, str] = {
    stufe: label for label, stufe in STRING_TO_DETAILLIERUNGSSTUFE.items()
}


class Datentyp(Enum):
    ANY_URI = "001"
    BASE64_BINARY = "002"
    BOOLEAN = "003"
    BYTE = "004"
    DATE = "005"
    DATE_TIME = "006"
    DECIMAL = "007"
    DOUBLE = "008"
    DURATION = "009"
    ENTITIES = "010"
    ENTITY = "011"
    FLOAT = "012"
    G_DAY = "013"
    G_MONTH = "014"
    G_MONTH_DAY = "015"
    G_YEAR = "016"
    G_YEAR_MONTH = "017"
    HEX_BINARY = "018"
    ID = "019"
    IDREF = "020"
    IDREFS = "021"
    INT = "022"
    INTEGER = "023"
    LANGUAGE = "024"
    LONG = "025"
    NAME = "026"
    NCNAME = "027"
    NEGATIVE_INTEGER = "028"
    NMTOKEN = "029"
    NMTOKENS = "030"
    NON_NEGATIVE_INTEGER = "031"
    NON_POSITIVE_INTEGER = "032"
    NORMALIZED_STRING = "033"
    NOTATION = "034"
    POSITIVE_INTEGER = "035"
    QNAME = "036"
    SHORT = "037"
    STRING = "038"
    TIME = "039"
    TOKEN = "040"
    UNSIGNED_BYTE = "041"
    UNSIGNED_INT = "042"
    UNSIGNED_LONG = "043"
    UNSIGNED_SHORT = "044"
    ANY_SIMPLE_TYPE = "045"


@dataclass(slots=True)
class Merkmal:
    ordnungsrahmen_name: str | None
    ordnungsrahmen_version: str | None
    merkmal_id: Annotated[str | None, XmlValue("merkmalID")]
    merkmal_name: str
    merkmal_datentyp: Annotated[Datentyp | None, create_code_encoding(Datentyp)]
    merkmal_wert: list[str]


class DokumentsteckbriefRolle(Enum):
    AUSLOESER = "ausloeser"
    ERGEBNIS = "ergebnis"
    EINGEHENDE_DATEN = "eingehende daten"
    AUSGEHENDE_DATEN = "ausgehenden daten"


@dataclass(slots=True)
class ProzessSteckbrief:
    definition: str | None
    beschreibung: str | None
    ausloeser: list[AusloeserErgebnis]
    ergebnis: list[AusloeserErgebnis]
    prozessteilnehmer: list[Prozessteilnehmer]
    handlungsgrundlage: list[Handlungsgrundlage]
    detaillierungsstufe: (
        Annotated[
            Detaillierungsstufe,
            create_code_encoding(Detaillierungsstufe),
        ]
        | None
    )
    verwaltungspolitische_kodierung: list[VerwaltungspolitischeKodierung]
    zielvorgaben: str | None
    merkmal: list[Merkmal]

    def get_prozessteilnehmer_typ(self, rolle: Prozessrolle) -> list[str]:
        return [
            teilnehmer.name
            for teilnehmer in self.prozessteilnehmer
            if rolle in teilnehmer.rolle
        ]

    def get_dokumentsteckbrief_references(
        self,
    ) -> list[tuple[str, DokumentsteckbriefRolle]]:
        return [
            *[
                (el.formular_id, DokumentsteckbriefRolle.AUSLOESER)
                for el in self.ausloeser
                if el.formular_id is not None
            ],
            *[
                (el.formular_id, DokumentsteckbriefRolle.ERGEBNIS)
                for el in self.ergebnis
                if el.formular_id is not None
            ],
        ]

    @property
    def ausloeser_steckbrief_ids(self) -> list[str]:
        return [el.formular_id for el in self.ausloeser if el.formular_id is not None]

    @property
    def ergebnis_steckbrief_ids(self):
        return [el.formular_id for el in self.ergebnis if el.formular_id is not None]


@dataclass(slots=True)
class Klassifikation:
    ordnungsrahmen_name: str
    ordnungsrahmen_version: str | None
    klasse_id: Annotated[str, XmlValue("klasseID")]
    klasse_name: str | None


class ReferenzaktivitaetengruppeType(Enum):
    INFORMATION_EMPFAHNGEN = "1"
    INFORMATION_BEREITSTELLEN = "2"
    SACHVERHALT_FORMELL_PRUEFEN = "3"
    SACHVERHALT_BEURTEILEN_ENTSCHEIDEN_OHNE_SPIELRAUM = "4"
    SACHVERHALT_BEURTEILEN_ENTSCHEIDEN_MIT_SPIELRAUM = "5"
    DATEN_ZUM_SACHVERHALT_BEARBEITEN = "6"
    BETEILIGUNG_DURCHFUEHREN = "7"
    SONSTIGE_AKTIVITAET_DURCHFUEHREN = "8"


@dataclass(slots=True)
class Formularverweis:
    formularsteckbrief_id: Annotated[str, XmlValue("formularsteckbriefID")]
    stammformular_id: Annotated[str | None, XmlValue("stammformularID")]
    stammformular_version: str | None
    element_id: Annotated[list[str], XmlValue("elementID")]


@dataclass(slots=True)
class Daten:
    # According to specification both properties are mandatory.
    # According to the patch document exactly one of them is set.
    formularverweis: Formularverweis | None
    textuelle_beschreibung: str | None


class Uebermittlungsart(Enum):
    PAPIERBASIERT_PERSOENLICH = "1"
    PAPIERBASIERT_POSTALISCH = "2"
    ELEKTRONISCH_HALBAUTOMATISCH = "3"
    ELEKTRONISCH_VOLLAUTOMATISCH = "4"
    MUENDLICH_PERSOENLICH = "5"
    MUENDLICH_TELEFONISCH = "6"
    KEINE_VORGABE = "99"


@dataclass(slots=True)
class DatenEmpfangen:
    formularverweis: Daten
    uebermittlungsart: Annotated[
        Uebermittlungsart, create_code_encoding(Uebermittlungsart)
    ]
    sender: str


@dataclass(slots=True)
class SpezifischeAttribute1:
    empfangene_daten: list[DatenEmpfangen]


@dataclass(slots=True)
class DatenBereitgestellt:
    formularverweis: Daten
    uebermittlungsart: Annotated[
        Uebermittlungsart, create_code_encoding(Uebermittlungsart)
    ]
    empfaenger: str


@dataclass(slots=True)
class SpezifischeAttribute2:
    bereitgestellte_daten: list[DatenBereitgestellt]


class FormellePruefung(Enum):
    SACHLICHE_ZUSTAENDIGKEIT = "1"
    OERTLICHE_ZUSTAENDIGKEIT = "2"
    INSTANZIELLE_ZUSTAENDIGKEIT = "3"
    VERFAHREN = "4"
    FORM = "5"


@dataclass(slots=True)
class SpezifischeAttribute3:
    formelle_pruefung: list[
        Annotated[FormellePruefung, create_code_encoding(FormellePruefung)]
    ]


@dataclass(slots=True)
class SpezifischeAttribute4:
    hilfsmittel: str


class Entscheidungsart(Enum):
    AUSWAHLERMESSEN = "1"
    ENTSCHLIESSUNGSERMESSEN = "2"
    BEURTEILUNGSSPIELRAUM = "3"


@dataclass(slots=True)
class SpezifischeAttribute5:
    hilfsmittel: str
    entscheidungsart: Annotated[
        Entscheidungsart, create_code_encoding(Entscheidungsart)
    ]


class Bearbeitungsart(Enum):
    KEIN_EINTRAG = "0"
    ERSTELLUNG = "1"
    AKTUALISIERUNG = "2"
    LOESCHUNG = "3"
    ARCHIVIERUNG = "4"
    AUSSONDERUNG = "5"


@dataclass(slots=True)
class SpezifischeAttribute6:
    bearbeitungsart: Annotated[Bearbeitungsart, create_code_encoding(Bearbeitungsart)]


class Beteiligungsform(Enum):
    AMTSHILFE = "1"
    ANHOERUNG = "2"
    AUSKUNFT = "3"
    BENEMEN = "4"
    EINVERNEHMEN_OHNE_AUSSENWIRKUNG = "5"
    STELLUNGNAHME_GUTACHTEN = "6"
    VERNEHMUNG = "7"
    EINSICHTNAHME = "8"
    AUFTRAG = "9"
    SONSTIGE_BETEILIGUNGSFORM = "99"


@dataclass(slots=True)
class SpezifischeAttribute7:
    bereitgestellte_daten: list[DatenBereitgestellt]
    empfangene_daten: list[DatenEmpfangen]
    mitwirkungspflicht: bool
    beteiligungsform: Annotated[
        Beteiligungsform, create_code_encoding(Beteiligungsform)
    ]


@dataclass(slots=True)
class SpezifischeAttribute8:
    beschreibung: str


@dataclass(slots=True)
class SpezifischeAttribute:
    spezifische_attribute_1: SpezifischeAttribute1 | None
    spezifische_attribute_2: SpezifischeAttribute2 | None
    spezifische_attribute_3: SpezifischeAttribute3 | None
    spezifische_attribute_4: SpezifischeAttribute4 | None
    spezifische_attribute_5: SpezifischeAttribute5 | None
    spezifische_attribute_6: SpezifischeAttribute6 | None
    spezifische_attribute_7: SpezifischeAttribute7 | None
    spezifische_attribute_8: SpezifischeAttribute8 | None


@dataclass(slots=True)
class Aktivitaetengruppe:
    referenzaktivitaetengruppe_typ: Annotated[
        ReferenzaktivitaetengruppeType,
        create_code_encoding(ReferenzaktivitaetengruppeType),
    ]
    referenzaktivitaetengruppe_version: str
    id: str
    name: str
    sub_process_id: str | None
    beschreibung: str | None
    handlungsgrundlage: list[Handlungsgrundlage]
    eigehende_daten: list[Daten]
    ausgehende_daten: list[Daten]
    fachverfahren: list[str]
    spezifische_attribute: SpezifischeAttribute


@dataclass(slots=True)
class StrukturbeschreibungFim:
    aktivitaetengruppe: list[Aktivitaetengruppe]


def _is_dokumentensteckbrief_id(id: str):
    dokumentensteckbrief_id_regex_xdf2 = r"D[0-9]{8}"
    dokumentensteckbrief_id_regex_xdf3 = r"D[0-9]{12}"
    return (
        re.fullmatch(dokumentensteckbrief_id_regex_xdf2, id)
        or re.fullmatch(dokumentensteckbrief_id_regex_xdf3, id) is not None
    )


@dataclass(slots=True)
class Strukturbeschreibung:
    modellierungsmethode: Modellierungsmethode
    strukturbeschreibung_fim: Annotated[
        StrukturbeschreibungFim, XmlValue("strukturbeschreibungFIM")
    ]

    def get_dokumentsteckbrief_references(self):
        references: list[tuple[str, DokumentsteckbriefRolle]] = []
        for gruppe in self.strukturbeschreibung_fim.aktivitaetengruppe:
            for datum in gruppe.eigehende_daten:
                if datum.formularverweis and _is_dokumentensteckbrief_id(
                    datum.formularverweis.formularsteckbrief_id
                ):
                    references.append(
                        (
                            datum.formularverweis.formularsteckbrief_id,
                            DokumentsteckbriefRolle.EINGEHENDE_DATEN,
                        )
                    )
            for datum in gruppe.ausgehende_daten:
                if datum.formularverweis and _is_dokumentensteckbrief_id(
                    datum.formularverweis.formularsteckbrief_id
                ):
                    references.append(
                        (
                            datum.formularverweis.formularsteckbrief_id,
                            DokumentsteckbriefRolle.AUSGEHENDE_DATEN,
                        )
                    )
        return references


@dataclass(slots=True)
class Prozess:
    id: str
    version: str | None
    name: str
    bezeichnung: str | None
    fachlich_freigebende_stelle: str | None
    klassifikation: list[Klassifikation]
    schlagwort: Annotated[
        list[str], CustomListEncoding(xmlstruct.OptionalStringEncoding)
    ]
    prozesssteckbrief: ProzessSteckbrief | None
    prozessstrukturbeschreibung: Strukturbeschreibung | None
    prozessmodell: ProzessModell | None
    zustandsangaben: Zustandsangaben | None

    def get_dokumentsteckbrief_references(
        self,
    ) -> list[tuple[str, DokumentsteckbriefRolle]]:
        references: list[tuple[str, DokumentsteckbriefRolle]] = []
        if self.prozesssteckbrief is not None:
            references.extend(
                self.prozesssteckbrief.get_dokumentsteckbrief_references()
            )

        if self.prozessstrukturbeschreibung is not None:
            references.extend(
                self.prozessstrukturbeschreibung.get_dokumentsteckbrief_references()
            )

        return references

    @property
    def freigabe_status(self) -> FreigabeStatus | None:
        return self.zustandsangaben.status if self.zustandsangaben is not None else None

    @property
    def detaillierungsstufe(self) -> Detaillierungsstufe | None:
        return (
            self.prozesssteckbrief.detaillierungsstufe
            if self.prozesssteckbrief is not None
            else None
        )

    @property
    def letzter_aenderungszeitpunkt(self) -> datetime | None:
        return (
            self.zustandsangaben.letzter_aenderungszeitpunkt
            if self.zustandsangaben is not None
            else None
        )

    @property
    def gueltig_ab(self) -> datetime | None:
        if (
            self.zustandsangaben is not None
            and self.zustandsangaben.gueltigkeitszeitraum is not None
        ):
            return self.zustandsangaben.gueltigkeitszeitraum.beginn

    @property
    def gueltig_bis(self) -> datetime | None:
        if (
            self.zustandsangaben is not None
            and self.zustandsangaben.gueltigkeitszeitraum is not None
        ):
            return self.zustandsangaben.gueltigkeitszeitraum.ende

    def get_verwaltungspolitische_kodierung(
        self,
    ) -> list[Bundesland]:
        if self.prozesssteckbrief is None:
            return []
        laender = [
            kodierung.bundesland
            for kodierung in self.prozesssteckbrief.verwaltungspolitische_kodierung
            if kodierung.bundesland is not None
        ]
        if len(laender) != len(set(laender)):
            raise XProzessException(f"Double Entries in Bundeslaender: {laender}")

        return laender

    def has_visualization_file(self) -> bool:
        if (
            self.prozessmodell is None
            or len(self.prozessmodell.visualisierungsdatei) == 0
        ):
            return False

        return True

    def has_report_file(self) -> bool:
        if (
            self.prozessmodell is None
            or len(self.prozessmodell.beschreibungsdatei) == 0
        ):
            return False

        return True

    def get_visualisierungsdatei(self) -> tuple[str, bytes] | None:
        if self.prozessmodell is None:
            return None

        if len(self.prozessmodell.visualisierungsdatei) == 0:
            return None

        # TODO: Handle multiple files
        file = self.prozessmodell.visualisierungsdatei[0]

        try:
            zip_data = BytesIO(base64.b64decode(file.inhalt))
            with zipfile.ZipFile(zip_data) as archive:
                filenames = archive.namelist()
                assert len(filenames) == 1
                filename = filenames[0]

                with archive.open(filename) as xml_file:
                    bpmn_content = xml_file.read()

            return file.dateiname, bpmn_content
        except Exception as ex:
            logger.error(f"Failed to retrieve bpmn file: {str(ex)}")

    def get_report_file(self) -> bytes | None:
        if self.prozessmodell is None:
            return None

        if len(self.prozessmodell.beschreibungsdatei) == 0:
            return None

        # TODO: Handle multiple files
        file = self.prozessmodell.beschreibungsdatei[0]

        try:
            zip_data = BytesIO(base64.b64decode(file.inhalt))
            with zipfile.ZipFile(zip_data) as archive:
                filenames = archive.namelist()
                assert len(filenames) == 1
                filename = filenames[0]

                with archive.open(filename) as xml_file:
                    content = xml_file.read()

            return content
        except Exception as ex:
            logger.error(f"Failed to retrieve report file: {str(ex)}")


@dataclass(slots=True)
class ProzessNachrichtenKopf:
    nachricht_uuid: Annotated[str, XmlValue("nachrichtUUID")]
    erstellungszeitpunkt: datetime
    autor: str | None
    leser: str | None


@dataclass(slots=True)
class Gliederungsebene:
    gliederungsebene_nummer: int
    gliedrungsebene_name: str | None


class Zwecksetzung(Enum):
    ABGABENVERWALTUNG = "1"
    BEDARFSVERWALTUNG = "2"
    GEWAEHRLEISTUNGSVERWALTUNG = "3"
    LEISTUNGSVERWALTUNG_DASEINSVORSORGE = "4"
    LENKUNGSVERWALTUNG = "5"
    ORDNUNGSVERWALTUNG = "6"
    SONSTIGE = "9"


class OperativesZiel(Enum):
    GENERELL_ABSTRAKTE_REGELUNG = "101"
    KONKRETE_REGELUNG_VERHALTEN_PERSON = "201"
    KONKRETE_REGELUNG_RECHTSVERHAELTNIS_FESTSTELLUNG = "202"
    KONKRETE_REGELUNG_RECHTSVERHAELTNIS_HANDLUNG = "203"
    KONKRETE_REGELUNG_RECHTSVERHAELTNIS_VERWALTUNG = "204"
    NORMKONKRETISIERENDE_REGELUNG = "205"
    NORMINTERPRETIERENDE_REGELUNG = "206"
    NORMVERTRENDE_REGELUNG = "207"
    ORGANINTERNE_INTERORGANE_REGELUNG = "208"
    OEFFENTLICH_RECHTLICHE_GELDFORDERUNG = "311"
    ERZWINGUNG_DULDUNG_UNTERLASSUNG_HANDLUNG = "312"
    GELDLEISTUNG = "313"
    SACH_DIENSTLEISTUNG = "314"
    KONKRETE_SACH_RECHSLAGE = "321"
    GEMEINWESEN = "401"
    ARBEIT_DER_VERWALTUNG = "501"
    RECHTMAESSIGKEIT_ARBEIT_DER_VERWALTUNG = "601"
    ZWECKMAESSIGKEIT_ARBEIT_DER_VERWALTUNG = "602"
    SONSTIGE = "999"

    def to_label(self) -> str:
        return OPERATIVES_ZIEL_TO_STRING[self]


STRING_TO_OPERATIVES_ZIEL: dict[str, OperativesZiel] = {
    "Herbeiführung einer generell-abstrakte Regelung von Sachverhalten": OperativesZiel.GENERELL_ABSTRAKTE_REGELUNG,
    "Herbeiführung einer konkreten Regelung eines Sachverhaltes, die eine bestimmte Person oder einen nach allgemeinen Merkmalen bestimmbaren Personenkreis zu einem bestimmten Verhalten (Tun, Dulden, Unterlassen) verpflichtet": OperativesZiel.KONKRETE_REGELUNG_VERHALTEN_PERSON,
    "Herbeiführung einer konkreten Regelung eines Sachverhaltes, die ein Rechtsverhältnis oder eine rechtlich erhebliche Eigenschaft einer bestimmten Person oder eines nach allgemeinen Merkmalen bestimmbaren Personenkreises verbindlich feststellt": OperativesZiel.KONKRETE_REGELUNG_RECHTSVERHAELTNIS_FESTSTELLUNG,
    "Herbeiführung einer konkreten Regelung eines Sachverhaltes, die ein Rechtsverhältnis oder eine rechtlich erhebliche Eigenschaft einer bestimmten Person oder eines nach allgemeinen Merkmalen bestimmbaren Personenkreises begründen, ändern oder aufheben": OperativesZiel.KONKRETE_REGELUNG_RECHTSVERHAELTNIS_HANDLUNG,
    "Herbeiführung einer konkreten Regelung eines Sachverhaltes, die ein Rechtsverhältnis zwischen rechtsfähigen Trägern der öffentlichen Verwaltung begründen, ändern oder aufheben": OperativesZiel.KONKRETE_REGELUNG_RECHTSVERHAELTNIS_VERWALTUNG,
    "Herbeiführung einer normkonkretisierenden Regelung": OperativesZiel.NORMKONKRETISIERENDE_REGELUNG,
    "Herbeiführung einer norminterpretierenden Regelung": OperativesZiel.NORMINTERPRETIERENDE_REGELUNG,
    "Herbeiführung einer normvertretenden Regelung": OperativesZiel.NORMVERTRENDE_REGELUNG,
    "Herbeiführung einer organinternen oder interorganen Regelung": OperativesZiel.ORGANINTERNE_INTERORGANE_REGELUNG,
    "Beitreibung einer öffentlich-rechtlichen Geldforderung": OperativesZiel.OEFFENTLICH_RECHTLICHE_GELDFORDERUNG,
    "Erzwingung, Duldung oder Unterlassung einer Handlung": OperativesZiel.ERZWINGUNG_DULDUNG_UNTERLASSUNG_HANDLUNG,
    "Erbringung einer Geldleistung": OperativesZiel.GELDLEISTUNG,
    "Erbringung einer Sach- oder Dienstleistung": OperativesZiel.SACH_DIENSTLEISTUNG,
    "Unterrichtung zu konkreter Sach- oder Rechtslage": OperativesZiel.KONKRETE_SACH_RECHSLAGE,
    "Daseinsvorsorge, Bereitstellung von Infrastrukturen und öffentlichen Einrichtungen, harmonische Gestaltung des Gemeinwesens": OperativesZiel.GEMEINWESEN,
    "Gewährleistung der Arbeit der Verwaltung": OperativesZiel.ARBEIT_DER_VERWALTUNG,
    "Gewährleistung der Rechtmäßigkeit der Arbeit der Verwaltung": OperativesZiel.RECHTMAESSIGKEIT_ARBEIT_DER_VERWALTUNG,
    "Gewährleistung der Zweckmäßigkeit der Arbeit der Verwaltung": OperativesZiel.ZWECKMAESSIGKEIT_ARBEIT_DER_VERWALTUNG,
    "Sonstige / weitere Zielstellung": OperativesZiel.SONSTIGE,
}

OPERATIVES_ZIEL_TO_STRING: dict[OperativesZiel, str] = {
    ziel: label for label, ziel in STRING_TO_OPERATIVES_ZIEL.items()
}


class Handlungsform(Enum):
    NORMERLASS_EU_BESCHLUSS = "101"
    NORMERLASS_EU_RICHTLINIE = "102"
    NORMERLASS_EU_VERORDNUNG = "103"
    NORMERLASS_GESETZ = "104"
    NORMERLASS_GESETZ_EU_RICHTLINIE = "105"
    NORMERLASS_RECHTSVERORDNUNG = "111"
    NORMERLASS_SATZUNG = "112"
    NORMERLASS_VERWALTUNGSVORSCHRIFT = "113"
    NORMERLASS_GESCHAEFTSORDNUNG = "114"
    NORMERLASS_BESCHLUSS = "115"
    ENTWICKLUNG_NORM_ODER_STANDARD = "121"
    VERWALTUNGSAKT = "201"
    VERWALTUNGSAKT_ALLGEMEINVERFUEGUNG = "202"
    VERWALTUNGSAKT_RECHTSAUFSICHTLICHE_WEISUNG = "203"
    VERWALTUNGSRECHTLICHE_WILLENSERKLAERUNG_FACHAUFSICHTLICHE_WEISUNG = "204"
    VERWALTUNGSRECHTLICHE_WILLENSERKLAERUNG_INNERDIENSTLICHE_WEISUNG = "205"
    VERWALTUNGSRECHTLICHE_WILLENSERKLAERUNG = "210"
    OEFFENTLICH_RECHTLICHER_VERTRAG_KOORDINATIONSRECHTLICHER_VERTRAG = "221"
    OEFFENTLICH_RECHTLICHER_VERTRAG_SUBORDINATIONSRECHTLICHER_VERTRAG = "222"
    REALAKT_ABGABE_WISSENSERKLAERUNG = "301"
    REALAKT_ABWICKLUNG_FEHLGESCHLAGENER_LEISTUNGSBEZIEHUNGEN = "302"
    REALAKT_PLAN = "303"
    REALAKT_VOLLSTRECKUNG_VERWALTUNGSAKT = "304"
    REALAKT_VOLLZUG_VERWALTUNGSAKT = "305"
    REALAKT_SCHLICHT_HOHEITLICHES_VERWALTUNGSHANDELN = "310"
    PRIVATRECHTLICHES_HILFSGESCHAEFT = "410"
    PRIVATRECHTLICHER_VERTRAG = "420"
    VERWALTUNGSPRIVATRECHTLICHES_HANDELN = "430"
    ERWERBSWIRTSCHAFTLICHES_HANDELN = "450"
    BESCHLUSS = "801"
    VERFUEGUNG = "802"
    URTEIL = "803"
    JUSTIZVERWALTUNGSAKT = "804"
    SONSTIGE_WEITERE_HANDLUNGSFORM = "999"

    def to_label(self) -> str:
        return HANDLUNGSFORM_TO_STRING[self]


STRING_TO_HANDLUNGSFORM: dict[str, Handlungsform] = {
    "Normerlass EU-Beschluss": Handlungsform.NORMERLASS_EU_BESCHLUSS,
    "Normerlass EU-Richtlinie": Handlungsform.NORMERLASS_EU_RICHTLINIE,
    "Normerlass EU-Verordnung": Handlungsform.NORMERLASS_EU_VERORDNUNG,
    "Normerlass Gesetz": Handlungsform.NORMERLASS_GESETZ,
    "Normerlass GesetzEuRichtlinie": Handlungsform.NORMERLASS_GESETZ_EU_RICHTLINIE,
    "Normerlass Rechtsverordnung": Handlungsform.NORMERLASS_RECHTSVERORDNUNG,
    "Normerlass Satzung": Handlungsform.NORMERLASS_SATZUNG,
    "Normerlass Verwaltungsvorschrift": Handlungsform.NORMERLASS_VERWALTUNGSVORSCHRIFT,
    "Normerlass Geschaeftsordnung": Handlungsform.NORMERLASS_GESCHAEFTSORDNUNG,
    "Normerlass Beschluss": Handlungsform.NORMERLASS_BESCHLUSS,
    "Entwicklung, Norm Oder Standard": Handlungsform.ENTWICKLUNG_NORM_ODER_STANDARD,
    "Verwaltungsakt": Handlungsform.VERWALTUNGSAKT,
    "Verwaltungsakt / Allgemeinverfuegung": Handlungsform.VERWALTUNGSAKT_ALLGEMEINVERFUEGUNG,
    "Verwaltungsakt / Rechtsaufsichtliche Weisung": Handlungsform.VERWALTUNGSAKT_RECHTSAUFSICHTLICHE_WEISUNG,
    "Verwaltungsrechtliche Willenserklaerung / FachaufsichtlicheWeisung": Handlungsform.VERWALTUNGSRECHTLICHE_WILLENSERKLAERUNG_FACHAUFSICHTLICHE_WEISUNG,
    "Verwaltungsrechtliche Willenserklaerung / InnerdienstlicheWeisung": Handlungsform.VERWALTUNGSRECHTLICHE_WILLENSERKLAERUNG_INNERDIENSTLICHE_WEISUNG,
    "Verwaltungsrechtliche Willenserklaerung": Handlungsform.VERWALTUNGSRECHTLICHE_WILLENSERKLAERUNG,
    "Oeffentlich-Rechtlicher Vertrag / Koordinationsrechtlicher Vertrag": Handlungsform.OEFFENTLICH_RECHTLICHER_VERTRAG_KOORDINATIONSRECHTLICHER_VERTRAG,
    "Oeffentlich-Rechtlicher Vertrag / Subordinationsrechtlicher Vertrag": Handlungsform.OEFFENTLICH_RECHTLICHER_VERTRAG_SUBORDINATIONSRECHTLICHER_VERTRAG,
    "Realakt / Abgabe Wissenserklaerung": Handlungsform.REALAKT_ABGABE_WISSENSERKLAERUNG,
    "Realakt / Abwicklung Fehlgeschlagener Leistungsbeziehungen": Handlungsform.REALAKT_ABWICKLUNG_FEHLGESCHLAGENER_LEISTUNGSBEZIEHUNGEN,
    "Realakt / Plan": Handlungsform.REALAKT_PLAN,
    "Realakt / Vollstreckung / Verwaltungsakt": Handlungsform.REALAKT_VOLLSTRECKUNG_VERWALTUNGSAKT,
    "Realakt / Vollzug / Verwaltungsakt": Handlungsform.REALAKT_VOLLZUG_VERWALTUNGSAKT,
    "Realakt / Schlicht Hoheitliches Verwaltungshandeln": Handlungsform.REALAKT_SCHLICHT_HOHEITLICHES_VERWALTUNGSHANDELN,
    "Privatrechtliches Hilfsgeschaeft": Handlungsform.PRIVATRECHTLICHES_HILFSGESCHAEFT,
    "Privatrechtlicher Vertrag": Handlungsform.PRIVATRECHTLICHER_VERTRAG,
    "Verwaltungsprivatrechtliches Handeln": Handlungsform.VERWALTUNGSPRIVATRECHTLICHES_HANDELN,
    "Erwerbswirtschaftliches Handeln": Handlungsform.ERWERBSWIRTSCHAFTLICHES_HANDELN,
    "Beschluss": Handlungsform.BESCHLUSS,
    "Verfuegung": Handlungsform.VERFUEGUNG,
    "Urteil": Handlungsform.URTEIL,
    "Justizverwaltungsakt": Handlungsform.JUSTIZVERWALTUNGSAKT,
    "SonstigeWeitereHandlungsform": Handlungsform.SONSTIGE_WEITERE_HANDLUNGSFORM,
}

HANDLUNGSFORM_TO_STRING: dict[Handlungsform, str] = {
    form: label for label, form in STRING_TO_HANDLUNGSFORM.items()
}


class Verfahrensart(Enum):
    GESETZGEBUNGSVERFAHREN_FORMELLES_RECHT = "101"
    GESETZGEBUNGSVERFAHREN_FORMELLES_UND_MATERIELLES_RECHT = "111"
    HAUSHALTSPLANUNG_HAUSHALTSPLANFESTSTELLUNG_FORMELLES_GESETZ = "121"
    NORMSETZUNGSVERFAHREN_OHNE_FORMELLICHE_GESETZE = "201"
    LANDESENTWICKLUNGSPLANFESTSETZUNG = "221"
    REGIONALENTWICKLUNGSPLANFESTSETZUNG = "222"
    BAULEITPLANVERFAHREN = "231"
    NICHT_FORMLICHES_VERWALTUNGSVERFAHREN_VWVFG_VWGO = "301"
    FORMAL_VERWALTUNGSVERFAHREN_VWVFG = "302"
    WIDERSPRUCHSVERFAHREN_VORVERFAHREN_VWGO = "303"
    WIDERSPRUCHSVERFAHREN_ABHILFE_VERFAHREN_VWGO = "304"
    VERWALTUNGSVERFAHREN_EINHEITLICHE_STELLE_VWVFG = "311"
    VERWALTUNGSVERFAHREN_GENEHMIGUNGSFIKTION_VWVFG = "312"
    PLANFESTSTELLUNGSVERFAHREN_VWVFG = "321"
    PLANFESTSTELLUNGSVERFAHREN_BUENDELUNG_VWVFG = "322"
    VERWALTUNGSGEBUEHREN_AUSLAGEN_FESTSETZUNG_VERFAHREN = "331"
    BENUTZUNGSGEBUEHREN_AUSLAGEN_FESTSETZUNG_VERFAHREN = "332"
    PRIVATRECHTLICHES_ENTGELT_FESTSETZUNG_VERFAHREN = "333"
    BEITRAG_FESTSETZUNG_VERFAHREN = "334"
    VOLLSTRECKUNGSVERFAHREN_VWVFG = "341"
    BUSSGELDVERFAHREN_VWVFG = "361"
    BESTEUERUNGSVERFAHREN_AO = "401"
    GESONDERTE_FESTSTELLUNG_VERFAHREN_AO = "421"
    STEUERMESSBETRAEGE_FESTSETZUNG_VERFAHREN_AO = "422"
    EINSRUCHSVERFAHREN_VORVERFAHREN_AO = "403"
    VOLLSTRECKUNGSVERFAHREN_AO = "441"
    STEUERSTRAFVERFAHREN_AO = "451"
    BUSSGELDVERFAHREN_AO = "461"
    SOZIALVERWALTUNGSVERFAHREN_SGB_X = "501"
    WIDERSPRUCHSVERFAHREN_VORVERFAHREN_SGB_X = "503"
    VOLLSTRECKUNGSVERFAHREN_SGB_X = "541"
    BUSSGELDVERFAHREN_SGB_X = "561"
    NORMENKONTROLLVERFAHREN_VWGO = "801"
    ANFECHTUNGSKLAGEVERFAHREN_VWGO = "831"
    VERPFLICHTUNGSKLAGEVERFAHREN_VWGO = "832"
    FESTSTELLUNGSKLAGEVERFAHREN_VWGO = "833"
    LEISTUNGSKLAGEVERFAHREN_VWGO = "834"
    ANFECHTUNGSKLAGEVERFAHREN_FGO = "841"
    VERPFLICHTUNGSKLAGEVERFAHREN_FGO = "842"
    FESTSTELLUNGSKLAGEVERFAHREN_FGO = "843"
    LEISTUNGSKLAGEVERFAHREN_FGO = "844"
    ANFECHTUNGSKLAGEVERFAHREN_SGG = "851"
    VERPFLICHTUNGSKLAGEVERFAHREN_SGG = "852"
    FESTSTELLUNGSKLAGEVERFAHREN_SGG = "853"
    LEISTUNGSKLAGEVERFAHREN_SGG = "854"
    KOSTENFESTSETZUNGSVERFAHREN_ZPO = "861"
    JUSTIZVERWALTUNGSVERFAHREN_EGGVG = "871"
    BEAMTENRECHTLICHES_DISZIPLINARVERFAHREN = "911"
    ARBEITSRECHTLICHES_DISZIPLINARVERFAHREN = "912"
    VERGABEVERFAHREN = "921"
    KEIN_OE_RECHTLICHES_OE_RECHT_PRIVATRECHT_VERFAHREN = "998"
    KEINE_VORGABE = "999"

    def to_label(self) -> str:
        return VERFAHRENSART_TO_STRING[self]


STRING_TO_VERFAHRENSART: dict[str, Verfahrensart] = {
    "Gesetzgebungsverfahren (formelles Recht)": Verfahrensart.GESETZGEBUNGSVERFAHREN_FORMELLES_RECHT,
    "Gesetzgebungsverfahren (formelles und materielles Recht)": Verfahrensart.GESETZGEBUNGSVERFAHREN_FORMELLES_UND_MATERIELLES_RECHT,
    "Haushaltsplanung und Haushaltsplanfeststellung (formelles Gesetz)": Verfahrensart.HAUSHALTSPLANUNG_HAUSHALTSPLANFESTSTELLUNG_FORMELLES_GESETZ,
    "Normsetzungsverfahren (ohne förmliche Gesetze)": Verfahrensart.NORMSETZUNGSVERFAHREN_OHNE_FORMELLICHE_GESETZE,
    "Landesentwicklungsplanfestsetzung": Verfahrensart.LANDESENTWICKLUNGSPLANFESTSETZUNG,
    "Regionalentwicklungsplanfestsetzung": Verfahrensart.REGIONALENTWICKLUNGSPLANFESTSETZUNG,
    "Bauleitplanverfahren": Verfahrensart.BAULEITPLANVERFAHREN,
    "Nicht förmliches Verwaltungsverfahren nach VwVfG und VwGO": Verfahrensart.NICHT_FORMLICHES_VERWALTUNGSVERFAHREN_VWVFG_VWGO,
    "Förmliches Verwaltungsverfahren nach VwVfG": Verfahrensart.FORMAL_VERWALTUNGSVERFAHREN_VWVFG,
    "Widerspruchsverfahren (Vorverfahren) nach VwGO": Verfahrensart.WIDERSPRUCHSVERFAHREN_VORVERFAHREN_VWGO,
    "Widerspruchsverfahren (Vorverfahren) nach VwGO - Abhilfeverfahren": Verfahrensart.WIDERSPRUCHSVERFAHREN_ABHILFE_VERFAHREN_VWGO,
    "Verwaltungsverfahren über eine einheitliche Stelle nach VwVfG": Verfahrensart.VERWALTUNGSVERFAHREN_EINHEITLICHE_STELLE_VWVFG,
    "Verwaltungsverfahren mit Genehmigungsfiktion nach VwVfG": Verfahrensart.VERWALTUNGSVERFAHREN_GENEHMIGUNGSFIKTION_VWVFG,
    "Planfeststellungsverfahren nach VwVfG": Verfahrensart.PLANFESTSTELLUNGSVERFAHREN_VWVFG,
    "Planfeststellungsverfahren (Bündelung mehrerer Verfahren) nach VwVfG": Verfahrensart.PLANFESTSTELLUNGSVERFAHREN_BUENDELUNG_VWVFG,
    "Verfahren zur Festsetzung von Verwaltungsgebühren und Auslagen": Verfahrensart.VERWALTUNGSGEBUEHREN_AUSLAGEN_FESTSETZUNG_VERFAHREN,
    "Verfahren zur Festsetzung von Benutzungsgebühren und Auslagen": Verfahrensart.BENUTZUNGSGEBUEHREN_AUSLAGEN_FESTSETZUNG_VERFAHREN,
    "Verfahren zur Festsetzung von privatrechtlichen Entgelten": Verfahrensart.PRIVATRECHTLICHES_ENTGELT_FESTSETZUNG_VERFAHREN,
    "Verfahren zur Festsetzung von Beiträgen": Verfahrensart.BEITRAG_FESTSETZUNG_VERFAHREN,
    "Vollstreckungsverfahren nach VwVfG": Verfahrensart.VOLLSTRECKUNGSVERFAHREN_VWVFG,
    "Bußgeldverfahren nach VwVfG": Verfahrensart.BUSSGELDVERFAHREN_VWVFG,
    "Besteuerungsverfahren nach AO": Verfahrensart.BESTEUERUNGSVERFAHREN_AO,
    "Verfahren zur gesonderten Feststellung nach AO": Verfahrensart.GESONDERTE_FESTSTELLUNG_VERFAHREN_AO,
    "Verfahren zur Festsetzung der Steuermessbeträge nach AO": Verfahrensart.STEUERMESSBETRAEGE_FESTSETZUNG_VERFAHREN_AO,
    "Einspruchsverfahren (Vorverfahren) nach AO": Verfahrensart.EINSRUCHSVERFAHREN_VORVERFAHREN_AO,
    "Vollstreckungsverfahren nach AO": Verfahrensart.VOLLSTRECKUNGSVERFAHREN_AO,
    "Steuerstrafverfahren nach AO": Verfahrensart.STEUERSTRAFVERFAHREN_AO,
    "Bußgeldverfahren nach AO": Verfahrensart.BUSSGELDVERFAHREN_AO,
    "Sozialverwaltungsverfahren nach SGB X": Verfahrensart.SOZIALVERWALTUNGSVERFAHREN_SGB_X,
    "Widerspruchsverfahren (Vorverfahren) nach SGB X": Verfahrensart.WIDERSPRUCHSVERFAHREN_VORVERFAHREN_SGB_X,
    "Vollstreckungsverfahren nach SGB X": Verfahrensart.VOLLSTRECKUNGSVERFAHREN_SGB_X,
    "Bußgeldverfahren nach SGB X": Verfahrensart.BUSSGELDVERFAHREN_SGB_X,
    "Normenkontrollverfahren nach VwGO": Verfahrensart.NORMENKONTROLLVERFAHREN_VWGO,
    "Anfechtungsklageverfahren nach VwGO": Verfahrensart.ANFECHTUNGSKLAGEVERFAHREN_VWGO,
    "Verpflichtungsklageverfahren nach VwGO": Verfahrensart.VERPFLICHTUNGSKLAGEVERFAHREN_VWGO,
    "Feststellungsklageverfahren nach VwGO": Verfahrensart.FESTSTELLUNGSKLAGEVERFAHREN_VWGO,
    "Leistungsklageverfahren nach VwGO": Verfahrensart.LEISTUNGSKLAGEVERFAHREN_VWGO,
    "Anfechtungsklageverfahren FGO": Verfahrensart.ANFECHTUNGSKLAGEVERFAHREN_FGO,
    "Verpflichtungsklageverfahren nach FGO": Verfahrensart.VERPFLICHTUNGSKLAGEVERFAHREN_FGO,
    "Feststellungsklageverfahren nach FGO": Verfahrensart.FESTSTELLUNGSKLAGEVERFAHREN_FGO,
    "Leistungsklageverfahren nach FGO": Verfahrensart.LEISTUNGSKLAGEVERFAHREN_FGO,
    "Anfechtungsklageverfahren nach SGG": Verfahrensart.ANFECHTUNGSKLAGEVERFAHREN_SGG,
    "Verpflichtungsklageverfahren nach SGG": Verfahrensart.VERPFLICHTUNGSKLAGEVERFAHREN_SGG,
    "Feststellungsklageverfahren nach SGG": Verfahrensart.FESTSTELLUNGSKLAGEVERFAHREN_SGG,
    "Leistungsklageverfahren nach SGG": Verfahrensart.LEISTUNGSKLAGEVERFAHREN_SGG,
    "Kostenfestsetzungsverfahren nach ZPO": Verfahrensart.KOSTENFESTSETZUNGSVERFAHREN_ZPO,
    "Justizverwaltungsverfahren nach EGGVG": Verfahrensart.JUSTIZVERWALTUNGSVERFAHREN_EGGVG,
    "Beamtenrechtliches Disziplinarverfahren": Verfahrensart.BEAMTENRECHTLICHES_DISZIPLINARVERFAHREN,
    "Arbeitsrechtliches Disziplinarverfahren": Verfahrensart.ARBEITSRECHTLICHES_DISZIPLINARVERFAHREN,
    "Vergabeverfahren": Verfahrensart.VERGABEVERFAHREN,
    "Kein öffentlich-rechtliches bzw. öffentlich-rechtlich-privatrechtliches Verfahren": Verfahrensart.KEIN_OE_RECHTLICHES_OE_RECHT_PRIVATRECHT_VERFAHREN,
    "Keine Vorgabe": Verfahrensart.KEINE_VORGABE,
}
VERFAHRENSART_TO_STRING: dict[Verfahrensart, str] = {
    art: label for label, art in STRING_TO_VERFAHRENSART.items()
}


@dataclass(slots=True)
class Prozessklasse:
    id: str
    version: str | None
    gliederungsebene: Gliederungsebene | None
    uebergeordnete_prozessklasse_id: str | None
    name: str
    bezeichnung: str | None
    definition: str | None
    handlungsgrundlage: list[Handlungsgrundlage]
    zwecksetzung: Annotated[Zwecksetzung | None, create_code_encoding(Zwecksetzung)]
    operatives_ziel: Annotated[
        OperativesZiel | None, create_code_encoding(OperativesZiel)
    ]
    handlungsform: Annotated[Handlungsform | None, create_code_encoding(Handlungsform)]
    verfahrensart: Annotated[Verfahrensart | None, create_code_encoding(Verfahrensart)]
    fachlich_freigebende_stelle: str | None
    verwaltungspolitische_kodierung: list[VerwaltungspolitischeKodierung]
    merkmal: list[Merkmal]
    klassifikation: list[Klassifikation]
    schlagwort: Annotated[
        list[str], CustomListEncoding(xmlstruct.OptionalStringEncoding)
    ]
    zustandsangaben: Zustandsangaben | None

    @property
    def letzter_aenderungszeitpunkt(self) -> datetime | None:
        return (
            self.zustandsangaben.letzter_aenderungszeitpunkt
            if self.zustandsangaben is not None
            else None
        )

    @property
    def freigabe_status(self) -> FreigabeStatus | None:
        return self.zustandsangaben.status if self.zustandsangaben is not None else None

    @property
    def gueltig_ab(self) -> datetime | None:
        if (
            self.zustandsangaben is not None
            and self.zustandsangaben.gueltigkeitszeitraum is not None
        ):
            return self.zustandsangaben.gueltigkeitszeitraum.beginn

    @property
    def gueltig_bis(self) -> datetime | None:
        if (
            self.zustandsangaben is not None
            and self.zustandsangaben.gueltigkeitszeitraum is not None
        ):
            return self.zustandsangaben.gueltigkeitszeitraum.ende

    @property
    def prozessart(self) -> str | None:
        for value in self.merkmal:
            if value.merkmal_name == "Prozessart":
                try:
                    return value.merkmal_wert[
                        0
                    ]  # Standard defines this to be of at least length 1
                except IndexError:
                    return None


@dataclass(slots=True)
class ProzessKatalog:
    name: str
    version: str | None
    verwaltungspolitische_kodierung: list[VerwaltungspolitischeKodierung]
    prozessklasse: list[Prozessklasse]


@dataclass(slots=True)
class ProzessBibliothek:
    name: str
    verwaltungspolitische_kodierung: list[VerwaltungspolitischeKodierung]
    prozess: list[Prozess]


@dataclass(slots=True)
class ProzessMessage:
    nachrichtenkopf: ProzessNachrichtenKopf
    prozesskatalog: ProzessKatalog
    prozessbibliothek: ProzessBibliothek

    @property
    def prozess(self):
        return self.prozessbibliothek.prozess

    @property
    def prozessklasse(self):
        return self.prozesskatalog.prozessklasse


@dataclass(slots=True)
class AlleInhalte0302:
    anfrage_uuid: str
    prozess_katalog: ProzessKatalog | None
    prozess_bibliothek: ProzessBibliothek | None


XPROZESS_NAMESPACE = "http://www.regierung-mv.de/xprozess/2"

ProzessMessageEncoding = xmlstruct.derive(
    ProzessMessage, namespace=XPROZESS_NAMESPACE, local_name="alleInhalte.export.0303"
)


def parse_prozess_message(data: bytes | str) -> ProzessMessage:
    return ProzessMessageEncoding.parse(data)


VerwaltungspolitischeKodierungEncoding = xmlstruct.derive(
    VerwaltungspolitischeKodierung,
    namespace=XPROZESS_NAMESPACE,
    local_name="alleInhalte.export.0303",
)
ProzessEncoding = xmlstruct.derive(
    Prozess, namespace=XPROZESS_NAMESPACE, local_name="prozess"
)
parse_prozess = ProzessEncoding.parse
ProzessklasseEncoding = xmlstruct.derive(
    Prozessklasse, namespace=XPROZESS_NAMESPACE, local_name="prozessklasse"
)
parse_prozessklasse = ProzessklasseEncoding.parse

XPROZESS_NACHRICHTENKOPF = f"{{{XPROZESS_NAMESPACE}}}nachrichtenkopf"
XPROZESS_NACHRICHT_UUID = f"{{{XPROZESS_NAMESPACE}}}nachrichtUUID"
XPROZESS_NACHRICHTENTYP = f"{{{XPROZESS_NAMESPACE}}}nachrichtentyp"
XPROZESS_ERSTELLUNGSZEITPUNKT = f"{{{XPROZESS_NAMESPACE}}}erstellungszeitpunkt"
XPROZESS_AUTOR = f"{{{XPROZESS_NAMESPACE}}}autor"
XPROZESS_NAME = f"{{{XPROZESS_NAMESPACE}}}name"
XPROZESS_VERSION = f"{{{XPROZESS_NAMESPACE}}}version"
XPROZESS_PROZESSBIBLIOTHEK = f"{{{XPROZESS_NAMESPACE}}}prozessbibliothek"
XPROZESS_PROZESS = f"{{{XPROZESS_NAMESPACE}}}prozess"
XPROZESS_PROZESSKATALOG = f"{{{XPROZESS_NAMESPACE}}}prozesskatalog"
XPROZESS_PROZESSKLASSE = f"{{{XPROZESS_NAMESPACE}}}prozessklasse"

XPROZESS_VERWALTUNGSPOLITISCHE_KODIERUNG = (
    f"{{{XPROZESS_NAMESPACE}}}verwaltungspolitischeKodierung"
)


def _serialize(child: xml.XmlElement) -> str:
    return xml.serialize(child).decode("utf-8")


def _parse_prozessklassen(
    node: xml.XmlElement,
) -> list[tuple[Prozessklasse, str]]:
    prozessklassen: list[tuple[Prozessklasse, str]] = []
    for child in node:
        if child.tag == XPROZESS_PROZESSKLASSE:
            raw_prozessklasse = _serialize(child)
            try:
                prozessklassen.append((parse_prozessklasse(child), raw_prozessklasse))
            except XProzessException as ex:
                logger.error(f"Failed to parse Prozessklasse: {str(ex)}")

    return prozessklassen


def _parse_prozesse(
    node: xml.XmlElement,
) -> list[tuple[Prozess, str]]:
    prozesse: list[tuple[Prozess, str]] = []
    for child in node:
        if child.tag == XPROZESS_PROZESS:
            raw_prozess = _serialize(child)
            try:
                prozesse.append((parse_prozess(child), raw_prozess))
            except XProzessException as ex:
                logger.error(f"Failed to parse Prozess: {str(ex)}")

    return prozesse


@dataclass(slots=True)
class ParsedProzesseResponse:
    prozesse: list[tuple[Prozess, str]]
    prozessklassen: list[tuple[Prozessklasse, str]]


logger = logging.getLogger(__name__)


def parse_search_result(node: xml.XmlElement) -> ParsedProzesseResponse:
    prozesse: list[tuple[Prozess, str]] = []
    prozessklassen: list[tuple[Prozessklasse, str]] = []
    for child in node:
        if child.tag == XPROZESS_PROZESSBIBLIOTHEK:
            [
                prozesse.append((prozess, content))
                for prozess, content in _parse_prozesse(child)
            ]
        elif child.tag == XPROZESS_PROZESSKATALOG:
            [
                prozessklassen.append((prozess, content))
                for prozess, content in _parse_prozessklassen(child)
            ]

    return ParsedProzesseResponse(
        prozesse=prozesse,
        prozessklassen=prozessklassen,
    )


def _create_xprozess_nachrichtenkopf() -> ET._Element:  # type: ignore
    nachrichtenkopf = ET.Element(XPROZESS_NACHRICHTENKOPF)
    ET.SubElement(
        nachrichtenkopf, XPROZESS_NACHRICHT_UUID
    ).text = "e05803ea-cbd7-4c97-93f9-0b088388a291"
    nachrichtentyp = ET.Element(
        XPROZESS_NACHRICHTENTYP,
        attrib={
            "listURI": "urn:xoev-de:xprozess:codeliste:nachricht",
            "listVersionID": "1.0",
        },
    )
    ET.SubElement(nachrichtentyp, "code").text = "0303"
    nachrichtenkopf.append(nachrichtentyp)
    ET.SubElement(
        nachrichtenkopf, XPROZESS_ERSTELLUNGSZEITPUNKT
    ).text = utc_now().isoformat()
    ET.SubElement(nachrichtenkopf, XPROZESS_AUTOR).text = "BOC"

    return nachrichtenkopf


def create_xprozess_message_for_prozess(prozess_xml: str) -> str:
    nsmap = {"xprozess": XPROZESS_NAMESPACE}

    alle_inhalte_0303 = ET.Element(
        f"{{{XPROZESS_NAMESPACE}}}alleInhalte.export.0303",
        nsmap=nsmap,
        attrib={
            "produkt": "ADONIS",
            "produkthersteller": "BOC",
            "xprozessVersion": "2.0",
        },
    )

    alle_inhalte_0303.append(_create_xprozess_nachrichtenkopf())

    prozesskatalog = ET.Element(XPROZESS_PROZESSKATALOG)
    ET.SubElement(prozesskatalog, XPROZESS_NAME).text = "Prozesskatalog (FIM)"
    alle_inhalte_0303.append(prozesskatalog)

    prozessbibliothek = ET.Element(XPROZESS_PROZESSBIBLIOTHEK)
    ET.SubElement(prozessbibliothek, XPROZESS_NAME).text = "FIM Prozessbibliothek Bund"
    prozess = ET.fromstring(prozess_xml.encode("utf-8"))
    prozessbibliothek.append(prozess)
    alle_inhalte_0303.append(prozessbibliothek)

    return xml.serialize(alle_inhalte_0303).decode("utf-8")


def create_xprozess_message_for_prozessklasse(prozessklasse_xml: str) -> str:
    nsmap = {"xprozess": XPROZESS_NAMESPACE}

    alle_inhalte_0303 = ET.Element(
        f"{{{XPROZESS_NAMESPACE}}}alleInhalte.export.0303",
        nsmap=nsmap,
        attrib={
            "produkt": "ADONIS",
            "produkthersteller": "BOC",
            "xprozessVersion": "2.0",
        },
    )

    alle_inhalte_0303.append(_create_xprozess_nachrichtenkopf())

    prozesskatalog = ET.Element(XPROZESS_PROZESSKATALOG)
    ET.SubElement(prozesskatalog, XPROZESS_NAME).text = "Prozesskatalog (FIM)"
    prozessklasse = ET.fromstring(prozessklasse_xml.encode("utf-8"))
    prozesskatalog.append(prozessklasse)
    alle_inhalte_0303.append(prozesskatalog)

    prozessbibliothek = ET.Element(XPROZESS_PROZESSBIBLIOTHEK)
    ET.SubElement(prozessbibliothek, XPROZESS_NAME).text = "FIM Prozessbibliothek Bund"
    alle_inhalte_0303.append(prozessbibliothek)

    return xml.serialize(alle_inhalte_0303).decode("utf-8")
