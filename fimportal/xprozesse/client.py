from __future__ import annotations
from abc import ABC, abstractmethod
from asyncio import sleep
from datetime import datetime
import logging
from uuid import uuid4

import httpx

from fimportal import xml
from fimportal.helpers import utc_now
from fimportal.xprozesse.prozess import (
    XPROZESS_NAMESPACE,
    ParsedProzesseResponse,
    Prozess,
    Prozessklasse,
    XProzessException,
    parse_search_result,
)

logger = logging.getLogger(__name__)


SOAP_NAMESPACE = "http://schemas.xmlsoap.org/soap/envelope/"

SOAP_ENVELOPE = f"{{{SOAP_NAMESPACE}}}Envelope"
SOAP_BODY = f"{{{SOAP_NAMESPACE}}}Body"
XPROZESS_ANTWORT_LEISTUNG = f"{{{XPROZESS_NAMESPACE}}}alleInhalte.antwort.0302"


class Client(ABC):
    @abstractmethod
    async def get_prozess_data(
        self,
    ) -> ParsedProzesseResponse: ...


class SearchNotFinishedException(Exception):
    pass


class SearchTookTooLongException(Exception):
    pass


class HttpClient(Client):
    def __init__(
        self, base_url: str, api_key: str, certificate_filepath: str, key_filepath: str
    ):
        self._base_url = base_url
        self._api_key = api_key

        self._client = httpx.AsyncClient(
            timeout=60, cert=(certificate_filepath, key_filepath)
        )

    async def get_prozess_data(self) -> ParsedProzesseResponse:
        sleep_time = 20
        max_seconds_per_batch = 20 * 60
        start_batch = utc_now()
        status_url = await self._create_search_request()
        logger.info(f"Started Search Request: {start_batch}")
        while True:
            if _is_loop_too_long(start_batch, max_seconds_per_batch):
                raise SearchTookTooLongException(
                    f"Search did not finish within {max_seconds_per_batch} Seconds but expected to finish within 1 Minute"
                )
            try:
                result_url = await self.get_search_request_status(status_url)
                break
            except SearchNotFinishedException:
                await sleep(sleep_time)

        while True:
            if _is_loop_too_long(start_batch, max_seconds_per_batch):
                raise SearchTookTooLongException(
                    f"Search did not finish within {max_seconds_per_batch} Seconds but expected to finish within 1 Minute"
                )
            try:
                prozesse_with_content = await self.get_search_request_result(result_url)
                logger.info(
                    f"Search finished after {(utc_now() - start_batch).total_seconds()} seconds"
                )
                return prozesse_with_content
            except SearchNotFinishedException:
                await sleep(sleep_time)

    async def _create_search_request(self) -> str:
        content = _search_prozesse_alle_inhalte()
        http_response = await self._client.post(
            f"{self._base_url}/search_requests/?api_key={self._api_key}",
            content=content,
        )

        if not http_response.is_success:
            raise XProzessException(
                f"Failed to create XProzess Search (Code: {http_response.status_code}): \n{http_response.read()}"
            )

        status_url = http_response.headers.get("X-Status-URL")
        if status_url is None:
            raise XProzessException("Header X-Status-URL not given in response.")
        logger.info(f"Created Search: {status_url}")
        return status_url

    async def get_search_request_status(self, status_url: str) -> str:
        status_response = await self._client.get(
            f"{status_url}/?api_key={self._api_key}"
        )

        if status_response.status_code == 201:
            result_url = status_response.headers.get("X-Response-URL")
            if result_url is None:
                raise XProzessException("Header X-Response-URL not given in response.")
            logger.info(f"Search Status Request finished: {utc_now()}")
            return result_url
        if status_response.status_code == 211:
            logger.info("Search Status Request still processing")
            raise SearchNotFinishedException(
                f"Search Status Response: {status_response.status_code}"
            )
        data = status_response.read()
        raise XProzessException(
            f"Failed to get search request status (Code: {status_response.status_code}): \n{data}"
        )

    async def get_search_request_result(
        self, result_url: str
    ) -> ParsedProzesseResponse:
        result_response = await self._client.get(
            f"{result_url}/?api_key={self._api_key}"
        )

        if result_response.status_code == 200:
            result_body = result_response.text
            logger.info(f"Search Result Request finished: {utc_now()}")
            return parse_response(result_body)

        if result_response.status_code == 211:
            logger.info("Search Result Request still processing")
            raise SearchNotFinishedException(
                f"Search Result Response: {result_response.status_code}"
            )

        data = result_response.read()
        raise XProzessException(
            f"Failed to get search request result (Code: {result_response.status_code}): \n{data}"
        )


# <xprozess:suchprofil>
#     <xprozess:einschraenkungProzessklasse>
#         <xprozess:sucheHatProzesse>true</xprozess:sucheHatProzesse>
#     </xprozess:einschraenkungProzessklasse>
#     <xprozess:einschraenkungProzesseVersionen listURI="urn:xoev-de:xprozess:codeliste:versionen"
#         listVersionID="1.0">
#         <code>2</code>
#     </xprozess:einschraenkungProzesseVersionen>
# </xprozess:suchprofil>
def _search_prozesse_alle_inhalte() -> str:
    return f"""
<xprozess:alleInhalte.anfrage.0301 xmlns:xprozess="http://www.regierung-mv.de/xprozess/2" produkt="ADONIS NP" produkthersteller="BOC" xprozessVersion="2.0">
    <xprozess:nachrichtenkopf>
        <xprozess:nachrichtUUID>{uuid4()}</xprozess:nachrichtUUID>
        <xprozess:nachrichtentyp listURI="urn:xoev-de:xprozess:codeliste:nachricht"
            listVersionID="1.0"><code>0301</code></xprozess:nachrichtentyp>
        <xprozess:erstellungszeitpunkt>{utc_now().isoformat()}</xprozess:erstellungszeitpunkt>
    </xprozess:nachrichtenkopf>   
</xprozess:alleInhalte.anfrage.0301>
"""


def _is_loop_too_long(start: datetime, max_duration: int) -> bool:
    return (utc_now() - start).total_seconds() > max_duration


TEST_PROZESS_XML = """
<xprozess:prozess xmlns:xprozess="http://www.regierung-mv.de/xprozess/2" >
    <xprozess:id>00000000000000</xprozess:id>
    <xprozess:name>Antrag Erteilung Aufenthaltserlaubnis</xprozess:name>
</xprozess:prozess>
"""
TEST_PROZESSKLASSE_XML = """
<xprozess:prozessklasse xmlns:xprozess="http://www.regierung-mv.de/xprozess/2">
    <xprozess:id>00000000000000</xprozess:id>
    <xprozess:name>Antrag Erteilung Aufenthaltserlaubnis</xprozess:name>
</xprozess:prozessklasse>
"""


def parse_response(content: str) -> ParsedProzesseResponse:
    content_size = len(content.encode("utf-8"))
    if content_size > 25 * 1024 * 1024:  # 25MB in bytes
        logger.error(f"Response content is very large: {content_size}")

    node = xml.parse_document(content)

    return parse_search_result(node)


class TestClient(Client):
    def __init__(self):
        self.result: ParsedProzesseResponse = ParsedProzesseResponse(
            prozesse=[], prozessklassen=[]
        )

    def add_prozess(self, prozess: Prozess, xml_content: str = TEST_PROZESS_XML):
        self.result.prozesse = [(prozess, xml_content)]

    def add_prozessklasse(
        self, prozessklasse: Prozessklasse, xml_content: str = TEST_PROZESSKLASSE_XML
    ):
        self.result.prozessklassen = [(prozessklasse, xml_content)]

    async def get_prozess_data(
        self,
    ) -> ParsedProzesseResponse:
        return self.result
