## Warum machen wir das?
<!-- Eine kurze Erklärung des Warum, nicht des Was oder Wie. Gehen Sie davon aus, dass der/die Leser:in die Hintergründe nicht kennt und keine Zeit hat, Informationen aus Kommentaren herauszusuchen. -->


## Relevante Informationen
<!-- Informationen, die im Kontext der Umsetzung dieser User Story relevant sind. -->


## Akzeptanzkriterien
<!-- Was muss erfüllt sein, damit diese User Story als erledigt angesehen werden kann. -->


