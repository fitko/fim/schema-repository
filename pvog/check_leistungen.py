from pathlib import Path
import zipfile
from collections import defaultdict
from dataclasses import dataclass
from datetime import datetime
from enum import Enum
from itertools import chain
from typing import Any

import orjson
import click
import pandas as pd

from fimportal.xzufi.common import (
    ALT_GERMAN,
    GERMAN,
    REDAKTION_ID_TO_LABEL,
    Leistungsschluessel,
    StringLocalized,
    XzufiException,
)
from fimportal.xzufi.leistung import (
    Identifikator,
    LanguageCode,
    ModulTextTyp,
    Leistung,
    parse_leistung,
)


class ErrorCode(Enum):
    MISSING_MODUL = 0
    DUPLICATE_TEXT_MODUL = 1
    TEXT_MODUL_MISSING_TRANSLATION = 2
    TEXT_MODUL_DUPLICATE_LANGUAGE = 3
    NO_LEIKA_ID = 4
    MULTIPLE_LEIKA_IDS = 5
    INVALID_LEIKA_ID = 6
    MISSING_SDG_INFORMATION = 7
    INVALID_SDG_CODE = 8


@dataclass(slots=True)
class FailedCheck:
    code: ErrorCode
    params: list[str]
    message: str


@dataclass(slots=True)
class FailedCheckWithMetadata:
    source: str
    check: FailedCheck
    leistung_id: Identifikator
    leika_ids: list[str]
    sdg_informationsbereiche: list[str]
    kategorien: list[str]
    kategorieklassen: list[str]
    created_at: datetime | None
    updated_at: datetime | None


@dataclass(slots=True)
class LeistungWithMetadata:
    leistung: Leistung
    source: str
    kategorien: list[str]
    kategorieklassen: list[str]
    created_at: datetime | None
    updated_at: datetime | None
    failed_checks: list[FailedCheck]

    def get_failed_checks_with_metadata(self) -> list[FailedCheckWithMetadata]:
        return [
            FailedCheckWithMetadata(
                source=self.source,
                check=check,
                leistung_id=self.leistung.id,
                leika_ids=self.leistung.get_leistungsschluessel(),
                sdg_informationsbereiche=self.leistung.informationsbereich_sdg,
                kategorien=self.kategorien,
                kategorieklassen=self.kategorieklassen,
                created_at=self.created_at,
                updated_at=self.updated_at,
            )
            for check in self.failed_checks
        ]

    def get_unique_id(self) -> str:
        return f"{self.leistung.id.scheme_agency_id}_{self.leistung.id.value}"

    def to_parquet_errors(self) -> list[dict[str, Any]]:
        unique_id = self.get_unique_id()

        redaktion_id = self.leistung.id.scheme_agency_id
        assert redaktion_id is not None

        if self.leistung.modul_fachliche_freigabe:
            fachlich_freigegeben_am = (
                self.leistung.modul_fachliche_freigabe.fachlich_freigegeben_am
            )
            fachlich_freigegeben_durch = _get_localized_string_translation(
                self.leistung.modul_fachliche_freigabe.fachlich_freigegeben_durch
            )
        else:
            fachlich_freigegeben_am = None
            fachlich_freigegeben_durch = None

        if fachlich_freigegeben_am is not None:
            fachlich_freigegeben_am = fachlich_freigegeben_am.isoformat()

        return [
            {
                "leistung_unique_id": unique_id,
                "source": REDAKTION_ID_TO_LABEL.get(redaktion_id, redaktion_id),
                "code": check.code.value,
                "message": check.message,
                "params": check.params,
                "leistungsschluessel": self.leistung.get_leistungsschluessel(),
                "sdg_informationsbereiche": self.leistung.informationsbereich_sdg,
                "kategorien": self.kategorien,
                "kategorieklassen": self.kategorieklassen,
                "typ": [typ.value for typ in self.leistung.typisierung],
                "created_at": self.created_at,
                "updated_at": self.updated_at,
                "fachlich_freigegeben_am": fachlich_freigegeben_am,
                "fachlich_freigegeben_durch": fachlich_freigegeben_durch,
                "total_errors": len(self.failed_checks),
            }
            for check in self.failed_checks
        ]

    def to_parquet_leistung(self) -> list[dict[str, Any]]:
        redaktion_id = self.leistung.id.scheme_agency_id
        assert redaktion_id is not None

        if self.leistung.modul_fachliche_freigabe:
            fachlich_freigegeben_am = (
                self.leistung.modul_fachliche_freigabe.fachlich_freigegeben_am
            )
            fachlich_freigegeben_durch = _get_localized_string_translation(
                self.leistung.modul_fachliche_freigabe.fachlich_freigegeben_durch
            )
        else:
            fachlich_freigegeben_am = None
            fachlich_freigegeben_durch = None

        if fachlich_freigegeben_am is not None:
            fachlich_freigegeben_am = fachlich_freigegeben_am.isoformat()

        # Include at least one entry, even if there is now Leistungsschluessel
        leistungsschluessel = self.leistung.get_leistungsschluessel()
        if len(leistungsschluessel) == 0:
            leistungsschluessel = [None]

        return [
            {
                "unique_id": self.get_unique_id(),
                "agency_id": redaktion_id,
                "primary_leistung_id": self.leistung.id.value,
                "source": REDAKTION_ID_TO_LABEL.get(redaktion_id, redaktion_id),
                "leistungsschluessel": lsl,
                "bezeichnung": _get_text_modul_content(
                    self.leistung, ModulTextTyp.LEISTUNGSBEZEICHNUNG
                ),
                "bezeichnung 2": _get_text_modul_content(
                    self.leistung, ModulTextTyp.LEISTUNGSBEZEICHNUNG_II
                ),
                "kurztext": _get_text_modul_content(
                    self.leistung, ModulTextTyp.KURZTEXT
                ),
                "volltext": _get_text_modul_content(
                    self.leistung, ModulTextTyp.VOLLTEXT
                ),
                "rechtsgrundlagen": _get_text_modul_content(
                    self.leistung, ModulTextTyp.RECHTSGRUNDLAGEN
                ),
                "typ": [typ.value for typ in self.leistung.typisierung],
                "created_at": self.created_at,
                "updated_at": self.updated_at,
                "sdg_informationsbereiche": self.leistung.informationsbereich_sdg,
                "kategorien": self.kategorien,
                "kategorieklassen": self.kategorieklassen,
                "total_errors": len(self.failed_checks),
                "fachlich_freigegeben_am": fachlich_freigegeben_am,
                "fachlich_freigegeben_durch": fachlich_freigegeben_durch,
            }
            for lsl in leistungsschluessel
        ]


def _get_text_modul_content(leistung: Leistung, typ: ModulTextTyp) -> str | None:
    """
    Extract any content (inhalt and/or links) for the specified text module if present.
    """
    for modul in leistung.modul_text:
        if modul.leika_textmodul != typ:
            continue

        content = _get_localized_string_translation(modul.inhalt)
        if content is None and len(modul.weiterfuehrender_link) == 0:
            return None

        content = content or ""

        return "\n\n".join(
            [content, *[link.uri for link in modul.weiterfuehrender_link]]
        )


def _get_localized_string_translation(values: list[StringLocalized]) -> str | None:
    for value in values:
        if value.language_code == GERMAN or value.language_code == ALT_GERMAN:
            return value.value

    return None


"""
These are all requried text modules for "Leistungsbeschreibungen".
AFAIK, every entry in the PVOG is a finished Leistungsbeschreibung, so these should all be required here.

Information from:
https://fimportal.de/fim-haus -> 'QS-Kriterien (Leistungen)'
"""
REQUIRED_TEXT_MODULE = [
    ModulTextTyp.LEISTUNGSBEZEICHNUNG,
    ModulTextTyp.LEISTUNGSBEZEICHNUNG_II,
    ModulTextTyp.VOLLTEXT,
    ModulTextTyp.ERFORDERLICHE_UNTERLAGEN,
    ModulTextTyp.VORAUSSETZUNGEN,
    ModulTextTyp.VERFAHRENSABLAUF,
    ModulTextTyp.FORMUALRE,
    ModulTextTyp.HINWEISE,
    ModulTextTyp.URHEBER,
    ModulTextTyp.ZUSTAENDIGE_STELLE,
    ModulTextTyp.ANSPRECHPUNKT,
    ModulTextTyp.RECHTSBEHELF,
    ModulTextTyp.TEASER,
]


def check_text_modul_missing_translation(
    leistung: Leistung, failed_checks: list[FailedCheck]
):
    for text_modul in leistung.modul_text:
        # Skip modules without any translation, as only a link could been provided.
        # But if there is a translation, german should be supplied.
        if len(text_modul.inhalt) == 0:
            continue

        translations = [content.language_code for content in text_modul.inhalt]

        for sprachversion in leistung.sprachversion:
            if sprachversion.language_code not in translations:
                failed_checks.append(
                    FailedCheck(
                        code=ErrorCode.TEXT_MODUL_MISSING_TRANSLATION,
                        params=[
                            text_modul.leika_textmodul.name,
                            sprachversion.language_code,
                        ],
                        message=f"No translation for {sprachversion.language_code} in TextModul of type {text_modul.leika_textmodul}",
                    )
                )


def check_unique_text_modul(leistung: Leistung, failed_checks: list[FailedCheck]):
    existing_text_module: set[ModulTextTyp] = set()
    for text_modul in leistung.modul_text:
        if text_modul.leika_textmodul in existing_text_module:
            failed_checks.append(
                FailedCheck(
                    code=ErrorCode.DUPLICATE_TEXT_MODUL,
                    params=[text_modul.leika_textmodul.name],
                    message=f"Duplicate TextModul of type {text_modul.leika_textmodul}",
                )
            )

        existing_text_module.add(text_modul.leika_textmodul)


def check_missing_modul(leistung: Leistung, failed_checks: list[FailedCheck]):
    existing_text_module: set[ModulTextTyp] = set(
        [modul.leika_textmodul for modul in leistung.modul_text]
    )

    for modul_typ in REQUIRED_TEXT_MODULE:
        if modul_typ not in existing_text_module:
            failed_checks.append(
                FailedCheck(
                    code=ErrorCode.MISSING_MODUL,
                    params=[modul_typ.name],
                    message=f"Missing TextModul of type {modul_typ}",
                )
            )


def check_text_modul_unique_language(
    leistung: Leistung, failed_checks: list[FailedCheck]
):
    for text_modul in leistung.modul_text:
        existing_languages: set[LanguageCode] = set()

        for content in text_modul.inhalt:
            if content.language_code in existing_languages:
                failed_checks.append(
                    FailedCheck(
                        code=ErrorCode.TEXT_MODUL_DUPLICATE_LANGUAGE,
                        params=[content.language_code],
                        message=f"Duplicate inhalt in TextModul for language {content.language_code}",
                    )
                )

            existing_languages.add(LanguageCode(content.language_code))


def check_leistungsschluessel(leistung: Leistung, failed_checks: list[FailedCheck]):
    leistungsschluessel = leistung.get_leistungsschluessel()

    if len(leistungsschluessel) == 0:
        failed_checks.append(
            FailedCheck(
                code=ErrorCode.NO_LEIKA_ID,
                params=[],
                message="Keine LeiKa Referenze",
            )
        )
    elif len(leistungsschluessel) > 1:
        failed_checks.append(
            FailedCheck(
                code=ErrorCode.MULTIPLE_LEIKA_IDS,
                params=[],
                message="Mehrere LeiKa Referenzen",
            )
        )

    for referenz in leistungsschluessel:
        try:
            _ = Leistungsschluessel.from_str(referenz)
        except XzufiException as error:
            failed_checks.append(
                FailedCheck(
                    code=ErrorCode.INVALID_LEIKA_ID,
                    params=[],
                    message=f"Ungueltige LeiKa Referenz: {str(error)}",
                )
            )


def check_sdg_codes(leistung: Leistung, failed_checks: list[FailedCheck]):
    if len(leistung.informationsbereich_sdg) == 0:
        failed_checks.append(
            FailedCheck(
                code=ErrorCode.MISSING_SDG_INFORMATION,
                params=[],
                message="Kein SDG Informationsbereich angegeben",
            )
        )

    for sdg_code in leistung.informationsbereich_sdg:
        if not _is_valid_sdg_code(sdg_code):
            failed_checks.append(
                FailedCheck(
                    code=ErrorCode.INVALID_SDG_CODE,
                    params=[],
                    message="Ungueltiger SDG Informationsbereich",
                )
            )


def _is_valid_sdg_code(value: str) -> bool:
    if len(value) != 7:
        return False

    try:
        int(value)
    except Exception:
        return False

    # TODO: Check if code is in list of all possible codes

    return True


def check_leistung(leistung: Leistung) -> list[FailedCheck]:
    failed_checks = []

    check_missing_modul(leistung, failed_checks)
    check_unique_text_modul(leistung, failed_checks)
    check_text_modul_missing_translation(leistung, failed_checks)
    check_text_modul_unique_language(leistung, failed_checks)
    check_leistungsschluessel(leistung, failed_checks)
    check_sdg_codes(leistung, failed_checks)

    return failed_checks


def _check_leistungen(
    leistungen: list[Leistung], source: str
) -> list[LeistungWithMetadata]:
    results: list[LeistungWithMetadata] = []

    for leistung in leistungen:
        failed_checks = check_leistung(leistung)

        kategorien = list(
            set(kategorie.get_german_bezeichnung() for kategorie in leistung.kategorie)
        )
        kategorieklassen = list(
            set(
                kategorie.klasse.get_german_bezeichnung()
                for kategorie in leistung.kategorie
            )
        )

        if leistung.versionsinformation:
            created_at = leistung.versionsinformation.erstellt_datum_zeit
            updated_at = leistung.versionsinformation.geaendert_datum_zeit
        else:
            created_at = None
            updated_at = None

        results.append(
            LeistungWithMetadata(
                leistung=leistung,
                source=source,
                kategorien=kategorien,
                kategorieklassen=kategorieklassen,
                created_at=created_at,
                updated_at=updated_at,
                failed_checks=failed_checks,
            )
        )

    return results


def print_status(
    failed_checks: list[FailedCheckWithMetadata], leistungen: list[LeistungWithMetadata]
):
    error_code_to_amount: dict[ErrorCode, int] = defaultdict(int)
    leistungen_with_failures: set[Identifikator] = set()
    for failed_check in failed_checks:
        error_code_to_amount[failed_check.check.code] += 1
        leistungen_with_failures.add(failed_check.leistung_id)

    print(
        f"Leistungen with failed checks: {len(leistungen_with_failures)} ({100 * len(leistungen_with_failures) / len(leistungen):.3f} %)"
    )
    print(f"Total failed checks: {len(failed_checks)}")
    for error_code, amount in error_code_to_amount.items():
        print(f"{error_code}: {amount}")


def _load_archive(archive_path: str) -> list[Leistung]:
    leistungen: list[Leistung] = []
    id_to_xml: dict[Identifikator, bytes] = {}

    with zipfile.ZipFile(archive_path, "r") as archive:
        for filename in archive.namelist():
            with archive.open(filename, "r") as file:
                xml_data = file.read()

            try:
                leistung = parse_leistung(xml_data)
            except XzufiException as error:
                print("Skip leistung: ", str(error))
            else:
                if leistung.id not in id_to_xml:
                    id_to_xml[leistung.id] = xml_data
                    leistungen.append(leistung)

    return leistungen


@click.command()
@click.argument("archive")
@click.argument("output_dir")
def check_leistungen(archive: str, output_dir: str):
    """
    Check all of the leistungen in the zip archive. Store the results in the specified output directory.
    """

    output_dir_path = Path(output_dir)
    output_dir_path.mkdir(exist_ok=True)

    leistungen = _load_archive(archive)
    print(f"Leistungen: {len(leistungen)}")

    leistungen_with_metadata = _check_leistungen(leistungen, source="pvog")

    # Save leistungen
    leistung_df_items = list(
        chain.from_iterable(
            (leistung.to_parquet_leistung() for leistung in leistungen_with_metadata)
        )
    )

    with open(output_dir_path / "leistungen.json", "wb") as file:
        file.write(orjson.dumps(leistung_df_items))

    df = pd.DataFrame(leistung_df_items)
    df.set_index("unique_id", inplace=True)
    print(df)
    df.to_csv(output_dir_path / "leistungen.csv")

    # Save errors
    error_df_items = list(
        chain.from_iterable(
            (leistung.to_parquet_errors() for leistung in leistungen_with_metadata)
        )
    )

    with open(output_dir_path / "errors.json", "wb") as file:
        file.write(orjson.dumps(error_df_items))

    df = pd.DataFrame(error_df_items)
    print(df)
    df.to_csv(output_dir_path / "errors.csv")


if __name__ == "__main__":
    check_leistungen()
