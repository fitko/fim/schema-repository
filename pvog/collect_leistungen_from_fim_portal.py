import asyncio
import zipfile
from pathlib import Path

import click
import httpx


async def _load_available_leistungen(
    client: httpx.AsyncClient, only_steckbriefe: bool
) -> list[tuple[str, str]]:
    base_url = "https://schema.fim.fitko.net/api/v0/xzufi-services?limit=200"
    if only_steckbriefe:
        base_url += "&hierarchy_type=steckbrief"

    entries = []
    has_more = True
    total_count = None
    while has_more:
        response = await client.get(f"{base_url}&offset={len(entries)}")
        response.raise_for_status()

        result = response.json()

        for entry in result["items"]:
            entries.append((entry["redaktion_id"], entry["id"]))

        total_count = result["total_count"]
        print(f"Indexed {len(entries)} / {total_count}")
        has_more = len(entries) < total_count

    return entries


async def _load_xzufi_content(
    client: httpx.AsyncClient, redaktion_id: str, leistung_id: str
) -> bytes:
    while True:
        response = await client.get(
            f"https://schema.fim.fitko.net/api/v0/xzufi-services/{redaktion_id}/{leistung_id}/xzufi"
        )

        if response.status_code == 429:
            # Too many requests. Wait 50ms, then try again.
            await asyncio.sleep(0.05)
        else:
            response.raise_for_status()
            return response.content


async def _import_leistungen(archive_path: str, only_steckbriefe: bool):
    async with httpx.AsyncClient() as client:
        available_leistungen = await _load_available_leistungen(
            client, only_steckbriefe=only_steckbriefe
        )

        Path(archive_path).parent.mkdir(exist_ok=True)
        with zipfile.ZipFile(archive_path, "w") as archive:
            for index, (redaktion_id, leistung_id) in enumerate(available_leistungen):
                content = await _load_xzufi_content(client, redaktion_id, leistung_id)

                filename = f"{redaktion_id}_{leistung_id}.xml"
                with archive.open(filename, "w") as file:
                    file.write(content)

                print(f"[{index + 1} / {len(available_leistungen)}] {filename}")


@click.command()
@click.argument("archive")
@click.option(
    "--steckbriefe",
    is_flag=True,
    show_default=True,
    default=False,
    help="Only import Steckbriefe.",
)
def collect_leistungen_from_fim_portal(archive: str, steckbriefe: bool):
    asyncio.run(_import_leistungen(archive, steckbriefe))


if __name__ == "__main__":
    collect_leistungen_from_fim_portal()
