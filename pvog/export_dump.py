import asyncio
from dataclasses import dataclass
import zipfile

import click

from fimportal import pvog


@dataclass
class PvogCredentials:
    base_url: str
    client_id: str
    client_secret: str


def _load_pvog_credentials() -> PvogCredentials:
    # TODO: Load your PVOG credentials here in which ever way is most suitable for you.

    raise NotImplementedError(
        "Loading the PVOG credentials has not been implemented yet."
    )


async def _export_dump(credentials: PvogCredentials, archive_path: str):
    client = pvog.HttpClient(
        base_url=credentials.base_url,
        client_id=credentials.client_id,
        client_secret=credentials.client_secret,
    )

    if zipfile.is_zipfile(archive_path):
        with zipfile.ZipFile(
            archive_path, "a", compression=zipfile.ZIP_LZMA
        ) as archive:
            last_transfer_message = sorted(archive.namelist())[-1]
            update_index = _extract_update_index(last_transfer_message)
            print(f"Restarting import at index {update_index}")

            await _sync_data(archive, client, update_index=update_index)
    else:
        with zipfile.ZipFile(
            archive_path, "w", compression=zipfile.ZIP_LZMA
        ) as archive:
            await _sync_data(
                archive,
                client,
                update_index=0,
            )


def _extract_update_index(filename: str) -> int:
    assert filename.endswith(".xml"), f"Invalid transfer message name: {filename}"
    return int(filename[:-4])


async def _sync_data(archive: zipfile.ZipFile, client: pvog.Client, update_index: int):
    while True:
        batch = await client.load_batch(update_index)
        if batch is None:
            return

        # Use the next index as the filename and insert "0" at the beginning to
        # recreate the correct file order when ordering by filename.
        filename = f"{batch.next_update_index}.xml".rjust(16, "0")
        assert len(filename) == 16, "next_update_index too large"

        with archive.open(filename, "w") as file:
            file.write(batch.xzufi_xml.encode("utf-8"))

        print(filename)
        update_index = batch.next_update_index


@click.command()
@click.argument("archive")
def export_dump(archive: str):
    """
    Export a full dump as a compressed zip archive.
    """
    credentials = _load_pvog_credentials()

    asyncio.run(_export_dump(credentials=credentials, archive_path=archive))


if __name__ == "__main__":
    # Just poor man's testing
    assert _extract_update_index("0000001234.xml") == 1234

    export_dump()
