from pathlib import Path
import os
import zipfile
import gzip

import click


@click.command()
@click.argument("input_dir")
@click.argument("output_archive")
def archive_dump(input_dir: str, output_archive: str):
    """
    Archive the current set of Leistungen into a single zip archive.
    """

    input_path = Path(input_dir)
    assert input_path.is_dir()

    filenames = os.listdir(input_path)

    with zipfile.ZipFile(output_archive, "w", compression=zipfile.ZIP_LZMA) as archive:
        for index, filename in enumerate(filenames):
            assert filename.endswith(".gz")
            with gzip.open(input_path / filename, "r") as file:
                archive.writestr(filename[:-3], data=file.read())

            print(f"Imported {filename[:-3]} [{index + 1} / {len(filenames)}]")


if __name__ == "__main__":
    archive_dump()
