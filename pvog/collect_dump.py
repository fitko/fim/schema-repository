from pathlib import Path
import os
import time
import zipfile
import gzip

import click

from fimportal import xml, xzufi
from fimportal.xzufi.transfer import (
    LoeschEvent,
    LoeschKlasse,
    SchreibEvent,
    SchreibKlasse,
)


def _process_events(document: xml.XmlDocument, output_dir: Path):
    for event in xzufi.iter_transfer_events(document.getroot()):
        match event:
            case SchreibEvent(klasse=SchreibKlasse.LEISTUNG, node=node):
                leistung = xzufi.parse_leistung(node)
                filename = _get_leistung_filename(leistung.id)

                with gzip.open(output_dir / filename, "wb") as file:
                    file.write(xml.serialize(node))
            case LoeschEvent(klasse=LoeschKlasse.LEISTUNG, id=id):
                filename = _get_leistung_filename(id)

                try:
                    os.remove(output_dir / filename)
                except FileNotFoundError:
                    pass
                else:
                    print(f"Removed {filename}")
            case _:
                # Ignore events for other resources
                pass


def _get_leistung_filename(identifikator: xzufi.Identifikator) -> str:
    assert identifikator.scheme_agency_id is not None

    return f"{identifikator.scheme_agency_id}_{identifikator.value}.xml.gz"


@click.command()
@click.argument("archive")
@click.argument("output_dir")
def collect_dump(archive: str, output_dir: str):
    """
    Collect the current set of Leistungen from a full PVOG dump.
    """

    output_path = Path(output_dir)
    output_path.mkdir(parents=True)

    with zipfile.ZipFile(archive, "r") as input_archive:
        filenames = input_archive.namelist()
        filenames.sort()

        for filename in filenames:
            start = time.time()

            with input_archive.open(filename) as file:
                document = xml.load_document(file)

            _process_events(document, output_path)

            end = time.time()
            print(f"Processed {filename}, [{int((end - start) * 1_000)} ms]")


if __name__ == "__main__":
    collect_dump()
