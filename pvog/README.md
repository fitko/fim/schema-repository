# DQM

## Installation with `sammelrepository` as a Dependency

```
# First, clone the repo somewhere
git clone git@gitlab.opencode.de:fitko/fim/schema-repository.git <path/to/sammelrepository>

# Optional: Set up a virtual env for your own repository
cd <your/own/repository>
python -m venv .venv
source ./.venv/bin/activate

# Then, just install the project with your favorite package manager.
# This step maybe needs [poetry](https://python-poetry.org/) installed.
# This step will also install all dependencies of the sammelrepository.
pip install <path/to/sammelrepository>
```

You should now be able to import the `sammelrepository` dependency in yout own scripts.

## Data Collection

```
python pvog/collect_leistungen_from_fim_portal.py ./data/leistungen.zip
```

## Checks

The zip archive created in the previous step can be analyzed via:

```
python pvog/check_leistungen.py ./data/leistungen.zip ./data/results/
```
