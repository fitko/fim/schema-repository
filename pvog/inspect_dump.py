import time
import zipfile

import click

from fimportal import xml, xzufi
from fimportal.xzufi.transfer import (
    LoeschEvent,
    LoeschKlasse,
    SchreibEvent,
    SchreibKlasse,
)


def _process_events(document: xml.XmlDocument):
    for event in xzufi.iter_transfer_events(document.getroot()):
        match event:
            case SchreibEvent(klasse=SchreibKlasse.LEISTUNG):
                pass
            case LoeschEvent(klasse=LoeschKlasse.LEISTUNG):
                pass
            case SchreibEvent(klasse=SchreibKlasse.ORGANISATIONSEINHEIT, node=node):
                organisationseinheit = xzufi.OrganisationseinheitEncoding.parse(node)
                print(organisationseinheit)
            case _:
                pass


@click.command()
@click.argument("archive")
def inspect_dump(archive: str):
    with zipfile.ZipFile(archive, "r") as input_archive:
        filenames = input_archive.namelist()
        filenames.sort()

        for filename in filenames:
            start = time.time()

            with input_archive.open(filename) as file:
                document = xml.load_document(file)

            _process_events(document)

            end = time.time()
            print(f"Processed {filename}, [{int((end - start) * 1_000)} ms]")

            return


if __name__ == "__main__":
    inspect_dump()
