# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/).

## [Unreleased]

## [v0.24.0]

### Added

- Add new endpoints to get XDF representations for _Datenfeldgruppen_ and _Datenfelder_.
- Include download url for immutable resources directly in the API responses.


## [v0.23.0]

### Added

- Export up-to-date xdf representations of a _Schema_ to include all of the latest changes.

## [v0.22.0]

### Updated

- Improve full-text-search performance.
- Improve search UI.
- Make most attributes of _Schema_, _Datenfeld_, _Datenfeldgruppe_,
  _Regel_ and _Steckbrief_ immutable.
- _Prozessklassen_ and _Prozesse_ Import implemented.
- References between _Bausteinen_.

### Fixed

- Fix a minor bug where an empty `bezug` of _Datenfeldgruppe_ or _Datenfeld_
  was stored as array with the empty string.
- Fix _Schema_'s `bezug_components`. They did not
  contain the `bezug` of `struktur` of _Gruppen_.
- Fix a server error that would come when the search string contained something in quotes that ended with a backslash
  (`exa "mp\" le`).

## [v0.21.0]

### Added

- Allow uploads of elements from another _Nummernkreis_ when uploading a _Schema_.
- Add links to converters and checks to the _Arbeitsmittel_ page.

## [v0.20.0]

### Fixed

- Fix issue when parsing a _Leistung_ with multiple _Leistungskategorien_.
- Fix handling invalid dates when parsing a _Leistung_.

## [v0.19.0]

### Fixed

- Fix full-text-search performance.

## [v0.18.0]

### Updated

- Bump dependencies.

## [v0.17.3]

### Added

- More glossary links in the UI.

### Fixes

- Small UI fixes.

## [v0.17.1]

### Fixes

- Small UI fixes.

## [v0.17.0]

### Added

- Enable new landing page.

## [v0.16.0]

### Fixed

- Fix the display of _Begriffe im Kontext_ for _Leistungen_.
- Fix the display of _ModulText_ headers in the UI.
- Fix several minor naming issues.

## [v0.15.0]

### Added

- Add convencience Filters to BOB and KATE in the UI.
- Unify headers on the landing page and the search page.
- Add sort order for _Steckbriefe/Stammtexte_ of _Leistungen_ (last updated, alphabetically and by _Leistungsschlüssel_).
- Add highlighting of search results when searching only in _Leistungsbezeichnung I_ or _Leistungsbezeichnung II_.

## [v0.14.0]

### Fixed

- Correctly display the data source of a _Leistung_ in the UI.

### Changed

- Use more neutral UX hints to visualize empty modules in a _Leistung_.

## [v0.13.0]

### Added

- Add endpoint to validate an access token.

### Changed

- Update Design of the UI.

## [v0.12.0]

### Added

- Sort "_teckbriefe/Stammtexte_ and _Leistungsbeschreibungen_.
- Filter _Leistungsbeschreibungen_ by _Leistungsbezeichnung_ and _Leistungsbezeichnung_II_.

### Fixed

- Allow to process data without a version in our `xdf` tools (checks, converters, etc.).

## [v0.11.0]

### Fixed

- Correctly detect whether or not a _Leistung_ is a _Leistungsbeschreibung_.
- Re-enable the import of all "Steckbriefe".

## [v0.10.0]

### Added

- Add `order_by` parameter for `/api/v0/services` and `/api/v0/xzufi-services`.

### Fixed

- Correctly parse _SDG Informationsbereiche_
- Rename `leika` filter to `leistungsschluessel`.

## [v0.9.0]

### Fixed

- Temporarily remove most _Leistungen_ from the LeiKa API.

## [v0.8.0]

### Added

- In UI searches, for versioned artifacts, show only the latest version instead of all.
- Split Search into Bausteine

## [v0.7.0]

### Added

- Get data directly from the XZuFi _Redaktion_ and the PVOG.
- Import _Prozesse_ from the _Prozesse Sammelrepository_.

## [v0.6.0]

### Added

- Include all transitive child elements of a _Datenfelgruppe_ in `/api/v1/groups/<namespace>/<fim_id>/<fim_version>`.
- Visualize the full tree for _Datenschemata_ and _Datenfeldgruppen_.
- Visualize BPMN diagrams for _Prozesse_.
- Add search and detail endpoints for _Leistungen_ at `/api/v0/services` and `/api/v0/services/<leistungsschluessel>` respectively.
- Add references to _Leistungsbeschreibungen_ when loading details for a specific _Leistung_.
- Added upload functionality for xdf3 _Steckbriefe_.

### Changed

- Move endpoints for xzufi _Leistungen_ from `/api/v0/services` to `/api/v0/xzufi-services`.

### Fixed

- Include `nummernkreis` when returning a single _Datenfeld_ or _Datenfeldgruppe_.

## [v0.5.0]

### Added

- Show more attributes of _Datenschemata_, _Datenfelder_ and _Datenfeldgruppen_.
- Search _Leistungen_ grouped bei _Leistungsschlüssel_.

## [v0.4.0]

### Added

- Add additional Filters to the UI.

### Fixed

- Update the quality report when a new upload is made for an existing _Datenschema_.
- Fill in missing relations in the tree structure of _Datenschemata_.

## [v0.3.0]

### Added

- Import referenced external code lists to enable additional quality checks.

## [v0.2.0]

### Changed

- Make _Leistungen_ scoped to the source _Redaktion_: `/api/v0/services/<redaktion_id>/<leistung_id>`. This will allow imports from multiple _Redaktionen_
  in the future, as the primary ID of a _Leistung_ is only unique within a single _Redaktion_.

### Fixed

- Update `last_update` of a _Stammdatenschema_ after changes to the derived data (JSON Schema Files, XSD Files, Quality Checks).

[unreleased]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.24.0...main
[v0.24.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.23.0...v0.24.0
[v0.23.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.22.0...v0.23.0
[v0.22.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.21.0...v0.22.0
[v0.21.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.20.0...v0.21.0
[v0.20.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.19.0...v0.20.0
[v0.19.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.18.0...v0.19.0
[v0.18.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.17.3...v0.18.0
[v0.17.3]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.17.1...v0.17.3
[v0.17.1]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.17.0...v0.17.1
[v0.17.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.16.0...v0.17.0
[v0.16.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.15.0...v0.16.0
[v0.15.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.14.0...v0.15.0
[v0.14.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.13.0...v0.14.0
[v0.13.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.12.0...v0.13.0
[v0.12.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.11.0...v0.12.0
[v0.11.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.10.0...v0.11.0
[v0.10.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.9.0...v0.10.0
[v0.9.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.8.0...v0.9.0
[v0.8.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.7.0...v0.8.0
[v0.7.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.6.0...v0.7.0
[v0.6.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.5.0...v0.6.0
[v0.5.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.4.0...v0.5.0
[v0.4.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.3.0...v0.4.0
[v0.3.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.2.0...v0.3.0
[v0.2.0]: https://gitlab.opencode.de/fitko/fim/portal/-/compare/v0.1.0...v0.2.0
