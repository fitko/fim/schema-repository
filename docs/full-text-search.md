# Full Text Search

To allow for intuitive searching of the documents in our database, we support
full-text-search (fts) for many types of resources.
Since we already have all of the data in our
PostgreSQL database, we utilize the [built-in fts-capabilities](https://www.postgresql.org/docs/current/textsearch.html)
for this feature.


###### Important Note

The current implementation results in very fast fts-searches (usually <1 ms).
This is both due to efficient indexing and the relatively small amount of data overall.
What is however costly is the computation of the fragments of the text in which the search term has a match, 
which is used to display the search results in the UI.

Out initial naive implementation works well for small documents, but breaks down quickly as soon
as the documents grow to a few 100kb in size.
For this reason, we implemented several strategies to keep the size of the documents as small as possible:

1. Strip all of the XML tags from the data, as those are ignored by the fts-preprocessing in any case.
2. For _Datenschemata_: Normalize the document by only including each group/field/rule 
once instead of representing the full nested tree with a lot of duplicates.
Also, only include the relevant information for the search.

## Table Structure

Each table that supports full text search has the following setup:

```sql
-- Assuming the table is called 'schema'
ALTER TABLE schema ADD COLUMN raw_content TEXT NOT NULL;
ALTER TABLE schema ADD COLUMN raw_content_fts TSVECTOR NOT NULL GENERATED ALWAYS AS (to_tsvector('german', raw_content)) STORED;

CREATE INDEX idx_schema_fts ON schema USIN GIN (raw_content_fts);
```

Then you can search for a term in the `raw_content` column like this:

```sql
SELECT ts_headline('german', raw_content, to_tsquery('german', $1))
FROM schema
WHERE raw_content_fts @@ to_tsquery('german', $1);
```

This will perform an efficient full-text-search on the `raw_content` column
and return the list of matched fragments.
When actually performing this query, we also have additional configuration when for `ts_headling`, see `database.py` for details.

## Strategies to fill the `raw_content` column

We have implemented several strategies to fill the `raw_content` column with the relevant data for the search.
Where we only want to strip the XML tags, this processing is done directly in SQL:

```sql
-- Actually strip the xml tags
CREATE FUNCTION xzufi_xml_to_raw_content(xml_data TEXT) RETURNS TEXT AS $$
	DECLARE
		content text;
		ns_array text[][];
BEGIN
	ns_array := ARRAY[ARRAY['xzufi', 'http://xoev.de/schemata/xzufi/2_2_0']];
	content := array_to_string(xpath('//text()', xmlparse(DOCUMENT xml_data), ns_array), ' ');
    RETURN content;
END;
$$ LANGUAGE plpgsql;

-- Trigger logic to update the raw_content column
CREATE FUNCTION leistung_raw_content_trigger() RETURNS trigger AS $$
DECLARE
BEGIN 
  NEW.raw_content := xzufi_xml_to_raw_content(NEW.xml_content);
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- Create the trigger
CREATE TRIGGER trg_leistung_raw_content BEFORE INSERT OR UPDATE ON leistung FOR EACH ROW EXECUTE FUNCTION leistung_raw_content_trigger();
```

We have two special cases for this:

- For _Prozesse_: The XML content also includes large, base64-encoded files.
These are stripped in the application code.
The insertion of the raw_content is then done explicitly in the INSERT statement instead of with the trigger.
The rest of the setup is identical.
- For _Datenschemata_: Since the manually create a search document, the raw_content is created in the application code and passed to the INSERT statement directly.
There is not additional SQL processing required.

