# FIM Portal search

This is a brief comparison of the search of the current [FIM-Portal](fimportal.de) and the planned search of the new FIM Portal.

## Old FIM Portal

- Import of `json` objects from the "Redaktionssysteme", which contain information about a "Stammdatenschema", a "Leistung", or a "Prozess". The actual data in the `json` object only contains
a small subset of the actual underlying documents. (At least for "Stammdatenschema". We do not have access to the "Redaktionssysteme" for "Leistung" and "Prozesse" and therefore cannot make a statement for these documents.
We know however, that the structure of the `json` document is identical, as this allows to search all resources at once, so there is likely also a lot of data missing in those `json` objects.)
- Save all `json` objects in ElasticSearch. (The actual documents are also saved, but AFAIK only for download purposes and are not used in the search.)
- All search queries search through the list of all the `json` objects. There are some filters (e.g. for type of the result, i.e. "Stammdatenschema", "Leistung", and/or "Prozess") and a "full-text-search" string.
"Full-text-search" is deceiving here, as this does not search through the "full text" of the documents, but only through the `json` documents.

## New FIM Portal (WIP)

- Import of the original documents from the "Redaktionssysteme". Extraction of necessary attributes for specialized filters (e.g. "FreigabeStatus") and sub-ressources (e.g. "Datenfelder" and "Datenfeldgruppen" from "Stammdatenschemata").
- Save all original documents as well as the extracted attributes into PostgreSQL. Each resource type ("Stammdatenschema", "Datenfeldgruppe", "Datenfeld", "Leistung", "Prozess", "Steckbrief") has its own table. This allows to not restrain the filters to the
lowest common denominator, but have maximum introspection into each resource type.
- Search through each resource individually, which allows resource-specific filters (e.g. "Leistungstypisierung"). The full-text-search searches through the full text of the document. The current implementation (WIP) just tests whether the string
is contained in the resource text. We are however currently working at a full-text-search index to both speed-up the search and make the results more accurate and intuitive. PostgreSQL has a [well-documented](https://www.postgresql.org/docs/current/textsearch.html) index-type for this,
which includes expected features like "fuzzy-search", configurable priority of different fields (e.g. a match in the title should be more important than a match in the description), and automatic parsing of XML documents to properly detect and handle text within XML nodes.

#### Why not ElasticSearch?

- Since we extract potentially multiple resources from documents (e.g. "Datenfelder" from "Stammdatenschemata"), and these (sub-)resources have links to each other even between documents, we want strong consistency-guarantees to avoid race-conditions 
and not accidentally show inconsistent or partial states in case of an error during import.
We therefore definitely want a basic, general-purpose database at the core of the application.
By using the built-in features for full-text-search from PostgreSQL, we avoid adding more infrastructure and the need to hold an additional database in sync with our main data store.
Unless proven otherwise, we consider the built-in full-text-search as good-enough for this application and therefore see this architectural decision as a good trade between simplicity and performance.

