# Integration with Sammelrepository for xProzesse

The integration with the sammelrepository for xProzesse is documented [here](https://www.xrepository.de/api/xrepository/urn:xoev-de:mv:em:standard:xprozess:dokumentation:Handlungsanweisung_zum_Sammelrepository_des_FIM-Bausteins_Prozesse).

One needs to generate a certificate which can be simplified by using the config file in this directory. Follow the instructions [here](https://www.df.eu/de/support/df-faq/ssl-zertifikate/externe-einbindung/csr-erstellen/#accordion-23228) and generate a private key.

## Test

```
openssl genrsa -out sammelrepo-prozesse-test.key 2048
openssl req -new -key sammelrepo-prozesse-test.key -out sammelrepo-prozesse-test.csr.datei -config prozesse-zertifikat-config-test.cnf
```

## Prod

```
openssl genrsa -out sammelrepo-prozesse-prod.key 2048
openssl req -new -key sammelrepo-prozesse-prod.key -out sammelrepo-prozesse-prod.csr.datei -config prozesse-zertifikat-config-prod.cnf
```
