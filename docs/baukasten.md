# Baukasten

To allow searching through the set of available fields and groups, our server must save each field and group into our database
when importing a new schema.

during an import, the schema and all elements (fields, groups) that the caller has write-access to (see `strict` options below) are saved.
Additionaly, the complete tree structure of the original schema is saved into the database.

We support two import modes, _strict_ and _legacy_.

### _Strict_ Mode

In strict mode, the caller only has write-access to the elements within the same Nummernkreis as the schema itself.
All elements from another Nummernkreis included in the schema and that already exist in our database will not be updated.
If an element from another Nummernkreis is not currently present in our database, then the import fails.
This mode is always used by our API and will therefore be the only option for importing new schemas.

### _Legacy_ Mode

In legacy mode, elements from another Nummernkreis are never updated, but created if necessary.
As the existing xdf2 schemas already contain references to elements with a differnet Nummernkreis, this is necessary for the initial
import of the existing data.

## Namespace

The special Nummernkreis `99` can be used to mark an element as _document-local_.
This means that even if two schemas both contain a field with an identical ID with Nummernkreis `99`,
then these fields should nevertheless not be considered identical.

To represent this behaviour in the FIM-Portal, each element has an additional _namespace_ besides the usual ID and version.
By default, each element is saved to the global namespace `baukasten`.
If, however, an element is document-local, then the namespace is the ID of the including schema instead.

For example, if the schema `S00001` includes the two fields `F00001V1.0` and `F99001V1.0`, then the following fields
would be available from our API afterwards:

`GET /api/v1/fields/baukasten/F00001/1.0/
GET /api/v1/fields/S00001/F99001/1.0/`
