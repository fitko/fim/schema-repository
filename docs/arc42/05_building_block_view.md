# Building Block View

```mermaid
graph LR
  subgraph ENTRY["ENTRY POINTS"]
    http
    cli
  end

  subgraph BUSINESS
    service.py
  end

  subgraph DATA
    database.py
  end

  subgraph util["STANDALONE MODULES"]
    xdf
    xzufi
    zprozess
    xrepository
    genericode
    pvog
  end

  http-->service.py
  cli-->service.py

  postgres[(PostgreSQL)]

  service.py-->database.py
  database.py<==>postgres
```
