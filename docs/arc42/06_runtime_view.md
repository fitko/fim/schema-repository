# Runtime View

The application consists of three independent services:

- FIM-Portal API: The web and api server
- Import Worker: Regularly imports all data from the configured external repositories
- Update Worker: Updates generated files like JSON Schema or XSD after each deployment

All of the services operate on the same linux machine.
There is no direct communication between the three services, instead they integrate by
accessing the same database.
This way, changes made by the Import or Update Worker are immediately reflected in the
API.

Please see [server-config.md](../server-config.md) for a detailed description of how to setup
those services in a production context.
