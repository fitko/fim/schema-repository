# Cross-cutting Concepts
## Logging
implemented:
- none

requirements:
- API calls to all end points
- number of warnings per file that was subject to the qa service
- upload events with data on the source "Nummernkreis"
- download events with data on the source tool

## Business Data Model
```mermaid
classDiagram
    Dokumentsteckbrief "1" --> "1..*" Datenschema
    Datenschema "1" --> "*" Codeliste
    Datenschema "1" --> "*" Datenfeldgruppe
    Datenschema "1" --> "*" Datenfeld
    Datenfeldgruppe "1" --> "*" Datenfeld
```
The business data model reflects the XDF standards.