# System Scope and Context

## Business Context

**\<Diagram or Table>**

**\<optionally: Explanation of external domain interfaces>**

## Technical Context

### Data Import

```mermaid
graph LR
  subgraph LEISTUNGEN
    PVOG
    leika["Baustein Leistungen"]
  end

  subgraph DATENFELDER
    fred_c["FRED Classic (xdf 2.0)"]
    fred_3["FRED 3 (xdf 3.0.0)"]
  end

  subgraph PROZESSE
    proc["Baustein Prozesse"]
  end

  subgraph fimportal["FIM PORTAL"]
    importers
    api

    service["Internal Service"]

    postgres[(PostgreSQL)]

    importers-->service
    api<-->service
    service<-->postgres
  end

  PVOG-- "Pull" -->importers
  leika-- "Pull" -->importers

  fred_c-- "Pull" -->importers
  fred_3-- "Push" -->api

  proc-- "Pull" -->importers
```

