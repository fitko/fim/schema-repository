# Create Backups

The FIM-Portal is hosted at [OVH](https://www.ovhcloud.com/) which provides an [automated backup service for virtual private servers](<[Title](https://www.ovhcloud.com/de/vps/vps-backup/)>).

# Restore Backups

The FIM-Portal service connects to a postgresql database hosted by [OVH](https://www.ovhcloud.com/). One can find a list of all database instances in the [management console](https://www.ovh.com/manager/#/web/private_database) of OVH.

To see the available backups for a specific database of an instance follow the link above and choose

```
<service_name> => Databases => <database> => three-dot-menu => Show backups
```

For most databases there is a backup created on a daily basis. Each of these backups is available to restore a certain state of the database. Older backups are removed. One can find the option `restore backup` for each individual backup in the three-dot-menu. However, as of now (February 2024) OVH doesn't offer this for postgresql databases.

![Backup Screenshot](./backup-fitko.png)

Therefore restoring a backup is a two-way-process. First download the backup to restore. For that choose

```
<backup> => three-dot-menu => Download the backup
```

You will download the backup to your local machine. Afterwards you import this to the database. Choose

```
Databases => <database> => three-dot-menu => Import file
```

A separate dialogue opens to upload the import file.

```
 Import a new file => Choose file => Send => Next => Tick `Empty the current database` & choose whether an email should be sent => Confirm
```

The database is emptied so that during the import no SQL constraints are violated.

**Be aware that this leads to a short downtime since the running service needs to re-establish the database connection. As of now (February 2024) you can expect about 2-3 minutes.**
