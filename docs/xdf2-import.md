# `xdf2` Import

All of the created `xdf2` files are located in decentralized repositories.
Some of those repositories use the `FRED` editor, which provides an interface to import all
available schemas. We use this interface to fill our database with the available schemas.

The import must run in several steps:

1. Load an initial `html` page containing a list of all the available files of the type "Stammdatenschema".
2. For each "Datenschema", load a custom `json` object containing links to the schema itself and the code lists used by the schema as well as additional metadata. 
All of those files are then loaded and imported. In addition, the field `freigabe_status` is not part of `xdf2` and is therefore also imported from the `json` file.

