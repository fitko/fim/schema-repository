# Deployment View

```mermaid
graph LR
  subgraph PIPELINE
    test -.-> build -.-> deploy
  end

  cms("headless CMS")

  subgraph ARTEFACTS
    wheel["Python package (wheel)"]
    assets["preprocessed and validated CMS assets
    (data, images, videos, ...)"]
  end

  subgraph ENVIRONMENTS
    STAGE
    PROD
  end

  build ==> ARTEFACTS
  ARTEFACTS ==> deploy
  cms ==> build

  deploy-- on push to main -->STAGE
  deploy-- on release -->PROD
```

The project repository is hosted on the [OpenCode](https://opencode.de) Gitlab instance.
Our deployment is based on [Gitlab CI/CD Pipelines](https://docs.gitlab.com/ee/ci/pipelines/).

We currently have two separate deployment environments, **stage** and **production**.
The [environments](https://gitlab.opencode.de/fitko/fim/schema-repository/-/environments) subpage of the OpenCode repository
can be checked to easily determine which exact commit is currently running on each of our environments
(Note: This is only visibile to maintainers of the project).
 
## Stage Environment

Our stage environment is currently deployed on https://uber.space as a temporary solution.

After merging code onto our **main** branch, the CI pipeline will automatically deploy the code to our stage
environment upon successful completion of our tests.

## Production Environment

Our production environment is hosted on [OVHcloud](https://www.ovhcloud.com/de/).

Deployment to our production environment is only started after manually creating a new 
[release](https://docs.gitlab.com/ee/user/project/releases/) on OpenCode.
This starts the production release pipeline, which first re-tests the application against the full test suite
before updating the production environment.

### Preparing A New Release

- Update the file `CHANGELOG.md` with the tag of the coming release
- Update the version in `./sammelrepository/main.py`, which will be included in the generated openapi specs

### Creating A New Release

Creating a new release on OpenCode creates a new tag for the selected branch.
The release pipeline expects the tag to be a [protected tag](https://docs.gitlab.com/ee/user/project/protected_tags.html),
as only protected tags have access to our deployment secrets.
Additionaly, only maintainers of the project can create a protected tag and therefore successfully deploy to production.

A protected tag is any tag, that starts with `v`. For example, `v1.0.5` or `v2023.12.24` would both be protected tags and
start a deployment upon creation.
The tags `1.0.5` or `2023.12.24` would however fail to deploy.
