# Server Config

```mermaid
graph LR

  internet

  subgraph SERVER
    nginx

    assets["Static Assets"]
    api["API + Website"]

    subgraph SERVICES
      leistungen["Import Leistungen"]
      datenfelder["Import Datenfelder"]
      prozesse["Import Prozesse"]
      kataloge["Import Kataloge"]
      gebiete["Import Gebiete"]
      updater["Internal Updater"]
    end
  end

  postgres[(PostgreSQL)]

  internet-->nginx
  nginx-->assets
  nginx-->api
  api-->postgres
  SERVICES-->postgres
```

The application consists of several [systemd](https://systemd.io/) services, which is the
default system and service manager in many popular linux distributions (e.g. Ubuntu and Debian):

- A central web and api server
- Several import services for regular data imports
- An update service for generated files
- A link check service for uptime checks

The actual config and data for the services are placed in the working directory
`/home/debian/fim_portal` of the services (see `README.md` for a description of those).

The debian user is created automatically by the OVH VPS setup and should always be used instead of the root user
when configuring/updating the server.

The services itself will use the [DynamicUser](https://www.freedesktop.org/software/systemd/man/latest/systemd.exec.html#DynamicUser=)
option of systemd to minimize service permissions.

## OVH Setup

Our setup (both STAGE and PRODUCTION) consists only of a single app server and a managed PostgreSQL database respectively.
Use the following guides to set up the resources:

- VPS: https://help.ovhcloud.com/csm/de-vps-getting-started?id=kb_article_view&sysparm_article=KB0035078
- PostgreSQL: https://help.ovhcloud.com/csm/de-public-cloud-databases-postgresql-configure-instance?id=kb_article_view&sysparm_article=KB0049383

We currently use debian 12 as our server operating system.

## Server Setup

All following instructions are meant to setup the VPS created in the step before. This part also assumes, that a PostgreSQL instance
is accessible from the VPS (as described in the PostgreSQL guide above).

### Disable root login

First, ensure that root and password ssh logins are disabled by setting the following line in `/etc/ssh/sshd_config` if not already
present:

```
PermitRootLogin no
PasswordAuthentication no
```

Then, restart the ssh daemon via `sudo systemctl restart sshd`.

### Install Dependencies

Install the necessary dependencies for the server:

```bash
sudo apt update

# Install the necessary packages
sudo apt install python3.11 python3.11-venv nodejs nginx certbot python3-certbot-nginx jq postgresql-client
```

### Project Directory

All of the code and config for the project will live in `/home/debian/fim_portal/`.

```bash
cd /home/debian
mkdir fim_portal
cd ./fim_portal
```

This user will be used to run all of the fim portal services (see below).

We use a dedicated Python virtual environment to avoid conflicts with other parts of the system:

```bash
python3.11 -m venv ENV_3_11
```

### FIM-Portal Config

Since the config contains sensitive information, it is only stored encrypted on the server 
and provided to the services via [systemd credentials](https://systemd.io/CREDENTIALS/).

Once the config file is created (see the `README` for a description of the content), the file must be encrypted and moved
to the correct location:

```bash
# Encrypt the config file
sudo systemd-creds encrypt --name=config <location_of_unencrypted_config> /etc/fimportal.config.json.cred

# Completely remove the unencrypted file
shred -u <location_of_unencrypted_config>
```

IMPORTANT: The config file must be named `config`, as this name will be verified by the systemd
when loading the credentials.
The unit files are configured so that the final config will be available under `$CREDENTIALS_DIRECTORY/config`
which is the recommended behavior described in the linked systemd credentials docs above.

#### Update config

If the config needs to be updated, the following steps should be taken:

```bash
# Decrypt the config file
sudo systemd-creds decrypt --name=config /etc/fimportal.config.json.cred <location_of_unencrypted_config>

# Update the file
sudo vim <location_of_unencrypted_config>

# Encrypt the file again
sudo systemd-creds encrypt --name=config <location_of_unencrypted_config> /etc/fimportal.config.json.cred

# Remove the unencrypted file
sudo shred -u <location_of_unencrypted_config>
```

### Nginx & Certbot

We use [nginx](https://nginx.org/en/docs/) as reverse proxy and for automatic HTTPs termination. 
In addition we use [certbot](https://certbot.eff.org/) for automatic certificate renewel. 
The nginx config can be found in the nginx directory of the server, meaning at

```
/etc/nginx/nginx.conf
```

The configurations for stage and production are part of the repository and can be found 
at `config/stage.nginx.conf` and `config/prod.nginx.conf` respectively.

### Systemd Units

We manage the necessary services of the FIM Portal (app server, regular imports, etc.) with systemd.
Systemd can manage several types of units, e.g. services or timers.
The base for this configuration is copied from the [gunicorn deployment guide](https://docs.gunicorn.org/en/stable/deploy.html#systemd)
for systemd.
All necessary systemd units are part of the repository and can be found in the `config/systemd` directory.
These files should be copied to `/etc/systemd/system` on the server.

The communication between nginx and our server is done via a systemd-managed unix socket.
This way, any incomming requests are buffered by the OS during restarts of our server.
This both reduces the impact of outages as well as enables zero-downtime deployments.

### Enable/Start Services

To enable autostart on boot (e.g. after a system update or some unexpected system failure)
for the server, the socket, and the import timer, execute:

```bash
# First, reload the systemd daemon to read all of the changed configs
sudo systemctl daemon-reload

sudo systemctl enable fim_portal_app.socket
sudo systemctl enable fim_portal_app.service
sudo systemctl enable fim_portal_datenfelder_import.timer
sudo systemctl enable fim_portal_leistungen_import.timer
sudo systemctl enable fim_portal_prozesse_import.timer
sudo systemctl enable fim_portal_gebiet_import.timer
sudo systemctl enable fim_portal_kataloge_import.timer
sudo systemctl enable fim_portal_link_check.timer
```

The update and import services should not be enabled directly, as those should
not automatically start when the system reboots.

## Logging

### Application Log Config

The application must be configured to log in a format that our logging stack can understand.
The current configuration can be found in the repository under `/config/log_config.json`.
This file must be copied to `/home/debian/fim_portal/log_config.json` on the server, where it will
be loaded from all systemd services. The same config is valid for both the gunicorn server
and our custom cli commands.

### Filebeat

We use filebeat to forward all logs to the FITKO ELK stack. Just follow the installation instructions in the official
[setup docs](https://www.elastic.co/guide/en/beats/filebeat/7.17/filebeat-installation-configuration.html).
The credentials should be configured using the filebeat [keystore](https://www.elastic.co/guide/en/beats/filebeat/7.17/keystore.html).
The actual url of the elasticsearch host as well as the credentials can be obtained from the FITKO.

The config can be found in the repository under `/config/filebeat.yml`.
The file should be copied to `/etc/filebeat/filebeat.yml` on the server.

```
scp config/filebeat.yml debian@<server-ip>:/tmp/filebeat.yml
sudo chown root:root /tmp/filebeat.yml
sudo chmod 600 /tmp/filebeat.yml
sudo mv /tmp/filebeat.yml /etc/filebeat/filebeat.yml
```

After configuring filebeat, start and enable the corresponding systemd service:

```
sudo systemctl start filebeat.service
sudo systemctl enable filebeat.service
```

## Basic Deployment Routine

A deployment should always follow these steps:

1. Stop all three systemd services and the timer
2. Upload and install the new version into the virtual environment
3. Execute all pending migrations (`python -m fimportal migrate`)
4. Start the server and the updater, start the timer

## Helpful Commands

Some helpful commands, which can be executed when accessing the server via `ssh`:

```
# See the status of a unit
sudo systemctl status fim_portal_app.service

# See the last log entries of a specific unit.
sudo journalctl -u fim_portal_app.service

# Listen to any new logs of a specific unit.
sudo journalctl -u fim_portal_app.service -f

# Reload systemd after a unit configuration has been changed or added.
sudo systemctl daemon-reload

# Execute the FIM Portal CLI within the virtual env,
# e.g. for executing the migrations manually.
/home/debian/fim_portal/ENV_3_11/bin/python -m fimportal

# Run a one-off command with access to the credentials
sudo systemd-run \
    -p LoadCredentialEncrypted=config:/etc/fimportal.config.json.cred \
    --wait -P \
    /home/debian/fim_portal/ENV_3_11/bin/python -m fimportal <command>

# Connect to the database
sudo systemd-run -P --wait -p LoadCredentialEncrypted=config:/etc/fimportal.config.json.cred systemd-creds cat config \
    | jq .database_url \
    | xargs -I {} -o psql postgres://{}
```


