# Integration with JSON API for Prozesse

The integration with the JSON API for xProzesse is documented [here](https://www.xrepository.de/api/xrepository/urn:xoev-de:mv:em:standard:xprozess:dokumentation:Handlungsanweisung_zum_Sammelrepository_des_FIM-Bausteins_Prozesse). However under the section `CN` (i.e. `commonName`)use

```
FKT:FIM-Export-Client <organisation name>
```

One needs to generate a certificate which can be simplified by using the config file in this directory. Follow the instructions [here](https://www.df.eu/de/support/df-faq/ssl-zertifikate/externe-einbindung/csr-erstellen/#accordion-23228) and generate a private key. Execute the following commands in this directory.

```
openssl genrsa -out json-schnittstelle-zertifikat.key 2048
```

Afterwards use the certificate config and the generated key to create the certificate signing request (CSR).

```
openssl req -new -key json-schnittstelle-zertifikat.key -out json-schnittstelle-zertifikat.csr.datei -config json-schnittstelle-zertifikat-config.cnf
```
