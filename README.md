# FIM Portal

![pipeline](https://gitlab.opencode.de/fitko/fim/portal/badges/main/pipeline.svg)

The project is implemented as a [FastAPI](https://fastapi.tiangolo.com/) application backed by a
[PostgreSQL](https://www.postgresql.org/) database.
The server provides both a `json` Api and a [Web UI](https://neu.fimportal.de).

- [User Documentation](https://docs.fitko.de/fim), targeting decision makers and developers tasked with employing the API for their respective systems.
- [OpenAPI documentation](https://neu.fimportal.de/docs), targeting developers and all parties interested in learning more about existing data.
- [Architectural Documentation](https://gitlab.opencode.de/fitko/fim/portal/-/tree/main/docs), targeting architects interested in understanding the structure of our solution and the decisions that were made during the development process.
- Developer Documentation as part of the source code, targeting future developers tasked with servicing and extending the existing solution.


## Contact

To contact the project team you may add an issue to our backlog by sending an
[email](mailto:git+fitko-fim-portal-1010-issue-@opencode.de).

## Technical Overview


### Frontend

The application is a dynamically, server-side rendered web app.
It is implemented using jinja2 templates and vanilla javascript where necessary.
The endpoints are served by the FastAPI server.

Some parts of the frontend are dynamically generated based on data from a headless CMS, [Cockpit](https://getcockpit.com/).
All necessary data is fetched from the CMS as part of the deployment pipeline and then simply served from memory by the server.


### Backend

This is an overview over the implemented modules (see `./fim_portal`).

- `service`: All high-level features of the project. Called either from the server or by the CLI.
- `commands`: CLI commands.
- `xdatenfelder`: Data structures and parsers to work with `xdf2` and eventually `xdf3`.
- `genericode`: Data structures and parsers to work with genericode code lists.
- `din91379`: String.Latin data types.
- `xml`: Some helpers for efficient and **secure**
  (see [why](https://docs.python.org/3/library/xml.html#xml-vulnerabilities)) `XML` handling.
- `json_schema`: Conversion from `xdf2` to JSON Schema.
- `migrations`: Database migrations.
- `database`: All database interactions.
- `crawler`: Module to import data from certain external repositories.
- `dependencies`, `routers`, `main`: FastAPI server setup.

The complete SQL schema is dumped at `./schema.sql` for easy reference of the full database structure.
The dump can be updated via `make dump-sql-schema`. This command assumes that postgresql is installed on the system, as it
uses [`pg_dump`](https://www.postgresql.org/docs/current/app-pgdump.html) internally.


## Installation

To run or test the app locally, the following dependencies must be installed first:

- [Python](https://www.python.org/) >= 3.10
- [poetry](https://python-poetry.org/) >= 1.4.2
- [docker](https://www.docker.com/)
- [node.js](https://nodejs.org/en) >= 18

Then, install the Python dependencies via Poetry:

```sh
poetry install

# You can now enter the virtualenv created by poetry
poetry shell
```

You should now be able to run all tests. This requires docker to be running in the background.

```sh
make test
```


## Running locally

First, create a config file `fimportal.config.json` in the root directory of the project. See a more detailed explanation below.
This is a minimal working example:

```json
{
  "environment": "local",
  "baseurl": "http://localhost:8000",
  "immutable_base_url": "http://localhost:8000",
  "database_url": "postgres:postgrespw@localhost:5432/test?sslmode=disable",
  "admin_password_hash": "$2b$12$AlZ6GMw07byv85DkNtzar.EsrgWhjSJbPbvKmzEexUp6Zgwxi/.x6",
  "cms": {
    "data_file": "./example_content.json"
  }
}
```

The `admin_password_hash` is the hash of the password `test`.
Use this password and the user `admin` to log in to the admin dashboard under `/admin`.

The `database_url` should point to a local database, which can
be created e.g. using [docker](https://hub.docker.com/_/postgres/). Assuming that docker is
installed locally one can run the following command to create a container that the above `database_url` works with.

```sh
docker run --name db_container -e POSTGRES_DB=test -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgrespw -d -p 5432:5432 postgres:15.7 postgres -c fsync=off
```

Then, initialize the database:

```sh
make reset-db
```

This command will do several things:

- Delete and then migrate the database (migrations located in `/fimportal/migrations`).
- Seed the database with test access tokens for the upload feature.
  A test token always has the form `test_<xdf3-nummernkreis>`. For example, the token
  `token_12000` can be used to upload xdf2 schemas to `Nummernkreis` 12
  and xdf3 schemas to `Nummernkreis` 12000.
- Import some local test files for each Baustein (Leistungen, Datenfelder, Prozesse).

You should now be able to start the server:

```sh
make run-local
```


## Configuration

The server and most of the cli commands expect a config file when started.
The location of the config file is determined by the environment variable `FIM_PORTAL_CONFIG_FILE`.
On the server, this file will be securely provided by the environment. For details, see `/docs/server-config.md`.
During development, this variable is set to `./fimportal.config.json` in the `Makefile`.

The environment must be one of `local`, `stage`, or `production`. Unless set to `local`, the api will enforce
a strict [content security policy](https://infosec.mozilla.org/guidelines/web_security#content-security-policy), effectively forcing
the browser to only use https. This is deactivated for `local` to not complicate development.

See `./fimportal/config.py` for all available configuration options.


## Using actual CMS Data

The minimal config file points to the `example_content.json` file, which is a static file that is part of the repository.
This contains some example data normally fetched from the CMS, which is useful for development and testing.
Before compiling the actual data from the CMS, the config file must be updated:

```json
{
  "cms": {
    "cockpit_token": "valid_cockpit_token",
    "data_file": "./public/cms_content.json",
    "assets_dir": "./public/assets",
    "content_assets_dir": "./public/content_assets",
    "cache_dir": "./assets_cache",
    "serve_assets": true
  }
}
```

This will both enable fetching the data from the CMS, and will activate the serving of the assets from the server.
The following command will compile the actual assets from the CMS:

```sh
make watch-cms-content
```

This command will watch for changes in the code and restart the update process automatically.
Since the assets are cached between runs, the automatic update process should be very fast.


## CLI

We have a custom cli to help with tasks during and after the deployment.

```sh
# list all available commands
python -m fimportal --help

# show the full description of a specific command
python -m fimportal <command> --help
```


## Tooling

We use the following tools to check our source code during CI:

- [ruff](https://github.com/charliermarsh/ruff) (formatting and linting)
- [pyright](https://github.com/microsoft/pyright) (type checking)

If you want to build the gitlab ci image locally, you can try out this tool [gitlab-ci-local][https://github.com/firecow/gitlab-ci-local].
After installing you call in your terminal simply from the root directory of the project:

```sh
gitlab-ci-local
```

## XSD Cache

We use several XSD files for data validation. To not depend on external dependencies every time the schema is loaded and to speed
up the start-up time, we cache the schemas locally. The cache can be updated via the following command:

```
python -m fimportal populate-local-xsd-cache
```

## Admin dashboard

The application includes an admin dashboard, which can be accessed via the `/admin/` route.

## Creating A New Deployment

Please have a look at our [deployment docs](./docs/arc42/07_deployment_view.md) to see how to create a new deployment.

## License

Source code is licensed under the [EUPL](licenses/EUPL-1.2.txt).
