--
-- PostgreSQL database dump
--

-- Dumped from database version 15.7 (Debian 15.7-1.pgdg120+1)
-- Dumped by pg_dump version 15.10 (Homebrew)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: call_update_latest_datenfeld(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.call_update_latest_datenfeld() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN

    PERFORM update_latest_datenfeld(NEW.namespace, NEW.fim_id);

    RETURN NEW;
END;
$$;


--
-- Name: call_update_latest_datenfeldgruppe(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.call_update_latest_datenfeldgruppe() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN

    PERFORM update_latest_datenfeldgruppe(NEW.namespace, NEW.fim_id);

    RETURN NEW;
END;
$$;


--
-- Name: call_update_latest_dokumentensteckbrief(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.call_update_latest_dokumentensteckbrief() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN

    PERFORM update_latest_dokumentensteckbrief(NEW.fim_id);

    RETURN NEW;
END;
$$;


--
-- Name: call_update_latest_schema(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.call_update_latest_schema() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN

    PERFORM update_latest_schema(NEW.fim_id);

    RETURN NEW;
END;
$$;


--
-- Name: leistung_raw_content_trigger(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.leistung_raw_content_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN 
  NEW.raw_content := xzufi_xml_to_raw_content(NEW.xml_content);
  RETURN NEW;
END;
$$;


--
-- Name: prozess_xml_to_raw_content(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.prozess_xml_to_raw_content(xml_data text) RETURNS text
    LANGUAGE plpgsql
    AS $$
	DECLARE
		content text;
		ns_array text[][];
BEGIN
	ns_array := ARRAY[ARRAY['xprozess', 'http://www.regierung-mv.de/xprozess/2']];
	content := array_to_string(xpath('//text()', xmlparse(DOCUMENT xml_data), ns_array), ' ');
    return content;
END;
$$;


--
-- Name: prozessklasse_xml_content_trigger(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.prozessklasse_xml_content_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN 
  NEW.raw_content := prozessklasse_xml_to_raw_content(NEW.xml_content);
  RETURN NEW;
END;
$$;


--
-- Name: prozessklasse_xml_to_raw_content(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.prozessklasse_xml_to_raw_content(xml_data text) RETURNS text
    LANGUAGE plpgsql
    AS $$
	DECLARE
		content text;
		ns_array text[][];
BEGIN
	ns_array := ARRAY[ARRAY['xprozess', 'http://www.regierung-mv.de/xprozess/2']];
	content := array_to_string(xpath('//text()', xmlparse(DOCUMENT xml_data), ns_array), ' ');
    RETURN content;
END;
$$;


--
-- Name: schema_element_raw_content_trigger(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.schema_element_raw_content_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN 
  NEW.raw_content := schema_element_xml_to_raw_content(NEW.xml_content);
  RETURN NEW;
END;
$$;


--
-- Name: schema_element_xml_to_raw_content(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.schema_element_xml_to_raw_content(xml_data text) RETURNS text
    LANGUAGE plpgsql
    AS $$
	DECLARE
		content text;
		ns_array text[][];
BEGIN
	ns_array := ARRAY[ARRAY['ns0', 'urn:xoev-de:fim:standard:xdatenfelder_2'], ARRAY['ns0', 'urn:xoev-de:fim:standard:xdatenfelder_3.0.0']];
	content := array_to_string(xpath('//text()', xmlparse(DOCUMENT xml_data), ns_array), ' ');
    RETURN content;
END;
$$;


--
-- Name: update_latest_datenfeld(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_latest_datenfeld(p_namespace text, p_fim_id text) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Set all rows for the same namespace and fim_id to FALSE
    UPDATE datenfeld
    SET is_latest = FALSE
    WHERE namespace = p_namespace 
        AND fim_id = p_fim_id;

    -- Set the row with the highest fim_version to TRUE
    UPDATE datenfeld
    SET is_latest = TRUE
    WHERE namespace = p_namespace
      AND fim_id = p_fim_id
      AND fim_version = (
          SELECT fim_version
          FROM datenfeld
          WHERE namespace = p_namespace
            AND fim_id = p_fim_id
          ORDER BY string_to_array(fim_version, '.')::int[] DESC
          LIMIT 1
      );
END;
$$;


--
-- Name: update_latest_datenfeldgruppe(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_latest_datenfeldgruppe(p_namespace text, p_fim_id text) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Set all rows for the same namespace and fim_id to FALSE
    UPDATE datenfeldgruppe
    SET is_latest = FALSE
    WHERE namespace = p_namespace 
        AND fim_id = p_fim_id;

    -- Set the row with the highest fim_version to TRUE
    UPDATE datenfeldgruppe
    SET is_latest = TRUE
    WHERE namespace = p_namespace
      AND fim_id = p_fim_id
      AND fim_version = (
          SELECT fim_version
          FROM datenfeldgruppe
          WHERE namespace = p_namespace
            AND fim_id = p_fim_id
          ORDER BY string_to_array(fim_version, '.')::int[] DESC
          LIMIT 1
      );
END;
$$;


--
-- Name: update_latest_dokumentensteckbrief(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_latest_dokumentensteckbrief(p_fim_id text) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Set all rows for the same fim_id to FALSE
    UPDATE dokumentensteckbrief
    SET is_latest = FALSE
    WHERE fim_id = p_fim_id;

    -- Set the row with the highest fim_version to TRUE
    UPDATE dokumentensteckbrief
    SET is_latest = TRUE
    WHERE fim_id = p_fim_id
      AND fim_version = (
          SELECT fim_version
          FROM dokumentensteckbrief
          WHERE fim_id = p_fim_id
          ORDER BY string_to_array(fim_version, '.')::int[] DESC
          LIMIT 1
      );
END;
$$;


--
-- Name: update_latest_schema(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_latest_schema(p_fim_id text) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Set all rows for the same fim_id to FALSE
    UPDATE schema
    SET is_latest = FALSE
    WHERE fim_id = p_fim_id;

    -- Set the row with the highest fim_version to TRUE
    UPDATE schema
    SET is_latest = TRUE
    WHERE fim_id = p_fim_id
      AND fim_version = (
          SELECT fim_version
          FROM schema
          WHERE fim_id = p_fim_id
          ORDER BY string_to_array(fim_version, '.')::int[] DESC
          LIMIT 1
      );
END;
$$;


--
-- Name: update_schema_content(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_schema_content() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    xml_content TEXT;
BEGIN
    UPDATE schema SET xml_content = NEW.content WHERE fim_id = NEW.schema_fim_id AND fim_version = NEW.schema_fim_version;
  RETURN NEW;
END;
$$;


--
-- Name: xzufi_xml_to_raw_content(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.xzufi_xml_to_raw_content(xml_data text) RETURNS text
    LANGUAGE plpgsql
    AS $$
	DECLARE
		content text;
		ns_array text[][];
BEGIN
	ns_array := ARRAY[ARRAY['xzufi', 'http://xoev.de/schemata/xzufi/2_2_0']];
	content := array_to_string(xpath('//text()', xmlparse(DOCUMENT xml_data), ns_array), ' ');
    RETURN content;
END;
$$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: access_token; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.access_token (
    id integer NOT NULL,
    token character varying NOT NULL,
    nummernkreis character varying NOT NULL,
    description character varying NOT NULL
);


--
-- Name: access_token_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.access_token_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: access_token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.access_token_id_seq OWNED BY public.access_token.id;


--
-- Name: admin_session; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.admin_session (
    token character varying NOT NULL,
    created_at timestamp with time zone NOT NULL
);


--
-- Name: code_list; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.code_list (
    id integer NOT NULL,
    genericode_canonical_version_uri character varying NOT NULL,
    genericode_canonical_uri character varying NOT NULL,
    genericode_version character varying,
    is_external boolean NOT NULL,
    content character varying,
    source character varying
);


--
-- Name: code_list_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.code_list_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: code_list_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.code_list_id_seq OWNED BY public.code_list.id;


--
-- Name: datenfeld; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.datenfeld (
    namespace character varying NOT NULL,
    fim_id character varying NOT NULL,
    fim_version character varying NOT NULL,
    nummernkreis character varying NOT NULL,
    xdf_version character varying NOT NULL,
    name character varying NOT NULL,
    beschreibung character varying,
    definition character varying,
    status_gesetzt_durch character varying,
    feldart character varying NOT NULL,
    datentyp character varying NOT NULL,
    bezug text[] NOT NULL,
    freigabe_status smallint NOT NULL,
    status_gesetzt_am date,
    gueltig_ab date,
    gueltig_bis date,
    versionshinweis character varying,
    veroeffentlichungsdatum date,
    letzte_aenderung timestamp with time zone NOT NULL,
    last_update timestamp with time zone NOT NULL,
    is_latest boolean DEFAULT false NOT NULL,
    xml_content character varying,
    raw_content text NOT NULL,
    immutable_json jsonb,
    immutable_hash text,
    raw_content_fts tsvector GENERATED ALWAYS AS (to_tsvector('german'::regconfig, raw_content)) STORED
);


--
-- Name: datenfeld_regel_relation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.datenfeld_regel_relation (
    datenfeld_namespace text NOT NULL,
    datenfeld_fim_id text NOT NULL,
    datenfeld_fim_version text NOT NULL,
    regel_fim_id text NOT NULL,
    regel_fim_version text NOT NULL
);


--
-- Name: datenfeld_relations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.datenfeld_relations (
    source_namespace text NOT NULL,
    source_fim_id text NOT NULL,
    source_fim_version text NOT NULL,
    target_fim_id text NOT NULL,
    target_fim_version text NOT NULL,
    praedikat text NOT NULL
);


--
-- Name: datenfeldgruppe; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.datenfeldgruppe (
    namespace character varying NOT NULL,
    fim_id character varying NOT NULL,
    fim_version character varying NOT NULL,
    nummernkreis character varying NOT NULL,
    xdf_version character varying NOT NULL,
    name character varying NOT NULL,
    beschreibung character varying,
    definition character varying,
    status_gesetzt_durch character varying,
    bezug text[] NOT NULL,
    freigabe_status smallint NOT NULL,
    status_gesetzt_am date,
    gueltig_ab date,
    gueltig_bis date,
    versionshinweis character varying,
    veroeffentlichungsdatum date,
    letzte_aenderung timestamp with time zone NOT NULL,
    last_update timestamp with time zone NOT NULL,
    xml_content character varying,
    is_latest boolean DEFAULT false NOT NULL,
    raw_content text NOT NULL,
    immutable_json jsonb,
    immutable_hash text,
    raw_content_fts tsvector GENERATED ALWAYS AS (to_tsvector('german'::regconfig, raw_content)) STORED
);


--
-- Name: datenfeldgruppe_regel_relation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.datenfeldgruppe_regel_relation (
    datenfeldgruppe_namespace text NOT NULL,
    datenfeldgruppe_fim_id text NOT NULL,
    datenfeldgruppe_fim_version text NOT NULL,
    regel_fim_id text NOT NULL,
    regel_fim_version text NOT NULL
);


--
-- Name: datenfeldgruppe_relations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.datenfeldgruppe_relations (
    source_namespace text NOT NULL,
    source_fim_id text NOT NULL,
    source_fim_version text NOT NULL,
    target_fim_id text NOT NULL,
    target_fim_version text NOT NULL,
    praedikat text NOT NULL
);


--
-- Name: dokumentensteckbrief; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dokumentensteckbrief (
    fim_id character varying NOT NULL,
    fim_version character varying NOT NULL,
    nummernkreis character varying NOT NULL,
    name character varying NOT NULL,
    definition character varying,
    bezeichnung character varying,
    beschreibung character varying,
    freigabe_status smallint NOT NULL,
    versionshinweis character varying,
    status_gesetzt_durch character varying,
    status_gesetzt_am date,
    gueltig_ab date,
    gueltig_bis date,
    bezug text[] NOT NULL,
    stichwort text[] NOT NULL,
    letzte_aenderung timestamp with time zone NOT NULL,
    last_update timestamp with time zone NOT NULL,
    dokumentart character varying,
    ist_abstrakt boolean NOT NULL,
    hilfetext character varying,
    xdf_version character varying NOT NULL,
    xml_content character varying NOT NULL,
    is_latest boolean DEFAULT false NOT NULL,
    raw_content text NOT NULL,
    immutable_json jsonb,
    immutable_hash text,
    raw_content_fts tsvector GENERATED ALWAYS AS (to_tsvector('german'::regconfig, raw_content)) STORED,
    veroeffentlichungsdatum date
);


--
-- Name: dokumentsteckbrief_relations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dokumentsteckbrief_relations (
    source_fim_id text NOT NULL,
    source_fim_version text NOT NULL,
    target_fim_id text NOT NULL,
    target_fim_version text NOT NULL,
    praedikat text NOT NULL
);


--
-- Name: gebiet_id; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gebiet_id (
    id text NOT NULL,
    label text NOT NULL,
    urn text NOT NULL,
    version date NOT NULL
);


--
-- Name: json_schema_file; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.json_schema_file (
    filename character varying NOT NULL,
    content character varying NOT NULL,
    created_at timestamp with time zone NOT NULL,
    generated_from character varying NOT NULL,
    canonical_hash character varying NOT NULL
);


--
-- Name: katalog; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.katalog (
    filename text NOT NULL,
    source text NOT NULL,
    content bytea NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: leistung; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.leistung (
    id character varying NOT NULL,
    typisierung character varying[] NOT NULL,
    leistungsschluessel text[] NOT NULL,
    title character varying NOT NULL,
    kurztext character varying,
    volltext character varying,
    xml_content character varying NOT NULL,
    redaktion_id text NOT NULL,
    rechtsgrundlagen character varying,
    leistungstyp character varying,
    hierarchy_type text NOT NULL,
    root_for_leistungsschluessel text,
    sdg_informationsbereiche text[] NOT NULL,
    klassifizierung text[] NOT NULL,
    source text NOT NULL,
    freigabestatus_katalog smallint,
    freigabestatus_bibliothek smallint,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    erstellt_datum_zeit timestamp with time zone,
    geaendert_datum_zeit timestamp with time zone,
    leistungsbezeichnung text,
    leistungsbezeichnung_2 text,
    raw_content text NOT NULL,
    raw_content_fts tsvector GENERATED ALWAYS AS (to_tsvector('german'::regconfig, raw_content)) STORED,
    einheitlicher_ansprechpartner boolean
);


--
-- Name: link; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.link (
    uri character varying NOT NULL,
    last_checked timestamp with time zone,
    last_online timestamp with time zone
);


--
-- Name: link_leistung_relation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.link_leistung_relation (
    uri character varying NOT NULL,
    redaktion character varying NOT NULL,
    leistung character varying NOT NULL
);


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration_file text NOT NULL,
    migration_sha text NOT NULL
);


--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: prozess; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.prozess (
    id character varying NOT NULL,
    version character varying,
    name character varying NOT NULL,
    xml_content character varying NOT NULL,
    letzter_aenderungszeitpunkt timestamp with time zone,
    raw_content_without_visualization text,
    raw_content_fts tsvector GENERATED ALWAYS AS (to_tsvector('german'::regconfig, raw_content_without_visualization)) STORED,
    freigabe_status smallint,
    detaillierungsstufe text,
    prozessklasse text DEFAULT 'UNKNOWN'::text NOT NULL,
    verwaltungspolitische_kodierung text[],
    report_file bytea,
    visualization_file bytea
);


--
-- Name: prozess_dokumentsteckbrief_relation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.prozess_dokumentsteckbrief_relation (
    prozess_id text NOT NULL,
    dokumentsteckbrief_fim_id text NOT NULL,
    role text NOT NULL
);


--
-- Name: prozessklasse; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.prozessklasse (
    id text NOT NULL,
    name text NOT NULL,
    version text,
    letzter_aenderungszeitpunkt timestamp with time zone,
    freigabe_status smallint,
    xml_content text NOT NULL,
    raw_content text NOT NULL,
    raw_content_fts tsvector GENERATED ALWAYS AS (to_tsvector('german'::regconfig, raw_content)) STORED NOT NULL,
    operatives_ziel text,
    verfahrensart text,
    handlungsform text
);


--
-- Name: pvog_batch; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pvog_batch (
    batch_id integer NOT NULL,
    batch_xml text NOT NULL
);


--
-- Name: raw_pvog_resource; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.raw_pvog_resource (
    redaktion_id character varying NOT NULL,
    id character varying NOT NULL,
    xml_content character varying NOT NULL,
    last_synced timestamp with time zone,
    resource_class text NOT NULL
);


--
-- Name: regel; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.regel (
    fim_id text NOT NULL,
    fim_version text NOT NULL,
    xdf_version text NOT NULL,
    xml_content text NOT NULL,
    immutable_json jsonb,
    immutable_hash text,
    nummernkreis text NOT NULL,
    name text NOT NULL,
    letzte_aenderung timestamp with time zone NOT NULL,
    veroeffentlichungsdatum date,
    last_update timestamp with time zone NOT NULL,
    freigabe_status smallint,
    status_gesetzt_durch text,
    status_gesetzt_am date
);


--
-- Name: schema; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema (
    fim_id character varying NOT NULL,
    fim_version character varying NOT NULL,
    nummernkreis character varying NOT NULL,
    name character varying NOT NULL,
    beschreibung character varying,
    definition character varying,
    bezug text[] NOT NULL,
    freigabe_status smallint NOT NULL,
    status_gesetzt_durch character varying,
    gueltig_ab date,
    gueltig_bis date,
    versionshinweis character varying,
    letzte_aenderung timestamp with time zone NOT NULL,
    stichwort text[] NOT NULL,
    steckbrief_id character varying,
    xdf_version character varying NOT NULL,
    last_update timestamp with time zone NOT NULL,
    bezeichnung character varying,
    status_gesetzt_am date,
    bezug_components text[] NOT NULL,
    quality_report jsonb NOT NULL,
    veroeffentlichungsdatum date,
    xml_content text,
    is_latest boolean DEFAULT false NOT NULL,
    raw_content text,
    immutable_json jsonb,
    immutable_hash text,
    raw_content_fts tsvector GENERATED ALWAYS AS (to_tsvector('german'::regconfig, raw_content)) STORED
);


--
-- Name: schema_file; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_file (
    filename character varying NOT NULL,
    content character varying NOT NULL,
    created_at timestamp with time zone NOT NULL,
    schema_fim_id character varying NOT NULL,
    schema_fim_version character varying NOT NULL
);


--
-- Name: schema_file_code_list; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_file_code_list (
    code_list_id integer NOT NULL,
    schema_filename character varying NOT NULL
);


--
-- Name: schema_regel_relation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_regel_relation (
    schema_fim_id text NOT NULL,
    schema_fim_version text NOT NULL,
    regel_fim_id text NOT NULL,
    regel_fim_version text NOT NULL
);


--
-- Name: schema_relations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_relations (
    source_fim_id text NOT NULL,
    source_fim_version text NOT NULL,
    target_fim_id text NOT NULL,
    target_fim_version text NOT NULL,
    praedikat text NOT NULL
);


--
-- Name: tree_node; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tree_node (
    id integer NOT NULL,
    schema_fim_id character varying,
    schema_fim_version character varying,
    group_namespace character varying,
    group_fim_id character varying,
    group_fim_version character varying,
    field_namespace character varying,
    field_fim_id character varying,
    field_fim_version character varying
);


--
-- Name: tree_node_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tree_node_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tree_node_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tree_node_id_seq OWNED BY public.tree_node.id;


--
-- Name: tree_relation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tree_relation (
    parent_id integer NOT NULL,
    child_index integer NOT NULL,
    child_id integer NOT NULL,
    anzahl character varying NOT NULL,
    bezug character varying[] NOT NULL
);


--
-- Name: xsd_file; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.xsd_file (
    filename character varying NOT NULL,
    content character varying NOT NULL,
    created_at timestamp with time zone NOT NULL,
    generated_from character varying NOT NULL,
    canonical_hash character varying NOT NULL
);


--
-- Name: xzufi_organisationseinheit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.xzufi_organisationseinheit (
    redaktion_id text NOT NULL,
    id text NOT NULL,
    xml_content text NOT NULL
);


--
-- Name: xzufi_spezialisierung; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.xzufi_spezialisierung (
    redaktion_id text NOT NULL,
    id text NOT NULL,
    leistung_id text NOT NULL,
    leistung_redaktion_id text NOT NULL,
    xml_content text NOT NULL
);


--
-- Name: access_token id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_token ALTER COLUMN id SET DEFAULT nextval('public.access_token_id_seq'::regclass);


--
-- Name: code_list id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.code_list ALTER COLUMN id SET DEFAULT nextval('public.code_list_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: tree_node id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tree_node ALTER COLUMN id SET DEFAULT nextval('public.tree_node_id_seq'::regclass);


--
-- Name: access_token access_token_nummernkreis_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_token
    ADD CONSTRAINT access_token_nummernkreis_key UNIQUE (nummernkreis);


--
-- Name: access_token access_token_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_token
    ADD CONSTRAINT access_token_pkey PRIMARY KEY (id);


--
-- Name: access_token access_token_token_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_token
    ADD CONSTRAINT access_token_token_key UNIQUE (token);


--
-- Name: admin_session admin_session_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.admin_session
    ADD CONSTRAINT admin_session_pkey PRIMARY KEY (token);


--
-- Name: code_list code_list_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.code_list
    ADD CONSTRAINT code_list_pkey PRIMARY KEY (id);


--
-- Name: datenfeld datenfeld_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.datenfeld
    ADD CONSTRAINT datenfeld_pkey PRIMARY KEY (namespace, fim_id, fim_version);


--
-- Name: datenfeld_regel_relation datenfeld_regel_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.datenfeld_regel_relation
    ADD CONSTRAINT datenfeld_regel_relation_pkey PRIMARY KEY (datenfeld_namespace, datenfeld_fim_id, datenfeld_fim_version, regel_fim_id, regel_fim_version);


--
-- Name: datenfeld_relations datenfeld_relations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.datenfeld_relations
    ADD CONSTRAINT datenfeld_relations_pkey PRIMARY KEY (source_fim_id, source_fim_version, target_fim_id, target_fim_version, praedikat);


--
-- Name: datenfeldgruppe datenfeldgruppe_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.datenfeldgruppe
    ADD CONSTRAINT datenfeldgruppe_pkey PRIMARY KEY (namespace, fim_id, fim_version);


--
-- Name: datenfeldgruppe_regel_relation datenfeldgruppe_regel_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.datenfeldgruppe_regel_relation
    ADD CONSTRAINT datenfeldgruppe_regel_relation_pkey PRIMARY KEY (datenfeldgruppe_namespace, datenfeldgruppe_fim_id, datenfeldgruppe_fim_version, regel_fim_id, regel_fim_version);


--
-- Name: datenfeldgruppe_relations datenfeldgruppe_relations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.datenfeldgruppe_relations
    ADD CONSTRAINT datenfeldgruppe_relations_pkey PRIMARY KEY (source_fim_id, source_fim_version, target_fim_id, target_fim_version, praedikat);


--
-- Name: dokumentensteckbrief dokumentensteckbrief_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dokumentensteckbrief
    ADD CONSTRAINT dokumentensteckbrief_pkey PRIMARY KEY (fim_id, fim_version);


--
-- Name: dokumentsteckbrief_relations dokumentsteckbrief_relations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dokumentsteckbrief_relations
    ADD CONSTRAINT dokumentsteckbrief_relations_pkey PRIMARY KEY (source_fim_id, source_fim_version, target_fim_id, target_fim_version, praedikat);


--
-- Name: gebiet_id gebiet_id_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gebiet_id
    ADD CONSTRAINT gebiet_id_pkey PRIMARY KEY (urn, id);


--
-- Name: json_schema_file json_schema_file_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.json_schema_file
    ADD CONSTRAINT json_schema_file_pkey PRIMARY KEY (filename);


--
-- Name: katalog katalog_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.katalog
    ADD CONSTRAINT katalog_pkey PRIMARY KEY (filename);


--
-- Name: leistung leistung_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.leistung
    ADD CONSTRAINT leistung_pkey PRIMARY KEY (redaktion_id, id);


--
-- Name: leistung leistung_unique_root_for_leistungsschluessel; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.leistung
    ADD CONSTRAINT leistung_unique_root_for_leistungsschluessel UNIQUE (root_for_leistungsschluessel);


--
-- Name: link_leistung_relation link_leistung_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.link_leistung_relation
    ADD CONSTRAINT link_leistung_relation_pkey PRIMARY KEY (uri, redaktion, leistung);


--
-- Name: link link_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.link
    ADD CONSTRAINT link_pkey PRIMARY KEY (uri);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: prozess_dokumentsteckbrief_relation prozess_dokumentsteckbrief_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prozess_dokumentsteckbrief_relation
    ADD CONSTRAINT prozess_dokumentsteckbrief_relation_pkey PRIMARY KEY (prozess_id, dokumentsteckbrief_fim_id, role);


--
-- Name: prozess prozess_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prozess
    ADD CONSTRAINT prozess_pkey PRIMARY KEY (id);


--
-- Name: prozessklasse prozessklasse_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prozessklasse
    ADD CONSTRAINT prozessklasse_pkey PRIMARY KEY (id);


--
-- Name: pvog_batch pvog_batch_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pvog_batch
    ADD CONSTRAINT pvog_batch_pkey PRIMARY KEY (batch_id);


--
-- Name: raw_pvog_resource raw_pvog_resource_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.raw_pvog_resource
    ADD CONSTRAINT raw_pvog_resource_pkey PRIMARY KEY (resource_class, redaktion_id, id);


--
-- Name: regel regel_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.regel
    ADD CONSTRAINT regel_pkey PRIMARY KEY (fim_id, fim_version);


--
-- Name: schema_file_code_list schema_file_code_list_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_file_code_list
    ADD CONSTRAINT schema_file_code_list_pkey PRIMARY KEY (code_list_id, schema_filename);


--
-- Name: schema_file schema_file_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_file
    ADD CONSTRAINT schema_file_pkey PRIMARY KEY (filename);


--
-- Name: schema schema_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema
    ADD CONSTRAINT schema_pkey PRIMARY KEY (fim_id, fim_version);


--
-- Name: schema_regel_relation schema_regel_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_regel_relation
    ADD CONSTRAINT schema_regel_relation_pkey PRIMARY KEY (schema_fim_id, schema_fim_version, regel_fim_id, regel_fim_version);


--
-- Name: schema_relations schema_relations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_relations
    ADD CONSTRAINT schema_relations_pkey PRIMARY KEY (source_fim_id, source_fim_version, target_fim_id, target_fim_version, praedikat);


--
-- Name: tree_node tree_node_field_namespace_field_fim_id_field_fim_version_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tree_node
    ADD CONSTRAINT tree_node_field_namespace_field_fim_id_field_fim_version_key UNIQUE (field_namespace, field_fim_id, field_fim_version);


--
-- Name: tree_node tree_node_group_namespace_group_fim_id_group_fim_version_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tree_node
    ADD CONSTRAINT tree_node_group_namespace_group_fim_id_group_fim_version_key UNIQUE (group_namespace, group_fim_id, group_fim_version);


--
-- Name: tree_node tree_node_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tree_node
    ADD CONSTRAINT tree_node_pkey PRIMARY KEY (id);


--
-- Name: tree_node tree_node_schema_fim_id_schema_fim_version_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tree_node
    ADD CONSTRAINT tree_node_schema_fim_id_schema_fim_version_key UNIQUE (schema_fim_id, schema_fim_version);


--
-- Name: tree_relation tree_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tree_relation
    ADD CONSTRAINT tree_relation_pkey PRIMARY KEY (parent_id, child_index);


--
-- Name: xsd_file xsd_file_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.xsd_file
    ADD CONSTRAINT xsd_file_pkey PRIMARY KEY (filename);


--
-- Name: xzufi_organisationseinheit xzufi_organisationseinheit_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.xzufi_organisationseinheit
    ADD CONSTRAINT xzufi_organisationseinheit_pkey PRIMARY KEY (redaktion_id, id);


--
-- Name: xzufi_spezialisierung xzufi_spezialisierung_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.xzufi_spezialisierung
    ADD CONSTRAINT xzufi_spezialisierung_pkey PRIMARY KEY (redaktion_id, id);


--
-- Name: idx_btree_leistungsschluessel; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_btree_leistungsschluessel ON public.leistung USING btree (leistungsschluessel) WHERE (root_for_leistungsschluessel IS NULL);


--
-- Name: idx_canonical_version_uri; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_canonical_version_uri ON public.code_list USING btree (genericode_canonical_version_uri) WHERE (is_external = true);


--
-- Name: idx_datenfeld_fts; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_datenfeld_fts ON public.datenfeld USING gin (raw_content_fts);


--
-- Name: idx_datenfeldgruppe_fts; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_datenfeldgruppe_fts ON public.datenfeldgruppe USING gin (raw_content_fts);


--
-- Name: idx_detaillierungsstufe_prozess; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_detaillierungsstufe_prozess ON public.prozess USING btree (detaillierungsstufe);


--
-- Name: idx_dokumentsteckbrief_fts; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_dokumentsteckbrief_fts ON public.dokumentensteckbrief USING gin (raw_content_fts);


--
-- Name: idx_leistung_fts; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_leistung_fts ON public.leistung USING gin (raw_content_fts);


--
-- Name: idx_leistungsschluessel; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_leistungsschluessel ON public.leistung USING gin (leistungsschluessel) WHERE (root_for_leistungsschluessel IS NULL);


--
-- Name: idx_prozess_fts; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_prozess_fts ON public.prozess USING gin (raw_content_fts);


--
-- Name: idx_prozessklasse_raw_content_fts; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_prozessklasse_raw_content_fts ON public.prozessklasse USING gin (raw_content_fts);


--
-- Name: idx_root_for_leistungsschluessel; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_root_for_leistungsschluessel ON public.leistung USING btree (root_for_leistungsschluessel) WHERE (root_for_leistungsschluessel IS NOT NULL);


--
-- Name: idx_schema_fts; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_schema_fts ON public.schema USING gin (raw_content_fts);


--
-- Name: idx_unique_is_latest_per_fim_id_dokumentensteckbrief; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_unique_is_latest_per_fim_id_dokumentensteckbrief ON public.dokumentensteckbrief USING btree (fim_id) WHERE (is_latest = true);


--
-- Name: idx_unique_is_latest_per_fim_id_schema; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_unique_is_latest_per_fim_id_schema ON public.schema USING btree (fim_id) WHERE (is_latest = true);


--
-- Name: idx_unique_is_latest_per_namespace_fim_id_datenfeld; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_unique_is_latest_per_namespace_fim_id_datenfeld ON public.datenfeld USING btree (namespace, fim_id) WHERE (is_latest = true);


--
-- Name: idx_unique_is_latest_per_namespace_fim_id_datenfeldgruppe; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_unique_is_latest_per_namespace_fim_id_datenfeldgruppe ON public.datenfeldgruppe USING btree (namespace, fim_id) WHERE (is_latest = true);


--
-- Name: leistung_source_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX leistung_source_idx ON public.leistung USING btree (source);


--
-- Name: datenfeld trg_datenfeld_raw_content; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trg_datenfeld_raw_content BEFORE INSERT OR UPDATE ON public.datenfeld FOR EACH ROW EXECUTE FUNCTION public.schema_element_raw_content_trigger();


--
-- Name: datenfeldgruppe trg_datenfeldgruppe_raw_content; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trg_datenfeldgruppe_raw_content BEFORE INSERT OR UPDATE ON public.datenfeldgruppe FOR EACH ROW EXECUTE FUNCTION public.schema_element_raw_content_trigger();


--
-- Name: dokumentensteckbrief trg_dokumentensteckbrief_raw_content; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trg_dokumentensteckbrief_raw_content BEFORE INSERT OR UPDATE ON public.dokumentensteckbrief FOR EACH ROW EXECUTE FUNCTION public.schema_element_raw_content_trigger();


--
-- Name: leistung trg_leistung_raw_content; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trg_leistung_raw_content BEFORE INSERT OR UPDATE ON public.leistung FOR EACH ROW EXECUTE FUNCTION public.leistung_raw_content_trigger();


--
-- Name: prozessklasse trg_prozessklasse_transform_content; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trg_prozessklasse_transform_content BEFORE INSERT OR UPDATE ON public.prozessklasse FOR EACH ROW EXECUTE FUNCTION public.prozessklasse_xml_content_trigger();


--
-- Name: schema_file trg_schema_content; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trg_schema_content BEFORE INSERT OR UPDATE ON public.schema_file FOR EACH ROW EXECUTE FUNCTION public.update_schema_content();


--
-- Name: datenfeld trg_update_is_latest; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trg_update_is_latest AFTER INSERT ON public.datenfeld FOR EACH ROW EXECUTE FUNCTION public.call_update_latest_datenfeld();


--
-- Name: datenfeldgruppe trg_update_is_latest; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trg_update_is_latest AFTER INSERT ON public.datenfeldgruppe FOR EACH ROW EXECUTE FUNCTION public.call_update_latest_datenfeldgruppe();


--
-- Name: dokumentensteckbrief trg_update_is_latest; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trg_update_is_latest AFTER INSERT ON public.dokumentensteckbrief FOR EACH ROW EXECUTE FUNCTION public.call_update_latest_dokumentensteckbrief();


--
-- Name: schema trg_update_is_latest; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trg_update_is_latest AFTER INSERT ON public.schema FOR EACH ROW EXECUTE FUNCTION public.call_update_latest_schema();


--
-- Name: datenfeld_regel_relation datenfeld_regel_relation_datenfeld_namespace_datenfeld_fim_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.datenfeld_regel_relation
    ADD CONSTRAINT datenfeld_regel_relation_datenfeld_namespace_datenfeld_fim_fkey FOREIGN KEY (datenfeld_namespace, datenfeld_fim_id, datenfeld_fim_version) REFERENCES public.datenfeld(namespace, fim_id, fim_version) ON DELETE CASCADE;


--
-- Name: datenfeld_regel_relation datenfeld_regel_relation_regel_fim_id_regel_fim_version_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.datenfeld_regel_relation
    ADD CONSTRAINT datenfeld_regel_relation_regel_fim_id_regel_fim_version_fkey FOREIGN KEY (regel_fim_id, regel_fim_version) REFERENCES public.regel(fim_id, fim_version);


--
-- Name: datenfeld_relations datenfeld_relations_source_namespace_source_fim_id_source__fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.datenfeld_relations
    ADD CONSTRAINT datenfeld_relations_source_namespace_source_fim_id_source__fkey FOREIGN KEY (source_namespace, source_fim_id, source_fim_version) REFERENCES public.datenfeld(namespace, fim_id, fim_version) ON DELETE CASCADE;


--
-- Name: datenfeldgruppe_regel_relation datenfeldgruppe_regel_relatio_datenfeldgruppe_namespace_da_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.datenfeldgruppe_regel_relation
    ADD CONSTRAINT datenfeldgruppe_regel_relatio_datenfeldgruppe_namespace_da_fkey FOREIGN KEY (datenfeldgruppe_namespace, datenfeldgruppe_fim_id, datenfeldgruppe_fim_version) REFERENCES public.datenfeldgruppe(namespace, fim_id, fim_version) ON DELETE CASCADE;


--
-- Name: datenfeldgruppe_regel_relation datenfeldgruppe_regel_relatio_regel_fim_id_regel_fim_versi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.datenfeldgruppe_regel_relation
    ADD CONSTRAINT datenfeldgruppe_regel_relatio_regel_fim_id_regel_fim_versi_fkey FOREIGN KEY (regel_fim_id, regel_fim_version) REFERENCES public.regel(fim_id, fim_version);


--
-- Name: datenfeldgruppe_relations datenfeldgruppe_relations_source_namespace_source_fim_id_s_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.datenfeldgruppe_relations
    ADD CONSTRAINT datenfeldgruppe_relations_source_namespace_source_fim_id_s_fkey FOREIGN KEY (source_namespace, source_fim_id, source_fim_version) REFERENCES public.datenfeldgruppe(namespace, fim_id, fim_version) ON DELETE CASCADE;


--
-- Name: dokumentsteckbrief_relations dokumentsteckbrief_relations_source_fim_id_source_fim_vers_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dokumentsteckbrief_relations
    ADD CONSTRAINT dokumentsteckbrief_relations_source_fim_id_source_fim_vers_fkey FOREIGN KEY (source_fim_id, source_fim_version) REFERENCES public.dokumentensteckbrief(fim_id, fim_version) ON DELETE CASCADE;


--
-- Name: json_schema_file json_schema_file_generated_from_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.json_schema_file
    ADD CONSTRAINT json_schema_file_generated_from_fkey FOREIGN KEY (generated_from) REFERENCES public.schema_file(filename) ON DELETE CASCADE;


--
-- Name: link_leistung_relation link_leistung_relation_redaktion_leistung_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.link_leistung_relation
    ADD CONSTRAINT link_leistung_relation_redaktion_leistung_fkey FOREIGN KEY (redaktion, leistung) REFERENCES public.leistung(redaktion_id, id) ON DELETE CASCADE;


--
-- Name: link_leistung_relation link_leistung_relation_uri_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.link_leistung_relation
    ADD CONSTRAINT link_leistung_relation_uri_fkey FOREIGN KEY (uri) REFERENCES public.link(uri) ON DELETE CASCADE;


--
-- Name: prozess_dokumentsteckbrief_relation prozess_dokumentsteckbrief_relation_prozess_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prozess_dokumentsteckbrief_relation
    ADD CONSTRAINT prozess_dokumentsteckbrief_relation_prozess_id_fkey FOREIGN KEY (prozess_id) REFERENCES public.prozess(id);


--
-- Name: schema_file_code_list schema_file_code_list_code_list_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_file_code_list
    ADD CONSTRAINT schema_file_code_list_code_list_id_fkey FOREIGN KEY (code_list_id) REFERENCES public.code_list(id) ON DELETE RESTRICT;


--
-- Name: schema_file_code_list schema_file_code_list_schema_filename_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_file_code_list
    ADD CONSTRAINT schema_file_code_list_schema_filename_fkey FOREIGN KEY (schema_filename) REFERENCES public.schema_file(filename) ON DELETE CASCADE;


--
-- Name: schema_file schema_file_schema_fim_id_schema_fim_version_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_file
    ADD CONSTRAINT schema_file_schema_fim_id_schema_fim_version_fkey FOREIGN KEY (schema_fim_id, schema_fim_version) REFERENCES public.schema(fim_id, fim_version) ON DELETE CASCADE;


--
-- Name: schema_regel_relation schema_regel_relation_regel_fim_id_regel_fim_version_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_regel_relation
    ADD CONSTRAINT schema_regel_relation_regel_fim_id_regel_fim_version_fkey FOREIGN KEY (regel_fim_id, regel_fim_version) REFERENCES public.regel(fim_id, fim_version);


--
-- Name: schema_regel_relation schema_regel_relation_schema_fim_id_schema_fim_version_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_regel_relation
    ADD CONSTRAINT schema_regel_relation_schema_fim_id_schema_fim_version_fkey FOREIGN KEY (schema_fim_id, schema_fim_version) REFERENCES public.schema(fim_id, fim_version) ON DELETE CASCADE;


--
-- Name: schema_relations schema_relations_source_fim_id_source_fim_version_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_relations
    ADD CONSTRAINT schema_relations_source_fim_id_source_fim_version_fkey FOREIGN KEY (source_fim_id, source_fim_version) REFERENCES public.schema(fim_id, fim_version) ON DELETE CASCADE;


--
-- Name: tree_node tree_node_field_namespace_field_fim_id_field_fim_version_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tree_node
    ADD CONSTRAINT tree_node_field_namespace_field_fim_id_field_fim_version_fkey FOREIGN KEY (field_namespace, field_fim_id, field_fim_version) REFERENCES public.datenfeld(namespace, fim_id, fim_version) MATCH FULL;


--
-- Name: tree_node tree_node_group_namespace_group_fim_id_group_fim_version_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tree_node
    ADD CONSTRAINT tree_node_group_namespace_group_fim_id_group_fim_version_fkey FOREIGN KEY (group_namespace, group_fim_id, group_fim_version) REFERENCES public.datenfeldgruppe(namespace, fim_id, fim_version) MATCH FULL;


--
-- Name: tree_node tree_node_schema_fim_id_schema_fim_version_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tree_node
    ADD CONSTRAINT tree_node_schema_fim_id_schema_fim_version_fkey FOREIGN KEY (schema_fim_id, schema_fim_version) REFERENCES public.schema(fim_id, fim_version) MATCH FULL;


--
-- Name: tree_relation tree_relation_child_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tree_relation
    ADD CONSTRAINT tree_relation_child_id_fkey FOREIGN KEY (child_id) REFERENCES public.tree_node(id) ON DELETE RESTRICT;


--
-- Name: tree_relation tree_relation_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tree_relation
    ADD CONSTRAINT tree_relation_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.tree_node(id) ON DELETE CASCADE;


--
-- Name: xsd_file xsd_file_generated_from_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.xsd_file
    ADD CONSTRAINT xsd_file_generated_from_fkey FOREIGN KEY (generated_from) REFERENCES public.schema_file(filename) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

