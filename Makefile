watch-cms-content:
	FIM_PORTAL_CONFIG_FILE="./fimportal.config.json" \
	poetry run watchfiles "python -m fimportal compile-cms-content" ./fimportal 

update-cms-content:
	FIM_PORTAL_CONFIG_FILE="./fimportal.config.json" \
	poetry run python -m fimportal compile-cms-content

migrate:
	FIM_PORTAL_CONFIG_FILE="./fimportal.config.json" \
	poetry run python -m fimportal migrate

run-datenfeld-import:
	FIM_PORTAL_CONFIG_FILE="./fimportal.config.json" \
	poetry run python -m fimportal run-datenfeld-import

run-leistung-import:
	FIM_PORTAL_CONFIG_FILE="./fimportal.config.json" \
	poetry run python -m fimportal run-leistung-import

run-prozess-import:
	FIM_PORTAL_CONFIG_FILE="./fimportal.config.json" \
	poetry run python -m fimportal run-prozesse-import

run-gebiet-id-import:
	FIM_PORTAL_CONFIG_FILE="./fimportal.config.json" \
	poetry run python -m fimportal run-gebiet-id-import

check-generated-data:
	FIM_PORTAL_CONFIG_FILE="./fimportal.config.json" \
	poetry run python -m fimportal check-generated-data

# Reset the database migrations and import api tokens.
reset-db:
	FIM_PORTAL_CONFIG_FILE="./fimportal.config.json" \
	poetry run python -m fimportal resetdb

check-links:
	FIM_PORTAL_CONFIG_FILE="./fimportal.config.json" \
	poetry run python -m fimportal check-links

# Run the app in local environment
run-local:
	FIM_PORTAL_CONFIG_FILE="./fimportal.config.json" \
	poetry run uvicorn fimportal.main:create_app --factory  \
		--reload \
		--reload-include *.css \
		--reload-include *.js  \
		--reload-include *.jinja  

run-gunicorn:
	FIM_PORTAL_CONFIG_FILE="./fimportal.config.json" \
	gunicorn \
		-k uvicorn.workers.UvicornWorker \
		--workers=3 \
		--access-logfile="-" \
		--log-config-json="./config/log_config.json" \
		"fimportal.main:create_app()"

dump-sql-schema:
	FIM_PORTAL_CONFIG_FILE="./fimportal.config.json" \
	poetry run python -m fimportal print-sql-schema > schema.sql

# Clean up all compiled Python files and build artifacts
clean:
	find . -name "*.pyc" -delete
	find . -name "*.pyo" -delete
	rm -rf build/ dist/ *.egg-info/

check:
	poetry run ruff format .
	poetry run ruff check .
	poetry run pyright .
	poetry run pytest -n auto --maxprocesses=4

format:
	poetry run ruff format .

lint:
	poetry run ruff check .
	poetry run pyright .

test:
	poetry run pytest -n auto --maxprocesses=4

