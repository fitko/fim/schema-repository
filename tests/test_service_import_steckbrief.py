from datetime import date, datetime, timezone

import pytest
from freezegun import freeze_time

from fimportal.common import FreigabeStatus
from fimportal.errors import ImportException, StatusHasNotChangedException
from fimportal.models.imports import XdfVersion
from fimportal.models.xdf3 import FullSteckbriefOut, RelationOut
from fimportal.service import Service
from fimportal.xdatenfelder.xdf3.common import (
    Dokumentart,
    Identifier,
    Relation,
    RelationTyp,
    Version,
)
from tests.factories.xdf2 import Xdf2SteckbriefImportFactory
from tests.factories.xdf3 import TEST_STECKBRIEF_XML, Xdf3SteckbriefMessageFactory


@pytest.mark.asyncio(loop_scope="session")
async def test_should_not_overwrite_a_steckbrief_if_the_status_is_identical(
    service: Service,
):
    steckbrief_import = Xdf2SteckbriefImportFactory().full().build()
    await service.import_xdf2_steckbrief(steckbrief_import, check_status=True)

    with pytest.raises(StatusHasNotChangedException):
        await service.import_xdf2_steckbrief(steckbrief_import, check_status=True)


@pytest.mark.asyncio(loop_scope="session")
async def test_should_update_a_steckbrief_if_the_status_has_changed(service: Service):
    steckbrief_import = (
        Xdf2SteckbriefImportFactory(
            id="D12345",
            version="1.0",
            freigabe_status=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        )
        .full()
        .build()
    )
    with freeze_time("2023-10-16T00:00:00"):
        await service.import_xdf2_steckbrief(steckbrief_import, check_status=True)

    steckbrief_import.freigabe_status = FreigabeStatus.VORGESEHEN_ZUM_LOESCHEN
    steckbrief_import.letzte_aenderung = datetime(2023, 10, 16, tzinfo=timezone.utc)

    with freeze_time("2023-10-17T00:00:00"):
        await service.import_xdf2_steckbrief(steckbrief_import, check_status=True)

    steckbrief = await service.get_steckbrief(
        steckbrief_import.id, steckbrief_import.version
    )

    assert steckbrief == FullSteckbriefOut(
        fim_id="D12345",
        fim_version="1.0",
        nummernkreis="12000",
        name=steckbrief_import.steckbrief.name,
        definition=steckbrief_import.steckbrief.definition,
        freigabe_status=FreigabeStatus.VORGESEHEN_ZUM_LOESCHEN,
        freigabe_status_label=FreigabeStatus.VORGESEHEN_ZUM_LOESCHEN.to_label(),
        status_gesetzt_durch=steckbrief_import.steckbrief.fachlicher_ersteller,
        bezug=[steckbrief_import.steckbrief.bezug or ""],
        versionshinweis=steckbrief_import.steckbrief.versionshinweis,
        veroeffentlichungsdatum=steckbrief_import.steckbrief.veroeffentlichungsdatum,
        letzte_aenderung=steckbrief_import.letzte_aenderung,
        last_update=datetime(2023, 10, 17, tzinfo=timezone.utc),
        status_gesetzt_am=steckbrief_import.steckbrief.freigabedatum,
        gueltig_ab=steckbrief_import.steckbrief.gueltig_ab,
        gueltig_bis=steckbrief_import.steckbrief.gueltig_bis,
        bezeichnung=steckbrief_import.steckbrief.bezeichnung_eingabe,
        dokumentart=Dokumentart.from_label(steckbrief_import.steckbrief.dokumentart),
        stichwort=[],
        ist_abstrakt=steckbrief_import.steckbrief.is_referenz,
        hilfetext=steckbrief_import.steckbrief.hilfetext,
        xdf_version=XdfVersion.XDF2,
        beschreibung=steckbrief_import.steckbrief.beschreibung,
        is_latest=True,
        prozesse=[],
        datenschemata=[],
        relation=[],
    )


@pytest.mark.asyncio(loop_scope="session")
async def test_should_create_steckbrief_with_is_latest_true(service: Service):
    steckbrief_import = Xdf2SteckbriefImportFactory(
        id="D12000",
        version="1.0",
        freigabe_status=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
    ).build()
    await service.import_xdf2_steckbrief(steckbrief_import, check_status=True)

    saved_steckbrief = await service.get_steckbrief("D12000", "1.0")
    assert saved_steckbrief is not None
    assert saved_steckbrief.is_latest is True


@pytest.mark.asyncio(loop_scope="session")
async def test_should_update_is_latest_to_highest_steckbrief_version(service: Service):
    steckbrief_import = Xdf2SteckbriefImportFactory(
        id="D12000",
        version="1.0",
        freigabe_status=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
    ).build()
    await service.import_xdf2_steckbrief(steckbrief_import, check_status=True)

    steckbrief_import = Xdf2SteckbriefImportFactory(
        id="D12000",
        version="1.1",
        freigabe_status=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
    ).build()
    await service.import_xdf2_steckbrief(steckbrief_import, check_status=True)

    saved_steckbrief = await service.get_steckbrief("D12000", "1.0")
    assert saved_steckbrief is not None
    assert saved_steckbrief.is_latest is False

    saved_steckbrief = await service.get_steckbrief("D12000", "1.1")
    assert saved_steckbrief is not None
    assert saved_steckbrief.is_latest is True


@pytest.mark.asyncio(loop_scope="session")
async def test_should_keep_is_latest_on_highest_steckbrief_version(service: Service):
    steckbrief_import = Xdf2SteckbriefImportFactory(
        id="D12000",
        version="1.1",
        freigabe_status=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
    ).build()
    await service.import_xdf2_steckbrief(steckbrief_import, check_status=True)

    steckbrief_import = Xdf2SteckbriefImportFactory(
        id="D12000",
        version="1.0",
        freigabe_status=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
    ).build()
    await service.import_xdf2_steckbrief(steckbrief_import, check_status=True)

    saved_steckbrief = await service.get_steckbrief("D12000", "1.1")
    assert saved_steckbrief is not None
    assert saved_steckbrief.is_latest is True

    saved_steckbrief = await service.get_steckbrief("D12000", "1.0")
    assert saved_steckbrief is not None
    assert saved_steckbrief.is_latest is False


@pytest.mark.asyncio(loop_scope="session")
async def test_should_raise_immutability_exception(service: Service):
    steckbrief_import = Xdf2SteckbriefImportFactory(
        id="D12345",
        version="1.0",
        freigabe_status=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        beschreibung="Eine Beschreibung",
    ).build()
    with freeze_time("2023-10-16T00:00:00"):
        await service.import_xdf2_steckbrief(steckbrief_import, check_status=True)

    steckbrief_import.steckbrief.beschreibung = "Bla Bla"

    with pytest.raises(
        ImportException, match="Cannot update document-profile D12345 version 1.0"
    ):
        await service.import_xdf2_steckbrief(steckbrief_import, check_status=False)


@pytest.mark.asyncio(loop_scope="session")
async def test_should_update_relation(service: Service):
    steckbrief_message = Xdf3SteckbriefMessageFactory(
        relation=[
            Relation(
                RelationTyp.IST_AEQUIVALENT_ZU,
                Identifier("D12345", version=Version("1.0.0")),
            )
        ]
    ).build_message()
    await service.import_xdf3_steckbrief(
        steckbrief_message, TEST_STECKBRIEF_XML, check_status=True
    )

    steckbrief_message.steckbrief.relation = [
        Relation(
            RelationTyp.IST_ABGELEITET_VON,
            Identifier("D54321", version=Version("1.0.0")),
        )
    ]
    await service.import_xdf3_steckbrief(
        steckbrief_message, TEST_STECKBRIEF_XML, check_status=False
    )

    saved_steckbrief = await service.get_steckbrief(
        steckbrief_message.fim_id(), steckbrief_message.assert_fim_version()
    )
    assert saved_steckbrief is not None
    assert saved_steckbrief.relation == [
        RelationOut(
            typ=RelationTyp.IST_ABGELEITET_VON,
            fim_id="D54321",
            fim_version="1.0.0",
            name=None,
        )
    ]


@pytest.mark.asyncio(loop_scope="session")
async def test_should_update_steckbrief_with_veroeffentlichungsdatum_none(
    service: Service,
):
    steckbrief_import = Xdf2SteckbriefImportFactory().build()
    steckbrief_import.steckbrief.veroeffentlichungsdatum = None

    await service.import_xdf2_steckbrief(steckbrief_import, check_status=True)
    saved_steckbrief = await service.get_steckbrief(
        steckbrief_import.id, steckbrief_import.version
    )
    assert saved_steckbrief is not None
    assert saved_steckbrief.veroeffentlichungsdatum == None

    steckbrief_import.steckbrief.veroeffentlichungsdatum = date(2024, 11, 25)
    await service.import_xdf2_steckbrief(steckbrief_import, check_status=False)

    saved_steckbrief = await service.get_steckbrief(
        steckbrief_import.id, steckbrief_import.version
    )
    assert saved_steckbrief is not None
    assert saved_steckbrief.veroeffentlichungsdatum == date(2024, 11, 25)


@pytest.mark.asyncio(loop_scope="session")
async def test_should_not_update_steckbrief_with_veroeffentlichungsdatum_date(
    service: Service,
):
    steckbrief_import = Xdf2SteckbriefImportFactory().build()
    steckbrief = steckbrief_import.steckbrief
    steckbrief.veroeffentlichungsdatum = date(2019, 9, 19)

    await service.import_xdf2_steckbrief(steckbrief_import, check_status=True)

    steckbrief.veroeffentlichungsdatum = date(2024, 11, 25)

    with pytest.raises(
        ImportException,
        match=f"Cannot update document-profile {steckbrief_import.id} version {steckbrief_import.version}",
    ):
        await service.import_xdf2_steckbrief(steckbrief_import, check_status=False)
