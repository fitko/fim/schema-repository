from fastapi.testclient import TestClient
import pytest


def test_redirect_services_page(client: TestClient):
    response = client.get("/detail/L/123")

    assert response.headers["location"] == "/services/123"


def test_redirect_schemas_page(client: TestClient):
    response = client.get("/detail/D/S123V0.1")

    assert response.headers["location"] == "/schemas/S123/0.1"


def test_redirect_document_profile_page(client: TestClient):
    response = client.get("/detail/D/D123V0.1")

    assert response.headers["location"] == "/document-profiles/D123/0.1"


@pytest.mark.parametrize("fim_id_with_version", ["S123v0.1", "A123V0.1"])
def test_redirect_schemas_faulty_syntax_page(
    client: TestClient, fim_id_with_version: str
):
    response = client.get(f"/detail/D/{fim_id_with_version}")

    assert response.status_code == 404
    assert (
        f"Resource {fim_id_with_version} konnte nicht gefunden werden." in response.text
    )


def test_redirect_processclass_page(client: TestClient):
    response = client.get("/detail/P/123")

    assert response.headers["location"] == "/processclasses/123"


def test_prozesse_rss_redirect(client: TestClient):
    response = client.get("/fim-feeds/prozesse.rss")

    assert response.headers["location"] == "/tools/rss/processes"


def test_datenfelder_rss_redirect(client: TestClient):
    response = client.get("/fim-feeds/datenfelder.rss")

    assert response.headers["location"] == "/tools/rss/schemas"


def test_leistungen_rss_redirect(client: TestClient):
    response = client.get("/fim-feeds/leistungen.rss")

    assert response.headers["location"] == "/tools/rss/services"
