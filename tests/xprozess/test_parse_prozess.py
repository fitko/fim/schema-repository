import datetime

from fimportal.common import Bundesland, FreigabeStatus
from fimportal.xprozesse.prozess import (
    Aktivitaetengruppe,
    AusloeserErgebnis,
    Bearbeitungsart,
    Beteiligungsform,
    Datei,
    Daten,
    DatenBereitgestellt,
    DatenEmpfangen,
    Datentyp,
    Detaillierungsstufe,
    Entscheidungsart,
    FormellePruefung,
    Formularverweis,
    Handlungsform,
    Handlungsgrundlage,
    Handlungsgrundlagenart,
    Klassifikation,
    Merkmal,
    MimeType,
    Modellierungsmethode,
    ModellierungsmethodeType,
    OperativesZiel,
    Prozess,
    ProzessBibliothek,
    ProzessKatalog,
    Prozessklasse,
    ProzessMessage,
    ProzessModell,
    ProzessNachrichtenKopf,
    Prozessrolle,
    ProzessSteckbrief,
    Prozessteilnehmer,
    ReferenzaktivitaetengruppeType,
    SpezifischeAttribute,
    SpezifischeAttribute1,
    SpezifischeAttribute2,
    SpezifischeAttribute3,
    SpezifischeAttribute4,
    SpezifischeAttribute5,
    SpezifischeAttribute6,
    SpezifischeAttribute7,
    Strukturbeschreibung,
    StrukturbeschreibungFim,
    Uebermittlungsart,
    Verfahrensart,
    VerwaltungspolitischeKodierung,
    Zeitraum,
    Zustandsangaben,
    Zwecksetzung,
    parse_prozess_message,
)
from tests.data import XPROZESS2_DATA


def test_should_parse_prozess():
    with open(XPROZESS2_DATA / "prozess_without_files.xml", "rb") as file:
        data = file.read()

    prozess_nachricht = parse_prozess_message(data)

    assert prozess_nachricht == ProzessMessage(
        nachrichtenkopf=ProzessNachrichtenKopf(
            nachricht_uuid="edfde7f3-6655-4ddd-ac19-46efa718fe02",
            erstellungszeitpunkt=datetime.datetime(
                2023,
                12,
                12,
                9,
                0,
                20,
                247000,
                tzinfo=datetime.timezone(datetime.timedelta(seconds=3600)),
            ),
            autor="BOC",
            leser=None,
        ),
        prozesskatalog=ProzessKatalog(
            name="Prozesskatalog (FIM)",
            version="1.2.3",
            verwaltungspolitische_kodierung=[],
            prozessklasse=[
                Prozessklasse(
                    id="99010023001000",
                    version="01.00.00",
                    gliederungsebene=None,
                    uebergeordnete_prozessklasse_id=None,
                    name="Antrag Erteilung Aufenthaltserlaubnis aus familiären Gründen bearbeiten",
                    bezeichnung="Aufenthaltserlaubnis aus familiären Gründen Erteilung",
                    definition="""Die Person stellt den Antrag auf einen Aufenthaltstitel bei der
                Auslandsvertretung des Auswärtigen Amts oder der für den Wohnort der Person
                zuständigen Ausländerbehörde.
                Die Ausländerbehörde fragt gegebenenfalls. bei anderen Behörden an.
                Die zuständige Ausländerbehörde erteilt den Aufenthaltstitel oder lehnt den Antrag
                der Person ab.
""",
                    handlungsgrundlage=[
                        Handlungsgrundlage(
                            name="§§ " "27-36a " "Aufenthaltsgesetz " "(AufenthG)",
                            art=Handlungsgrundlagenart.GESETZ,
                            uri="\n"
                            "                    "
                            "https://www.gesetze-im-internet.de/aufenthg_2004/BJNR195010004.html#BJNR195010004BJNG000801310",
                            gueltigkeitszeitraum=None,
                        )
                    ],
                    zwecksetzung=Zwecksetzung.ORDNUNGSVERWALTUNG,
                    operatives_ziel=OperativesZiel.KONKRETE_REGELUNG_RECHTSVERHAELTNIS_HANDLUNG,
                    handlungsform=Handlungsform.VERWALTUNGSAKT,
                    verfahrensart=Verfahrensart.NICHT_FORMLICHES_VERWALTUNGSVERFAHREN_VWVFG_VWGO,
                    fachlich_freigebende_stelle="Bundesministerium des Innern und für Heimat (BMI)",
                    verwaltungspolitische_kodierung=[],
                    merkmal=[
                        Merkmal(
                            ordnungsrahmen_name=None,
                            ordnungsrahmen_version=None,
                            merkmal_id=None,
                            merkmal_name="Prozessart",
                            merkmal_datentyp=Datentyp.STRING,
                            merkmal_wert=["Kernprozess"],
                        )
                    ],
                    klassifikation=[
                        Klassifikation(
                            ordnungsrahmen_name="01 FIM Prozesskatalog (IPR)",
                            ordnungsrahmen_version=None,
                            klasse_id="115",
                            klasse_name="Angelegenheiten des Ausländer- und Staatsangehörigkeitsrecht",
                        ),
                        Klassifikation(
                            ordnungsrahmen_name="02 FIM Prozesskatalog (IPR)",
                            ordnungsrahmen_version=None,
                            klasse_id="115.03",
                            klasse_name="Aufenthaltsgenehmigungen",
                        ),
                        Klassifikation(
                            ordnungsrahmen_name="03 FIM Prozesskatalog (Leistungsbündel)",
                            ordnungsrahmen_version=None,
                            klasse_id="115.03.03",
                            klasse_name="Aufenthaltserlaubnis",
                        ),
                    ],
                    schlagwort=[],
                    zustandsangaben=Zustandsangaben(
                        erstellungszeitpunkt=None,
                        letzter_aenderungszeitpunkt=datetime.datetime(
                            2023, 11, 16, 23, 0, tzinfo=datetime.timezone.utc
                        ),
                        letzter_bearbeiter="FIM-Baustein Prozesse",
                        status=FreigabeStatus.IN_BEARBEITUNG,
                        anmerkung_letzte_aenderung="Neu erfasst",
                        gueltigkeitszeitraum=Zeitraum(
                            beginn=datetime.datetime(
                                2008, 2, 24, 23, 0, tzinfo=datetime.timezone.utc
                            ),
                            ende=None,
                            zusatz=[],
                        ),
                        fachlicher_freigabezeitpunkt=datetime.datetime(
                            2023, 9, 26, 22, 0, tzinfo=datetime.timezone.utc
                        ),
                        formeller_freigabezeitpunkt=datetime.datetime(
                            2023, 11, 16, 23, 0, tzinfo=datetime.timezone.utc
                        ),
                    ),
                )
            ],
        ),
        prozessbibliothek=ProzessBibliothek(
            name="FIM Prozessbibliothek Bund",
            verwaltungspolitische_kodierung=[],
            prozess=[
                Prozess(
                    id="99010023001000",
                    version="01.00.00",
                    name="Antrag Erteilung Aufenthaltserlaubnis aus familiären Gründen bearbeiten",
                    bezeichnung="Aufenthaltserlaubnis aus familiären Gründen Erteilung",
                    fachlich_freigebende_stelle="Bundesministerium des Innern und für Heimat (BMI)",
                    schlagwort=[],
                    klassifikation=[
                        Klassifikation(
                            ordnungsrahmen_name="FIM Prozesskatalog",
                            ordnungsrahmen_version=None,
                            klasse_id="99010023001000",
                            klasse_name="""Antrag Erteilung Aufenthaltserlaubnis aus familiären Gründen
                    bearbeiten""",
                        ),
                    ],
                    prozesssteckbrief=ProzessSteckbrief(
                        definition=None,
                        beschreibung="""Die Person stellt den Antrag auf einen Aufenthaltstitel bei
                    der Auslandsvertretung des Auswärtigen Amts oder der für den Wohnort der Person
                    zuständigen Ausländerbehörde.
                    Die Ausländerbehörde fragt ggf. bei anderen Behörden an.
                    Die zuständige Ausländerbehörde erteilt den Aufenthaltstitel oder lehnt den
                    Antrag der Person ab.""",
                        ausloeser=[
                            AusloeserErgebnis(
                                formular_id="D00000343",
                                prozess_id=None,
                                textuelle_beschreibung=None,
                            )
                        ],
                        ergebnis=[
                            AusloeserErgebnis(
                                formular_id="D00000074",
                                prozess_id="99012",
                                textuelle_beschreibung="some textuelle beschreibung",
                            ),
                            AusloeserErgebnis(
                                formular_id="D00000325",
                                prozess_id=None,
                                textuelle_beschreibung=None,
                            ),
                        ],
                        prozessteilnehmer=[
                            Prozessteilnehmer(
                                name="Antragstellende " "Person",
                                rolle=[Prozessrolle.INITIATOR],
                            ),
                            Prozessteilnehmer(
                                name="Ausländerbehörde",
                                rolle=[Prozessrolle.HAUPTAKTEUR],
                            ),
                            Prozessteilnehmer(
                                name="Dolmetscher", rolle=[Prozessrolle.MITWIRKENDER]
                            ),
                            Prozessteilnehmer(
                                name="Bundeskriminalamt (BKA)",
                                rolle=[Prozessrolle.MITWIRKENDER],
                            ),
                            Prozessteilnehmer(
                                name="Bundesdruckerei GmbH",
                                rolle=[Prozessrolle.MITWIRKENDER],
                            ),
                            Prozessteilnehmer(
                                name="Bundesamt für Migration und Flüchtlinge (BAMF)",
                                rolle=[Prozessrolle.MITWIRKENDER],
                            ),
                            Prozessteilnehmer(
                                name="Bundesverwaltungsamt (BVA)",
                                rolle=[Prozessrolle.MITWIRKENDER],
                            ),
                            Prozessteilnehmer(
                                name="Gericht", rolle=[Prozessrolle.MITWIRKENDER]
                            ),
                            Prozessteilnehmer(
                                name="Antragstellende Person",
                                rolle=[Prozessrolle.ERGEBENIS_EMPFAENGER],
                            ),
                        ],
                        handlungsgrundlage=[
                            Handlungsgrundlage(
                                name="§§ 27-36a " "AufenthG",
                                art=Handlungsgrundlagenart.GESETZ,
                                uri="\n"
                                "                        "
                                "https://www.gesetze-im-internet.de/aufenthg_2004/BJNR195010004.html#BJNR195010004BJNG000801310",
                                gueltigkeitszeitraum=None,
                            )
                        ],
                        detaillierungsstufe=Detaillierungsstufe.STAMMINFORMATIONEN,
                        verwaltungspolitische_kodierung=[
                            VerwaltungspolitischeKodierung(
                                kreis=None,
                                bezirk=None,
                                bundesland=Bundesland.SCHLESWIG_HOLSTEIN,
                                gemeindeschluessel=None,
                                regionalschluessel=None,
                                nation=None,
                                gemeindeverband=None,
                            ),
                            VerwaltungspolitischeKodierung(
                                kreis=None,
                                bezirk="02",
                                bundesland=None,
                                gemeindeschluessel=None,
                                regionalschluessel=None,
                                nation=None,
                                gemeindeverband=None,
                            ),
                            VerwaltungspolitischeKodierung(
                                kreis="03",
                                bezirk=None,
                                bundesland=None,
                                gemeindeschluessel=None,
                                regionalschluessel=None,
                                nation=None,
                                gemeindeverband=None,
                            ),
                            VerwaltungspolitischeKodierung(
                                kreis=None,
                                bezirk=None,
                                bundesland=None,
                                gemeindeschluessel="04",
                                regionalschluessel=None,
                                nation=None,
                                gemeindeverband=None,
                            ),
                            VerwaltungspolitischeKodierung(
                                kreis=None,
                                bezirk=None,
                                bundesland=None,
                                gemeindeschluessel=None,
                                regionalschluessel="05",
                                nation=None,
                                gemeindeverband=None,
                            ),
                            VerwaltungspolitischeKodierung(
                                kreis=None,
                                bezirk=None,
                                bundesland=None,
                                gemeindeschluessel=None,
                                regionalschluessel=None,
                                nation="06",
                                gemeindeverband=None,
                            ),
                            VerwaltungspolitischeKodierung(
                                kreis=None,
                                bezirk=None,
                                bundesland=None,
                                gemeindeschluessel=None,
                                regionalschluessel=None,
                                nation=None,
                                gemeindeverband="07",
                            ),
                        ],
                        zielvorgaben=None,
                        merkmal=[],
                    ),
                    prozessstrukturbeschreibung=Strukturbeschreibung(
                        modellierungsmethode=Modellierungsmethode(
                            ModellierungsmethodeType.FIM, freitext="FIM"
                        ),
                        strukturbeschreibung_fim=StrukturbeschreibungFim(
                            aktivitaetengruppe=[
                                Aktivitaetengruppe(
                                    referenzaktivitaetengruppe_typ=ReferenzaktivitaetengruppeType.INFORMATION_BEREITSTELLEN,
                                    referenzaktivitaetengruppe_version="1.00",
                                    id="38",
                                    name="Befragung und Rechtsfolgen hinweisen",
                                    sub_process_id="_a0ebdc9f-7a4a-4ba7-a054-91103549d225",
                                    beschreibung="Beschreibung mit spezifischen Attribut 2",
                                    handlungsgrundlage=[
                                        Handlungsgrundlage(
                                            name="§ 54 (2) Nr. 7 AufenthG",
                                            art=Handlungsgrundlagenart.GESETZ,
                                            uri="https://www.gesetze-im-internet.de/aufenthg_2004/__54.html",
                                            gueltigkeitszeitraum=None,
                                        )
                                    ],
                                    eigehende_daten=[],
                                    ausgehende_daten=[],
                                    fachverfahren=[],
                                    spezifische_attribute=SpezifischeAttribute(
                                        spezifische_attribute_1=None,
                                        spezifische_attribute_2=None,
                                        spezifische_attribute_3=SpezifischeAttribute3(
                                            formelle_pruefung=[
                                                FormellePruefung.VERFAHREN
                                            ]
                                        ),
                                        spezifische_attribute_4=None,
                                        spezifische_attribute_5=None,
                                        spezifische_attribute_6=None,
                                        spezifische_attribute_7=None,
                                        spezifische_attribute_8=None,
                                    ),
                                ),
                                Aktivitaetengruppe(
                                    referenzaktivitaetengruppe_typ=ReferenzaktivitaetengruppeType.BETEILIGUNG_DURCHFUEHREN,
                                    referenzaktivitaetengruppe_version="1.00",
                                    id="32",
                                    name="Aufenthaltstitel produzieren",
                                    sub_process_id="_cc41d6fd-8c02-498a-ab0c-da5569d3da98",
                                    beschreibung=None,
                                    handlungsgrundlage=[],
                                    eigehende_daten=[],
                                    ausgehende_daten=[],
                                    fachverfahren=[],
                                    spezifische_attribute=SpezifischeAttribute(
                                        spezifische_attribute_1=None,
                                        spezifische_attribute_2=None,
                                        spezifische_attribute_3=None,
                                        spezifische_attribute_4=None,
                                        spezifische_attribute_5=None,
                                        spezifische_attribute_6=None,
                                        spezifische_attribute_7=SpezifischeAttribute7(
                                            bereitgestellte_daten=[
                                                DatenBereitgestellt(
                                                    formularverweis=Daten(
                                                        formularverweis=Formularverweis(
                                                            formularsteckbrief_id="Aufenthaltstitel",
                                                            stammformular_id=None,
                                                            stammformular_version=None,
                                                            element_id=[],
                                                        ),
                                                        textuelle_beschreibung=None,
                                                    ),
                                                    uebermittlungsart=Uebermittlungsart.KEINE_VORGABE,
                                                    empfaenger="Ausländerbehörde",
                                                )
                                            ],
                                            empfangene_daten=[
                                                DatenEmpfangen(
                                                    formularverweis=Daten(
                                                        formularverweis=None,
                                                        textuelle_beschreibung="Bestelldaten",
                                                    ),
                                                    uebermittlungsart=Uebermittlungsart.ELEKTRONISCH_HALBAUTOMATISCH,
                                                    sender="Ausländerbehörde",
                                                )
                                            ],
                                            mitwirkungspflicht=True,
                                            beteiligungsform=Beteiligungsform.AUFTRAG,
                                        ),
                                        spezifische_attribute_8=None,
                                    ),
                                ),
                                Aktivitaetengruppe(
                                    referenzaktivitaetengruppe_typ=ReferenzaktivitaetengruppeType.SACHVERHALT_BEURTEILEN_ENTSCHEIDEN_MIT_SPIELRAUM,
                                    referenzaktivitaetengruppe_version="2.00",
                                    id="26",
                                    name="Name Spezifische Attribute 5",
                                    sub_process_id="_573fc526-db39-49d2-9c5a-96878540a194",
                                    beschreibung="Beschreibung Spezifische Attribute 5",
                                    handlungsgrundlage=[
                                        Handlungsgrundlage(
                                            name="§ 36 AufenthG",
                                            art=Handlungsgrundlagenart.GESETZ,
                                            uri="https://www.gesetze-im-internet.de/aufenthg_2004/__36.html",
                                            gueltigkeitszeitraum=None,
                                        )
                                    ],
                                    eigehende_daten=[],
                                    ausgehende_daten=[],
                                    fachverfahren=[],
                                    spezifische_attribute=SpezifischeAttribute(
                                        spezifische_attribute_1=None,
                                        spezifische_attribute_2=None,
                                        spezifische_attribute_3=None,
                                        spezifische_attribute_4=None,
                                        spezifische_attribute_5=SpezifischeAttribute5(
                                            hilfsmittel="keine",
                                            entscheidungsart=Entscheidungsart.AUSWAHLERMESSEN,
                                        ),
                                        spezifische_attribute_6=None,
                                        spezifische_attribute_7=None,
                                        spezifische_attribute_8=None,
                                    ),
                                ),
                                Aktivitaetengruppe(
                                    referenzaktivitaetengruppe_typ=ReferenzaktivitaetengruppeType.INFORMATION_BEREITSTELLEN,
                                    referenzaktivitaetengruppe_version="1.00",
                                    id="33",
                                    name="Aufenthaltstitel aushändigen",
                                    sub_process_id="_0e724c77-f0ae-4b37-92ca-c341f6307c90",
                                    beschreibung="Diese Aktivitätengruppe ist nicht direkt ableitbar.",
                                    handlungsgrundlage=[],
                                    eigehende_daten=[],
                                    ausgehende_daten=[],
                                    fachverfahren=[],
                                    spezifische_attribute=SpezifischeAttribute(
                                        spezifische_attribute_1=None,
                                        spezifische_attribute_2=SpezifischeAttribute2(
                                            bereitgestellte_daten=[
                                                DatenBereitgestellt(
                                                    formularverweis=Daten(
                                                        formularverweis=Formularverweis(
                                                            formularsteckbrief_id="Aufenthaltstitel",
                                                            stammformular_id=None,
                                                            stammformular_version=None,
                                                            element_id=[],
                                                        ),
                                                        textuelle_beschreibung=None,
                                                    ),
                                                    uebermittlungsart=Uebermittlungsart.KEINE_VORGABE,
                                                    empfaenger="Antragstellende "
                                                    "Person",
                                                )
                                            ]
                                        ),
                                        spezifische_attribute_3=None,
                                        spezifische_attribute_4=None,
                                        spezifische_attribute_5=None,
                                        spezifische_attribute_6=None,
                                        spezifische_attribute_7=None,
                                        spezifische_attribute_8=None,
                                    ),
                                ),
                                Aktivitaetengruppe(
                                    referenzaktivitaetengruppe_typ=ReferenzaktivitaetengruppeType.SACHVERHALT_BEURTEILEN_ENTSCHEIDEN_OHNE_SPIELRAUM,
                                    referenzaktivitaetengruppe_version="2.00",
                                    id="29",
                                    name="Dauer und Art des Aufenthaltstitels festlegen",
                                    sub_process_id="_e4c5e011-5ff7-48bf-97a6-74a5ee0c87b1",
                                    beschreibung="Beschreibung mit spezifischen Attribut 4",
                                    handlungsgrundlage=[
                                        Handlungsgrundlage(
                                            name="§ 27 (1), (4) " "AufenthG",
                                            art=Handlungsgrundlagenart.GESETZ,
                                            uri="https://www.gesetze-im-internet.de/aufenthg_2004/__27.html",
                                            gueltigkeitszeitraum=None,
                                        ),
                                        Handlungsgrundlage(
                                            name="§ 28 (2)-(4) " "AufenthG",
                                            art=Handlungsgrundlagenart.GESETZ,
                                            uri="https://www.gesetze-im-internet.de/aufenthg_2004/__28.html",
                                            gueltigkeitszeitraum=None,
                                        ),
                                        Handlungsgrundlage(
                                            name="§ 33 AufenthG",
                                            art=Handlungsgrundlagenart.GESETZ,
                                            uri="https://www.gesetze-im-internet.de/aufenthg_2004/__33.html",
                                            gueltigkeitszeitraum=None,
                                        ),
                                        Handlungsgrundlage(
                                            name="§ 34 (2) " "AufenthG",
                                            art=Handlungsgrundlagenart.GESETZ,
                                            uri="https://www.gesetze-im-internet.de/aufenthg_2004/__34.html",
                                            gueltigkeitszeitraum=None,
                                        ),
                                        Handlungsgrundlage(
                                            name="§ 35 (1) " "AufenthG",
                                            art=Handlungsgrundlagenart.GESETZ,
                                            uri="https://www.gesetze-im-internet.de/aufenthg_2004/__35.html",
                                            gueltigkeitszeitraum=None,
                                        ),
                                    ],
                                    eigehende_daten=[],
                                    ausgehende_daten=[],
                                    fachverfahren=[],
                                    spezifische_attribute=SpezifischeAttribute(
                                        spezifische_attribute_1=None,
                                        spezifische_attribute_2=None,
                                        spezifische_attribute_3=None,
                                        spezifische_attribute_4=SpezifischeAttribute4(
                                            hilfsmittel="keine"
                                        ),
                                        spezifische_attribute_5=None,
                                        spezifische_attribute_6=None,
                                        spezifische_attribute_7=None,
                                        spezifische_attribute_8=None,
                                    ),
                                ),
                                Aktivitaetengruppe(
                                    referenzaktivitaetengruppe_typ=ReferenzaktivitaetengruppeType.DATEN_ZUM_SACHVERHALT_BEARBEITEN,
                                    referenzaktivitaetengruppe_version="1.00",
                                    id="14",
                                    name="Fiktionsbescheinigung ausstellen",
                                    sub_process_id="_5130cde6-5f73-4450-834a-608e903cb469",
                                    beschreibung="§ 81 AufenthG (Beantragung des "
                                    "Aufenthaltstitels)",
                                    handlungsgrundlage=[
                                        Handlungsgrundlage(
                                            name="§ 81 (5) " "AufenthG",
                                            art=Handlungsgrundlagenart.GESETZ,
                                            uri="https://www.gesetze-im-internet.de/aufenthg_2004/__81.html",
                                            gueltigkeitszeitraum=None,
                                        )
                                    ],
                                    eigehende_daten=[],
                                    ausgehende_daten=[
                                        Daten(
                                            formularverweis=Formularverweis(
                                                formularsteckbrief_id="Fiktionsbescheinigung ",
                                                stammformular_id=None,
                                                stammformular_version=None,
                                                element_id=[],
                                            ),
                                            textuelle_beschreibung=None,
                                        )
                                    ],
                                    fachverfahren=[],
                                    spezifische_attribute=SpezifischeAttribute(
                                        spezifische_attribute_1=None,
                                        spezifische_attribute_2=None,
                                        spezifische_attribute_3=None,
                                        spezifische_attribute_4=None,
                                        spezifische_attribute_5=None,
                                        spezifische_attribute_6=SpezifischeAttribute6(
                                            bearbeitungsart=Bearbeitungsart.ERSTELLUNG
                                        ),
                                        spezifische_attribute_7=None,
                                        spezifische_attribute_8=None,
                                    ),
                                ),
                                Aktivitaetengruppe(
                                    referenzaktivitaetengruppe_typ=ReferenzaktivitaetengruppeType.INFORMATION_EMPFAHNGEN,
                                    referenzaktivitaetengruppe_version="1.00",
                                    id="36",
                                    name="Widerspruch entgegennehmen",
                                    sub_process_id="_b5991538-d434-44a7-b78a-537e3c31281b",
                                    beschreibung="§ 79 VwVfG (Rechtsbehelfe gegen Verwaltungsakte)",
                                    handlungsgrundlage=[
                                        Handlungsgrundlage(
                                            name="§ 79 VwVfG",
                                            art=Handlungsgrundlagenart.GESETZ,
                                            uri="http://www.gesetze-im-internet.de/vwvfg/__79.html",
                                            gueltigkeitszeitraum=None,
                                        )
                                    ],
                                    eigehende_daten=[],
                                    ausgehende_daten=[],
                                    fachverfahren=[],
                                    spezifische_attribute=SpezifischeAttribute(
                                        spezifische_attribute_1=SpezifischeAttribute1(
                                            empfangene_daten=[
                                                DatenEmpfangen(
                                                    formularverweis=Daten(
                                                        formularverweis=Formularverweis(
                                                            formularsteckbrief_id="Widerspruch "
                                                            "nach "
                                                            "VwGO",
                                                            stammformular_id=None,
                                                            stammformular_version=None,
                                                            element_id=[],
                                                        ),
                                                        textuelle_beschreibung=None,
                                                    ),
                                                    uebermittlungsart=Uebermittlungsart.KEINE_VORGABE,
                                                    sender="Antragstellende " "Person",
                                                )
                                            ]
                                        ),
                                        spezifische_attribute_2=None,
                                        spezifische_attribute_3=None,
                                        spezifische_attribute_4=None,
                                        spezifische_attribute_5=None,
                                        spezifische_attribute_6=None,
                                        spezifische_attribute_7=None,
                                        spezifische_attribute_8=None,
                                    ),
                                ),
                            ]
                        ),
                    ),
                    prozessmodell=ProzessModell(
                        prozessmodelldatei=[
                            Datei(
                                dateiname="BPMN DI-Export",
                                modellierungsmethode=Modellierungsmethode(
                                    code=ModellierungsmethodeType.FIM, freitext="FIM"
                                ),
                                mime_type=MimeType.PDF,
                                inhalt="PROZESSMODELLDATEIINHALT",
                            )
                        ],
                        visualisierungsdatei=[
                            Datei(
                                dateiname="Modellgrafik",
                                modellierungsmethode=Modellierungsmethode(
                                    code=ModellierungsmethodeType.FIM, freitext="FIM"
                                ),
                                mime_type=MimeType.PDF,
                                inhalt="VISUALISIERUNGSDATEIINHALT",
                            )
                        ],
                        beschreibungsdatei=[
                            Datei(
                                dateiname="Standard Report",
                                modellierungsmethode=Modellierungsmethode(
                                    code=ModellierungsmethodeType.FIM, freitext="FIM"
                                ),
                                mime_type=MimeType.PDF,
                                inhalt="BESCHREIBUNGSDATEIINHALT",
                            )
                        ],
                    ),
                    zustandsangaben=Zustandsangaben(
                        erstellungszeitpunkt=None,
                        letzter_aenderungszeitpunkt=datetime.datetime(
                            2023, 11, 30, 14, 0, tzinfo=datetime.timezone.utc
                        ),
                        letzter_bearbeiter="FIM-Baustein Prozesse",
                        status=FreigabeStatus.IN_BEARBEITUNG,
                        anmerkung_letzte_aenderung="Fachlich freigegeben durch Herrn Muster",
                        gueltigkeitszeitraum=Zeitraum(
                            beginn=datetime.datetime(
                                2004, 7, 29, 22, 0, tzinfo=datetime.timezone.utc
                            ),
                            ende=None,
                            zusatz=[],
                        ),
                        fachlicher_freigabezeitpunkt=datetime.datetime(
                            2023, 9, 26, 22, 0, tzinfo=datetime.timezone.utc
                        ),
                        formeller_freigabezeitpunkt=datetime.datetime(
                            2023, 12, 7, 10, 0, tzinfo=datetime.timezone.utc
                        ),
                    ),
                ),
            ],
        ),
    )
