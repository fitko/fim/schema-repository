from datetime import datetime, timezone

import pytest
from fastapi.testclient import TestClient

from fimportal.common import Anwendungsgebiet, Bundesland, FreigabeStatus
from fimportal.helpers import format_iso8601
from fimportal.xprozesse.prozess import (
    Detaillierungsstufe,
    parse_prozess_message,
)
from tests.conftest import CommandRunner
from tests.data import XPROZESS2_DATA
from tests.factories.prozess import ProzessFactory

ENDPOINT = "/api/v0/processes"


def test_return_empty_list(client: TestClient):
    response = client.get(ENDPOINT)

    assert response.status_code == 200

    assert response.json() == {
        "items": [],
        "count": 0,
        "offset": 0,
        "limit": 200,
        "total_count": 0,
    }


def test_return_existing_prozesse(runner: CommandRunner, client: TestClient):
    prozess = ProzessFactory(
        letzter_aenderungszeitpunkt=datetime(2023, 5, 5, tzinfo=timezone.utc)
    ).save(runner)
    assert prozess.zustandsangaben is not None
    assert prozess.zustandsangaben.letzter_aenderungszeitpunkt is not None

    response = client.get(ENDPOINT)

    assert response.status_code == 200
    assert response.json() == {
        "items": [
            {
                "id": prozess.id,
                "version": prozess.version,
                "name": prozess.name,
                "freigabe_status": prozess.freigabe_status,
                "letzter_aenderungszeitpunkt": format_iso8601(
                    prozess.zustandsangaben.letzter_aenderungszeitpunkt
                ),
                "fts_match": None,
            }
        ],
        "count": 1,
        "offset": 0,
        "limit": 200,
        "total_count": 1,
    }


def test_return_prozesse_by_freigabe_status(runner: CommandRunner, client: TestClient):
    prozess = ProzessFactory(
        freigabe_status=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
    ).save(runner)
    ProzessFactory(freigabe_status=FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER).save(
        runner
    )

    response = client.get(
        f"{ENDPOINT}?freigabe_status={FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value}"
    )

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["id"] == prozess.id


def test_return_prozesse_by_detaillierungsstufe(
    runner: CommandRunner, client: TestClient
):
    prozess = ProzessFactory(
        detaillierungsstufe=Detaillierungsstufe.LOKALINFORMATIONEN
    ).save(runner)
    ProzessFactory(
        detaillierungsstufe=Detaillierungsstufe.OZG_REFERENZINFORMATIONEN
    ).save(runner)

    response = client.get(
        f"{ENDPOINT}?detaillierungsstufe={Detaillierungsstufe.LOKALINFORMATIONEN.value}"
    )

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["id"] == prozess.id


def test_return_prozesse_by_anwendungsgebiet_bund(
    runner: CommandRunner, client: TestClient
):
    prozess = ProzessFactory(
        verwaltungspolitisch_kodierung=[
            Bundesland.SCHLESWIG_HOLSTEIN,
            Bundesland.HAMBURG,
            Bundesland.NIEDERSACHSEN,
            Bundesland.BREMEN,
            Bundesland.NORDRHEIN_WESTFALEN,
            Bundesland.HESSEN,
            Bundesland.RHEINLAND_PFALZ,
            Bundesland.BADEN_WUERTTEMBERG,
            Bundesland.BAYERN,
            Bundesland.SAARLAND,
            Bundesland.BERLIN,
            Bundesland.BRANDENBURG,
            Bundesland.MECKLENBURG_VORPOMMERN,
            Bundesland.SACHSEN,
            Bundesland.SACHSEN_ANHALT,
            Bundesland.THUERINGEN,
        ]
    ).save(runner)
    ProzessFactory(verwaltungspolitisch_kodierung=[Bundesland.BERLIN]).save(runner)

    response = client.get(f"{ENDPOINT}?anwendungsgebiet={Anwendungsgebiet.BUND.value}")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["id"] == prozess.id


def test_return_prozesse_by_anwendungsgebiet_single_bundesland(
    runner: CommandRunner, client: TestClient
):
    prozess = ProzessFactory(
        verwaltungspolitisch_kodierung=[
            Bundesland.BADEN_WUERTTEMBERG,
            Bundesland.SCHLESWIG_HOLSTEIN,
        ]
    ).save(runner)
    ProzessFactory(
        verwaltungspolitisch_kodierung=[Bundesland.BERLIN, Bundesland.BAYERN]
    ).save(runner)

    response = client.get(
        f"{ENDPOINT}?anwendungsgebiet={Anwendungsgebiet.BADEN_WUERTTEMBERG.value}"
    )

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["id"] == prozess.id


@pytest.mark.parametrize(
    "fts_query,fts_matches",
    [
        ("990100", ["99010023001000"]),
        ("fam", ["familiären"]),
        (
            "zuständige PERSON",
            ["Person", "zuständigen"],
        ),  # multiple searchterms are separated and connected with an and-relation
        ("https://www.gesetze-im-internet.de", ["www.gesetze-im-internet.de"]),
    ],
)
def test_get_prozesse_by_fts_query(
    runner: CommandRunner, client: TestClient, fts_query: str, fts_matches: list[str]
):
    ProzessFactory(detaillierungsstufe=Detaillierungsstufe.STAMMINFORMATIONEN).save(
        runner
    )
    with open(XPROZESS2_DATA / "prozess_without_files.xml", "rb") as file:
        data = file.read()

    prozess_message = parse_prozess_message(data)
    runner.import_prozess(prozess_message.prozess[0], data.decode("utf-8"))

    response = client.get(f"{ENDPOINT}?fts_query={fts_query}")
    assert response.status_code == 200

    result = response.json()
    assert result["total_count"] == 1
    assert prozess_message.prozess[0].id == result["items"][0]["id"]
    for match in fts_matches:
        assert f"[[[{match}]]]" in result["items"][0]["fts_match"]


def test_return_musterprozesse(runner: CommandRunner, client: TestClient):
    musterprozess = ProzessFactory(
        detaillierungsstufe=Detaillierungsstufe.MUSTERINFORMATIONEN
    ).save(runner)
    prozess = ProzessFactory(
        detaillierungsstufe=Detaillierungsstufe.STAMMINFORMATIONEN
    ).save(runner)

    response = client.get(f"{ENDPOINT}?is_musterprozess=false")

    assert response.status_code == 200
    assert response.json() == {
        "items": [
            {
                "id": prozess.id,
                "version": prozess.version,
                "name": prozess.name,
                "freigabe_status": prozess.freigabe_status,
                "letzter_aenderungszeitpunkt": None,
                "fts_match": None,
            }
        ],
        "count": 1,
        "offset": 0,
        "limit": 200,
        "total_count": 1,
    }

    response = client.get(f"{ENDPOINT}?is_musterprozess=true")

    assert response.status_code == 200
    assert response.json() == {
        "items": [
            {
                "id": musterprozess.id,
                "version": musterprozess.version,
                "name": musterprozess.name,
                "freigabe_status": musterprozess.freigabe_status,
                "letzter_aenderungszeitpunkt": None,
                "fts_match": None,
            }
        ],
        "count": 1,
        "offset": 0,
        "limit": 200,
        "total_count": 1,
    }


def test_should_count_prozess_without_detaillierungsstufe_as_prozess(
    runner: CommandRunner, client: TestClient
):
    prozess = ProzessFactory(detaillierungsstufe=None).save(runner)

    response = client.get(f"{ENDPOINT}?is_musterprozess=false")

    assert response.status_code == 200
    assert response.json() == {
        "items": [
            {
                "id": prozess.id,
                "version": prozess.version,
                "name": prozess.name,
                "freigabe_status": prozess.freigabe_status,
                "letzter_aenderungszeitpunkt": None,
                "fts_match": None,
            }
        ],
        "count": 1,
        "offset": 0,
        "limit": 200,
        "total_count": 1,
    }

    response = client.get(f"{ENDPOINT}?is_musterprozess=true")

    assert response.status_code == 200
    assert response.json() == {
        "items": [],
        "count": 0,
        "offset": 0,
        "limit": 200,
        "total_count": 0,
    }
