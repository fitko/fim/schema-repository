from fastapi.testclient import TestClient


def test_default_404_page(client: TestClient) -> None:
    response = client.get("/unknown")

    assert response.status_code == 404
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert "Diese Seite existiert nicht." in response.text
