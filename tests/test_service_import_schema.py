from datetime import date, datetime, timezone

import pytest
from freezegun import freeze_time

from fimportal.common import FreigabeStatus
from fimportal.din91379 import parse_string_latin
from fimportal.errors import (
    ImportException,
    StatusHasNotChangedException,
)
from fimportal.models.imports import ChildType, Xdf3SchemaImport
from fimportal.models.xdf3 import (
    DatenfeldOut,
    DirectChild,
    FullDatenfeldgruppeOut,
    FullDatenfeldOut,
    RegelReferenz,
    RelationOut,
    RuleOut,
    SchemaOut,
    XdfVersion,
)
from fimportal.service import Service
from fimportal.xdatenfelder.xdf3.common import (
    Anzahl,
    ElementReference,
    ElementType,
    Identifier,
    Relation,
    RelationTyp,
    Version,
)
from tests.factories.genericode import CodeListFactory, CodeListImportFactory
from tests.factories.xdf2 import Xdf2Factory
from tests.factories.xdf3 import (
    Xdf3Factory,
)


class TestStrictUpload:
    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_correctly_upload_a_schema(self, service: Service):
        factory = Xdf3Factory()
        field = factory.field(id="F12000").build()
        xdf3_import = factory.schema(id="S12000").with_field(field).build_import()

        await service.import_xdf3_schema(xdf3_import, strict=True)

        saved_schema = await service.get_schema(xdf3_import.id, xdf3_import.version)
        assert saved_schema is not None

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_correctly_save_fields(self, service: Service):
        factory = Xdf3Factory()
        field = factory.field(id="F12000").build()
        xdf3_import = factory.schema("S12000").with_field(field).build_import()
        schema = xdf3_import.schema_message.schema

        with freeze_time("2023-10-17T00:00:00"):
            await service.import_xdf3_schema(xdf3_import, strict=True)

        result = await service.get_field(
            "baukasten", field.identifier.id, field.identifier.assert_version()
        )

        assert result == FullDatenfeldOut(
            namespace="baukasten",
            fim_id=field.identifier.id,
            fim_version=field.identifier.assert_version(),
            nummernkreis="12000",
            xdf_version=XdfVersion.XDF3,
            name=field.name,
            beschreibung=field.beschreibung,
            definition=field.definition,
            bezug=[],
            freigabe_status=field.freigabe_status,
            freigabe_status_label=field.freigabe_status.to_label(),
            status_gesetzt_am=field.status_gesetzt_am,
            status_gesetzt_durch=field.status_gesetzt_durch,
            gueltig_ab=field.gueltig_ab,
            gueltig_bis=field.gueltig_bis,
            versionshinweis=field.versionshinweis,
            veroeffentlichungsdatum=field.veroeffentlichungsdatum,
            letzte_aenderung=field.letzte_aenderung,
            last_update=datetime(2023, 10, 17, tzinfo=timezone.utc),
            feldart=field.feldart,
            datentyp=field.datentyp,
            code_list=None,
            is_latest=True,
            regeln=[],
            relation=[],
            schemas=[
                SchemaOut(
                    fim_id=schema.identifier.id,
                    fim_version=schema.identifier.assert_version(),
                    nummernkreis=schema.identifier.nummernkreis,
                    name=schema.name,
                    beschreibung=schema.beschreibung,
                    definition=schema.definition,
                    freigabe_status=schema.freigabe_status,
                    freigabe_status_label=schema.freigabe_status.to_label(),
                    gueltig_ab=schema.gueltig_ab,
                    gueltig_bis=schema.gueltig_bis,
                    status_gesetzt_durch=schema.status_gesetzt_durch,
                    steckbrief_id=schema.dokumentsteckbrief.id,
                    xdf_version=XdfVersion.XDF3,
                    bezug=[],
                    versionshinweis=schema.versionshinweis,
                    stichwort=[],
                    letzte_aenderung=schema.letzte_aenderung,
                    last_update=datetime(2023, 10, 17, tzinfo=timezone.utc),
                    status_gesetzt_am=schema.status_gesetzt_am,
                    bezeichnung=schema.bezeichnung,
                    veroeffentlichungsdatum=schema.veroeffentlichungsdatum,
                    bezug_components=[],
                    is_latest=True,
                    fts_match=None,
                )
            ],
        )

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_correctly_save_groups(self, service: Service):
        factory = Xdf3Factory()
        field = factory.field(id="F12000").build()
        group = factory.group(id="G12000").with_field(field).build()
        xdf3_import = factory.schema("S12000").with_group(group).build_import()
        schema = xdf3_import.schema_message.schema

        with freeze_time("2023-10-17T00:00:00"):
            await service.import_xdf3_schema(xdf3_import, strict=True)

        result = await service.get_group(
            "baukasten", group.identifier.id, group.identifier.assert_version()
        )

        assert result == FullDatenfeldgruppeOut(
            namespace="baukasten",
            fim_id=group.identifier.id,
            fim_version=group.identifier.assert_version(),
            nummernkreis="12000",
            xdf_version=XdfVersion.XDF3,
            name=group.name,
            beschreibung=group.beschreibung,
            definition=group.definition,
            bezug=[],
            freigabe_status=group.freigabe_status,
            freigabe_status_label=group.freigabe_status.to_label(),
            status_gesetzt_am=group.status_gesetzt_am,
            status_gesetzt_durch=group.status_gesetzt_durch,
            gueltig_ab=group.gueltig_ab,
            gueltig_bis=group.gueltig_bis,
            versionshinweis=group.versionshinweis,
            veroeffentlichungsdatum=group.veroeffentlichungsdatum,
            letzte_aenderung=group.letzte_aenderung,
            last_update=datetime(2023, 10, 17, tzinfo=timezone.utc),
            is_latest=True,
            relation=[],
            schemas=[
                SchemaOut(
                    fim_id=schema.identifier.id,
                    fim_version=schema.identifier.assert_version(),
                    nummernkreis=schema.identifier.nummernkreis,
                    name=schema.name,
                    beschreibung=schema.beschreibung,
                    definition=schema.definition,
                    freigabe_status=schema.freigabe_status,
                    freigabe_status_label=schema.freigabe_status.to_label(),
                    gueltig_ab=schema.gueltig_ab,
                    gueltig_bis=schema.gueltig_bis,
                    status_gesetzt_durch=schema.status_gesetzt_durch,
                    steckbrief_id=schema.dokumentsteckbrief.id,
                    xdf_version=XdfVersion.XDF3,
                    bezug=[],
                    versionshinweis=schema.versionshinweis,
                    stichwort=[],
                    letzte_aenderung=schema.letzte_aenderung,
                    last_update=datetime(2023, 10, 17, tzinfo=timezone.utc),
                    status_gesetzt_am=schema.status_gesetzt_am,
                    bezeichnung=schema.bezeichnung,
                    veroeffentlichungsdatum=schema.veroeffentlichungsdatum,
                    bezug_components=[],
                    is_latest=True,
                    fts_match=None,
                )
            ],
            datenfelder=[
                DatenfeldOut(
                    namespace="baukasten",
                    fim_id=field.identifier.id,
                    fim_version=field.identifier.assert_version(),
                    nummernkreis=field.identifier.nummernkreis,
                    name=field.name,
                    beschreibung=field.beschreibung,
                    definition=field.definition,
                    bezug=[],
                    freigabe_status=field.freigabe_status,
                    freigabe_status_label=field.freigabe_status.to_label(),
                    status_gesetzt_am=field.status_gesetzt_am,
                    status_gesetzt_durch=field.status_gesetzt_durch,
                    gueltig_ab=field.gueltig_ab,
                    gueltig_bis=field.gueltig_bis,
                    versionshinweis=field.versionshinweis,
                    veroeffentlichungsdatum=field.veroeffentlichungsdatum,
                    letzte_aenderung=field.letzte_aenderung,
                    last_update=datetime(2023, 10, 17, tzinfo=timezone.utc),
                    feldart=field.feldart,
                    datentyp=field.datentyp,
                    xdf_version=XdfVersion.XDF3,
                    fts_match=None,
                    is_latest=True,
                )
            ],
            datenfeldgruppen=[],
            children=[
                DirectChild(
                    namespace="baukasten",
                    fim_id=field.identifier.id,
                    fim_version=field.identifier.assert_version(),
                    type=ChildType.FIELD,
                    anzahl=str(schema.struktur[0].anzahl),
                    bezug=[bezug.text for bezug in schema.struktur[0].bezug],
                )
            ],
            regeln=[],
        )

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_fail_to_create_fields_from_another_nummernkreis(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(id="F12345").build()
        xdf3_import = factory.schema(id="S00001").with_field(field).build_import()

        with pytest.raises(
            ImportException,
            match="Cannot create field F12345: The field is from another Nummernkreis and not yet published to the Sammelrepository by the original author. Creating fields, groups or rules from another Nummernkreis is not allowed.",
        ):
            await service.import_xdf3_schema(xdf3_import, strict=True)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_reuse_fields_from_another_nummernkreis(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(id="F12345").build()
        xdf3_import = factory.schema(id="S12345").with_field(field).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        xdf3_import = factory.schema(id="S00001").with_field(field).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        saved_schema = await service.get_schema(xdf3_import.id, xdf3_import.version)

        assert saved_schema is not None
        assert len(saved_schema.datenfelder) == 1
        assert saved_schema.datenfelder[0].fim_id == "F12345"

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_fail_to_create_groups_from_another_nummernkreis(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(id="F00001").build()
        group = factory.group(id="G12000").with_field(field).build()
        xdf3_import = factory.schema("S00001").with_group(group).build_import()

        with pytest.raises(
            ImportException,
            match="Cannot create group G12000: The group is from another Nummernkreis and not yet published to the Sammelrepository by the original author. Creating fields, groups or rules from another Nummernkreis is not allowed.",
        ):
            await service.import_xdf3_schema(xdf3_import, strict=True)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_update_existing_fields_from_the_same_nummernkreis(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(
            id="F12000", freigabe_status=FreigabeStatus.ENTWURF
        ).build()
        xdf3_import = factory.schema(id="S120001").with_field(field).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        field.freigabe_status = FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
        xdf3_import = factory.schema(id="S120002").with_field(field).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        result = await service.get_field(
            "baukasten", field.identifier.id, field.identifier.assert_version()
        )

        assert result is not None
        assert result.freigabe_status == FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_update_existing_groups_from_the_same_nummernkreis(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(id="F12000").build()
        group = (
            factory.group(id="G12000", freigabe_status=FreigabeStatus.ENTWURF)
            .with_field(field)
            .build()
        )
        xdf3_import = factory.schema("S12000").with_group(group).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        group.freigabe_status = FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
        xdf3_import = factory.schema(id="S120002").with_group(group).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        result = await service.get_group(
            "baukasten", group.identifier.id, group.identifier.assert_version()
        )

        assert result is not None
        assert result.freigabe_status == FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_not_update_fields_from_another_nummernkreis(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(
            id="F00001", freigabe_status=FreigabeStatus.ENTWURF
        ).build()
        xdf3_import = factory.schema(id="S00001").with_field(field).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        field.freigabe_status = FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
        xdf3_import = factory.schema(id="S12345").with_field(field).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        result = await service.get_field(
            "baukasten", field.identifier.id, field.identifier.assert_version()
        )

        assert result is not None
        assert result.freigabe_status == FreigabeStatus.ENTWURF

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_not_update_existing_groups_from_another_nummernkreis(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(id="F12000").build()
        group = (
            factory.group(id="G12000", freigabe_status=FreigabeStatus.ENTWURF)
            .with_field(field)
            .build()
        )
        xdf3_import = factory.schema("S12000").with_group(group).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        group.freigabe_status = FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
        xdf3_import = factory.schema(id="S123456").with_group(group).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        result = await service.get_group(
            "baukasten", group.identifier.id, group.identifier.assert_version()
        )

        assert result is not None
        assert result.freigabe_status == FreigabeStatus.ENTWURF

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_work_with_duplicate_childs(self, service: Service):
        factory = Xdf3Factory()
        field = factory.field(id="F12000").build()
        xdf3_import = (
            factory.schema(id="S12000")
            .with_field(field)
            .with_field(field)
            .build_import()
        )

        await service.import_xdf3_schema(xdf3_import, strict=True)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_save_elements_from_the_nummernkreis_99_in_the_schema_namespace(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(id="F99000").build()
        group = factory.group(id="G99000").with_field(field).build()
        xdf3_import = (
            factory.schema(id="S12345")
            .with_group(group)
            .with_field(field)
            .build_import()
        )
        await service.import_xdf3_schema(xdf3_import, strict=True)

        saved_schema = await service.get_schema(xdf3_import.id, xdf3_import.version)

        assert saved_schema is not None

        assert len(saved_schema.datenfelder) == 1
        assert saved_schema.datenfelder[0].namespace == "S12345"
        assert saved_schema.datenfelder[0].fim_id == "F99000"

        assert len(saved_schema.datenfeldgruppen) == 1
        assert saved_schema.datenfeldgruppen[0].namespace == "S12345"
        assert saved_schema.datenfeldgruppen[0].fim_id == "G99000"

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_update_elements_from_the_nummernkreis_99_in_the_schema_namespace(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(
            id="F99000", freigabe_status=FreigabeStatus.ENTWURF
        ).build()
        group = (
            factory.group(id="G99000", freigabe_status=FreigabeStatus.ENTWURF)
            .with_field(field)
            .build()
        )
        xdf3_import = (
            factory.schema(id="S12345")
            .with_group(group)
            .with_field(field)
            .build_import()
        )
        await service.import_xdf3_schema(xdf3_import, strict=True)

        field.freigabe_status = FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
        group.freigabe_status = FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
        await service.import_xdf3_schema(xdf3_import, strict=True)

        saved_schema = await service.get_schema(xdf3_import.id, xdf3_import.version)

        assert saved_schema is not None
        assert (
            saved_schema.datenfelder[0].freigabe_status
            == FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
        )
        assert (
            saved_schema.datenfeldgruppen[0].freigabe_status
            == FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
        )


class TestIsLatestColumn:
    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_create_schema_with_is_latest_true(self, service: Service):
        factory = Xdf3Factory()
        xdf3_import = (
            factory.schema(id="S12000", version="12.34.5").with_field().build_import()
        )
        await service.import_xdf3_schema(xdf3_import, strict=True)
        saved_schema = await service.get_schema("S12000", "12.34.5")
        assert saved_schema is not None
        assert saved_schema.is_latest is True

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_update_is_latest_to_highest_schema_version(
        self, service: Service
    ):
        factory = Xdf3Factory()
        xdf3_import = (
            factory.schema(id="S12000", version="12.34.5").with_field().build_import()
        )
        await service.import_xdf3_schema(xdf3_import, strict=True)
        xdf3_import = (
            factory.schema(id="S12000", version="12.35.0").with_field().build_import()
        )
        await service.import_xdf3_schema(xdf3_import, strict=True)
        saved_schema = await service.get_schema("S12000", "12.34.5")
        assert saved_schema is not None
        assert saved_schema.is_latest is False

        saved_schema = await service.get_schema("S12000", "12.35.0")
        assert saved_schema is not None
        assert saved_schema.is_latest is True

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_keep_is_latest_on_highest_schema_version(
        self, service: Service
    ):
        factory = Xdf3Factory()
        xdf3_import = (
            factory.schema(id="S12000", version="12.0.0").with_field().build_import()
        )
        await service.import_xdf3_schema(xdf3_import, strict=True)
        xdf3_import = (
            factory.schema(id="S12000", version="10.0.0").with_field().build_import()
        )
        await service.import_xdf3_schema(xdf3_import, strict=True)
        saved_schema = await service.get_schema("S12000", "12.0.0")
        assert saved_schema is not None
        assert saved_schema.is_latest is True

        saved_schema = await service.get_schema("S12000", "10.0.0")
        assert saved_schema is not None
        assert saved_schema.is_latest is False

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_create_group_with_is_latest_true(self, service: Service):
        factory = Xdf3Factory()
        field = factory.field(id="F12000").build()
        group = factory.group(id="G12000", version="12.34.5").with_field(field).build()
        xdf3_import = (
            factory.schema(id="S12000")
            .with_group(group)
            .with_field(field)
            .build_import()
        )
        await service.import_xdf3_schema(xdf3_import, strict=True)
        saved_group = await service.get_group("baukasten", "G12000", "12.34.5")
        assert saved_group is not None
        assert saved_group.is_latest is True

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_update_is_latest_to_highest_group_version(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(id="F12000").build()
        group = factory.group(id="G12000", version="12.34.5").with_field(field).build()
        xdf3_import = (
            factory.schema(id="S120001")
            .with_group(group)
            .with_field(field)
            .build_import()
        )
        await service.import_xdf3_schema(xdf3_import, strict=True)
        group = factory.group(id="G12000", version="12.35.0").with_field(field).build()
        xdf3_import = (
            factory.schema(id="S120002")
            .with_group(group)
            .with_field(field)
            .build_import()
        )
        await service.import_xdf3_schema(xdf3_import, strict=True)
        saved_group = await service.get_group("baukasten", "G12000", "12.34.5")
        assert saved_group is not None
        assert saved_group.is_latest is False
        saved_group = await service.get_group("baukasten", "G12000", "12.35.0")
        assert saved_group is not None
        assert saved_group.is_latest is True

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_keep_is_latest_on_highest_group_version(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(id="F12000").build()
        group = factory.group(id="G12000", version="12.0.0").with_field(field).build()
        xdf3_import = (
            factory.schema(id="S120001")
            .with_group(group)
            .with_field(field)
            .build_import()
        )
        await service.import_xdf3_schema(xdf3_import, strict=True)
        group = factory.group(id="G12000", version="10.0.0").with_field(field).build()
        xdf3_import = (
            factory.schema(id="S120002")
            .with_group(group)
            .with_field(field)
            .build_import()
        )
        await service.import_xdf3_schema(xdf3_import, strict=True)
        saved_group = await service.get_group("baukasten", "G12000", "12.0.0")
        assert saved_group is not None
        assert saved_group.is_latest is True

        saved_group = await service.get_group("baukasten", "G12000", "10.0.0")
        assert saved_group is not None
        assert saved_group.is_latest is False

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_create_field_with_is_latest_true(self, service: Service):
        factory = Xdf3Factory()
        field = factory.field(id="F12000", version="12.34.5").build()
        xdf3_import = factory.schema(id="S12000").with_field(field).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        saved_field = await service.get_field("baukasten", "F12000", "12.34.5")
        assert saved_field is not None
        assert saved_field.is_latest is True

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_update_is_latest_to_highest_field_version(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(id="F12000", version="12.34.5").build()
        xdf3_import = factory.schema(id="S120001").with_field(field).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        field = factory.field(id="F12000", version="12.35.0").build()
        xdf3_import = factory.schema(id="S120002").with_field(field).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        saved_field = await service.get_field("baukasten", "F12000", "12.34.5")
        assert saved_field is not None
        assert saved_field.is_latest is False

        saved_field = await service.get_field("baukasten", "F12000", "12.35.0")
        assert saved_field is not None
        assert saved_field.is_latest is True

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_keep_is_latest_on_highest_field_version(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(id="F12000", version="12.0.0").build()
        xdf3_import = factory.schema(id="S120001").with_field(field).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        field = factory.field(id="F12000", version="10.0.0").build()
        xdf3_import = factory.schema(id="S120002").with_field(field).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        saved_field = await service.get_field("baukasten", "F12000", "12.0.0")
        assert saved_field is not None
        assert saved_field.is_latest is True

        saved_field = await service.get_field("baukasten", "F12000", "10.0.0")
        assert saved_field is not None
        assert saved_field.is_latest is False


class TestRuleImport:
    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_correctly_save_rule(self, service: Service):
        factory = Xdf3Factory()
        xdf3_import = (
            factory.schema("S12000")
            .with_rule(
                factory.rule(
                    id="R12000",
                    version="1.2.3",
                    name="Debra Vazquez",
                    letzte_aenderung=datetime(2024, 1, 1, tzinfo=timezone.utc),
                ).build()
            )
            .with_field()
            .build_import()
        )

        with freeze_time("2024-11-13T00:00:00"):
            await service.import_xdf3_schema(xdf3_import, strict=True)

        rule = await service.get_rule(rule_id="R12000", rule_version="1.2.3")
        assert rule is not None
        assert rule == RuleOut(
            fim_id="R12000",
            fim_version="1.2.3",
            nummernkreis="12000",
            name="Debra Vazquez",
            letzte_aenderung=datetime(2024, 1, 1, tzinfo=timezone.utc),
            last_update=datetime(2024, 11, 13, tzinfo=timezone.utc),
            xdf_version=XdfVersion.XDF3,
        )

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_fail_to_create_rule_from_another_nummernkreis(
        self, service: Service
    ):
        factory = Xdf3Factory()
        xdf3_import = (
            factory.schema("S12000")
            .with_rule(factory.rule(id="R13000").build())
            .with_field()
            .build_import()
        )
        with pytest.raises(
            ImportException,
            match="Cannot create rule R13000: The rule is from another Nummernkreis and not yet published to the Sammelrepository by the original author. Creating fields, groups or rules from another Nummernkreis is not allowed.",
        ):
            await service.import_xdf3_schema(xdf3_import, strict=True)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_not_update_existing_rules_from_another_nummernkreis(
        self, service: Service
    ):
        factory = Xdf3Factory()
        rule = factory.rule(
            id="R12000",
            version="1.2.3",
            letzte_aenderung=datetime(2023, 12, 31, tzinfo=timezone.utc),
        ).build()
        xdf3_import = (
            factory.schema("S12000").with_rule(rule).with_field().build_import()
        )
        await service.import_xdf3_schema(xdf3_import, strict=True)

        rule.letzte_aenderung = datetime(2024, 1, 1, tzinfo=timezone.utc)
        xdf3_import = (
            factory.schema(id="S123456").with_rule(rule).with_field().build_import()
        )
        await service.import_xdf3_schema(xdf3_import, strict=True)

        saved_rule = await service.get_rule(rule_id="R12000", rule_version="1.2.3")

        assert saved_rule is not None
        assert saved_rule.letzte_aenderung == datetime(
            2023, 12, 31, tzinfo=timezone.utc
        )

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_create_schema_rule_connection(self, service: Service):
        factory = Xdf3Factory()
        xdf3_import = (
            factory.schema(id="S12000", version="1.0.0")
            .with_rule(factory.rule(id="R12000", version="1.2.3").build())
            .with_field()
            .build_import()
        )

        await service.import_xdf3_schema(xdf3_import, strict=True)

        schema = await service.get_schema(fim_id="S12000", fim_version="1.0.0")
        assert schema is not None
        assert len(schema.regeln) == 1
        assert schema.regeln[0] == RegelReferenz(fim_id="R12000", fim_version="1.2.3")

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_create_group_rule_connection(self, service: Service):
        factory = Xdf3Factory()
        field = factory.field("F12000").build()
        rule = factory.rule(id="R12000", version="1.2.3").build()

        group = (
            factory.group(id="G12000", version="0.2.0")
            .with_rule(rule)
            .with_field(field)
        ).build()

        xdf3_import = (
            factory.schema(id="S12000", version="1.0.0")
            .with_group(group)
            .build_import()
        )

        await service.import_xdf3_schema(xdf3_import, strict=True)

        result = await service.get_group(
            namespace="baukasten", fim_id="G12000", fim_version="0.2.0"
        )
        assert result is not None
        assert len(result.regeln) == 1
        assert result.regeln[0] == RegelReferenz(fim_id="R12000", fim_version="1.2.3")

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_create_field_rule_connection(self, service: Service):
        factory = Xdf3Factory()
        rule = factory.rule(id="R12000", version="1.2.3").build()
        field = factory.field(id="F12000", version="0.0.3").with_rule(rule).build()

        xdf3_import = (
            factory.schema(id="S12000", version="1.0.0")
            .with_field(field)
            .build_import()
        )

        await service.import_xdf3_schema(xdf3_import, strict=True)

        result = await service.get_field(
            namespace="baukasten", fim_id="F12000", fim_version="0.0.3"
        )
        assert result is not None
        assert len(result.regeln) == 1
        assert result.regeln[0] == RegelReferenz(fim_id="R12000", fim_version="1.2.3")


class TestLegacyUpload:
    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_create_fields_from_another_nummernkreis(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(id="F12345").build()
        xdf3_import = factory.schema(id="S00001").with_field(field).build_import()

        await service.import_xdf3_schema(xdf3_import, strict=False)

        saved_field = await service.get_field(
            "baukasten", field.identifier.id, field.identifier.assert_version()
        )
        assert saved_field is not None

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_create_groups_from_another_nummernkreis(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(id="F00001").build()
        group = factory.group(id="G12000").with_field(field).build()
        xdf3_import = factory.schema("S00001").with_group(group).build_import()

        await service.import_xdf3_schema(xdf3_import, strict=False)

        saved_group = await service.get_group(
            "baukasten", group.identifier.id, group.identifier.assert_version()
        )
        assert saved_group is not None

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_not_update_existing_groups_from_another_nummernkreis(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(id="F12000").build()
        group = (
            factory.group(id="G12000", freigabe_status=FreigabeStatus.ENTWURF)
            .with_field(field)
            .build()
        )
        xdf3_import = factory.schema("S12000").with_group(group).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=False)

        group.freigabe_status = FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
        xdf3_import = factory.schema(id="S123456").with_group(group).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=False)

        result = await service.get_group(
            "baukasten", group.identifier.id, group.identifier.assert_version()
        )

        assert result is not None
        assert result.freigabe_status == FreigabeStatus.ENTWURF

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_create_multiple_groups(self, service: Service):
        factory = Xdf3Factory()
        field = factory.field(id="F00001").build()
        group = factory.group(id="G12000").with_field(field).build()
        group_2 = factory.group(id="G13000").with_field(field).build()
        xdf3_import = (
            factory.schema("S00001")
            .with_group(group)
            .with_group(group_2)
            .build_import()
        )

        await service.import_xdf3_schema(xdf3_import, strict=False)

        saved_group = await service.get_group(
            "baukasten", group.identifier.id, group.identifier.assert_version()
        )
        assert saved_group is not None


class TestStatusCheck:
    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_not_overwrite_a_schema_if_the_status_is_identical(
        self, service: Service
    ):
        factory = Xdf3Factory()
        xdf3_import = factory.schema().full().build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        with pytest.raises(StatusHasNotChangedException):
            await service.import_xdf3_schema(
                xdf3_import, strict=True, check_status=True
            )

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_overwrite_a_schema_if_the_status_has_changed(
        self, service: Service
    ):
        factory = Xdf3Factory()
        xdf3_import = (
            factory.schema(freigabe_status=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD)
            .full()
            .build_import()
        )
        await service.import_xdf3_schema(xdf3_import, strict=True)

        xdf3_import.schema_message.schema.freigabe_status = (
            FreigabeStatus.VORGESEHEN_ZUM_LOESCHEN
        )

        await service.import_xdf3_schema(xdf3_import, strict=True, check_status=True)

        schema = await service.get_schema(
            xdf3_import.id,
            xdf3_import.version,
        )

        assert schema is not None
        assert schema.freigabe_status == FreigabeStatus.VORGESEHEN_ZUM_LOESCHEN


class TestNoStatusCheck:
    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_overwrite_a_schema_if_the_status_is_identical(
        self, service: Service
    ):
        factory = Xdf3Factory()
        xdf3_import = (
            factory.schema(letzte_aenderung=datetime(2023, 12, 31, tzinfo=timezone.utc))
            .full()
            .build_import()
        )
        await service.import_xdf3_schema(xdf3_import, strict=True)

        xdf3_import.schema_message.schema.letzte_aenderung = datetime(
            2024, 1, 1, tzinfo=timezone.utc
        )
        await service.import_xdf3_schema(xdf3_import, strict=True, check_status=False)

        schema = await service.get_schema(
            xdf3_import.id,
            xdf3_import.version,
        )

        assert schema is not None
        assert schema.letzte_aenderung == datetime(2024, 1, 1, tzinfo=timezone.utc)


class TestImmutabilityConstraints:
    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_allow_updating_steckbrief_id(self, service: Service):
        factory = Xdf3Factory()
        message = (
            factory.schema(dokumentsteckbrief=Identifier("D12345", None))
            .full()
            .message()
        )

        await service.import_xdf3_schema(Xdf3SchemaImport.from_message(message))

        message.schema.dokumentsteckbrief = Identifier("D54321", None)
        await service.import_xdf3_schema(Xdf3SchemaImport.from_message(message))

        saved_schema = await service.get_schema(message.id, message.assert_version())
        assert saved_schema is not None
        assert saved_schema.steckbrief_id == "D54321"

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_allow_empty_relation_version(self, service: Service):
        factory = Xdf3Factory()
        field = factory.field(
            relation=[
                Relation(
                    RelationTyp.IST_ABGELEITET_VON,
                    Identifier("F12345", None),
                ),
            ]
        ).build()
        group = (
            factory.group(
                relation=[
                    Relation(
                        RelationTyp.IST_ABGELEITET_VON,
                        Identifier("G12345", None),
                    ),
                ]
            )
            .with_field(field)
            .build()
        )
        schema_factory = factory.schema(
            relation=[
                Relation(
                    RelationTyp.IST_ABGELEITET_VON,
                    Identifier("S12345", None),
                )
            ]
        ).with_group(group)

        await service.import_xdf3_schema(schema_factory.build_import())

        saved_field = await service.get_field(
            "baukasten", field.identifier.id, field.identifier.assert_version()
        )
        assert saved_field is not None
        assert saved_field.relation == [
            RelationOut(
                typ=RelationTyp.IST_ABGELEITET_VON,
                fim_id="F12345",
                fim_version=None,
                name=None,
            )
        ]

        saved_group = await service.get_group(
            "baukasten", group.identifier.id, group.identifier.assert_version()
        )
        assert saved_group is not None
        assert saved_group.relation == [
            RelationOut(
                typ=RelationTyp.IST_ABGELEITET_VON,
                fim_id="G12345",
                fim_version=None,
                name=None,
            )
        ]

        saved_schema = await service.get_schema(
            schema_factory.id, schema_factory.version
        )
        assert saved_schema is not None
        assert saved_schema.relation == [
            RelationOut(
                typ=RelationTyp.IST_ABGELEITET_VON,
                fim_id="S12345",
                fim_version=None,
                name=None,
            )
        ]

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_allow_updating_relations(self, service: Service):
        factory = Xdf3Factory()
        field = factory.field(
            relation=[
                Relation(
                    RelationTyp.IST_ABGELEITET_VON,
                    Identifier("F12345", Version("1.0.0")),
                ),
            ]
        ).build()
        group = (
            factory.group(
                relation=[
                    Relation(
                        RelationTyp.IST_ABGELEITET_VON,
                        Identifier("G12345", Version("1.0.0")),
                    ),
                ]
            )
            .with_field(field)
            .build()
        )
        message = (
            factory.schema(
                relation=[
                    Relation(
                        RelationTyp.IST_ABGELEITET_VON,
                        Identifier("S12345", Version("1.0.0")),
                    )
                ]
            )
            .with_group(group)
            .message()
        )

        await service.import_xdf3_schema(Xdf3SchemaImport.from_message(message))

        field.relation = [
            Relation(
                RelationTyp.IST_ABGELEITET_VON, Identifier("F54321", Version("1.0.0"))
            )
        ]
        group.relation = [
            Relation(
                RelationTyp.IST_ABGELEITET_VON, Identifier("G54321", Version("1.0.0"))
            )
        ]
        message.schema.relation = [
            Relation(
                RelationTyp.IST_ABGELEITET_VON, Identifier("S54321", Version("1.0.0"))
            )
        ]
        await service.import_xdf3_schema(Xdf3SchemaImport.from_message(message))

        saved_field = await service.get_field(
            "baukasten", field.identifier.id, field.identifier.assert_version()
        )
        assert saved_field is not None
        assert saved_field.relation == [
            RelationOut(
                typ=RelationTyp.IST_ABGELEITET_VON,
                fim_id="F54321",
                fim_version="1.0.0",
                name=None,
            )
        ]

        saved_group = await service.get_group(
            "baukasten", group.identifier.id, group.identifier.assert_version()
        )
        assert saved_group is not None
        assert saved_group.relation == [
            RelationOut(
                typ=RelationTyp.IST_ABGELEITET_VON,
                fim_id="G54321",
                fim_version="1.0.0",
                name=None,
            )
        ]

        saved_schema = await service.get_schema(message.id, message.assert_version())
        assert saved_schema is not None
        assert saved_schema.relation == [
            RelationOut(
                typ=RelationTyp.IST_ABGELEITET_VON,
                fim_id="S54321",
                fim_version="1.0.0",
                name=None,
            )
        ]

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_not_import_schema_with_changed_structure(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field_1 = factory.field().build()
        field_2 = factory.field().build()
        message = factory.schema().with_field(field_1).message()

        await service.import_xdf3_schema(Xdf3SchemaImport.from_message(message))

        # Reset the structure of the schema and add the other field
        message.schema.struktur = []
        message.schema.struktur.append(
            ElementReference(Anzahl(1, 1), [], ElementType.FELD, field_2.identifier)
        )
        message.fields[field_2.identifier] = field_2

        with pytest.raises(
            ImportException,
            match=f"Cannot update schema {message.id} version {message.version}",
        ):
            await service.import_xdf3_schema(Xdf3SchemaImport.from_message(message))

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_not_import_schema_with_changed_immutable_attribute_in_group(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field().build()
        group = factory.group(name="Original Name").with_field(field).build()
        xdf3_import = factory.schema().with_group(group).build_import()
        await service.import_xdf3_schema(xdf3_import)

        group.name = parse_string_latin("Changed Name")

        with pytest.raises(
            ImportException,
            match=f"Cannot update group {group.identifier.id} version {group.identifier.version}",
        ):
            await service.import_xdf3_schema(xdf3_import)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_not_import_schema_with_changed_immutable_attribute_in_field(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(name="Original Name").build()
        xdf3_import = factory.schema().with_field(field).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        field.name = parse_string_latin("Changed Name")

        with pytest.raises(
            ImportException,
            match=f"Cannot update field {field.identifier.id} version {field.identifier.version}",
        ):
            await service.import_xdf3_schema(xdf3_import, strict=True)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_not_import_schema_with_changed_immutable_attribute_in_rule(
        self, service: Service
    ):
        factory = Xdf3Factory()
        rule = factory.rule(name="Original Name").build()
        xdf3_import = factory.schema().with_rule(rule).with_field().build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        rule.name = parse_string_latin("Changed Name")

        with pytest.raises(
            ImportException,
            match=f"Cannot update rule {rule.identifier.id} version {rule.identifier.version}",
        ):
            await service.import_xdf3_schema(xdf3_import, strict=True)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_not_import_schema_with_different_rules(
        self, service: Service
    ):
        factory = Xdf3Factory()
        xdf3_import = (
            factory.schema()
            .with_rule(factory.rule().build())
            .with_field()
            .build_import()
        )
        await service.import_xdf3_schema(xdf3_import, strict=True)

        xdf3_import.schema_message.schema.regeln = []

        with pytest.raises(
            ImportException,
            match=f"Cannot update schema {xdf3_import.id} version {xdf3_import.version}",
        ):
            await service.import_xdf3_schema(xdf3_import, strict=True)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_not_import_schema_with_group_with_different_rules(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field().build()
        rule = factory.rule().build()
        group = factory.group().with_field(field).with_rule(rule).build()
        xdf3_import = factory.schema().with_group(group).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        group.regeln = []

        with pytest.raises(
            ImportException,
            match=f"Cannot update group {group.identifier.id} version {group.identifier.version}",
        ):
            await service.import_xdf3_schema(xdf3_import, strict=True)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_not_re_import_schema_with_field_with_different_rules(
        self, service: Service
    ):
        factory = Xdf3Factory()
        rule = factory.rule().build()
        field = factory.field().with_rule(rule).build()
        xdf3_import = factory.schema().with_field(field).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        field.regeln = []

        with pytest.raises(
            ImportException,
            match=f"Cannot update field {field.identifier.id} version {field.identifier.version}",
        ):
            await service.import_xdf3_schema(xdf3_import, strict=True)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_re_import_schema_with_veroeffentlichungsdatum_previously_none(
        self, service: Service
    ):
        factory = Xdf3Factory()
        xdf3_import = (
            factory.schema(veroeffentlichungsdatum=None).with_field().build_import()
        )
        schema = xdf3_import.schema_message.schema

        await service.import_xdf3_schema(xdf3_import, strict=True)
        saved_schema = await service.get_schema(
            schema.identifier.id, schema.identifier.assert_version()
        )
        assert saved_schema is not None
        assert saved_schema.veroeffentlichungsdatum == None

        schema.veroeffentlichungsdatum = date(2024, 11, 21)
        await service.import_xdf3_schema(xdf3_import, strict=True)

        saved_schema = await service.get_schema(
            schema.identifier.id, schema.identifier.assert_version()
        )
        assert saved_schema is not None
        assert saved_schema.veroeffentlichungsdatum == date(2024, 11, 21)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_re_import_schema_with_veroeffentlichungsdatum_previously_none_in_group(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field().build()
        group = factory.group(veroeffentlichungsdatum=None).with_field(field).build()
        xdf3_import = factory.schema().with_group(group).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        saved_group = await service.get_group(
            "baukasten", group.identifier.id, group.identifier.assert_version()
        )
        assert saved_group is not None
        assert saved_group.veroeffentlichungsdatum == None

        group.veroeffentlichungsdatum = date(2024, 11, 21)
        await service.import_xdf3_schema(xdf3_import, strict=True)

        saved_group = await service.get_group(
            "baukasten", group.identifier.id, group.identifier.assert_version()
        )
        assert saved_group is not None
        assert saved_group.veroeffentlichungsdatum == date(2024, 11, 21)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_re_import_schema_with_veroeffentlichungsdatum_previously_none_in_field(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(veroeffentlichungsdatum=None).build()
        xdf3_import = factory.schema().with_field(field).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        saved_field = await service.get_field(
            "baukasten", field.identifier.id, field.identifier.assert_version()
        )
        assert saved_field is not None
        assert saved_field.veroeffentlichungsdatum == None

        field.veroeffentlichungsdatum = date(2024, 11, 21)
        await service.import_xdf3_schema(xdf3_import, strict=True)

        saved_field = await service.get_field(
            "baukasten", field.identifier.id, field.identifier.assert_version()
        )
        assert saved_field is not None
        assert saved_field.veroeffentlichungsdatum == date(2024, 11, 21)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_not_re_import_schema_with_changed_veroeffentlichungsdatum_date(
        self, service: Service
    ):
        factory = Xdf3Factory()
        xdf3_import = (
            factory.schema(veroeffentlichungsdatum=date(2024, 11, 19))
            .with_field()
            .build_import()
        )
        await service.import_xdf3_schema(xdf3_import, strict=True)

        xdf3_import.schema_message.schema.veroeffentlichungsdatum = date(2024, 11, 21)

        with pytest.raises(
            ImportException,
            match=f"Cannot update schema {xdf3_import.id} version {xdf3_import.version}",
        ):
            await service.import_xdf3_schema(xdf3_import, strict=True)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_not_re_import_schema_with_changed_veroeffentlichungsdatum_date_in_group(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field().build()
        group = (
            factory.group(veroeffentlichungsdatum=date(2024, 11, 19))
            .with_field(field)
            .build()
        )
        xdf3_import = factory.schema().with_group(group).build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        group.veroeffentlichungsdatum = date(2024, 11, 21)

        with pytest.raises(
            ImportException,
            match=f"Cannot update group {group.identifier.id} version {group.identifier.version}",
        ):
            await service.import_xdf3_schema(xdf3_import)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_not_re_import_schema_with_changed_veroeffentlichungsdatum_date_in_field(
        self, service: Service
    ):
        factory = Xdf3Factory()
        field = factory.field(veroeffentlichungsdatum=date(2024, 11, 19)).build()
        xdf3_import = factory.schema().with_field(field).build_import()
        await service.import_xdf3_schema(xdf3_import)

        field.veroeffentlichungsdatum = date(2024, 11, 21)

        with pytest.raises(
            ImportException,
            match=f"Cannot update field {field.identifier.id} version {field.identifier.version}",
        ):
            await service.import_xdf3_schema(xdf3_import)


@pytest.mark.asyncio(loop_scope="session")
async def test_should_reuse_external_code_lists(service: Service):
    factory = Xdf3Factory()
    code_list = CodeListFactory().build()

    field_a = factory.field().with_code_list(code_list).build()
    import_a = factory.schema().with_field(field_a).build_import()
    await service.import_xdf3_schema(import_a)

    field_b = factory.field().with_code_list(code_list).build()
    import_b = factory.schema().with_field(field_b).build_import()
    await service.import_xdf3_schema(import_b)

    saved_field_a = await service.get_field(
        "baukasten", field_a.identifier.id, field_a.identifier.assert_version()
    )
    assert saved_field_a is not None

    saved_field_b = await service.get_field(
        "baukasten", field_b.identifier.id, field_b.identifier.assert_version()
    )
    assert saved_field_b is not None

    assert saved_field_a.code_list is not None
    assert saved_field_a.code_list.source is None
    assert saved_field_a.code_list == saved_field_b.code_list


@pytest.mark.asyncio(loop_scope="session")
async def test_should_not_reuse_internal_code_lists(service: Service):
    factory = Xdf2Factory()
    code_list = CodeListImportFactory().build()

    field_a = factory.field().with_code_list(code_list).build()
    import_a = factory.schema().with_field(field_a).build_import(code_lists=[code_list])
    await service.import_xdf2_schema(import_a)

    field_b = factory.field().with_code_list(code_list).build()
    import_b = factory.schema().with_field(field_b).build_import(code_lists=[code_list])
    await service.import_xdf2_schema(import_b)

    saved_field_a = await service.get_field(
        "baukasten", field_a.identifier.id, field_a.identifier.assert_version()
    )
    assert saved_field_a is not None

    saved_field_b = await service.get_field(
        "baukasten", field_b.identifier.id, field_b.identifier.assert_version()
    )
    assert saved_field_b is not None

    assert saved_field_a.code_list is not None
    assert saved_field_a.code_list.source == "internal"

    assert saved_field_b.code_list is not None
    assert saved_field_b.code_list.source == "internal"

    assert saved_field_a.code_list.id != saved_field_b.code_list.id
    assert (
        saved_field_a.code_list.genericode_canonical_version_uri
        == saved_field_b.code_list.genericode_canonical_version_uri
    )
