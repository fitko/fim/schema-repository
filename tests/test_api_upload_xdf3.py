from datetime import date

from fastapi.testclient import TestClient
from freezegun import freeze_time

from fimportal import genericode
from fimportal.genericode.code_list.serialize import serialize_code_list
from fimportal.xdatenfelder.xdf3.common import Stichwort
from fimportal.xrepository import FakeClient as XRepoTestClient
from tests.conftest import CommandRunner
from tests.data import XDF3_DATA
from tests.factories.xdf3 import (
    Xdf3Factory,
)


def test_should_fail_for_wrong_access_token(client: TestClient):
    with open(XDF3_DATA / "schema_without_code_lists.xdf3.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf3",
            files=[("schema", schema)],
            headers={"Access-Token": "wrong_access_token"},
        )

    assert response.status_code == 401
    assert response.json() == {"detail": "invalid access token"}


def test_should_return_the_schema_metadata(
    immutable_base_url: str, runner: CommandRunner, client: TestClient
):
    token = runner.create_access_token("12345")

    with freeze_time("2023-10-17T00:00:00"):
        with open(XDF3_DATA / "schema_without_code_lists.xdf3.xml", "rb") as schema:
            response = client.post(
                "/api/v1/schemas/xdf3",
                files=[("schema", schema)],
                headers={"Access-Token": token},
            )

    assert response.status_code == 200
    assert response.json() == {
        "fim_id": "S12345",
        "fim_version": "1.0.0",
        "nummernkreis": "12345",
        "name": "Test",
        "beschreibung": "Eine Beschreibung",
        "definition": "Eine Definition",
        "freigabe_status": 6,
        "freigabe_status_label": "fachlich freigegeben (gold)",
        "gueltig_ab": None,
        "gueltig_bis": None,
        "status_gesetzt_am": "2023-01-01",
        "bezug": ["Bezug"],
        "status_gesetzt_durch": "Test",
        "steckbrief_id": "D12345",
        "steckbrief_name": None,
        "versionshinweis": "Ein Versionshinweis",
        "veroeffentlichungsdatum": "2023-01-01",
        "stichwort": [],
        "bezeichnung": "Eine Bezeichnung",
        "letzte_aenderung": "2020-09-01T00:00:00Z",
        "last_update": "2023-10-17T00:00:00Z",
        "is_latest": True,
        "datenfelder": [
            {
                "namespace": "baukasten",
                "fim_id": "F12345",
                "fim_version": "1.1.0",
                "nummernkreis": "12345",
                "name": "Familienname",
                "beschreibung": "Eine Beschreibung",
                "definition": "Eine Definition",
                "gueltig_ab": None,
                "gueltig_bis": None,
                "bezug": ["Bezug"],
                "freigabe_status": 6,
                "freigabe_status_label": "fachlich freigegeben (gold)",
                "letzte_aenderung": "2020-09-01T00:00:00Z",
                "last_update": "2023-10-17T00:00:00Z",
                "status_gesetzt_durch": "Test",
                "status_gesetzt_am": "2023-01-01",
                "veroeffentlichungsdatum": "2023-01-01",
                "versionshinweis": "Ein Versionshinweis",
                "xdf_version": "3.0.0",
                "feldart": "input",
                "datentyp": "text",
                "fts_match": None,
                "is_latest": True,
            }
        ],
        "datenfeldgruppen": [],
        "children": [
            {
                "namespace": "baukasten",
                "fim_id": "F12345",
                "fim_version": "1.1.0",
                "type": "Feld",
                "anzahl": "1:1",
                "bezug": ["Ein Rechtsbezug"],
            }
        ],
        "regeln": [],
        "relation": [],
        "xdf_version": "3.0.0",
        "uploads": [
            {
                "filename": "S12345V1.0.0_2023-10-17-1697500800000.xdf3.xml",
                "url": f"{immutable_base_url}/immutable/schemas/S12345V1.0.0_2023-10-17-1697500800000.xdf3.xml",
                "upload_time": "2023-10-17T00:00:00Z",
                "code_lists": [],
            }
        ],
        "json_schema_files": [
            {
                "filename": "S12345V1.0.0_2023-10-17-1697500800000.schema.json",
                "url": f"{immutable_base_url}/immutable/schemas/S12345V1.0.0_2023-10-17-1697500800000.schema.json",
                "upload_time": "2023-10-17T00:00:00Z",
                "generated_from": "S12345V1.0.0_2023-10-17-1697500800000.xdf3.xml",
            }
        ],
        "xsd_files": [],
    }


def test_should_mark_code_lists_as_external(runner: CommandRunner, client: TestClient):
    token = runner.create_access_token("12345")

    with open(XDF3_DATA / "schema_with_code_list.xdf3.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf3",
            files=[("schema", schema)],
            headers={"Access-Token": token},
        )

    assert response.status_code == 200

    assert response.json()["uploads"][0]["code_lists"] == [
        {
            "id": 1,
            "url": None,
            "genericode_canonical_version_uri": "urn:de:code_list_a_1",
            "genericode_canonical_uri": "urn:de:code_list_a",
            "genericode_version": "1",
            "is_external": True,
        }
    ]


def test_should_fail_for_missing_access_token(client: TestClient):
    with open(XDF3_DATA / "schema_without_code_lists.xdf3.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf3",
            files=[("schema", schema)],
        )

    assert response.status_code == 401
    assert response.json() == {"detail": "invalid access token"}


def test_should_fail_for_invalid_token_scope(runner: CommandRunner, client: TestClient):
    access_token = runner.create_access_token("01000")

    with open(XDF3_DATA / "schema_without_code_lists.xdf3.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf3",
            files=[("schema", schema)],
            headers={"Access-Token": access_token},
        )

    assert response.status_code == 403
    assert response.json() == {
        "detail": "missing authorization: token can only upload for Nummernkreis 01000, the schema is for Nummernkreis 12345."
    }


def test_should_work_for_unknown_element_from_another_nummernkreis(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf3Factory()
    schema_import = (
        factory.schema(id="S12000")
        .with_field(factory.field(id="F00000").build())
        .build_import()
    )

    access_token = runner.create_access_token("12000")

    response = client.post(
        "/api/v1/schemas/xdf3",
        files=[
            ("schema", schema_import.schema_text),
        ],
        headers={"Access-Token": access_token},
    )

    assert response.status_code == 200


def test_should_save_the_schema_in_the_database(
    runner: CommandRunner, client: TestClient
):
    token = runner.create_access_token("12345")

    with open(XDF3_DATA / "schema_without_code_lists.xdf3.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf3",
            files=[("schema", schema)],
            headers={"Access-Token": token},
        )

    assert response.status_code == 200

    response = client.get("/api/v1/schemas/S12345/1.0.0")
    assert response.status_code == 200


def test_should_fail_for_empty_schema_version(
    runner: CommandRunner, client: TestClient
):
    token = runner.create_access_token("12345")

    with open(XDF3_DATA / "schema_empty_version.xdf3.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf3",
            files=[("schema", schema)],
            headers={"Access-Token": token},
        )

    assert response.status_code == 400
    assert response.json() == {
        "detail": "Cannot import schema without a version [id=S12345]"
    }


def test_should_fail_for_empty_group_version(runner: CommandRunner, client: TestClient):
    token = runner.create_access_token("12345")

    with open(XDF3_DATA / "schema_empty_group_version.xdf3.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf3",
            files=[("schema", schema)],
            headers={"Access-Token": token},
        )

    assert response.status_code == 400
    assert response.json() == {
        "detail": "Cannot import element without a version [id=G00001]"
    }


def test_should_fail_for_invalid_freigabe_status(
    runner: CommandRunner, client: TestClient
):
    token = runner.create_access_token("12345")

    with open(
        XDF3_DATA
        / "schema_without_code_lists_invalid_freigabe_status_for_upload.xdf3.xml",
        "rb",
    ) as schema:
        response = client.post(
            "/api/v1/schemas/xdf3",
            files=[("schema", schema)],
            headers={"Access-Token": token},
        )

    assert response.status_code == 400
    assert response.json() == {
        "detail": "Freigabestatus not allowed for import [got=in Planung]"
    }


def test_should_fail_for_invalid_schema(runner: CommandRunner, client: TestClient):
    token = runner.create_access_token("12000")

    with open(XDF3_DATA / "code_list_a.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf3",
            files=[("schema", schema)],
            headers={"Access-Token": token},
        )

    assert response.status_code == 400
    assert response.json() == {
        "detail": "Cannot parse schema: Element '{http://docs.oasis-open.org/codelist/ns/genericode/1.0/}CodeList': No matching global declaration available for the validation root., line 2"
    }


def test_should_work_for_uploading_existing_schema(
    immutable_base_url: str, runner: CommandRunner, client: TestClient
):
    token = runner.create_access_token("12345")
    with freeze_time("2023-10-17T00:00:00"):
        with open(XDF3_DATA / "schema_without_code_lists.xdf3.xml", "r") as file:
            text = file.read()
            schema = text.replace(
                """<code>6</code>
        </xdf:freigabestatus>""",
                """<code>5</code>
        </xdf:freigabestatus>""",
            ).encode()
            assert schema != text.encode()

            response = client.post(
                "/api/v1/schemas/xdf3",
                files=[("schema", schema)],
                headers={"Access-Token": token},
            )

    with freeze_time("2023-10-18T00:00:00"):
        with open(XDF3_DATA / "schema_without_code_lists.xdf3.xml", "rb") as schema:
            response = client.post(
                "/api/v1/schemas/xdf3",
                files=[("schema", schema)],
                headers={"Access-Token": token},
            )

    assert response.status_code == 200
    assert response.json() == {
        "fim_id": "S12345",
        "fim_version": "1.0.0",
        "nummernkreis": "12345",
        "name": "Test",
        "beschreibung": "Eine Beschreibung",
        "definition": "Eine Definition",
        "bezeichnung": "Eine Bezeichnung",
        "freigabe_status": 6,
        "freigabe_status_label": "fachlich freigegeben (gold)",
        "gueltig_ab": None,
        "gueltig_bis": None,
        "status_gesetzt_am": "2023-01-01",
        "bezug": ["Bezug"],
        "status_gesetzt_durch": "Test",
        "versionshinweis": "Ein Versionshinweis",
        "veroeffentlichungsdatum": "2023-01-01",
        "steckbrief_id": "D12345",
        "steckbrief_name": None,
        "stichwort": [],
        "letzte_aenderung": "2020-09-01T00:00:00Z",
        "last_update": "2023-10-18T00:00:00Z",
        "is_latest": True,
        "datenfelder": [
            {
                "namespace": "baukasten",
                "fim_id": "F12345",
                "fim_version": "1.1.0",
                "nummernkreis": "12345",
                "name": "Familienname",
                "beschreibung": "Eine Beschreibung",
                "definition": "Eine Definition",
                "gueltig_ab": None,
                "gueltig_bis": None,
                "bezug": ["Bezug"],
                "freigabe_status": 6,
                "freigabe_status_label": "fachlich freigegeben (gold)",
                "letzte_aenderung": "2020-09-01T00:00:00Z",
                "last_update": "2023-10-18T00:00:00Z",
                "status_gesetzt_durch": "Test",
                "status_gesetzt_am": "2023-01-01",
                "veroeffentlichungsdatum": "2023-01-01",
                "versionshinweis": "Ein Versionshinweis",
                "xdf_version": "3.0.0",
                "feldart": "input",
                "datentyp": "text",
                "fts_match": None,
                "is_latest": True,
            }
        ],
        "datenfeldgruppen": [],
        "children": [
            {
                "namespace": "baukasten",
                "fim_id": "F12345",
                "fim_version": "1.1.0",
                "type": "Feld",
                "anzahl": "1:1",
                "bezug": ["Ein Rechtsbezug"],
            }
        ],
        "regeln": [],
        "relation": [],
        "xdf_version": "3.0.0",
        "uploads": [
            {
                "filename": "S12345V1.0.0_2023-10-18-1697587200000.xdf3.xml",
                "url": f"{immutable_base_url}/immutable/schemas/S12345V1.0.0_2023-10-18-1697587200000.xdf3.xml",
                "upload_time": "2023-10-18T00:00:00Z",
                "code_lists": [],
            },
            {
                "filename": "S12345V1.0.0_2023-10-17-1697500800000.xdf3.xml",
                "url": f"{immutable_base_url}/immutable/schemas/S12345V1.0.0_2023-10-17-1697500800000.xdf3.xml",
                "upload_time": "2023-10-17T00:00:00Z",
                "code_lists": [],
            },
        ],
        "json_schema_files": [
            {
                "filename": "S12345V1.0.0_2023-10-17-1697500800000.schema.json",
                "url": f"{immutable_base_url}/immutable/schemas/S12345V1.0.0_2023-10-17-1697500800000.schema.json",
                "upload_time": "2023-10-17T00:00:00Z",
                "generated_from": "S12345V1.0.0_2023-10-17-1697500800000.xdf3.xml",
            },
        ],
        "xsd_files": [],
    }


def test_should_return_one_code_list(runner: CommandRunner, client: TestClient):
    token = runner.create_access_token("12345")
    with open(XDF3_DATA / "schema_with_code_list.xdf3.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf3",
            files=[("schema", schema)],
            headers={"Access-Token": token},
        )

    with open(XDF3_DATA / "schema_with_code_list.xdf3.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf3",
            files=[("schema", schema)],
            headers={"Access-Token": token},
        )
    assert response.status_code == 200
    assert len(response.json()["uploads"][0]["code_lists"]) == 1


def test_should_import_external_codelist_content(
    runner: CommandRunner, client: TestClient, xrepository_client: XRepoTestClient
):
    token = runner.create_access_token("12345")
    canonical_version_uri = "urn:de:code_list_a_1"
    retrievable_code_list = genericode.CodeList(
        identifier=genericode.Identifier(
            canonical_version_uri=canonical_version_uri,
            version="1",
            canonical_uri="urn:de:code_list_a",
        ),
        default_code_key="code",
        default_name_key="name",
        columns={"code": ["0", "1"], "name": ["Name0", "Name1"]},
    )
    xrepository_client.register_code_list(retrievable_code_list)
    with open(XDF3_DATA / "schema_with_code_list.xdf3.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf3",
            files=[("schema", schema)],
            headers={"Access-Token": token},
        )

    assert response.status_code == 200

    code_lists = response.json()["uploads"][0]["code_lists"]
    assert len(code_lists) == 1
    assert code_lists[0]["is_external"] == True
    code_list_response = client.get(
        f"/immutable/code-lists/{code_lists[0]['id']}/genericode.xml"
    )
    assert code_list_response.text == serialize_code_list(retrievable_code_list).decode(
        "utf-8"
    )


def test_should_fail_for_uploading_existing_schema_with_changed_immutable_attribute(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf3Factory()
    (
        factory.schema(
            id="S12345",
            version="1.0.0",
            name="tseT",
            stichwort=[
                Stichwort(uri="StichwortUri", value="StichwortValue"),
            ],
            beschreibung="old beschreibung",
            bezeichnung="old bezeichnung",
            definition="old definition",
            gueltig_ab=date(2023, 10, 17),
            gueltig_bis=date(2023, 10, 17),
            hilfetext="old hilfetext",
            versionshinweis="old versionshinweis",
            veroeffentlichungsdatum=date(2023, 10, 17),
        )
        .with_bezug(text="Rechtsbezug", link="link")
        .with_rule(factory.rule(id="R12345", version="1.0.0").build())
        .with_field(factory.field(id="F12345", version="1.0.0").build())
        .save(runner)
    )

    token = runner.create_access_token("12345")
    with open(XDF3_DATA / "schema_without_code_lists.xdf3.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf3",
            files=[("schema", schema)],
            headers={"Access-Token": token},
        )

    assert response.status_code == 400
    assert response.json() == {
        "detail": (
            "Cannot update schema S12345 version 1.0.0 because the following attributes are different:\n"
            "  - ableitungsmodifikationen_repraesentation: 0 -> 1\n"
            "  - ableitungsmodifikationen_struktur: 0 -> 3\n"
            "  - beschreibung: old beschreibung -> Eine Beschreibung\n"
            "  - bezeichnung: old bezeichnung -> Eine Bezeichnung\n"
            "  - bezug: [{'link': 'link', 'text': 'Rechtsbezug'}] -> [{'text': 'Bezug', 'link': 'some link'}]\n"
            "  - definition: old definition -> Eine Definition\n"
            "  - gueltig_ab: 2023-10-17 -> None\n"
            "  - gueltig_bis: 2023-10-17 -> None\n"
            "  - hilfetext: old hilfetext -> Ein Hilfetext\n"
            "  - name: tseT -> Test\n"
            "  - regeln: [{'id': 'R12345', 'version': '1.0.0'}] -> []\n"
            "  - status_gesetzt_durch: Fachlicher Ersteller -> Test\n"
            "  - stichwort: [{'uri': 'StichwortUri', 'value': 'StichwortValue'}] -> []\n"
            "  - struktur: [{'bezug': [], 'anzahl': {'max': 1, 'min': 1}, 'enthaelt': {'id': 'F12345', 'version': '1.0.0'}, 'element_type': 1}] -> [{'anzahl': {'min': 1, 'max': 1}, 'bezug': [{'text': 'Ein Rechtsbezug', 'link': None}], 'element_type': 1, 'enthaelt': {'id': 'F12345', 'version': '1.1.0'}}]\n"
            "  - veroeffentlichungsdatum: 2023-10-17 -> 2023-01-01\n"
            "  - versionshinweis: old versionshinweis -> Ein Versionshinweis"
        ),
    }
