from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.factories.xzufi import SpezialisierungFactory

ENDPOINT = "/api/v0/specialization"


def test_return_all_spezialsierungen(runner: CommandRunner, client: TestClient):
    spezialisierung = SpezialisierungFactory().save(runner)

    response = client.get(ENDPOINT)

    assert response.status_code == 200
    assert response.json() == {
        "items": [
            {
                "redaktion_id": spezialisierung.redaktion_id,
                "id": spezialisierung.id,
                "leistung_redaktion_id": spezialisierung.leistung_redaktion_id,
                "leistung_id": spezialisierung.leistung_id,
            }
        ],
        "count": 1,
        "offset": 0,
        "limit": 200,
        "total_count": 1,
    }
