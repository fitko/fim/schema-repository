from typing import AsyncContextManager, Callable

import pytest
from asyncpg import Pool

from fimportal import pvog
from fimportal.cli.commands.import_leistungen import sync_pvog_resources
from fimportal.models.xzufi import PvogResourceClass, XzufiSource
from fimportal.service import Service
from fimportal.xzufi.client import TEST_LEISTUNG_XML
from fimportal.xzufi.common import (
    LeistungsId,
    RedaktionsId,
    XzufiIdentifier,
)
from tests.data import XZUFI_2_2_0_DATA
from tests.factories.xzufi import (
    TEST_SPEZIALISIERUNG_XML,
    LeistungFactory,
    SpezialisierungFactory,
)


@pytest.mark.asyncio(loop_scope="session")
async def test_should_sync_pvog_write_event(
    get_service: Callable[[], AsyncContextManager[Service]],
    async_db_pool: Pool,
):
    steckbrief = LeistungFactory.steckbrief().build()

    async with get_service() as service:
        await service.import_leistung_batch(
            [(steckbrief, TEST_LEISTUNG_XML)],
            XzufiSource.XZUFI,
        )

    batch_xml = (XZUFI_2_2_0_DATA / "transfer_schreib_event.xml").read_text()
    batch_id = 0
    pvog_client = pvog.TestClient()
    pvog_client.add_event_batch(batch_id, batch_xml)

    await sync_pvog_resources(pvog_client, "localhost", async_db_pool)

    async with get_service() as service:
        # LeiKa Leistungen are not affected by PVOG import
        saved_steckbrief = await service.get_leistungssteckbrief(
            steckbrief.get_leistungsschluessel()[0]
        )
        assert saved_steckbrief is not None

        # Batch ID is updated
        latest_batch_id = await service.get_latest_pvog_batch_id()
        assert latest_batch_id == batch_id

        # State of Leistungen
        raw_pvog_leistung_identifiers = await service.get_raw_pvog_resource_identifiers(
            PvogResourceClass.LEISTUNG
        )
        assert raw_pvog_leistung_identifiers == [("L001", "100157500")]
        xzufi_leistung = await service.get_leistungsbeschreibung(
            raw_pvog_leistung_identifiers[0]
        )
        assert xzufi_leistung is not None
        assert xzufi_leistung.title == "Altersteilzeit Vereinbarung Für Arbeitgeber"

        # State of Spezialisierungen
        raw_pvog_spezialisierung_identifiers = (
            await service.get_raw_pvog_resource_identifiers(
                PvogResourceClass.SPEZIALISIERUNG
            )
        )
        assert raw_pvog_spezialisierung_identifiers == [("L100001", "10665799_8955200")]
        spezialisierung = await service.get_xzufi_spezialisierung(
            XzufiIdentifier.from_xzufi_leistung_identifier(
                raw_pvog_spezialisierung_identifiers[0]
            )
        )
        assert spezialisierung is not None
        assert spezialisierung.leistung_id == "8967107"

        # State of Organisationseinheiten
        raw_pvog_organisationseinheit_identifiers = (
            await service.get_raw_pvog_resource_identifiers(
                PvogResourceClass.ORGANISATIONSEINHEIT
            )
        )
        assert raw_pvog_organisationseinheit_identifiers == [
            ("S100003", "S1000030000011895")
        ]
        organisationseinheit = await service.get_xzufi_organisationseinheit(
            XzufiIdentifier.from_xzufi_leistung_identifier(
                raw_pvog_organisationseinheit_identifiers[0]
            )
        )
        assert organisationseinheit is not None

        # State of Zustaendigkeiten
        raw_pvog_zustaendigkeit_identifiers = (
            await service.get_raw_pvog_resource_identifiers(
                PvogResourceClass.ZUSTAENDIGKEIT
            )
        )
        assert raw_pvog_zustaendigkeit_identifiers == [
            ("L100042", "88162.44432.095750153000")
        ]
        zustaendigkeit = await service.get_xzufi_zustaendigkeit(
            XzufiIdentifier.from_xzufi_leistung_identifier(
                raw_pvog_zustaendigkeit_identifiers[0]
            )
        )
        assert zustaendigkeit is not None

        # State of onlinediensten
        raw_pvog_onlinedienst_identifiers = (
            await service.get_raw_pvog_resource_identifiers(
                PvogResourceClass.ONLINEDIENST
            )
        )
        assert raw_pvog_onlinedienst_identifiers == [("L100012", "280810655")]
        onlinedienst = await service.get_xzufi_onlinedienst(
            XzufiIdentifier.from_xzufi_leistung_identifier(
                raw_pvog_onlinedienst_identifiers[0]
            )
        )
        assert onlinedienst is not None


@pytest.mark.asyncio(loop_scope="session")
async def test_should_sync_pvog_delete_event(
    get_service: Callable[[], AsyncContextManager[Service]],
    async_db_pool: Pool,
):
    pvog_client = pvog.TestClient()

    write_batch_xml = (XZUFI_2_2_0_DATA / "transfer_schreib_event.xml").read_text()
    write_batch_id = 0
    pvog_client.add_event_batch(write_batch_id, write_batch_xml)

    delete_batch_xml = (XZUFI_2_2_0_DATA / "transfer_loesch_event.xml").read_text()
    delete_batch_id = 1
    pvog_client.add_event_batch(delete_batch_id, delete_batch_xml)

    await sync_pvog_resources(pvog_client, "localhost", async_db_pool)

    async with get_service() as service:
        latest_batch_id = await service.get_latest_pvog_batch_id()
        assert latest_batch_id == delete_batch_id

        raw_pvog_leistung_identifiers = await service.get_raw_pvog_resource_identifiers(
            PvogResourceClass.LEISTUNG
        )
        assert raw_pvog_leistung_identifiers == []
        leistung = await service.get_leistungsbeschreibung(
            (
                RedaktionsId("L001"),
                LeistungsId("100157500"),
            )
        )
        assert leistung is None

        raw_pvog_spezialisierung_identifiers = (
            await service.get_raw_pvog_resource_identifiers(
                PvogResourceClass.SPEZIALISIERUNG
            )
        )
        assert raw_pvog_spezialisierung_identifiers == []
        spezialisierung = await service.get_xzufi_spezialisierung(
            XzufiIdentifier("L100001", "10665799_8955200")
        )
        assert spezialisierung is None

        raw_pvog_organisationseinheit_identifiers = (
            await service.get_raw_pvog_resource_identifiers(
                PvogResourceClass.ORGANISATIONSEINHEIT
            )
        )
        assert raw_pvog_organisationseinheit_identifiers == []
        organisationseinheit = await service.get_xzufi_organisationseinheit(
            XzufiIdentifier("S100003", "S1000030000011895")
        )
        assert organisationseinheit is None

        raw_pvog_zustaendigkeit_identifiers = (
            await service.get_raw_pvog_resource_identifiers(
                PvogResourceClass.ZUSTAENDIGKEIT
            )
        )
        assert raw_pvog_zustaendigkeit_identifiers == []
        zustaendigkeit = await service.get_xzufi_zustaendigkeit(
            XzufiIdentifier("L100042", "88162.44432.095750153000")
        )
        assert zustaendigkeit is None

        raw_pvog_onlinedienst_identifiers = (
            await service.get_raw_pvog_resource_identifiers(
                PvogResourceClass.ONLINEDIENST
            )
        )
        assert raw_pvog_onlinedienst_identifiers == []
        onlinedienst = await service.get_xzufi_onlinedienst(
            XzufiIdentifier("L100012", "280810655")
        )
        assert onlinedienst is None


@pytest.mark.asyncio(loop_scope="session")
async def test_should_sync_only_new_events(
    get_service: Callable[[], AsyncContextManager[Service]],
    async_db_pool: Pool,
):
    pvog_client = pvog.TestClient()

    write_batch_xml = (XZUFI_2_2_0_DATA / "transfer_schreib_event.xml").read_text()
    new_batch_id = 100
    pvog_client.add_event_batch(0, "some-old-event")
    pvog_client.add_event_batch(new_batch_id, write_batch_xml)
    async with get_service() as service:
        await service.save_pvog_batch(50, "some-other-old-event")

    await sync_pvog_resources(pvog_client, "localhost", async_db_pool)

    async with get_service() as service:
        latest_batch_id = await service.get_latest_pvog_batch_id()
        assert latest_batch_id == new_batch_id

        raw_pvog_leistung_identifiers = await service.get_raw_pvog_resource_identifiers(
            PvogResourceClass.LEISTUNG
        )
        assert raw_pvog_leistung_identifiers == [("L001", "100157500")]

        leistung = await service.get_leistungsbeschreibung(
            raw_pvog_leistung_identifiers[0]
        )
        assert leistung is not None
        assert leistung.title == "Altersteilzeit Vereinbarung Für Arbeitgeber"


@pytest.mark.asyncio(loop_scope="session")
async def test_should_update_existing_resources(
    get_service: Callable[[], AsyncContextManager[Service]],
    async_db_pool: Pool,
):
    leistung_redaktion_id = "L001"
    leistung_id = "100157500"
    spezialisierung_redaktion_id = "L100001"
    spezialisierung_id = "10665799_8955200"
    existing_leistung = LeistungFactory(
        redaktion_id=leistung_redaktion_id,
        leistung_id=leistung_id,
        bezeichnung="an old description",
    ).build()
    existing_spezialisierung = SpezialisierungFactory(
        redaktion_id=spezialisierung_redaktion_id,
        id=spezialisierung_id,
        leistung_id="old-leistung-id",
        leistung_redaktion_id=leistung_redaktion_id,
    ).build()
    async with get_service() as service:
        await service.import_leistung_batch(
            [(existing_leistung, TEST_LEISTUNG_XML)],
            XzufiSource.PVOG,
        )
        await service.import_spezialisierung_batch(
            [(existing_spezialisierung, TEST_SPEZIALISIERUNG_XML)]
        )

    pvog_client = pvog.TestClient()

    write_batch_xml = (XZUFI_2_2_0_DATA / "transfer_schreib_event.xml").read_text()
    pvog_client.add_event_batch(0, write_batch_xml)

    await sync_pvog_resources(pvog_client, "localhost", async_db_pool)

    async with get_service() as service:
        leistung = await service.get_leistungsbeschreibung(
            (
                RedaktionsId(leistung_redaktion_id),
                LeistungsId(leistung_id),
            )
        )
        assert leistung is not None
        assert leistung.title == "Altersteilzeit Vereinbarung Für Arbeitgeber"

        spezialisierung = await service.get_xzufi_spezialisierung(
            identifier=XzufiIdentifier(spezialisierung_redaktion_id, spezialisierung_id)
        )
        assert spezialisierung is not None
        assert spezialisierung.leistung_id == "8967107"
