from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.factories import Xdf2SteckbriefImportFactory


def test_load_document_profile_page(client: TestClient, runner: CommandRunner) -> None:
    Xdf2SteckbriefImportFactory("D123", "1.0").save(runner)

    response = client.get("/document-profiles/D123/1.0")

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_unknown_document_profile(client: TestClient) -> None:
    response = client.get("/document-profiles/D1234/1.0")

    assert response.status_code == 404
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert (
        "Dokumentsteckbrief D1234 v1.0 konnte nicht gefunden werden." in response.text
    )
