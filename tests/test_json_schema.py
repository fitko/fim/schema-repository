import re

import pytest

from fimportal.json_schema import JsonSchemaException, from_xdf2
from fimportal.xdatenfelder import xdf2
from tests.factories import CodeListFactory
from tests.factories.xdf2 import Xdf2Factory


def test_should_correctly_convert_a_schema():
    factory = Xdf2Factory()
    field = factory.field(id="F12", version="1.0").build()
    group = factory.group(id="G12", version="1.0").build()
    message = (
        factory.schema(id="S12", version="1.0")
        .with_group(group, anzahl=xdf2.Anzahl(1, 1))
        .with_field(field, anzahl=xdf2.Anzahl(1, 1))
        .message()
    )

    id = "test"
    json_schema = from_xdf2(id, message, {})

    assert json_schema == {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "$id": id,
        "title": message.schema.name,
        "type": "object",
        "properties": {
            "G12V1.0": {"$ref": "#/$defs/G12V1.0"},
            "F12V1.0": {"$ref": "#/$defs/F12V1.0"},
        },
        "required": ["G12V1.0", "F12V1.0"],
        "$defs": {
            "G12V1.0": {
                "title": group.name,
                "type": "object",
                "properties": {},
                "required": [],
            },
            "F12V1.0": {
                "title": field.name,
                "type": "string",
            },
        },
    }


def test_should_correctly_handle_required_groups():
    factory = Xdf2Factory()
    group = factory.group(id="G12", version="1.0").build()
    message = factory.schema().with_group(group, anzahl=xdf2.Anzahl(1, 1)).message()

    json_schema = from_xdf2("test", message, {})

    assert json_schema["properties"] == {"G12V1.0": {"$ref": "#/$defs/G12V1.0"}}
    assert json_schema["required"] == ["G12V1.0"]


def test_should_correctly_handle_optional_groups():
    factory = Xdf2Factory()
    group = factory.group(id="G12", version="1.0").build()
    message = factory.schema().with_group(group, anzahl=xdf2.Anzahl(0, 1)).message()

    json_schema = from_xdf2("test", message, {})

    assert json_schema["properties"] == {"G12V1.0": {"$ref": "#/$defs/G12V1.0"}}
    assert json_schema["required"] == []


def test_should_correctly_handle_limited_array_groups():
    factory = Xdf2Factory()
    group = factory.group(id="G12", version="1.0").build()
    message = factory.schema().with_group(group, anzahl=xdf2.Anzahl(0, 5)).message()

    json_schema = from_xdf2("test", message, {})

    assert json_schema["properties"] == {
        "G12V1.0": {
            "type": "array",
            "items": {
                "$ref": "#/$defs/G12V1.0",
            },
            "minItems": 0,
            "maxItems": 5,
        }
    }
    assert json_schema["required"] == ["G12V1.0"]


def test_should_correctly_handle_unlimited_array_groups():
    factory = Xdf2Factory()
    group = factory.group(id="G12", version="1.0").build()
    message = factory.schema().with_group(group, anzahl=xdf2.Anzahl(0, None)).message()

    json_schema = from_xdf2("test", message, {})

    assert json_schema["properties"] == {
        "G12V1.0": {
            "type": "array",
            "items": {
                "$ref": "#/$defs/G12V1.0",
            },
            "minItems": 0,
        }
    }
    assert json_schema["required"] == ["G12V1.0"]


def test_should_handle_text_inputs():
    factory = Xdf2Factory()
    field = (
        factory.field(id="F12", version="1.0")
        .input(
            datentyp=xdf2.Datentyp.TEXT,
            praezisierung='{"minLength":"1","maxLength":"10","pattern":"[A-Z]+"}',
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf2("test", message, {})

    assert json_schema["$defs"]["F12V1.0"] == {
        "title": field.name,
        "type": "string",
        "minLength": 1,
        "maxLength": 10,
        "pattern": "[A-Z]+",
    }


def test_should_fail_for_invalid_regex():
    factory = Xdf2Factory()
    field = (
        factory.field(id="F12", version="1.0")
        .input(
            datentyp=xdf2.Datentyp.TEXT,
            praezisierung='{"pattern": "\\\\"}',
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    with pytest.raises(
        JsonSchemaException,
        match=re.escape("Invalid regular expression: /\\/: \\ at end of pattern\n"),
    ):
        from_xdf2("test", message, {})


def test_should_ignore_invalid_praezisierung():
    factory = Xdf2Factory()
    field = (
        factory.field(id="F12", version="1.0")
        .input(
            datentyp=xdf2.Datentyp.TEXT,
            praezisierung="not json",
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf2("test", message, {})

    assert json_schema["$defs"]["F12V1.0"] == {
        "title": field.name,
        "type": "string",
    }


def test_should_handle_date_inputs():
    factory = Xdf2Factory()
    field = (
        factory.field(id="F12", version="1.0")
        .input(datentyp=xdf2.Datentyp.DATUM)
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf2("test", message, {})

    assert json_schema["$defs"]["F12V1.0"] == {
        "title": field.name,
        "type": "string",
        "format": "date",
    }


def test_should_handle_integer_inputs():
    factory = Xdf2Factory()
    field = (
        factory.field(id="F12", version="1.0")
        .input(
            datentyp=xdf2.Datentyp.GANZZAHL,
            praezisierung='{"minValue":"1","maxValue":"10"}',
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf2("test", message, {})

    assert json_schema["$defs"]["F12V1.0"] == {
        "title": field.name,
        "type": "integer",
        "minimum": 1,
        "maximum": 10,
    }


def test_should_handle_numeric_inputs():
    factory = Xdf2Factory()
    field = (
        factory.field(id="F12", version="1.0")
        .input(
            datentyp=xdf2.Datentyp.NUMMER,
            praezisierung='{"minValue":"1","maxValue":"10"}',
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf2("test", message, {})

    assert json_schema["$defs"]["F12V1.0"] == {
        "title": field.name,
        "type": "number",
        "minimum": 1,
        "maximum": 10,
    }


def test_should_handle_currency_inputs():
    factory = Xdf2Factory()
    field = (
        factory.field(id="F12", version="1.0")
        .input(
            datentyp=xdf2.Datentyp.GELDBETRAG,
            praezisierung='{"minValue":"1","maxValue":"10"}',
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf2("test", message, {})

    assert json_schema["$defs"]["F12V1.0"] == {
        "title": field.name,
        "type": "number",
        "multipleOf": 0.01,
        "minimum": 1,
        "maximum": 10,
    }


def test_should_handle_file_inputs():
    factory = Xdf2Factory()
    field = (
        factory.field(id="F12", version="1.0")
        .input(datentyp=xdf2.Datentyp.ANLAGE)
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf2("test", message, {})

    assert json_schema["$defs"]["F12V1.0"] == {
        "title": field.name,
        "type": "string",
    }


def test_should_handle_object_inputs():
    factory = Xdf2Factory()
    field = (
        factory.field(id="F12", version="1.0")
        .input(datentyp=xdf2.Datentyp.OBJEKT)
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf2("test", message, {})

    assert json_schema["$defs"]["F12V1.0"] == {
        "title": field.name,
        "type": "string",
    }


def test_should_handle_bool_inputs():
    factory = Xdf2Factory()
    field = (
        factory.field(id="F12", version="1.0")
        .input(datentyp=xdf2.Datentyp.WAHRHEITSWERT)
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf2("test", message, {})

    assert json_schema["$defs"]["F12V1.0"] == {
        "title": field.name,
        "type": "boolean",
    }


def test_fail_for_input_with_code_list():
    code_list = CodeListFactory().build()
    factory = Xdf2Factory()
    field = (
        factory.field(id="F12", version="1.0").input().with_code_list(code_list).build()
    )
    message = factory.schema().with_field(field).message()

    with pytest.raises(
        JsonSchemaException,
        match=re.escape("INPUT field with code list [element=F12V1.0]"),
    ):
        from_xdf2(
            "test", message, {code_list.identifier.canonical_version_uri: code_list}
        )


def test_should_embed_code_list_keys():
    factory = Xdf2Factory()
    code_list = (
        CodeListFactory(default_code_key="code")
        .add_column("code", ["000", "001"])
        .build()
    )
    field = factory.field(id="F12", version="1.0").select(code_list=code_list).build()
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf2(
        "test",
        message,
        {
            code_list.identifier.canonical_version_uri: code_list,
        },
    )

    assert json_schema["$defs"]["F12V1.0"] == {
        "title": field.name,
        "type": "string",
        "enum": ["000", "001"],
    }


def test_should_embed_integer_code_list_keys_as_strings():
    code_list = CodeListFactory().build()
    factory = Xdf2Factory()
    field = (
        factory.field(id="F12", version="1.0")
        .select(code_list=code_list, datentyp=xdf2.Datentyp.GANZZAHL)
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf2(
        "test",
        message,
        {
            code_list.identifier.canonical_version_uri: code_list,
        },
    )

    assert json_schema["$defs"]["F12V1.0"] == {
        "title": field.name,
        "type": "string",
        "enum": ["0", "1"],
    }


def test_should_convert_boolean_select_fields_to_string():
    code_list = CodeListFactory().build()
    factory = Xdf2Factory()
    field = (
        factory.field(id="F12", version="1.0")
        .select(datentyp=xdf2.Datentyp.WAHRHEITSWERT)
        .with_code_list(code_list)
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf2(
        "test", message, {code_list.identifier.canonical_version_uri: code_list}
    )

    assert json_schema["$defs"]["F12V1.0"] == {
        "title": field.name,
        "type": "string",
        "enum": ["0", "1"],
    }


def test_should_fail_for_select_without_code_list():
    factory = Xdf2Factory()
    field = (
        factory.field(id="F12", version="1.0")
        .select(datentyp=xdf2.Datentyp.TEXT)
        .with_code_list(None)
        .build()
    )
    message = factory.schema().with_field(field).message()

    with pytest.raises(
        JsonSchemaException,
        match=re.escape("SELECT field without code list [element=F12V1.0]"),
    ):
        from_xdf2("test", message, {})


def test_should_fail_for_missing_code_list():
    code_list = CodeListFactory().build()
    factory = Xdf2Factory()
    field = factory.field().select(code_list).build()
    message = factory.schema().with_field(field).message()

    with pytest.raises(JsonSchemaException, match="Missing code lists"):
        from_xdf2("test", message, {})


def test_should_ignore_static_fields():
    factory = Xdf2Factory()
    field = factory.field().label().build()
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf2("test", message, {})

    assert json_schema["$defs"] == {}
    assert json_schema["properties"] == {}
