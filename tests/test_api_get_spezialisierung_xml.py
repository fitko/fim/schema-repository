from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.factories.xzufi import TEST_SPEZIALISIERUNG_XML, SpezialisierungFactory

ENDPOINT = "/api/v0/specialization"


def test_return_existing_spezialisierung(runner: CommandRunner, client: TestClient):
    spezialisierung = SpezialisierungFactory().save(
        runner, xml_content=TEST_SPEZIALISIERUNG_XML
    )

    response = client.get(
        f"{ENDPOINT}/{spezialisierung.redaktion_id}/{spezialisierung.id}/xzufi"
    )

    assert response.status_code == 200
    assert response.text == TEST_SPEZIALISIERUNG_XML
    assert (
        response.headers["content-disposition"]
        == f'attachment; filename="spezialisierung_{spezialisierung.redaktion_id}_{spezialisierung.id}.xzufi.xml"'
    )


def test_fail_for_unknown_spezialisierung(client: TestClient):
    response = client.get(f"{ENDPOINT}/L1234/sp_1/xzufi")

    assert response.status_code == 404
    assert response.json() == {
        "detail": "Could not find Spezialisierung sp_1 from Redaktion L1234.",
    }
