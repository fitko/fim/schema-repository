import re

import pytest

from fimportal import crawler
from fimportal.errors import ImportException
from fimportal.models.imports import deduplicate_code_list_imports
from fimportal.xdatenfelder import xdf3
from tests.factories import CodeListFactory, CodeListImportFactory


def test_should_correctly_extract_the_steckbrief_id():
    assert crawler.parse_steckbrief_id("D123") == "D123"
    assert crawler.parse_steckbrief_id("D123V1.0") == "D123"


SCHEMA_DATA = {
    "dateChanged": "2022-06-09T17:22:13+0200",
    "characteristics": [
        {
            "fieldName": "Name des Dokumentsteckbriefs",
            "fieldValue": "Antrag auf Erteilung einer Aufenthaltserlaubnis zum Zweck der Erwerbstätigkeit",
        },
        {"fieldName": "ID des Dokumentsteckbriefs", "fieldValue": "D00000304"},
        {
            "fieldName": "Definition",
            "fieldValue": "Arbeitnehmer aus Nicht-EU-Staaten können in Deutschland eine Aufenthaltserlaubnis zum Zweck der Erwerbstätigkeit beantragen. Der Antrag kann vom Ausländer selbst, von einem gesetzlichen Vertreter oder in manchen Fällen vom Arbeitgeber im Rahmen des beschleunigten Fachkräfteverfahrens bei der zuständigen Ausländerbehörde gestellt werden. Der Aufenthaltstitel wird in der Form des elektronischen Aufenthaltstitels (eAT) ausgestellt. Er wird befristet erteilt, wobei eine Verlängerung möglich ist.",
        },
        {
            "fieldName": "Handlungsgrundlagen",
            "fieldValue": "§§ 2, 3, 5, 18a - 21, 48, 49, 78, 80, 81a, 82 Aufenthaltsgesetz (AufenthG); §§ 107, 1902 Bürgerliches Gesetzbuch (BGB); § 14 Verwaltungsverfahrensgesetz (VwVfG); S. 30-34 Richtlinie 2016/801 des Rates; S. 9-12 Richtlinie 2014/66/EU des Rates",
        },
        {
            "fieldName": "Bezeichnung",
            "fieldValue": "Antrag auf Erteilung einer Aufenthaltserlaubnis zum Zweck der Erwerbstätigkeit (§§ 18 - 21 AufenthG) ",
        },
        {
            "fieldName": "Herausgeber",
            "fieldValue": "FIM Datenfelder Bundesredaktion",
        },
        {"fieldName": "Status gesetzt durch", "fieldValue": "BMI M3"},
        {"fieldName": "Status", "fieldValue": "in Bearbeitung"},
        {"fieldName": "Zuletzt geändert am", "fieldValue": "09.06.2022"},
    ],
    "formReferences": ["S00000221V1.0"],
    "typ": "F",
    "version": "1.0",
    "documentLinks": [
        {
            "docName": "S00000221V1.0_ansicht.html",
            "docFileType": "HTML",
            "docUrl": "https://www.example.de/S00000221V1.0_ansicht.html",
        },
        {
            "docName": "S00000221V1.0_xdatenfelder.xml",
            "docFileType": "XML",
            "docUrl": "https://www.example.de/S00000221V1.0_xdatenfelder.xml",
        },
        {
            "docName": "S00000221V1.0_xfall.xsd",
            "docFileType": "XML",
            "docUrl": "https://www.example.de/S00000221V1.0_xfall.xsd",
        },
        {
            "docName": "C60000030_genericode.xml",
            "docFileType": "XML",
            "docUrl": "https://www.example.de/C60000030_genericode.xml",
        },
        {
            "docName": "C60000031_genericode.xml",
            "docFileType": "XML",
            "docUrl": "https://www.example.de/C60000031_genericode.xml",
        },
        {
            "docName": "C60000018_genericode.xml",
            "docFileType": "XML",
            "docUrl": "https://www.example.de/C60000018_genericode.xml",
        },
        {
            "docName": "C60000020_genericode.xml",
            "docFileType": "XML",
            "docUrl": "https://www.example.de/C60000020_genericode.xml",
        },
        {
            "docName": "C60000019_genericode.xml",
            "docFileType": "XML",
            "docUrl": "https://www.example.de/C60000019_genericode.xml",
        },
        {
            "docName": "C60000006_genericode.xml",
            "docFileType": "XML",
            "docUrl": "https://www.example.de/C60000006_genericode.xml",
        },
        {
            "docName": "C60000020_genericode.xml",
            "docFileType": "XML",
            "docUrl": "https://www.example.de/C60000020_genericode.xml",
        },
        {
            "docName": "C00000363_genericode.xml",
            "docFileType": "XML",
            "docUrl": "https://www.example.de/C00000363_genericode.xml",
        },
        {
            "docName": "S00000221V1.0_archiv.zip",
            "docFileType": "ZIP",
            "docUrl": "https://www.example.de/S00000221V1.0_archiv.zip",
        },
    ],
    "dateCreated": "2022-06-09T11:46:58+0200",
    "dateApproved": "2022-06-09",
    "itemizationLinks": [],
    "name": "Antrag auf Erteilung einer Aufenthaltserlaubnis zum Zweck der Erwerbstätigkeit (fachlich freigegeben (gold))",
    "registryOfInformation": [
        {
            "fieldName": "Name des Datenschemas",
            "fieldValue": "Antrag auf Erteilung einer Aufenthaltserlaubnis zum Zweck der Erwerbstätigkeit",
        },
        {
            "fieldName": "ID des Datenschemas inkl. Version",
            "fieldValue": "S00000221V1.0",
        },
        {
            "fieldName": "Handlungsgrundlagen",
            "fieldValue": "AufenthG v. 9.7.2021; §§ 107, 1902 BGB v. 7.5.2021; § 14 VwVfG v. 4.5.2021; S. 30-34 Richtlinie 2016/801 des Rates; S. 9-12 Richtlinie  2014/66/EU des Rates ",
        },
        {
            "fieldName": "Bezeichnung",
            "fieldValue": "Antrag auf Erteilung einer Aufenthaltserlaubnis zum Zweck der Erwerbstätigkeit",
        },
        {
            "fieldName": "Herausgeber",
            "fieldValue": "FIM Datenfelder Bundesredaktion",
        },
        {"fieldName": "Status gesetzt durch", "fieldValue": "BMI M3"},
        {"fieldName": "Status", "fieldValue": "fachlich freigegeben (gold)"},
        {"fieldName": "Status gesetzt am", "fieldValue": "09.06.2022"},
        {"fieldName": "Veröffentlicht am", "fieldValue": "09.06.2022"},
        {"fieldName": "Zuletzt geändert am", "fieldValue": "09.06.2022"},
    ],
    "id": "S00000221V1.0",
    "alternativeName": "Antrag auf Erteilung einer Aufenthaltserlaubnis zum Zweck der Erwerbstätigkeit",
    "status": 6,
}


def test_should_correctly_parse_the_schema_metadata():
    result = crawler.parse_schema_metadata(SCHEMA_DATA)

    assert result.freigabe_status == xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
    assert result.steckbrief_id == "D00000304"
    assert result.schema_url == "https://www.example.de/S00000221V1.0_xdatenfelder.xml"
    assert result.code_lists == {
        "C60000030_genericode.xml": "https://www.example.de/C60000030_genericode.xml",
        "C60000031_genericode.xml": "https://www.example.de/C60000031_genericode.xml",
        "C60000018_genericode.xml": "https://www.example.de/C60000018_genericode.xml",
        "C60000020_genericode.xml": "https://www.example.de/C60000020_genericode.xml",
        "C60000019_genericode.xml": "https://www.example.de/C60000019_genericode.xml",
        "C60000006_genericode.xml": "https://www.example.de/C60000006_genericode.xml",
        "C00000363_genericode.xml": "https://www.example.de/C00000363_genericode.xml",
    }


def test_should_create_the_code_list_import_map():
    import_a = CodeListImportFactory().build()
    import_b = CodeListImportFactory().build()

    imports = deduplicate_code_list_imports([import_a, import_b])

    assert len(imports) == 2
    assert import_a in imports
    assert import_b in imports


def test_should_identify_inconsistent_code_lists():
    code_list = CodeListFactory(canonical_uri="de:test", version="1").build()
    code_list_imports = [
        CodeListImportFactory(code_list=code_list, code_list_text="Variant A").build(),
        CodeListImportFactory(code_list=code_list, code_list_text="Variant B").build(),
    ]

    with pytest.raises(
        ImportException,
        match=re.escape(
            "Could not import schema: inconsistent code list data [canonical_version_uri=de:test_1]"
        ),
    ):
        deduplicate_code_list_imports(code_list_imports)


STECKBRIEF_DATA = {
    "dateChanged": "2024-03-20T10:46:55+0100",
    "characteristics": [
        {
            "fieldName": "Name des Dokumentsteckbriefs",
            "fieldValue": "Aufforderung Vervollständigung Impfschutz Masern",
        },
        {"fieldName": "ID des Dokumentsteckbriefs", "fieldValue": "D13000303V2.1"},
        {
            "fieldName": "Versionshinweise",
            "fieldValue": "Ergänzung bei den Gesetzgrundlagen",
        },
        {
            "fieldName": "Definition",
            "fieldValue": "Wenn der Immunitätsnachweis gegen Masern nicht innerhalb einer angemessenen Frist vorgelegt wird, kann das Gesundheitsamt die verpflichtete Person zu einer Vervollständigung des Impfschutzes gegen Masern auffordern.",
        },
        {
            "fieldName": "Handlungsgrundlagen",
            "fieldValue": "§ 20 (12) IfSG (Infektionsschutzgesetz)",
        },
        {
            "fieldName": "Bezeichnung",
            "fieldValue": "Aufforderung der Vervollständigung vom Impfschutz gegen Masern",
        },
        {
            "fieldName": "Herausgeber",
            "fieldValue": "Landesredaktion Mecklenburg-Vorpommern",
        },
        {
            "fieldName": "Status gesetzt durch",
            "fieldValue": "Ministerium für Soziales, Gesundheit und Sport Mecklenburg-Vorpommern (SM M-V)",
        },
        {"fieldName": "Status", "fieldValue": "fachlich freigegeben (silber)"},
        {"fieldName": "Status gesetzt am", "fieldValue": "20.03.2024"},
        {"fieldName": "Veröffentlicht am", "fieldValue": "20.03.2024"},
        {"fieldName": "Zuletzt geändert am", "fieldValue": "20.03.2024"},
    ],
    "formReferences": [],
    "changeNote": "Ergänzung bei den Gesetzgrundlagen",
    "typ": "F",
    "version": "2.1",
    "documentLinks": [
        {
            "docName": "D13000303V2.1_xdatenfelder.xml",
            "docFileType": "XML",
            "docUrl": "https://www.example.de/D13000303V2.1_xdatenfelder.xml",
        }
    ],
    "dateCreated": "2024-03-18T11:59:55+0100",
    "dateApproved": "2024-03-20",
    "itemizationLinks": [],
    "name": "Aufforderung Vervollständigung Impfschutz Masern (fachlich freigegeben (silber))",
    "registryOfInformation": [],
    "id": "D13000303V2.1",
    "alternativeName": "Aufforderung der Vervollständigung vom Impfschutz gegen Masern",
    "status": 5,
}


def test_should_correctly_parse_the_steckbrief_metadata():
    result = crawler.parse_steckbrief_metadata(STECKBRIEF_DATA)

    assert result.freigabe_status == xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER
    assert (
        result.steckbrief_url == "https://www.example.de/D13000303V2.1_xdatenfelder.xml"
    )
