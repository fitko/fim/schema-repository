import pytest

from fimportal.helpers import utc_now
from fimportal.pagination import PaginationOptions
from fimportal.service import Service
from fimportal.xzufi.link_check import FakeLinkChecker
from tests.factories.xzufi import LinkFactory
from freezegun import freeze_time


@pytest.mark.asyncio(loop_scope="session")
async def test_check_links_for_the_first_time(
    service: Service, link_checker: FakeLinkChecker
):
    uri = "https://example.com"
    links = [LinkFactory(uri=uri).build(), LinkFactory(uri="some_unknown_uri").build()]
    link_checker.register_link(uri)
    await service.import_links([link.uri for link in links])

    now = utc_now()
    await service.check_link_status(now)

    updated_links = await service.get_links(PaginationOptions(offset=0, limit=10))
    assert updated_links.total_count == 2
    assert updated_links.items[0].last_checked is not None
    assert updated_links.items[0].last_checked > now
    assert updated_links.items[0].last_online is not None
    assert updated_links.items[0].last_online > now
    assert updated_links.items[1].last_checked is not None
    assert updated_links.items[1].last_checked > now
    assert updated_links.items[1].last_online is None


@pytest.mark.asyncio(loop_scope="session")
async def test_update_link_status(service: Service, link_checker: FakeLinkChecker):
    uri = "https://example.com"
    links = [LinkFactory(uri=uri).build()]
    link_checker.register_link(uri)
    await service.import_links([link.uri for link in links])

    timestamp_early = utc_now()
    with freeze_time(timestamp_early):
        await service.check_link_status(timestamp_early)

    link_checker.remove_link(uri)
    timestamp_later = utc_now()
    with freeze_time(timestamp_later):
        await service.check_link_status(timestamp_later)

    updated_links = await service.get_links(PaginationOptions(offset=0, limit=10))
    assert updated_links.total_count == 1
    assert updated_links.items[0].last_checked == timestamp_later
    assert updated_links.items[0].last_online == timestamp_early


@pytest.mark.asyncio(loop_scope="session")
async def test_links_between_batches(service: Service):
    links = [LinkFactory().build(), LinkFactory().build(), LinkFactory().build()]
    await service.import_links([link.uri for link in links])

    timestamp = utc_now()
    with freeze_time(timestamp):
        resultA = await service.check_link_status(timestamp, 2)
        resultB = await service.check_link_status(timestamp, 2)

    assert resultA == 2
    assert resultB == 1
    updated_links = await service.get_links(PaginationOptions(offset=0, limit=10))

    assert updated_links.total_count == 3
    assert updated_links.items[0].last_checked == timestamp
    assert updated_links.items[1].last_checked == timestamp
    assert updated_links.items[2].last_checked == timestamp


@pytest.mark.asyncio(loop_scope="session")
async def test_check_new_link_with_new_link(
    service: Service, link_checker: FakeLinkChecker
):
    uri = "https://example.com"
    link_checker.register_link(uri)

    timestamp = utc_now()
    with freeze_time(timestamp):
        status = await service.check_new_link(uri)

    assert status == True
    link = await service.get_link(uri)
    assert link is None
