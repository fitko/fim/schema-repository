from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.factories.xzufi import TEST_ONLINEDIENST_XML, OnlinedienstFactory

ENDPOINT = "/api/v0/online-service"


def test_return_existing_onlinedienst(runner: CommandRunner, client: TestClient):
    onlinedienst = OnlinedienstFactory().save(runner, xml_content=TEST_ONLINEDIENST_XML)

    response = client.get(
        f"{ENDPOINT}/{onlinedienst.redaktion_id}/{onlinedienst.id}/xzufi"
    )

    assert response.status_code == 200
    assert response.text == TEST_ONLINEDIENST_XML
    assert (
        response.headers["content-disposition"]
        == f'attachment; filename="onlinedienst_{onlinedienst.redaktion_id}_{onlinedienst.id}.xzufi.xml"'
    )


def test_fail_for_unknown_onlinedienst(client: TestClient):
    response = client.get(f"{ENDPOINT}/L1234/odi/xzufi")

    assert response.status_code == 404
    assert response.json() == {
        "detail": "Could not find Onlinedienst odi from Redaktion L1234.",
    }
