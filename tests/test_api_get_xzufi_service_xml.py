from fastapi.testclient import TestClient

from fimportal.xzufi.common import remove_user_data
from tests.conftest import CommandRunner
from tests.data import XZUFI_2_2_0_DATA
from tests.factories.xzufi import LeistungFactory

ENDPOINT = "/api/v0/xzufi-services"


def test_return_existing_leistung(runner: CommandRunner, client: TestClient):
    xml_content = (XZUFI_2_2_0_DATA / "leistung_with_user_data.xml").read_text()

    LeistungFactory(
        redaktion_id="L1234",
        leistung_id="leistung_1",
    ).save_pvog(runner, xml_content=xml_content)

    response = client.get(f"{ENDPOINT}/L1234/leistung_1/xzufi")

    assert response.status_code == 200
    assert response.text == remove_user_data(xml_content)
    assert (
        response.headers["content-disposition"]
        == 'attachment; filename="L1234_leistung_1.xzufi.xml"'
    )


def test_fail_for_steckbrief(client: TestClient, runner: CommandRunner) -> None:
    leistung = LeistungFactory.steckbrief().save_leika(runner)

    response = client.get(
        f"{ENDPOINT}/{leistung.redaktion_id}/{leistung.leistung_id}/xzufi"
    )

    assert response.status_code == 404
    assert response.json() == {
        "detail": f"Could not find service {leistung.leistung_id} from Redaktion {leistung.redaktion_id}.",
    }


def test_fail_for_unknown_leistung(client: TestClient):
    response = client.get(f"{ENDPOINT}/L1234/leistung_1/xzufi")

    assert response.status_code == 404
    assert response.json() == {
        "detail": "Could not find service leistung_1 from Redaktion L1234.",
    }
