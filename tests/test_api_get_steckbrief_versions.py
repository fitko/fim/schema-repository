from fastapi.testclient import TestClient
from freezegun import freeze_time

from tests.conftest import CommandRunner
from tests.factories import Xdf2SteckbriefImportFactory
from fimportal.helpers import format_iso8601, optional_date_to_string


def test_should_return_all_versions_for_a_steckbrief(
    runner: CommandRunner, client: TestClient
):
    freeze_timestamp = "2023-10-17T00:00:00Z"
    with freeze_time(freeze_timestamp):
        steckbrief_v1 = Xdf2SteckbriefImportFactory("D123", "1.0").full().save(runner)
        steckbrief_v2 = Xdf2SteckbriefImportFactory("D123", "2.0").full().save(runner)

    response = client.get("/api/v1/document-profiles/D123")

    assert response.status_code == 200
    assert response.json() == [
        {
            "fim_id": "D123",
            "fim_version": "1.0",
            "nummernkreis": "12000",
            "name": steckbrief_v1.name,
            "definition": steckbrief_v1.definition,
            "freigabe_status": steckbrief_v1.freigabe_status.value,
            "freigabe_status_label": steckbrief_v1.freigabe_status_label,
            "status_gesetzt_durch": steckbrief_v1.status_gesetzt_durch,
            "status_gesetzt_am": optional_date_to_string(
                steckbrief_v1.status_gesetzt_am
            ),
            "gueltig_ab": optional_date_to_string(steckbrief_v1.gueltig_ab),
            "gueltig_bis": optional_date_to_string(steckbrief_v1.gueltig_bis),
            "bezug": steckbrief_v1.bezug,
            "versionshinweis": steckbrief_v1.versionshinweis,
            "veroeffentlichungsdatum": optional_date_to_string(
                steckbrief_v1.veroeffentlichungsdatum
            ),
            "letzte_aenderung": format_iso8601(steckbrief_v1.letzte_aenderung),
            "last_update": freeze_timestamp,
            "bezeichnung": steckbrief_v1.bezeichnung,
            "dokumentart": steckbrief_v1.dokumentart.value,
            "stichwort": steckbrief_v1.stichwort,
            "xdf_version": "2.0",
            "beschreibung": steckbrief_v1.beschreibung,
            "ist_abstrakt": steckbrief_v1.ist_abstrakt,
            "hilfetext": steckbrief_v1.hilfetext,
            "is_latest": False,
            "fts_match": None,
        },
        {
            "fim_id": "D123",
            "fim_version": "2.0",
            "nummernkreis": "12000",
            "name": steckbrief_v2.name,
            "definition": steckbrief_v2.definition,
            "freigabe_status": steckbrief_v2.freigabe_status.value,
            "freigabe_status_label": steckbrief_v2.freigabe_status_label,
            "status_gesetzt_durch": steckbrief_v2.status_gesetzt_durch,
            "status_gesetzt_am": optional_date_to_string(
                steckbrief_v2.status_gesetzt_am
            ),
            "gueltig_ab": optional_date_to_string(steckbrief_v2.gueltig_ab),
            "gueltig_bis": optional_date_to_string(steckbrief_v2.gueltig_bis),
            "bezug": steckbrief_v2.bezug,
            "versionshinweis": steckbrief_v2.versionshinweis,
            "veroeffentlichungsdatum": optional_date_to_string(
                steckbrief_v2.veroeffentlichungsdatum
            ),
            "letzte_aenderung": format_iso8601(steckbrief_v2.letzte_aenderung),
            "last_update": freeze_timestamp,
            "bezeichnung": steckbrief_v2.bezeichnung,
            "dokumentart": steckbrief_v2.dokumentart.value,
            "stichwort": steckbrief_v2.stichwort,
            "xdf_version": "2.0",
            "beschreibung": steckbrief_v2.beschreibung,
            "ist_abstrakt": steckbrief_v2.ist_abstrakt,
            "hilfetext": steckbrief_v2.hilfetext,
            "is_latest": True,
            "fts_match": None,
        },
    ]


def test_should_return_empty_list_for_unknown_document_profile(client: TestClient):
    response = client.get("/api/v1/document-profiles/D12345")

    assert response.status_code == 200
    assert response.json() == []
