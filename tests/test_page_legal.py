from fastapi.testclient import TestClient


def test_load_barrierefreiheit_page(client: TestClient) -> None:
    response = client.get("/barrierefreiheit")

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_load_datenschutz_page(client: TestClient) -> None:
    response = client.get("/datenschutz")

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"
