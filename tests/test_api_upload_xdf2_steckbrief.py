from fastapi.testclient import TestClient
from freezegun import freeze_time

from fimportal.xdatenfelder import xdf3
from tests.conftest import CommandRunner
from tests.data import XDF2_DATA

POST_STECKBRIEF_ROUTE = "/api/v1/document-profiles/xdf2"


def test_should_fail_for_missing_access_token(client: TestClient):
    with open(XDF2_DATA / "steckbrief.xdf2.xml", "rb") as steckbrief:
        response = client.post(
            POST_STECKBRIEF_ROUTE,
            files=[("document_profile", steckbrief)],
            data={
                "freigabe_status": str(
                    xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                ),
            },
        )

    assert response.status_code == 401
    assert response.json() == {"detail": "invalid access token"}


def test_should_fail_for_wrong_access_token(client: TestClient):
    with open(XDF2_DATA / "steckbrief.xdf2.xml", "rb") as steckbrief:
        response = client.post(
            POST_STECKBRIEF_ROUTE,
            files=[("document_profile", steckbrief)],
            data={
                "freigabe_status": str(
                    xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                ),
            },
            headers={"Access-Token": "wrong_access_token"},
        )

    assert response.status_code == 401
    assert response.json() == {"detail": "invalid access token"}


def test_should_fail_for_invalid_steckbrief(runner: CommandRunner, client: TestClient):
    token = runner.create_access_token("12000")

    with open(XDF2_DATA / "code_list_a.xml", "rb") as schema:
        response = client.post(
            POST_STECKBRIEF_ROUTE,
            data={
                "freigabe_status": str(
                    xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                ),
            },
            files=[("document_profile", schema)],
            headers={"Access-Token": token},
        )

    assert response.status_code == 400
    assert response.json() == {
        "detail": "Could not parse document profile: Unexpected child node [tag={http://docs.oasis-open.org/codelist/ns/genericode/1.0/}CodeList], line 2"
    }


def test_should_correctly_save_the_steckbrief(
    runner: CommandRunner, client: TestClient
):
    token = runner.create_access_token("00000")

    freeze_timestamp = "2023-10-17T00:00:00Z"
    with freeze_time(freeze_timestamp):
        with open(XDF2_DATA / "steckbrief.xdf2.xml", "rb") as steckbrief:
            response = client.post(
                POST_STECKBRIEF_ROUTE,
                files=[("document_profile", steckbrief)],
                data={
                    "freigabe_status": str(
                        xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                    ),
                },
                headers={"Access-Token": token},
            )

    assert response.status_code == 200
    assert response.json() == {
        "fim_id": "D00000130",
        "fim_version": "1.0",
        "nummernkreis": "00000",
        "name": "Antrag Ausbildungsförderung ",
        "beschreibung": "Eine Steckrbief Beschreibung",
        "definition": "Definition",
        "freigabe_status": 6,
        "freigabe_status_label": "fachlich freigegeben (gold)",
        "status_gesetzt_durch": "BMBF",
        "status_gesetzt_am": "2019-08-27",
        "gueltig_ab": "2019-09-01",
        "gueltig_bis": "2020-09-01",
        "hilfetext": "Hilfetext",
        "ist_abstrakt": False,
        "bezug": ["Ein Bezug"],
        "versionshinweis": "Version zur Veröffentlichung",
        "veroeffentlichungsdatum": "2019-08-27",
        "letzte_aenderung": "2023-08-08T07:29:34.060000Z",
        "last_update": freeze_timestamp,
        "bezeichnung": "Antrag auf Ausbildungsförderung ",
        "dokumentart": "999",
        "stichwort": [],
        "relation": [],
        "xdf_version": "2.0",
        "is_latest": True,
        "prozesse": [],
        "datenschemata": [],
    }


def test_should_upload_even_for_identical_freigabe_status(
    runner: CommandRunner, client: TestClient
):
    token = runner.create_access_token("00000")
    content = (XDF2_DATA / "steckbrief.xdf2.xml").read_text()

    response = client.post(
        POST_STECKBRIEF_ROUTE,
        files=[("document_profile", content)],
        data={
            "freigabe_status": str(xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value),
        },
        headers={"Access-Token": token},
    )
    assert response.status_code == 200

    # Reupload with the same freigabe status
    response = client.post(
        POST_STECKBRIEF_ROUTE,
        files=[("document_profile", content)],
        data={
            "freigabe_status": str(xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value),
        },
        headers={"Access-Token": token},
    )
    assert response.status_code == 200


def test_should_fail_for_uploading_existing_steckbrief_with_changed_immutable_attribute(
    runner: CommandRunner, client: TestClient
):
    token = runner.create_access_token("00000")

    freeze_timestamp = "2023-10-17T00:00:00Z"
    with freeze_time(freeze_timestamp):
        with open(XDF2_DATA / "steckbrief.xdf2.xml", "rb") as steckbrief:
            response = client.post(
                POST_STECKBRIEF_ROUTE,
                files=[("document_profile", steckbrief)],
                data={
                    "freigabe_status": str(
                        xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                    ),
                },
                headers={"Access-Token": token},
            )

    assert response.status_code == 200

    with freeze_time(freeze_timestamp):
        with open(
            XDF2_DATA / "steckbrief_with_changed_attributes.xdf2.xml", "rb"
        ) as steckbrief:
            response = client.post(
                POST_STECKBRIEF_ROUTE,
                files=[("document_profile", steckbrief)],
                data={
                    "freigabe_status": str(
                        xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                    ),
                },
                headers={"Access-Token": token},
            )

    assert response.status_code == 400
    assert response.json() == {
        "detail": (
            "Cannot update document-profile D00000130 version 1.0 because the following attributes are different:\n"
            "  - definition: Definition -> Die Definition\n"
            "  - name: Antrag Ausbildungsförderung  -> Antrag Bildungsförderung "
        ),
    }
