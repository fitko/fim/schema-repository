from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.factories.xzufi import ZustaendigkeitFactory

ENDPOINT = "/api/v0/jurisdiction"


def test_return_all_zustaendigkeiten(runner: CommandRunner, client: TestClient):
    zustaendigkeit = ZustaendigkeitFactory().save(runner)

    response = client.get(ENDPOINT)

    assert response.status_code == 200
    assert response.json() == {
        "items": [
            {
                "redaktion_id": zustaendigkeit.redaktion_id,
                "id": zustaendigkeit.id,
            }
        ],
        "count": 1,
        "offset": 0,
        "limit": 200,
        "total_count": 1,
    }
