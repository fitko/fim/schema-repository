from fastapi.testclient import TestClient
import pytest

from fimportal.common import FreigabeStatus
from fimportal.xzufi.common import LeistungRedaktionId
from fimportal.xzufi.leistung import LeistungsTypisierung
from fimportal.helpers import format_iso8601, utc_now
from tests.conftest import CommandRunner
from tests.factories.prozess import ProzessklasseFactory
from tests.factories.xzufi import LeistungFactory

ENDPOINT = "/api/v0/services"


def test_fail_for_unknown_leistung(client: TestClient):
    response = client.get(f"{ENDPOINT}/99123123123000")

    assert response.status_code == 404
    assert response.json() == {
        "detail": "Could not find service 99123123123000.",
    }


@pytest.mark.parametrize(
    "typisierung",
    [
        LeistungsTypisierung.TYP_2,
        LeistungsTypisierung.TYP_3,
    ],
)
def test_return_existing_leistung(
    runner: CommandRunner, client: TestClient, typisierung: LeistungsTypisierung
):
    now = utc_now()
    leistungsschluessel = "99123400000000"

    leistung = (
        LeistungFactory(
            redaktion_id=LeistungRedaktionId.LEIKA.value,
            bezeichnung="Leistung 1",
            bezeichnung_2="LB2",
            freigabestatus_katalog=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
            freigabestatus_bibliothek=FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER,
        )
        .add_typisierung(typisierung)
        .add_sdg_informationsbereich("sdg_1")
        .add_leistungsschluessel(leistungsschluessel)
        .add_klassifizierung(list_uri="list_uri", value="value")
        .with_versionsinformation(
            erstellt_datum_zeit=now,
            geaendert_datum_zeit=now,
        )
        .save_leika(runner)
    )

    leistungsbeschreibung = (
        LeistungFactory(bezeichnung="Leistung 2")
        .add_typisierung(typisierung)
        .add_leistungsschluessel(leistungsschluessel)
        .save_pvog(runner)
    )

    response = client.get(f"{ENDPOINT}/{leistungsschluessel}")

    assert response.status_code == 200
    assert response.json() == {
        "redaktion_id": "TSA_TELEPORT",
        "leistung_id": leistung.leistung_id,
        "title": "Leistung 1",
        "leistungsbezeichnung": "Leistung 1",
        "leistungsbezeichnung_2": "LB2",
        "freigabestatus_katalog": FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value,
        "freigabestatus_bibliothek": FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER.value,
        "leistungsschluessel": "99123400000000",
        "typisierung": [typisierung.value],
        "leistungstyp": None,
        "sdg_informationsbereiche": ["sdg_1"],
        "klassifizierung": [{"list_uri": "list_uri", "value": "value"}],
        "erstellt_datum_zeit": format_iso8601(now),
        "geaendert_datum_zeit": format_iso8601(now),
        "has_stammtext": True,
        "leistungsbeschreibungen": [
            {
                "redaktion_id": leistungsbeschreibung.redaktion_id,
                "id": leistungsbeschreibung.id,
                "title": "Leistung 2",
            }
        ],
        "prozessklasse": None,
    }


class TestProzessklassenReferences:
    def test_return_existing_leistung(self, runner: CommandRunner, client: TestClient):
        leistung = LeistungFactory().steckbrief().save_leika(runner)
        prozess_klasse = ProzessklasseFactory(id=leistung.leistungsschluessel).save(
            runner
        )

        response = client.get(f"{ENDPOINT}/{leistung.leistungsschluessel}")

        assert response.status_code == 200
        assert response.json()["prozessklasse"] == {
            "id": leistung.leistungsschluessel,
            "name": prozess_klasse.name,
        }
