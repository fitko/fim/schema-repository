from datetime import date, datetime

import pytest

from fimportal.din91379 import parse_string_latin
from fimportal.xdatenfelder import xdf2, xdf2_to_xdf3, xdf3
from fimportal.xdatenfelder.common import ConversionException
from tests.factories import CodeListFactory
from tests.factories.xdf2 import Xdf2Factory

# Todo(Felix): Check for invalid din91379 inputs, or rely on implicit
# testing after the conversion by double-checking with the xdf3 parser


class TestRule:
    def test_should_convert_a_rule(self):
        rule = xdf2.Regel(
            identifier=xdf2.Identifier("R1234", xdf2.parse_version("1.0")),
            name="Regel",
            bezeichnung_eingabe=None,
            bezeichnung_ausgabe=None,
            beschreibung="Beschreibung",
            definition="Definition",
            bezug="Rechtsbezug",
            status=xdf2.Status.AKTIV,
            versionshinweis=None,
            gueltig_ab=None,
            gueltig_bis=None,
            fachlicher_ersteller="Ersteller",
            freigabedatum=None,
            veroeffentlichungsdatum=None,
            script="script",
        )
        letzte_aenderung = datetime.now()

        xdf3_regel = xdf2_to_xdf3.convert_rule(rule, letzte_aenderung)

        assert xdf3_regel == xdf3.Regel(
            identifier=xdf3.Identifier("R1200034", xdf3.parse_version("1.0.0")),
            name=parse_string_latin("Regel"),
            beschreibung=parse_string_latin("Beschreibung"),
            freitext_regel="Definition",
            bezug=[
                xdf3.Rechtsbezug(text=parse_string_latin("Rechtsbezug"), link=None),
            ],
            stichwort=[],
            fachlicher_ersteller="Ersteller",
            letzte_aenderung=letzte_aenderung,
            typ=xdf3.Regeltyp.KOMPLEX,
            param=[],
            ziel=[],
            skript="script",
            fehler=[],
        )


class TestField:
    def test_should_convert_a_data_field(self):
        field = (
            Xdf2Factory()
            .field(
                id="F1234",
                version="1.0",
                name="Datenfeld",
                beschreibung="Beschreibung",
                definition="Definition",
                bezug="Rechtsbezug",
                status=xdf2.Status.AKTIV,
                fachlicher_ersteller="Ersteller",
                versionshinweis="Hinweis",
                bezeichnung_eingabe="Eingabe",
                bezeichnung_ausgabe="Ausgabe",
                hilfetext_eingabe="Hilfe Eingabe",
                hilfetext_ausgabe="Hilfe Ausgabe",
                freigabedatum=date.today(),
            )
            .input(
                datentyp=xdf2.Datentyp.TEXT,
                praezisierung='{"minValue": 0, "maxValue": 1, "minLength": 0, "maxLength": 10, "pattern": "pattern"}',
            )
            .build()
        )
        letzte_aenderung = datetime.now()

        xdf3_field = xdf2_to_xdf3.convert_field(
            field,
            letzte_aenderung=letzte_aenderung,
            code_lists={},
        )

        assert xdf3_field == xdf3.Datenfeld(
            identifier=xdf3.Identifier("F1200034", version=xdf3.parse_version("1.0.0")),
            name=parse_string_latin("Datenfeld"),
            beschreibung=parse_string_latin("Beschreibung"),
            definition=parse_string_latin("Definition"),
            bezug=[
                xdf3.Rechtsbezug(text=parse_string_latin("Rechtsbezug"), link=None),
            ],
            freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
            status_gesetzt_am=field.freigabedatum,
            status_gesetzt_durch=parse_string_latin("Ersteller"),
            gueltig_ab=field.gueltig_ab,
            gueltig_bis=field.gueltig_bis,
            versionshinweis=parse_string_latin("Hinweis"),
            veroeffentlichungsdatum=field.veroeffentlichungsdatum,
            letzte_aenderung=letzte_aenderung,
            relation=[],
            stichwort=[],
            bezeichnung_eingabe=parse_string_latin("Eingabe"),
            bezeichnung_ausgabe=parse_string_latin("Ausgabe"),
            schema_element_art=field.schema_element_art,
            hilfetext_eingabe=parse_string_latin("Hilfe Eingabe"),
            hilfetext_ausgabe=parse_string_latin("Hilfe Ausgabe"),
            feldart=xdf3.Feldart.EINGABE,
            datentyp=xdf3.Datentyp.TEXT,
            praezisierung=xdf3.Praezisierung(
                min_value="0.0",
                max_value="1.0",
                min_length=0,
                max_length=10,
                pattern="pattern",
            ),
            inhalt=None,
            vorbefuellung=xdf3.Vorbefuellung.KEINE,
            werte=None,
            codeliste_referenz=None,
            code_key=None,
            name_key=None,
            help_key=None,
            regeln=[],
            max_size=None,
            media_type=[],
        )

    def test_should_ignore_invalid_praezisierung(self):
        field = (
            Xdf2Factory()
            .field()
            .input(
                datentyp=xdf2.Datentyp.TEXT,
                praezisierung="some invalid content",
            )
            .build()
        )

        xdf3_field = xdf2_to_xdf3.convert_field(
            field, letzte_aenderung=datetime.now(), code_lists={}
        )

        assert xdf3_field.praezisierung == None

    def test_should_embed_code_list(self):
        code_list = CodeListFactory(
            default_code_key="code",
            default_name_key="name",
            columns={"code": ["a", "b", "c"], "name": ["Name a", "Name b", "Name c"]},
        ).build()
        field = Xdf2Factory().field().select(code_list=code_list).build()

        xdf3_field = xdf2_to_xdf3.convert_field(
            field,
            letzte_aenderung=datetime.now(),
            code_lists={code_list.identifier: code_list},
        )

        assert xdf3_field.codeliste_referenz is None
        assert xdf3_field.werte == [
            xdf3.ListenWert("a", "Name a", None),
            xdf3.ListenWert("b", "Name b", None),
            xdf3.ListenWert("c", "Name c", None),
        ]

    def test_should_save_reference_for_missing_code_list_reference(self):
        code_list = CodeListFactory().build()
        field = Xdf2Factory().field().select(code_list=code_list).build()

        xdf3_field = xdf2_to_xdf3.convert_field(
            field,
            letzte_aenderung=datetime.now(),
            code_lists={},
        )

        assert field.code_listen_referenz is not None
        assert xdf3_field.werte == None
        assert (
            xdf3_field.codeliste_referenz
            == field.code_listen_referenz.genericode_identifier
        )

    def test_should_fail_for_code_list_on_non_select_field(self):
        # Todo(Felix): Should we enforce this?
        pass

    def test_should_convert_rule_references(self):
        factory = Xdf2Factory()
        rule = factory.rule(id="R1234", version="1.0").build()
        field = factory.field().with_rule(rule).build()

        xdf3_field = xdf2_to_xdf3.convert_field(
            field,
            letzte_aenderung=datetime.now(),
            code_lists={},
        )

        assert xdf3_field.regeln == [
            xdf3.Identifier("R1200034", xdf3.parse_version("1.0.0"))
        ]


class TestGroup:
    def test_should_convert_a_group(self):
        factory = Xdf2Factory()
        field = factory.field(id="F1234", version="1.0").build()
        group = (
            factory.group(
                id="G1234",
                version="1.0",
                beschreibung="Beschreibung",
                definition="Definition",
                bezug="Rechtsbezug",
                status=xdf2.Status.AKTIV,
                fachlicher_ersteller="Ersteller",
                versionshinweis="Hinweis",
                bezeichnung_eingabe="Eingabe",
                bezeichnung_ausgabe="Ausgabe",
                hilfetext_eingabe="Hilfe Eingabe",
                hilfetext_ausgabe="Hilfe Ausgabe",
                freigabedatum=date.today(),
            )
            .with_field(field, anzahl=xdf2.Anzahl(min=1, max=1), bezug="Rechtsbezug")
            .build()
        )
        letzte_aenderung = datetime.now()

        xdf3_group = xdf2_to_xdf3.convert_group(
            group, letzte_aenderung=letzte_aenderung
        )

        assert xdf3_group == xdf3.Gruppe(
            identifier=xdf3.Identifier(
                "G1200034",
                version=xdf3.parse_version("1.0.0"),
            ),
            name=parse_string_latin(group.name),
            beschreibung=parse_string_latin("Beschreibung"),
            definition=parse_string_latin("Definition"),
            bezug=[
                xdf3.Rechtsbezug(text=parse_string_latin("Rechtsbezug"), link=None),
            ],
            freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
            status_gesetzt_am=group.freigabedatum,
            status_gesetzt_durch=parse_string_latin("Ersteller"),
            gueltig_ab=group.gueltig_ab,
            gueltig_bis=group.gueltig_bis,
            versionshinweis=parse_string_latin("Hinweis"),
            veroeffentlichungsdatum=group.veroeffentlichungsdatum,
            letzte_aenderung=letzte_aenderung,
            relation=[],
            stichwort=[],
            bezeichnung_eingabe=parse_string_latin("Eingabe"),
            bezeichnung_ausgabe=parse_string_latin("Ausgabe"),
            schema_element_art=group.schema_element_art,
            hilfetext_eingabe=parse_string_latin("Hilfe Eingabe"),
            hilfetext_ausgabe=parse_string_latin("Hilfe Ausgabe"),
            art=None,
            regeln=[],
            struktur=[
                xdf3.ElementReference(
                    anzahl=xdf3.Anzahl(min=1, max=1),
                    bezug=[
                        xdf3.Rechtsbezug(
                            text=parse_string_latin("Rechtsbezug"), link=None
                        )
                    ],
                    element_type=xdf3.ElementType.FELD,
                    identifier=xdf3.Identifier(
                        "F1200034", version=xdf3.parse_version("1.0.0")
                    ),
                )
            ],
        )

    def test_should_fail_for_empty_group(self):
        group = Xdf2Factory().group(id="G1234", version="1.0").build()
        assert len(group.struktur) == 0

        with pytest.raises(
            ConversionException, match="Empty 'struktur' in group 'G1234V1.0'"
        ):
            xdf2_to_xdf3.convert_group(group, letzte_aenderung=datetime.now())

    def test_should_convert_rule_references(self):
        factory = Xdf2Factory()
        rule = factory.rule(id="R1234", version="1.0").build()
        group = factory.group().with_rule(rule).with_field().build()

        xdf3_group = xdf2_to_xdf3.convert_group(
            group,
            letzte_aenderung=datetime.now(),
        )

        assert xdf3_group.regeln == [
            xdf3.Identifier("R1200034", xdf3.parse_version("1.0.0"))
        ]


class TestSchemaMessage:
    def test_should_convert_a_complete_message(self):
        steckbrief = xdf3.Identifier("D12345", version=None)

        factory = Xdf2Factory()
        field = factory.field(id="F1234", version="1.0").build()
        xdf2_message = (
            factory.schema(
                id="S1234",
                version="1.0",
                name="Name",
                bezeichnung_eingabe="Eingabe",
                beschreibung="Beschreibung",
                definition="Definition",
                fachlicher_ersteller="Ersteller",
                versionshinweis="Hinweis",
                bezug="Rechtsbezug",
                freigabedatum=date.today(),
            )
            .with_field(field, anzahl=xdf2.Anzahl(min=1, max=1), bezug="Rechtsbezug")
            .message()
        )

        xdf3_message: xdf3.SchemaMessage = xdf2_to_xdf3.convert_message(
            xdf2_message,
            code_lists={},
            steckbrief=steckbrief,
        )

        assert xdf3_message.schema == xdf3.Schema(
            identifier=xdf3.Identifier("S1200034", version=xdf3.parse_version("1.0.0")),
            name=parse_string_latin("Name"),
            beschreibung=parse_string_latin("Beschreibung"),
            definition=parse_string_latin("Definition"),
            bezug=[
                xdf3.Rechtsbezug(text=parse_string_latin("Rechtsbezug"), link=None),
            ],
            freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
            status_gesetzt_am=xdf2_message.schema.freigabedatum,
            status_gesetzt_durch=parse_string_latin("Ersteller"),
            gueltig_ab=xdf2_message.schema.gueltig_ab,
            gueltig_bis=xdf2_message.schema.gueltig_bis,
            versionshinweis=parse_string_latin("Hinweis"),
            veroeffentlichungsdatum=xdf2_message.schema.veroeffentlichungsdatum,
            letzte_aenderung=xdf2_message.header.erstellungs_zeitpunkt,
            relation=[],
            stichwort=[],
            bezeichnung=parse_string_latin("Eingabe"),
            hilfetext=xdf2_message.schema.hilfetext,
            ableitungsmodifikationen_struktur=xdf2_message.schema.ableitungsmodifikationen_struktur,
            ableitungsmodifikationen_repraesentation=xdf2_message.schema.ableitungsmodifikationen_repraesentation,
            dokumentsteckbrief=steckbrief,
            regeln=[],
            struktur=[
                xdf3.ElementReference(
                    anzahl=xdf3.Anzahl(min=1, max=1),
                    bezug=[
                        xdf3.Rechtsbezug(
                            text=parse_string_latin("Rechtsbezug"), link=None
                        )
                    ],
                    element_type=xdf3.ElementType.FELD,
                    identifier=xdf3.Identifier(
                        "F1200034", version=xdf3.parse_version("1.0.0")
                    ),
                )
            ],
        )

    def test_should_fail_for_empty_schema(self):
        message = Xdf2Factory().schema(id="S1234", version="1.0").message()
        assert len(message.schema.struktur) == 0

        with pytest.raises(
            ConversionException, match="Empty 'struktur' in schema 'S1234V1.0'"
        ):
            xdf2_to_xdf3.convert_message(message, code_lists={})

    def test_should_convert_rule_references(self):
        factory = Xdf2Factory()
        rule = factory.rule(id="R1234", version="1.0").build()
        message = factory.schema().with_rule(rule).with_field().message()

        xdf3_message = xdf2_to_xdf3.convert_message(message, code_lists={})

        assert xdf3_message.schema.regeln == [
            xdf3.Identifier("R1200034", xdf3.parse_version("1.0.0"))
        ]

    def test_should_include_sub_elements(self):
        factory = Xdf2Factory()
        field = factory.field().build()
        group = factory.group().with_field(field).build()
        rule = factory.rule().build()
        message = (
            factory.schema()
            .with_rule(rule)
            .with_group(group)
            .with_field(field)
            .message()
        )

        letzte_aenderung = message.header.erstellungs_zeitpunkt
        xdf3_field = xdf2_to_xdf3.convert_field(
            field, letzte_aenderung=letzte_aenderung, code_lists={}
        )
        xdf3_group = xdf2_to_xdf3.convert_group(
            group, letzte_aenderung=letzte_aenderung
        )
        xdf3_rule = xdf2_to_xdf3.convert_rule(rule, letzte_aenderung=letzte_aenderung)
        xdf3_message = xdf2_to_xdf3.convert_message(message, code_lists={})

        assert xdf3_message.fields == {xdf3_field.identifier: xdf3_field}
        assert xdf3_message.groups == {xdf3_group.identifier: xdf3_group}
        assert xdf3_message.rules == {xdf3_rule.identifier: xdf3_rule}
