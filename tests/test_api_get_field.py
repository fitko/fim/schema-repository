from datetime import date
import pytest
from fastapi.testclient import TestClient
from freezegun import freeze_time

from fimportal.helpers import format_iso8601
from fimportal.xdatenfelder import xdf2, xdf3
from tests.conftest import CommandRunner
from tests.factories.xdf2 import FieldFactory, Xdf2Factory
from tests.factories.genericode import CodeListImportFactory


def test_should_correctly_return_a_field(
    runner: CommandRunner,
    client: TestClient,
):
    factory = Xdf2Factory()
    field = factory.field(bezug="Bezug").build()

    with freeze_time("2023-10-17T00:00:00"):
        schema = factory.schema().with_field(field).save(runner)

    response = client.get(
        f"/api/v1/fields/baukasten/{field.identifier.id}/{field.identifier.version}"
    )

    assert response.status_code == 200
    assert response.json() == {
        "namespace": "baukasten",
        "fim_id": field.identifier.id,
        "fim_version": field.identifier.version,
        "nummernkreis": field.identifier.get_xdf3_nummernkreis(),
        "name": field.name,
        "beschreibung": field.beschreibung,
        "definition": field.definition,
        "bezug": ["Bezug"],
        "freigabe_status": 2,
        "freigabe_status_label": "in Bearbeitung",
        "status_gesetzt_am": field.freigabedatum,
        "status_gesetzt_durch": field.fachlicher_ersteller,
        "gueltig_ab": field.gueltig_ab,
        "gueltig_bis": field.gueltig_bis,
        "versionshinweis": field.versionshinweis,
        "veroeffentlichungsdatum": field.veroeffentlichungsdatum,
        "letzte_aenderung": format_iso8601(schema.letzte_aenderung),
        "last_update": "2023-10-17T00:00:00Z",
        "feldart": field.feldart.value,
        "datentyp": field.datentyp.value,
        "code_list": None,
        "regeln": [],
        "relation": [],
        "schemas": [
            {
                "fim_id": schema.fim_id,
                "fim_version": schema.fim_version,
                "nummernkreis": schema.nummernkreis,
                "name": schema.name,
                "beschreibung": schema.beschreibung,
                "definition": schema.definition,
                "bezug": schema.bezug,
                "freigabe_status": schema.freigabe_status.value,
                "freigabe_status_label": schema.freigabe_status_label,
                "gueltig_ab": schema.gueltig_ab,
                "gueltig_bis": schema.gueltig_bis,
                "status_gesetzt_durch": schema.status_gesetzt_durch,
                "versionshinweis": schema.versionshinweis,
                "stichwort": schema.stichwort,
                "steckbrief_id": schema.steckbrief_id,
                "xdf_version": schema.xdf_version.value,
                "letzte_aenderung": format_iso8601(schema.letzte_aenderung),
                "last_update": "2023-10-17T00:00:00Z",
                "status_gesetzt_am": schema.status_gesetzt_am,
                "bezug_components": ["Bezug"],
                "bezeichnung": schema.bezeichnung,
                "veroeffentlichungsdatum": schema.veroeffentlichungsdatum,
                "is_latest": True,
                "fts_match": None,
            }
        ],
        "xdf_version": "2.0",
        "is_latest": True,
    }


@pytest.mark.parametrize(
    "field_factory,freigabe_status",
    [
        (
            Xdf2Factory().field(status=xdf2.Status.INAKTIV),
            xdf3.FreigabeStatus.INAKTIV,
        ),
        (
            Xdf2Factory().field(status=xdf2.Status.IN_VORBEREITUNG),
            xdf3.FreigabeStatus.IN_BEARBEITUNG,
        ),
        (
            Xdf2Factory().field(
                status=xdf2.Status.AKTIV,
                freigabedatum=None,
            ),
            xdf3.FreigabeStatus.IN_BEARBEITUNG,
        ),
        (
            Xdf2Factory().field(
                status=xdf2.Status.AKTIV,
                freigabedatum=date.today(),
            ),
            xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        ),
    ],
)
def test_should_correctly_map_the_freigabe_status(
    runner: CommandRunner,
    client: TestClient,
    field_factory: FieldFactory,
    freigabe_status: xdf3.FreigabeStatus,
):
    field = field_factory.build()
    field_factory.factory.schema().with_field(field).save(runner)

    response = client.get(
        f"/api/v1/fields/baukasten/{field.identifier.id}/{field.identifier.version}"
    )

    assert response.status_code == 200
    assert response.json()["freigabe_status"] == freigabe_status.value


def test_should_return_latest_version(runner: CommandRunner, client: TestClient):
    factory = Xdf2Factory()
    latest_field_version = factory.field(version="2.0", bezug="Bezug").build()
    with freeze_time("2023-10-17T00:00:00"):
        schema = factory.schema().with_field(latest_field_version).save(runner)

    previous_field_version = factory.field(
        id=latest_field_version.identifier.id, version="1.0"
    ).build()
    factory.schema().with_field(previous_field_version).save(runner)

    response = client.get(
        f"/api/v1/fields/baukasten/{latest_field_version.identifier.id}/latest"
    )

    assert response.status_code == 200
    assert response.json() == {
        "namespace": "baukasten",
        "fim_id": latest_field_version.identifier.id,
        "fim_version": latest_field_version.identifier.version,
        "nummernkreis": latest_field_version.identifier.get_xdf3_nummernkreis(),
        "name": latest_field_version.name,
        "beschreibung": latest_field_version.beschreibung,
        "definition": latest_field_version.definition,
        "bezug": ["Bezug"],
        "freigabe_status": 2,
        "freigabe_status_label": "in Bearbeitung",
        "status_gesetzt_am": latest_field_version.freigabedatum,
        "status_gesetzt_durch": latest_field_version.fachlicher_ersteller,
        "gueltig_ab": latest_field_version.gueltig_ab,
        "gueltig_bis": latest_field_version.gueltig_bis,
        "versionshinweis": latest_field_version.versionshinweis,
        "veroeffentlichungsdatum": latest_field_version.veroeffentlichungsdatum,
        "letzte_aenderung": format_iso8601(schema.letzte_aenderung),
        "last_update": "2023-10-17T00:00:00Z",
        "feldart": latest_field_version.feldart.value,
        "datentyp": latest_field_version.datentyp.value,
        "code_list": None,
        "regeln": [],
        "relation": [],
        "schemas": [
            {
                "fim_id": schema.fim_id,
                "fim_version": schema.fim_version,
                "nummernkreis": schema.nummernkreis,
                "name": schema.name,
                "beschreibung": schema.beschreibung,
                "definition": schema.definition,
                "bezug": schema.bezug,
                "freigabe_status": schema.freigabe_status.value,
                "freigabe_status_label": schema.freigabe_status_label,
                "gueltig_ab": schema.gueltig_ab,
                "gueltig_bis": schema.gueltig_bis,
                "status_gesetzt_durch": schema.status_gesetzt_durch,
                "versionshinweis": schema.versionshinweis,
                "stichwort": schema.stichwort,
                "steckbrief_id": schema.steckbrief_id,
                "xdf_version": schema.xdf_version.value,
                "letzte_aenderung": format_iso8601(schema.letzte_aenderung),
                "last_update": "2023-10-17T00:00:00Z",
                "status_gesetzt_am": schema.status_gesetzt_am,
                "bezug_components": ["Bezug"],
                "bezeichnung": schema.bezeichnung,
                "veroeffentlichungsdatum": schema.veroeffentlichungsdatum,
                "is_latest": True,
                "fts_match": None,
            }
        ],
        "xdf_version": "2.0",
        "is_latest": True,
    }


def test_should_return_code_list_information_for_internal_code_lists(
    immutable_base_url: str, runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    code_list = CodeListImportFactory().build()
    field = factory.field().with_code_list(code_list).build()
    factory.schema().with_field(field).save(runner, code_lists=[code_list])

    response = client.get(
        f"/api/v1/fields/baukasten/{field.identifier.id}/{field.identifier.version}"
    )

    assert response.status_code == 200

    code_list_ref = response.json()["code_list"]
    assert code_list_ref is not None

    code_list_id = code_list_ref["id"]  # internal , exact value not important
    assert code_list_ref == {
        "id": code_list_id,
        "genericode_canonical_version_uri": code_list.identifier.canonical_version_uri,
        "source": "internal",
        "url": f"{immutable_base_url}/immutable/code-lists/{code_list_id}/genericode.xml",
    }


def test_should_return_code_list_information_for_external_code_lists(
    immutable_base_url: str, runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    code_list = CodeListImportFactory().build()
    field = factory.field().with_code_list(code_list).build()
    factory.schema().with_field(field).save(runner)

    response = client.get(
        f"/api/v1/fields/baukasten/{field.identifier.id}/{field.identifier.version}"
    )

    assert response.status_code == 200

    code_list_ref = response.json()["code_list"]
    assert code_list_ref is not None

    code_list_id = code_list_ref["id"]  # internal , exact value not important
    assert code_list_ref == {
        "id": code_list_id,
        "genericode_canonical_version_uri": code_list.identifier.canonical_version_uri,
        "source": None,
        "url": None,
    }


def test_should_fail_for_unknown_field_with_latest_version(client: TestClient):
    response = client.get("/api/v1/fields/baukasten/F1234/latest")

    assert response.status_code == 404
    assert response.json() == {
        "detail": "Could not find field F1234Vlatest in namespace baukasten.",
    }


def test_should_fail_for_unknown_field(
    client: TestClient,
):
    response = client.get("/api/v1/fields/baukasten/F1/1.0")

    assert response.status_code == 404
    assert response.json() == {
        "detail": "Could not find field F1V1.0 in namespace baukasten."
    }
