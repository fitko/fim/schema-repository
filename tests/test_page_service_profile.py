from fastapi.testclient import TestClient
import pytest

from fimportal import xzufi
from tests.conftest import CommandRunner
from tests.data import XZUFI_2_2_0_DATA


@pytest.mark.parametrize(
    "page",
    [
        "",
        "?language_code=de-DE",
        "?language_code=en",
    ],
)
def test_load_service_profile_page(
    client: TestClient, runner: CommandRunner, page: str
) -> None:
    with open(
        XZUFI_2_2_0_DATA / "leistung_steckbrief.xml", "r", encoding="utf-8"
    ) as file:
        data = file.read()

    leistung = xzufi.parse_leistung(data)
    runner.import_leika(leistung, xml_content=data)

    response = client.get(f"/services/{leistung.get_leistungsschluessel()[0]}{page}")

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_unknown_leistungsschluessel(client: TestClient) -> None:
    response = client.get("/services/99123123123000")

    assert response.status_code == 404
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert "Leistung 99123123123000 konnte nicht gefunden werden." in response.text
