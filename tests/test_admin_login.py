from datetime import datetime, timedelta, timezone
from fastapi.testclient import TestClient
from freezegun import freeze_time

from fimportal.routers.admin import ADMIN_SESSION_TOKEN


def test_should_return_the_login_page_if_not_logged_in(client: TestClient):
    response = client.get("/admin/login")

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_should_create_session_cookie_after_login_and_redirect_to_startpage(
    client: TestClient, admin_password: str
):
    response = client.post(
        "/admin/login",
        data={"username": "admin", "password": admin_password},
    )

    assert response.status_code == 302
    assert response.headers["location"] == "/admin/"
    assert response.cookies[ADMIN_SESSION_TOKEN] is not None


def test_should_fail_for_no_data(client: TestClient):
    response = client.post("/admin/login")

    assert response.status_code == 422
    assert ADMIN_SESSION_TOKEN not in response.cookies


def test_should_fail_for_wrong_username(client: TestClient, admin_password: str):
    response = client.post(
        "/admin/login",
        data={"username": "wrong", "password": admin_password},
    )

    assert response.status_code == 400
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert ADMIN_SESSION_TOKEN not in response.cookies


def test_should_fail_for_wrong_password(client: TestClient):
    response = client.post(
        "/admin/login",
        data={"username": "admin", "password": "wrong"},
    )

    assert response.status_code == 400
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert ADMIN_SESSION_TOKEN not in response.cookies


def test_should_redirect_to_admin_startpage_if_logged_in(admin_client: TestClient):
    response = admin_client.get("/admin/login")

    assert response.status_code == 307
    assert response.headers["location"] == "/admin/"


def test_should_invalide_session_after_30_days(admin_client: TestClient):
    now = datetime.now(timezone.utc)
    next_month = now + timedelta(days=31)

    with freeze_time(next_month):
        response = admin_client.get("/admin/")

    assert response.status_code == 307
    assert response.headers["location"] == "/admin/login"


def test_should_logout_the_admin(admin_client: TestClient):
    response = admin_client.get("/admin/logout")

    assert response.status_code == 307
    assert response.headers["location"] == "/admin/login"
    assert ADMIN_SESSION_TOKEN not in response.cookies
