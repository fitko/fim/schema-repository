"""
This file contains fixtures that are shared between all tests.

Fixtures, that are only used internally and not directly by the tests,
are prefixed with an underscore.
"""

import asyncio
import os
from contextlib import asynccontextmanager
from pathlib import Path
from typing import Any, Generator

import pytest
import pytest_asyncio
from asyncpg import Connection, Pool, connect
from fastapi.testclient import TestClient
from testcontainers.postgres import PostgresContainer

from fimportal import xrepository
from fimportal.cms import CMSContent
from fimportal.config import AppConfig, create_connection_pool
from fimportal.database import clear_database, create_new_database, reset_database
from fimportal.main import create_app_with_config
from fimportal.models.imports import (
    Xdf2SchemaImport,
    Xdf2SteckbriefImport,
    Xdf3SchemaImport,
)
from fimportal.models.xdf3 import FullSchemaOut, FullSteckbriefOut
from fimportal.models.xzufi import (
    FullLeistungsbeschreibungOut,
    FullLeistungssteckbriefOut,
    Link,
    OnlinedienstOut,
    OrganisationseinheitOut,
    SpezialisierungOut,
    XzufiSource,
    ZustaendigkeitOut,
)
from fimportal.routers.admin import ADMIN_SESSION_TOKEN
from fimportal.service import Service
from fimportal.xdatenfelder import xdf2, xdf3
from fimportal.xdatenfelder.xdf3.steckbrief_message import SteckbriefMessage
from fimportal.xprozesse.client import TEST_PROZESS_XML, TEST_PROZESSKLASSE_XML
from fimportal.xprozesse.prozess import Prozess, Prozessklasse
from fimportal.xzufi import link_check
from fimportal.xzufi.client import TEST_LEISTUNG_XML
from fimportal.xzufi.leistung import Leistung
from fimportal.xzufi.onlinedienst import Onlinedienst
from fimportal.xzufi.organisationseinheit import Organisationseinheit
from fimportal.xzufi.spezialisierung import Spezialisierung
from fimportal.xzufi.zustaendigkeit import Zustaendigkeit


@pytest.fixture(scope="session")
def _async_event_loop():  # type: ignore [reportUnusedFunction]
    return asyncio.new_event_loop()


@pytest.fixture(scope="session")
def baseurl() -> str:
    return "http://dev"


@pytest.fixture(scope="session")
def immutable_base_url() -> str:
    return "http://immutable"


def _detect_external_database_url() -> str | None:
    if "TEST_DATABASE_URL" in os.environ:
        # Used during CI
        return os.environ["TEST_DATABASE_URL"]
    else:
        # Reuse the local dev database, if configured
        # Manually set the environment variable to the default local config location
        os.environ["FIM_PORTAL_CONFIG_FILE"] = "./fimportal.config.json"

        try:
            config = AppConfig.load_from_env()
        except Exception:
            return None

        return config.get_full_database_url()


@pytest.fixture(scope="session")
def _db_url(worker_id: str) -> Generator[str, None, None]:  # type: ignore [reportUnusedFunction]
    external_database_url = _detect_external_database_url()
    if external_database_url is not None:
        # Ensure that the URL is prefixed with `postgresql://`
        if not external_database_url.startswith("postgresql://"):
            external_database_url = f"postgresql://{external_database_url}"

        # Create a new database for the current worker
        yield asyncio.run(
            create_new_database(
                external_database_url,
                database_name=worker_id,
            ),
        )
    else:
        # No external database URL found, use a temporary Postgres container
        container = PostgresContainer(image="postgres:15.7", driver=None)
        container.with_command("postgres -c fsync=off")

        with container as postgres:
            yield postgres.get_connection_url()


@pytest.fixture(scope="session")
def _global_db_connection(  # type: ignore [reportUnusedFunction]
    _async_event_loop: asyncio.AbstractEventLoop,
    _db_url: str,
) -> Generator[Connection, Any, None]:
    connection: Connection = _async_event_loop.run_until_complete(connect(_db_url))

    async def _reset(db_connection: Connection):
        await reset_database(db_connection)

    _async_event_loop.run_until_complete(_reset(connection))

    yield connection

    _async_event_loop.run_until_complete(connection.close())


@pytest.fixture(scope="function")
def _cleared_db_connection(  # type: ignore [reportUnusedFunction]
    _async_event_loop: asyncio.AbstractEventLoop,
    _global_db_connection: Connection,
):
    async def _clear(db_connection: Connection):
        await clear_database(db_connection)

    _async_event_loop.run_until_complete(_clear(_global_db_connection))

    return _global_db_connection


@pytest.fixture(scope="session")
def xrepository_client():
    return xrepository.FakeClient()


@pytest.fixture(scope="session")
def link_checker():
    return link_check.FakeLinkChecker()


@pytest.fixture(scope="session")
def admin_password() -> str:
    return "test"


@pytest.fixture(scope="session")
def admin_password_hash() -> str:
    return "$2b$12$AlZ6GMw07byv85DkNtzar.EsrgWhjSJbPbvKmzEexUp6Zgwxi/.x6"


@pytest.fixture(scope="session")
def _global_test_client(  # type: ignore [reportUnusedFunction]
    baseurl: str,
    immutable_base_url: str,
    admin_password_hash: str,
    xrepository_client: xrepository.Client,
    link_checker: link_check.BaseLinkChecker,
    _db_url: str,
):
    """
    A single, global app instance wrapped in a test client.

    This way, the same instance can be reused between tests, which makes
    the tests twice as fast.
    """

    with open("example_content.json") as f:
        cms_content = CMSContent.model_validate_json(f.read())

    app = create_app_with_config(
        database_url=_db_url,
        xrepository_client=xrepository_client,
        link_checker=link_checker,
        admin_password_hash=admin_password_hash,
        baseurl=baseurl,
        immutable_base_url=immutable_base_url,
        user_tracking=False,
        cms_content=cms_content,
        cms_assets_dir=None,
        cms_content_assets_dir=None,
        show_production_header=False,
        sentry_config=None,
    )

    with TestClient(app) as test_client:
        test_client.follow_redirects = False

        yield test_client


@pytest.fixture()
def client(
    _cleared_db_connection: Connection,
    xrepository_client: xrepository.FakeClient,
    link_checker: link_check.FakeLinkChecker,
    _global_test_client: TestClient,
):
    """
    This fixture should be used in tests, not the global app fixture.
    By depending on `async_db_connection`, this ensures that the database is reset between runs.
    The XRepository client is cleared manually here.
    """
    xrepository_client.clear()
    link_checker.clear()

    return _global_test_client


@pytest.fixture()
def admin_client(client: TestClient, admin_password: str):
    response = client.post(
        "/admin/login",
        data={"username": "admin", "password": admin_password},
    )

    session_token = response.cookies[ADMIN_SESSION_TOKEN]
    client.cookies.set(ADMIN_SESSION_TOKEN, session_token)

    return client


class CommandRunner:
    """
    Custom command runner to simplify invoking the import within tests.
    """

    def __init__(
        self,
        event_loop: asyncio.AbstractEventLoop,
        immutable_base_url: str,
        db_connection: Connection,
        xrepository_service: xrepository.Service,
        link_checker: link_check.Service,
    ):
        self._event_loop = event_loop
        self._immutable_base_url = immutable_base_url
        self._db_connection = db_connection
        self._xrepository_service = xrepository_service
        self._link_checker = link_checker

    def import_xdf2_file(
        self,
        path: str | Path,
        freigabe_status: xdf3.FreigabeStatus = xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        steckbrief_id: str = "D12345",
    ) -> FullSchemaOut:
        async def _import() -> FullSchemaOut:
            with open(path, "r", encoding="utf-8") as file:
                xdf2_content = file.read()
                message = xdf2.parse_schema_message(xdf2_content)

            xdf2_import = Xdf2SchemaImport(
                schema_text=xdf2_content,
                schema_message=message,
                freigabe_status=freigabe_status,
                steckbrief_id=steckbrief_id,
                code_list_imports=[],
            )

            async with self._service() as service:
                await service.import_xdf2_schema(xdf2_import, strict=True)

                schema = await service.get_schema(xdf2_import.id, xdf2_import.version)
                assert schema is not None
                return schema

        return self._event_loop.run_until_complete(_import())

    def import_xdf3_file(
        self,
        path: str | Path,
    ) -> FullSchemaOut:
        async def _import() -> FullSchemaOut:
            with open(path, "r", encoding="utf-8") as file:
                xdf3_content = file.read()
                message = xdf3.parse_schema_message(xdf3_content)

                async with self._service() as service:
                    await service.import_xdf3_schema(
                        Xdf3SchemaImport(
                            schema_text=xdf3_content, schema_message=message
                        ),
                        strict=False,
                    )

                    schema = await service.get_schema(
                        message.id, message.schema.identifier.assert_version()
                    )
                    assert schema is not None
                    return schema

        return self._event_loop.run_until_complete(_import())

    def import_xdf2(
        self, schema_import: Xdf2SchemaImport, strict: bool = True
    ) -> FullSchemaOut:
        async def _import() -> FullSchemaOut:
            async with self._service() as service:
                await service.import_xdf2_schema(schema_import, strict=strict)

                schema = await service.get_schema(
                    schema_import.id, schema_import.version
                )
                assert schema is not None
                return schema

        return self._event_loop.run_until_complete(_import())

    def save_xdf3(self, message: xdf3.SchemaMessage):
        xdf3_import = Xdf3SchemaImport(
            schema_text=xdf3.serialize_schema_message(message).decode("utf-8"),
            schema_message=message,
        )

        return self.import_xdf3(xdf3_import)

    def import_xdf3(
        self, xdf3_import: Xdf3SchemaImport, strict: bool = True
    ) -> FullSchemaOut:
        async def _import() -> FullSchemaOut:
            async with self._service() as service:
                await service.import_xdf3_schema(xdf3_import, strict=strict)

                schema = await service.get_schema(xdf3_import.id, xdf3_import.version)
                assert schema is not None
                return schema

        return self._event_loop.run_until_complete(_import())

    def import_xdf2_steckbrief(
        self, steckbrief_import: Xdf2SteckbriefImport
    ) -> FullSteckbriefOut:
        async def _import() -> FullSteckbriefOut:
            async with self._service() as service:
                await service.import_xdf2_steckbrief(
                    steckbrief_import, check_status=True
                )
                steckbrief_out = await service.get_steckbrief(
                    steckbrief_import.id, steckbrief_import.version
                )
                assert steckbrief_out is not None

                return steckbrief_out

        return self._event_loop.run_until_complete(_import())

    def import_xdf3_steckbrief(
        self, steckbrief_message: SteckbriefMessage, content: str
    ):
        async def _import() -> FullSteckbriefOut:
            async with self._service() as service:
                await service.import_xdf3_steckbrief(
                    steckbrief_message, content, check_status=True
                )
                steckbrief_out = await service.get_steckbrief(
                    steckbrief_message.fim_id(), steckbrief_message.assert_fim_version()
                )
                assert steckbrief_out is not None
                return steckbrief_out

        return self._event_loop.run_until_complete(_import())

    def import_pvog(
        self,
        leistung: Leistung,
        xml_content: str = TEST_LEISTUNG_XML,
    ) -> FullLeistungsbeschreibungOut:
        async def _import():
            async with self._service() as service:
                identifiers = await service.import_leistung_batch(
                    [(leistung, xml_content)],
                    source=XzufiSource.PVOG,
                )
                assert len(identifiers) == 1

                leistung_out = await service.get_leistungsbeschreibung(identifiers[0])
                assert leistung_out is not None

                return leistung_out

        return self._event_loop.run_until_complete(_import())

    def import_organisationseinheit(
        self,
        orga_einheit: Organisationseinheit,
        xml_content: str,
    ) -> OrganisationseinheitOut:
        async def _import():
            async with self._service() as service:
                identifiers = await service.import_organisationseinheit_batch(
                    [(orga_einheit, xml_content)],
                )
                assert len(identifiers) == 1

                orga_einheit_out = await service.get_xzufi_organisationseinheit(
                    identifiers[0]
                )
                assert orga_einheit_out is not None

                return orga_einheit_out

        return self._event_loop.run_until_complete(_import())

    def import_zustaendigkeit(
        self,
        zustaendigkeit: Zustaendigkeit,
        xml_content: str,
    ) -> ZustaendigkeitOut:
        async def _import():
            async with self._service() as service:
                identifiers = await service.import_zustaendigkeit_batch(
                    [(zustaendigkeit, xml_content)],
                )
                assert len(identifiers) == 1

                zustaendigkeit_out = await service.get_xzufi_zustaendigkeit(
                    identifiers[0]
                )
                assert zustaendigkeit_out is not None

                return zustaendigkeit_out

        return self._event_loop.run_until_complete(_import())

    def import_onlinedienst(
        self,
        onlinedienst: Onlinedienst,
        xml_content: str,
    ) -> OnlinedienstOut:
        async def _import():
            async with self._service() as service:
                identifiers = await service.import_onlinedienst_batch(
                    [(onlinedienst, xml_content)],
                )
                assert len(identifiers) == 1

                onlinedienst_out = await service.get_xzufi_onlinedienst(identifiers[0])
                assert onlinedienst_out is not None

                return onlinedienst_out

        return self._event_loop.run_until_complete(_import())

    def import_spezialisierung(
        self,
        spezialisierung: Spezialisierung,
        xml_content: str,
    ) -> SpezialisierungOut:
        async def _import():
            async with self._service() as service:
                identifiers = await service.import_spezialisierung_batch(
                    [(spezialisierung, xml_content)],
                )
                assert len(identifiers) == 1

                spezialisierung_out = await service.get_xzufi_spezialisierung(
                    identifiers[0]
                )
                assert spezialisierung_out is not None

                return spezialisierung_out

        return self._event_loop.run_until_complete(_import())

    def import_leika(
        self,
        leistung: Leistung,
        xml_content: str = TEST_LEISTUNG_XML,
    ) -> FullLeistungssteckbriefOut:
        async def _import():
            async with self._service() as service:
                identifiers = await service.import_leistung_batch(
                    [(leistung, xml_content)],
                    source=XzufiSource.XZUFI,
                )
                assert len(identifiers) == 1

                leistung_out = await service.get_leistungssteckbrief(
                    leistung.get_leistungsschluessel()[0]
                )
                assert leistung_out is not None

                return leistung_out

        return self._event_loop.run_until_complete(_import())

    def import_prozess(self, prozess: Prozess, xml_content: str = TEST_PROZESS_XML):
        async def _import():
            async with self._service() as service:
                await service.import_prozess_batch([(prozess, xml_content)])

        return self._event_loop.run_until_complete(_import())

    def import_prozessklasse(
        self, prozessklasse: Prozessklasse, xml_content: str = TEST_PROZESSKLASSE_XML
    ):
        async def _import():
            async with self._service() as service:
                await service.import_prozessklasse_batch([(prozessklasse, xml_content)])

        return self._event_loop.run_until_complete(_import())

    def import_link(self, link: Link) -> None:
        async def _import():
            async with self._service() as service:
                await service.import_links([link.uri])
                await service.update_links([link])

        return self._event_loop.run_until_complete(_import())

    def create_access_token(self, nummernkreis: str) -> str:
        async def _import() -> str:
            async with self._service() as service:
                return await service.create_api_token(nummernkreis, description="test")

        return self._event_loop.run_until_complete(_import())

    @asynccontextmanager
    async def _service(self):
        async with self._db_connection.transaction():
            yield Service.from_connection(
                immutable_base_url=self._immutable_base_url,
                connection=self._db_connection,
                xrepository_service=self._xrepository_service,
                link_checker=self._link_checker,
            )


@pytest_asyncio.fixture(scope="session")  # type: ignore
async def async_db_pool(
    _db_url: str,
):
    """
    This function provides a database pool for the async command tests
    (e.g. test_command_sync_leistungen.py).

    The pool is created once and shared between all tests.
    """
    async with create_connection_pool(_db_url) as pool:
        yield pool


@pytest_asyncio.fixture()  # type: ignore
def get_service(
    immutable_base_url: str,
    _cleared_db_connection: Connection,
    async_db_pool: Pool,
    xrepository_client: xrepository.Client,
    link_checker: link_check.BaseLinkChecker,
):
    xrepository_service = xrepository.Service(xrepository_client)
    link_checker_service = link_check.Service(link_checker)

    @asynccontextmanager
    async def _get_service():
        async with async_db_pool.acquire() as connection:
            async with connection.transaction():
                yield Service.from_connection(
                    immutable_base_url=immutable_base_url,
                    connection=connection,
                    xrepository_service=xrepository_service,
                    link_checker=link_checker_service,
                )

    return _get_service


@pytest_asyncio.fixture()  # type: ignore
async def service(
    immutable_base_url: str,
    _cleared_db_connection: Connection,
    async_db_pool: Pool,
    xrepository_client: xrepository.Client,
    link_checker: link_check.BaseLinkChecker,
):
    xrepository_service = xrepository.Service(xrepository_client)
    link_checker_service = link_check.Service(link_checker)

    async with async_db_pool.acquire() as connection:
        async with connection.transaction():
            yield Service.from_connection(
                immutable_base_url=immutable_base_url,
                connection=connection,
                xrepository_service=xrepository_service,
                link_checker=link_checker_service,
            )


@pytest.fixture()
def runner(
    _async_event_loop: asyncio.AbstractEventLoop,
    immutable_base_url: str,
    _cleared_db_connection: Connection,
    xrepository_client: xrepository.Client,
    link_checker: link_check.BaseLinkChecker,
) -> CommandRunner:
    xrepository_service = xrepository.Service(xrepository_client)
    link_checker_service = link_check.Service(link_checker)

    return CommandRunner(
        _async_event_loop,
        immutable_base_url,
        _cleared_db_connection,
        xrepository_service,
        link_checker_service,
    )
