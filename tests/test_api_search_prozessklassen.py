from datetime import datetime, timezone
import pytest
from fastapi.testclient import TestClient

from fimportal.common import FreigabeStatus
from fimportal.helpers import format_iso8601
from fimportal.xprozesse.prozess import (
    Handlungsform,
    OperativesZiel,
    Verfahrensart,
    parse_prozess_message,
)
from tests.conftest import CommandRunner
from tests.data import XPROZESS2_DATA
from tests.factories.prozess import ProzessklasseFactory

ENDPOINT = "/api/v0/processclasses"


def test_return_empty_list(client: TestClient):
    response = client.get(ENDPOINT)

    assert response.status_code == 200

    assert response.json() == {
        "items": [],
        "count": 0,
        "offset": 0,
        "limit": 200,
        "total_count": 0,
    }


def test_return_existing_prozessklassen(runner: CommandRunner, client: TestClient):
    prozessklasse = ProzessklasseFactory(
        letzter_aenderungszeitpunkt=datetime(2023, 5, 5, tzinfo=timezone.utc)
    ).save(runner)
    assert prozessklasse.zustandsangaben is not None
    assert prozessklasse.zustandsangaben.letzter_aenderungszeitpunkt is not None

    response = client.get(ENDPOINT)

    assert response.status_code == 200
    assert response.json() == {
        "items": [
            {
                "id": prozessklasse.id,
                "version": prozessklasse.version,
                "name": prozessklasse.name,
                "freigabe_status": prozessklasse.freigabe_status,
                "letzter_aenderungszeitpunkt": format_iso8601(
                    prozessklasse.zustandsangaben.letzter_aenderungszeitpunkt
                ),
                "fts_match": None,
            }
        ],
        "count": 1,
        "offset": 0,
        "limit": 200,
        "total_count": 1,
    }


def test_return_prozessklassen_by_freigabe_status(
    runner: CommandRunner, client: TestClient
):
    prozessklasse = ProzessklasseFactory(
        freigabe_status=FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER
    ).save(runner)
    ProzessklasseFactory(freigabe_status=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD).save(
        runner
    )

    response = client.get(
        f"{ENDPOINT}?freigabe_status={FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER.value}"
    )

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["id"] == prozessklasse.id


def test_return_prozessklassen_by_operatives_ziel(
    runner: CommandRunner, client: TestClient
):
    prozessklasse = ProzessklasseFactory(
        operatives_ziel=OperativesZiel.ARBEIT_DER_VERWALTUNG
    ).save(runner)
    ProzessklasseFactory(
        operatives_ziel=OperativesZiel.ERZWINGUNG_DULDUNG_UNTERLASSUNG_HANDLUNG
    ).save(runner)

    response = client.get(
        f"{ENDPOINT}?operatives_ziel={OperativesZiel.ARBEIT_DER_VERWALTUNG.value}"
    )

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["id"] == prozessklasse.id


def test_return_prozessklassen_by_verfahrensart(
    runner: CommandRunner, client: TestClient
):
    prozessklasse = ProzessklasseFactory(
        verfahrensart=Verfahrensart.ANFECHTUNGSKLAGEVERFAHREN_FGO,
    ).save(runner)
    ProzessklasseFactory(
        verfahrensart=Verfahrensart.ANFECHTUNGSKLAGEVERFAHREN_SGG,
    ).save(runner)

    response = client.get(
        f"{ENDPOINT}?verfahrensart={Verfahrensart.ANFECHTUNGSKLAGEVERFAHREN_FGO.value}"
    )

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["id"] == prozessklasse.id


def test_return_prozessklassen_by_handlungsform(
    runner: CommandRunner, client: TestClient
):
    prozessklasse = ProzessklasseFactory(
        handlungsform=Handlungsform.ENTWICKLUNG_NORM_ODER_STANDARD
    ).save(runner)
    ProzessklasseFactory(handlungsform=Handlungsform.BESCHLUSS).save(runner)

    response = client.get(
        f"{ENDPOINT}?handlungsform={Handlungsform.ENTWICKLUNG_NORM_ODER_STANDARD.value}"
    )

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["id"] == prozessklasse.id


@pytest.mark.parametrize(
    "fts_query,fts_matches",
    [
        ("990100", ["99010023001000"]),
        ("fam", ["familiären"]),
        (
            "zuständige PERSON",
            ["Person", "zuständigen"],
        ),  # multiple searchterms are separated and connected with an and-relation
        ("https://www.gesetze-im-internet.de", ["www.gesetze-im-internet.de"]),
    ],
)
def test_get_prozessklassen_by_fts_query(
    runner: CommandRunner, client: TestClient, fts_query: str, fts_matches: list[str]
):
    with open(XPROZESS2_DATA / "prozess_without_files.xml", "rb") as file:
        data = file.read()

    prozess_message = parse_prozess_message(data)
    runner.import_prozessklasse(prozess_message.prozessklasse[0], data.decode("utf-8"))

    response = client.get(f"{ENDPOINT}?fts_query={fts_query}")
    assert response.status_code == 200

    result = response.json()
    assert len(result["items"]) == 1
    assert prozess_message.prozess[0].id == result["items"][0]["id"]
    for match in fts_matches:
        assert f"[[[{match}]]]" in result["items"][0]["fts_match"]
