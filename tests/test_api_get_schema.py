from datetime import date

from fastapi.testclient import TestClient
from freezegun import freeze_time

from fimportal.helpers import format_iso8601
from fimportal.xdatenfelder import xdf2, xdf3
from fimportal.xdatenfelder.common import Anzahl
from tests.conftest import CommandRunner
from tests.factories import Xdf2Factory
from tests.factories.genericode import CodeListFactory, CodeListImportFactory
from tests.factories.xdf2 import Xdf2SteckbriefImportFactory
from tests.factories.xdf3 import Xdf3Factory


def test_should_correctly_return_a_single_schema(
    immutable_base_url: str, runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    group = factory.group(id="G1234").build()
    group_bezug = "Rechtsbezug"
    group_anzahl = Anzahl(1, None)
    schema_import = (
        factory.schema(
            id="S1234",
            version="1.0",
            beschreibung="Beschreibung",
            definition="Definition",
            gueltig_ab=date(2020, 11, 4),
            gueltig_bis=date(2020, 11, 5),
            fachlicher_ersteller="Test",
            veroeffentlichungsdatum=date(2020, 11, 3),
        )
        .with_group(group, group_anzahl, group_bezug)
        .build_import()
    )

    with freeze_time("2023-10-17T00:00:00"):
        runner.import_xdf2(schema_import)

    response = client.get("/api/v1/schemas/S1234/1.0")

    schema = schema_import.schema_message.schema
    assert response.status_code == 200
    assert response.json() == {
        "fim_id": "S1234",
        "fim_version": "1.0",
        "nummernkreis": "12000",
        "name": schema.name,
        "beschreibung": "Beschreibung",
        "definition": "Definition",
        "bezug": [],
        "freigabe_status": schema_import.freigabe_status.value,
        "freigabe_status_label": schema_import.freigabe_status.to_label(),
        "gueltig_ab": "2020-11-04",
        "gueltig_bis": "2020-11-05",
        "status_gesetzt_am": schema.freigabedatum,
        "status_gesetzt_durch": "Test",
        "steckbrief_id": schema_import.steckbrief_id,
        "steckbrief_name": None,
        "stichwort": [],
        "versionshinweis": None,
        "veroeffentlichungsdatum": "2020-11-03",
        "datenfeldgruppen": [
            {
                "namespace": "baukasten",
                "fim_id": group.identifier.id,
                "fim_version": group.identifier.version,
                "nummernkreis": group.identifier.get_xdf3_nummernkreis(),
                "xdf_version": "2.0",
                "name": group.name,
                "beschreibung": group.beschreibung,
                "definition": group.definition,
                "freigabe_status": 2,
                "freigabe_status_label": "in Bearbeitung",
                "status_gesetzt_durch": group.fachlicher_ersteller,
                "children": [],
                "bezug": [],
            }
        ],
        "datenfelder": [],
        "xdf_version": "2.0",
        "bezeichnung": None,
        "letzte_aenderung": format_iso8601(
            schema_import.schema_message.header.erstellungs_zeitpunkt
        ),
        "last_update": "2023-10-17T00:00:00Z",
        "is_latest": True,
        "children": [
            {
                "namespace": "baukasten",
                "fim_id": group.identifier.id,
                "fim_version": group.identifier.version,
                "type": "Gruppe",
                "bezug": [group_bezug],
                "anzahl": "1:*",
            }
        ],
        "regeln": [],
        "relation": [],
        "uploads": [
            {
                "filename": "S1234V1.0_2023-10-17-1697500800000.xdf2.xml",
                "url": f"{immutable_base_url}/immutable/schemas/S1234V1.0_2023-10-17-1697500800000.xdf2.xml",
                "upload_time": "2023-10-17T00:00:00Z",
                "code_lists": [],
            },
        ],
        "json_schema_files": [
            {
                "filename": "S1234V1.0_2023-10-17-1697500800000.schema.json",
                "url": f"{immutable_base_url}/immutable/schemas/S1234V1.0_2023-10-17-1697500800000.schema.json",
                "upload_time": "2023-10-17T00:00:00Z",
                "generated_from": "S1234V1.0_2023-10-17-1697500800000.xdf2.xml",
            },
        ],
        "xsd_files": [
            {
                "filename": "S1234V1.0_2023-10-17-1697500800000.xsd",
                "url": f"{immutable_base_url}/immutable/schemas/S1234V1.0_2023-10-17-1697500800000.xsd",
                "upload_time": "2023-10-17T00:00:00Z",
                "generated_from": "S1234V1.0_2023-10-17-1697500800000.xdf2.xml",
            },
        ],
    }


def test_should_return_latest_schema_for_relations_without_version(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf3Factory()
    related_fim_id = "S000001"
    factory.schema(id=related_fim_id, version="1.0.0", name="Previous Name").save(
        runner
    )
    latest_related_schema = factory.schema(
        id=related_fim_id, version="1.0.1", name="New Name"
    ).save(runner)
    schema = factory.schema(
        id="S000002",
        relation=[
            xdf3.Relation(
                xdf3.RelationTyp.IST_ABGELEITET_VON,
                xdf3.Identifier(latest_related_schema.fim_id, None),
            )
        ],
    ).save(runner)

    response = client.get(f"/api/v1/schemas/{schema.fim_id}/{schema.fim_version}")

    assert response.status_code == 200
    result = response.json()
    assert result["relation"] == [
        {
            "fim_id": related_fim_id,
            "fim_version": None,
            "name": latest_related_schema.name,
            "typ": xdf3.RelationTyp.IST_ABGELEITET_VON.value,
        },
    ]


def test_should_return_relation_without_name(runner: CommandRunner, client: TestClient):
    factory = Xdf3Factory()
    related_fim_id = "S000002"
    schema = factory.schema(
        id="S000001",
        relation=[
            xdf3.Relation(
                xdf3.RelationTyp.IST_ABGELEITET_VON,
                xdf3.Identifier(related_fim_id, None),
            )
        ],
    ).save(runner)

    response = client.get(f"/api/v1/schemas/{schema.fim_id}/{schema.fim_version}")

    assert response.status_code == 200
    result = response.json()
    assert result["relation"] == [
        {
            "fim_id": related_fim_id,
            "fim_version": None,
            "name": None,
            "typ": xdf3.RelationTyp.IST_ABGELEITET_VON.value,
        },
    ]


def test_should_return_relations_with_version(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf3Factory()
    related_fim_id = "S000001"
    factory.schema(id=related_fim_id, version="1.0.1").save(runner)
    related_schema = factory.schema(id=related_fim_id, version="1.0.0").save(runner)
    related_fim_version = related_schema.fim_version
    assert related_fim_version is not None
    schema = factory.schema(
        id="S000002",
        relation=[
            xdf3.Relation(
                xdf3.RelationTyp.IST_ABGELEITET_VON,
                xdf3.Identifier(
                    related_schema.fim_id, xdf3.Version(related_fim_version)
                ),
            )
        ],
    ).save(runner)

    response = client.get(f"/api/v1/schemas/{schema.fim_id}/{schema.fim_version}")

    assert response.status_code == 200
    result = response.json()
    assert result["relation"] == [
        {
            "fim_id": related_schema.fim_id,
            "fim_version": related_fim_version,
            "name": related_schema.name,
            "typ": xdf3.RelationTyp.IST_ABGELEITET_VON.value,
        }
    ]


def test_should_return_latest_steckbrief_name_if_available(
    runner: CommandRunner, client: TestClient
):
    Xdf2SteckbriefImportFactory(id="D12345", version="1.0", name="old").save(runner)
    Xdf2SteckbriefImportFactory(id="D12345", version="2.0", name="new").save(runner)
    schema_import = Xdf2Factory().schema().save(runner, steckbrief_id="D12345")

    response = client.get(
        f"/api/v1/schemas/{schema_import.fim_id}/{schema_import.fim_version}"
    )

    assert response.status_code == 200
    assert response.json()["steckbrief_name"] == "new"


def test_should_correctly_return_a_single_schema_with_multiple_groups(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    group1 = factory.group().build()
    group2 = factory.group().build()
    schema = factory.schema().with_group(group1).with_group(group2).save(runner)

    response = client.get(f"/api/v1/schemas/{schema.fim_id}/{schema.fim_version}")

    assert response.status_code == 200
    assert len(response.json()["datenfeldgruppen"]) == 2


def test_should_return_schema_with_latest_version(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    factory.schema(id="S1234", version="1.0").save(runner)
    factory.schema(id="S1234", version="2.0").save(runner)

    response = client.get("/api/v1/schemas/S1234/latest")

    assert response.status_code == 200
    assert response.json()["fim_version"] == "2.0"


def test_should_return_the_full_latest_schema(
    runner: CommandRunner, client: TestClient
):
    schema = Xdf2Factory().schema().with_field().with_group().save(runner)

    response = client.get(f"/api/v1/schemas/{schema.fim_id}/latest")

    assert response.status_code == 200

    schema_out = response.json()
    assert len(schema_out["datenfeldgruppen"]) == 1
    assert len(schema_out["datenfelder"]) == 1
    assert len(schema_out["uploads"]) == 1
    assert len(schema_out["json_schema_files"]) == 1
    assert len(schema_out["xsd_files"]) == 1


def test_should_return_404_error_with_latest_version_when_none_exist(
    client: TestClient,
):
    response = client.get("/api/v1/schemas/S1234/latest")

    assert response.status_code == 404
    assert response.json() == {
        "detail": "Could not find schema S1234Vlatest.",
    }


def test_should_return_included_groups(runner: CommandRunner, client: TestClient):
    factory = Xdf2Factory()
    group = factory.group().build()
    schema = factory.schema().with_group(group).save(runner)

    response = client.get(f"/api/v1/schemas/{schema.fim_id}/{schema.fim_version}")

    assert response.status_code == 200
    assert response.json()["datenfeldgruppen"] == [
        {
            "namespace": "baukasten",
            "fim_id": group.identifier.id,
            "fim_version": group.identifier.version,
            "nummernkreis": group.identifier.get_xdf3_nummernkreis(),
            "name": group.name,
            "beschreibung": group.beschreibung,
            "definition": group.definition,
            "freigabe_status": 2,
            "freigabe_status_label": "in Bearbeitung",
            "status_gesetzt_durch": group.fachlicher_ersteller,
            "xdf_version": "2.0",
            "children": [],
            "bezug": [],
        }
    ]


def test_should_return_included_fields(runner: CommandRunner, client: TestClient):
    today = date.today()

    factory = Xdf2Factory()
    field = factory.field(
        bezug="Bezug",
        status=xdf2.Status.AKTIV,
        gueltig_ab=today,
        gueltig_bis=today,
        freigabedatum=today,
        veroeffentlichungsdatum=today,
    ).build()
    xdf2_import = factory.schema().with_field(field).build_import()

    with freeze_time("2023-10-17T00:00:00"):
        schema = runner.import_xdf2(xdf2_import)

    response = client.get(f"/api/v1/schemas/{schema.fim_id}/{schema.fim_version}")

    assert response.status_code == 200
    assert response.json()["datenfelder"] == [
        {
            "namespace": "baukasten",
            "fim_id": field.identifier.id,
            "fim_version": field.identifier.version,
            "nummernkreis": field.identifier.get_xdf3_nummernkreis(),
            "name": field.name,
            "beschreibung": field.beschreibung,
            "definition": field.definition,
            "freigabe_status": 6,
            "freigabe_status_label": "fachlich freigegeben (gold)",
            "gueltig_ab": today.isoformat(),
            "gueltig_bis": today.isoformat(),
            "bezug": ["Bezug"],
            "feldart": field.feldart.value,
            "datentyp": field.datentyp.value,
            "letzte_aenderung": format_iso8601(
                xdf2_import.schema_message.header.erstellungs_zeitpunkt
            ),
            "last_update": "2023-10-17T00:00:00Z",
            "versionshinweis": field.versionshinweis,
            "status_gesetzt_durch": field.fachlicher_ersteller,
            "status_gesetzt_am": today.isoformat(),
            "veroeffentlichungsdatum": today.isoformat(),
            "xdf_version": "2.0",
            "fts_match": None,
            "is_latest": True,
        }
    ]


def test_should_return_known_code_lists(
    immutable_base_url: str, runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    code_list = CodeListImportFactory().build()
    field = factory.field(id="F12001").select(code_list).build()

    (
        factory.schema(id="S12001", version="1.0")
        .with_field(field)
        .save(runner, code_lists=[code_list])
    )

    response = client.get("/api/v1/schemas/S12001/1.0")

    assert response.status_code == 200
    assert response.json()["uploads"][0]["code_lists"] == [
        {
            "id": 1,
            "url": f"{immutable_base_url}/immutable/code-lists/1/genericode.xml",
            "genericode_canonical_uri": code_list.identifier.canonical_uri,
            "genericode_version": code_list.identifier.version,
            "genericode_canonical_version_uri": code_list.identifier.canonical_version_uri,
            "is_external": False,
        },
    ]


def test_should_return_external_code_lists(runner: CommandRunner, client: TestClient):
    factory = Xdf2Factory()
    code_list = CodeListFactory().build()
    field = factory.field(id="F12001").select(code_list).build()
    factory.schema(id="S12001", version="1.0").with_field(field).save(runner)

    response = client.get("/api/v1/schemas/S12001/1.0")

    assert response.status_code == 200

    code_lists = response.json()["uploads"][0]["code_lists"]
    assert len(code_lists) == 1
    assert isinstance(code_lists[0]["id"], int)
    assert (
        code_lists[0]["genericode_canonical_uri"] == code_list.identifier.canonical_uri
    )
    assert code_lists[0]["genericode_version"] == code_list.identifier.version
    assert (
        code_lists[0]["genericode_canonical_version_uri"]
        == code_list.identifier.canonical_version_uri
    )
    assert code_lists[0]["is_external"] == True


def test_should_return_404_error(client: TestClient):
    response = client.get("/api/v1/schemas/S1234/1.0")

    assert response.status_code == 404
    assert response.json() == {
        "detail": "Could not find schema S1234V1.0.",
    }
