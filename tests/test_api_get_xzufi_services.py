from datetime import timedelta
import pytest
from fastapi.testclient import TestClient

from fimportal.common import FreigabeStatus
from fimportal.helpers import format_iso8601, utc_now
from fimportal.xzufi.common import (
    GERMAN,
    HyperlinkErweitert,
    Identifikator,
    StringLocalized,
)
from fimportal.xzufi.leistung import (
    Leistungsstruktur,
    LeistungsstrukturObjekt,
    LeistungsstrukturObjektMitVerrichtung,
    LeistungsstrukturObjektMitVerrichtungUndDetail,
    LeistungsTyp,
    LeistungsTypisierung,
    ModulTextTyp,
    RawCode,
    Textmodul,
    parse_leistung,
)
from tests.conftest import CommandRunner
from tests.data import XZUFI_2_2_0_DATA
from tests.factories.xzufi import LeistungFactory

ENDPOINT = "/api/v0/xzufi-services"


def test_return_empty_list(client: TestClient):
    response = client.get(ENDPOINT)

    assert response.status_code == 200

    assert response.json() == {
        "items": [],
        "count": 0,
        "offset": 0,
        "limit": 200,
        "total_count": 0,
    }


def test_return_existing_leistungen(runner: CommandRunner, client: TestClient):
    now = utc_now()

    LeistungFactory(
        redaktion_id="L123",
        leistung_id="leistung_1",
        bezeichnung="Leistung 1",
        bezeichnung_2="LB2",
        kurztext="Kurztext",
        volltext="Volltext",
        rechtsgrundlagen="Rechtsgrundlagen",
        freigabestatus_katalog=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        freigabestatus_bibliothek=FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER,
    ).add_typisierung(LeistungsTypisierung.TYP_1).add_leistungsschluessel(
        "99123400000000"
    ).with_versionsinformation(
        erstellt_datum_zeit=now,
        geaendert_datum_zeit=now,
    ).save_pvog(runner)

    response = client.get(ENDPOINT)

    assert response.status_code == 200
    assert response.json() == {
        "items": [
            {
                "redaktion_id": "L123",
                "id": "leistung_1",
                "title": "Leistung 1",
                "leistungsbezeichnung": "Leistung 1",
                "leistungsbezeichnung_2": "LB2",
                "freigabestatus_katalog": FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value,
                "freigabestatus_bibliothek": FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER.value,
                "hierarchy_type": "beschreibung",
                "root_for_leistungsschluessel": None,
                "leistungstyp": None,
                "typisierung": [LeistungsTypisierung.TYP_1.value],
                "leistungsschluessel": ["99123400000000"],
                "kurztext": "Kurztext",
                "volltext": "Volltext",
                "rechtsgrundlagen": "Rechtsgrundlagen",
                "erstellt_datum_zeit": format_iso8601(now),
                "geaendert_datum_zeit": format_iso8601(now),
                "fts_match": None,
            }
        ],
        "count": 1,
        "offset": 0,
        "limit": 200,
        "total_count": 1,
    }


def test_return_ignore_steckbriefe(runner: CommandRunner, client: TestClient):
    LeistungFactory.steckbrief().save_leika(runner)

    response = client.get(ENDPOINT)

    assert response.status_code == 200
    assert response.json() == {
        "items": [],
        "count": 0,
        "offset": 0,
        "limit": 200,
        "total_count": 0,
    }


@pytest.mark.parametrize(
    "term",
    [
        ("Ausweis"),
        ("ausweis"),
        ("aus"),
        ("weis"),
        ("uswei"),
    ],
)
def test_get_leistung_by_title(runner: CommandRunner, client: TestClient, term: str):
    leistung = LeistungFactory(bezeichnung="Ausweis").save_pvog(runner)
    LeistungFactory(bezeichnung="Test").save_pvog(runner)

    response = client.get(f"{ENDPOINT}?title={term}")

    assert response.status_code == 200

    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["id"] == leistung.id


@pytest.mark.parametrize(
    "fts",
    [
        ("9915", ["99154033000000"]),
        ("Informationen", ["Informationen"]),
        ("vaccination", ["vaccination"]),
        (
            "allgemeine zugangsrechte",
            ["Allgemeine", "Zugangsrechte"],
        ),  # multiple searchterms are separated and connected with an and-relation
        ("https://leika-schul.zfinder", ["leika-schul.zfinder.de"]),
    ],
)
def test_get_leistung_by_fts_query(
    runner: CommandRunner, client: TestClient, fts: tuple[str, list[str]]
):
    fts_query, fts_matches = fts
    with open(XZUFI_2_2_0_DATA / "leistung_with_content.xml", "rb") as file:
        data = file.read()

    leistung = parse_leistung(data)
    runner.import_pvog(leistung, xml_content=data.decode("utf-8"))

    response = client.get(f"{ENDPOINT}?fts_query={fts_query}")

    assert response.status_code == 200

    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["id"] == leistung.id.value
    for match in fts_matches:
        assert match in result["items"][0]["fts_match"]


def test_leistung_by_fts_query_leistungsbezeichnung(
    runner: CommandRunner, client: TestClient
):
    leistung = LeistungFactory(bezeichnung="Match").save_pvog(runner)
    LeistungFactory(bezeichnung="Random").save_pvog(runner)

    response = client.get(
        f"{ENDPOINT}?fts_query=match&suche_nur_in=leistungsbezeichnung"
    )

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["id"] == leistung.id


def test_leistung_by_fts_query_leistungsbezeichnung_2(
    runner: CommandRunner, client: TestClient
):
    leistung = LeistungFactory(bezeichnung_2="Match").save_pvog(runner)
    LeistungFactory(bezeichnung_2="Random").save_pvog(runner)

    response = client.get(
        f"{ENDPOINT}?fts_query=match&suche_nur_in=leistungsbezeichnung_II"
    )

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["id"] == leistung.id


def test_leistung_by_fts_query_rechtsgrundlagen(
    runner: CommandRunner, client: TestClient
):
    leistung = LeistungFactory(rechtsgrundlagen="Match").save_pvog(runner)
    LeistungFactory(rechtsgrundlagen="Random").save_pvog(runner)

    response = client.get(f"{ENDPOINT}?fts_query=match&suche_nur_in=rechtsgrundlagen")

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["id"] == leistung.id


@pytest.mark.parametrize(
    "search_term",
    [
        ("12345"),
        ("678"),
        ("12"),
        ("5000000000"),
    ],
)
def test_get_leistung_by_leistungsschluessel(
    runner: CommandRunner, client: TestClient, search_term: str
):
    leistung = (
        LeistungFactory()
        .add_leistungsschluessel("12345000000000")
        .add_leistungsschluessel("99678000000000")
        .save_pvog(runner)
    )
    LeistungFactory().add_leistungsschluessel("99765432100000").save_pvog(runner)

    response = client.get(f"{ENDPOINT}?leistungsschluessel={search_term}")
    assert response.status_code == 200

    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["id"] == leistung.id


@pytest.mark.parametrize(
    "rechtsgrundlagen",
    [
        ("rechts"),
        ("some"),
        ("GRUND"),
        ("e rec"),
        ("Title"),
        ("Beschreibung"),
    ],
)
def test_get_leistung_by_rechtsgrundlagen(
    runner: CommandRunner, client: TestClient, rechtsgrundlagen: str
):
    leistung = LeistungFactory(
        rechtsgrundlagen=Textmodul(
            position_darstellung=0,
            id=None,
            id_sekundaer=[],
            gueltigkeit=[],
            leika_textmodul=ModulTextTyp.RECHTSGRUNDLAGEN,
            leika_textmodul_abweichende_bezeichnung=[],
            inhalt=[StringLocalized(GERMAN, "some rechtsgrundlagen")],
            weiterfuehrender_link=[
                HyperlinkErweitert(
                    position_darstellung=None,
                    language_code=None,
                    uri="test",
                    titel="Title",
                    beschreibung="Beschreibung",
                )
            ],
        )
    ).save_pvog(runner)
    LeistungFactory(rechtsgrundlagen="another one").save_pvog(runner)

    response = client.get(f"{ENDPOINT}?rechtsgrundlagen={rechtsgrundlagen}")

    assert response.status_code == 200

    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["id"] == leistung.id


@pytest.mark.parametrize(
    "leistungsbezeichnung",
    [
        ("bezeichnung"),
        ("Leistung"),
        ("EISTUNG"),
    ],
)
def test_get_leistung_by_lb1(
    runner: CommandRunner, client: TestClient, leistungsbezeichnung: str
):
    leistung = LeistungFactory(bezeichnung="Leistungsbezeichnung").save_pvog(runner)

    LeistungFactory(bezeichnung="another one").save_pvog(runner)

    # First, make sure both leistungen are imported
    response = client.get(f"{ENDPOINT}")
    assert response.status_code == 200
    assert response.json()["count"] == 2

    response = client.get(f"{ENDPOINT}?leistungsbezeichnung={leistungsbezeichnung}")
    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["id"] == leistung.id


@pytest.mark.parametrize(
    "leistungsbezeichnung_II",
    [
        ("bezeichnung II"),
        ("Leistung"),
        ("EISTUNG"),
    ],
)
def test_get_leistung_by_lb2(
    runner: CommandRunner, client: TestClient, leistungsbezeichnung_II: str
):
    leistung = LeistungFactory(bezeichnung_2="Leistungsbezeichnung II").save_pvog(
        runner
    )

    LeistungFactory(bezeichnung_2="another one").save_pvog(runner)

    # First, make sure both leistungen are imported
    response = client.get(f"{ENDPOINT}")
    assert response.status_code == 200
    assert response.json()["count"] == 2

    response = client.get(f"{ENDPOINT}?leistungsbezeichnung2={leistungsbezeichnung_II}")
    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["id"] == leistung.id


@pytest.mark.parametrize(
    "typisierung",
    [("1"), ("2/3a")],
)
def test_get_leistung_by_typisierung(
    runner: CommandRunner, client: TestClient, typisierung: str
):
    leistung = (
        LeistungFactory()
        .add_typisierung(LeistungsTypisierung.TYP_1)
        .add_typisierung(LeistungsTypisierung.TYP_2_3A)
        .save_pvog(runner)
    )
    LeistungFactory().add_typisierung(LeistungsTypisierung.TYP_7).save_pvog(runner)

    response = client.get(f"{ENDPOINT}?typisierung={typisierung}")

    assert response.status_code == 200

    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["id"] == leistung.id


def test_get_leistung_by_redaktion_id(runner: CommandRunner, client: TestClient):
    leistung = LeistungFactory(redaktion_id="redaktion_a").save_pvog(runner)
    LeistungFactory(redaktion_id="redaktion_b").save_pvog(runner)

    response = client.get(f"{ENDPOINT}?redaktion_id=redaktion_a")

    assert response.status_code == 200

    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["id"] == leistung.id


@pytest.mark.parametrize(
    "struktur,leistungstyp",
    [
        (LeistungsstrukturObjekt(None), LeistungsTyp.LEISTUNGS_OBJEKT),
        (
            LeistungsstrukturObjektMitVerrichtung(
                None,
                Identifikator(
                    scheme_agency_id=None,
                    scheme_agency_name=None,
                    scheme_data_uri=None,
                    scheme_id=None,
                    scheme_name=None,
                    scheme_uri=None,
                    scheme_version_id=None,
                    value="L1234",
                ),
                RawCode("", "", ""),
            ),
            LeistungsTyp.LEISTUNGS_OBJEKT_MIT_VERRICHTUNG,
        ),
        (
            LeistungsstrukturObjektMitVerrichtungUndDetail(
                None,
                Identifikator(
                    scheme_agency_id=None,
                    scheme_agency_name=None,
                    scheme_data_uri=None,
                    scheme_id=None,
                    scheme_name=None,
                    scheme_uri=None,
                    scheme_version_id=None,
                    value="L1234",
                ),
                RawCode("", "", ""),
                [StringLocalized("GERMAN", "SOME CONTENT")],
            ),
            LeistungsTyp.LEISTUNGS_OBJEKT_MIT_VERRICHTUNG_UND_DETAIL,
        ),
    ],
)
def test_get_leistung_by_leistungstyp(
    runner: CommandRunner,
    client: TestClient,
    struktur: Leistungsstruktur,
    leistungstyp: LeistungsTyp,
):
    leistung = LeistungFactory(struktur=struktur).save_pvog(runner)
    LeistungFactory(struktur=None).save_pvog(runner)

    # First, make sure both leistungen are imported
    response = client.get(ENDPOINT)
    assert response.status_code == 200
    assert response.json()["count"] == 2

    response = client.get(f"{ENDPOINT}?leistungstyp={leistungstyp.value}")

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["id"] == leistung.id


def test_get_leistung_by_updated_since(runner: CommandRunner, client: TestClient):
    now = utc_now()

    leistung = (
        LeistungFactory()
        .with_versionsinformation(geaendert_datum_zeit=utc_now())
        .save_pvog(runner)
    )

    LeistungFactory().with_versionsinformation(
        geaendert_datum_zeit=now - timedelta(days=1)
    ).save_pvog(runner)

    LeistungFactory().with_versionsinformation(geaendert_datum_zeit=None).save_pvog(
        runner
    )

    response = client.get(f"{ENDPOINT}?updated_since={format_iso8601(now)}")

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["id"] == leistung.id


def test_order_results_by_erstellt_datum_zeit(
    runner: CommandRunner, client: TestClient
):
    now = utc_now()

    leistung_first = (
        LeistungFactory()
        .with_versionsinformation(erstellt_datum_zeit=now - timedelta(days=1))
        .save_pvog(runner)
    )
    leistung_second = (
        LeistungFactory()
        .with_versionsinformation(erstellt_datum_zeit=now)
        .save_pvog(runner)
    )
    leistung_unknown = (
        LeistungFactory()
        .with_versionsinformation(erstellt_datum_zeit=None)
        .save_pvog(runner)
    )

    # ascending order
    response = client.get(f"{ENDPOINT}?order_by=erstellt_datum_zeit_asc")
    assert response.status_code == 200
    results = response.json()
    assert results["items"][0]["id"] == leistung_first.id
    assert results["items"][1]["id"] == leistung_second.id
    assert results["items"][2]["id"] == leistung_unknown.id

    # descending order
    response = client.get(f"{ENDPOINT}?order_by=erstellt_datum_zeit_desc")
    assert response.status_code == 200
    results = response.json()
    assert results["items"][0]["id"] == leistung_second.id
    assert results["items"][1]["id"] == leistung_first.id
    assert results["items"][2]["id"] == leistung_unknown.id


def test_order_results_by_relevance(runner: CommandRunner, client: TestClient):
    leistung_second = LeistungFactory(
        bezeichnung="A", bezeichnung_2="Personalausweis"
    ).save_pvog(runner)
    leistung_first = LeistungFactory(bezeichnung="Personalausweis").save_pvog(runner)
    leistung_third = LeistungFactory(
        bezeichnung="B", bezeichnung_2="Personalausweis"
    ).save_pvog(runner)

    response = client.get(f"{ENDPOINT}?order_by=relevance&fts_query=personalausweis")
    assert response.status_code == 200
    results = response.json()
    assert results["items"][0]["id"] == leistung_first.id
    assert results["items"][1]["id"] == leistung_second.id
    assert results["items"][2]["id"] == leistung_third.id
