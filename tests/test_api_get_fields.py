from datetime import date, timedelta

import pytest
from fastapi.testclient import TestClient
from freezegun import freeze_time

from fimportal.helpers import format_iso8601
from fimportal.xdatenfelder import xdf2
from fimportal.xdatenfelder.xdf3 import Datentyp, Feldart
from tests.conftest import CommandRunner
from tests.factories.xdf2 import Xdf2Factory
from tests.factories.xdf3 import Xdf3Factory

today = date.today()


def test_should_correctly_return_a_field(
    runner: CommandRunner,
    client: TestClient,
):
    today = date.today()

    factory = Xdf2Factory()
    field = factory.field(
        bezug="Bezug",
        status=xdf2.Status.AKTIV,
        gueltig_ab=today,
        gueltig_bis=today,
        freigabedatum=today,
        veroeffentlichungsdatum=today,
    ).build()
    xdf2_import = factory.schema().with_field(field).build_import()

    with freeze_time("2023-10-17T00:00:00"):
        runner.import_xdf2(xdf2_import)

    response = client.get("/api/v1/fields")

    assert response.status_code == 200
    assert response.json() == {
        "offset": 0,
        "count": 1,
        "total_count": 1,
        "items": [
            {
                "namespace": "baukasten",
                "fim_id": field.identifier.id,
                "fim_version": field.identifier.version,
                "nummernkreis": field.identifier.get_xdf3_nummernkreis(),
                "name": field.name,
                "beschreibung": field.beschreibung,
                "definition": field.definition,
                "freigabe_status": 6,
                "freigabe_status_label": "fachlich freigegeben (gold)",
                "gueltig_ab": today.isoformat(),
                "gueltig_bis": today.isoformat(),
                "bezug": ["Bezug"],
                "feldart": field.feldart.value,
                "datentyp": field.datentyp.value,
                "letzte_aenderung": format_iso8601(
                    xdf2_import.schema_message.header.erstellungs_zeitpunkt
                ),
                "last_update": "2023-10-17T00:00:00Z",
                "versionshinweis": field.versionshinweis,
                "status_gesetzt_durch": field.fachlicher_ersteller,
                "status_gesetzt_am": today.isoformat(),
                "veroeffentlichungsdatum": today.isoformat(),
                "xdf_version": "2.0",
                "fts_match": None,
                "is_latest": True,
            }
        ],
        "limit": 200,
    }


@pytest.mark.parametrize(
    "query_parameter", ["some fie", "Some Field Name", "name", "NAME", "ield", "D n"]
)
def test_get_field_by_name(
    runner: CommandRunner, client: TestClient, query_parameter: str
):
    factory = Xdf2Factory()
    factory.schema(id="S01").with_field(
        factory.field(name="Some Field Name", id="F01").build()
    ).save(runner)

    factory.schema(id="S02").with_field(
        factory.field(name="Something Different", id="F02").build()
    ).save(runner)

    response = client.get(f"/api/v1/fields?name={query_parameter}")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "F01"


def test_return_an_empty_list(client: TestClient):
    response = client.get("/api/v1/fields")

    assert response.status_code == 200

    assert response.json() == {
        "items": [],
        "count": 0,
        "offset": 0,
        "limit": 200,
        "total_count": 0,
    }


def test_field_pagination_custom_limit(runner: CommandRunner, client: TestClient):
    factory = Xdf2Factory()
    for _ in range(5):
        factory.schema().with_field().save(runner)

    response = client.get("/api/v1/fields?limit=5")

    assert response.status_code == 200

    result = response.json()
    assert result["count"] == 5
    assert result["offset"] == 0
    assert result["limit"] == 5
    assert result["total_count"] == 5


def test_field_pagination_custom_limit_and_offset(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    for _ in range(5):
        factory.schema().with_field().save(runner)

    response = client.get("/api/v1/fields?limit=5&offset=2")

    assert response.status_code == 200

    result = response.json()
    assert result["count"] == 3
    assert result["offset"] == 2
    assert result["limit"] == 5
    assert result["total_count"] == 5


def test_field_pagination_wrong_limit(client: TestClient):
    response = client.get("/api/v1/fields?limit=5000")

    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "input": "5000",
                "loc": ["query", "limit"],
                "msg": "Input should be less than or equal to 200",
                "type": "less_than_equal",
                "ctx": {"le": 200},
            }
        ]
    }


def test_field_pagination_high_offset(runner: CommandRunner, client: TestClient):
    factory = Xdf2Factory()
    for _ in range(5):
        factory.schema().with_field().save(runner)

    response = client.get("/api/v1/fields?offset=5&limit=5")

    assert response.status_code == 200
    assert response.json()["count"] == 0


@pytest.mark.parametrize(
    "search_term",
    ["1", "12000"],
)
def test_get_field_by_nummernkreis(
    runner: CommandRunner, client: TestClient, search_term: str
):
    factory = Xdf2Factory()
    factory.schema("S12345").with_field(factory.field("F12345").build()).save(runner)

    response = client.get(f"/api/v1/fields?nummernkreis={search_term}")

    assert response.status_code == 200
    assert response.json()["count"] == 1


@pytest.mark.parametrize(
    "search_term",
    ["2", "2000"],
)
def test_should_not_return_field_by_wrong_nummernkreis(
    runner: CommandRunner, client: TestClient, search_term: str
):
    factory = Xdf2Factory()
    factory.schema("S12345").with_field(factory.field("F12345").build()).save(runner)

    response = client.get(f"/api/v1/fields?nummernkreis={search_term}")

    assert response.status_code == 200
    assert response.json()["count"] == 0


def test_get_field_by_author(runner: CommandRunner, client: TestClient):
    factory = Xdf2Factory()
    factory.schema().with_field(
        factory.field(fachlicher_ersteller="Ersteller").build()
    ).save(runner)

    response = client.get("/api/v1/fields?status_gesetzt_durch=Ersteller")

    assert response.status_code == 200
    assert response.json()["count"] == 1


def test_should_not_return_field_for_wrong_author(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    factory.schema().with_field(
        factory.field(fachlicher_ersteller="Ersteller").build()
    ).save(runner)

    response = client.get("/api/v1/fields?status_gesetzt_durch=Wrong")

    assert response.status_code == 200
    assert response.json()["count"] == 0


@pytest.mark.parametrize(
    "gueltig_ab, gueltig_bis",
    [
        (None, None),
        (today, today + timedelta(days=1)),
        (None, today),
        (today, None),
    ],
)
def test_get_field_by_gueltig_am(
    runner: CommandRunner,
    client: TestClient,
    gueltig_ab: date | None,
    gueltig_bis: date | None,
):
    gueltig_am = today.isoformat()

    factory = Xdf2Factory()
    factory.schema().with_field(
        factory.field(gueltig_ab=gueltig_ab, gueltig_bis=gueltig_bis, id="F001").build()
    ).save(runner)
    factory.schema().with_field(
        factory.field(
            gueltig_ab=today + timedelta(days=1),
            gueltig_bis=today + timedelta(days=2),
            id="F002",
        ).build()
    ).save(runner)

    response = client.get(f"/api/v1/fields?gueltig_am={gueltig_am}")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "F001"


@pytest.mark.parametrize("search_term", ["Lorem", "Lor", "lor", "em", "ore", "loREM"])
def test_get_field_by_different_bezug(
    runner: CommandRunner, client: TestClient, search_term: str
):
    factory = Xdf2Factory()
    factory.schema(id="S1234", version="1.1").with_field(
        factory.field(id="F1234", version="1.1", bezug="Lorem").build()
    ).save(runner)
    factory.schema(id="S1234", version="1.0").with_field(
        factory.field(id="F1234", version="1.0", bezug="Ipsum").build()
    ).save(runner)

    response = client.get(f"/api/v1/fields?bezug={search_term}")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_version"] == "1.1"


def test_should_return_fields_by_updated_since(
    runner: CommandRunner, client: TestClient
):
    early_date = "2023-10-17T00:00:00"
    late_date = "2023-10-18T00:00:00"

    factory = Xdf2Factory()

    with freeze_time(early_date):
        factory.schema("D123").with_field(factory.field("D123").build()).save(runner)

    with freeze_time(late_date):
        factory.schema("D234").with_field(factory.field("D234").build()).save(runner)

    response = client.get(f"/api/v1/fields?updated_since={late_date}")
    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "D234"


def test_should_return_field_filtered_by_status_gesetzt_seit(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    factory.schema().with_field(
        factory.field(freigabedatum=date(2020, 11, 4), id="F001").build()
    ).save(runner)

    factory.schema().with_field(
        factory.field(freigabedatum=date(2021, 11, 4), id="F002").build()
    ).save(runner)

    response = client.get(f"/api/v1/fields?status_gesetzt_seit={date(2021, 1, 1)}")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "F002"


def test_should_return_field_filtered_by_status_gesetzt_bis(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    factory.schema().with_field(
        factory.field(freigabedatum=date(2020, 11, 4), id="F001").build()
    ).save(runner)
    factory.schema().with_field(
        factory.field(freigabedatum=date(2021, 11, 4), id="F002").build()
    ).save(runner)

    response = client.get(f"/api/v1/fields?status_gesetzt_bis={date(2021, 1, 1)}")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "F001"


def test_should_return_field_filtered_by_status_gesetzt_am(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    factory.schema().with_field(
        factory.field(freigabedatum=date(2020, 11, 4), id="F001").build()
    ).save(runner)
    factory.schema().with_field(
        factory.field(freigabedatum=date(2021, 11, 4), id="F002").build()
    ).save(runner)
    factory.schema().with_field(
        factory.field(freigabedatum=date(2022, 11, 4), id="F003").build()
    ).save(runner)

    response = client.get(
        f"/api/v1/fields?status_gesetzt_seit={date(2021, 1, 1)}&status_gesetzt_bis={date(2022, 1, 1)}"
    )

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "F002"


def test_should_return_field_filtered_by_xdf_version(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    factory.schema().with_field(factory.field(id="F001").build()).save(runner)

    factory = Xdf3Factory()
    factory.schema().with_field(factory.field(id="F000002").build()).save(runner)

    response = client.get("/api/v1/fields?xdf_version=3.0.0")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "F000002"


def test_should_return_field_filtered_by_feldart(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf3Factory()
    factory.schema().with_field(
        factory.field(feldart=Feldart.EINGABE, id="F000001").build()
    ).save(runner)
    factory.schema().with_field(
        factory.field(feldart=Feldart.STATISCH, id="F000002").build()
    ).save(runner)

    response = client.get("/api/v1/fields?feldart=input")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "F000001"


def test_should_return_field_filtered_by_datentyp(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf3Factory()
    factory.schema().with_field(
        factory.field(datentyp=Datentyp.DATUM, id="F000001").build()
    ).save(runner)
    factory.schema().with_field(
        factory.field(datentyp=Datentyp.GANZZAHL, id="F000002").build()
    ).save(runner)

    response = client.get("/api/v1/fields?datentyp=date")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "F000001"


def test_should_not_return_field_filtered_by_versionshinweis(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    factory.schema().with_field(
        factory.field(versionshinweis="something").build()
    ).save(runner)

    response = client.get("/api/v1/fields?Versionshinweis=different")

    assert response.status_code == 200
    assert response.json()["count"] == 0


def test_should_not_return_field_filtered_by_versionshinweis_and_gueltig_am_filter(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    factory.schema().with_field(
        factory.field(versionshinweis="something").build()
    ).save(runner)

    response = client.get(
        "/api/v1/fields?Versionshinweis=different&gueltig_am=2024-06-26"
    )

    assert response.status_code == 200
    assert response.json()["count"] == 0


@pytest.mark.parametrize(
    "fts",
    [
        ("alternativ", ["Alternativer"]),
        ("Person", ["Person"]),
        (
            "Person Annahme",
            ["Annahme", "Person"],
        ),  # multiple searchterms are separated and connected with an and-relation
        ("F00001", ["F00001234"]),
    ],
)
def test_should_filter_by_fts_query(
    runner: CommandRunner, client: TestClient, fts: tuple[str, list[str]]
):
    factory = Xdf2Factory()
    fts_query, fts_matches = fts
    field_id = "F00001234"
    (
        factory.schema()
        .with_field(
            factory.field(
                id=field_id,
                name="Alternativer Empfänger",
                beschreibung="Hier können Sie einen weitere Person zur Annahme angeben.",
            ).build()
        )
        .save(runner)
    )
    factory.schema().with_field(factory.field(id="F00005678").build()).save(runner)

    response = client.get(f"/api/v1/fields?fts_query={fts_query}")

    assert response.status_code == 200
    assert response.json()["count"] == 1

    assert response.json()["items"][0]["fim_id"] == field_id
    for match in fts_matches:
        assert f"[[[{match}]]]" in response.json()["items"][0]["fts_match"]


def test_should_filter_by_fts_query_handlungsgrundlage(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    schema = (
        factory.schema().with_field(factory.field(bezug="Match").build()).save(runner)
    )
    factory.schema().with_field(factory.field(bezug="random").build()).save(runner)

    response = client.get(
        "/api/v1/fields?fts_query=match&suche_nur_in=Rechtsgrundlagen"
    )

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["fim_id"] == schema.children[0].fim_id


def test_should_filter_by_fts_query_status_gestetzt_durch(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    schema = (
        factory.schema()
        .with_field(factory.field(fachlicher_ersteller="Match").build())
        .save(runner)
    )
    factory.schema().with_field(
        factory.field(fachlicher_ersteller="random").build()
    ).save(runner)

    response = client.get(
        "/api/v1/fields?fts_query=match&suche_nur_in=Status_gesetzt_durch"
    )

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["fim_id"] == schema.children[0].fim_id


def test_should_filter_by_fts_query_versionshinweis(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    schema = (
        factory.schema()
        .with_field(factory.field(versionshinweis="Match").build())
        .save(runner)
    )
    factory.schema().with_field(factory.field(versionshinweis="random").build()).save(
        runner
    )

    response = client.get("/api/v1/fields?fts_query=match&suche_nur_in=Versionshinweis")

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["fim_id"] == schema.children[0].fim_id


def test_should_get_only_latest_field_when_is_latest_is_true(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf3Factory()
    field_1 = factory.field(id="F12000", version="1.0.1").build()
    field_2 = factory.field(id="F12000", version="1.0.2").build()
    (factory.schema(id="S120001").with_field(field_1).with_field(field_2).save(runner))

    response = client.get("/api/v1/fields?is_latest=true")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_version"] == "1.0.2"


def test_should_get_only_older_fields_when_is_latest_is_false(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf3Factory()
    field_1 = factory.field(id="F12000", version="1.0.1").build()
    field_2 = factory.field(id="F12000", version="1.0.2").build()
    field_3 = factory.field(id="F12000", version="1.0.3").build()
    (
        factory.schema(id="S120001")
        .with_field(field_1)
        .with_field(field_2)
        .with_field(field_3)
        .save(runner)
    )

    response = client.get("/api/v1/fields?is_latest=false")

    assert response.status_code == 200
    assert response.json()["count"] == 2
    assert response.json()["items"][0]["fim_version"] in ("1.0.1", "1.0.2")
    assert response.json()["items"][1]["fim_version"] in ("1.0.1", "1.0.2")
