from datetime import date

from fastapi.testclient import TestClient
from freezegun import freeze_time

from fimportal import genericode
from fimportal.genericode.code_list.serialize import serialize_code_list
from fimportal.xdatenfelder import xdf2, xdf3
from fimportal.xrepository import FakeClient as XRepoTestClient
from tests.conftest import CommandRunner
from tests.data import XDF2_DATA
from tests.factories.xdf2 import Xdf2Factory


def test_should_fail_for_empty_access_token(client: TestClient):
    with open(XDF2_DATA / "schema_without_code_lists.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf2",
            data={
                "steckbrief_id": "D1234",
                "freigabe_status": str(
                    xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                ),
            },
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 401
    assert response.json() == {"detail": "invalid access token"}


def test_should_fail_for_invalid_access_token(client: TestClient):
    with open(XDF2_DATA / "schema_without_code_lists.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf2",
            data={
                "steckbrief_id": "D1234",
                "freigabe_status": str(
                    xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                ),
            },
            files=[
                ("schema", schema),
            ],
            headers={"Access-Token": "wrong_access_token"},
        )

    assert response.status_code == 401
    assert response.json() == {"detail": "invalid access token"}


def test_should_fail_for_invalid_token_scope(runner: CommandRunner, client: TestClient):
    access_token = runner.create_access_token("99000")

    with open(XDF2_DATA / "schema_without_code_lists.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf2",
            data={
                "steckbrief_id": "D1234",
                "freigabe_status": str(
                    xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                ),
            },
            files=[
                ("schema", schema),
            ],
            headers={"Access-Token": access_token},
        )

    assert response.status_code == 403
    assert response.json() == {
        "detail": "missing authorization: token can only upload for Nummernkreis 99000, the schema is for Nummernkreis 12000."
    }


def test_should_work_for_unknown_element_from_another_nummernkreis(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    schema_import = factory.schema(id="S12000").with_field().build_import()

    access_token = runner.create_access_token("12000")

    response = client.post(
        "/api/v1/schemas/xdf2",
        data={
            "steckbrief_id": "D1234",
            "freigabe_status": str(xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value),
        },
        files=[
            ("schema", schema_import.schema_text),
        ],
        headers={"Access-Token": access_token},
    )

    assert response.status_code == 200


def test_should_upload_the_schema(runner: CommandRunner, client: TestClient):
    access_token = runner.create_access_token("12000")

    with open(XDF2_DATA / "schema_without_code_lists.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf2",
            data={
                "steckbrief_id": "D1234",
                "freigabe_status": str(
                    xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                ),
            },
            files=[
                ("schema", schema),
            ],
            headers={"Access-Token": access_token},
        )

    assert response.status_code == 200

    response = client.get("/api/v1/schemas/S1234/1.0")
    assert response.status_code == 200


def test_should_return_the_schema_metadata(
    immutable_base_url: str, runner: CommandRunner, client: TestClient
):
    access_token = runner.create_access_token("12000")

    with freeze_time("2023-10-17T00:00:00"):
        with open(XDF2_DATA / "schema_without_code_lists.xml", "rb") as schema:
            response = client.post(
                "/api/v1/schemas/xdf2",
                data={
                    "steckbrief_id": "D1234",
                    "freigabe_status": str(
                        xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                    ),
                },
                files=[
                    ("schema", schema),
                ],
                headers={"Access-Token": access_token},
            )

    assert response.status_code == 200
    assert response.json() == {
        "fim_id": "S1234",
        "fim_version": "1.0",
        "nummernkreis": "12000",
        "xdf_version": "2.0",
        "name": "Test",
        "beschreibung": "Eine Beschreibung",
        "definition": "Eine Definition",
        "freigabe_status": 6,
        "freigabe_status_label": "fachlich freigegeben (gold)",
        "gueltig_ab": None,
        "gueltig_bis": None,
        "status_gesetzt_am": "2023-01-01",
        "bezug": ["Bezug"],
        "status_gesetzt_durch": "Test",
        "versionshinweis": "Ein Versionshinweis",
        "veroeffentlichungsdatum": "2023-01-01",
        "steckbrief_id": "D1234",
        "steckbrief_name": None,
        "stichwort": [],
        "bezeichnung": "Test",
        "letzte_aenderung": "2020-09-01T00:00:00Z",
        "last_update": "2023-10-17T00:00:00Z",
        "is_latest": True,
        "datenfelder": [
            {
                "namespace": "baukasten",
                "fim_id": "F12001",
                "fim_version": "1.0",
                "nummernkreis": "12000",
                "name": "Hinweis",
                "beschreibung": None,
                "definition": None,
                "gueltig_ab": None,
                "gueltig_bis": None,
                "bezug": [],
                "freigabe_status": 2,
                "freigabe_status_label": "in Bearbeitung",
                "letzte_aenderung": "2020-09-01T00:00:00Z",
                "last_update": "2023-10-17T00:00:00Z",
                "veroeffentlichungsdatum": None,
                "status_gesetzt_durch": "Creator",
                "status_gesetzt_am": None,
                "versionshinweis": None,
                "xdf_version": "2.0",
                "feldart": "input",
                "datentyp": "text",
                "fts_match": None,
                "is_latest": True,
            }
        ],
        "datenfeldgruppen": [],
        "children": [
            {
                "namespace": "baukasten",
                "fim_id": "F12001",
                "fim_version": "1.0",
                "type": "Feld",
                "anzahl": "1:1",
                "bezug": [],
            }
        ],
        "regeln": [],
        "relation": [],
        "uploads": [
            {
                "filename": "S1234V1.0_2023-10-17-1697500800000.xdf2.xml",
                "url": f"{immutable_base_url}/immutable/schemas/S1234V1.0_2023-10-17-1697500800000.xdf2.xml",
                "upload_time": "2023-10-17T00:00:00Z",
                "code_lists": [],
            }
        ],
        "json_schema_files": [
            {
                "filename": "S1234V1.0_2023-10-17-1697500800000.schema.json",
                "url": f"{immutable_base_url}/immutable/schemas/S1234V1.0_2023-10-17-1697500800000.schema.json",
                "upload_time": "2023-10-17T00:00:00Z",
                "generated_from": "S1234V1.0_2023-10-17-1697500800000.xdf2.xml",
            }
        ],
        "xsd_files": [
            {
                "filename": "S1234V1.0_2023-10-17-1697500800000.xsd",
                "url": f"{immutable_base_url}/immutable/schemas/S1234V1.0_2023-10-17-1697500800000.xsd",
                "upload_time": "2023-10-17T00:00:00Z",
                "generated_from": "S1234V1.0_2023-10-17-1697500800000.xdf2.xml",
            }
        ],
    }


def test_should_also_import_code_lists(runner: CommandRunner, client: TestClient):
    access_token = runner.create_access_token("12000")

    with (
        open(XDF2_DATA / "schema.xml", "rb") as schema,
        open(XDF2_DATA / "code_list_a.xml", "rb") as code_list_a,
        open(XDF2_DATA / "code_list_b.xml", "rb") as code_list_b,
    ):
        response = client.post(
            "/api/v1/schemas/xdf2",
            data={
                "steckbrief_id": "D1234",
                "freigabe_status": str(
                    xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                ),
            },
            files=[
                ("schema", schema),
                ("code_lists", code_list_a),
                ("code_lists", code_list_b),
            ],
            headers={"Access-Token": access_token},
        )

    assert response.status_code == 200
    assert len(response.json()["uploads"][0]["code_lists"]) == 2


def test_should_mark_missing_code_lists_as_external(
    runner: CommandRunner, client: TestClient
):
    access_token = runner.create_access_token("12000")

    with open(XDF2_DATA / "schema.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf2",
            data={
                "steckbrief_id": "D1234",
                "freigabe_status": str(
                    xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                ),
            },
            files=[
                ("schema", schema),
            ],
            headers={"Access-Token": access_token},
        )

    assert response.status_code == 200

    code_lists = response.json()["uploads"][0]["code_lists"]
    assert len(code_lists) == 2
    assert code_lists[0]["is_external"] == True
    assert code_lists[1]["is_external"] == True


def test_should_import_external_codelist_content(
    runner: CommandRunner, client: TestClient, xrepository_client: XRepoTestClient
):
    access_token = runner.create_access_token("12000")
    canonical_version_uri = "urn:de:code_list_a_1"
    retrievable_code_list = genericode.CodeList(
        identifier=genericode.Identifier(
            canonical_version_uri=canonical_version_uri,
            version="1",
            canonical_uri="urn:de:code_list_a",
        ),
        default_code_key="code",
        default_name_key="name",
        columns={"code": ["0", "1"], "name": ["Name0", "Name1"]},
    )
    xrepository_client.register_code_list(retrievable_code_list)
    with open(XDF2_DATA / "schema.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf2",
            data={
                "steckbrief_id": "D1234",
                "freigabe_status": str(
                    xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                ),
            },
            files=[
                ("schema", schema),
            ],
            headers={"Access-Token": access_token},
        )

    assert response.status_code == 200

    code_lists = response.json()["uploads"][0]["code_lists"]
    assert len(code_lists) == 2
    assert code_lists[0]["is_external"] == True
    assert code_lists[1]["is_external"] == True
    code_list_with_content = [
        code_list
        for code_list in code_lists
        if code_list["genericode_canonical_version_uri"] == canonical_version_uri
    ][0]
    code_list_response = client.get(
        f"/immutable/code-lists/{code_list_with_content['id']}/genericode.xml"
    )
    assert code_list_response.text == serialize_code_list(retrievable_code_list).decode(
        "utf-8"
    )


def test_should_fail_for_invalid_schema(runner: CommandRunner, client: TestClient):
    access_token = runner.create_access_token("12000")

    with open(XDF2_DATA / "code_list_a.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf2",
            data={
                "steckbrief_id": "D1234",
                "freigabe_status": str(
                    xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                ),
            },
            files=[
                ("schema", schema),
            ],
            headers={"Access-Token": access_token},
        )

    assert response.status_code == 400
    assert response.json() == {
        "detail": "Could not parse schema: Unexpected child node [tag={http://docs.oasis-open.org/codelist/ns/genericode/1.0/}CodeList], line 2"
    }


def test_should_fail_for_invalid_code_list(runner: CommandRunner, client: TestClient):
    access_token = runner.create_access_token("12000")

    with (
        open(XDF2_DATA / "schema.xml", "rb") as schema,
        open(XDF2_DATA / "schema.xml", "rb") as invalid_code_list,
    ):
        response = client.post(
            "/api/v1/schemas/xdf2",
            data={
                "steckbrief_id": "D1234",
                "freigabe_status": str(
                    xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                ),
            },
            files=[
                ("schema", schema),
                ("code_lists", invalid_code_list),
            ],
            headers={"Access-Token": access_token},
        )

    assert response.status_code == 400
    assert response.json() == {
        "detail": "Could not parse code list: Unexpected child node [tag={urn:xoev-de:fim:standard:xdatenfelder_2}xdatenfelder.stammdatenschema.0102], line 2"
    }


def test_should_ignore_empty_code_list_entry(runner: CommandRunner, client: TestClient):
    """
    If no code list is selected in an HTML form, Firefox sends one item
    with the empty byte slice (`b''`) instead of nothing for the code list array.
    """
    access_token = runner.create_access_token("12000")

    with open(XDF2_DATA / "schema_without_code_lists.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf2",
            data={
                "steckbrief_id": "D1234",
                "freigabe_status": str(
                    xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                ),
            },
            files=[
                ("schema", schema),
                ("code_lists", b""),
            ],
            headers={"Access-Token": access_token},
        )

    assert response.status_code == 200


def test_should_fail_for_missing_steckbrief_id(
    runner: CommandRunner, client: TestClient
):
    access_token = runner.create_access_token("12000")

    with open(XDF2_DATA / "schema.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf2",
            data={
                "freigabe_status": str(
                    xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                ),
            },
            files=[
                ("schema", schema),
            ],
            headers={"Access-Token": access_token},
        )

    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "input": None,
                "loc": ["body", "steckbrief_id"],
                "msg": "Field required",
                "type": "missing",
            }
        ]
    }


def test_should_fail_for_missing_freigabe_status(
    runner: CommandRunner, client: TestClient
):
    access_token = runner.create_access_token("12000")

    with open(XDF2_DATA / "schema.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf2",
            data={
                "steckbrief_id": "D1234",
            },
            files=[
                ("schema", schema),
            ],
            headers={"Access-Token": access_token},
        )

    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "input": None,
                "loc": ["body", "freigabe_status"],
                "msg": "Field required",
                "type": "missing",
            }
        ]
    }


def test_should_fail_for_invalid_freigabe_status(
    runner: CommandRunner, client: TestClient
):
    access_token = runner.create_access_token("12000")

    with open(XDF2_DATA / "schema_without_code_lists.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf2",
            data={
                "steckbrief_id": "D1234",
                "freigabe_status": str(xdf3.FreigabeStatus.IN_BEARBEITUNG.value),
            },
            files=[
                ("schema", schema),
            ],
            headers={"Access-Token": access_token},
        )

    assert response.status_code == 400
    assert response.json() == {
        "detail": "Freigabestatus not allowed for import [got=in Bearbeitung]"
    }


def test_should_work_for_re_uploading_existing_schema(
    immutable_base_url: str, runner: CommandRunner, client: TestClient
):
    access_token = runner.create_access_token("12000")
    with freeze_time("2023-10-17T00:00:00"):
        with open(XDF2_DATA / "schema_without_code_lists.xml", "r") as file:
            text = file.read()
            schema = text.replace(
                "<xdf:freigabedatum>2023-01-01</xdf:freigabedatum>",
                "<xdf:freigabedatum>2022-01-01</xdf:freigabedatum>",
            ).encode()
            assert schema != text.encode()

            client.post(
                "/api/v1/schemas/xdf2",
                data={
                    "steckbrief_id": "D1234",
                    "freigabe_status": str(
                        xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER.value
                    ),
                },
                files=[
                    ("schema", schema),
                ],
                headers={"Access-Token": access_token},
            )

    with freeze_time("2023-10-18T00:00:00"):
        with open(XDF2_DATA / "schema_without_code_lists.xml", "rb") as schema:
            response = client.post(
                "/api/v1/schemas/xdf2",
                data={
                    "steckbrief_id": "D1234",
                    "freigabe_status": str(
                        xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                    ),
                },
                files=[
                    ("schema", schema),
                ],
                headers={"Access-Token": access_token},
            )

    assert response.status_code == 200
    assert response.json() == {
        "fim_id": "S1234",
        "fim_version": "1.0",
        "nummernkreis": "12000",
        "xdf_version": "2.0",
        "name": "Test",
        "beschreibung": "Eine Beschreibung",
        "definition": "Eine Definition",
        "bezeichnung": "Test",
        "freigabe_status": 6,
        "freigabe_status_label": "fachlich freigegeben (gold)",
        "gueltig_ab": None,
        "gueltig_bis": None,
        "status_gesetzt_am": "2023-01-01",
        "bezug": ["Bezug"],
        "status_gesetzt_durch": "Test",
        "steckbrief_id": "D1234",
        "steckbrief_name": None,
        "versionshinweis": "Ein Versionshinweis",
        "veroeffentlichungsdatum": "2023-01-01",
        "stichwort": [],
        "letzte_aenderung": "2020-09-01T00:00:00Z",
        "last_update": "2023-10-18T00:00:00Z",
        "is_latest": True,
        "datenfelder": [
            {
                "namespace": "baukasten",
                "fim_id": "F12001",
                "fim_version": "1.0",
                "nummernkreis": "12000",
                "name": "Hinweis",
                "beschreibung": None,
                "definition": None,
                "gueltig_ab": None,
                "gueltig_bis": None,
                "bezug": [],
                "freigabe_status": 2,
                "freigabe_status_label": "in Bearbeitung",
                "letzte_aenderung": "2020-09-01T00:00:00Z",
                "last_update": "2023-10-18T00:00:00Z",
                "veroeffentlichungsdatum": None,
                "status_gesetzt_am": None,
                "versionshinweis": None,
                "status_gesetzt_durch": "Creator",
                "xdf_version": "2.0",
                "feldart": "input",
                "datentyp": "text",
                "fts_match": None,
                "is_latest": True,
            }
        ],
        "datenfeldgruppen": [],
        "children": [
            {
                "namespace": "baukasten",
                "fim_id": "F12001",
                "fim_version": "1.0",
                "type": "Feld",
                "anzahl": "1:1",
                "bezug": [],
            }
        ],
        "regeln": [],
        "relation": [],
        "uploads": [
            {
                "filename": "S1234V1.0_2023-10-18-1697587200000.xdf2.xml",
                "url": f"{immutable_base_url}/immutable/schemas/S1234V1.0_2023-10-18-1697587200000.xdf2.xml",
                "upload_time": "2023-10-18T00:00:00Z",
                "code_lists": [],
            },
            {
                "filename": "S1234V1.0_2023-10-17-1697500800000.xdf2.xml",
                "url": f"{immutable_base_url}/immutable/schemas/S1234V1.0_2023-10-17-1697500800000.xdf2.xml",
                "upload_time": "2023-10-17T00:00:00Z",
                "code_lists": [],
            },
        ],
        "json_schema_files": [
            {
                "filename": "S1234V1.0_2023-10-17-1697500800000.schema.json",
                "url": f"{immutable_base_url}/immutable/schemas/S1234V1.0_2023-10-17-1697500800000.schema.json",
                "upload_time": "2023-10-17T00:00:00Z",
                "generated_from": "S1234V1.0_2023-10-17-1697500800000.xdf2.xml",
            },
        ],
        "xsd_files": [
            {
                "filename": "S1234V1.0_2023-10-17-1697500800000.xsd",
                "url": f"{immutable_base_url}/immutable/schemas/S1234V1.0_2023-10-17-1697500800000.xsd",
                "upload_time": "2023-10-17T00:00:00Z",
                "generated_from": "S1234V1.0_2023-10-17-1697500800000.xdf2.xml",
            },
        ],
    }


def test_should_fail_for_uploading_unused_code_list(
    runner: CommandRunner, client: TestClient
):
    access_token = runner.create_access_token("12")

    with (
        open(XDF2_DATA / "schema_without_code_lists.xml", "rb") as schema,
        open(XDF2_DATA / "code_list_a.xml", "rb") as code_list,
    ):
        response = client.post(
            "/api/v1/schemas/xdf2",
            data={
                "steckbrief_id": "D1234",
                "freigabe_status": str(
                    xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                ),
            },
            files=[("schema", schema), ("code_lists", code_list)],
            headers={"Access-Token": access_token},
        )

    assert response.status_code == 400
    assert response.json() == {
        "detail": "Imported CodeList urn:de:code_list_a_1 not referenced in schema S1234V1.0"
    }


def test_should_fail_for_empty_versions(runner: CommandRunner, client: TestClient):
    token = runner.create_access_token("12000")

    with open(XDF2_DATA / "schema_without_versions.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf2",
            data={
                "steckbrief_id": "D1234",
                "freigabe_status": str(
                    xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                ),
            },
            files=[
                ("schema", schema),
                ("code_lists", b""),
            ],
            headers={"Access-Token": token},
        )

    assert response.status_code == 400
    assert response.json() == {
        "detail": "Cannot import element without a version [id=G12001]"
    }


def test_should_not_work_for_re_uploading_existing_schema_with_changed_immutable_attribute(
    runner: CommandRunner, client: TestClient
):
    Xdf2Factory().schema(
        id="S1234", version="1.0", name="tseT", fachlicher_ersteller="Laura Grant"
    ).save(runner)

    access_token = runner.create_access_token("12000")
    with open(XDF2_DATA / "schema_without_code_lists.xml", "rb") as schema:
        response = client.post(
            "/api/v1/schemas/xdf2",
            data={
                "steckbrief_id": "D1234",
                "freigabe_status": str(
                    xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value
                ),
            },
            files=[
                ("schema", schema),
            ],
            headers={"Access-Token": access_token},
        )

    assert response.status_code == 400
    print(response.json()["detail"])
    assert response.json() == {
        "detail": (
            "Cannot update schema S1234 version 1.0 because the following attributes are different:\n"
            "  - ableitungsmodifikationen_repraesentation: 0 -> 1\n"
            "  - ableitungsmodifikationen_struktur: 0 -> 3\n"
            "  - beschreibung: None -> Eine Beschreibung\n"
            "  - bezeichnung_eingabe: None -> Test\n"
            "  - bezug: None -> Bezug\n"
            "  - definition: None -> Eine Definition\n"
            "  - fachlicher_ersteller: Laura Grant -> Test\n"
            "  - name: tseT -> Test\n"
            "  - struktur: [] -> [{'anzahl': {'min': 1, 'max': 1}, 'bezug': None, 'element_type': 1, 'enthaelt': {'id': 'F12001', 'version': '1.0'}}]\n"
            "  - versionshinweis: None -> Ein Versionshinweis"
        ),
    }


def test_should_re_upload_rule_with_changed_veroeffentlichungsdatum_none(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    rule = factory.rule(id="R1200", veroeffentlichungsdatum=None).build()
    factory.schema(id="S1234", version="1.0").with_rule(rule).save(runner)

    rule.veroeffentlichungsdatum = date(2024, 11, 21)
    schema_message = factory.schema(id="S1235").with_rule(rule).message()

    access_token = runner.create_access_token("12000")
    response = client.post(
        "/api/v1/schemas/xdf2",
        data={
            "steckbrief_id": "D1234",
            "freigabe_status": str(xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value),
        },
        files=[
            ("schema", xdf2.serialize_schema_message(schema_message)),
        ],
        headers={"Access-Token": access_token},
    )

    assert response.status_code == 200


def test_should_fail_on_re_uploading_rule_with_changed_veroeffentlichungsdatum_date(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    rule = factory.rule(id="R1201", veroeffentlichungsdatum=date(2024, 11, 19)).build()
    factory.schema(id="S1234", version="1.0").with_rule(rule).save(runner)

    rule.veroeffentlichungsdatum = date(2024, 11, 21)
    schema_message = factory.schema(id="S1235").with_rule(rule).message()

    access_token = runner.create_access_token("12000")
    response = client.post(
        "/api/v1/schemas/xdf2",
        data={
            "steckbrief_id": "D1234",
            "freigabe_status": str(xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value),
        },
        files=[
            ("schema", xdf2.serialize_schema_message(schema_message)),
        ],
        headers={"Access-Token": access_token},
    )

    assert response.status_code == 400
    assert response.json() == {
        "detail": (
            f"Cannot update rule {rule.identifier.id} version {rule.identifier.version} because the following attributes are different:\n"
            "  - veroeffentlichungsdatum: 2024-11-19 -> 2024-11-21"
        ),
    }
