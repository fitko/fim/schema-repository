from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.factories.xzufi import OnlinedienstFactory

ENDPOINT = "/api/v0/online-service"


def test_return_all_onlinedienste(runner: CommandRunner, client: TestClient):
    onlinedienst = OnlinedienstFactory().save(runner)

    response = client.get(ENDPOINT)

    assert response.status_code == 200
    assert response.json() == {
        "items": [
            {
                "redaktion_id": onlinedienst.redaktion_id,
                "id": onlinedienst.id,
            }
        ],
        "count": 1,
        "offset": 0,
        "limit": 200,
        "total_count": 1,
    }
