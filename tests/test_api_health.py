from fastapi.testclient import TestClient


def test_should_correctly_return_a_field(
    client: TestClient,
):
    response = client.get("/api/health")

    assert response.status_code == 200
