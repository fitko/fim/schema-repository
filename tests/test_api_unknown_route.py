from fastapi.testclient import TestClient


def test_default_404_error(client: TestClient) -> None:
    response = client.get("/api/v1/unknown")

    assert response.status_code == 404
    assert response.headers["content-type"] == "application/json"
    assert response.json() == {
        "detail": "unknown route",
    }
