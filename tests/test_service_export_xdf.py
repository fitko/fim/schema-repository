from datetime import date

import pytest

from fimportal.models.imports import Xdf2SchemaImport, Xdf3SchemaImport
from fimportal.service import Service
from fimportal.xdatenfelder import xdf2, xdf3
from tests.factories.xdf2 import Xdf2Factory
from tests.factories.xdf3 import Xdf3Factory


class TestExportXdf2:
    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_correctly_export_a_schema(self, service: Service):
        message = Xdf2Factory().schema().full().message()

        xdf2_import = Xdf2SchemaImport.from_message(message)
        await service.import_xdf2_schema(xdf2_import)

        saved_message = await service.export_xdf_schema_message(
            message.id, message.assert_version()
        )
        assert saved_message is not None
        assert isinstance(saved_message, xdf2.SchemaMessage)

        # patch the header, as this should always be unique
        saved_message.header = message.header

        assert saved_message == message

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_correctly_export_a_datenfeldgruppe(self, service: Service):
        factory = Xdf2Factory()
        message = (
            factory.group().with_group(factory.group().full().build()).full().message()
        )

        xdf2_import = Xdf2SchemaImport.from_message(
            factory.schema(freigabedatum=date(2025, 1, 27))
            .with_group(message.group)
            .message()
        )
        await service.import_xdf2_schema(xdf2_import)

        saved_message = await service.export_xdf_datenfeldgruppe_message(
            "baukasten", message.id, message.assert_version()
        )
        assert saved_message is not None
        assert isinstance(saved_message, xdf2.DatenfeldgruppeMessage)

        # patch the header, as this should always be unique
        saved_message.header = message.header

        assert saved_message == message

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_correctly_export_a_datenfeld(self, service: Service):
        factory = Xdf2Factory()
        message = factory.field().full().message()

        xdf2_import = Xdf2SchemaImport.from_message(
            factory.schema(freigabedatum=date(2025, 1, 27))
            .with_field(message.field)
            .message()
        )
        await service.import_xdf2_schema(xdf2_import)

        saved_message = await service.export_xdf_datenfeld_message(
            "baukasten", message.id, message.assert_version()
        )
        assert saved_message is not None
        assert isinstance(saved_message, xdf2.DatenfeldMessage)

        # patch the header, as this should always be unique
        saved_message.header = message.header

        assert saved_message == message


class TestExportXdf3:
    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_correctly_export_a_schema(self, service: Service):
        message = Xdf3Factory().schema().full().message()

        xdf3_import = Xdf3SchemaImport.from_message(message)
        await service.import_xdf3_schema(xdf3_import)

        saved_message = await service.export_xdf_schema_message(
            message.id, message.assert_version()
        )
        assert saved_message is not None
        assert isinstance(saved_message, xdf3.SchemaMessage)

        # patch the header, as this should always be unique
        saved_message.header = message.header

        assert saved_message == message

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_correctly_export_a_datenfeldgruppe(self, service: Service):
        factory = Xdf3Factory()
        message = (
            factory.group().with_group(factory.group().full().build()).full().message()
        )

        xdf2_import = Xdf3SchemaImport.from_message(
            factory.schema().with_group(message.group).message()
        )
        await service.import_xdf3_schema(xdf2_import)

        saved_message = await service.export_xdf_datenfeldgruppe_message(
            "baukasten", message.id, message.assert_version()
        )
        assert saved_message is not None
        assert isinstance(saved_message, xdf3.DatenfeldgruppeMessage)

        # patch the header, as this should always be unique
        saved_message.header = message.header

        assert saved_message == message

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_correctly_export_a_datenfeld(self, service: Service):
        factory = Xdf3Factory()
        message = factory.field().full().message()

        xdf2_import = Xdf3SchemaImport.from_message(
            factory.schema().with_field(message.field).message()
        )
        await service.import_xdf3_schema(xdf2_import)

        saved_message = await service.export_xdf_datenfeld_message(
            "baukasten", message.id, message.assert_version()
        )
        assert saved_message is not None
        assert isinstance(saved_message, xdf3.DatenfeldMessage)

        # patch the header, as this should always be unique
        saved_message.header = message.header

        assert saved_message == message
