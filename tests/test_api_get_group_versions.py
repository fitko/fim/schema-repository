from datetime import date
from fastapi.testclient import TestClient
from freezegun import freeze_time

from fimportal.helpers import format_iso8601
from tests.conftest import CommandRunner
from tests.factories.xdf2 import Xdf2Factory


def test_should_return_all_versions_for_a_group(
    runner: CommandRunner, client: TestClient
):
    today = date.today()

    factory = Xdf2Factory()
    group_1 = (
        factory.group(
            id="G12345",
            version="1.0",
            bezug="Bezug",
            gueltig_ab=today,
            gueltig_bis=today,
            freigabedatum=today,
            veroeffentlichungsdatum=today,
        )
        .full()
        .build()
    )
    group_2 = (
        factory.group(
            id="G12345",
            version="2.0",
            bezug="Bezug",
            gueltig_ab=today,
            gueltig_bis=today,
            freigabedatum=today,
            veroeffentlichungsdatum=today,
        )
        .full()
        .build()
    )
    fixed_time = "2023-10-17T00:00:00Z"
    with freeze_time(fixed_time):
        xdf2_import_1 = factory.schema(id="S12345").with_group(group_1).build_import()
        runner.import_xdf2(xdf2_import_1, strict=False)

        xdf2_import_2 = factory.schema(id="S12346").with_group(group_2).build_import()
        runner.import_xdf2(xdf2_import_2)

    response = client.get("/api/v1/groups/baukasten/G12345")

    assert response.status_code == 200
    assert response.json() == [
        {
            "namespace": "baukasten",
            "fim_id": group_1.identifier.id,
            "fim_version": group_1.identifier.version,
            "nummernkreis": group_1.identifier.get_xdf3_nummernkreis(),
            "name": group_1.name,
            "beschreibung": group_1.beschreibung,
            "definition": group_1.definition,
            "freigabe_status": xdf2_import_1.freigabe_status.value,
            "freigabe_status_label": xdf2_import_1.freigabe_status.to_label(),
            "status_gesetzt_durch": group_1.fachlicher_ersteller,
            "xdf_version": "2.0",
            "bezug": ["Bezug"],
            "gueltig_ab": today.isoformat(),
            "gueltig_bis": today.isoformat(),
            "versionshinweis": group_1.versionshinweis,
            "last_update": fixed_time,
            "letzte_aenderung": format_iso8601(
                xdf2_import_1.schema_message.header.erstellungs_zeitpunkt
            ),
            "status_gesetzt_am": today.isoformat(),
            "veroeffentlichungsdatum": today.isoformat(),
            "is_latest": False,
            "fts_match": None,
        },
        {
            "namespace": "baukasten",
            "fim_id": group_2.identifier.id,
            "fim_version": group_2.identifier.version,
            "nummernkreis": group_2.identifier.get_xdf3_nummernkreis(),
            "name": group_2.name,
            "beschreibung": group_2.beschreibung,
            "definition": group_2.definition,
            "freigabe_status": xdf2_import_2.freigabe_status.value,
            "freigabe_status_label": xdf2_import_2.freigabe_status.to_label(),
            "status_gesetzt_durch": group_2.fachlicher_ersteller,
            "xdf_version": "2.0",
            "bezug": ["Bezug"],
            "gueltig_ab": today.isoformat(),
            "gueltig_bis": today.isoformat(),
            "versionshinweis": group_2.versionshinweis,
            "last_update": fixed_time,
            "letzte_aenderung": format_iso8601(
                xdf2_import_2.schema_message.header.erstellungs_zeitpunkt
            ),
            "status_gesetzt_am": today.isoformat(),
            "veroeffentlichungsdatum": today.isoformat(),
            "is_latest": True,
            "fts_match": None,
        },
    ]


def test_should_return_empty_list_for_unknown_group(client: TestClient):
    response = client.get("/api/v1/groups/baukasten/G12345")

    assert response.status_code == 200
    assert response.json() == []
