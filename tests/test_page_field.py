from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.factories.xdf3 import Xdf3Factory


def test_load_field_page(client: TestClient, runner: CommandRunner) -> None:
    factory = Xdf3Factory()
    field = factory.field().build()
    factory.schema().with_field(field).save(runner)

    response = client.get(
        f"/fields/baukasten/{field.identifier.id}/{field.identifier.version}"
    )

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_unknown_field(client: TestClient) -> None:
    response = client.get("/fields/baukasten/F1234/1.0")

    assert response.status_code == 404
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert "Datenfeld F1234 v1.0 konnte nicht gefunden werden." in response.text
