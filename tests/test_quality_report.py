import pytest

from fimportal.xdatenfelder import xdf2, xdf3
from fimportal.xdatenfelder.reports import QualityReport
from fimportal.xdatenfelder.checks import ElementReport, FailingCheck


@pytest.mark.parametrize(
    "report",
    [
        QualityReport.create(
            schema_checks=[],
            group_reports=[],
            field_reports=[],
            rule_reports=[],
        ),
        QualityReport.create(
            schema_checks=[
                FailingCheck(code=0, error_type="info", message="Testnachricht")
            ],
            group_reports=[
                ElementReport(
                    identifier=xdf2.Identifier("G00001", xdf2.parse_version("1.0")),
                    failing_checks=[
                        FailingCheck(code=0, error_type="info", message="Testnachricht")
                    ],
                )
            ],
            field_reports=[
                ElementReport(
                    identifier=xdf2.Identifier("F00001", xdf2.parse_version("1.0")),
                    failing_checks=[
                        FailingCheck(code=0, error_type="info", message="Testnachricht")
                    ],
                )
            ],
            rule_reports=[
                ElementReport(
                    identifier=xdf2.Identifier("R00001", xdf2.parse_version("1.0")),
                    failing_checks=[
                        FailingCheck(code=0, error_type="info", message="Testnachricht")
                    ],
                )
            ],
        ),
        QualityReport.create(
            schema_checks=[
                FailingCheck(code=0, error_type="info", message="Testnachricht")
            ],
            group_reports=[
                ElementReport(
                    identifier=xdf3.Identifier("G00001", xdf3.parse_version("1.0.0")),
                    failing_checks=[
                        FailingCheck(code=0, error_type="info", message="Testnachricht")
                    ],
                )
            ],
            field_reports=[
                ElementReport(
                    identifier=xdf3.Identifier("F00001", xdf3.parse_version("1.0.0")),
                    failing_checks=[
                        FailingCheck(code=0, error_type="info", message="Testnachricht")
                    ],
                )
            ],
            rule_reports=[
                ElementReport(
                    identifier=xdf3.Identifier("R00001", xdf3.parse_version("1.0.0")),
                    failing_checks=[
                        FailingCheck(code=0, error_type="info", message="Testnachricht")
                    ],
                )
            ],
        ),
    ],
)
def test_should_correctly_serialize_and_deserialize_a_report(report: QualityReport):
    data = report.to_dict()
    parsed_report = QualityReport.from_dict(data)

    assert parsed_report == report
