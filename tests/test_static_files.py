from fastapi.testclient import TestClient
from fastapi import FastAPI, status

import pytest
import os
from tempfile import TemporaryDirectory

from fimportal.static import StaticFiles, StaticFile


def test_load_static_files():
    with TemporaryDirectory() as tmpdir:
        with open(os.path.join(tmpdir, "file.js"), "w") as file:
            file.write("some content")

        files = StaticFiles.load(tmpdir)

        assert len(files.files) == 1
        assert files.files[0].path == "/file.js"
        assert files.files[0].content == b"some content"


def test_load_nested_static_files():
    with TemporaryDirectory() as tmpdir:
        os.mkdir(os.path.join(tmpdir, "styles"))
        with open(os.path.join(tmpdir, "styles", "file.css"), "w") as file:
            file.write("some content")

        files = StaticFiles.load(tmpdir)

        assert len(files.files) == 1
        assert files.files[0].path == "/styles/file.css"
        assert files.files[0].content == b"some content"


def _create_client(files: StaticFiles):
    app = FastAPI()
    files.add_static_paths(app)

    return TestClient(app)


@pytest.fixture(scope="module")
def static_file_client():
    """The first file is the one we test against. The second file is there to
    rule out a condition where due to late binding all static file routes would
    erroneously serve the last file.
    """
    return _create_client(
        StaticFiles(
            [
                StaticFile(
                    path="/file1.txt",
                    content_path="/hash1.txt",
                    content_type="text/plain",
                    content_length="12",
                    content_hash="hash1",
                    content=b"some content",
                ),
                StaticFile(
                    path="/file2.txt",
                    content_path="/hash2.txt",
                    content_type="text/plain",
                    content_length="17",
                    content_hash="hash2",
                    content=b"different content",
                ),
            ],
        ),
    )


def test_get_static_file(static_file_client: TestClient):
    response = static_file_client.get("/hash1.txt")

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/plain"
    assert response.text == "some content"


def test_get_is_not_modified_response_from_static_file(static_file_client: TestClient):
    response = static_file_client.get(
        "/hash1.txt", headers={"if-none-match": "hash1-gzip"}
    )

    assert response.status_code == status.HTTP_304_NOT_MODIFIED


def test_get_static_file_when_sending_differing_etag(static_file_client: TestClient):
    response = static_file_client.get(
        "/hash1.txt", headers={"if-none-match": "invalid_hash"}
    )

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/plain"
    assert response.text == "some content"
