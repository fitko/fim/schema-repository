from fastapi.testclient import TestClient

from tests.conftest import CommandRunner


API_ENDPOINT = "/api/v1/token/access"


def test_should_fail_for_empty_access_token(client: TestClient):
    response = client.get(API_ENDPOINT)

    assert response.status_code == 401
    assert response.json() == {"detail": "invalid access token"}


def test_should_fail_for_invalid_access_token(client: TestClient):
    response = client.get(
        API_ENDPOINT,
        headers={"Access-Token": "wrong_access_token"},
    )

    assert response.status_code == 401
    assert response.json() == {"detail": "invalid access token"}


def test_should_work_for_valid_token(runner: CommandRunner, client: TestClient):
    access_token = runner.create_access_token("12000")

    response = client.get(API_ENDPOINT, headers={"Access-Token": access_token})

    assert response.status_code == 200
    assert response.json() == {"nummernkreis": "12000"}
