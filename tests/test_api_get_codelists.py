from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.factories.genericode import CodeListImportFactory
from tests.factories.xdf2 import Xdf2Factory


def test_should_return_paginated_result(client: TestClient):
    response = client.get("/api/v0/code-lists")

    assert response.status_code == 200

    assert response.json() == {
        "items": [],
        "offset": 0,
        "limit": 200,
        "count": 0,
        "total_count": 0,
    }


def test_should_return_imported_code_list(
    immutable_base_url: str, runner: CommandRunner, client: TestClient
):
    code_list = CodeListImportFactory().build()
    factory = Xdf2Factory()
    field = (
        factory.field()
        .select(
            code_list=code_list,
        )
        .build()
    )
    factory.schema().with_field(field).save(runner, code_lists=[code_list])

    response = client.get("/api/v0/code-lists")

    assert response.status_code == 200

    result = response.json()["items"]
    assert result == [
        {
            "id": 1,
            "url": f"{immutable_base_url}/immutable/code-lists/1/genericode.xml",
            "genericode_canonical_uri": code_list.identifier.canonical_uri,
            "genericode_version": code_list.identifier.version,
            "genericode_canonical_version_uri": code_list.identifier.canonical_version_uri,
            "is_external": False,
        },
    ]


def test_should_return_empty_list_for_no_code_list(client: TestClient):
    response = client.get("/api/v0/code-lists")

    assert response.status_code == 200
    assert response.json()["count"] == 0
