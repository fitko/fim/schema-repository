from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.factories.xzufi import TEST_ZUSTAENDIGKEIT_XML, ZustaendigkeitFactory

ENDPOINT = "/api/v0/jurisdiction"


def test_return_existing_zustaendigkeit(runner: CommandRunner, client: TestClient):
    zustaendigkeit = ZustaendigkeitFactory().save(
        runner, xml_content=TEST_ZUSTAENDIGKEIT_XML
    )

    response = client.get(
        f"{ENDPOINT}/{zustaendigkeit.redaktion_id}/{zustaendigkeit.id}/xzufi"
    )

    assert response.status_code == 200
    assert response.text == TEST_ZUSTAENDIGKEIT_XML
    assert (
        response.headers["content-disposition"]
        == f'attachment; filename="zustaendigkeit_{zustaendigkeit.redaktion_id}_{zustaendigkeit.id}.xzufi.xml"'
    )


def test_fail_for_unknown_zustaendigkeit(client: TestClient):
    response = client.get(f"{ENDPOINT}/L1234/zs_1/xzufi")

    assert response.status_code == 404
    assert response.json() == {
        "detail": "Could not find Zustaendigkeit zs_1 from Redaktion L1234.",
    }
