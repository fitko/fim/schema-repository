import re
from datetime import date, datetime, timedelta, timezone

import pytest

from fimportal.common import FreigabeStatus
from fimportal.xzufi.common import (
    ENGLISH,
    GERMAN,
    Identifikator,
    LeistungsAdressat,
    LeistungsTypisierung,
    ModulTextTyp,
    Sprachversion,
    StringLocalized,
    Vertrauensniveau,
    XzufiException,
)
from fimportal.xzufi.leistung import (
    Amount,
    Bearbeitungsdauermodul,
    BegriffImKontext,
    BegriffImKontextModul,
    BegriffImKontextTyp,
    FachlicheFreigabeModul,
    FristDauer,
    FristMitTyp,
    Fristmodul,
    FristOhneTyp,
    FristStichtag,
    Fristtyp,
    HyperlinkErweitert,
    Klassifizierung,
    Kosten,
    KostenFix,
    KostenFrei,
    Kostenmodul,
    Kostentyp,
    KostenVariabel,
    Leistung,
    Leistungskategorie,
    LeistungskategorieKlasse,
    Leistungsstruktur,
    LeistungsstrukturObjekt,
    LeistungsstrukturObjektMitVerrichtung,
    LeistungsstrukturObjektMitVerrichtungUndDetail,
    MonatTag,
    RawCode,
    Textmodul,
    TextmodulIndividuell,
    Versionsinformation,
    Zeiteinheit,
    parse_leistung,
)
from tests.data import XZUFI_2_2_0_DATA


def test_should_parse_a_minimal_leistung():
    with open(XZUFI_2_2_0_DATA / "leistung_minimal.xml", "rb") as file:
        leistung = parse_leistung(file)

    assert leistung == Leistung(
        id=Identifikator(
            scheme_agency_id="B100019",
            scheme_agency_name=None,
            scheme_data_uri=None,
            scheme_id=None,
            scheme_name=None,
            scheme_uri=None,
            scheme_version_id=None,
            value="leistung_minimal",
        ),
        id_sekundaer=[],
        struktur=None,
        typisierung=[],
        referenz_leika=[],
        modul_text=[],
        modul_text_individuell=[],
        modul_frist=None,
        modul_kosten=None,
        modul_bearbeitungsdauer=None,
        modul_begriff_im_kontext=[],
        modul_fachliche_freigabe=None,
        modul_auskunftshinweis=None,
        modul_dokument=None,
        modul_ursprungsportal=[],
        vertrauensniveau=None,
        leistungsadressat=[],
        kennzeichen_schritformerfordernis=None,
        kategorie=[],
        klassifizierung=[],
        informationsbereich_sdg=[],
        id_leistung_im_kontext=[],
        id_prozess=[],
        herausgeber=None,
        gueltigkeit_gebiet_id=[],
        kennzeichen_ea=None,
        relevant_fuer_wirtschaftszweig=[],
        relevant_fuer_rechtsform=[],
        relevant_fuer_staatsangehoerigkeit=[],
        versionsinformation=None,
        sprachversion=[
            Sprachversion(
                language_code=GERMAN,
                sprachbezeichnung_deutsch=None,
                sprachbezeichnung_nativ=None,
                erstellt_datum_zeit=None,
                erstellt_durch=None,
                geaendert_datum_zeit=None,
                geaendert_durch=None,
                version=None,
            ),
        ],
        gueltigkeit=[],
    )


def test_should_parse_a_leistung():
    with open(XZUFI_2_2_0_DATA / "leistung_full.xml", "rb") as file:
        leistung = parse_leistung(file)

    assert leistung == Leistung(
        id=Identifikator(
            scheme_agency_id="B100019",
            scheme_agency_name="TSA Public Service GmbH",
            scheme_data_uri=None,
            scheme_id="TSA_LEIKA_OID",
            scheme_name="Leika Schul ObjektID",
            scheme_uri=None,
            scheme_version_id=None,
            value="100157500",
        ),
        id_sekundaer=[
            Identifikator(
                scheme_agency_id="GK_LEIKA",
                scheme_agency_name="GK LeiKa",
                scheme_data_uri=None,
                scheme_id="LEIKA_LEISTUNG_SCHLUESSEL",
                scheme_name="LeiKa Leistungsschlüssel",
                scheme_uri=None,
                scheme_version_id="2",
                value="99154033000000",
            ),
        ],
        struktur=None,
        typisierung=[LeistungsTypisierung.TYP_1],
        referenz_leika=[],
        modul_text=[
            Textmodul(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                leika_textmodul=ModulTextTyp.LEISTUNGSBEZEICHNUNG,
                leika_textmodul_abweichende_bezeichnung=[
                    StringLocalized(GERMAN, "Alternative Bezeichnung")
                ],
                inhalt=[StringLocalized(GERMAN, "Leistungsbezeichnung")],
                weiterfuehrender_link=[
                    HyperlinkErweitert(
                        language_code=GERMAN,
                        uri="example.com",
                        titel="Title",
                        beschreibung="Beschreibung",
                        position_darstellung=0,
                    )
                ],
            ),
            Textmodul(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                leika_textmodul=ModulTextTyp.LEISTUNGSBEZEICHNUNG_II,
                leika_textmodul_abweichende_bezeichnung=[],
                inhalt=[StringLocalized(GERMAN, "Leistungsbezeichnung II")],
                weiterfuehrender_link=[
                    HyperlinkErweitert(None, "example.com", None, None, None)
                ],
            ),
            Textmodul(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                leika_textmodul=ModulTextTyp.KURZTEXT,
                leika_textmodul_abweichende_bezeichnung=[],
                inhalt=[StringLocalized(GERMAN, "Kurztext")],
                weiterfuehrender_link=[
                    HyperlinkErweitert(None, "example.com", None, None, None)
                ],
            ),
            Textmodul(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                leika_textmodul=ModulTextTyp.VOLLTEXT,
                leika_textmodul_abweichende_bezeichnung=[],
                inhalt=[StringLocalized(GERMAN, "Volltext")],
                weiterfuehrender_link=[
                    HyperlinkErweitert(None, "example.com", None, None, None)
                ],
            ),
            Textmodul(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                leika_textmodul=ModulTextTyp.RECHTSGRUNDLAGEN,
                leika_textmodul_abweichende_bezeichnung=[],
                inhalt=[StringLocalized(GERMAN, "Rechtsgrundlagen")],
                weiterfuehrender_link=[
                    HyperlinkErweitert(None, "example.com", None, None, None)
                ],
            ),
            Textmodul(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                leika_textmodul=ModulTextTyp.ERFORDERLICHE_UNTERLAGEN,
                leika_textmodul_abweichende_bezeichnung=[],
                inhalt=[StringLocalized(GERMAN, "Erforderliche Unterlagen")],
                weiterfuehrender_link=[
                    HyperlinkErweitert(None, "example.com", None, None, None)
                ],
            ),
            Textmodul(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                leika_textmodul=ModulTextTyp.VORAUSSETZUNGEN,
                leika_textmodul_abweichende_bezeichnung=[],
                inhalt=[StringLocalized(GERMAN, "Voraussetzungen")],
                weiterfuehrender_link=[
                    HyperlinkErweitert(None, "example.com", None, None, None)
                ],
            ),
            Textmodul(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                leika_textmodul=ModulTextTyp.VERFAHRENSABLAUF,
                leika_textmodul_abweichende_bezeichnung=[],
                inhalt=[StringLocalized(GERMAN, "Verfahrensablauf")],
                weiterfuehrender_link=[
                    HyperlinkErweitert(None, "example.com", None, None, None)
                ],
            ),
            Textmodul(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                leika_textmodul=ModulTextTyp.FORMUALRE,
                leika_textmodul_abweichende_bezeichnung=[],
                inhalt=[StringLocalized(GERMAN, "Formulare")],
                weiterfuehrender_link=[
                    HyperlinkErweitert(None, "example.com", None, None, None)
                ],
            ),
            Textmodul(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                leika_textmodul=ModulTextTyp.WEITERFUEHRENDE_INFORMATIONEN,
                leika_textmodul_abweichende_bezeichnung=[],
                inhalt=[StringLocalized(GERMAN, "Weiterfuehrende Informationen")],
                weiterfuehrender_link=[
                    HyperlinkErweitert(None, "example.com", None, None, None)
                ],
            ),
            Textmodul(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                leika_textmodul=ModulTextTyp.HINWEISE,
                leika_textmodul_abweichende_bezeichnung=[],
                inhalt=[StringLocalized(GERMAN, "Hinweise")],
                weiterfuehrender_link=[
                    HyperlinkErweitert(None, "example.com", None, None, None)
                ],
            ),
            Textmodul(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                leika_textmodul=ModulTextTyp.URHEBER,
                leika_textmodul_abweichende_bezeichnung=[],
                inhalt=[StringLocalized(GERMAN, "Urheber")],
                weiterfuehrender_link=[
                    HyperlinkErweitert(None, "example.com", None, None, None)
                ],
            ),
            Textmodul(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                leika_textmodul=ModulTextTyp.ZUSTAENDIGE_STELLE,
                leika_textmodul_abweichende_bezeichnung=[],
                inhalt=[StringLocalized(GERMAN, "Zustaendige Stelle")],
                weiterfuehrender_link=[
                    HyperlinkErweitert(None, "example.com", None, None, None)
                ],
            ),
            Textmodul(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                leika_textmodul=ModulTextTyp.ANSPRECHPUNKT,
                leika_textmodul_abweichende_bezeichnung=[],
                inhalt=[StringLocalized(GERMAN, "Ansprechpunkt")],
                weiterfuehrender_link=[
                    HyperlinkErweitert(None, "example.com", None, None, None)
                ],
            ),
            Textmodul(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                leika_textmodul=ModulTextTyp.TEASER,
                leika_textmodul_abweichende_bezeichnung=[],
                inhalt=[StringLocalized(GERMAN, "Teaser")],
                weiterfuehrender_link=[
                    HyperlinkErweitert(None, "example.com", None, None, None)
                ],
            ),
            Textmodul(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                leika_textmodul=ModulTextTyp.RECHTSBEHELF,
                leika_textmodul_abweichende_bezeichnung=[],
                inhalt=[StringLocalized(GERMAN, "Rechtsbehelf")],
                weiterfuehrender_link=[
                    HyperlinkErweitert(None, "example.com", None, None, None)
                ],
            ),
        ],
        modul_text_individuell=[
            TextmodulIndividuell(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                individuelles_textmodultyp=RawCode(
                    list_uri="some_codelist",
                    list_version_id="1",
                    code="ABC",
                ),
                inhalt=[StringLocalized(GERMAN, "Textmodul Individuell")],
                weiterfuehrender_link=[
                    HyperlinkErweitert(
                        language_code=GERMAN,
                        uri="example.com",
                        titel="Title",
                        beschreibung="Beschreibung",
                        position_darstellung=0,
                    )
                ],
            ),
        ],
        modul_frist=Fristmodul(
            position_darstellung=None,
            id=None,
            id_sekundaer=[],
            gueltigkeit=[],
            frist=[
                FristMitTyp(
                    typ=Fristtyp.ANTRAGSFRIST,
                    fristauswahl=FristDauer(
                        dauer=2,
                        einheit=Zeiteinheit.WOCHE,
                        dauer_bis=None,
                        einheit_bis=None,
                    ),
                    beschreibung=[
                        StringLocalized(
                            language_code="de", value="vorher " "beantragen"
                        ),
                        StringLocalized(
                            language_code="en", value="request " "beforehand"
                        ),
                    ],
                    position_darstellung=None,
                    gueltigkeit=[],
                )
            ],
            beschreibung=[],
            weiterfuehrender_link=[],
        ),
        modul_kosten=Kostenmodul(
            position_darstellung=None,
            id=None,
            id_sekundaer=[],
            gueltigkeit=[],
            kosten=[
                Kosten(
                    typ=Kostentyp.GEBUEHR,
                    kostenauswahl=KostenFix(
                        betrag=Amount(currency_code="EUR", value=207.30)
                    ),
                    beschreibung=[
                        StringLocalized(
                            language_code="de",
                            value="1100 l Container bei Leerung.",
                        ),
                    ],
                    vorkasse=False,
                    link_kostenbildung=None,
                    position_darstellung=0,
                    gueltigkeit=[],
                )
            ],
            beschreibung=[],
            weiterfuehrender_link=[],
        ),
        modul_bearbeitungsdauer=None,
        modul_begriff_im_kontext=[
            BegriffImKontextModul(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                begriff_im_kontext=[
                    BegriffImKontext(
                        begriff=StringLocalized(
                            language_code="de-DE",
                            value="Krebsfrüherkennung",
                        ),
                        typ=None,
                    ),
                ],
            ),
        ],
        modul_fachliche_freigabe=FachlicheFreigabeModul(
            position_darstellung=None,
            id=None,
            id_sekundaer=[],
            gueltigkeit=[],
            fachlich_freigegeben_am=date(2020, 9, 23),
            fachlich_freigegeben_durch=[
                StringLocalized(language_code="de-DE", value="Deutsch")
            ],
        ),
        modul_auskunftshinweis=None,
        modul_dokument=None,
        modul_ursprungsportal=[],
        vertrauensniveau=Vertrauensniveau.UNBESTIMMT,
        leistungsadressat=[LeistungsAdressat.BUERGER],
        kennzeichen_schritformerfordernis=None,
        kategorie=[],
        klassifizierung=[
            Klassifizierung(
                list_uri="urn:xoev-de:fim:codeliste:pvlagen", value="2030700"
            )
        ],
        informationsbereich_sdg=[],
        id_leistung_im_kontext=[],
        id_prozess=[],
        herausgeber=None,
        gueltigkeit_gebiet_id=[],
        kennzeichen_ea=False,
        relevant_fuer_wirtschaftszweig=[],
        relevant_fuer_rechtsform=[],
        relevant_fuer_staatsangehoerigkeit=[],
        versionsinformation=Versionsinformation(
            erstellt_datum_zeit=datetime(2012, 11, 20, 7, 18, 24, tzinfo=timezone.utc),
            erstellt_durch="Mustermann, Max",
            geaendert_datum_zeit=datetime(2023, 5, 8, 12, 56, 9, tzinfo=timezone.utc),
            geaendert_durch="Mustermann, Max",
            version=None,
        ),
        sprachversion=[
            Sprachversion(
                language_code=GERMAN,
                sprachbezeichnung_deutsch="Deutsch",
                sprachbezeichnung_nativ="Deutsch",
                erstellt_datum_zeit=datetime(
                    2017, 6, 7, 6, 11, 14, tzinfo=timezone(timedelta(seconds=7200))
                ),
                erstellt_durch="system",
                geaendert_datum_zeit=datetime(
                    2017, 6, 9, 9, 23, 30, tzinfo=timezone(timedelta(seconds=7200))
                ),
                geaendert_durch="Administrator",
                version=None,
            ),
            Sprachversion(
                language_code=ENGLISH,
                sprachbezeichnung_deutsch="Englisch",
                sprachbezeichnung_nativ="English",
                erstellt_datum_zeit=datetime(
                    2021, 7, 7, 11, 12, 26, tzinfo=timezone(timedelta(seconds=7200))
                ),
                erstellt_durch="Administrator",
                geaendert_datum_zeit=datetime(
                    2021, 4, 20, 11, 56, 3, tzinfo=timezone(timedelta(seconds=7200))
                ),
                geaendert_durch="Mustermann, Max",
                version=None,
            ),
        ],
        gueltigkeit=[],
    )


@pytest.mark.parametrize(
    "filename,status_katalog,status_bibliothek",
    [
        ("leistung_minimal.xml", None, None),
        (
            "leistung_freigabestatus.xml",
            FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
            FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER,
        ),
    ],
)
def test_should_correctly_extract_freigabestati(
    filename: str,
    status_katalog: FreigabeStatus | None,
    status_bibliothek: FreigabeStatus | None,
):
    with open(XZUFI_2_2_0_DATA / filename, "rb") as file:
        leistung = parse_leistung(file)

    assert leistung.get_freigabe_status_katalog() == status_katalog
    assert leistung.get_freigabe_status_bibliothek() == status_bibliothek


@pytest.mark.parametrize(
    "filename,modul",
    [
        (
            "leistung_frist_dauer.xml",
            Fristmodul(
                id=None,
                id_sekundaer=[],
                position_darstellung=None,
                gueltigkeit=[],
                frist=[
                    FristMitTyp(
                        typ=Fristtyp.ANTRAGSFRIST,
                        fristauswahl=FristDauer(
                            dauer=2,
                            einheit=Zeiteinheit.WOCHE,
                            dauer_bis=4,
                            einheit_bis=Zeiteinheit.WOCHE,
                        ),
                        beschreibung=[],
                        position_darstellung=None,
                        gueltigkeit=[],
                    )
                ],
                beschreibung=[],
                weiterfuehrender_link=[],
            ),
        ),
        (
            "leistung_frist_stichtag_datum.xml",
            Fristmodul(
                id=None,
                id_sekundaer=[],
                position_darstellung=None,
                gueltigkeit=[],
                frist=[
                    FristMitTyp(
                        typ=Fristtyp.ANTRAGSFRIST,
                        fristauswahl=FristStichtag(
                            von=date(2023, 3, 15),
                            bis=date(2023, 9, 30),
                        ),
                        beschreibung=[],
                        position_darstellung=None,
                        gueltigkeit=[],
                    )
                ],
                beschreibung=[],
                weiterfuehrender_link=[],
            ),
        ),
        (
            "leistung_frist_stichtag_monat_tag.xml",
            Fristmodul(
                id=None,
                id_sekundaer=[],
                position_darstellung=None,
                gueltigkeit=[],
                frist=[
                    FristMitTyp(
                        typ=Fristtyp.ANTRAGSFRIST,
                        fristauswahl=FristStichtag(
                            von=MonatTag("--02-01+01:00"),
                            bis=MonatTag("--02-28+01:00"),
                        ),
                        beschreibung=[],
                        position_darstellung=None,
                        gueltigkeit=[],
                    )
                ],
                beschreibung=[],
                weiterfuehrender_link=[],
            ),
        ),
    ],
)
def test_should_parse_modul_frist(filename: str, modul: Fristmodul):
    with open(XZUFI_2_2_0_DATA / filename, "rb") as file:
        leistung = parse_leistung(file)

    assert leistung.modul_frist == modul


@pytest.mark.parametrize(
    "filename,modul",
    [
        (
            "leistung_kosten_ungelisteter_typ.xml",
            Kostenmodul(
                id=None,
                id_sekundaer=[],
                position_darstellung=None,
                gueltigkeit=[],
                kosten=[
                    Kosten(
                        typ="Gebühr Personenstandsurkunde",
                        kostenauswahl=KostenFrei(beschreibung_kostenfreiheit=[]),
                        beschreibung=[],
                        vorkasse=None,
                        link_kostenbildung=None,
                        position_darstellung=None,
                        gueltigkeit=[],
                    )
                ],
                beschreibung=[],
                weiterfuehrender_link=[],
            ),
        ),
        (
            "leistung_kosten_fix.xml",
            Kostenmodul(
                id=None,
                id_sekundaer=[],
                position_darstellung=None,
                gueltigkeit=[],
                kosten=[
                    Kosten(
                        typ=Kostentyp.GEBUEHR,
                        kostenauswahl=KostenFix(
                            Amount(currency_code="EUR", value=13.0)
                        ),
                        beschreibung=[],
                        vorkasse=None,
                        link_kostenbildung=None,
                        position_darstellung=None,
                        gueltigkeit=[],
                    )
                ],
                beschreibung=[],
                weiterfuehrender_link=[],
            ),
        ),
        (
            "leistung_kosten_frei.xml",
            Kostenmodul(
                id=None,
                id_sekundaer=[],
                position_darstellung=None,
                gueltigkeit=[],
                kosten=[
                    Kosten(
                        typ=Kostentyp.VERWALTUNGSGEBUEHR,
                        kostenauswahl=KostenFrei(beschreibung_kostenfreiheit=[]),
                        beschreibung=[],
                        vorkasse=None,
                        link_kostenbildung=None,
                        position_darstellung=None,
                        gueltigkeit=[],
                    )
                ],
                beschreibung=[],
                weiterfuehrender_link=[],
            ),
        ),
        (
            "leistung_kosten_variabel.xml",
            Kostenmodul(
                id=None,
                id_sekundaer=[],
                position_darstellung=None,
                gueltigkeit=[],
                kosten=[
                    Kosten(
                        typ=Kostentyp.GEBUEHR,
                        kostenauswahl=KostenVariabel(
                            betrag_untergrenze=Amount(currency_code="EUR", value=25.0),
                            betrag_obergrenze=Amount(currency_code="EUR", value=250.0),
                            beschreibung_variabilitaet=[
                                StringLocalized(
                                    language_code="de", value="Beschreibung"
                                )
                            ],
                        ),
                        beschreibung=[],
                        vorkasse=None,
                        link_kostenbildung=None,
                        position_darstellung=None,
                        gueltigkeit=[],
                    )
                ],
                beschreibung=[],
                weiterfuehrender_link=[],
            ),
        ),
    ],
)
def test_should_parse_modul_kosten(filename: str, modul: Kostenmodul):
    with open(XZUFI_2_2_0_DATA / filename, "rb") as file:
        leistung = parse_leistung(file)

    assert leistung.modul_kosten == modul


@pytest.mark.parametrize(
    "filename,modul",
    [
        (
            "leistung_bearbeitungsdauer_empty.xml",
            Bearbeitungsdauermodul(
                id=None,
                id_sekundaer=[],
                position_darstellung=None,
                gueltigkeit=[],
                bearbeitungsdauer=[],
                beschreibung=[],
                weiterfuehrender_link=[],
            ),
        ),
        (
            "leistung_bearbeitungsdauer_frist_dauer.xml",
            Bearbeitungsdauermodul(
                id=None,
                id_sekundaer=[],
                position_darstellung=None,
                gueltigkeit=[],
                bearbeitungsdauer=[
                    FristOhneTyp(
                        fristauswahl=FristDauer(
                            dauer=1,
                            einheit=Zeiteinheit.WOCHE,
                            dauer_bis=4,
                            einheit_bis=Zeiteinheit.WOCHE,
                        ),
                        beschreibung=[],
                        position_darstellung=None,
                        gueltigkeit=[],
                    )
                ],
                beschreibung=[],
                weiterfuehrender_link=[],
            ),
        ),
    ],
)
def test_should_parse_modul_bearbeitungsdauer(
    filename: str, modul: Bearbeitungsdauermodul
):
    with open(XZUFI_2_2_0_DATA / filename, "rb") as file:
        leistung = parse_leistung(file)

    assert leistung.modul_bearbeitungsdauer == modul


@pytest.mark.parametrize(
    "filename,struktur",
    [
        (
            "leistung_struktur_lo_leika.xml",
            LeistungsstrukturObjekt(
                leistungsgruppierung=RawCode(
                    list_uri="urn:de:fim:leika:leistungsgruppierung",
                    list_version_id="20231229",
                    code="155",
                )
            ),
        ),
        (
            "leistung_struktur_lo_individuell.xml",
            LeistungsstrukturObjekt(
                leistungsgruppierung=Leistungskategorie(
                    id=Identifikator(
                        scheme_agency_id=None,
                        scheme_agency_name=None,
                        scheme_data_uri=None,
                        scheme_id="LEISTUNGSGRUPPIERUNG",
                        scheme_name=None,
                        scheme_uri=None,
                        scheme_version_id=None,
                        value="080",
                    ),
                    id_sekundaer=[],
                    beschreibung=[],
                    uebergeordnete_kategorie_id=None,
                    untergeordnete_kategorie_id=[],
                    bezeichnung=[StringLocalized("de", "Bezeichnung")],
                    klasse=LeistungskategorieKlasse(
                        id=Identifikator(
                            scheme_agency_id=None,
                            scheme_agency_name=None,
                            scheme_data_uri=None,
                            scheme_id=None,
                            scheme_name=None,
                            scheme_uri=None,
                            scheme_version_id=None,
                            value="LEISTUNGSGRUPPIERUNG",
                        ),
                        id_sekundaer=[],
                        bezeichnung=[
                            StringLocalized("de", "Individuelle Leistungsgruppierung")
                        ],
                        beschreibung=[],
                        version=None,
                    ),
                ),
            ),
        ),
        (
            "leistung_struktur_lov.xml",
            LeistungsstrukturObjektMitVerrichtung(
                leistungsgruppierung=RawCode(
                    list_uri="urn:de:fim:leika:leistungsgruppierung",
                    list_version_id="20231229",
                    code="109",
                ),
                leistungsobjekt_id=Identifikator(
                    scheme_agency_id=None,
                    scheme_agency_name=None,
                    scheme_data_uri=None,
                    scheme_id=None,
                    scheme_name=None,
                    scheme_uri=None,
                    scheme_version_id=None,
                    value="102835419",
                ),
                verrichtung=RawCode(
                    list_uri="urn:de:fim:leika:verrichtung",
                    list_version_id="20230523",
                    code="001",
                ),
            ),
        ),
        (
            "leistung_struktur_lovd.xml",
            LeistungsstrukturObjektMitVerrichtungUndDetail(
                leistungsgruppierung=Leistungskategorie(
                    id=Identifikator(
                        scheme_agency_id=None,
                        scheme_agency_name=None,
                        scheme_data_uri=None,
                        scheme_id="LEISTUNGSGRUPPIERUNG",
                        scheme_name=None,
                        scheme_uri=None,
                        scheme_version_id=None,
                        value="038",
                    ),
                    id_sekundaer=[],
                    uebergeordnete_kategorie_id=None,
                    untergeordnete_kategorie_id=[],
                    bezeichnung=[
                        StringLocalized(ENGLISH, "Compensation benefits"),
                        StringLocalized("de", "Entgeltersatzleistungen"),
                    ],
                    beschreibung=[],
                    klasse=LeistungskategorieKlasse(
                        id=Identifikator(
                            scheme_agency_id=None,
                            scheme_agency_name=None,
                            scheme_data_uri=None,
                            scheme_id=None,
                            scheme_name=None,
                            scheme_uri=None,
                            scheme_version_id=None,
                            value="LEISTUNGSGRUPPIERUNG",
                        ),
                        id_sekundaer=[],
                        bezeichnung=[
                            StringLocalized("de", "Individuelle Leistungsgruppierung")
                        ],
                        beschreibung=[],
                        version=None,
                    ),
                ),
                leistungsobjekt_id=Identifikator(
                    scheme_agency_id=None,
                    scheme_agency_name=None,
                    scheme_data_uri=None,
                    scheme_id=None,
                    scheme_name=None,
                    scheme_uri=None,
                    scheme_version_id=None,
                    value="967685",
                ),
                verrichtung=RawCode(
                    list_uri="urn:de:fim:leika:verrichtung",
                    list_version_id="20230523",
                    code="223",
                ),
                verrichtungsdetail=[
                    StringLocalized(ENGLISH, "For employers"),
                    StringLocalized("de", "Für " "Arbeitgeber"),
                ],
            ),
        ),
    ],
)
def test_should_parse_struktur(filename: str, struktur: Leistungsstruktur):
    with open(XZUFI_2_2_0_DATA / filename, "rb") as file:
        leistung = parse_leistung(file)

    assert leistung.struktur == struktur


def test_should_parse_informationsbereich_sdg():
    with open(XZUFI_2_2_0_DATA / "leistung_informationsbereich_sdg.xml", "rb") as file:
        leistung = parse_leistung(file)

    assert leistung.informationsbereich_sdg == ["2050100"]


def test_should_parse_begriff_im_kontext():
    with open(XZUFI_2_2_0_DATA / "leistung_begriff_im_kontext.xml", "rb") as file:
        leistung = parse_leistung(file)

    assert leistung.modul_begriff_im_kontext == [
        BegriffImKontextModul(
            id=None,
            id_sekundaer=[],
            gueltigkeit=[],
            position_darstellung=None,
            begriff_im_kontext=[
                BegriffImKontext(
                    begriff=StringLocalized(language_code="de", value="Begriff"),
                    typ=BegriffImKontextTyp.SYNONYM,
                )
            ],
        )
    ]


def test_should_parse_kategorie():
    with open(XZUFI_2_2_0_DATA / "leistung_kategorie.xml", "rb") as file:
        leistung = parse_leistung(file)

    assert leistung.kategorie == [
        Leistungskategorie(
            id=Identifikator(
                scheme_agency_id=None,
                scheme_agency_name=None,
                scheme_data_uri=None,
                scheme_id=None,
                scheme_name=None,
                scheme_uri=None,
                scheme_version_id=None,
                value="8935715",
            ),
            id_sekundaer=[],
            beschreibung=[],
            klasse=LeistungskategorieKlasse(
                id=Identifikator(
                    scheme_agency_id=None,
                    scheme_agency_name=None,
                    scheme_data_uri=None,
                    scheme_id=None,
                    scheme_name=None,
                    scheme_uri=None,
                    scheme_version_id=None,
                    value="LEBENSLAGE_UNTERNEHMENSLAGE",
                ),
                id_sekundaer=[],
                bezeichnung=[StringLocalized("de", "Lebens- und Unternehmenslage")],
                beschreibung=[],
                version=None,
            ),
            uebergeordnete_kategorie_id=Identifikator(
                scheme_agency_id=None,
                scheme_agency_name=None,
                scheme_data_uri=None,
                scheme_id=None,
                scheme_name=None,
                scheme_uri=None,
                scheme_version_id=None,
                value="8935703",
            ),
            untergeordnete_kategorie_id=[
                Identifikator(
                    scheme_agency_id=None,
                    scheme_agency_name=None,
                    scheme_data_uri=None,
                    scheme_id=None,
                    scheme_name=None,
                    scheme_uri=None,
                    scheme_version_id=None,
                    value="9544001",
                ),
                Identifikator(
                    scheme_agency_id=None,
                    scheme_agency_name=None,
                    scheme_data_uri=None,
                    scheme_id=None,
                    scheme_name=None,
                    scheme_uri=None,
                    scheme_version_id=None,
                    value="9544004",
                ),
            ],
            bezeichnung=[StringLocalized("de", "Genehmigungen/ Bescheinigungen")],
        )
    ]


def test_should_handle_invalid_dates_correctly():
    with open(XZUFI_2_2_0_DATA / "leistung_with_invalid_date.xml", "rb") as file:
        content = file.read()

    with pytest.raises(
        XzufiException,
        match=re.escape("Could not parse leistung: Invalid date '202214-02-14+01:00'"),
    ):
        parse_leistung(content)
