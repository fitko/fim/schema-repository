from datetime import datetime

from fimportal.common import Bundesland, VerwaltungspolitischeKodierung
from fimportal.xzufi import (
    parse_raw_organisationseinheit,
)
from fimportal.xzufi.common import (
    ALT_GERMAN,
    Identifikator,
    Sprachversion,
    StringLocalized,
    Zeitraum,
)
from fimportal.xzufi.organisationseinheit import (
    AnschriftOrganisationseinheit,
    Anschrifttyp,
    Erreichbarkeit,
    Erreichbarkeitskanal,
    Geokodierung,
    Herausgeber,
    NameOrganisation,
    Organisationseinheit,
)
from tests.data import XZUFI_2_2_0_DATA


def test_should_parse_an_organisationseinheit():
    data = (XZUFI_2_2_0_DATA / "organisationseinheit.xml").read_text()

    organisationseinheit = parse_raw_organisationseinheit(data)

    assert organisationseinheit == Organisationseinheit(
        id=Identifikator(
            scheme_agency_id="S100003",
            scheme_agency_name=None,
            scheme_data_uri=None,
            scheme_id=None,
            scheme_name=None,
            scheme_uri=None,
            scheme_version_id=None,
            value="S1000030000011895",
        ),
        id_sekundaer=[],
        externe_organisationseinheitsermittlung=False,
        uebergeordnete_organisationseinheit_id=None,
        untergeordnete_organisationseinheit_id=[],
        alternative_hierarchie=[],
        kategorie=[],
        name=[
            NameOrganisation(
                name=[
                    StringLocalized(
                        language_code="de", value="Zentrale Kfz-Zulassungsbehörde"
                    )
                ],
                kurzbeschreibung=[],
                gueltigkeit=None,
            )
        ],
        beschreibung=[
            StringLocalized(
                language_code="de",
                value="Eine Terminvereinbarung",
            )
        ],
        kurzbeschreibung=[],
        info_oeffnungszeiten_text=[
            StringLocalized(
                language_code="de",
                value="Mo-Fr 08:00 - 12:00",
            )
        ],
        info_oeffnungszeiten_struktur=[],
        info_intern_servicecenter=[],
        info_sonstige=[
            StringLocalized(language_code="de", value="<p>bar,\xa0EC-Karte</p>")
        ],
        anschrift=[
            AnschriftOrganisationseinheit(
                typ=Anschrifttyp.HAUSANSCHRIFT,
                strasse="Stresemannstraße",
                hausnummer="48",
                postfach=None,
                postleitzahl="28207",
                ort="Bremen",
                ort_id=[],
                verwaltungspolitische_kodierung=VerwaltungspolitischeKodierung(
                    kreis=None,
                    bezirk=None,
                    bundesland=Bundesland.BREMEN,
                    gemeindeschluessel="04011000",
                    regionalschluessel="040110000000",
                    nation=None,
                    gemeindeverband=None,
                ),
                zusatz=None,
                geokodierung=[Geokodierung(4509789.876932327, 5301670.372857233)],
                id=None,
                gueltigkeit=[],
                anfahrtsuri=[],
                info_parkplatz=[],
                info_oepnv=[],
                info_barrierefreiheit=[],
                kennzeichen_aufzug=None,
                kennzeichen_rollstuhlgerecht=None,
            )
        ],
        erreichbarkeit=[
            Erreichbarkeit(
                kanal=Erreichbarkeitskanal.TELEFON_FESTNETZ,
                kennung="(0421) 361-88668, Terminvereinbarungen: Tel. (0421) 115",
                kennungszusatz=None,
                zusatz=None,
                gueltigkeit=[],
            ),
            Erreichbarkeit(
                kanal=Erreichbarkeitskanal.FAX,
                kennung="(0421) 361-14096 oder 496-12289",
                kennungszusatz=None,
                zusatz=None,
                gueltigkeit=[],
            ),
            Erreichbarkeit(
                kanal=Erreichbarkeitskanal.EMAIL,
                kennung="zentralekfz-zulassungsbehoerde@buergeramt.bremen.de",
                kennungszusatz=None,
                zusatz=None,
                gueltigkeit=[],
            ),
        ],
        kommunikationssystem=[],
        internetaddresse=[],
        bankverbindung=[],
        glaeubiger_identifikationsnummer=None,
        zahlungsweise=[],
        zahlungsweise_text=[],
        synonym=[],
        herausgeber=Herausgeber(
            id=Identifikator(
                scheme_agency_id=None,
                scheme_agency_name=None,
                scheme_data_uri=None,
                scheme_id=None,
                scheme_name=None,
                scheme_uri=None,
                scheme_version_id=None,
                value="Bremen",
            ),
            id_sekundaer=[],
            bezeichnung=["Serviceportal der Freien Hansestadt Bremen"],
            kurzbezeichnung=["Serviceportal der Freien Hansestadt Bremen"],
            beschreibung=[],
            standard_zustaendigkeitsgebiet=[],
        ),
        versions_informationen=None,
        sprachversion=[
            Sprachversion(
                language_code=ALT_GERMAN,
                sprachbezeichnung_deutsch="Deutsch",
                sprachbezeichnung_nativ="Deutsch",
                erstellt_datum_zeit=None,
                erstellt_durch=None,
                geaendert_datum_zeit=None,
                geaendert_durch=None,
                version=None,
            )
        ],
        gueltigkeit=[Zeitraum(beginn=datetime(2024, 7, 9, 0, 0), ende=None, zusatz=[])],
    )
