from fimportal import xml
from fimportal.xzufi.common import remove_user_data


XML_WITH_USER_DATA = """<?xml version="1.0" ?>
<xzufi:leistung xmlns:xzufi="http://xoev.de/schemata/xzufi/2_2_0">
	<xzufi:id schemeAgencyID="B100019">leistung_with_user_data</xzufi:id>

    <xzufi:versionsinformation>
        <xzufi:erstelltDurch>Serviceportal der Freien Hansestadt Bremen</xzufi:erstelltDurch>
        <xzufi:geaendertDurch>Serviceportal der Freien Hansestadt Bremen</xzufi:geaendertDurch>
    </xzufi:versionsinformation>
    <xzufi:sprachversion>
        <xzufi:languageCode>de</xzufi:languageCode>
        <xzufi:sprachbezeichnungDeutsch>Deutsch</xzufi:sprachbezeichnungDeutsch>
        <xzufi:sprachbezeichnungNativ>Deutsch</xzufi:sprachbezeichnungNativ>
        <xzufi:erstelltDurch>Serviceportal der Freien Hansestadt Bremen</xzufi:erstelltDurch>
        <xzufi:geaendertDurch>Serviceportal der Freien Hansestadt Bremen</xzufi:geaendertDurch>
    </xzufi:sprachversion>
</xzufi:leistung>
"""

XML_STRIPPED_USER_DATA = """<?xml version="1.0" ?>
<xzufi:leistung xmlns:xzufi="http://xoev.de/schemata/xzufi/2_2_0">
	<xzufi:id schemeAgencyID="B100019">leistung_with_user_data</xzufi:id>

    <xzufi:versionsinformation>
        </xzufi:versionsinformation>
    <xzufi:sprachversion>
        <xzufi:languageCode>de</xzufi:languageCode>
        <xzufi:sprachbezeichnungDeutsch>Deutsch</xzufi:sprachbezeichnungDeutsch>
        <xzufi:sprachbezeichnungNativ>Deutsch</xzufi:sprachbezeichnungNativ>
    </xzufi:sprachversion>
</xzufi:leistung>
"""


def test_should_remove_user_data():
    stripped_xml = remove_user_data(XML_WITH_USER_DATA)

    assert xml.canonicalize(stripped_xml) == xml.canonicalize(XML_STRIPPED_USER_DATA)
