from fimportal.xzufi.common import (
    ALT_GERMAN,
    Identifikator,
    Sprachversion,
    StringLocalized,
    Zahlungsweise,
)
from fimportal.xzufi.onlinedienst import (
    LinkTyp,
    OnlineDienstDynamischeParameter,
    OnlinedienstLink,
    Onlinedienst,
    OnlineDienstDynamischeParameterTyp,
    OnlinedienstParameter,
    Vertrauensniveau,
    parse_onlinedienst,
)
from tests.data import XZUFI_2_2_0_DATA


def test_should_parse_an_onlinedienst():
    with open(XZUFI_2_2_0_DATA / "onlinedienst.xml", "rb") as file:
        data = file.read()

    onlinedienst = parse_onlinedienst(data)

    assert onlinedienst == Onlinedienst(
        id=Identifikator(
            scheme_agency_id="L100012",
            scheme_agency_name=None,
            scheme_data_uri=None,
            scheme_id="OID",
            scheme_name=None,
            scheme_uri=None,
            scheme_version_id=None,
            value="280810655",
        ),
        id_sekundaer=[],
        bezeichnung=[
            StringLocalized(ALT_GERMAN, "Vorkaufsrecht der Gemeinde online ausüben"),
        ],
        kurzbezeichnung=[],
        beschreibung=[],
        teaser=[],
        link=[
            OnlinedienstLink(
                language_code=ALT_GERMAN,
                typ=LinkTyp.INTERNETAUFRUF,
                link="https://afm.schleswig-holstein.de/intelliform/forms/land/sh/vkg_vorkaufsrechtgemeindesh/vkg_vorkaufsrechtgemeindesh/index?gebiets_id=",
                dynamische_parameter=[
                    OnlineDienstDynamischeParameter(
                        typ=OnlineDienstDynamischeParameterTyp.GEBIET,
                        parameter_name="gebiets_id",
                    )
                ],
                titel="Vorkaufsrecht der Gemeinde online ausüben",
                beschreibung=None,
                position_darstellung=None,
            )
        ],
        parameter=[
            OnlinedienstParameter(
                parameter_name="datenschutz",
                parameter_wert="https://amt-lauenburgische-seen.de/impressum.html",
            ),
            OnlinedienstParameter(
                parameter_name="gebuehrensatzung",
                parameter_wert="{Link auf Gebührensatzung}",
            ),
            OnlinedienstParameter(
                parameter_name="impressum",
                parameter_wert="{Link auf Impressum}",
            ),
            OnlinedienstParameter(
                parameter_name="oeid",
                parameter_wert="9369271",
            ),
            OnlinedienstParameter(
                parameter_name="Zustellungskanal",
                parameter_wert="Kontaktsystem",
            ),
            OnlinedienstParameter(
                parameter_name="kontaktsystemTyp",
                parameter_wert="De-Mail",
            ),
            OnlinedienstParameter(
                parameter_name="kontaktsystemKennung",
                parameter_wert="ordnungsamt@amt-lauenburgische-seen.de",
            ),
            OnlinedienstParameter(
                parameter_name="kontaktsystemZusatz",
                parameter_wert=None,
            ),
        ],
        durchfuehrung_staatsangehoerigkeit=[],
        durchfuehrung_sprachen=[],
        zahlungsweise=[Zahlungsweise.VISA_KARTE],
        vertrauensniveau=Vertrauensniveau.NIEDRIG,
        identifizierungsmittel=[],
        logo=[],
        hilfe_link=[],
        hilfe_text=[],
        herausgeber=None,
        versionsinformationen=None,
        sprachversion=[
            Sprachversion(
                language_code=ALT_GERMAN,
                sprachbezeichnung_deutsch="Deutsch",
                sprachbezeichnung_nativ="Deutsch",
                erstellt_datum_zeit=None,
                erstellt_durch=None,
                geaendert_datum_zeit=None,
                geaendert_durch=None,
                version=None,
            )
        ],
        gueltigkeit=[],
    )
