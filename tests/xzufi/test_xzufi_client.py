from datetime import datetime, timedelta, timezone
import pytest

from fimportal import xzufi
from fimportal.xzufi.common import (
    ENGLISH,
    GERMAN,
    Identifikator,
    LanguageCode,
    StringLocalized,
    Versionsinformation,
)
from fimportal.xzufi.leistung import (
    Klassifizierung,
    Leistung,
    Leistungskategorie,
    LeistungskategorieKlasse,
    LeistungsTypisierung,
    LeistungsstrukturObjektMitVerrichtungUndDetail,
    ModulTextTyp,
    RawCode,
    Sprachversion,
    Textmodul,
)
from tests.data import XZUFI_2_2_0_DATA


class TestParseLeistungResponse:
    def test_should_correctly_parse_a_leistung_response(self):
        with open(
            XZUFI_2_2_0_DATA / "leistung_soap_response_success.xml", "rb"
        ) as file:
            response = file.read()

        leistungen = xzufi.parse_leistung_soap_response(response)

        assert len(leistungen) == 1
        leistung, _xml_content = leistungen[0]
        assert leistung == Leistung(
            id=Identifikator(
                scheme_agency_id="TSA_TELEPORT",
                scheme_agency_name="TSA Public Service GmbH",
                scheme_data_uri=None,
                scheme_id="TSA_LEIKA_OID",
                scheme_name="Leika Schul ObjektID",
                scheme_uri=None,
                scheme_version_id=None,
                value="100157500",
            ),
            id_sekundaer=[
                Identifikator(
                    scheme_agency_id="GK_LEIKA",
                    scheme_agency_name="GK LeiKa",
                    scheme_data_uri=None,
                    scheme_id="LEIKA_LEISTUNG_SCHLUESSEL",
                    scheme_name="LeiKa Leistungsschlüssel",
                    scheme_uri=None,
                    scheme_version_id="2",
                    value="99038009223001",
                ),
            ],
            struktur=LeistungsstrukturObjektMitVerrichtungUndDetail(
                leistungsgruppierung=Leistungskategorie(
                    id=Identifikator(
                        scheme_agency_id=None,
                        scheme_agency_name=None,
                        scheme_data_uri=None,
                        scheme_id="LEISTUNGSGRUPPIERUNG",
                        scheme_name=None,
                        scheme_uri=None,
                        scheme_version_id=None,
                        value="038",
                    ),
                    id_sekundaer=[],
                    uebergeordnete_kategorie_id=None,
                    untergeordnete_kategorie_id=[],
                    bezeichnung=[
                        StringLocalized(ENGLISH, "Compensation benefits"),
                        StringLocalized(GERMAN, "Entgeltersatzleistungen"),
                    ],
                    beschreibung=[],
                    klasse=LeistungskategorieKlasse(
                        id=Identifikator(
                            scheme_agency_id=None,
                            scheme_agency_name=None,
                            scheme_data_uri=None,
                            scheme_id=None,
                            scheme_name=None,
                            scheme_uri=None,
                            scheme_version_id=None,
                            value="LEISTUNGSGRUPPIERUNG",
                        ),
                        id_sekundaer=[],
                        beschreibung=[],
                        version=None,
                        bezeichnung=[
                            StringLocalized(GERMAN, "Individuelle Leistungsgruppierung")
                        ],
                    ),
                ),
                leistungsobjekt_id=Identifikator(
                    scheme_agency_id=None,
                    scheme_agency_name=None,
                    scheme_data_uri=None,
                    scheme_id=None,
                    scheme_name=None,
                    scheme_uri=None,
                    scheme_version_id=None,
                    value="967685",
                ),
                verrichtung=RawCode(
                    list_uri="urn:de:fim:leika:verrichtung",
                    list_version_id="20230523",
                    code="223",
                ),
                verrichtungsdetail=[
                    StringLocalized(ENGLISH, "For employers"),
                    StringLocalized(GERMAN, "Für Arbeitgeber"),
                ],
            ),
            typisierung=[LeistungsTypisierung.TYP_1],
            vertrauensniveau=None,
            kennzeichen_schritformerfordernis=None,
            referenz_leika=[],
            modul_text=[
                Textmodul(
                    position_darstellung=None,
                    id=None,
                    id_sekundaer=[],
                    gueltigkeit=[],
                    leika_textmodul=ModulTextTyp.LEISTUNGSBEZEICHNUNG,
                    leika_textmodul_abweichende_bezeichnung=[],
                    inhalt=[
                        StringLocalized(
                            GERMAN, "Altersteilzeit Vereinbarung Für Arbeitgeber"
                        )
                    ],
                    weiterfuehrender_link=[],
                )
            ],
            modul_text_individuell=[],
            modul_frist=None,
            modul_fachliche_freigabe=None,
            modul_kosten=None,
            modul_bearbeitungsdauer=None,
            modul_dokument=None,
            modul_auskunftshinweis=None,
            modul_ursprungsportal=[],
            informationsbereich_sdg=[],
            klassifizierung=[
                Klassifizierung(
                    list_uri="urn:xoev-de:fim:codeliste:pvlagen", value="2030700"
                )
            ],
            modul_begriff_im_kontext=[],
            kategorie=[],
            id_leistung_im_kontext=[],
            id_prozess=[],
            herausgeber=None,
            gueltigkeit_gebiet_id=[],
            kennzeichen_ea=False,
            relevant_fuer_wirtschaftszweig=[],
            relevant_fuer_rechtsform=[],
            relevant_fuer_staatsangehoerigkeit=[],
            versionsinformation=Versionsinformation(
                erstellt_datum_zeit=datetime(
                    2012, 11, 20, 8, 18, 24, tzinfo=timezone(timedelta(seconds=3600))
                ),
                erstellt_durch="Lück, Clemens",
                geaendert_datum_zeit=datetime(
                    2023,
                    5,
                    8,
                    14,
                    56,
                    9,
                    327000,
                    tzinfo=timezone(timedelta(seconds=7200)),
                ),
                geaendert_durch="Rose, Katja",
                version=None,
            ),
            sprachversion=[
                Sprachversion(
                    LanguageCode("de-DE"),
                    sprachbezeichnung_deutsch="Deutsch",
                    sprachbezeichnung_nativ="Deutsch",
                    erstellt_datum_zeit=datetime(
                        2017, 6, 7, 6, 11, 14, tzinfo=timezone(timedelta(seconds=7200))
                    ),
                    erstellt_durch="system",
                    geaendert_datum_zeit=datetime(
                        2017, 6, 9, 9, 23, 30, tzinfo=timezone(timedelta(seconds=7200))
                    ),
                    geaendert_durch="Administrator",
                    version=None,
                ),
                Sprachversion(
                    LanguageCode("en"),
                    sprachbezeichnung_deutsch="Englisch",
                    sprachbezeichnung_nativ="English",
                    erstellt_datum_zeit=datetime(
                        2021, 7, 7, 11, 12, 26, tzinfo=timezone(timedelta(seconds=7200))
                    ),
                    erstellt_durch="Administrator",
                    geaendert_datum_zeit=datetime(
                        2021, 4, 20, 11, 56, 3, tzinfo=timezone(timedelta(seconds=7200))
                    ),
                    geaendert_durch="Rose, Katja",
                    version=None,
                ),
            ],
            leistungsadressat=[],
            gueltigkeit=[],
        )

    def test_should_fail_for_invalid_leistung(self):
        with open(
            XZUFI_2_2_0_DATA / "leistung_soap_response_invalid_leistung.xml", "rb"
        ) as file:
            response = file.read()

        with pytest.raises(
            xzufi.XzufiException,
            match="Could not parse leitungs-response: 'invalid' is not a valid ModulTextTyp",
        ):
            xzufi.parse_leistung_soap_response(response)
