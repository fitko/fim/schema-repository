from fimportal.xzufi.common import Identifikator
from fimportal.xzufi.zustaendigkeit import (
    ZustaendigkeitOrganisationseinheit,
    Zustaendigkeitsrolle,
    parse_zustaendigkeit_variante,
)
from tests.data import XZUFI_2_2_0_DATA


def test_should_parse_an_organisationseinheit():
    data = (XZUFI_2_2_0_DATA / "zustaendigkeit.xml").read_text()

    zustaendigkeit = parse_zustaendigkeit_variante(data)

    assert zustaendigkeit == ZustaendigkeitOrganisationseinheit(
        id=Identifikator(
            scheme_agency_id="L100042",
            scheme_agency_name=None,
            scheme_data_uri=None,
            scheme_id="ZustaendigkeitOrganisationseinheit",
            scheme_name=None,
            scheme_uri=None,
            scheme_version_id=None,
            value="88162.44432.095750153000",
        ),
        leistung_id=[
            Identifikator(
                scheme_agency_id="L100042",
                scheme_agency_name=None,
                scheme_data_uri=None,
                scheme_id=None,
                scheme_name=None,
                scheme_uri=None,
                scheme_version_id=None,
                value="88162",
            )
        ],
        gebiet_id=[
            Identifikator(
                scheme_agency_id="L100042",
                scheme_agency_name=None,
                scheme_data_uri=None,
                scheme_id=None,
                scheme_name=None,
                scheme_uri=None,
                scheme_version_id=None,
                value="095750153000",
            )
        ],
        weitere_informationen=[],
        herausgeber=None,
        gueltigkeit=[],
        id_sekundaer=[],
        rolle=Zustaendigkeitsrolle.ZUSTAENDIGE_STELLE_UND_ANSPRECHPUNKT,
        kriterium=[],
    )
