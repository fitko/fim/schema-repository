import re

import pytest

from fimportal.xzufi.common import Leistungsschluessel, XzufiException


class TestLeistungsschluessel:
    @pytest.mark.parametrize(
        "leistungsschluessel",
        [
            "99001001000000",
            "05001001999000",
        ],
    )
    def test_should_not_fail_for_valid_leistungsschluessel(
        self, leistungsschluessel: str
    ):
        schluessel = Leistungsschluessel.from_str(leistungsschluessel)

        assert str(schluessel) == leistungsschluessel

    def test_fail_for_wrong_length(self):
        with pytest.raises(XzufiException, match="invalid length"):
            Leistungsschluessel.from_str("TMP_98001001000000")

    def test_fail_for_invalid_leistungsgruppierung(self):
        with pytest.raises(
            XzufiException,
            match=re.escape(
                "Invalid Leistungsschluessel: '000' not valid for 'Leistungsgruppierung' [value=99000001000000]"
            ),
        ):
            Leistungsschluessel.from_str("99000001000000")

    def test_fail_for_invalid_characters(self):
        with pytest.raises(
            XzufiException,
            match=re.escape(
                "Invalid Leistungsschluessel: invalid characters, only digits are allowed [value=98abc001000000]"
            ),
        ):
            Leistungsschluessel.from_str("98abc001000000")

    def test_fail_for_unknown_instanz(self):
        with pytest.raises(
            XzufiException,
            match=re.escape(
                "Invalid Leistungsschluessel: unknown 'Instanz' [value=98001001000000]"
            ),
        ):
            Leistungsschluessel.from_str("98001001000000")

    @pytest.mark.parametrize(
        "leistungsschluessel",
        [
            "99001001001999",
            "99001001999001",
            "99001999001001",
            "99999001001001",
        ],
    )
    def test_fail_for_invalid_instanz(self, leistungsschluessel: str):
        with pytest.raises(
            XzufiException,
            match=re.escape(
                f"Invalid Leistungsschluessel: invalid 'Instanz' [value={leistungsschluessel}]"
            ),
        ):
            Leistungsschluessel.from_str(leistungsschluessel)
