from fimportal.xzufi.common import Identifikator, StringLocalized
from fimportal.xzufi.leistung import (
    Amount,
    Kosten,
    KostenFix,
    Kostenmodul,
    Kostentyp,
)
from fimportal.xzufi.spezialisierung import (
    LeistungsmodulSpezialisierung,
    Spezialisierung,
    SpezialisierungEncoding,
)
from tests.data import XZUFI_2_2_0_DATA


def test_should_parse_a_spezialisierung():
    data = (XZUFI_2_2_0_DATA / "spezialisierung.xml").read_text()

    raw_spezialisierung = SpezialisierungEncoding.parse(data)

    assert raw_spezialisierung == Spezialisierung(
        id_leistung=Identifikator(
            scheme_agency_id="L100001",
            scheme_agency_name=None,
            scheme_data_uri=None,
            scheme_id="OID",
            scheme_name=None,
            scheme_uri=None,
            scheme_version_id=None,
            value="8967107",
        ),
        id_gebiet=[
            Identifikator(
                scheme_agency_id=None,
                scheme_agency_name=None,
                scheme_data_uri=None,
                scheme_id="urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:rs",
                scheme_name=None,
                scheme_uri=None,
                scheme_version_id="2022-09-30",
                value="064380002002",
            )
        ],
        bezeichnung=[],
        modul_spezialisiert=[
            LeistungsmodulSpezialisierung(
                modul=Kostenmodul(
                    position_darstellung=None,
                    id=None,
                    id_sekundaer=[],
                    gueltigkeit=[],
                    kosten=[
                        Kosten(
                            typ=Kostentyp.GEBUEHR,
                            kostenauswahl=KostenFix(
                                betrag=Amount(currency_code="EUR", value=207.30)
                            ),
                            beschreibung=[
                                StringLocalized(
                                    language_code="de",
                                    value="1100 l Container bei Leerung.",
                                )
                            ],
                            vorkasse=False,
                            link_kostenbildung=None,
                            position_darstellung=0,
                            gueltigkeit=[],
                        ),
                        Kosten(
                            typ=Kostentyp.GEBUEHR,
                            kostenauswahl=KostenFix(
                                betrag=Amount(currency_code="EUR", value=22.60)
                            ),
                            beschreibung=[
                                StringLocalized(
                                    language_code="de",
                                    value="120 l Tonne bei vierzehntäglicher Leerung.",
                                )
                            ],
                            vorkasse=False,
                            link_kostenbildung=None,
                            position_darstellung=0,
                            gueltigkeit=[],
                        ),
                    ],
                    beschreibung=[],
                    weiterfuehrender_link=[],
                ),
                ersetzung=False,
            )
        ],
        id=Identifikator(
            scheme_agency_id="L100001",
            scheme_agency_name=None,
            scheme_data_uri=None,
            scheme_id="OID",
            scheme_name=None,
            scheme_uri=None,
            scheme_version_id=None,
            value="10665799_8955200",
        ),
        id_sekundaer=[],
    )
