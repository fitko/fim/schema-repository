import re

import pytest

from fimportal.json_schema import JsonSchemaException, from_xdf3
from fimportal.xdatenfelder import xdf3
from tests.factories import (
    CodeListFactory,
    Xdf3Factory,
)


def test_should_convert_an_empty_schema():
    factory = Xdf3Factory()
    message = factory.schema(id="S12345", version="1.0.0").message()

    id = "test"
    json_schema = from_xdf3(id, message, {})

    assert json_schema == {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "$id": id,
        "title": message.schema.name,
        "type": "object",
        "properties": {},
        "required": [],
        "$defs": {},
    }


def test_should_fail_for_invalid_regex():
    factory = Xdf3Factory()
    field = (
        factory.field(id="F12345", version="1.0.0")
        .input(
            datentyp=xdf3.Datentyp.TEXT,
            praezisierung=xdf3.Praezisierung(
                pattern="([0]{1}[1-9]{1}|[1-9]{1}[0-9]{1})[0-9]{3})"
            ),
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    with pytest.raises(
        JsonSchemaException,
        match=re.escape(
            "Invalid regular expression: /([0]{1}[1-9]{1}|[1-9]{1}[0-9]{1})[0-9]{3})/: Unmatched ')'\n"
        ),
    ):
        from_xdf3("test", message, {})


def test_should_correctly_handle_required_elements():
    factory = Xdf3Factory()
    group = factory.group(id="G12345", version="1.0.0").build()
    message = factory.schema().with_group(group, anzahl=xdf3.Anzahl(1, 1)).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["properties"] == {
        "G12345V1.0.0": {"$ref": "#/$defs/G12345V1.0.0"}
    }
    assert json_schema["required"] == ["G12345V1.0.0"]


def test_should_correctly_handle_optional_elements():
    factory = Xdf3Factory()
    group = factory.group(id="G12345", version="1.0.0").build()
    message = factory.schema().with_group(group, anzahl=xdf3.Anzahl(0, 1)).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["properties"] == {
        "G12345V1.0.0": {"$ref": "#/$defs/G12345V1.0.0"}
    }
    assert json_schema["required"] == []


def test_should_correctly_handle_limited_array_elements():
    factory = Xdf3Factory()
    group = factory.group(id="G12345", version="1.0.0").build()
    message = factory.schema().with_group(group, anzahl=xdf3.Anzahl(0, 5)).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["properties"] == {
        "G12345V1.0.0": {
            "type": "array",
            "items": {
                "$ref": "#/$defs/G12345V1.0.0",
            },
            "minItems": 0,
            "maxItems": 5,
        }
    }
    assert json_schema["required"] == ["G12345V1.0.0"]


def test_should_correctly_handle_unlimited_array_elements():
    factory = Xdf3Factory()
    group = factory.group(id="G12345", version="1.0.0").build()
    message = factory.schema().with_group(group, anzahl=xdf3.Anzahl(0, None)).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["properties"] == {
        "G12345V1.0.0": {
            "type": "array",
            "items": {
                "$ref": "#/$defs/G12345V1.0.0",
            },
            "minItems": 0,
        }
    }
    assert json_schema["required"] == ["G12345V1.0.0"]


def test_should_create_group_definitions():
    factory = Xdf3Factory()
    group = factory.group(id="G12345", version="1.0.0").build()
    message = factory.schema().with_group(group).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["$defs"] == {
        "G12345V1.0.0": {
            "title": group.name,
            "type": "object",
            "properties": {},
            "required": [],
        }
    }


def test_should_convert_text_inputs():
    factory = Xdf3Factory()
    field = (
        factory.field(id="F12345", version="1.0.0")
        .input(
            xdf3.Datentyp.TEXT,
            praezisierung=xdf3.Praezisierung(
                min_length=1,
                max_length=10,
                pattern="[A-Z]+",
                min_value="ignore",
                max_value="ignore",
            ),
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["$defs"]["F12345V1.0.0"] == {
        "title": field.name,
        "type": "string",
        "minLength": 1,
        "maxLength": 10,
        "pattern": "[A-Z]+",
    }


def test_should_convert_string_latin_inputs():
    factory = Xdf3Factory()
    field = (
        factory.field(id="F12345", version="1.0.0")
        .input(
            xdf3.Datentyp.STRING_LATIN,
            praezisierung=xdf3.Praezisierung(
                min_length=1,
                max_length=10,
                pattern="[A-Z]+",
                min_value="ignore",
                max_value="ignore",
            ),
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["$defs"]["F12345V1.0.0"] == {
        "title": field.name,
        "type": "string",
        "minLength": 1,
        "maxLength": 10,
        "pattern": "[A-Z]+",
    }


def test_should_convert_date_inputs():
    factory = Xdf3Factory()
    field = (
        factory.field(id="F12345", version="1.0.0")
        .input(
            xdf3.Datentyp.DATUM,
            praezisierung=xdf3.Praezisierung(
                min_length=1,
                max_length=10,
                pattern="[A-Z]+",
                min_value="ignore",
                max_value="ignore",
            ),
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["$defs"]["F12345V1.0.0"] == {
        "title": field.name,
        "type": "string",
        "format": "date",
    }


def test_should_convert_time_inputs():
    factory = Xdf3Factory()
    field = (
        factory.field(id="F12345", version="1.0.0")
        .input(
            xdf3.Datentyp.ZEIT,
            praezisierung=xdf3.Praezisierung(
                min_length=1,
                max_length=10,
                pattern="[A-Z]+",
                min_value="ignore",
                max_value="ignore",
            ),
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["$defs"]["F12345V1.0.0"] == {
        "title": field.name,
        "type": "string",
        "format": "time",
    }


def test_should_convert_datetime_inputs():
    factory = Xdf3Factory()
    field = (
        factory.field(id="F12345", version="1.0.0")
        .input(
            xdf3.Datentyp.ZEITPUNKT,
            praezisierung=xdf3.Praezisierung(
                min_length=1,
                max_length=10,
                pattern="[A-Z]+",
                min_value="ignore",
                max_value="ignore",
            ),
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["$defs"]["F12345V1.0.0"] == {
        "title": field.name,
        "type": "string",
        "format": "date-time",
    }


def test_should_convert_bool_inputs():
    factory = Xdf3Factory()
    field = (
        factory.field(id="F12345", version="1.0.0")
        .input(
            xdf3.Datentyp.WAHRHEITSWERT,
            praezisierung=xdf3.Praezisierung(
                min_length=1,
                max_length=10,
                pattern="[A-Z]+",
                min_value="ignore",
                max_value="ignore",
            ),
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["$defs"]["F12345V1.0.0"] == {
        "title": field.name,
        "type": "boolean",
    }


def test_should_convert_numeric_inputs():
    factory = Xdf3Factory()
    field = (
        factory.field(id="F12345", version="1.0.0")
        .input(
            xdf3.Datentyp.NUMMER,
            praezisierung=xdf3.Praezisierung(
                min_length=1,
                max_length=10,
                pattern="[A-Z]+",
                min_value="1",
                max_value="2",
            ),
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["$defs"]["F12345V1.0.0"] == {
        "title": field.name,
        "type": "number",
        "minimum": 1.0,
        "maximum": 2.0,
    }


def test_should_fail_for_invalid_numeric_limits():
    factory = Xdf3Factory()
    field = (
        factory.field(id="F12345", version="1.0.0")
        .input(
            xdf3.Datentyp.NUMMER,
            praezisierung=xdf3.Praezisierung(
                min_length=1,
                max_length=10,
                pattern="[A-Z]+",
                min_value="not-a-number",
                max_value="not-a-number",
            ),
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    with pytest.raises(
        JsonSchemaException,
        match=re.escape("Invalid numeric limit 'not-a-number' in field 'F12345V1.0.0'"),
    ):
        from_xdf3("test", message, {})


def test_should_convert_integer_inputs():
    factory = Xdf3Factory()
    field = (
        factory.field(id="F12345", version="1.0.0")
        .input(
            xdf3.Datentyp.GANZZAHL,
            praezisierung=xdf3.Praezisierung(
                min_length=1,
                max_length=10,
                pattern="[A-Z]+",
                min_value="1",
                max_value="2",
            ),
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["$defs"]["F12345V1.0.0"] == {
        "title": field.name,
        "type": "integer",
        "minimum": 1.0,
        "maximum": 2.0,
    }


def test_should_convert_currency_inputs():
    factory = Xdf3Factory()
    field = (
        factory.field(id="F12345", version="1.0.0")
        .input(
            xdf3.Datentyp.GELDBETRAG,
            praezisierung=xdf3.Praezisierung(
                min_length=1,
                max_length=10,
                pattern="[A-Z]+",
                min_value="1",
                max_value="2",
            ),
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["$defs"]["F12345V1.0.0"] == {
        "title": field.name,
        "type": "number",
        "multipleOf": 0.01,
        "minimum": 1.0,
        "maximum": 2.0,
    }


def test_should_convert_file_inputs():
    factory = Xdf3Factory()
    field = (
        factory.field(id="F12345", version="1.0.0")
        .input(
            xdf3.Datentyp.ANLAGE,
            praezisierung=xdf3.Praezisierung(
                min_length=1,
                max_length=10,
                pattern="[A-Z]+",
                min_value="1",
                max_value="2",
            ),
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["$defs"]["F12345V1.0.0"] == {
        "title": field.name,
        "type": "string",
    }


def test_should_convert_object_inputs():
    factory = Xdf3Factory()
    field = (
        factory.field(id="F12345", version="1.0.0")
        .input(
            xdf3.Datentyp.OBJEKT,
            praezisierung=xdf3.Praezisierung(
                min_length=1,
                max_length=10,
                pattern="[A-Z]+",
                min_value="1",
                max_value="2",
            ),
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["$defs"]["F12345V1.0.0"] == {
        "title": field.name,
        "type": "string",
    }


def test_should_convert_select_with_external_code_list():
    code_list = (
        CodeListFactory(default_code_key="code")
        .add_column("code", ["000", "001"])
        .build()
    )
    factory = Xdf3Factory()
    field = (
        factory.field(id="F12345", version="1.0.0").select(code_list=code_list).build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf3(
        "test",
        message,
        {code_list.identifier.canonical_version_uri: code_list},
    )

    assert json_schema["$defs"]["F12345V1.0.0"] == {
        "title": field.name,
        "type": "string",
        "enum": ["000", "001"],
    }


def test_should_use_code_key_if_set():
    code_list = CodeListFactory().add_column("alt_code", ["a", "b"]).build()
    factory = Xdf3Factory()
    field = (
        factory.field(id="F12345", version="1.0.0")
        .select(code_list=code_list, code_key="alt_code")
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf3(
        "test",
        message,
        {code_list.identifier.canonical_version_uri: code_list},
    )

    assert json_schema["$defs"]["F12345V1.0.0"] == {
        "title": field.name,
        "type": "string",
        "enum": ["a", "b"],
    }


def test_should_convert_select_with_embedded_code_list():
    factory = Xdf3Factory()
    field = (
        factory.field(id="F12345", version="1.0.0")
        .select(
            werte=[
                xdf3.ListenWert(code="000", name="Label 1", hilfe=None),
                xdf3.ListenWert(code="001", name="Label 2", hilfe=None),
            ]
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["$defs"]["F12345V1.0.0"] == {
        "title": field.name,
        "type": "string",
        "enum": ["000", "001"],
    }


def test_should_fail_for_missing_code_list_data():
    factory = Xdf3Factory()
    field = factory.field(id="F12345", version="1.0.0").select().build()
    message = factory.schema().with_field(field).message()

    with pytest.raises(
        JsonSchemaException,
        match=re.escape(
            "Missing code list information for select field 'F12345V1.0.0'"
        ),
    ):
        from_xdf3("test", message, {})


def test_should_ignore_select_data_type():
    factory = Xdf3Factory()
    field = (
        factory.field(id="F12345", version="1.0.0")
        .select(
            datentyp=xdf3.Datentyp.WAHRHEITSWERT,
            werte=[
                xdf3.ListenWert(code="true", name="Label 1", hilfe=None),
                xdf3.ListenWert(code="false", name="Label 2", hilfe=None),
            ],
        )
        .build()
    )
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["$defs"]["F12345V1.0.0"]["type"] == "string"


def test_should_ignore_static_fields():
    factory = Xdf3Factory()
    field = factory.field(id="F12345", version="1.0.0").label().build()
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["$defs"] == {}
    assert json_schema["properties"] == {}


def test_should_ignore_blocked_fields():
    factory = Xdf3Factory()
    field = factory.field(id="F12345", version="1.0.0").blocked().build()
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["$defs"] == {}
    assert json_schema["properties"] == {}


def test_should_ignore_hidden_fields():
    factory = Xdf3Factory()
    field = factory.field(id="F12345", version="1.0.0").hidden().build()
    message = factory.schema().with_field(field).message()

    json_schema = from_xdf3("test", message, {})

    assert json_schema["$defs"] == {}
    assert json_schema["properties"] == {}
