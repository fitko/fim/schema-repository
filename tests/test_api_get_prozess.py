from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.factories.prozess import ProzessFactory
from tests.factories.xdf2 import Xdf2SteckbriefImportFactory
from tests.factories.xzufi import LeistungFactory

ENDPOINT = "/api/v0/processes"


def test_get_prozess(runner: CommandRunner, client: TestClient):
    id = "some prozess id"
    prozess = ProzessFactory(id=id).save(runner)

    response = client.get(f"{ENDPOINT}/{id}")

    assert response.status_code == 200
    assert response.json() == {
        "id": prozess.id,
        "version": prozess.version,
        "name": prozess.name,
        "freigabe_status": prozess.freigabe_status,
        "letzter_aenderungszeitpunkt": None,
        "dokumentsteckbriefe": {},
        "leistungssteckbrief": {
            "leistungsschluessel": prozess.id,
            "name": None,
        },
        "prozessklasse": None,
    }


def test_get_prozess_not_found(client: TestClient):
    response = client.get(f"{ENDPOINT}/some-id")

    assert response.status_code == 404


class TestLeistungsteckbriefReferences:
    def test_should_save_references_to_leistungssteckbrief_based_on_the_id(
        self, runner: CommandRunner, client: TestClient
    ):
        prozess = ProzessFactory(id="99123000000000").save(runner)

        response = client.get(f"{ENDPOINT}/{prozess.id}")

        assert response.status_code == 200
        assert response.json()["leistungssteckbrief"] == {
            "leistungsschluessel": "99123000000000",
            "name": None,
        }

    def test_should_include_the_title_of_the_steckbrief_if_available(
        self, runner: CommandRunner, client: TestClient
    ):
        leistungsschluesssel = "99123000000000"
        leistungssteckbrief = (
            LeistungFactory()
            .steckbrief(
                leistungsschluessel=leistungsschluesssel,
            )
            .save_leika(runner)
        )
        prozess = ProzessFactory(id=leistungsschluesssel).save(runner)

        response = client.get(f"{ENDPOINT}/{prozess.id}")

        assert response.status_code == 200
        assert response.json()["leistungssteckbrief"] == {
            "leistungsschluessel": leistungsschluesssel,
            "name": leistungssteckbrief.title,
        }


class TestDokumentsteckbriefReferences:
    def test_should_save_references_to_dokumentsteckbriefe(
        self, runner: CommandRunner, client: TestClient
    ):
        prozess = (
            ProzessFactory()
            .add_ausloeser("D1234501")
            .add_ergebnis("D1234502")
            .save(runner)
        )

        response = client.get(f"{ENDPOINT}/{prozess.id}")

        assert response.status_code == 200
        assert response.json()["dokumentsteckbriefe"] == {
            "D1234501": {
                "fim_id": "D1234501",
                "rolle": "ausloeser",
                "name": None,
            },
            "D1234502": {
                "fim_id": "D1234502",
                "rolle": "ergebnis",
                "name": None,
            },
        }

    def test_should_include_name_of_dokumentsteckbrief_if_available(
        self, runner: CommandRunner, client: TestClient
    ):
        ausloeser = Xdf2SteckbriefImportFactory().save(runner)
        prozess = ProzessFactory().add_ausloeser(ausloeser.fim_id).save(runner)

        response = client.get(f"{ENDPOINT}/{prozess.id}")

        assert response.status_code == 200
        assert response.json()["dokumentsteckbriefe"] == {
            ausloeser.fim_id: {
                "fim_id": ausloeser.fim_id,
                "rolle": "ausloeser",
                "name": ausloeser.name,
            },
        }

    def test_should_use_latest_references_to_dokumentsteckbriefe(
        self, runner: CommandRunner, client: TestClient
    ):
        Xdf2SteckbriefImportFactory("D12345", "1.0", name="old").save(runner)
        Xdf2SteckbriefImportFactory("D12345", "2.0", name="new").save(runner)
        prozess = ProzessFactory().add_ausloeser("D12345").save(runner)

        response = client.get(f"{ENDPOINT}/{prozess.id}")

        assert response.status_code == 200
        assert response.json()["dokumentsteckbriefe"] == {
            "D12345": {
                "fim_id": "D12345",
                "rolle": "ausloeser",
                "name": "new",
            },
        }
