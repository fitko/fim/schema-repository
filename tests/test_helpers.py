import pytest

from fimportal.helpers import (
    extract_xzufi_rechtsgrundlagen_lines,
    get_latest_version,
    sort_versions,
    ts_query_param,
)
from fimportal.service import Service


def test_should_correctly_return_latest_version():
    assert get_latest_version(["1.1", "1.0", "1.2"]) == "1.2"

    assert get_latest_version(["1.9", "1.10", "1.8"]) == "1.10"

    assert get_latest_version(["11.1", "1.11"]) == "11.1"

    assert get_latest_version(["2.0", "1.1"]) == "2.0"


def test_should_correctly_sort_versions():
    assert sort_versions(["1.1", "1.0", "1.2"]) == ["1.0", "1.1", "1.2"]

    assert sort_versions(["1.9", "1.10", "1.8"]) == ["1.8", "1.9", "1.10"]

    assert sort_versions(["11.1", "1.11"]) == ["1.11", "11.1"]

    assert sort_versions(["2.0", "1.1"]) == ["1.1", "2.0"]


class TestExtractXzufiRechtsgrundlagenLines:
    def test_should_strip_whitespace(self):
        text = "         content      "

        assert extract_xzufi_rechtsgrundlagen_lines(text) == ["content"]

    def test_should_remove_empty_lines(self):
        text = """
            <ul> 
             <li> 
              <a href="https://admincenter.service-bw.de/l/17077"></a> 
             </li> 
            </ul>
        """

        assert extract_xzufi_rechtsgrundlagen_lines(text) == []

    def test_should_remove_excess_whitespace(self):
        text = """
            <ul> 
             <li> 
              <a href="https://admincenter.service-bw.de/l/17077">§ 2 Nr. 22 Fahrzeug-Zulassungsverordnung (FZV) (Begriffsbestimmungen)</a> 
             </li> 
             <li> 
              <a href="https://admincenter.service-bw.de/l/18811">§ 9 Fahrzeug-Zulassungsverordnung (FZV) (Besondere Kennzeichen)</a> 
             </li> 
            </ul>
        """

        assert extract_xzufi_rechtsgrundlagen_lines(text) == [
            "§ 2 Nr. 22 Fahrzeug-Zulassungsverordnung (FZV) (Begriffsbestimmungen)",
            "§ 9 Fahrzeug-Zulassungsverordnung (FZV) (Besondere Kennzeichen)",
        ]


@pytest.mark.parametrize(
    "query,result",
    [
        ("Term", "Term:*"),
        ("Term ", "Term:*"),
        ("A     B", "A:* & B:*"),
        ("A\nB", "A:* & B:*"),
        ("A\tB", "A:* & B:*"),
        ("& !", "\\&:* & \\!:*"),
        ('""', None),
        ('"', None),
        ('"A B"', "(A <-> B)"),
        ('"A B" C', "(A <-> B) & C:*"),
        ('C "A B"', "C:* & (A <-> B)"),
        ('"A &"', "(A <-> \\&)"),
        ("<->", "\\<->:*"),
        ("<N>", "\\<N>:*"),
        ("@>", "@>:*"),
        ("<@", "\\<@:*"),
        ("\\", "\\\\:*"),
        ("/", "/:*"),
        ('\\"\\"', "\\\\:* & (\\\\)"),
        (
            "Hello <xml-tag> & paragraph §§",
            "Hello:* & \\<xml-tag>:* & \\&:* & paragraph:* & §§:*",
        ),
    ],
)
def test_should_correctly_create_ts_query_parameters(query: str, result: str | None):
    assert ts_query_param(query) == result


@pytest.mark.asyncio(loop_scope="session")
@pytest.mark.parametrize(
    "search_input,ts_query",
    [
        # Explicitly wanted behavior
        ("Term", "'term':*"),
        ("A B", "'a':* & 'b':*"),
        ("", None),
        (" ", None),
        ('A "B C" D', "'a':* & 'b' <-> 'c' & 'd':*"),
        # Behavior of operators in input
        ("Term:*", "'term':*"),
        ("A:* & B:*", "'a':* & 'b':*"),
        ("(A <-> B)", "'a':* & 'b':*"),
        ("(A <-> B) & C:*", "'a':* & 'b':* & 'c':*"),
        ("C:* & (A <-> B)", "'c':* & 'a':* & 'b':*"),
        ("A <-> B", "'a':* & 'b':*"),
        ("A <-> B:*", "'a':* & 'b':*"),
        ("A & B", "'a':* & 'b':*"),
        ("A ! B", "'a':* & 'b':*"),
        ("A | B", "'a':* & 'b':*"),
        ("A <2> B", "'a':* & '2':* & 'b':*"),
        ("A <@ B", "'a':* & 'b':*"),
        ("A @> B", "'a':* & 'b':*"),
        # Behavior of special characters in input
        ("A$B", "'a':* <-> 'b':*"),
        ("A.B", "'a.b':*"),
        ("A/B", "'a/b':*"),
        ("A .B", "'a':* & 'b':*"),
        ("\\&:* & \\!:*", ""),
        ("(A <-> &)", "'a':*"),
        ("@>:*", ""),
        ("<@:*", ""),
        ("\\:*", ""),
        ("/:*", ""),
        ("\\:* & (\\)", ""),
        ("<A\\>", "'a':*"),
        ("?", ""),
        (".", ""),
        ("\\A", "'a':*"),
        # Behavior of double quotes in input
        ('"   "', None),
        ('" $ "', ""),
        ('A "" B', "'a':* & 'b':*"),
        ("""A "&./.§:*'x" B""", "'a':* & 'x' & 'b':*"),
        ('A "B', "'a':* & 'b'"),
        # Behavior of single quotes in input
        ("'   '", ""),
        ("' $ '", ""),
        ("A & '   ' & B", "'a':* & 'b':*"),
        ("A & ' $ ' & B", "'a':* & 'b':*"),
        ("Der hat's in sich.", "'s':*"),
        (
            "Dern hatn's inn sichn.",
            "'dern':* & 'hatn':* <-> 's':* & 'inn':* & 'sichn':*",
        ),
        # Curious cases
        ("A$B:*", "'a':* <-> 'b':*"),
        ("A:B:*", "'a':* <-> 'b':*"),
        ("<?>", ""),
        ("<2>", "'2':*"),
        ("<WORD>", ""),
        ("<WORD2>", ""),
        ("<WORD?>", "'word':*"),
        (
            "Hello <xml-tag> paragraph §§",
            "'hello':* & 'paragraph':*",
        ),
        ('A "A <-> B" B', "'a':* & 'a' <2> 'b' & 'b':*"),
        ('"A <-> B"', "'a' <2> 'b'"),
    ],
)
async def test_should_create_correct_ts_query(
    search_input: str, ts_query: str | None, service: Service
):
    database = service._database  # type: ignore

    parameter = ts_query_param(search_input)

    if parameter is None:
        result = None
    else:
        result = await database.connection.fetchval(
            """SELECT to_tsquery('german', $1);""", parameter
        )

    assert result == ts_query
