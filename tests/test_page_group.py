from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.factories.xdf3 import (
    Xdf3Factory,
)


def test_load_group_page(client: TestClient, runner: CommandRunner) -> None:
    factory = Xdf3Factory()
    field = factory.field().build()
    group = factory.group().with_field(field).build()
    factory.schema().with_group(group).save(runner)

    response = client.get(
        f"/groups/baukasten/{group.identifier.id}/{group.identifier.version}"
    )

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_load_group_structure_page(client: TestClient, runner: CommandRunner) -> None:
    factory = Xdf3Factory()
    field = factory.field().build()
    group = factory.group().with_field(field).build()
    factory.schema().with_group(group).save(runner)

    response = client.get(
        f"/groups/baukasten/{group.identifier.id}/{group.identifier.version}/structure"
    )

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_load_group_references_page(client: TestClient, runner: CommandRunner) -> None:
    factory = Xdf3Factory()
    field = factory.field().build()
    group = factory.group().with_field(field).build()
    factory.schema().with_group(group).save(runner)

    response = client.get(
        f"/groups/baukasten/{group.identifier.id}/{group.identifier.version}/references"
    )

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_unknown_group(client: TestClient) -> None:
    response = client.get("/groups/baukasten/G1234/1.0")

    assert response.status_code == 404
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert "Datenfeldgruppe G1234 v1.0 konnte nicht gefunden werden." in response.text
