from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.factories import EXAMPLE_CODE_LIST
from tests.factories.genericode import CodeListImportFactory
from tests.factories.xdf2 import Xdf2Factory


def test_should_return_the_original_code_list(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()

    code_list_text = EXAMPLE_CODE_LIST.replace("Anschrift", "something original")
    code_list = CodeListImportFactory(code_list_text=code_list_text).build()

    field = factory.field(id="F12345").select(code_list=code_list).build()

    schema = (
        factory.schema(id="S12345")
        .with_field(field)
        .save(runner, code_lists=[code_list])
    )
    response = client.get(
        f"/immutable/code-lists/{schema.uploads[0].code_lists[0].id}/genericode.xml"
    )

    assert response.status_code == 200
    assert response.headers["content-type"] == "application/xml"
    assert (
        response.headers["content-disposition"]
        == f'attachment; filename="code_list_{schema.uploads[0].code_lists[0].id}_genericode.xml"'
    )

    content = response.read().decode("utf-8")
    assert content == code_list_text


def test_should_fail_for_unknown_code_list_id(
    runner: CommandRunner, client: TestClient
):
    Xdf2Factory().schema().save(runner)

    response = client.get("/immutable/code-lists/1/genericode.xml")

    assert response.status_code == 404
    assert response.json() == {"detail": "Could not find code list 1"}
