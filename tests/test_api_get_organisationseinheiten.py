from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.factories.xzufi import OrganisationseinheitFactory

ENDPOINT = "/api/v0/organizational-unit"


def test_return_all_organisationseinheiten(runner: CommandRunner, client: TestClient):
    orga_einheit = OrganisationseinheitFactory().save(runner)

    response = client.get(ENDPOINT)

    assert response.status_code == 200
    assert response.json() == {
        "items": [
            {
                "redaktion_id": orga_einheit.redaktion_id,
                "id": orga_einheit.id,
            }
        ],
        "count": 1,
        "offset": 0,
        "limit": 200,
        "total_count": 1,
    }
