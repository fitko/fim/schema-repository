from typing import AsyncContextManager, Callable
from asyncpg import Pool
import io
import pytest
import zipfile

from fimportal.cli.commands.import_kataloge import sync_kataloge
from fimportal.kataloge import FakeClient, XzufiKatalogIdentifier
from fimportal.service import Service


@pytest.mark.asyncio(loop_scope="session")
async def test_should_add_new_katalog_files(
    get_service: Callable[[], AsyncContextManager[Service]],
    async_db_pool: Pool,
):
    client = FakeClient()
    filename = XzufiKatalogIdentifier.LEIKA_PLUS_DE.get_source_filename()
    content = b"content"

    client.add_kataloge(filename, content)

    await sync_kataloge(client, async_db_pool, "localhost")

    async with get_service() as service:
        saved_content = await service.get_katalog_content(f"{filename}.zip")
        assert saved_content is not None
        assert _unzip_content(saved_content) == content


@pytest.mark.asyncio(loop_scope="session")
async def test_should_update_a_katalog_files(
    get_service: Callable[[], AsyncContextManager[Service]],
    async_db_pool: Pool,
):
    client = FakeClient()
    filename = XzufiKatalogIdentifier.LEIKA_PLUS_DE.get_source_filename()

    client.add_kataloge(filename, b"before.csv")

    await sync_kataloge(client, async_db_pool, "localhost")
    client.clear()

    client.add_kataloge(filename, b"after.csv")
    await sync_kataloge(client, async_db_pool, "localhost")

    async with get_service() as service:
        saved_content = await service.get_katalog_content(f"{filename}.zip")
        assert saved_content is not None
        assert _unzip_content(saved_content) == b"after.csv"


@pytest.mark.asyncio(loop_scope="session")
async def test_should_retain_unavailable_downloads(
    get_service: Callable[[], AsyncContextManager[Service]],
    async_db_pool: Pool,
):
    client = FakeClient()
    filename = XzufiKatalogIdentifier.LEIKA_PLUS_DE.get_source_filename()
    content = b"content"

    client.add_kataloge(filename, content)

    await sync_kataloge(client, async_db_pool, "localhost")
    client.clear()
    await sync_kataloge(client, async_db_pool, "localhost")

    async with get_service() as service:
        saved_content = await service.get_katalog_content(f"{filename}.zip")
        assert saved_content is not None
        assert _unzip_content(saved_content) == content


def _unzip_content(zipped_content: bytes) -> bytes:
    zip_buffer = io.BytesIO(zipped_content)
    with zipfile.ZipFile(zip_buffer, "r") as zip_file:
        return zip_file.read(zip_file.namelist()[0])
