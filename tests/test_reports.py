import pytest

from fimportal import genericode
from fimportal.xdatenfelder import reports, xdf2, xdf3
from tests.factories import (
    CodeListFactory,
    Xdf3Factory,
)
from tests.factories.xdf2 import Xdf2Factory


class TestXdf2Group:
    def test_should_show_no_warnings_for_correct_group(self):
        group = Xdf2Factory().group().valid().build()

        report = reports.check_xdf2_group(group)

        assert len(report.failing_checks) == 0

    def test_missing_bezeichnung_eingabe(self):
        group = Xdf2Factory().group().valid().with_bezeichnung_eingabe(None).build()

        report = reports.check_xdf2_group(group)

        assert len(report.failing_checks) == 1
        assert (
            report.failing_checks[0].message
            == "`bezeichnungEingabe` sollte nicht leer sein."
        )

    def test_no_child_elements(self):
        group = Xdf2Factory().group().valid().remove_children().build()

        report = reports.check_xdf2_group(group)

        assert len(report.failing_checks) == 1
        assert (
            report.failing_checks[0].message
            == "Datenfeldgruppe hat keine Unterelemente."
        )


class TestXdf2Field:
    def test_should_show_no_warnings_for_correct_field(self):
        field = Xdf2Factory().field().valid().build()

        report = reports.check_xdf2_field(field, {})

        assert len(report.failing_checks) == 0

    def test_missing_bezeichnung_eingabe(self):
        field = Xdf2Factory().field().valid().with_bezeichnunug_eingabe(None).build()

        report = reports.check_xdf2_field(field, {})

        assert len(report.failing_checks) == 1
        assert (
            report.failing_checks[0].message
            == "`bezeichnungEingabe` sollte nicht leer sein."
        )

    def test_invalid_select_data_type(self):
        code_list = CodeListFactory().build()
        field = (
            Xdf2Factory()
            .field()
            .valid()
            .select(datentyp=xdf2.Datentyp.WAHRHEITSWERT, code_list=code_list)
            .build()
        )

        report = reports.check_xdf2_field(
            field, {code_list.identifier.canonical_version_uri: code_list}
        )

        assert len(report.failing_checks) == 1
        assert (
            report.failing_checks[0].message
            == "SELECT Felder sollten den Datentyp `text` haben, nicht `bool`."
        )

    def test_missing_code_lists(self):
        field = Xdf2Factory().field().valid().select().with_code_list(None).build()

        report = reports.check_xdf2_field(field, {})

        assert len(report.failing_checks) == 1
        assert report.failing_checks[0].code == 1018

    @pytest.mark.parametrize("canonical_uri", ["de:ä", "de:ö", "de:ü"])
    def test_invalid_code_list_chars(self, canonical_uri: str):
        code_list = CodeListFactory(canonical_uri=canonical_uri).build()
        field = Xdf2Factory().field().valid().select(code_list=code_list).build()

        report = reports.check_xdf2_field(
            field, {code_list.identifier.canonical_version_uri: code_list}
        )

        assert len(report.failing_checks) == 1
        assert (
            report.failing_checks[0].message
            == f"Codelistenreferenzen dürfen keine Umlaute enthalten: {canonical_uri}."
        )

    @pytest.mark.parametrize(
        "field",
        [
            Xdf2Factory()
            .field()
            .valid()
            .label()
            .with_praezisierung("some value")
            .build(),
            Xdf2Factory()
            .field()
            .valid()
            .select()
            .with_praezisierung("some value")
            .build(),
        ],
    )
    def test_unused_praezisierung(self, field: xdf2.Datenfeld):
        code_list_map = {}
        if field.code_listen_referenz:
            code_list_map[
                field.code_listen_referenz.genericode_identifier.canonical_version_uri
            ] = CodeListFactory().build()

        report = reports.check_xdf2_field(field, code_list_map)

        assert len(report.failing_checks) == 1
        assert (
            report.failing_checks[0].message
            == "`praezisierung` wird ignoriert bei LABEL und SELECT Feldern"
        )

    @pytest.mark.parametrize(
        "field",
        [
            Xdf2Factory()
            .field()
            .valid()
            .label()
            .with_code_list(CodeListFactory().build())
            .build(),
            Xdf2Factory()
            .field()
            .valid()
            .input()
            .with_code_list(CodeListFactory().build())
            .build(),
        ],
    )
    def test_unused_code_list_referenz(self, field: xdf2.Datenfeld):
        code_list_map = {}
        if field.code_listen_referenz:
            code_list_map[
                field.code_listen_referenz.genericode_identifier.canonical_version_uri
            ] = CodeListFactory().build()
        report = reports.check_xdf2_field(field, code_list_map)

        assert len(report.failing_checks) == 1
        assert (
            report.failing_checks[0].message
            == "`codelisteReferenz` wird ignoriert bei LABEL und INPUT Feldern."
        )


class TestXdf3Schema:
    def test_should_show_warnings_for_correct_schema(self):
        schema = Xdf3Factory().schema().message()

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 0

    def test_element_versions(self):
        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_rule(factory.rule(id="R000001", version="1.0.0").build())
            .with_rule(factory.rule(id="R000001", version="1.0.1").build())
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 1
        assert report.schema_checks[0].code == 1003
        assert report.schema_checks[0].error_type == "warning"

    def test_feldart_for_wertelist_and_codelist(self):
        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_field(
                factory.field(
                    feldart=xdf3.Feldart.EINGABE,
                    werte=[
                        xdf3.ListenWert("1", "name", None),
                        xdf3.ListenWert("2", "name", None),
                    ],
                ).build()
            )
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 1
        assert report.field_reports[0].failing_checks[0].code == 1017

    def test_missing_codelist(self):
        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_field(
                factory.field(
                    feldart=xdf3.Feldart.AUSWAHL, codeliste_referenz=None, inhalt=None
                ).build()
            )
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 2
        assert report.field_reports[0].failing_checks[1].code == 1018
        assert report.field_reports[0].failing_checks[1].error_type == "warning"

    def test_codelist_versions(self):
        code_list_identifiers = (
            genericode.Identifier(
                canonical_uri="URI",
                canonical_version_uri="1",
                version="2020-01-01",
            ),
            genericode.Identifier(
                canonical_uri="URI",
                canonical_version_uri="1",
                version="2020-01-02",
            ),
        )

        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_field(
                factory.field(
                    feldart=xdf3.Feldart.AUSWAHL,
                    datentyp=xdf3.Datentyp.TEXT,
                    praezisierung=xdf3.Praezisierung(min_length=1, max_length=2),
                    codeliste_referenz=code_list_identifiers[0],
                ).build()
            )
            .with_field(
                factory.field(
                    feldart=xdf3.Feldart.AUSWAHL,
                    datentyp=xdf3.Datentyp.TEXT,
                    praezisierung=xdf3.Praezisierung(min_length=1, max_length=2),
                    codeliste_referenz=code_list_identifiers[1],
                ).build()
            )
            .message()
        )

        code_list_map = {
            code_list_identifiers[0].canonical_version_uri: CodeListFactory().build(),
            code_list_identifiers[1].canonical_version_uri: CodeListFactory().build(),
        }

        report = reports.check_xdf3_quality(schema, code_list_map)

        assert report.total_checks == 3
        assert report.schema_checks[0].code == 1037
        assert report.schema_checks[0].error_type == "warning"

    def test_element_versions_group(self):
        factory = Xdf3Factory()
        fieldA = factory.field().build()
        fieldB = factory.field().build()
        ruleA = factory.rule(id="R000001", version="1.0.0").build()
        ruleB = factory.rule(id="R000001", version="1.0.1").build()
        schema = (
            factory.schema()
            .with_group(
                factory.group()
                .with_field(fieldA)
                .with_field(fieldB)
                .with_rule(ruleA)
                .with_rule(ruleB)
                .build()
            )
            .with_field(fieldA)
            .with_field(fieldB)
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 2
        assert report.schema_checks[0].code == 1003
        assert report.group_reports[0].failing_checks[0].code == 1004
        assert report.schema_checks[0].error_type == "warning"
        assert report.group_reports[0].failing_checks[0].error_type == "warning"

    def test_unused_praezisierung_for_auswahl(self):
        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_field(
                factory.field(
                    feldart=xdf3.Feldart.AUSWAHL,
                    datentyp=xdf3.Datentyp.TEXT,
                    praezisierung=xdf3.Praezisierung(),
                    werte=[
                        xdf3.ListenWert("1", "Value1", None),
                        xdf3.ListenWert("2", "Value2", None),
                    ],
                    inhalt="1",
                ).build()
            )
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 1
        assert report.field_reports[0].failing_checks[0].code == 1012
        assert report.field_reports[0].failing_checks[0].error_type == "method"

    def test_struktur_elementart(self):
        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_field(
                factory.field(schema_element_art=xdf3.SchemaElementArt.ABSTRAKT).build()
            )
            .with_group(
                factory.group(schema_element_art=xdf3.SchemaElementArt.ABSTRAKT).build()
            )
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 3
        assert report.schema_checks[0].code == 1013
        assert report.schema_checks[0].error_type == "method"
        assert report.schema_checks[1].code == 1013
        assert report.schema_checks[1].error_type == "method"
        assert report.group_reports[0].failing_checks[0].code == 1106
        assert report.group_reports[0].failing_checks[0].error_type == "warning"

    def test_bezug_for_group_rechtsnormgebunden(self):
        factory = Xdf3Factory()
        fieldA = factory.field(
            schema_element_art=xdf3.SchemaElementArt.ABSTRAKT
        ).build()
        fieldB = factory.field(
            schema_element_art=xdf3.SchemaElementArt.HARMONISIERT
        ).build()
        schema = (
            factory.schema()
            .with_group(
                factory.group(
                    schema_element_art=xdf3.SchemaElementArt.RECHTSNORMGEBUNDEN
                )
                .with_field(fieldA)
                .with_field(fieldB)
                .build()
            )
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 4
        assert report.schema_checks[0].code == 1013
        assert report.schema_checks[1].code == 1022
        assert report.schema_checks[1].error_type == "method"
        assert report.schema_checks[2].code == 1022
        assert report.schema_checks[2].error_type == "method"
        assert report.field_reports[0].failing_checks[0].code == 1101
        assert report.field_reports[1].failing_checks == []

    def test_child_versions(self):
        factory = Xdf3Factory()
        fieldA = factory.field(id="F000001", version="1.0.0").build()
        fieldB = factory.field(id="F000001", version="1.0.1").build()
        schema = factory.schema().with_field(fieldA).with_field(fieldB).message()

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 2
        assert report.schema_checks[0].code == 1003
        assert report.schema_checks[1].code == 1025
        assert report.schema_checks[1].error_type == "method"

    def test_auswahl_group(self):
        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_group(factory.group(art=xdf3.Gruppenart.AUSWAHL).build())
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 2
        assert report.group_reports[0].failing_checks[0].code == 1106
        assert report.group_reports[0].failing_checks[1].code == 1043
        assert report.group_reports[0].failing_checks[1].error_type == "method"

    def test_number_entries_wertelist(self):
        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_field(
                factory.field(
                    werte=[xdf3.ListenWert(code="1", name="name", hilfe=None)],
                ).build()
            )
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 2
        assert report.field_reports[0].failing_checks[0].code == 1044
        assert report.field_reports[0].failing_checks[0].error_type == "method"

    def test_column_definition(self):
        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_field(
                factory.field(codeliste_referenz=None, code_key="some-code-key").build()
            )
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 1
        assert report.field_reports[0].failing_checks[0].code == 1058
        assert report.field_reports[0].failing_checks[0].error_type == "method"

    def test_inhalt_werteliste(self):
        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_field(
                factory.field(
                    feldart=xdf3.Feldart.AUSWAHL,
                    werte=[
                        xdf3.ListenWert(code="code1", name="name1", hilfe=None),
                        xdf3.ListenWert(code="code2", name="name2", hilfe=None),
                    ],
                    inhalt="code3",
                ).build()
            )
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 2
        assert report.field_reports[0].failing_checks[0].code == 1108
        assert report.field_reports[0].failing_checks[1].code == 1085
        assert report.field_reports[0].failing_checks[1].error_type == "method"

    def test_relation_target(self):
        factory = Xdf3Factory()
        schema = (
            factory.schema(
                relation=[
                    xdf3.Relation(
                        praedikat=xdf3.RelationTyp.ERSETZT,
                        objekt=xdf3.Identifier("F000001", None),
                    )
                ]
            )
            .with_field(
                factory.field(
                    relation=[
                        xdf3.Relation(
                            praedikat=xdf3.RelationTyp.IST_ABGELEITET_VON,
                            objekt=xdf3.Identifier("S000001", None),
                        )
                    ]
                ).build()
            )
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 2
        assert report.schema_checks[0].code == 1087
        assert report.field_reports[0].failing_checks[0].code == 1087

    def test_relation_type(self):
        factory = Xdf3Factory()
        schema = (
            factory.schema(
                relation=[
                    xdf3.Relation(
                        praedikat=xdf3.RelationTyp.IST_VERKNUEPFT_MIT,
                        objekt=xdf3.Identifier("S000001", None),
                    )
                ]
            )
            .with_field(
                factory.field(
                    relation=[
                        xdf3.Relation(
                            praedikat=xdf3.RelationTyp.IST_VERKNUEPFT_MIT,
                            objekt=xdf3.Identifier("F000001", None),
                        )
                    ]
                ).build()
            )
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 2
        assert report.schema_checks[0].code == 1088
        assert report.field_reports[0].failing_checks[0].code == 1088

    def test_empty_bezug_for_field_harmonisert(self):
        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_field(
                factory.field(
                    schema_element_art=xdf3.SchemaElementArt.HARMONISIERT, bezug=[]
                ).build()
            )
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 1
        assert report.field_reports[0].failing_checks[0].code == 1101
        assert report.field_reports[0].failing_checks[0].error_type == "warning"

    def test_praezisierung_for_text_field(self):
        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_field(factory.field().input(datentyp=xdf3.Datentyp.TEXT).build())
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 1
        assert report.field_reports[0].failing_checks[0].code == 1102
        assert report.field_reports[0].failing_checks[0].error_type == "warning"

    def test_praezisierung_for_range_field(self):
        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_field(factory.field().input(datentyp=xdf3.Datentyp.GANZZAHL).build())
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 1
        assert report.field_reports[0].failing_checks[0].code == 1103
        assert report.field_reports[0].failing_checks[0].error_type == "warning"

    def test_codelist_version_format_with_undefined_version(self):
        code_list_identifier = genericode.Identifier(
            canonical_uri="URI",
            canonical_version_uri="2",
            version=None,
        )

        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_field(factory.field(codeliste_referenz=code_list_identifier).build())
            .message()
        )

        code_list_map = {
            code_list_identifier.canonical_version_uri: CodeListFactory().build()
        }

        report = reports.check_xdf3_quality(schema, code_list_map)

        assert report.total_checks == 2
        assert report.field_reports[0].failing_checks[0].code == 1105
        assert report.field_reports[0].failing_checks[0].error_type == "warning"

    def test_codelist_version_format(self):
        code_list_identifier = genericode.Identifier(
            canonical_uri="URI",
            canonical_version_uri="2",
            version="other-format",
        )

        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_field(factory.field(codeliste_referenz=code_list_identifier).build())
            .message()
        )

        code_list_map = {
            code_list_identifier.canonical_version_uri: CodeListFactory().build()
        }

        report = reports.check_xdf3_quality(schema, code_list_map)

        assert report.total_checks == 2
        assert report.field_reports[0].failing_checks[0].code == 1105
        assert report.field_reports[0].failing_checks[0].error_type == "warning"

    def test_number_of_children_group(self):
        factory = Xdf3Factory()
        schema = factory.schema().with_group(factory.group().build()).message()

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 1
        assert report.group_reports[0].failing_checks[0].code == 1106

    def test_datatype_auswahl_field(self):
        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_field(
                factory.field(
                    datentyp=xdf3.Datentyp.OBJEKT,
                    feldart=xdf3.Feldart.AUSWAHL,
                    praezisierung=None,
                    werte=[
                        xdf3.ListenWert("1", "Value1", None),
                        xdf3.ListenWert("2", "Value2", None),
                    ],
                    inhalt="1",
                ).build()
            )
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 1
        assert report.field_reports[0].failing_checks[0].code == 1108
        assert report.field_reports[0].failing_checks[0].error_type == "warning"

    def test_static_field_vorbefuellung(self):
        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_field(
                factory.field(
                    feldart=xdf3.Feldart.STATISCH,
                    datentyp=xdf3.Datentyp.TEXT,
                    praezisierung=xdf3.Praezisierung(min_length=1, max_length=3),
                    inhalt=None,
                ).build()
            )
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 1
        assert report.field_reports[0].failing_checks[0].code == 1109
        assert report.field_reports[0].failing_checks[0].error_type == "warning"

    def test_static_field_datatype(self):
        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_field(
                factory.field(
                    feldart=xdf3.Feldart.STATISCH,
                    datentyp=xdf3.Datentyp.OBJEKT,
                    vorbefuellung=xdf3.Vorbefuellung.VERPFLICHTEND,
                ).build()
            )
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 1
        assert report.field_reports[0].failing_checks[0].code == 1109
        assert report.field_reports[0].failing_checks[0].error_type == "warning"

    def test_element_status(self):
        factory = Xdf3Factory()
        groupFieldA = factory.field(
            freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
        ).build()
        groupFieldB = factory.field(freigabe_status=xdf3.FreigabeStatus.INAKTIV).build()
        schema = (
            factory.schema(freigabe_status=xdf3.FreigabeStatus.INAKTIV)
            .with_group(
                factory.group(freigabe_status=xdf3.FreigabeStatus.INAKTIV)
                .with_field(groupFieldA)
                .with_field(groupFieldB)
                .build()
            )
            .with_field(
                factory.field(freigabe_status=xdf3.FreigabeStatus.INAKTIV).build()
            )
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 4
        assert report.schema_checks[0].code == 1116
        assert report.group_reports[0].failing_checks[0].code == 1116
        assert report.field_reports[0].failing_checks[0].code == 1116
        assert report.field_reports[0].failing_checks[0].error_type == "warning"
        assert report.field_reports[1].failing_checks[0].code == 1116
        assert report.field_reports[1].failing_checks[0].error_type == "warning"
        assert report.field_reports[2].failing_checks == []

    def test_hinweis_fachlicher_ersteller(self):
        factory = Xdf3Factory()
        schema = factory.schema(status_gesetzt_durch=None).message()

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 1
        assert report.schema_checks[0].code == 1117
        assert report.schema_checks[0].error_type == "info"

    def test_inhalt_codeliste(self):
        code_list = CodeListFactory().build()

        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_field(
                factory.field(
                    feldart=xdf3.Feldart.AUSWAHL,
                    datentyp=xdf3.Datentyp.TEXT,
                    inhalt="not in the code list",
                )
                .with_code_list(code_list)
                .build()
            )
            .message()
        )

        report = reports.check_xdf3_quality(
            schema, {code_list.identifier.canonical_version_uri: code_list}
        )

        assert report.total_checks == 1
        assert report.field_reports[0].failing_checks[0].code == 1076
        assert report.field_reports[0].failing_checks[0].error_type == "method"

    def test_code_list_availability(self):
        factory = Xdf3Factory()
        schema = (
            factory.schema()
            .with_field(
                factory.field(
                    feldart=xdf3.Feldart.AUSWAHL,
                    datentyp=xdf3.Datentyp.TEXT,
                )
                .with_code_list(CodeListFactory().build())
                .build()
            )
            .message()
        )

        report = reports.check_xdf3_quality(schema, {})

        assert report.total_checks == 1
        assert report.field_reports[0].failing_checks[0].code == 1115
        assert report.field_reports[0].failing_checks[0].error_type == "warning"
