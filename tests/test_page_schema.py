import pytest
from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.data import XDF2_DATA, XDF3_DATA
from tests.factories.xdf2 import Xdf2Factory

SUBPATHS = [
    "",
    "/structure",
    "/report",
    "/downloads",
]


@pytest.mark.parametrize("filename", ["S1234V1.0.xml"])
@pytest.mark.parametrize("subpath", SUBPATHS)
def test_load_xdf2_schema_page(
    client: TestClient,
    runner: CommandRunner,
    filename: str,
    subpath: str,
) -> None:
    schema = runner.import_xdf2_file(XDF2_DATA / filename)

    response = client.get(f"/schemas/{schema.fim_id}/{schema.fim_version}{subpath}")

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_load_xdf2_schema_page_when_schema_is_not_convertible_to_xdf3(
    client: TestClient,
    runner: CommandRunner,
) -> None:
    # Each xdf3 schema/group must have at least one child.
    # This schema is therefore not convertible to xdf3.
    schema = Xdf2Factory().schema().save(runner)
    assert len(schema.children) == 0

    response = client.get(f"/schemas/{schema.fim_id}/{schema.fim_version}")

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


@pytest.mark.parametrize("filename", ["schema.xml", "schema_empty_bezug.xml"])
@pytest.mark.parametrize("subpath", SUBPATHS)
def test_load_xdf3_schema_page(
    client: TestClient,
    runner: CommandRunner,
    filename: str,
    subpath: str,
) -> None:
    schema = runner.import_xdf3_file(XDF3_DATA / filename)

    response = client.get(f"/schemas/{schema.fim_id}/{schema.fim_version}{subpath}")

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_unknown_schema(client: TestClient) -> None:
    response = client.get("/schemas/S1234/1.0")

    assert response.status_code == 404
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert "Datenschema S1234 v1.0 konnte nicht gefunden werden." in response.text
