from fastapi.testclient import TestClient
from freezegun import freeze_time

from fimportal.helpers import format_iso8601, optional_date_to_string
from fimportal.xdatenfelder import xdf3
from fimportal.xdatenfelder.xdf3.common import Dokumentart
from tests.conftest import CommandRunner
from tests.factories import Xdf2SteckbriefImportFactory
from tests.factories.prozess import ProzessFactory
from tests.factories.xdf2 import Xdf2Factory
from tests.factories.xdf3 import Xdf3SteckbriefMessageFactory


def test_should_correctly_import_document_profile(
    runner: CommandRunner, client: TestClient
):
    steckbrief_import = Xdf2SteckbriefImportFactory(
        "D123", "1.0", bezug="Bezug"
    ).build()
    steckbrief = steckbrief_import.steckbrief

    freeze_timestamp = "2023-10-17T00:00:00Z"
    with freeze_time(freeze_timestamp):
        runner.import_xdf2_steckbrief(steckbrief_import)

    response = client.get("/api/v1/document-profiles/D123/1.0")

    assert response.status_code == 200
    assert response.json() == {
        "fim_id": "D123",
        "fim_version": "1.0",
        "nummernkreis": "12000",
        "name": steckbrief.name,
        "definition": steckbrief.definition,
        "freigabe_status": steckbrief_import.freigabe_status.value,
        "freigabe_status_label": steckbrief_import.freigabe_status.to_label(),
        "status_gesetzt_durch": steckbrief.fachlicher_ersteller,
        "status_gesetzt_am": optional_date_to_string(steckbrief.freigabedatum),
        "gueltig_ab": None,
        "gueltig_bis": None,
        "bezug": ["Bezug"],
        "versionshinweis": steckbrief.versionshinweis,
        "veroeffentlichungsdatum": optional_date_to_string(
            steckbrief.veroeffentlichungsdatum
        ),
        "letzte_aenderung": format_iso8601(steckbrief_import.letzte_aenderung),
        "last_update": freeze_timestamp,
        "bezeichnung": steckbrief.bezeichnung_eingabe,
        "dokumentart": Dokumentart.from_label(steckbrief.dokumentart).value,
        "ist_abstrakt": steckbrief.is_referenz,
        "hilfetext": steckbrief.hilfetext,
        "stichwort": [],
        "xdf_version": "2.0",
        "beschreibung": steckbrief.beschreibung,
        "is_latest": True,
        "prozesse": [],
        "datenschemata": [],
        "relation": [],
    }


def test_should_load_the_latest_version_of_a_steckbrief(
    runner: CommandRunner, client: TestClient
):
    Xdf2SteckbriefImportFactory("D123", "1.0").save(runner)
    Xdf2SteckbriefImportFactory("D123", "1.1").save(runner)

    response = client.get("/api/v1/document-profiles/D123/latest")

    assert response.status_code == 200
    assert response.json()["fim_version"] == "1.1"


def test_should_fail_for_unknown_steckbrief(client: TestClient):
    response = client.get("/api/v1/document-profiles/D123/1.0")

    assert response.status_code == 404
    assert response.json() == {"detail": "Could not find document profile D123V1.0."}


# TODO: tests for is_latest filter parameter


def test_should_get_only_latest_steckbrief_when_is_latest_is_true(
    runner: CommandRunner, client: TestClient
):
    Xdf2SteckbriefImportFactory(id="D12000", version="1.1").save(runner)
    Xdf2SteckbriefImportFactory(id="D12000", version="1.2").save(runner)

    response = client.get("/api/v1/document-profiles?is_latest=true")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_version"] == "1.2"


def test_should_get_only_older_steckbriefe_when_is_latest_is_false(
    runner: CommandRunner, client: TestClient
):
    Xdf2SteckbriefImportFactory(id="D12000", version="1.1").save(runner)
    Xdf2SteckbriefImportFactory(id="D12000", version="1.2").save(runner)
    Xdf2SteckbriefImportFactory(id="D12000", version="1.3").save(runner)

    response = client.get("/api/v1/document-profiles?is_latest=false")

    assert response.status_code == 200
    assert response.json()["count"] == 2
    assert response.json()["items"][0]["fim_version"] in ("1.1", "1.2")
    assert response.json()["items"][1]["fim_version"] in ("1.1", "1.2")


def test_should_load_referencing_prozesse(runner: CommandRunner, client: TestClient):
    steckbrief = Xdf2SteckbriefImportFactory().save(runner)
    prozess = ProzessFactory().add_ausloeser(steckbrief.fim_id).save(runner)

    response = client.get(
        f"/api/v1/document-profiles/{steckbrief.fim_id}/{steckbrief.fim_version}"
    )

    assert response.status_code == 200
    assert response.json()["prozesse"] == [
        {
            "id": prozess.id,
            "name": prozess.name,
        }
    ]


def test_should_load_referencing_datenschemata(
    runner: CommandRunner, client: TestClient
):
    steckbrief = Xdf2SteckbriefImportFactory().save(runner)
    datenschema = Xdf2Factory().schema().save(runner, steckbrief_id=steckbrief.fim_id)

    response = client.get(
        f"/api/v1/document-profiles/{steckbrief.fim_id}/{steckbrief.fim_version}"
    )

    assert response.status_code == 200
    assert response.json()["datenschemata"] == [
        {
            "fim_id": datenschema.fim_id,
            "fim_version": datenschema.fim_version,
            "name": datenschema.name,
        }
    ]


def test_should_return_relations(runner: CommandRunner, client: TestClient):
    related_steckbrief = Xdf3SteckbriefMessageFactory().save(runner)
    related_fim_version = related_steckbrief.fim_version
    assert related_fim_version is not None
    steckbrief = Xdf3SteckbriefMessageFactory(
        relation=[
            xdf3.Relation(
                xdf3.RelationTyp.IST_ABGELEITET_VON,
                xdf3.Identifier(
                    related_steckbrief.fim_id, xdf3.Version(related_fim_version)
                ),
            )
        ]
    ).save(runner)

    response = client.get(
        f"/api/v1/document-profiles/{steckbrief.fim_id}/{steckbrief.fim_version}"
    )

    assert response.status_code == 200
    result = response.json()
    assert result["relation"] == [
        {
            "fim_id": related_steckbrief.fim_id,
            "fim_version": related_fim_version,
            "name": related_steckbrief.name,
            "typ": xdf3.RelationTyp.IST_ABGELEITET_VON.value,
        }
    ]
