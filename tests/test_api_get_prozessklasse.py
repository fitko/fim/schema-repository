from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.factories.prozess import ProzessklasseFactory
from tests.factories.xzufi import LeistungFactory

ENDPOINT = "/api/v0/processclasses"


def test_get_prozessklasse(runner: CommandRunner, client: TestClient):
    id = "some prozess id"
    prozess_klasse = ProzessklasseFactory(id=id).save(runner)

    response = client.get(f"{ENDPOINT}/{id}")

    assert response.status_code == 200
    assert response.json() == {
        "id": prozess_klasse.id,
        "version": prozess_klasse.version,
        "name": prozess_klasse.name,
        "freigabe_status": prozess_klasse.freigabe_status,
        "letzter_aenderungszeitpunkt": None,
        "leistungssteckbrief": {
            "leistungsschluessel": prozess_klasse.id,
            "name": None,
        },
        "prozesse": [],
    }


def test_get_prozessklasse_not_found(client: TestClient):
    response = client.get(f"{ENDPOINT}/some-id")

    assert response.status_code == 404


class TestLeistungsteckbriefReferences:
    def test_should_save_references_to_leistungssteckbrief_based_on_the_id(
        self, runner: CommandRunner, client: TestClient
    ):
        prozess_klasse = ProzessklasseFactory(id="99123000000000").save(runner)

        response = client.get(f"{ENDPOINT}/{prozess_klasse.id}")

        assert response.status_code == 200
        assert response.json()["leistungssteckbrief"] == {
            "leistungsschluessel": "99123000000000",
            "name": None,
        }

    def test_should_include_the_title_of_the_steckbrief_if_available(
        self, runner: CommandRunner, client: TestClient
    ):
        leistungsschluesssel = "99123000000000"
        leistungssteckbrief = (
            LeistungFactory()
            .steckbrief(
                leistungsschluessel=leistungsschluesssel,
            )
            .save_leika(runner)
        )
        prozess_klasse = ProzessklasseFactory(id=leistungsschluesssel).save(runner)

        response = client.get(f"{ENDPOINT}/{prozess_klasse.id}")

        assert response.status_code == 200
        assert response.json()["leistungssteckbrief"] == {
            "leistungsschluessel": leistungsschluesssel,
            "name": leistungssteckbrief.title,
        }
