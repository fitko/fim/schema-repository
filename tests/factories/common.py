_global_unique_counter = 0


def get_unique_id() -> int:
    global _global_unique_counter
    _global_unique_counter += 1

    return _global_unique_counter


# Sentry class to mark an optional value to be auto generated.
# This way, `None` can be passed explicitly to communicate, that the value should stay empty.
class AutoGenerate:
    pass
