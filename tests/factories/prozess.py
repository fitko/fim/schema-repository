from datetime import datetime
from faker import Faker
from fimportal.common import Bundesland, FreigabeStatus, VerwaltungspolitischeKodierung
from fimportal.xprozesse.client import TEST_PROZESSKLASSE_XML
from fimportal.xprozesse.prozess import (
    Detaillierungsstufe,
    AusloeserErgebnis,
    Handlungsform,
    Klassifikation,
    OperativesZiel,
    Prozess,
    ProzessBibliothek,
    ProzessKatalog,
    ProzessMessage,
    ProzessNachrichtenKopf,
    ProzessSteckbrief,
    Prozessklasse,
    Verfahrensart,
    Zustandsangaben,
)
from tests.conftest import TEST_PROZESS_XML, CommandRunner
from tests.factories.common import get_unique_id


faker = Faker()


class ProzessMessageFactory:
    prozess: Prozess

    def __init__(self, prozess: Prozess | None = None):
        self.prozess = prozess or ProzessFactory().build()

    def build(self):
        return ProzessMessage(
            nachrichtenkopf=ProzessNachrichtenKopf(
                nachricht_uuid=str(get_unique_id()),
                erstellungszeitpunkt=faker.date_time(),
                autor=None,
                leser=None,
            ),
            prozesskatalog=ProzessKatalog(
                name=faker.name(),
                version=None,
                verwaltungspolitische_kodierung=[],
                prozessklasse=[],
            ),
            prozessbibliothek=ProzessBibliothek(
                name=faker.name(),
                verwaltungspolitische_kodierung=[],
                prozess=[self.prozess],
            ),
        )


class ProzessFactory:
    id: str
    version: str | None
    name: str
    detaillierungsstufe: Detaillierungsstufe | None
    letzter_aenderungszeitpunkt: datetime | None
    prozessklasse_id: str
    freigabe_status: FreigabeStatus | None
    verwaltungspolitisch_kodierung: list[Bundesland]

    _ausloeser: list[AusloeserErgebnis]
    _ergebnis: list[AusloeserErgebnis]

    def __init__(
        self,
        id: str | None = None,
        version: str | None = None,
        name: str | None = None,
        detaillierungsstufe: Detaillierungsstufe | None = None,
        prozessklasse_id: str | None = None,
        letzter_aenderungszeitpunkt: datetime | None = None,
        freigabe_status: FreigabeStatus
        | None = FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        verwaltungspolitisch_kodierung: list[Bundesland] | None = None,
    ):
        self.id = id or str(get_unique_id())
        self.prozessklasse_id = prozessklasse_id or str(get_unique_id())
        self.version = version
        self.name = name or faker.name()
        self.detaillierungsstufe = detaillierungsstufe
        self.letzter_aenderungszeitpunkt = letzter_aenderungszeitpunkt
        self.freigabe_status = freigabe_status
        self.verwaltungspolitisch_kodierung = verwaltungspolitisch_kodierung or []

        self._ausloeser = []
        self._ergebnis = []

    def add_ausloeser(self, dokumentsteckbrief_fim_id: str):
        self._ausloeser.append(
            AusloeserErgebnis(
                formular_id=dokumentsteckbrief_fim_id,
                prozess_id=None,
                textuelle_beschreibung=None,
            )
        )

        return self

    def add_ergebnis(self, dokumentsteckbrief_fim_id: str):
        self._ergebnis.append(
            AusloeserErgebnis(
                formular_id=dokumentsteckbrief_fim_id,
                prozess_id=None,
                textuelle_beschreibung=None,
            )
        )

        return self

    def build(self):
        return Prozess(
            id=self.id,
            version=self.version,
            name=self.name,
            bezeichnung=None,
            klassifikation=[
                Klassifikation(
                    ordnungsrahmen_name=faker.name(),
                    ordnungsrahmen_version=None,
                    klasse_id=self.prozessklasse_id,
                    klasse_name=None,
                )
            ],
            fachlich_freigebende_stelle=None,
            schlagwort=[],
            prozesssteckbrief=ProzessSteckbrief(
                definition=None,
                beschreibung=None,
                ausloeser=self._ausloeser,
                ergebnis=self._ergebnis,
                prozessteilnehmer=[],
                handlungsgrundlage=[],
                detaillierungsstufe=self.detaillierungsstufe,
                verwaltungspolitische_kodierung=[
                    VerwaltungspolitischeKodierung(
                        kreis=None,
                        bezirk=None,
                        bundesland=land,
                        gemeindeschluessel=None,
                        regionalschluessel=None,
                        nation=None,
                        gemeindeverband=None,
                    )
                    for land in self.verwaltungspolitisch_kodierung
                ],
                zielvorgaben=None,
                merkmal=[],
            ),
            prozessmodell=None,
            prozessstrukturbeschreibung=None,
            zustandsangaben=Zustandsangaben(
                erstellungszeitpunkt=None,
                letzter_aenderungszeitpunkt=self.letzter_aenderungszeitpunkt,
                letzter_bearbeiter=None,
                status=self.freigabe_status,
                anmerkung_letzte_aenderung=None,
                gueltigkeitszeitraum=None,
                fachlicher_freigabezeitpunkt=None,
                formeller_freigabezeitpunkt=None,
            ),
        )

    def save(self, runner: CommandRunner, xml_content: str = TEST_PROZESS_XML):
        prozess = self.build()
        runner.import_prozess(prozess, xml_content)
        return prozess


class ProzessklasseFactory:
    id: str
    name: str
    letzter_aenderungszeitpunkt: datetime | None
    freigabe_status: FreigabeStatus | None
    operatives_ziel: OperativesZiel | None
    verfahrensart: Verfahrensart | None
    handlungsform: Handlungsform | None

    def __init__(
        self,
        id: str | None = None,
        name: str | None = None,
        letzter_aenderungszeitpunkt: datetime | None = None,
        freigabe_status: FreigabeStatus
        | None = FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        operatives_ziel: OperativesZiel | None = None,
        verfahrensart: Verfahrensart | None = None,
        handlungsform: Handlungsform | None = None,
    ):
        self.id = id or str(get_unique_id())
        self.name = name or faker.name()
        self.letzter_aenderungszeitpunkt = letzter_aenderungszeitpunkt
        self.freigabe_status = freigabe_status
        self.operatives_ziel = operatives_ziel
        self.verfahrensart = verfahrensart
        self.handlungsform = handlungsform

    def build(self) -> Prozessklasse:
        return Prozessklasse(
            id=self.id,
            version=None,
            gliederungsebene=None,
            uebergeordnete_prozessklasse_id=None,
            name=self.name,
            bezeichnung=None,
            definition=None,
            handlungsgrundlage=[],
            zwecksetzung=None,
            operatives_ziel=self.operatives_ziel,
            handlungsform=self.handlungsform,
            verfahrensart=self.verfahrensart,
            fachlich_freigebende_stelle=None,
            verwaltungspolitische_kodierung=[],
            merkmal=[],
            klassifikation=[
                Klassifikation(
                    ordnungsrahmen_name="some name",
                    ordnungsrahmen_version=None,
                    klasse_id="100",
                    klasse_name="First Class",
                ),
                Klassifikation(
                    ordnungsrahmen_name="some name",
                    ordnungsrahmen_version=None,
                    klasse_id="100.01",
                    klasse_name="Second Class",
                ),
                Klassifikation(
                    ordnungsrahmen_name="some name",
                    ordnungsrahmen_version=None,
                    klasse_id="100.01.01",
                    klasse_name="Third Class",
                ),
            ],
            schlagwort=[],
            zustandsangaben=Zustandsangaben(
                erstellungszeitpunkt=None,
                letzter_aenderungszeitpunkt=self.letzter_aenderungszeitpunkt,
                letzter_bearbeiter=None,
                status=self.freigabe_status,
                anmerkung_letzte_aenderung=None,
                gueltigkeitszeitraum=None,
                fachlicher_freigabezeitpunkt=None,
                formeller_freigabezeitpunkt=None,
            ),
        )

    def save(self, runner: CommandRunner, xml_content: str = TEST_PROZESSKLASSE_XML):
        prozessklasse = self.build()
        runner.import_prozessklasse(prozessklasse, xml_content)
        return prozessklasse
