from __future__ import annotations

from faker import Faker

from fimportal import genericode
from fimportal.models.imports import CodeListImport
from .common import get_unique_id
from tests.data import GENERICODE_DATA


faker = Faker()

EXAMPLE_CODE_LIST = (GENERICODE_DATA / "anschrift.xml").read_text()


class CodeListImportFactory:
    code_list: genericode.CodeList
    code_list_text: str

    def __init__(
        self,
        code_list: genericode.CodeList | None = None,
        code_list_text: str | None = None,
    ):
        if code_list is None:
            code_list = CodeListFactory().build()

        if code_list_text is None:
            code_list_text = EXAMPLE_CODE_LIST

        self.code_list = code_list
        self.code_list_text = code_list_text

    def build(self) -> CodeListImport:
        return CodeListImport(
            code_list_text=self.code_list_text,
            code_list=self.code_list,
        )


class CodeListFactory:
    canonical_uri: str
    version: str | None
    canonical_version_uri: str

    default_code_key: str
    default_name_key: str | None
    columns: dict[str, list[str | None]]

    def __init__(
        self,
        canonical_uri: str | None = None,
        version: str | None = None,
        default_code_key: str | None = None,
        default_name_key: str | None = None,
        columns: dict[str, list[str | None]] | None = None,
    ):
        if canonical_uri is None:
            canonical_uri = f"urn:de:test-{get_unique_id()}"
        self.canonical_uri = canonical_uri

        self.version = "0000-00-00" if version is None else version

        self.canonical_version_uri = f"{self.canonical_uri}_{self.version}"

        self.default_code_key = (
            "code_key" if default_code_key is None else default_code_key
        )
        self.default_name_key = (
            "name_key" if default_name_key is None else default_name_key
        )
        self.columns = (
            columns
            if columns is not None
            else {
                self.default_code_key: ["0", "1"],
                self.default_name_key: ["Name0", "Name1"],
            }
        )

    def add_column(self, key: str, values: list[str | None]):
        self.columns[key] = values
        return self

    def build(self) -> genericode.CodeList:
        assert self.default_name_key is not None
        return genericode.CodeList(
            identifier=genericode.Identifier(
                canonical_uri=self.canonical_uri,
                version=self.version,
                canonical_version_uri=self.canonical_version_uri,
            ),
            default_code_key=self.default_code_key,
            default_name_key=self.default_name_key,
            columns=self.columns,
        )
