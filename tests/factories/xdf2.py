from __future__ import annotations

from dataclasses import dataclass
from datetime import date, datetime, timezone
from typing import Iterable

from faker import Faker

from fimportal import din91379, genericode
from fimportal.helpers import utc_now
from fimportal.models.imports import (
    CodeListImport,
    Xdf2SchemaImport,
    Xdf2SteckbriefImport,
)
from fimportal.models.xdf3 import FullSchemaOut, FullSteckbriefOut
from fimportal.xdatenfelder import xdf2, xdf3
from tests.conftest import CommandRunner

from .common import get_unique_id
from .genericode import CodeListFactory

faker = Faker()

TEST_STECKBRIEF_XML = """
<xdf:xdatenfelder.dokumentsteckbrief.0101 xmlns:xdf="urn:xoev-de:fim:standard:xdatenfelder_2">
    <xdf:header>
        <xdf:nachrichtID>B01BEB657FA84011</xdf:nachrichtID>
        <xdf:erstellungszeitpunkt>2023-08-08T07:29:34.060Z</xdf:erstellungszeitpunkt>
    </xdf:header>
</xdf:xdatenfelder.dokumentsteckbrief.0101>
"""


class Xdf2SteckbriefImportFactory:
    id: str
    version: str
    name: str
    bezeichnung_eingabe: str | None
    beschreibung: str | None
    definition: str | None
    bezug: str | None
    freigabe_status: xdf3.FreigabeStatus
    status: xdf2.Status
    versionshinweis: str | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    fachlicher_ersteller: str | None
    freigabedatum: date | None
    veroeffentlichungsdatum: date | None
    is_referenz: bool
    dokumentart: xdf3.Dokumentart
    hilfetext: str | None

    def __init__(
        self,
        id: str | None = None,
        version: str | None = None,
        name: str | None = None,
        definition: str | None = None,
        freigabe_status: xdf3.FreigabeStatus = xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        fachlicher_ersteller: str | None = None,
        bezug: str | None = None,
        letzte_aenderung: datetime | None = None,
        versionshinweis: str | None = None,
        beschreibung: str | None = None,
        gueltig_ab: date | None = None,
        gueltig_bis: date | None = None,
        is_referenz: bool = False,
        dokumentart: xdf3.Dokumentart = xdf3.Dokumentart.UNBESTIMMT,
        hilfetext: str | None = None,
        bezeichnung_eingabe: str | None = None,
    ):
        if id is None:
            id = f"D00000{get_unique_id()}"
        self.id = id

        if version is None:
            version = "1.0"
        self.version = version

        if name is None:
            name = str(faker.name())
        self.name = name

        self.freigabe_status = freigabe_status
        self.definition = definition
        self.bezeichnung_eingabe = bezeichnung_eingabe
        self.beschreibung = beschreibung
        self.versionshinweis = versionshinweis
        self.fachlicher_ersteller = fachlicher_ersteller
        self.freigabedatum = faker.date_object()
        self.veroeffentlichungsdatum = faker.date_object()
        self.bezug = bezug
        self.letzte_aenderung = (
            letzte_aenderung
            if letzte_aenderung is not None
            else faker.date_time().astimezone(timezone.utc)
        )
        self.gueltig_ab = gueltig_ab
        self.gueltig_bis = gueltig_bis
        self.is_referenz = is_referenz
        self.dokumentart = dokumentart
        self.hilfetext = hilfetext

    def full(self) -> Xdf2SteckbriefImportFactory:
        self.definition = self.definition or faker.text()
        self.bezeichnung_eingabe = self.bezeichnung_eingabe or faker.name()
        self.beschreibung = self.beschreibung or faker.text()
        self.fachlicher_ersteller = self.fachlicher_ersteller or faker.name()
        self.freigabedatum = self.freigabedatum or faker.date_object()
        self.letzte_aenderung = self.letzte_aenderung or faker.date_time().astimezone(
            timezone.utc
        )
        self.versionshinweis = self.versionshinweis or faker.text()
        self.bezug = self.bezug or faker.text()
        self.gueltig_ab = self.gueltig_ab or faker.date_object()
        self.gueltig_bis = self.gueltig_bis or faker.date_object()
        self.hilfetext = self.hilfetext or faker.text()

        return self

    def build(self) -> Xdf2SteckbriefImport:
        def _xdf3_freigabe_status_to_xdf2_status(
            status: xdf3.FreigabeStatus,
        ) -> xdf2.Status:
            if status < 4:
                return xdf2.Status.IN_VORBEREITUNG
            if status >= 4 and status < 7:
                return xdf2.Status.AKTIV
            return xdf2.Status.INAKTIV

        return Xdf2SteckbriefImport(
            letzte_aenderung=self.letzte_aenderung,
            freigabe_status=self.freigabe_status,
            xml_content=TEST_STECKBRIEF_XML,
            steckbrief=xdf2.Steckbrief(
                identifier=xdf2.Identifier(self.id, xdf2.parse_version(self.version)),
                name=self.name,
                bezeichnung_eingabe=self.bezeichnung_eingabe,
                bezeichnung_ausgabe=None,
                beschreibung=self.beschreibung,
                definition=self.definition,
                bezug=self.bezug,
                status=_xdf3_freigabe_status_to_xdf2_status(self.freigabe_status),
                versionshinweis=self.versionshinweis,
                gueltig_ab=self.gueltig_ab,
                gueltig_bis=self.gueltig_bis,
                fachlicher_ersteller=self.fachlicher_ersteller,
                freigabedatum=self.freigabedatum,
                veroeffentlichungsdatum=self.veroeffentlichungsdatum,
                is_referenz=self.is_referenz,
                dokumentart=self.dokumentart.to_label(),
                hilfetext=self.hilfetext,
            ),
        )

    def save(self, runner: CommandRunner) -> FullSteckbriefOut:
        steckbrief_import = self.build()
        return runner.import_xdf2_steckbrief(steckbrief_import)


class Xdf2Factory:
    """
    Factory for creating XDF2 elements (schemas, groups, fields, rules).

    Since schemas contain a full tree of groups, fields and rules, this factory
    makes it easier to create deeply nested structures without the need to explicitly
    add all elements to the schema message before serializing.

    Instead, all created elements are registered in the factory.
    When finally building a message, all references can be resolved automatically.

    Example usage:

    ```python
    factory = Xdf2Factory()

    rule = factory.rule().build()
    field = factory.field().with_rule(rule).build()
    group = factory.group().with_field(field).build()


    # Building the schema message now automatically resolves all references
    # to groups, fields and rules, no need to register them manually anymore.
    schema_message = factory.schema().with_group(group).message()
    ```

    """

    _schemas: dict[xdf2.Identifier, xdf2.Schema]
    _groups: dict[xdf2.Identifier, xdf2.Gruppe]
    _fields: dict[xdf2.Identifier, xdf2.Datenfeld]
    _rules: dict[xdf2.Identifier, xdf2.Regel]

    def __init__(
        self,
    ):
        self._schemas = {}
        self._groups = {}
        self._fields = {}
        self._rules = {}

    def register_schema(self, schema: xdf2.Schema) -> None:
        assert schema.identifier not in self._schemas
        self._schemas[schema.identifier] = schema

    def register_group(self, group: xdf2.Gruppe) -> None:
        assert group.identifier not in self._groups
        self._groups[group.identifier] = group

    def register_field(self, field: xdf2.Datenfeld) -> None:
        assert field.identifier not in self._fields
        self._fields[field.identifier] = field

    def register_rule(self, rule: xdf2.Regel) -> None:
        assert rule.identifier not in self._rules
        self._rules[rule.identifier] = rule

    def resolve_rules(
        self, rule_identifiers: Iterable[xdf2.Identifier]
    ) -> dict[xdf2.Identifier, xdf2.Regel]:
        return {identifier: self._rules[identifier] for identifier in rule_identifiers}

    def resolve_tree(
        self, struktur: list[xdf2.ElementReference], root_rules: list[xdf2.Identifier]
    ) -> tuple[
        dict[xdf2.Identifier, xdf2.Gruppe],
        dict[xdf2.Identifier, xdf2.Datenfeld],
        dict[xdf2.Identifier, xdf2.Regel],
    ]:
        """
        Recursively collect all groups, fields and rules referenced by the given structure.
        """

        groups = {}
        fields = {}
        rule_identifiers = set(root_rules)

        stack = struktur.copy()
        while stack:
            element = stack.pop()

            if element.element_type == xdf2.ElementType.GRUPPE:
                group = self._groups[element.identifier]
                groups[group.identifier] = group
                stack.extend(group.struktur)

                rule_identifiers.update(group.regeln)
            else:
                assert element.element_type == xdf2.ElementType.FELD
                field = self._fields[element.identifier]
                fields[field.identifier] = field

                rule_identifiers.update(field.regeln)

        rules = self.resolve_rules(rule_identifiers)

        return groups, fields, rules

    def schema(
        self,
        id: str | None = None,
        version: str | None = None,
        name: str | None = None,
        bezeichnung_eingabe: str | None = None,
        bezeichnung_ausgabe: str | None = None,
        beschreibung: str | None = None,
        definition: str | None = None,
        bezug: str | None = None,
        versionshinweis: str | None = None,
        fachlicher_ersteller: str | None = None,
        gueltig_ab: date | None = None,
        gueltig_bis: date | None = None,
        freigabedatum: date | None = None,
        veroeffentlichungsdatum: date | None = None,
        hilfetext: str | None = None,
    ) -> Xdf2SchemaFactory:
        return Xdf2SchemaFactory(
            factory=self,
            id=id or f"S00000{get_unique_id()}",
            version=version or "1.0",
            name=name or faker.name(),
            bezeichnung_eingabe=bezeichnung_eingabe,
            bezeichnung_ausgabe=bezeichnung_ausgabe,
            beschreibung=beschreibung,
            definition=definition,
            bezug=bezug,
            versionshinweis=versionshinweis,
            gueltig_ab=gueltig_ab,
            gueltig_bis=gueltig_bis,
            fachlicher_ersteller=fachlicher_ersteller or faker.name(),
            freigabedatum=freigabedatum,
            veroeffentlichungsdatum=veroeffentlichungsdatum,
            regeln=[],
            struktur=[],
            hilfetext=hilfetext,
        )

    def group(
        self,
        id: str | None = None,
        version: str | None = None,
        nummernkreis: str | None = None,
        name: str | None = None,
        bezeichnung_eingabe: str | None = None,
        bezeichnung_ausgabe: str | None = None,
        beschreibung: str | None = None,
        definition: str | None = None,
        bezug: str | None = None,
        status: xdf2.Status = xdf2.Status.AKTIV,
        fachlicher_ersteller: str | None = None,
        versionshinweis: str | None = None,
        hilfetext_eingabe: str | None = None,
        hilfetext_ausgabe: str | None = None,
        schema_element_art: xdf2.SchemaElementArt = xdf2.SchemaElementArt.HARMONISIERT,
        gueltig_ab: date | None = None,
        gueltig_bis: date | None = None,
        freigabedatum: date | None = None,
        veroeffentlichungsdatum: date | None = None,
    ) -> GroupFactory:
        if id is None:
            nummernkreis = nummernkreis or "00"
            id = f"G{nummernkreis}{get_unique_id()}"
        else:
            assert nummernkreis is None

        return GroupFactory(
            factory=self,
            id=id,
            version=version or "1.0",
            name=name or faker.name(),
            bezeichnung_eingabe=bezeichnung_eingabe,
            bezeichnung_ausgabe=bezeichnung_ausgabe,
            beschreibung=beschreibung,
            definition=definition,
            bezug=bezug,
            status=status,
            fachlicher_ersteller=fachlicher_ersteller,
            versionshinweis=versionshinweis,
            gueltig_ab=gueltig_ab,
            gueltig_bis=gueltig_bis,
            freigabedatum=freigabedatum,
            veroeffentlichungsdatum=veroeffentlichungsdatum,
            hilfetext_eingabe=hilfetext_eingabe,
            hilfetext_ausgabe=hilfetext_ausgabe,
            schema_element_art=schema_element_art,
            regeln=[],
            struktur=[],
        )

    def field(
        self,
        id: str | None = None,
        version: str | None = None,
        nummernkreis: str | None = None,
        name: str | None = None,
        bezeichnung_eingabe: str | None = None,
        bezeichnung_ausgabe: str | None = None,
        beschreibung: str | None = None,
        definition: str | None = None,
        bezug: str | None = None,
        status: xdf2.Status = xdf2.Status.AKTIV,
        fachlicher_ersteller: str | None = None,
        freigabedatum: date | None = None,
        versionshinweis: str | None = None,
        veroeffentlichungsdatum: date | None = None,
        hilfetext_eingabe: str | None = None,
        hilfetext_ausgabe: str | None = None,
        gueltig_ab: date | None = None,
        gueltig_bis: date | None = None,
        schema_element_art: xdf2.SchemaElementArt = xdf2.SchemaElementArt.HARMONISIERT,
        datentyp: xdf2.Datentyp | None = None,
    ) -> FieldFactory:
        if id is None:
            nummernkreis = nummernkreis or "00"
            id = f"F{nummernkreis}{get_unique_id()}"
        else:
            assert nummernkreis is None

        return FieldFactory(
            self,
            id=id,
            version=version or "1.0",
            name=name or faker.name(),
            bezeichnung_eingabe=bezeichnung_eingabe,
            bezeichnung_ausgabe=bezeichnung_ausgabe,
            beschreibung=beschreibung,
            definition=definition,
            bezug=bezug,
            status=status,
            fachlicher_ersteller=fachlicher_ersteller,
            freigabedatum=freigabedatum,
            gueltig_ab=gueltig_ab,
            gueltig_bis=gueltig_bis,
            versionshinweis=versionshinweis,
            veroeffentlichungsdatum=veroeffentlichungsdatum,
            hilfetext_eingabe=hilfetext_eingabe,
            hilfetext_ausgabe=hilfetext_ausgabe,
            schema_element_art=schema_element_art,
            regeln=[],
            feldart=xdf2.Feldart.INPUT,
            datentyp=datentyp or xdf2.Datentyp.TEXT,
        )

    def rule(
        self,
        id: str | None = None,
        version: str | None = None,
        nummernkreis: str | None = None,
        name: str | None = None,
        bezeichnung_eingabe: str | None = None,
        bezeichnung_ausgabe: str | None = None,
        beschreibung: str | None = None,
        definition: str | None = None,
        bezug: str | None = None,
        status: xdf2.Status = xdf2.Status.AKTIV,
        versionshinweis: str | None = None,
        gueltig_ab: date | None = None,
        gueltig_bis: date | None = None,
        fachlicher_ersteller: str | None = None,
        freigabedatum: date | None = None,
        veroeffentlichungsdatum: date | None = None,
        script: str | None = None,
    ) -> Xdf2RuleFactory:
        if id is None:
            nummernkreis = nummernkreis or "00"
            id = f"R{nummernkreis}{get_unique_id()}"
        else:
            assert nummernkreis is None

        return Xdf2RuleFactory(
            self,
            id=id,
            version=version or "1.0",
            name=name or faker.name(),
            bezeichnung_eingabe=bezeichnung_eingabe,
            bezeichnung_ausgabe=bezeichnung_ausgabe,
            beschreibung=beschreibung,
            definition=definition,
            bezug=bezug,
            status=status,
            versionshinweis=versionshinweis,
            gueltig_ab=gueltig_ab,
            gueltig_bis=gueltig_bis,
            fachlicher_ersteller=fachlicher_ersteller,
            freigabedatum=freigabedatum,
            veroeffentlichungsdatum=veroeffentlichungsdatum,
            script=script,
        )


@dataclass(slots=True)
class Xdf2SchemaFactory:
    factory: Xdf2Factory

    id: str
    version: str
    name: str
    bezeichnung_eingabe: str | None
    bezeichnung_ausgabe: str | None
    beschreibung: str | None
    definition: str | None
    bezug: str | None
    versionshinweis: str | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    fachlicher_ersteller: str
    freigabedatum: date | None
    veroeffentlichungsdatum: date | None
    hilfetext: str | None
    regeln: list[xdf2.Identifier]
    struktur: list[xdf2.ElementReference]

    def get_nummernkreis(self) -> str:
        assert len(self.id) >= 3
        return self.id[1:3]

    def full(self) -> Xdf2SchemaFactory:
        self.bezeichnung_eingabe = self.bezeichnung_eingabe or faker.name()
        self.bezeichnung_ausgabe = self.bezeichnung_ausgabe or faker.name()
        self.beschreibung = self.beschreibung or faker.text()
        self.definition = self.definition or faker.text()
        self.bezug = self.bezug or faker.text()
        self.fachlicher_ersteller = self.fachlicher_ersteller or faker.name()
        self.freigabedatum = self.freigabedatum or faker.date_object()
        self.gueltig_ab = self.gueltig_ab or faker.date_object()
        self.gueltig_bis = self.gueltig_bis or faker.date_object()
        self.versionshinweis = self.versionshinweis or faker.text()
        self.veroeffentlichungsdatum = (
            self.veroeffentlichungsdatum or faker.date_object()
        )

        self.hilfetext = self.hilfetext or faker.text()

        if len(self.struktur) == 0:
            group = (
                self.factory.group(nummernkreis=self.get_nummernkreis()).full().build()
            )
            self.with_group(group)

        if len(self.regeln) == 0:
            rule = (
                self.factory.rule(nummernkreis=self.get_nummernkreis()).full().build()
            )
            self.with_rule(rule)

        return self

    def with_rule(self, rule: xdf2.Regel) -> Xdf2SchemaFactory:
        self.regeln.append(rule.identifier)
        return self

    def with_group(
        self,
        group: xdf2.Gruppe | None = None,
        anzahl: xdf2.Anzahl | None = None,
        bezug: str | None = None,
    ) -> Xdf2SchemaFactory:
        if anzahl is None:
            anzahl = xdf2.Anzahl(1, 1)

        if group is None:
            group = self.factory.group(nummernkreis=self.get_nummernkreis()).build()

        self.struktur.append(
            xdf2.ElementReference(
                anzahl=anzahl,
                bezug=bezug,
                element_type=xdf2.ElementType.GRUPPE,
                identifier=group.identifier,
            )
        )

        return self

    def with_field(
        self,
        field: xdf2.Datenfeld | None = None,
        anzahl: xdf2.Anzahl | None = None,
        bezug: str | None = None,
    ) -> Xdf2SchemaFactory:
        if anzahl is None:
            anzahl = xdf2.Anzahl(1, 1)

        if field is None:
            field = self.factory.field().build()

        self.struktur.append(
            xdf2.ElementReference(
                anzahl=anzahl,
                bezug=bezug,
                element_type=xdf2.ElementType.FELD,
                identifier=field.identifier,
            )
        )

        return self

    def build(self) -> xdf2.Schema:
        schema = xdf2.Schema(
            identifier=xdf2.Identifier(
                id=self.id, version=xdf2.parse_version(self.version)
            ),
            name=self.name,
            bezeichnung_eingabe=self.bezeichnung_eingabe,
            bezeichnung_ausgabe=self.bezeichnung_ausgabe,
            beschreibung=self.beschreibung,
            definition=self.definition,
            bezug=self.bezug,
            status=xdf2.Status.AKTIV,
            gueltig_ab=self.gueltig_ab,
            gueltig_bis=self.gueltig_bis,
            fachlicher_ersteller=self.fachlicher_ersteller,
            versionshinweis=self.versionshinweis,
            freigabedatum=self.freigabedatum,
            veroeffentlichungsdatum=self.veroeffentlichungsdatum,
            hilfetext=(
                din91379.parse_string_latin(self.hilfetext)
                if self.hilfetext is not None
                else None
            ),
            ableitungsmodifikationen_struktur=xdf2.AbleitungsmodifikationenStruktur.NICHT_MODIFIZIERBAR,
            ableitungsmodifikationen_repraesentation=xdf2.AbleitungsmodifikationenRepraesentation.NICHT_MODIFIZIERBAR,
            struktur=self.struktur,
            regeln=self.regeln,
        )

        self.factory.register_schema(schema)

        return schema

    def message(
        self, erstellungs_zeitpunkt: datetime | None = None
    ) -> xdf2.SchemaMessage:
        erstellungs_zeitpunkt = erstellungs_zeitpunkt or utc_now()

        schema = self.build()
        groups, fields, rules = self.factory.resolve_tree(
            schema.struktur, schema.regeln
        )

        return xdf2.SchemaMessage(
            header=xdf2.MessageHeader(
                nachricht_id="some_id",
                erstellungs_zeitpunkt=erstellungs_zeitpunkt,
                referenz_id=None,
            ),
            schema=schema,
            groups=groups,
            fields=fields,
            rules=rules,
        )

    def build_import(
        self,
        erstellungs_zeitpunkt: datetime | None = None,
        freigabe_status: xdf3.FreigabeStatus | None = None,
        steckbrief_id: str | None = None,
        code_lists: list[CodeListImport] | None = None,
    ) -> Xdf2SchemaImport:
        message = self.message(erstellungs_zeitpunkt)

        freigabe_status = (
            freigabe_status or xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
        )
        steckbrief_id = steckbrief_id or "D1"
        code_lists = code_lists or []

        return Xdf2SchemaImport(
            schema_text=xdf2.serialize_schema_message(message).decode("utf-8"),
            schema_message=message,
            freigabe_status=freigabe_status,
            steckbrief_id=steckbrief_id,
            code_list_imports=code_lists,
        )

    def save(
        self,
        runner: CommandRunner,
        strict: bool = True,
        erstellungs_zeitpunkt: datetime | None = None,
        freigabe_status: xdf3.FreigabeStatus | None = None,
        steckbrief_id: str | None = None,
        code_lists: list[CodeListImport] | None = None,
    ) -> FullSchemaOut:
        schema_import = self.build_import(
            erstellungs_zeitpunkt=erstellungs_zeitpunkt,
            freigabe_status=freigabe_status,
            steckbrief_id=steckbrief_id,
            code_lists=code_lists,
        )

        return runner.import_xdf2(schema_import, strict=strict)


@dataclass(slots=True)
class GroupFactory:
    factory: Xdf2Factory

    id: str
    version: str
    name: str
    bezeichnung_eingabe: str | None
    bezeichnung_ausgabe: str | None
    beschreibung: str | None
    definition: str | None
    bezug: str | None
    status: xdf2.Status
    fachlicher_ersteller: str | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    versionshinweis: str | None

    hilfetext_eingabe: str | None
    hilfetext_ausgabe: str | None
    schema_element_art: xdf2.SchemaElementArt

    freigabedatum: date | None
    veroeffentlichungsdatum: date | None

    regeln: list[xdf2.Identifier]
    struktur: list[xdf2.ElementReference]

    def get_nummernkreis(self) -> str:
        assert len(self.id) >= 3
        return self.id[1:3]

    def valid(self) -> GroupFactory:
        """
        Set default values so that the group does not trigger any warning.
        """
        self.bezeichnung_eingabe = self.bezeichnung_eingabe or self.name

        if len(self.struktur) == 0:
            self.with_field()

        return self

    def minimal(self) -> GroupFactory:
        self.bezeichnung_eingabe = None
        self.bezeichnung_ausgabe = None
        self.beschreibung = None
        self.definition = None
        self.bezug = None
        self.fachlicher_ersteller = None
        self.gueltig_ab = None
        self.gueltig_bis = None
        self.versionshinweis = None

        self.hilfetext_eingabe = None
        self.hilfetext_ausgabe = None

        self.regeln = []
        self.struktur = []

        return self

    def full(self) -> GroupFactory:
        self.bezeichnung_eingabe = self.bezeichnung_eingabe or faker.name()
        self.bezeichnung_ausgabe = self.bezeichnung_ausgabe or faker.name()
        self.beschreibung = self.beschreibung or faker.text()
        self.definition = self.definition or faker.text()
        self.bezug = self.bezug or faker.text()
        self.fachlicher_ersteller = self.fachlicher_ersteller or faker.name()
        self.gueltig_ab = self.gueltig_ab or faker.date_object()
        self.gueltig_bis = self.gueltig_bis or faker.date_object()
        self.versionshinweis = self.versionshinweis or faker.text()

        self.hilfetext_eingabe = self.hilfetext_eingabe or faker.text()
        self.hilfetext_ausgabe = self.hilfetext_ausgabe or faker.text()

        if len(self.struktur) == 0:
            field = (
                self.factory.field(nummernkreis=self.get_nummernkreis()).full().build()
            )
            self.with_field(field)

        if len(self.regeln) == 0:
            rule = (
                self.factory.rule(nummernkreis=self.get_nummernkreis()).full().build()
            )
            self.with_rule(rule)

        return self

    def with_bezeichnung_eingabe(self, value: str | None) -> GroupFactory:
        self.bezeichnung_eingabe = value
        return self

    def remove_children(self) -> GroupFactory:
        self.struktur = []
        return self

    def with_rule(self, rule: xdf2.Regel) -> GroupFactory:
        self.regeln.append(rule.identifier)
        return self

    def with_group(
        self,
        group: xdf2.Gruppe | None = None,
        anzahl: xdf2.Anzahl | None = None,
        bezug: str | None = None,
    ) -> GroupFactory:
        if anzahl is None:
            anzahl = xdf2.Anzahl(1, 1)

        if group is None:
            group = self.factory.group(nummernkreis=self.get_nummernkreis()).build()

        self.struktur.append(
            xdf2.ElementReference(
                anzahl=anzahl,
                bezug=bezug,
                element_type=xdf2.ElementType.GRUPPE,
                identifier=group.identifier,
            )
        )

        return self

    def with_field(
        self,
        field: xdf2.Datenfeld | None = None,
        anzahl: xdf2.Anzahl | None = None,
        bezug: str | None = None,
    ) -> GroupFactory:
        if field is None:
            field = self.factory.field(nummernkreis=self.get_nummernkreis()).build()

        if anzahl is None:
            anzahl = xdf2.Anzahl(1, 1)

        self.struktur.append(
            xdf2.ElementReference(
                anzahl=anzahl,
                bezug=bezug,
                element_type=xdf2.ElementType.FELD,
                identifier=field.identifier,
            )
        )

        return self

    def build(self) -> xdf2.Gruppe:
        group = xdf2.Gruppe(
            identifier=xdf2.Identifier(self.id, xdf2.parse_version(self.version)),
            name=self.name,
            bezeichnung_eingabe=self.bezeichnung_eingabe,
            bezeichnung_ausgabe=self.bezeichnung_ausgabe,
            beschreibung=self.beschreibung,
            definition=self.definition,
            bezug=self.bezug,
            status=self.status,
            gueltig_ab=self.gueltig_ab,
            gueltig_bis=self.gueltig_bis,
            fachlicher_ersteller=self.fachlicher_ersteller,
            versionshinweis=self.versionshinweis,
            freigabedatum=self.freigabedatum,
            veroeffentlichungsdatum=self.veroeffentlichungsdatum,
            hilfetext_eingabe=self.hilfetext_eingabe,
            hilfetext_ausgabe=self.hilfetext_ausgabe,
            schema_element_art=self.schema_element_art,
            struktur=self.struktur,
            regeln=self.regeln,
        )

        self.factory.register_group(group)

        return group

    def message(
        self, erstellungs_zeitpunkt: datetime | None = None
    ) -> xdf2.DatenfeldgruppeMessage:
        erstellungs_zeitpunkt = erstellungs_zeitpunkt or utc_now()

        group = self.build()
        groups, fields, rules = self.factory.resolve_tree(group.struktur, group.regeln)

        return xdf2.DatenfeldgruppeMessage(
            header=xdf2.MessageHeader(
                nachricht_id="some_id",
                erstellungs_zeitpunkt=erstellungs_zeitpunkt,
                referenz_id=None,
            ),
            group=group,
            groups=groups,
            fields=fields,
            rules=rules,
        )


@dataclass(slots=True)
class FieldFactory:
    factory: Xdf2Factory

    id: str
    version: str
    name: str
    bezeichnung_eingabe: str | None
    bezeichnung_ausgabe: str | None
    beschreibung: str | None
    definition: str | None
    bezug: str | None
    status: xdf2.Status
    fachlicher_ersteller: str | None
    freigabedatum: date | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    versionshinweis: str | None
    veroeffentlichungsdatum: date | None

    hilfetext_eingabe: str | None
    hilfetext_ausgabe: str | None
    schema_element_art: xdf2.SchemaElementArt

    regeln: list[xdf2.Regel]
    feldart: xdf2.Feldart
    datentyp: xdf2.Datentyp
    praezisierung: str | None = None
    inhalt: str | None = None
    code_list_referenz: xdf2.CodeListenReferenz | None = None

    def get_nummernkreis(self) -> str:
        assert len(self.id) >= 3
        return self.id[1:3]

    def valid(self) -> FieldFactory:
        """
        Set default values so that the field does not trigger any warning.
        """
        self.bezeichnung_eingabe = self.bezeichnung_eingabe or self.name

        return self

    def minimal(self) -> FieldFactory:
        self.bezeichnung_eingabe = None
        self.bezeichnung_ausgabe = None
        self.beschreibung = None
        self.definition = None
        self.bezug = None
        self.fachlicher_ersteller = None
        self.gueltig_ab = None
        self.gueltig_bis = None
        self.versionshinweis = None

        self.hilfetext_eingabe = None
        self.hilfetext_ausgabe = None

        self.regeln = []

        return self

    def full(self) -> FieldFactory:
        self.bezeichnung_eingabe = self.bezeichnung_eingabe or faker.name()
        self.bezeichnung_ausgabe = self.bezeichnung_ausgabe or faker.name()
        self.beschreibung = self.beschreibung or faker.text()
        self.definition = self.definition or faker.text()
        self.bezug = self.bezug or faker.text()
        self.fachlicher_ersteller = self.fachlicher_ersteller or faker.name()
        self.gueltig_ab = self.gueltig_ab or faker.date_object()
        self.gueltig_bis = self.gueltig_bis or faker.date_object()
        self.versionshinweis = self.versionshinweis or faker.text()

        self.hilfetext_eingabe = self.hilfetext_eingabe or faker.text()
        self.hilfetext_ausgabe = self.hilfetext_ausgabe or faker.text()

        if len(self.regeln) == 0:
            rule = (
                self.factory.rule(nummernkreis=self.get_nummernkreis()).full().build()
            )
            self.with_rule(rule)

        return self

    def with_bezeichnunug_eingabe(self, value: str | None) -> FieldFactory:
        self.bezeichnung_eingabe = value
        return self

    def with_bezeichnunug_ausgabe(self, value: str | None) -> FieldFactory:
        self.bezeichnung_ausgabe = value
        return self

    def with_code_list(
        self, code_list: genericode.CodeList | CodeListImport | None
    ) -> FieldFactory:
        if code_list is None:
            self.code_list_referenz = None
        else:
            if isinstance(code_list, CodeListImport):
                code_list = code_list.code_list

            self.code_list_referenz = xdf2.CodeListenReferenz(
                identifier=xdf2.CodeListIdentifier(
                    id=f"C00000{get_unique_id()}",
                    version=None,
                ),
                genericode_identifier=code_list.identifier,
            )

        return self

    def with_praezisierung(self, value: str | None) -> FieldFactory:
        self.praezisierung = value
        return self

    def with_rule(self, rule: xdf2.Regel) -> FieldFactory:
        self.regeln.append(rule)
        return self

    def input(
        self,
        datentyp: xdf2.Datentyp = xdf2.Datentyp.TEXT,
        praezisierung: str | None = None,
    ) -> FieldFactory:
        self.feldart = xdf2.Feldart.INPUT
        self.datentyp = datentyp
        self.praezisierung = praezisierung

        return self

    def select(
        self,
        code_list: genericode.CodeList | CodeListImport | None = None,
        datentyp: xdf2.Datentyp = xdf2.Datentyp.TEXT,
    ) -> FieldFactory:
        self.feldart = xdf2.Feldart.SELECT
        self.datentyp = datentyp

        if code_list is None:
            code_list = CodeListFactory().build()
        self.with_code_list(code_list)

        return self

    def label(self) -> FieldFactory:
        self.feldart = xdf2.Feldart.LABEL
        self.datentyp = xdf2.Datentyp.TEXT

        return self

    def build(self) -> xdf2.Datenfeld:
        field = xdf2.Datenfeld(
            identifier=xdf2.Identifier(self.id, xdf2.parse_version(self.version)),
            name=self.name,
            bezeichnung_eingabe=self.bezeichnung_eingabe,
            bezeichnung_ausgabe=self.bezeichnung_ausgabe,
            beschreibung=self.beschreibung,
            definition=self.definition,
            bezug=self.bezug,
            status=self.status,
            gueltig_ab=self.gueltig_ab,
            gueltig_bis=self.gueltig_bis,
            fachlicher_ersteller=self.fachlicher_ersteller,
            versionshinweis=self.versionshinweis,
            freigabedatum=self.freigabedatum,
            veroeffentlichungsdatum=self.veroeffentlichungsdatum,
            hilfetext_eingabe=self.hilfetext_eingabe,
            hilfetext_ausgabe=self.hilfetext_ausgabe,
            schema_element_art=self.schema_element_art,
            feldart=self.feldart,
            datentyp=self.datentyp,
            praezisierung=self.praezisierung,
            inhalt=self.inhalt,
            code_listen_referenz=self.code_list_referenz,
            regeln=[regel.identifier for regel in self.regeln],
        )

        self.factory.register_field(field)
        return field

    def message(
        self,
        erstellungs_zeitpunkt: datetime | None = None,
    ) -> xdf2.DatenfeldMessage:
        erstellungs_zeitpunkt = erstellungs_zeitpunkt or utc_now()

        field = self.build()
        rules = self.factory.resolve_rules(field.regeln)

        return xdf2.DatenfeldMessage(
            header=xdf2.MessageHeader(
                nachricht_id="some_id",
                erstellungs_zeitpunkt=erstellungs_zeitpunkt,
                referenz_id=None,
            ),
            field=field,
            rules=rules,
        )


@dataclass(slots=True)
class Xdf2RuleFactory:
    factory: Xdf2Factory

    id: str
    version: str
    name: str
    bezeichnung_eingabe: str | None
    bezeichnung_ausgabe: str | None
    beschreibung: str | None
    definition: str | None
    bezug: str | None
    status: xdf2.Status
    versionshinweis: str | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    fachlicher_ersteller: str | None
    freigabedatum: date | None
    veroeffentlichungsdatum: date | None
    script: str | None

    def full(self) -> Xdf2RuleFactory:
        """
        Fill all optional fields with values.
        """

        self.bezeichnung_eingabe = self.bezeichnung_eingabe or faker.text()
        self.bezeichnung_ausgabe = self.bezeichnung_ausgabe or faker.text()
        self.beschreibung = self.beschreibung or faker.text()
        self.definition = self.definition or faker.text()
        self.bezug = self.bezug or faker.text()
        self.versionshinweis = self.versionshinweis or faker.text()
        self.gueltig_ab = self.gueltig_ab or faker.date_object()
        self.gueltig_bis = self.gueltig_bis or faker.date_object()
        self.fachlicher_ersteller = self.fachlicher_ersteller or faker.text()
        self.freigabedatum = self.freigabedatum or faker.date_object()
        self.veroeffentlichungsdatum = (
            self.veroeffentlichungsdatum or faker.date_object()
        )
        self.script = self.script or faker.text()

        return self

    def build(self) -> xdf2.Regel:
        rule = xdf2.Regel(
            identifier=xdf2.Identifier(self.id, xdf2.parse_version(self.version)),
            name=self.name,
            bezeichnung_eingabe=self.bezeichnung_eingabe,
            bezeichnung_ausgabe=self.bezeichnung_ausgabe,
            beschreibung=self.beschreibung,
            definition=self.definition,
            bezug=self.bezug,
            status=self.status,
            versionshinweis=self.versionshinweis,
            gueltig_ab=self.gueltig_ab,
            gueltig_bis=self.gueltig_bis,
            fachlicher_ersteller=self.fachlicher_ersteller,
            freigabedatum=self.freigabedatum,
            veroeffentlichungsdatum=self.veroeffentlichungsdatum,
            script=self.script,
        )

        self.factory.register_rule(rule)
        return rule
