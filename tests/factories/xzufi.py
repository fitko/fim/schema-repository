from __future__ import annotations
from datetime import datetime

from faker import Faker
from fimportal.common import FreigabeStatus, RawCode
from fimportal.models.xzufi import (
    FullLeistungssteckbriefOut,
    FullLeistungsbeschreibungOut,
    Link,
    OnlinedienstOut,
    OrganisationseinheitOut,
    SpezialisierungOut,
    ZustaendigkeitOut,
)
from fimportal.xzufi.client import TEST_LEISTUNG_XML
from fimportal.xzufi.common import (
    GERMAN,
    HyperlinkErweitert,
    Identifikator,
    LeistungRedaktionId,
    Leistungsschluessel,
    ModulTextTyp,
    RedaktionsId,
    Sprachversion,
    StringLocalized,
    Versionsinformation,
    Vertrauensniveau,
)

from fimportal.xzufi.leistung import (
    LEIKA_SCHEME_ID,
    Bearbeitungsdauermodul,
    BegriffImKontext,
    BegriffImKontextModul,
    FristDauer,
    FristOhneTyp,
    Klassifizierung,
    Leistung,
    Leistungsstruktur,
    LeistungsTypisierung,
    Textmodul,
    TextmodulIndividuell,
    Zeiteinheit,
)
from fimportal.xzufi.onlinedienst import Onlinedienst
from fimportal.xzufi.organisationseinheit import Organisationseinheit
from fimportal.xzufi.spezialisierung import Spezialisierung
from fimportal.xzufi.zustaendigkeit import Zustaendigkeit
from tests.conftest import CommandRunner
from tests.factories.common import AutoGenerate, get_unique_id


faker = Faker()


class TextmodulFactory:
    modul_typ: ModulTextTyp
    inhalt: list[StringLocalized]
    links: list[HyperlinkErweitert]

    def __init__(
        self,
        modul_typ: ModulTextTyp = ModulTextTyp.LEISTUNGSBEZEICHNUNG,
        inhalt: list[StringLocalized] | None = None,
        links: list[HyperlinkErweitert] | None = None,
    ):
        self.modul_typ = modul_typ
        self.inhalt = inhalt or [StringLocalized(GERMAN, faker.name())]
        self.links = links or []

    def with_link(self, link: HyperlinkErweitert | str | None):
        if link is None:
            self.links.append(
                HyperlinkErweitert(
                    None, faker.uri(), "Link Titel", "Link Beschreibung", 1
                )
            )
        elif isinstance(link, str):
            self.links.append(
                HyperlinkErweitert(None, link, "Link Titel", "Link Beschreibung", 1)
            )
        else:
            self.links.append(link)
        return self

    def build(self):
        return Textmodul(1, None, [], [], self.modul_typ, [], self.inhalt, self.links)


class LeistungFactory:
    id: Identifikator
    struktur: Leistungsstruktur | None
    typisierung: list[LeistungsTypisierung]
    leistungsschluessel: list[Leistungsschluessel]
    bezeichnung: Textmodul | None
    bezeichnung_2: Textmodul | None
    volltext: Textmodul | None
    kurztext: Textmodul | None
    rechtsgrundlagen: Textmodul | None
    urheber: Textmodul | None
    begriff_im_kontext: list[BegriffImKontextModul]
    bearbeitungsdauer: Bearbeitungsdauermodul | None = None
    sdg_informationsbereiche: list[str]
    klassifizierung: list[Klassifizierung]
    modul_text_individuell: list[TextmodulIndividuell]
    versionsinformation: Versionsinformation | None
    einheitlicher_ansprechpartner: bool | None

    def __init__(
        self,
        freigabestatus_katalog: FreigabeStatus | None = None,
        freigabestatus_bibliothek: FreigabeStatus | None = None,
        bezeichnung: Textmodul | str | None | AutoGenerate = AutoGenerate(),
        bezeichnung_2: Textmodul | str | None | AutoGenerate = AutoGenerate(),
        struktur: Leistungsstruktur | None = None,
        redaktion_id: str | None = None,
        leistung_id: str | None = None,
        kurztext: str | None = None,
        volltext: Textmodul | str | None = None,
        urheber: Textmodul | str | None = None,
        begriff_im_kontext: str | None = None,
        bearbeitungsdauer: tuple[int, Zeiteinheit] | None = None,
        rechtsgrundlagen: Textmodul | str | None = None,
        einheitlicher_ansprechpartner: bool | None = None,
    ):
        redaktion_id = redaktion_id or str(get_unique_id())
        leistung_id = leistung_id or str(get_unique_id())
        self.id = Identifikator(
            scheme_agency_id=redaktion_id,
            scheme_agency_name=None,
            scheme_data_uri=None,
            scheme_id=None,
            scheme_name=None,
            scheme_uri=None,
            scheme_version_id=None,
            value=leistung_id,
        )

        self.struktur = struktur
        self.typisierung = []
        self.leistungsschluessel = []
        self.text_module = []
        self.sdg_informationsbereiche = []
        self.klassifizierung = []
        self.modul_text_individuell = []
        self.versionsinformation = None

        if freigabestatus_katalog is not None:
            self.with_freigabestatus_katalog(freigabestatus_katalog)

        if freigabestatus_bibliothek is not None:
            self.with_freigabestatus_bibliothek(freigabestatus_bibliothek)

        if isinstance(bezeichnung, str):
            self.bezeichnung = TextmodulFactory(
                modul_typ=ModulTextTyp.LEISTUNGSBEZEICHNUNG,
                inhalt=[StringLocalized(GERMAN, bezeichnung)],
            ).build()
        elif isinstance(bezeichnung, AutoGenerate):
            self.bezeichnung = TextmodulFactory(
                modul_typ=ModulTextTyp.LEISTUNGSBEZEICHNUNG,
                inhalt=[StringLocalized(GERMAN, faker.name())],
            ).build()
        else:
            self.bezeichnung = bezeichnung

        if isinstance(bezeichnung_2, str):
            self.bezeichnung_2 = TextmodulFactory(
                modul_typ=ModulTextTyp.LEISTUNGSBEZEICHNUNG_II,
                inhalt=[StringLocalized(GERMAN, bezeichnung_2)],
            ).build()
        elif isinstance(bezeichnung_2, AutoGenerate):
            self.bezeichnung_2 = TextmodulFactory(
                modul_typ=ModulTextTyp.LEISTUNGSBEZEICHNUNG_II,
                inhalt=[StringLocalized(GERMAN, faker.name())],
            ).build()
        else:
            self.bezeichnung_2 = bezeichnung_2

        if kurztext is not None:
            self.kurztext = TextmodulFactory(
                modul_typ=ModulTextTyp.KURZTEXT,
                inhalt=[StringLocalized(GERMAN, kurztext)],
            ).build()
        else:
            self.kurztext = None

        if isinstance(volltext, str):
            self.volltext = TextmodulFactory(
                modul_typ=ModulTextTyp.VOLLTEXT,
                inhalt=[StringLocalized(GERMAN, volltext)],
            ).build()
        else:
            self.volltext = volltext

        if isinstance(rechtsgrundlagen, str):
            self.rechtsgrundlagen = TextmodulFactory(
                modul_typ=ModulTextTyp.RECHTSGRUNDLAGEN,
                inhalt=[StringLocalized(GERMAN, rechtsgrundlagen)],
            ).build()
        else:
            self.rechtsgrundlagen = rechtsgrundlagen

        if isinstance(urheber, str):
            self.urheber = TextmodulFactory(
                modul_typ=ModulTextTyp.URHEBER,
                inhalt=[StringLocalized(GERMAN, urheber)],
            ).build()
        else:
            self.urheber = urheber

        self.begriff_im_kontext = []
        if begriff_im_kontext is not None:
            self.begriff_im_kontext.append(
                BegriffImKontextModul(
                    position_darstellung=None,
                    id=None,
                    id_sekundaer=[],
                    gueltigkeit=[],
                    begriff_im_kontext=[
                        BegriffImKontext(
                            begriff=StringLocalized(GERMAN, begriff_im_kontext),
                            typ=None,
                        )
                    ],
                )
            )

        if bearbeitungsdauer is not None:
            self.bearbeitungsdauer = Bearbeitungsdauermodul(
                position_darstellung=None,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                bearbeitungsdauer=[
                    FristOhneTyp(
                        fristauswahl=FristDauer(
                            dauer=bearbeitungsdauer[0],
                            einheit=bearbeitungsdauer[1],
                            dauer_bis=None,
                            einheit_bis=None,
                        ),
                        beschreibung=[
                            StringLocalized(GERMAN, "some frist beschreibung")
                        ],
                        position_darstellung=None,
                        gueltigkeit=[],
                    )
                ],
                beschreibung=[StringLocalized(GERMAN, "Bearbeitungsdauerbeschreibung")],
                weiterfuehrender_link=[],
            )

        self.einheitlicher_ansprechpartner = einheitlicher_ansprechpartner

    @staticmethod
    def steckbrief(
        bezeichnung: str | None = None,
        bezeichnung_2: str | None = None,
        leistungsschluessel: str | None = None,
        rechtsgrundlagen: str | None = None,
        typisierung: LeistungsTypisierung = LeistungsTypisierung.TYP_2,
    ) -> LeistungFactory:
        return (
            LeistungFactory(
                bezeichnung=bezeichnung,
                bezeichnung_2=bezeichnung_2,
                rechtsgrundlagen=rechtsgrundlagen,
                redaktion_id=LeistungRedaktionId.LEIKA.value,
                freigabestatus_bibliothek=None,
            )
            .add_leistungsschluessel(
                leistungsschluessel or generate_leistungsschluessel()
            )
            .add_typisierung(typisierung)
        )

    @staticmethod
    def stammtext(
        leistungsschluessel: str | None = None,
        typisierung: LeistungsTypisierung = LeistungsTypisierung.TYP_2,
        freigabestatus_bibliothek: (
            FreigabeStatus | None
        ) = FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
    ) -> LeistungFactory:
        return (
            LeistungFactory(
                redaktion_id=LeistungRedaktionId.LEIKA.value,
                freigabestatus_bibliothek=freigabestatus_bibliothek,
            )
            .add_leistungsschluessel(
                leistungsschluessel or generate_leistungsschluessel()
            )
            .add_typisierung(typisierung)
        )

    @staticmethod
    def leistungsbeschreibung() -> LeistungFactory:
        return LeistungFactory(redaktion_id=RedaktionsId("not TSA TELEPORT"))

    def with_einheitlicher_ansprechpartner(self, value: bool | None):
        self.einheitlicher_ansprechpartner = value
        return self

    def with_freigabestatus_katalog(
        self, freigabestatus: FreigabeStatus
    ) -> LeistungFactory:
        self.modul_text_individuell.append(
            TextmodulIndividuell(
                id=None,
                id_sekundaer=[],
                position_darstellung=None,
                gueltigkeit=[],
                individuelles_textmodultyp=RawCode(
                    list_uri="individuell",
                    list_version_id="unbestimmt",
                    code="STATUSKATALOG",
                ),
                inhalt=[StringLocalized("de", str(freigabestatus.value))],
                weiterfuehrender_link=[],
            )
        )

        return self

    def with_freigabestatus_bibliothek(
        self, freigabestatus: FreigabeStatus
    ) -> LeistungFactory:
        self.modul_text_individuell.append(
            TextmodulIndividuell(
                id=None,
                id_sekundaer=[],
                position_darstellung=None,
                gueltigkeit=[],
                individuelles_textmodultyp=RawCode(
                    list_uri="individuell",
                    list_version_id="unbestimmt",
                    code="STATUS",
                ),
                inhalt=[StringLocalized("de", str(freigabestatus.value))],
                weiterfuehrender_link=[],
            )
        )

        return self

    def with_bezeichnung(self, bezeichnung: str | Textmodul | None) -> LeistungFactory:
        if isinstance(bezeichnung, str):
            self.bezeichnung = TextmodulFactory(
                modul_typ=ModulTextTyp.LEISTUNGSBEZEICHNUNG,
                inhalt=[StringLocalized(GERMAN, bezeichnung)],
            ).build()
        elif isinstance(bezeichnung, AutoGenerate):
            self.bezeichnung = TextmodulFactory(
                modul_typ=ModulTextTyp.LEISTUNGSBEZEICHNUNG,
                inhalt=[StringLocalized(GERMAN, faker.name())],
            ).build()
        else:
            self.bezeichnung = bezeichnung

        return self

    def with_leistungsbezeichnung_II(
        self, leistungsbezeichnung_II: str | Textmodul | None
    ) -> LeistungFactory:
        if isinstance(leistungsbezeichnung_II, str):
            self.bezeichnung_2 = TextmodulFactory(
                modul_typ=ModulTextTyp.LEISTUNGSBEZEICHNUNG_II,
                inhalt=[StringLocalized(GERMAN, leistungsbezeichnung_II)],
            ).build()
        elif isinstance(leistungsbezeichnung_II, AutoGenerate):
            self.bezeichnung_2 = TextmodulFactory(
                modul_typ=ModulTextTyp.LEISTUNGSBEZEICHNUNG_II,
                inhalt=[StringLocalized(GERMAN, faker.name())],
            ).build()
        else:
            self.bezeichnung_2 = leistungsbezeichnung_II

        return self

    def with_versionsinformation(
        self,
        erstellt_datum_zeit: datetime | None = None,
        geaendert_datum_zeit: datetime | None = None,
    ) -> LeistungFactory:
        self.versionsinformation = Versionsinformation(
            erstellt_datum_zeit=erstellt_datum_zeit,
            erstellt_durch=None,
            geaendert_datum_zeit=geaendert_datum_zeit,
            geaendert_durch=None,
            version=None,
        )

        return self

    def with_struktur(self, struktur: Leistungsstruktur | None) -> LeistungFactory:
        self.struktur = struktur

        return self

    def with_rechtsgrundlagen(
        self, rechtsgrundlagen: Textmodul | str | None
    ) -> LeistungFactory:
        if isinstance(rechtsgrundlagen, str):
            self.rechtsgrundlagen = TextmodulFactory(
                modul_typ=ModulTextTyp.RECHTSGRUNDLAGEN,
                inhalt=[StringLocalized(GERMAN, rechtsgrundlagen)],
            ).build()
        else:
            self.rechtsgrundlagen = rechtsgrundlagen

        return self

    def add_typisierung(self, typ: LeistungsTypisierung) -> LeistungFactory:
        self.typisierung.append(typ)
        return self

    def add_klassifizierung(self, list_uri: str, value: str) -> LeistungFactory:
        self.klassifizierung.append(Klassifizierung(list_uri=list_uri, value=value))
        return self

    def add_leistungsschluessel(
        self, referenz: str | Leistungsschluessel
    ) -> LeistungFactory:
        if isinstance(referenz, str):
            referenz = Leistungsschluessel.from_str(referenz)

        self.leistungsschluessel.append(referenz)
        return self

    def add_sdg_informationsbereich(self, value: str) -> LeistungFactory:
        self.sdg_informationsbereiche.append(value)
        return self

    def build(self) -> Leistung:
        modul_text: list[Textmodul] = []
        if self.bezeichnung is not None:
            modul_text.append(self.bezeichnung)
        if self.bezeichnung_2 is not None:
            modul_text.append(self.bezeichnung_2)
        if self.kurztext is not None:
            modul_text.append(self.kurztext)
        if self.volltext is not None:
            modul_text.append(self.volltext)
        if self.rechtsgrundlagen is not None:
            modul_text.append(self.rechtsgrundlagen)

        return Leistung(
            id=self.id,
            id_sekundaer=[
                Identifikator(
                    scheme_agency_id=None,
                    scheme_agency_name=None,
                    scheme_data_uri=None,
                    scheme_name=None,
                    scheme_uri=None,
                    scheme_version_id=None,
                    scheme_id=LEIKA_SCHEME_ID,
                    value=str(item),
                )
                for item in self.leistungsschluessel
            ],
            struktur=self.struktur,
            referenz_leika=[],
            modul_text=modul_text,
            modul_text_individuell=self.modul_text_individuell,
            modul_frist=None,
            modul_kosten=None,
            modul_bearbeitungsdauer=self.bearbeitungsdauer,
            modul_begriff_im_kontext=self.begriff_im_kontext,
            modul_fachliche_freigabe=None,
            modul_auskunftshinweis=None,
            modul_dokument=None,
            modul_ursprungsportal=[],
            typisierung=self.typisierung,
            vertrauensniveau=None,
            leistungsadressat=[],
            kennzeichen_schritformerfordernis=None,
            kategorie=[],
            klassifizierung=self.klassifizierung,
            informationsbereich_sdg=self.sdg_informationsbereiche,
            id_leistung_im_kontext=[],
            id_prozess=[],
            herausgeber=None,
            gueltigkeit_gebiet_id=[],
            kennzeichen_ea=self.einheitlicher_ansprechpartner,
            relevant_fuer_wirtschaftszweig=[],
            relevant_fuer_rechtsform=[],
            relevant_fuer_staatsangehoerigkeit=[],
            versionsinformation=self.versionsinformation,
            sprachversion=[
                Sprachversion(
                    language_code=GERMAN,
                    sprachbezeichnung_deutsch=None,
                    sprachbezeichnung_nativ=None,
                    erstellt_datum_zeit=None,
                    erstellt_durch=None,
                    geaendert_datum_zeit=None,
                    geaendert_durch=None,
                    version=None,
                )
            ],
            gueltigkeit=[],
        )

    def save_pvog(
        self,
        runner: CommandRunner,
        xml_content: str = TEST_LEISTUNG_XML,
    ) -> FullLeistungsbeschreibungOut:
        leistung = self.build()
        return runner.import_pvog(
            leistung,
            xml_content=xml_content,
        )

    def save_leika(
        self,
        runner: CommandRunner,
        xml_content: str = TEST_LEISTUNG_XML,
    ) -> FullLeistungssteckbriefOut:
        leistung = self.build()
        return runner.import_leika(
            leistung,
            xml_content=xml_content,
        )


leistungsschluessel_counter: int = 0


def generate_leistungsschluessel() -> str:
    global leistungsschluessel_counter
    leistungsschluessel_counter += 1

    # Make sure only the last three digits are filled
    leistungsschluessel = leistungsschluessel_counter % 1000

    return f"99123123123{leistungsschluessel:03}"


class LinkFactory:
    uri: str
    last_checked: datetime | None
    last_online: datetime | None

    def __init__(
        self,
        uri: str | None = None,
        last_checked: datetime | None = None,
        last_online: datetime | None = None,
    ):
        self.uri = uri or faker.uri()
        self.last_checked = last_checked
        self.last_online = last_online

    def build(self):
        return Link(
            uri=self.uri, last_checked=self.last_checked, last_online=self.last_online
        )


TEST_SPEZIALISIERUNG_XML = """
<xzufi:spezialisierung xmlns:xzufi="http://xoev.de/schemata/xzufi/2_2_0"
    xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink">
    <xzufi:idLeistung schemeAgencyID="L100001" schemeID="OID">8967107</xzufi:idLeistung>
    <xzufi:id schemeAgencyID="L100001" schemeID="OID">10665799_8955200</xzufi:id>
</xzufi:spezialisierung>
"""


class SpezialisierungFactory:
    id: Identifikator
    leistung: Identifikator

    def __init__(
        self,
        redaktion_id: str | None = None,
        id: str | None = None,
        leistung_redaktion_id: str | None = None,
        leistung_id: str | None = None,
    ):
        redaktion_id = redaktion_id or str(get_unique_id())
        id = id or str(get_unique_id())
        self.id = Identifikator(
            scheme_agency_id=redaktion_id,
            scheme_agency_name=None,
            scheme_data_uri=None,
            scheme_id=None,
            scheme_name=None,
            scheme_uri=None,
            scheme_version_id=None,
            value=id,
        )

        leistung_id = leistung_id or str(get_unique_id())
        leistung_redaktion_id = leistung_redaktion_id or str(get_unique_id())
        self.leistung = Identifikator(
            scheme_agency_id=leistung_redaktion_id,
            scheme_agency_name=None,
            scheme_data_uri=None,
            scheme_id=None,
            scheme_name=None,
            scheme_uri=None,
            scheme_version_id=None,
            value=leistung_id,
        )

    def build(self) -> Spezialisierung:
        return Spezialisierung(
            id=self.id,
            id_leistung=self.leistung,
            id_gebiet=[],
            id_sekundaer=[],
            bezeichnung=[],
            modul_spezialisiert=[],
        )

    def save(
        self,
        runner: CommandRunner,
        xml_content: str = TEST_SPEZIALISIERUNG_XML,
    ) -> SpezialisierungOut:
        spezialisierung = self.build()
        return runner.import_spezialisierung(
            spezialisierung,
            xml_content=xml_content,
        )


TEST_ORGANISATIONSEINHEIT_XML = """
<xzufi:organisationseinheit xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xzufi="http://xoev.de/schemata/xzufi/2_2_0"
    xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink"
    xsi:type="xzufi:OrganisationseinheitErweitert">
            <xzufi:id schemeAgencyID="S100003">S1000030000011895</xzufi:id>
</xzufi:organisationseinheit>
"""


class OrganisationseinheitFactory:
    id: Identifikator

    def __init__(self, redaktion_id: str | None = None, id: str | None = None):
        redaktion_id = redaktion_id or str(get_unique_id())
        id = id or str(get_unique_id())
        self.id = Identifikator(
            scheme_agency_id=redaktion_id,
            scheme_agency_name=None,
            scheme_data_uri=None,
            scheme_id=None,
            scheme_name=None,
            scheme_uri=None,
            scheme_version_id=None,
            value=id,
        )

    def build(self):
        return Organisationseinheit(
            id=self.id,
            id_sekundaer=[],
            externe_organisationseinheitsermittlung=False,
            uebergeordnete_organisationseinheit_id=None,
            untergeordnete_organisationseinheit_id=[],
            alternative_hierarchie=[],
            kategorie=[],
            name=[],
            beschreibung=[],
            kurzbeschreibung=[],
            info_oeffnungszeiten_text=[],
            info_oeffnungszeiten_struktur=[],
            info_intern_servicecenter=[],
            info_sonstige=[],
            anschrift=[],
            erreichbarkeit=[],
            kommunikationssystem=[],
            internetaddresse=[],
            bankverbindung=[],
            glaeubiger_identifikationsnummer=None,
            zahlungsweise=[],
            zahlungsweise_text=[],
            synonym=[],
            herausgeber=None,
            versions_informationen=None,
            sprachversion=[],
            gueltigkeit=[],
        )

    def save(
        self,
        runner: CommandRunner,
        xml_content: str = TEST_ORGANISATIONSEINHEIT_XML,
    ) -> OrganisationseinheitOut:
        orga_einheit = self.build()
        return runner.import_organisationseinheit(
            orga_einheit,
            xml_content=xml_content,
        )


TEST_ZUSTAENDIGKEIT_XML = """
<xzufi:zustaendigkeit xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:type="xzufi:ZustaendigkeitOrganisationseinheit">
    <xzufi:id schemeAgencyID="L100042" schemeID="ZustaendigkeitOrganisationseinheit">
        88162.44432.095750153000</xzufi:id>
</xzufi:zustaendigkeit>
"""


class ZustaendigkeitFactory:
    id: Identifikator

    def __init__(self, redaktion_id: str | None = None, id: str | None = None):
        redaktion_id = redaktion_id or str(get_unique_id())
        id = id or str(get_unique_id())
        self.id = Identifikator(
            scheme_agency_id=redaktion_id,
            scheme_agency_name=None,
            scheme_data_uri=None,
            scheme_id=None,
            scheme_name=None,
            scheme_uri=None,
            scheme_version_id=None,
            value=id,
        )

    def build(self):
        return Zustaendigkeit(
            id=self.id,
            id_sekundaer=[],
            leistung_id=[],
            gebiet_id=[],
            weitere_informationen=[],
            herausgeber=None,
            gueltigkeit=[],
        )

    def save(
        self,
        runner: CommandRunner,
        xml_content: str = TEST_ZUSTAENDIGKEIT_XML,
    ) -> ZustaendigkeitOut:
        zustaendigkeit = self.build()
        return runner.import_zustaendigkeit(
            zustaendigkeit,
            xml_content=xml_content,
        )


TEST_ONLINEDIENST_XML = """
<xzufi:onlinedienst xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xzufi="http://xoev.de/schemata/xzufi/2_2_0" xmlns:gml="http://www.opengis.net/gml/3.2"
  xmlns:xlink="http://www.w3.org/1999/xlink" xsi:type="xzufi:OnlinedienstErweitert">
  <xzufi:id schemeAgencyID="L100012" schemeID="OID">280810655</xzufi:id>
  <xzufi:vertrauensniveau listURI="urn:xoev-de:fim:codeliste:vertrauensniveau" listVersionID="4">
    <code>15</code>
  </xzufi:vertrauensniveau>
</xzufi:onlinedienst>
"""


class OnlinedienstFactory:
    id: Identifikator

    def __init__(self, redaktion_id: str | None = None, id: str | None = None):
        redaktion_id = redaktion_id or str(get_unique_id())
        id = id or str(get_unique_id())
        self.id = Identifikator(
            scheme_agency_id=redaktion_id,
            scheme_agency_name=None,
            scheme_data_uri=None,
            scheme_id=None,
            scheme_name=None,
            scheme_uri=None,
            scheme_version_id=None,
            value=id,
        )

    def build(self):
        return Onlinedienst(
            id=self.id,
            id_sekundaer=[],
            bezeichnung=[],
            kurzbezeichnung=[],
            beschreibung=[],
            teaser=[],
            link=[],
            parameter=[],
            durchfuehrung_staatsangehoerigkeit=[],
            durchfuehrung_sprachen=[],
            zahlungsweise=[],
            vertrauensniveau=Vertrauensniveau.BASISREGISTRIERUNG,
            identifizierungsmittel=[],
            logo=[],
            hilfe_link=[],
            hilfe_text=[],
            herausgeber=None,
            versionsinformationen=None,
            sprachversion=[],
            gueltigkeit=[],
        )

    def save(
        self,
        runner: CommandRunner,
        xml_content: str = TEST_ONLINEDIENST_XML,
    ) -> OnlinedienstOut:
        onlinedienst = self.build()
        return runner.import_onlinedienst(
            onlinedienst,
            xml_content=xml_content,
        )
