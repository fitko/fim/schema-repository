from __future__ import annotations

from dataclasses import dataclass
from datetime import date, datetime, timezone
from typing import Iterable

from faker import Faker

from fimportal import din91379, genericode
from fimportal.din91379 import parse_optional_string_latin, parse_string_latin
from fimportal.helpers import utc_now
from fimportal.models.imports import Xdf3SchemaImport
from fimportal.models.xdf3 import FullSchemaOut
from fimportal.xdatenfelder import xdf2, xdf3
from tests.conftest import CommandRunner

from .common import get_unique_id

faker = Faker()


TEST_STECKBRIEF_XML = """
<xdf3:xdatenfelder.dokumentsteckbrief.0101 xmlns:xdf3="urn:xoev-de:fim:standard:xdatenfelder_3.0.0">
    <xdf3:header>
        <xdf3:nachrichtID>db723d0c-67ab-486b-8178-2f33a4c5b3e6</xdf3:nachrichtID>
        <xdf3:erstellungszeitpunkt>2024-06-27T15:40:36+02:00</xdf3:erstellungszeitpunkt>
        <xdf3:produktbezeichnung>FRED3</xdf3:produktbezeichnung>
        <xdf3:produkthersteller>FJD Information Technologies AG</xdf3:produkthersteller>
        <xdf3:produktversion>1.13.0</xdf3:produktversion>
        <xdf3:datenqualitaet listURI="urn:xoev-de:fim:codeliste:datenfelder.datenqualitaet"
            listVersionID="1.0">
            <code>TEST</code>
        </xdf3:datenqualitaet>
    </xdf3:header>
    <xdf3:dokumentsteckbrief>
        <xdf3:identifikation>
            <xdf3:id>D02000000001</xdf3:id>
            <xdf3:version>1.0.0</xdf3:version>
        </xdf3:identifikation>
        <xdf3:beschreibung>Beschreibung des Dokumentensteckbriefs</xdf3:beschreibung>
        <xdf3:name>abstrakter Dokumentsteckbrief</xdf3:name>
        <xdf3:definition>Definition des Berichts</xdf3:definition>
        <xdf3:freigabestatus listURI="urn:xoev-de:xprozess:codeliste:status"
            listVersionID="2020-03-19">
            <code>2</code>
        </xdf3:freigabestatus>
        <xdf3:statusGesetztDurch>Feldertest</xdf3:statusGesetztDurch>
        <xdf3:statusGesetztAm>2019-08-27</xdf3:statusGesetztAm>
        <xdf3:versionshinweis>Version erstellt ohne Angabe eines Versionshinweises.</xdf3:versionshinweis>
        <xdf3:letzteAenderung>2024-06-27T13:40:20+00:00</xdf3:letzteAenderung>
        <xdf3:gueltigAb>2019-09-01</xdf3:gueltigAb>
        <xdf3:gueltigBis>2020-09-01</xdf3:gueltigBis>
        <xdf3:veroeffentlichungsdatum>2019-08-27</xdf3:veroeffentlichungsdatum>
        <xdf3:stichwort uri="Anwendungsgebiet">Freie und Hansestadt Hamburg</xdf3:stichwort>
        <xdf3:istAbstrakt>true</xdf3:istAbstrakt>
        <xdf3:dokumentart listURI="urn:xoev-de:fim-datenfelder:codeliste:dokumentart"
            listVersionID="2022-01-01">
            <code>003</code>
        </xdf3:dokumentart>
        <xdf3:bezeichnung>abstrakter Dokumentsteckbrief</xdf3:bezeichnung>
        <xdf3:bezug link="some link">Bezug</xdf3:bezug>
        <xdf3:hilfetext>Ein Hilfetext</xdf3:hilfetext>
    </xdf3:dokumentsteckbrief>
</xdf3:xdatenfelder.dokumentsteckbrief.0101>
"""


class Xdf3SteckbriefMessageFactory:
    id: str
    version: xdf3.Version
    name: str
    beschreibung: str | None
    definition: str | None
    bezug: list[xdf3.Rechtsbezug]
    freigabe_status: xdf3.FreigabeStatus
    status_gesetzt_am: date | None
    status_gesetzt_durch: str | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    versionshinweis: str | None
    veroeffentlichungsdatum: date | None
    letzte_aenderung: datetime
    relation: list[xdf3.Relation]
    stichwort: list[xdf3.Stichwort]
    ist_abtrakt: bool
    dokumentart: xdf3.Dokumentart
    bezeichnung: str
    hilfetext: str | None

    def __init__(
        self,
        id: str | None = None,
        version: str | None = None,
        name: str | None = None,
        beschreibung: str | None = None,
        definition: str | None = None,
        freigabe_status: xdf3.FreigabeStatus = xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        status_gesetzt_am: date | None = None,
        status_gesetzt_durch: str | None = None,
        gueltig_ab: date | None = None,
        gueltig_bis: date | None = None,
        versionshinweis: str | None = None,
        veroeffentlichungsdatum: date | None = None,
        relation: list[xdf3.Relation] | None = None,
        stichwort: list[xdf3.Stichwort] | None = None,
        letzte_aenderung: datetime | None = None,
        ist_abstrakt: bool = False,
        dokumentart: xdf3.Dokumentart = xdf3.Dokumentart.UNBESTIMMT,
        bezeichnung: str | None = None,
        hilfetext: str | None = None,
    ):
        self.id = id or f"D00000{get_unique_id()}"
        version = xdf3.parse_version(version or "1.0.0")
        assert version is not None
        self.version = version
        self.name = name or faker.name()
        self.beschreibung = beschreibung
        self.definition = definition
        self.bezug = []
        self.freigabe_status = freigabe_status
        self.status_gesetzt_am = status_gesetzt_am
        self.status_gesetzt_durch = status_gesetzt_durch
        self.gueltig_ab = gueltig_ab
        self.gueltig_bis = gueltig_bis
        self.versionshinweis = versionshinweis
        self.veroeffentlichungsdatum = veroeffentlichungsdatum
        self.letzte_aenderung = letzte_aenderung or utc_now()
        self.relation = relation or []
        self.stichwort = stichwort or []

        self.ist_abtrakt = ist_abstrakt
        self.dokumentart = dokumentart
        self.bezeichnung = parse_string_latin(bezeichnung or faker.name())
        self.hilfetext = parse_optional_string_latin(hilfetext)

    def full(self) -> Xdf3SteckbriefMessageFactory:
        self.definition = self.definition or faker.text()
        self.bezeichnung_eingabe = self.bezeichnung_eingabe or faker.name()
        self.beschreibung = self.beschreibung or faker.text()
        self.status_gesetzt_durch = self.status_gesetzt_durch or faker.name()
        self.status_gesetzt_am = self.status_gesetzt_am or faker.date_object()
        self.letzte_aenderung = self.letzte_aenderung or faker.date_time().astimezone(
            timezone.utc
        )
        self.versionshinweis = self.versionshinweis or faker.text()

        if len(self.bezug) == 0:
            self.bezug.append(
                xdf3.Rechtsbezug(
                    parse_string_latin(faker.text()), link="https://example.com"
                )
            )

        self.gueltig_ab = self.gueltig_ab or faker.date_object()
        self.gueltig_bis = self.gueltig_bis or faker.date_object()
        self.hilfetext = self.hilfetext or faker.text()

        return self

    def build(self) -> xdf3.Steckbrief:
        return xdf3.Steckbrief(
            identifier=xdf3.Identifier(self.id, xdf3.parse_version(self.version)),
            name=parse_string_latin(self.name),
            freigabe_status=self.freigabe_status,
            bezeichnung=parse_string_latin(self.bezeichnung),
            beschreibung=parse_optional_string_latin(self.beschreibung),
            definition=parse_optional_string_latin(self.definition),
            bezug=self.bezug,
            versionshinweis=parse_optional_string_latin(self.versionshinweis),
            gueltig_ab=self.gueltig_ab,
            gueltig_bis=self.gueltig_bis,
            status_gesetzt_am=self.status_gesetzt_am,
            status_gesetzt_durch=parse_optional_string_latin(self.status_gesetzt_durch),
            letzte_aenderung=self.letzte_aenderung,
            relation=self.relation,
            stichwort=self.stichwort,
            ist_abstrakt=self.ist_abtrakt,
            veroeffentlichungsdatum=self.veroeffentlichungsdatum,
            dokumentart=self.dokumentart,
            hilfetext=parse_optional_string_latin(self.hilfetext),
        )

    def build_message(self) -> xdf3.SteckbriefMessage:
        return xdf3.SteckbriefMessage(
            header=xdf3.MessageHeader(
                nachricht_id="some_id",
                erstellungs_zeitpunkt=utc_now(),
            ),
            steckbrief=self.build(),
        )

    def save(self, runner: CommandRunner, content: str = TEST_STECKBRIEF_XML):
        steckbrief = self.build_message()
        return runner.import_xdf3_steckbrief(steckbrief, content)


class Xdf3Factory:
    """
    Factory for creating XDF3 elements (schemas, groups, fields, rules).

    Since schemas contain a full tree of groups, fields and rules, this factory
    makes it easier to create deeply nested structures without the need to explicitly
    add all elements to the schema message before serializing.

    Instead, all created elements are registered in the factory.
    When finally building a message, all references can be resolved automatically.

    Example usage:

    ```python
    factory = Xdf3Factory()

    rule = factory.rule().build()
    field = factory.field().with_rule(rule).build()
    group = factory.group().with_field(field).build()


    # Building the schema message now automatically resolves all references
    # to groups, fields and rules, no need to register them manually anymore.
    schema_message = factory.schema().with_group(group).message()
    ```

    """

    _schemas: dict[xdf3.Identifier, xdf3.Schema]
    _groups: dict[xdf3.Identifier, xdf3.Gruppe]
    _fields: dict[xdf3.Identifier, xdf3.Datenfeld]
    _rules: dict[xdf3.Identifier, xdf3.Regel]

    def __init__(
        self,
    ):
        self._schemas = {}
        self._groups = {}
        self._fields = {}
        self._rules = {}

    def register_schema(self, schema: xdf3.Schema) -> None:
        assert schema.identifier not in self._schemas
        self._schemas[schema.identifier] = schema

    def register_group(self, group: xdf3.Gruppe) -> None:
        assert group.identifier not in self._groups
        self._groups[group.identifier] = group

    def register_field(self, field: xdf3.Datenfeld) -> None:
        assert field.identifier not in self._fields
        self._fields[field.identifier] = field

    def register_rule(self, rule: xdf3.Regel) -> None:
        assert rule.identifier not in self._rules
        self._rules[rule.identifier] = rule

    def resolve_rules(
        self, rule_identifiers: Iterable[xdf3.Identifier]
    ) -> dict[xdf3.Identifier, xdf3.Regel]:
        return {identifier: self._rules[identifier] for identifier in rule_identifiers}

    def resolve_tree(
        self, struktur: list[xdf3.ElementReference], root_rules: list[xdf3.Identifier]
    ) -> tuple[
        dict[xdf3.Identifier, xdf3.Gruppe],
        dict[xdf3.Identifier, xdf3.Datenfeld],
        dict[xdf3.Identifier, xdf3.Regel],
    ]:
        """
        Recursively collect all groups, fields and rules referenced by the given structure.
        """

        groups = {}
        fields = {}
        rule_identifiers = set(root_rules)

        stack = struktur.copy()
        while stack:
            element = stack.pop()

            if element.element_type == xdf3.ElementType.GRUPPE:
                group = self._groups[element.identifier]
                groups[group.identifier] = group
                stack.extend(group.struktur)

                rule_identifiers.update(group.regeln)
            else:
                assert element.element_type == xdf3.ElementType.FELD
                field = self._fields[element.identifier]
                fields[field.identifier] = field

                rule_identifiers.update(field.regeln)

        rules = self.resolve_rules(rule_identifiers)

        return groups, fields, rules

    def schema(
        self,
        id: str | None = None,
        version: str | None = None,
        name: str | None = None,
        stichwort: list[xdf3.Stichwort] | None = None,
        beschreibung: str | None = None,
        definition: str | None = None,
        freigabe_status: xdf3.FreigabeStatus = xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        status_gesetzt_am: date | None = None,
        status_gesetzt_durch: str | None = "Fachlicher Ersteller",
        gueltig_ab: date | None = None,
        gueltig_bis: date | None = None,
        versionshinweis: str | None = None,
        veroeffentlichungsdatum: date | None = None,
        letzte_aenderung: datetime | None = None,
        bezeichnung: str | None = None,
        hilfetext: str | None = None,
        ableitungsmodifikationen_struktur: xdf3.AbleitungsmodifikationenStruktur = xdf3.AbleitungsmodifikationenStruktur.NICHT_MODIFIZIERBAR,
        ableitungsmodifikationen_repraesentation: xdf3.AbleitungsmodifikationenRepraesentation = xdf3.AbleitungsmodifikationenRepraesentation.NICHT_MODIFIZIERBAR,
        dokumentsteckbrief: xdf3.Identifier | None = None,
        relation: list[xdf3.Relation] | None = None,
    ) -> Xdf3SchemaFactory:
        id = id or f"S00000{get_unique_id()}"
        version = xdf3.parse_version(version or "1.0.0")
        assert version is not None

        return Xdf3SchemaFactory(
            factory=self,
            id=id,
            version=version,
            name=parse_string_latin(name or faker.name()),
            beschreibung=parse_optional_string_latin(beschreibung),
            definition=parse_optional_string_latin(definition),
            bezug=[],
            freigabe_status=freigabe_status,
            status_gesetzt_am=status_gesetzt_am,
            status_gesetzt_durch=parse_optional_string_latin(status_gesetzt_durch),
            gueltig_ab=gueltig_ab,
            gueltig_bis=gueltig_bis,
            versionshinweis=parse_optional_string_latin(versionshinweis),
            veroeffentlichungsdatum=veroeffentlichungsdatum,
            letzte_aenderung=letzte_aenderung or utc_now(),
            relation=relation or [],
            stichwort=stichwort or [],
            bezeichnung=parse_string_latin(bezeichnung or faker.name()),
            hilfetext=parse_optional_string_latin(hilfetext),
            ableitungsmodifikationen_struktur=ableitungsmodifikationen_struktur,
            ableitungsmodifikationen_repraesentation=(
                ableitungsmodifikationen_repraesentation
            ),
            dokumentsteckbrief=dokumentsteckbrief
            or xdf3.Identifier("D00000000001", version=None),
            regeln=[],
            struktur=[],
        )

    def group(
        self,
        id: str | None = None,
        version: str | None = None,
        nummernkreis: str | None = None,
        name: str | None = None,
        beschreibung: str | None = None,
        definition: str | None = None,
        freigabe_status: xdf3.FreigabeStatus = xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        status_gesetzt_am: date | None = None,
        status_gesetzt_durch: str | None = None,
        gueltig_ab: date | None = None,
        gueltig_bis: date | None = None,
        versionshinweis: str | None = None,
        veroeffentlichungsdatum: date | None = None,
        letzte_aenderung: datetime | None = None,
        bezeichnung_eingabe: str | None = None,
        bezeichnung_ausgabe: str | None = None,
        schema_element_art: xdf3.SchemaElementArt = xdf3.SchemaElementArt.HARMONISIERT,
        hilfetext_eingabe: str | None = None,
        hilfetext_ausgabe: str | None = None,
        art: xdf3.Gruppenart | None = None,
        relation: list[xdf3.Relation] | None = None,
    ) -> Xdf3GroupFactory:
        if id is None:
            nummernkreis = nummernkreis or "00000"
            id = f"G{nummernkreis}{get_unique_id()}"
        else:
            assert nummernkreis is None
            id = id

        version = xdf3.parse_version(version or "1.0.0")
        assert version is not None
        return Xdf3GroupFactory(
            factory=self,
            id=id,
            version=version,
            name=parse_string_latin(name or faker.name()),
            beschreibung=parse_optional_string_latin(beschreibung),
            definition=parse_optional_string_latin(definition),
            bezug=[],
            freigabe_status=freigabe_status,
            status_gesetzt_am=status_gesetzt_am,
            status_gesetzt_durch=parse_optional_string_latin(status_gesetzt_durch),
            gueltig_ab=gueltig_ab,
            gueltig_bis=gueltig_bis,
            versionshinweis=parse_optional_string_latin(versionshinweis),
            veroeffentlichungsdatum=veroeffentlichungsdatum,
            letzte_aenderung=letzte_aenderung or utc_now(),
            relation=relation or [],
            stichwort=[],
            bezeichnung_eingabe=(
                parse_string_latin(bezeichnung_eingabe or faker.name())
            ),
            bezeichnung_ausgabe=parse_optional_string_latin(bezeichnung_ausgabe),
            schema_element_art=schema_element_art,
            hilfetext_eingabe=parse_optional_string_latin(hilfetext_eingabe),
            hilfetext_ausgabe=parse_optional_string_latin(hilfetext_ausgabe),
            art=art,
            regeln=[],
            struktur=[],
        )

    def field(
        self,
        id: str | None = None,
        version: str | None = None,
        nummernkreis: str | None = None,
        name: str | None = None,
        beschreibung: str | None = None,
        definition: str | None = None,
        freigabe_status: xdf3.FreigabeStatus = xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        status_gesetzt_am: date | None = None,
        status_gesetzt_durch: str | None = None,
        gueltig_ab: date | None = None,
        gueltig_bis: date | None = None,
        versionshinweis: str | None = None,
        veroeffentlichungsdatum: date | None = None,
        letzte_aenderung: datetime | None = None,
        bezeichnung_eingabe: str | None = None,
        bezeichnung_ausgabe: str | None = None,
        schema_element_art: xdf3.SchemaElementArt = xdf3.SchemaElementArt.RECHTSNORMGEBUNDEN,
        hilfetext_eingabe: str | None = None,
        hilfetext_ausgabe: str | None = None,
        codeliste_referenz: genericode.Identifier | None = None,
        bezug: list[xdf3.Rechtsbezug] | None = None,
        feldart: xdf3.Feldart = xdf3.Feldart.EINGABE,
        datentyp: xdf3.Datentyp = xdf3.Datentyp.OBJEKT,
        vorbefuellung: xdf3.Vorbefuellung = xdf3.Vorbefuellung.KEINE,
        praezisierung: xdf3.Praezisierung | None = None,
        werte: list[xdf3.ListenWert] | None = None,
        code_key: str | None = None,
        name_key: str | None = None,
        help_key: str | None = None,
        inhalt: str | None = None,
        relation: list[xdf3.Relation] | None = None,
    ) -> Xdf3FieldFactory:
        if id is None:
            nummernkreis = nummernkreis or "00000"
            id = f"F{nummernkreis}{get_unique_id()}"
        else:
            assert nummernkreis is None
            id = id

        version = xdf3.parse_version(version or "1.0.0")
        assert version is not None

        return Xdf3FieldFactory(
            factory=self,
            id=id,
            version=version,
            name=parse_string_latin(name or faker.name()),
            beschreibung=parse_optional_string_latin(beschreibung),
            definition=parse_optional_string_latin(definition),
            bezug=bezug or [],
            freigabe_status=freigabe_status,
            status_gesetzt_am=status_gesetzt_am,
            status_gesetzt_durch=parse_optional_string_latin(status_gesetzt_durch),
            gueltig_ab=gueltig_ab,
            gueltig_bis=gueltig_bis,
            versionshinweis=parse_optional_string_latin(versionshinweis),
            veroeffentlichungsdatum=veroeffentlichungsdatum,
            letzte_aenderung=letzte_aenderung or utc_now(),
            relation=relation or [],
            stichwort=[],
            bezeichnung_eingabe=(
                parse_string_latin(bezeichnung_eingabe or faker.name())
            ),
            bezeichnung_ausgabe=parse_optional_string_latin(bezeichnung_ausgabe),
            schema_element_art=schema_element_art,
            hilfetext_eingabe=parse_optional_string_latin(hilfetext_eingabe),
            hilfetext_ausgabe=parse_optional_string_latin(hilfetext_ausgabe),
            feldart=feldart,
            datentyp=datentyp,
            praezisierung=praezisierung,
            inhalt=parse_string_latin(inhalt) if inhalt is not None else None,
            vorbefuellung=vorbefuellung,
            werte=werte,
            codeliste_referenz=codeliste_referenz,
            code_key=code_key,
            name_key=name_key,
            help_key=help_key,
            regeln=[],
            max_size=None,
            media_type=[],
        )

    def rule(
        self,
        id: str | None = None,
        version: str | None = None,
        nummernkreis: str | None = None,
        name: str | None = None,
        beschreibung: str | None = None,
        freitext_regel: str | None = None,
        fachlicher_ersteller: str | None = None,
        letzte_aenderung: datetime | None = None,
        typ: xdf3.Regeltyp = xdf3.Regeltyp.KOMPLEX,
        skript: str | None = None,
    ) -> Xdf3RuleFactory:
        if id is None:
            nummernkreis = nummernkreis or "00000"
            id = f"R{nummernkreis}{get_unique_id()}"
        else:
            assert nummernkreis is None
            id = id

        version = xdf3.parse_version(version or "1.0.0")
        assert version is not None

        return Xdf3RuleFactory(
            factory=self,
            id=id,
            version=version,
            name=name or faker.name(),
            beschreibung=beschreibung,
            freitext_regel=freitext_regel,
            bezug=[],
            stichwort=[],
            fachlicher_ersteller=fachlicher_ersteller,
            letzte_aenderung=letzte_aenderung or utc_now(),
            typ=typ,
            param=[],
            ziel=[],
            skript=skript,
            fehler=[],
        )


@dataclass(slots=True)
class Xdf3SchemaFactory:
    factory: Xdf3Factory

    id: str
    version: xdf3.Version
    name: din91379.StringLatin
    beschreibung: din91379.StringLatin | None
    definition: din91379.StringLatin | None
    bezug: list[xdf3.Rechtsbezug]
    freigabe_status: xdf3.FreigabeStatus
    status_gesetzt_am: date | None
    status_gesetzt_durch: din91379.StringLatin | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    versionshinweis: din91379.StringLatin | None
    veroeffentlichungsdatum: date | None
    letzte_aenderung: datetime
    relation: list[xdf3.Relation]
    stichwort: list[xdf3.Stichwort]

    bezeichnung: din91379.StringLatin
    hilfetext: din91379.StringLatin | None
    ableitungsmodifikationen_struktur: xdf3.AbleitungsmodifikationenStruktur
    ableitungsmodifikationen_repraesentation: (
        xdf3.AbleitungsmodifikationenRepraesentation
    )
    dokumentsteckbrief: xdf3.Identifier
    regeln: list[xdf3.Identifier]
    struktur: list[xdf3.ElementReference]

    def get_nummernkreis(self) -> str:
        assert len(self.id) >= 6
        return self.id[1:6]

    def full(self) -> Xdf3SchemaFactory:
        self.beschreibung = self.beschreibung or parse_string_latin(faker.text())
        self.definition = self.definition or parse_string_latin(faker.text())

        if len(self.bezug) == 0:
            self.bezug.append(
                xdf3.Rechtsbezug(parse_string_latin("Rechtsbezug"), link="link")
            )

        self.status_gesetzt_am = self.status_gesetzt_am or utc_now().date()
        self.status_gesetzt_durch = self.status_gesetzt_durch or parse_string_latin(
            faker.name()
        )
        self.gueltig_ab = self.gueltig_ab or utc_now().date()
        self.gueltig_bis = self.gueltig_bis or utc_now().date()
        self.versionshinweis = self.versionshinweis or parse_string_latin(faker.text())
        self.veroeffentlichungsdatum = self.veroeffentlichungsdatum or utc_now().date()

        if len(self.relation) == 0:
            self.relation.append(
                xdf3.Relation(
                    xdf3.RelationTyp.ERSETZT,
                    self.factory.schema().build().identifier,
                )
            )
            self.relation.append(
                xdf3.Relation(
                    xdf3.RelationTyp.IST_ABGELEITET_VON,
                    self.factory.schema().build().identifier,
                )
            )

        if len(self.stichwort) == 0:
            self.stichwort.append(
                xdf3.Stichwort(uri="StichwortUri", value="StichwortValue")
            )

        self.bezeichnung = self.bezeichnung or self.name
        self.hilfetext = self.hilfetext or parse_string_latin(faker.text())

        if len(self.struktur) == 0:
            group = (
                self.factory.group(nummernkreis=self.get_nummernkreis()).full().build()
            )
            self.with_group(group)

        if len(self.regeln) == 0:
            rule = (
                self.factory.rule(nummernkreis=self.get_nummernkreis()).full().build()
            )
            self.with_rule(rule)

        return self

    def with_bezug(
        self, text: str | None = None, link: str | None = None
    ) -> Xdf3SchemaFactory:
        text = parse_string_latin(text or faker.text())

        self.bezug.append(
            xdf3.Rechtsbezug(text=text, link=link),
        )

        return self

    def with_rule(self, rule: xdf3.Regel | None = None) -> Xdf3SchemaFactory:
        if rule is None:
            rule = self.factory.rule(nummernkreis=self.get_nummernkreis()).build()

        self.regeln.append(rule.identifier)

        return self

    def with_group(
        self,
        group: xdf3.Gruppe | None = None,
        anzahl: xdf3.Anzahl | None = None,
        bezug: list[xdf3.Rechtsbezug] = [],
    ) -> Xdf3SchemaFactory:
        if group is None:
            group = self.factory.group(nummernkreis=self.get_nummernkreis()).build()

        if anzahl is None:
            anzahl = xdf3.Anzahl(1, 1)

        self.struktur.append(
            xdf3.ElementReference(
                anzahl=anzahl,
                bezug=bezug,
                element_type=xdf2.ElementType.GRUPPE,
                identifier=group.identifier,
            )
        )

        return self

    def with_field(
        self,
        field: xdf3.Datenfeld | None = None,
        anzahl: xdf3.Anzahl | None = None,
        bezug: list[xdf3.Rechtsbezug] = [],
    ) -> Xdf3SchemaFactory:
        if field is None:
            field = self.factory.field(nummernkreis=self.get_nummernkreis()).build()

        if anzahl is None:
            anzahl = xdf3.Anzahl(1, 1)

        # self.fields[field.identifier] = field
        self.struktur.append(
            xdf3.ElementReference(
                anzahl=anzahl,
                bezug=bezug,
                element_type=xdf3.ElementType.FELD,
                identifier=field.identifier,
            )
        )

        return self

    def build(self) -> xdf3.Schema:
        schema = xdf3.Schema(
            identifier=xdf3.Identifier(self.id, self.version),
            name=self.name,
            beschreibung=self.beschreibung,
            definition=self.definition,
            bezug=self.bezug,
            freigabe_status=self.freigabe_status,
            status_gesetzt_am=self.status_gesetzt_am,
            status_gesetzt_durch=self.status_gesetzt_durch,
            gueltig_ab=self.gueltig_ab,
            gueltig_bis=self.gueltig_bis,
            versionshinweis=self.versionshinweis,
            veroeffentlichungsdatum=self.veroeffentlichungsdatum,
            letzte_aenderung=self.letzte_aenderung,
            relation=self.relation,
            stichwort=self.stichwort,
            bezeichnung=self.bezeichnung,
            hilfetext=self.hilfetext,
            ableitungsmodifikationen_struktur=self.ableitungsmodifikationen_struktur,
            ableitungsmodifikationen_repraesentation=self.ableitungsmodifikationen_repraesentation,
            dokumentsteckbrief=self.dokumentsteckbrief,
            regeln=self.regeln,
            struktur=self.struktur,
        )
        self.factory.register_schema(schema)

        return schema

    def message(
        self, erstellungs_zeitpunkt: datetime | None = None
    ) -> xdf3.SchemaMessage:
        erstellungs_zeitpunkt = erstellungs_zeitpunkt or utc_now()
        schema = self.build()
        groups, fields, rules = self.factory.resolve_tree(
            schema.struktur, schema.regeln
        )

        return xdf3.SchemaMessage(
            header=xdf3.MessageHeader(
                nachricht_id="some_id",
                erstellungs_zeitpunkt=erstellungs_zeitpunkt,
            ),
            schema=schema,
            groups=groups,
            fields=fields,
            rules=rules,
        )

    def build_import(self) -> Xdf3SchemaImport:
        message = self.message()
        return Xdf3SchemaImport.from_message(message)

    def save(self, runner: CommandRunner) -> FullSchemaOut:
        xdf3_import = self.build_import()
        return runner.import_xdf3(xdf3_import)


@dataclass(slots=True)
class Xdf3GroupFactory:
    factory: Xdf3Factory

    id: str
    version: xdf3.Version
    name: din91379.StringLatin
    beschreibung: din91379.StringLatin | None
    definition: din91379.StringLatin | None
    bezug: list[xdf3.Rechtsbezug]
    freigabe_status: xdf3.FreigabeStatus
    status_gesetzt_am: date | None
    status_gesetzt_durch: din91379.StringLatin | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    versionshinweis: din91379.StringLatin | None
    veroeffentlichungsdatum: date | None
    letzte_aenderung: datetime
    relation: list[xdf3.Relation]
    stichwort: list[xdf3.Stichwort]

    bezeichnung_eingabe: din91379.StringLatin
    bezeichnung_ausgabe: din91379.StringLatin | None
    schema_element_art: xdf3.SchemaElementArt
    hilfetext_eingabe: din91379.StringLatin | None
    hilfetext_ausgabe: din91379.StringLatin | None

    art: xdf3.Gruppenart | None
    regeln: list[xdf3.Identifier]
    struktur: list[xdf3.ElementReference]

    def get_nummernkreis(self) -> str:
        assert len(self.id) >= 6
        return self.id[1:6]

    def valid(self) -> Xdf3GroupFactory:
        """
        Set default values so that the group does not trigger any warning.
        """
        if len(self.struktur) == 0:
            self.with_field()

        return self

    def minimal(self) -> Xdf3GroupFactory:
        self.beschreibung = None
        self.definition = None
        self.bezug = []
        self.status_gesetzt_am = None
        self.status_gesetzt_durch = None
        self.gueltig_ab = None
        self.gueltig_bis = None
        self.versionshinweis = None
        self.relation = []
        self.stichwort = []
        self.bezeichnung_ausgabe = None
        self.hilfetext_eingabe = None
        self.hilfetext_ausgabe = None
        self.struktur = []
        self.regeln = []

        if len(self.struktur) == 0:
            field = (
                self.factory.field(nummernkreis=self.get_nummernkreis())
                .minimal()
                .build()
            )
            self.with_field(field)

        return self

    def full(self) -> Xdf3GroupFactory:
        self.beschreibung = self.beschreibung or parse_string_latin(faker.text())
        self.definition = self.definition or parse_string_latin(faker.text())
        self.bezug.append(
            xdf3.Rechtsbezug(parse_string_latin("Rechtsbezug"), link="link")
        )
        self.status_gesetzt_am = self.status_gesetzt_am or utc_now().date()
        self.status_gesetzt_durch = self.status_gesetzt_durch or parse_string_latin(
            faker.name()
        )
        self.gueltig_ab = self.gueltig_ab or utc_now().date()
        self.gueltig_bis = self.gueltig_bis or utc_now().date()
        self.versionshinweis = self.versionshinweis or parse_string_latin(faker.text())
        self.veroeffentlichungsdatum = self.veroeffentlichungsdatum or utc_now().date()
        self.relation.append(
            xdf3.Relation(
                xdf3.RelationTyp.ERSETZT, self.factory.group().build().identifier
            )
        )
        self.stichwort.append(
            xdf3.Stichwort(uri="StichwortUri", value="StichwortValue")
        )

        self.bezeichnung_ausgabe = self.bezeichnung_ausgabe or self.bezeichnung_eingabe
        self.hilfetext_eingabe = self.hilfetext_eingabe or parse_string_latin(
            faker.text()
        )
        self.hilfetext_ausgabe = self.hilfetext_ausgabe or parse_string_latin(
            faker.text()
        )

        self.art = xdf3.Gruppenart.AUSWAHL

        if len(self.struktur) == 0:
            field = (
                self.factory.field(nummernkreis=self.get_nummernkreis()).full().build()
            )
            self.with_field(field)

        if len(self.regeln) == 0:
            rule = (
                self.factory.rule(nummernkreis=self.get_nummernkreis()).full().build()
            )
            self.with_rule(rule)

        return self

    def with_bezug(self, text: str) -> Xdf3GroupFactory:
        self.bezug.append(xdf3.Rechtsbezug(parse_string_latin(text), link=None))

        return self

    def remove_children(self) -> Xdf3GroupFactory:
        self.struktur = []
        return self

    def with_rule(self, rule: xdf3.Regel | None = None) -> Xdf3GroupFactory:
        if rule is None:
            rule = self.factory.rule().build()

        self.regeln.append(rule.identifier)
        return self

    def with_group(
        self,
        group: xdf3.Gruppe | None = None,
        anzahl: xdf3.Anzahl | None = None,
        bezug: list[xdf3.Rechtsbezug] = [],
    ) -> Xdf3GroupFactory:
        if anzahl is None:
            anzahl = xdf3.Anzahl(1, 1)

        if group is None:
            group = self.factory.group(nummernkreis=self.get_nummernkreis()).build()

        self.struktur.append(
            xdf3.ElementReference(
                anzahl=anzahl,
                bezug=bezug,
                element_type=xdf3.ElementType.GRUPPE,
                identifier=group.identifier,
            )
        )

        return self

    def with_field(
        self,
        field: xdf3.Datenfeld | None = None,
        anzahl: xdf3.Anzahl | None = None,
        bezug: list[xdf3.Rechtsbezug] = [],
    ) -> Xdf3GroupFactory:
        if field is None:
            field = self.factory.field(nummernkreis=self.get_nummernkreis()).build()

        if anzahl is None:
            anzahl = xdf3.Anzahl(1, 1)

        self.struktur.append(
            xdf3.ElementReference(
                anzahl=anzahl,
                bezug=bezug,
                element_type=xdf3.ElementType.FELD,
                identifier=field.identifier,
            )
        )

        return self

    def build(self) -> xdf3.Gruppe:
        group = xdf3.Gruppe(
            identifier=xdf3.Identifier(self.id, self.version),
            name=self.name,
            beschreibung=self.beschreibung,
            definition=self.definition,
            bezug=self.bezug,
            freigabe_status=self.freigabe_status,
            status_gesetzt_am=self.status_gesetzt_am,
            status_gesetzt_durch=self.status_gesetzt_durch,
            gueltig_ab=self.gueltig_ab,
            gueltig_bis=self.gueltig_bis,
            versionshinweis=self.versionshinweis,
            veroeffentlichungsdatum=self.veroeffentlichungsdatum,
            letzte_aenderung=self.letzte_aenderung,
            relation=self.relation,
            stichwort=self.stichwort,
            bezeichnung_eingabe=self.bezeichnung_eingabe,
            bezeichnung_ausgabe=self.bezeichnung_ausgabe,
            schema_element_art=self.schema_element_art,
            hilfetext_eingabe=self.hilfetext_eingabe,
            hilfetext_ausgabe=self.hilfetext_ausgabe,
            art=self.art,
            regeln=self.regeln,
            struktur=self.struktur,
        )

        self.factory.register_group(group)

        return group

    def message(
        self, erstellungs_zeitpunkt: datetime | None = None
    ) -> xdf3.DatenfeldgruppeMessage:
        erstellungs_zeitpunkt = erstellungs_zeitpunkt or utc_now()

        group = self.build()
        groups, fields, rules = self.factory.resolve_tree(group.struktur, group.regeln)

        return xdf3.DatenfeldgruppeMessage(
            header=xdf3.MessageHeader(
                nachricht_id="some_id",
                erstellungs_zeitpunkt=erstellungs_zeitpunkt,
            ),
            group=group,
            groups=groups,
            fields=fields,
            rules=rules,
        )


@dataclass(slots=True)
class Xdf3FieldFactory:
    factory: Xdf3Factory

    id: str
    version: xdf3.Version
    name: din91379.StringLatin
    beschreibung: din91379.StringLatin | None
    definition: din91379.StringLatin | None
    bezug: list[xdf3.Rechtsbezug]
    freigabe_status: xdf3.FreigabeStatus
    status_gesetzt_am: date | None
    status_gesetzt_durch: din91379.StringLatin | None
    gueltig_ab: date | None
    gueltig_bis: date | None
    versionshinweis: din91379.StringLatin | None
    veroeffentlichungsdatum: date | None
    letzte_aenderung: datetime
    relation: list[xdf3.Relation]
    stichwort: list[xdf3.Stichwort]

    bezeichnung_eingabe: din91379.StringLatin
    bezeichnung_ausgabe: din91379.StringLatin | None
    schema_element_art: xdf3.SchemaElementArt
    hilfetext_eingabe: din91379.StringLatin | None
    hilfetext_ausgabe: din91379.StringLatin | None

    feldart: xdf3.Feldart
    datentyp: xdf3.Datentyp
    praezisierung: xdf3.Praezisierung | None
    inhalt: din91379.StringLatin | None
    vorbefuellung: xdf3.Vorbefuellung
    werte: list[xdf3.ListenWert] | None
    codeliste_referenz: genericode.Identifier | None
    code_key: str | None
    name_key: str | None
    help_key: str | None
    regeln: list[xdf3.Identifier]
    max_size: int | None
    media_type: list[str]

    def get_nummernkreis(self) -> str:
        assert len(self.id) >= 3
        return self.id[1:6]

    def minimal(self) -> Xdf3FieldFactory:
        self.beschreibung = None
        self.definition = None
        self.bezug = []
        self.status_gesetzt_am = None
        self.status_gesetzt_durch = None
        self.gueltig_ab = None
        self.gueltig_bis = None
        self.versionshinweis = None
        self.veroeffentlichungsdatum = None
        self.relation = []
        self.stichwort = []
        self.bezeichnung_ausgabe = None
        self.hilfetext_eingabe = None
        self.hilfetext_ausgabe = None
        self.regeln = []

        self.feldart = xdf3.Feldart.EINGABE
        self.datentyp = xdf3.Datentyp.TEXT
        self.praezisierung = None
        self.inhalt = None
        self.vorbefuellung = xdf3.Vorbefuellung.KEINE
        self.werte = None
        self.codeliste_referenz = None
        self.code_key = None
        self.name_key = None
        self.help_key = None
        self.regeln = []
        self.max_size = None
        self.media_type = []

        return self

    def full(self) -> Xdf3FieldFactory:
        self.beschreibung = self.beschreibung or parse_string_latin(faker.text())
        self.definition = self.definition or parse_string_latin(faker.text())
        self.bezug.append(
            xdf3.Rechtsbezug(parse_string_latin("Rechtsbezug"), link="link")
        )
        self.status_gesetzt_am = self.status_gesetzt_am or utc_now().date()
        self.status_gesetzt_durch = self.status_gesetzt_durch or parse_string_latin(
            faker.name()
        )
        self.gueltig_ab = self.gueltig_ab or utc_now().date()
        self.gueltig_bis = self.gueltig_bis or utc_now().date()
        self.versionshinweis = self.versionshinweis or parse_string_latin(faker.text())
        self.veroeffentlichungsdatum = self.veroeffentlichungsdatum or utc_now().date()
        self.relation.append(
            xdf3.Relation(
                xdf3.RelationTyp.ERSETZT, self.factory.field().build().identifier
            )
        )
        self.stichwort.append(
            xdf3.Stichwort(uri="StichwortUri", value="StichwortValue")
        )

        self.bezeichnung_ausgabe = self.bezeichnung_ausgabe or self.bezeichnung_eingabe
        self.hilfetext_eingabe = self.hilfetext_eingabe or parse_string_latin(
            faker.text()
        )
        self.hilfetext_ausgabe = self.hilfetext_ausgabe or parse_string_latin(
            faker.text()
        )

        if len(self.regeln) == 0:
            self.with_rule()

        return self

    def with_rule(self, rule: xdf3.Regel | None = None) -> Xdf3FieldFactory:
        if rule is None:
            rule = self.factory.rule().build()

        self.regeln.append(rule.identifier)
        return self

    def with_code_list(self, code_list: genericode.CodeList | None) -> Xdf3FieldFactory:
        if code_list is None:
            self.codeliste_referenz = None
        else:
            self.codeliste_referenz = code_list.identifier

        return self

    def with_bezug(self, text: str) -> Xdf3FieldFactory:
        self.bezug.append(xdf3.Rechtsbezug(parse_string_latin(text), link=None))

        return self

    def input(
        self,
        datentyp: xdf3.Datentyp = xdf3.Datentyp.DATUM,
        praezisierung: xdf3.Praezisierung | None = None,
        max_size: int | None = None,
        media_type: list[str] | None = None,
    ) -> Xdf3FieldFactory:
        self.feldart = xdf3.Feldart.EINGABE
        self.datentyp = datentyp
        self.praezisierung = praezisierung
        self.max_size = max_size
        self.media_type = media_type or []

        return self

    def select(
        self,
        datentyp: xdf3.Datentyp = xdf3.Datentyp.TEXT,
        code_key: str | None = None,
        name_key: str | None = None,
        help_key: str | None = None,
        code_list: genericode.CodeList | None = None,
        werte: list[xdf3.ListenWert] | None = None,
    ) -> Xdf3FieldFactory:
        self.feldart = xdf3.Feldart.AUSWAHL
        self.datentyp = datentyp
        self.code_key = code_key
        self.name_key = name_key
        self.help_key = help_key

        self.with_code_list(code_list)
        self.werte = werte

        return self

    def label(self) -> Xdf3FieldFactory:
        self.feldart = xdf3.Feldart.STATISCH
        self.datentyp = xdf3.Datentyp.TEXT

        return self

    def blocked(self) -> Xdf3FieldFactory:
        self.feldart = xdf3.Feldart.GESPERRT
        self.datentyp = xdf3.Datentyp.TEXT

        return self

    def hidden(self) -> Xdf3FieldFactory:
        self.feldart = xdf3.Feldart.VERSTECKT
        self.datentyp = xdf3.Datentyp.TEXT

        return self

    def build(self) -> xdf3.Datenfeld:
        field = xdf3.Datenfeld(
            identifier=xdf3.Identifier(self.id, self.version),
            name=self.name,
            beschreibung=self.beschreibung,
            definition=self.definition,
            bezug=self.bezug,
            freigabe_status=self.freigabe_status,
            status_gesetzt_am=self.status_gesetzt_am,
            status_gesetzt_durch=self.status_gesetzt_durch,
            gueltig_ab=self.gueltig_ab,
            gueltig_bis=self.gueltig_bis,
            versionshinweis=self.versionshinweis,
            veroeffentlichungsdatum=self.veroeffentlichungsdatum,
            letzte_aenderung=self.letzte_aenderung,
            relation=self.relation,
            stichwort=self.stichwort,
            bezeichnung_eingabe=self.bezeichnung_eingabe,
            bezeichnung_ausgabe=self.bezeichnung_ausgabe,
            schema_element_art=self.schema_element_art,
            hilfetext_eingabe=self.hilfetext_eingabe,
            hilfetext_ausgabe=self.hilfetext_ausgabe,
            feldart=self.feldart,
            datentyp=self.datentyp,
            praezisierung=self.praezisierung,
            inhalt=self.inhalt,
            vorbefuellung=self.vorbefuellung,
            werte=self.werte,
            codeliste_referenz=self.codeliste_referenz,
            code_key=self.code_key,
            name_key=self.name_key,
            help_key=self.help_key,
            regeln=self.regeln,
            max_size=self.max_size,
            media_type=self.media_type,
        )

        self.factory.register_field(field)
        return field

    def message(
        self,
        erstellungs_zeitpunkt: datetime | None = None,
    ) -> xdf3.DatenfeldMessage:
        erstellungs_zeitpunkt = erstellungs_zeitpunkt or utc_now()

        field = self.build()
        rules = self.factory.resolve_rules(field.regeln)

        return xdf3.DatenfeldMessage(
            header=xdf3.MessageHeader(
                nachricht_id="some_id",
                erstellungs_zeitpunkt=erstellungs_zeitpunkt,
            ),
            field=field,
            rules=rules,
        )


@dataclass(slots=True)
class Xdf3RuleFactory:
    factory: Xdf3Factory

    id: str
    version: xdf3.Version
    name: str
    beschreibung: str | None
    freitext_regel: str | None
    bezug: list[xdf3.Rechtsbezug]
    stichwort: list[xdf3.Stichwort]
    fachlicher_ersteller: str | None
    letzte_aenderung: datetime
    typ: xdf3.Regeltyp
    param: list[xdf3.RegelParameter]
    ziel: list[xdf3.RegelZiel]
    skript: str | None
    fehler: list[xdf3.Fehlermeldung]

    def full(self) -> Xdf3RuleFactory:
        """
        Fill all optional fields with values.
        """
        self.beschreibung = self.beschreibung or faker.text()
        self.freitext_regel = self.freitext_regel or faker.text()
        self.bezug.append(
            xdf3.Rechtsbezug(parse_string_latin("Rechtsbezug"), link="link")
        )
        self.stichwort.append(
            xdf3.Stichwort(uri="StichwortUri", value="StichwortValue")
        )
        self.fachlicher_ersteller = self.fachlicher_ersteller or faker.name()
        self.param.append(
            xdf3.RegelParameter("element", "name", xdf3.RegelParamTyp.WERT)
        )
        self.ziel.append(xdf3.RegelZiel("element", "name"))
        self.skript = self.skript or faker.text()
        self.fehler.append(xdf3.Fehlermeldung(code=1, message="Fehlernachricht"))

        return self

    def with_bezug(self, text: str) -> Xdf3RuleFactory:
        self.bezug.append(xdf3.Rechtsbezug(parse_string_latin(text), link=None))

        return self

    def build(self) -> xdf3.Regel:
        rule = xdf3.Regel(
            identifier=xdf3.Identifier(self.id, self.version),
            name=self.name,
            beschreibung=self.beschreibung,
            freitext_regel=self.freitext_regel,
            bezug=self.bezug,
            stichwort=self.stichwort,
            fachlicher_ersteller=self.fachlicher_ersteller,
            letzte_aenderung=self.letzte_aenderung,
            typ=self.typ,
            param=self.param,
            ziel=self.ziel,
            skript=self.skript,
            fehler=self.fehler,
        )

        self.factory.register_rule(rule)
        return rule
