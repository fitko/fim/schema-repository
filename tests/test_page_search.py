from fastapi.testclient import TestClient
import pytest


def test_page_load(client: TestClient) -> None:
    response = client.get("/search")

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


@pytest.mark.parametrize(
    "query",
    [
        "/search",
        "/search?resource=invalid",
        "/search?resource=schema",
        "/search?resource=document-profile",
        "/search?resource=group",
        "/search?resource=field",
        "/search?resource=process",
        "/search?resource=service-description",
        "/search?resource=service",
        "/search?resource=service&freigabe_status_katalog=4",
        "/search?resource=service&freigabe_status_katalog=invalid",
        "/search?resource=service&freigabe_status_bibliothek=4",
        "/search?resource=service&freigabe_status_bibliothek=invalid",
        "/search?resource=service&invalid_params=value",
        "/search?resource=service&leistung_suche_in=leistungsbezeichnung&term=Test&order_by=relevance",
    ],
)
def test_should_successfully_load_search_results(
    client: TestClient, query: str
) -> None:
    response = client.get(query)

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"
