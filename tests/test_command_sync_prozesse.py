from typing import AsyncContextManager, Callable
from asyncpg import Pool
import pytest

from fimportal.cli.commands.import_prozesse import sync_prozesse
from fimportal.service import Service
from fimportal import xprozesse
from fimportal.xprozesse.client import TEST_PROZESS_XML, TEST_PROZESSKLASSE_XML
from tests.factories.prozess import ProzessFactory, ProzessklasseFactory


@pytest.mark.asyncio(loop_scope="session")
async def test_should_add_new_resources(
    get_service: Callable[[], AsyncContextManager[Service]],
    async_db_pool: Pool,
):
    prozessklasse = ProzessklasseFactory().build()
    prozess = ProzessFactory(prozessklasse_id=prozessklasse.id).build()
    xprozesse_client = xprozesse.TestClient()
    xprozesse_client.add_prozessklasse(prozessklasse)
    xprozesse_client.add_prozess(prozess)

    await sync_prozesse(
        client=xprozesse_client,
        pool=async_db_pool,
        immutable_base_url="localhost",
    )

    async with get_service() as service:
        prozess_out = await service.get_prozess(prozess.id)
        assert prozess_out is not None

        prozessklasse_out = await service.get_prozessklasse(prozessklasse.id)
        assert prozessklasse_out is not None


@pytest.mark.asyncio(loop_scope="session")
async def test_should_update_existing_resources(
    get_service: Callable[[], AsyncContextManager[Service]],
    async_db_pool: Pool,
):
    prozessklasse = ProzessklasseFactory(name="pre").build()
    async with get_service() as service:
        await service.import_prozessklasse_batch(
            [(prozessklasse, TEST_PROZESSKLASSE_XML)],
        )

    prozess = ProzessFactory(name="pre", prozessklasse_id=prozessklasse.id).build()
    async with get_service() as service:
        await service.import_prozess_batch(
            [(prozess, TEST_PROZESS_XML)],
        )

    prozessklasse = ProzessklasseFactory(
        id=prozess.id,
        name="post",
    ).build()
    prozess = ProzessFactory(
        id=prozess.id,
        name="post",
    ).build()
    xprozesse_client = xprozesse.TestClient()
    xprozesse_client.add_prozessklasse(prozessklasse)
    xprozesse_client.add_prozess(prozess)

    await sync_prozesse(
        client=xprozesse_client,
        immutable_base_url="localhost",
        pool=async_db_pool,
    )

    async with get_service() as service:
        prozessklasse_out = await service.get_prozessklasse(prozessklasse.id)
        assert prozessklasse_out is not None
        assert prozessklasse_out.name == "post"

        prozess_out = await service.get_prozess(prozess.id)
        assert prozess_out is not None
        assert prozess_out.name == "post"


@pytest.mark.asyncio(loop_scope="session")
async def test_should_delete_removed_prozesse(
    get_service: Callable[[], AsyncContextManager[Service]],
    async_db_pool: Pool,
):
    prozessklasse = ProzessklasseFactory().build()
    async with get_service() as service:
        await service.import_prozessklasse_batch(
            [(prozessklasse, TEST_PROZESSKLASSE_XML)],
        )

    prozess = ProzessFactory().build()
    async with get_service() as service:
        await service.import_prozess_batch(
            [(prozess, TEST_PROZESS_XML)],
        )

    await sync_prozesse(
        client=xprozesse.TestClient(),
        immutable_base_url="localhost",
        pool=async_db_pool,
    )

    async with get_service() as service:
        prozessklasse_out = await service.get_prozessklasse(prozessklasse.id)
        assert prozessklasse_out is None

        prozess_out = await service.get_prozess(prozess.id)
        assert prozess_out is None
