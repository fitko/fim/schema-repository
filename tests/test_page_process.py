from fastapi.testclient import TestClient

from fimportal.xprozesse.prozess import parse_prozess_message
from tests.conftest import CommandRunner
from tests.data import XPROZESS2_DATA


def test_load_schema_page(client: TestClient, runner: CommandRunner) -> None:
    with open(XPROZESS2_DATA / "prozess_with_files.xml", "rb") as file:
        data = file.read()

    prozess_message = parse_prozess_message(data)
    runner.import_prozess(prozess_message.prozess[0])

    response = client.get(f"/processes/{prozess_message.prozess[0].id}")

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_load_process_page(client: TestClient) -> None:
    response = client.get("/processes/test")

    assert response.status_code == 404
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert "Prozess test konnte nicht gefunden werden." in response.text
