from fastapi.testclient import TestClient

from fimportal.models.imports import Xdf2SteckbriefImport
from fimportal.xdatenfelder import xdf3
from tests.conftest import CommandRunner
from tests.data import XDF2_DATA


def test_should_return_the_steckbrief_file(runner: CommandRunner, client: TestClient):
    with open(XDF2_DATA / "steckbrief.xdf2.xml", "rb") as file:
        originalFile = file.read()
        steckbrief_import = Xdf2SteckbriefImport.from_bytes(
            originalFile, xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
        )

    steckbrief = runner.import_xdf2_steckbrief(steckbrief_import)

    response = client.get(
        f"/api/v1/document-profiles/{steckbrief.fim_id}/{steckbrief.fim_version}/xdf"
    )

    assert response.status_code == 200
    assert response.headers["content-type"] == "application/xml"
    assert (
        response.headers["content-disposition"]
        == f'attachment; filename="{steckbrief.fim_id}V{steckbrief.fim_version}.xdf2.xml"'
    )

    assert response.read() == originalFile


def test_should_work_with_latest(runner: CommandRunner, client: TestClient):
    with open(XDF2_DATA / "steckbrief.xdf2.xml", "rb") as file:
        originalFile = file.read()
        steckbrief_import = Xdf2SteckbriefImport.from_bytes(
            originalFile, xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
        )

    steckbrief = runner.import_xdf2_steckbrief(steckbrief_import)

    response = client.get(f"/api/v1/document-profiles/{steckbrief.fim_id}/latest/xdf")

    assert response.status_code == 200
    assert response.headers["content-type"] == "application/xml"
    assert (
        response.headers["content-disposition"]
        == f'attachment; filename="{steckbrief.fim_id}V{steckbrief.fim_version}.xdf2.xml"'
    )

    assert response.read() == originalFile


def test_should_fail_for_unknown_steckbrief(client: TestClient):
    response = client.get("/api/v1/document-profiles/D123456/1.0/xdf")

    assert response.status_code == 404
    assert response.json() == {"detail": "Could not find document profile D123456V1.0."}
