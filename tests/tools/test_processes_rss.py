from datetime import datetime, timezone
from fastapi.testclient import TestClient
from freezegun import freeze_time

from fimportal import xml
from tests.conftest import CommandRunner
from tests.factories.prozess import ProzessFactory


def test_should_return_latest_updates_for_processes(
    baseurl: str, runner: CommandRunner, client: TestClient
):
    ProzessFactory(
        letzter_aenderungszeitpunkt=datetime(2023, 5, 17, tzinfo=timezone.utc)
    ).save(runner)

    prozess = ProzessFactory(
        letzter_aenderungszeitpunkt=datetime(2023, 10, 18, tzinfo=timezone.utc)
    ).save(runner)

    query_time = "2023-10-21T00:00:00"
    with freeze_time(query_time):
        response = client.get("/tools/rss/processes")

    assert response.status_code == 200
    expected_result = f"""<?xml version="1.0" encoding="UTF-8" ?>
        <rss version="2.0">
            <channel>
              <title>Prozesse Updates</title>
              <description>Aktuelle Updates von Prozessen</description>
              <item>
                <title>Prozess {prozess.name} wurde aktualisiert ({prozess.id})</title>
                <link>{baseurl}/processes/{prozess.id}</link>
                <pubDate>Wed, 18 Oct 2023 00:00:00 +0000</pubDate>
              </item>
            </channel>
        </rss> 
    """

    assert xml.prettify(response.text) == xml.prettify(expected_result)
