from fastapi.testclient import TestClient

from fimportal.xzufi.link_check import FakeLinkChecker
from tests.conftest import CommandRunner
from tests.data import XZUFI_2_2_0_DATA
from tests.factories.xzufi import LinkFactory


ENDPOINT = "/tools/check-leistung"


def test_get_quality_report_json(client: TestClient):
    with open(XZUFI_2_2_0_DATA / "leistung_minimal.xml", "rb") as file:
        response = client.post(ENDPOINT, files=[("leistung", file)])

    assert response.status_code == 200
    assert response.json() == {
        "redaktion_id": "B100019",
        "leistung_id": "leistung_minimal",
        "link_statuses": [],
        "failed_checks": [
            {
                "code": 0,
                "message": "Missing TextModul of type ModulTextTyp.LEISTUNGSBEZEICHNUNG",
                "params": ["LEISTUNGSBEZEICHNUNG"],
            },
            {
                "code": 0,
                "message": "Missing TextModul of type ModulTextTyp.LEISTUNGSBEZEICHNUNG_II",
                "params": ["LEISTUNGSBEZEICHNUNG_II"],
            },
            {
                "code": 0,
                "message": "Missing TextModul of type ModulTextTyp.VOLLTEXT",
                "params": ["VOLLTEXT"],
            },
            {
                "code": 0,
                "message": "Missing TextModul of type ModulTextTyp.ERFORDERLICHE_UNTERLAGEN",
                "params": ["ERFORDERLICHE_UNTERLAGEN"],
            },
            {
                "code": 0,
                "message": "Missing TextModul of type ModulTextTyp.VORAUSSETZUNGEN",
                "params": ["VORAUSSETZUNGEN"],
            },
            {
                "code": 0,
                "message": "Missing TextModul of type ModulTextTyp.VERFAHRENSABLAUF",
                "params": ["VERFAHRENSABLAUF"],
            },
            {
                "code": 0,
                "message": "Missing TextModul of type ModulTextTyp.FORMUALRE",
                "params": ["FORMUALRE"],
            },
            {
                "code": 0,
                "message": "Missing TextModul of type ModulTextTyp.HINWEISE",
                "params": ["HINWEISE"],
            },
            {
                "code": 0,
                "message": "Missing TextModul of type ModulTextTyp.URHEBER",
                "params": ["URHEBER"],
            },
            {
                "code": 0,
                "message": "Missing TextModul of type ModulTextTyp.ZUSTAENDIGE_STELLE",
                "params": ["ZUSTAENDIGE_STELLE"],
            },
            {
                "code": 0,
                "message": "Missing TextModul of type ModulTextTyp.ANSPRECHPUNKT",
                "params": ["ANSPRECHPUNKT"],
            },
            {
                "code": 0,
                "message": "Missing TextModul of type ModulTextTyp.RECHTSBEHELF",
                "params": ["RECHTSBEHELF"],
            },
            {
                "code": 0,
                "message": "Missing TextModul of type ModulTextTyp.TEASER",
                "params": ["TEASER"],
            },
            {
                "code": 4,
                "message": "Keine LeiKa Referenze",
                "params": [],
            },
            {
                "code": 7,
                "message": "Kein SDG Informationsbereich angegeben",
                "params": [],
            },
        ],
    }


def test_get_quality_report_with_unpersisted_link(
    client: TestClient, link_checker: FakeLinkChecker
):
    uri = "example.com"
    link_checker.register_link(uri)

    with open(XZUFI_2_2_0_DATA / "leistung_with_link.xml", "rb") as file:
        response = client.post(ENDPOINT, files=[("leistung", file)])

    assert response.status_code == 200
    assert response.json()["link_statuses"] == [
        {
            "uri": "example.com",
            "is_online": True,
        }
    ]


def test_get_quality_report_with_persisted_link(
    client: TestClient,
    runner: CommandRunner,
):
    uri = "example.com"
    runner.import_link(LinkFactory(uri=uri).build())

    with open(XZUFI_2_2_0_DATA / "leistung_with_link.xml", "rb") as file:
        response = client.post(ENDPOINT, files=[("leistung", file)])

    assert response.status_code == 200
    assert response.json()["link_statuses"] == [
        {
            "uri": "example.com",
            "is_online": False,
        }
    ]
