import zipfile
from io import BytesIO

from fastapi.testclient import TestClient

from fimportal.xdatenfelder import xdf3
from tests.data import XDF3_DATA
from tests.factories.xdf3 import Xdf3Factory

API_ENDPOINT = "tools/xdf3-to-xdf2-converter"


def test_should_return_the_converted_schema_with_all_code_lists(client: TestClient):
    factory = Xdf3Factory()
    field = (
        factory.field()
        .select(werte=[xdf3.ListenWert(code="A", name="Label", hilfe=None)])
        .build()
    )
    message = factory.schema(id="S1200034", version="1.0.0").with_field(field).message()

    response = client.post(
        API_ENDPOINT,
        files=[
            ("schema", xdf3.serialize_schema_message(message)),
        ],
    )

    assert response.status_code == 200
    assert response.headers["content-type"] == "application/zip"
    assert (
        response.headers["content-disposition"]
        == 'attachment; filename="S1234V1.0_converted.zip"'
    )

    with zipfile.ZipFile(BytesIO(response.read())) as archive:
        filenames = archive.namelist()

    assert len(filenames) == 2
    assert "S1234V1.0_converted.xdf2.xml" in filenames
    assert "C99000001.xml" in filenames


def test_should_handle_not_convertible_schema(client: TestClient):
    message = Xdf3Factory().schema(id="S12345", version="1.0.0").full().message()

    response = client.post(
        API_ENDPOINT,
        files=[
            ("schema", xdf3.serialize_schema_message(message)),
        ],
    )

    assert response.status_code == 400
    assert response.json() == {
        "detail": "Cannot convert fim-id S12345: The Unternummernkreis must be 000, but got 345"
    }


def test_should_fail_for_invalid_schema(client: TestClient):
    response = client.post(
        API_ENDPOINT,
        files=[
            ("schema", b"not_xml"),
        ],
    )

    assert response.status_code == 400
    assert response.json() == {
        "detail": "Cannot parse schema: Start tag expected, '<' not found, line 1, column 1 (<string>, line 1)"
    }


def test_should_return_the_converter_html_page(client: TestClient):
    response = client.get(API_ENDPOINT)

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_should_work_for_schema_without_version(client: TestClient):
    with open(XDF3_DATA / "schema_without_versions.xml", "rb") as schema:
        response = client.post(
            API_ENDPOINT,
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 200
