from fastapi.testclient import TestClient

from tests.data import XDF2_DATA


def test_should_return_the_generated_json_schema(client: TestClient):
    with (
        open(XDF2_DATA / "schema.xml", "rb") as schema,
        open(XDF2_DATA / "code_list_a.xml", "rb") as code_list_a,
        open(XDF2_DATA / "code_list_b.xml", "rb") as code_list_b,
    ):
        response = client.post(
            "/tools/xdf2-json-schema-converter",
            files=[
                ("schema", schema),
                ("code_lists", code_list_a),
                ("code_lists", code_list_b),
            ],
        )

    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    assert (
        response.headers["content-disposition"]
        == 'attachment; filename="S1234V1.0_schema.json"'
    )

    assert response.json()["$schema"] == "https://json-schema.org/draft/2020-12/schema"


def test_should_fail_for_missing_code_lists(client: TestClient):
    with open(XDF2_DATA / "schema.xml", "rb") as schema:
        response = client.post(
            "/tools/xdf2-json-schema-converter",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 400
    error_message: str = response.json()["detail"]

    # Do not check the full error message, as the list of code lists is not stable
    assert error_message.startswith("Missing code lists")
    assert "urn:de:code_list_a_1" in error_message
    assert "urn:de:code_list_b_1" in error_message


def test_should_ignore_unused_code_lists(client: TestClient):
    with (
        open(XDF2_DATA / "schema_without_code_lists.xml", "rb") as schema,
        open(XDF2_DATA / "code_list_a.xml", "rb") as code_list,
    ):
        response = client.post(
            "/tools/xdf2-json-schema-converter",
            files=[("schema", schema), ("code_lists", code_list)],
        )

    assert response.status_code == 200


def test_should_fail_for_invalid_schema(client: TestClient):
    with open(XDF2_DATA / "code_list_a.xml", "rb") as schema:
        response = client.post(
            "/tools/xdf2-json-schema-converter",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 400
    assert response.json() == {
        "detail": "Could not parse schema: Unexpected child node [tag={http://docs.oasis-open.org/codelist/ns/genericode/1.0/}CodeList], line 2"
    }


def test_should_fail_for_invalid_code_list(client: TestClient):
    with (
        open(XDF2_DATA / "schema.xml", "rb") as schema,
        open(XDF2_DATA / "schema.xml", "rb") as invalid_code_list,
    ):
        response = client.post(
            "/tools/xdf2-json-schema-converter",
            files=[
                ("schema", schema),
                ("code_lists", invalid_code_list),
            ],
        )

    assert response.status_code == 400
    assert response.json() == {
        "detail": "Could not parse code list: Unexpected child node [tag={urn:xoev-de:fim:standard:xdatenfelder_2}xdatenfelder.stammdatenschema.0102], line 2"
    }


def test_should_ignore_empty_code_list_entry(client: TestClient):
    """
    If no code list is selected in an HTML form, Firefox sends one item
    with the empty byte slice (`b''`) instead of nothing for the code list array.
    """
    with open(XDF2_DATA / "schema_without_code_lists.xml", "rb") as schema:
        response = client.post(
            "/tools/xdf2-json-schema-converter",
            files=[
                ("schema", schema),
                ("code_lists", b""),
            ],
        )

    assert response.status_code == 200


def test_should_return_json_converter_html_page(client: TestClient):
    response = client.get("/tools/xdf2-json-schema-converter")

    assert response.status_code == 200


def test_should_work_for_schema_without_version(client: TestClient):
    with open(XDF2_DATA / "schema_without_versions.xml", "rb") as schema:
        response = client.post(
            "/tools/xdf2-json-schema-converter",
            files=[
                ("schema", schema),
                ("code_lists", b""),
            ],
        )

    assert response.status_code == 200
