from datetime import datetime, timezone
from fastapi.testclient import TestClient
from freezegun import freeze_time

from fimportal import xml
from fimportal.common import FreigabeStatus
from fimportal.xzufi.common import Versionsinformation
from tests.conftest import CommandRunner
from tests.factories.xzufi import LeistungFactory


def test_should_return_latest_updates_for_services(
    baseurl: str, runner: CommandRunner, client: TestClient
):
    factory_1 = LeistungFactory.steckbrief()
    factory_1.versionsinformation = Versionsinformation(
        erstellt_datum_zeit=None,
        erstellt_durch=None,
        geaendert_datum_zeit=datetime(2023, 5, 17, tzinfo=timezone.utc),
        geaendert_durch=None,
        version=None,
    )
    runner.import_leika(factory_1.build())

    factory_2 = LeistungFactory.steckbrief(bezeichnung="Leistung 2")
    factory_2.versionsinformation = Versionsinformation(
        erstellt_datum_zeit=None,
        erstellt_durch=None,
        geaendert_datum_zeit=datetime(2023, 10, 18, tzinfo=timezone.utc),
        geaendert_durch=None,
        version=None,
    )
    leistung_2 = factory_2.build()
    runner.import_leika(leistung_2)

    factory_3 = LeistungFactory.steckbrief(
        bezeichnung="Leistung 3"
    ).with_freigabestatus_katalog(FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD)
    factory_3.versionsinformation = Versionsinformation(
        erstellt_datum_zeit=None,
        erstellt_durch=None,
        geaendert_datum_zeit=datetime(2023, 10, 19, tzinfo=timezone.utc),
        geaendert_durch=None,
        version=None,
    )
    leistung_3 = factory_3.build()
    runner.import_leika(leistung_3)

    query_time = "2023-10-21T00:00:00"
    with freeze_time(query_time):
        response = client.get("/tools/rss/services")

    assert response.status_code == 200
    expected_result = f"""<?xml version="1.0" encoding="UTF-8" ?>
        <rss version="2.0">
            <channel>
              <title>Leistungssteckbrief Updates</title>
              <description>Aktuelle Updates von Leistungssteckbriefen</description>
              <item>
                <title>Leistungssteckbrief Leistung 3 wurde aktualisiert ({leistung_3.get_leistungsschluessel()[0]})</title>
                <link>{baseurl}/services/{leistung_3.get_leistungsschluessel()[0]}</link>
                <description>Handlungsgrundlage: - | Status: {FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.to_label()}</description>
                <pubDate>Thu, 19 Oct 2023 00:00:00 +0000</pubDate>
              </item>
              <item>
                <title>Leistungssteckbrief Leistung 2 wurde aktualisiert ({leistung_2.get_leistungsschluessel()[0]})</title>
                <link>{baseurl}/services/{leistung_2.get_leistungsschluessel()[0]}</link>
                <description>Handlungsgrundlage: - | Status: -</description>
                <pubDate>Wed, 18 Oct 2023 00:00:00 +0000</pubDate>
              </item>
            </channel>
        </rss> 
    """

    assert xml.prettify(response.text) == xml.prettify(expected_result)
