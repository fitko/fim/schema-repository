from fastapi.testclient import TestClient

from tests.data import XDF3_DATA


def test_should_return_the_quality_report(client: TestClient):
    with open(XDF3_DATA / "schema_without_code_lists.xdf3.xml", "rb") as schema:
        response = client.post(
            "/tools/check-xdf3",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 200
    assert response.json() == {
        "group_reports": [],
        "total_group_checks": 0,
        "field_reports": [
            {
                "identifier": {
                    "id": "F12345",
                    "version": "1.1.0",
                    "nummernkreis": "12345",
                },
                "failing_checks": [],
            }
        ],
        "total_field_checks": 0,
        "rule_reports": [],
        "total_rule_checks": 0,
        "schema_checks": [],
        "total_checks": 0,
    }


def test_should_fail_for_invalid_xdf3_file(client: TestClient):
    with open(XDF3_DATA / "code_list_a.xml", "rb") as schema:
        response = client.post(
            "/tools/check-xdf3",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 400
    assert (
        response.json()["detail"]
        == "Cannot parse schema: Element '{http://docs.oasis-open.org/codelist/ns/genericode/1.0/}CodeList': No matching global declaration available for the validation root., line 2"
    )


def test_should_work_for_schema_without_version(client: TestClient):
    with open(XDF3_DATA / "schema_without_versions.xml", "rb") as schema:
        response = client.post(
            "/tools/check-xdf3",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 200
