from fastapi.testclient import TestClient

from tests.data import XDF2_DATA


def test_should_return_the_check_page(client: TestClient):
    response = client.get("/tools/check-xdf2")

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_should_return_the_html_page_for_no_failing_checks(client: TestClient):
    with open(XDF2_DATA / "schema.xml", "rb") as schema:
        response = client.post(
            "/tools/check-xdf2/report",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_should_return_the_html_page_for_failing_checks(client: TestClient):
    with open(XDF2_DATA / "schema_without_bezeichnung_eingabe.xml", "rb") as schema:
        response = client.post(
            "/tools/check-xdf2/report",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_should_print_the_error_for_invalid_schema(client: TestClient):
    with open(XDF2_DATA / "code_list_a.xml", "rb") as schema:
        response = client.post(
            "/tools/check-xdf2/report",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert "Could not parse schema" in response.text


def test_should_work_for_schema_without_version(client: TestClient):
    with open(XDF2_DATA / "schema_without_versions.xml", "rb") as schema:
        response = client.post(
            "/tools/check-xdf2/report",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 200
