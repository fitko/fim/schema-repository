import csv

from fimportal.common import FreigabeStatus
from fimportal.helpers import utc_now
from fimportal.xdatenfelder.xdf2.common import Status
from fimportal.xprozesse.prozess import Detaillierungsstufe
from fimportal.xzufi.common import LeistungsTypisierung
from tests.conftest import CommandRunner, TestClient
from tests.factories import Xdf2Factory
from tests.factories.prozess import ProzessFactory, ProzessklasseFactory
from tests.factories.xdf2 import Xdf2SteckbriefImportFactory
from tests.factories.xzufi import LeistungFactory

ENDPOINT = "/tools/search-csv-download"


class TestDokumentsteckbriefDownload:
    def test_should_download_a_dokumentsteckbrief_csv_file(
        self, baseurl: str, runner: CommandRunner, client: TestClient
    ):
        steckbrief_1 = Xdf2SteckbriefImportFactory(id="D0000001", version="1.0").save(
            runner
        )
        steckbrief_2 = Xdf2SteckbriefImportFactory(id="D0000002", version="1.0").save(
            runner
        )

        response = client.get(f"{ENDPOINT}?resource=document-profile&order_by=id_asc")
        assert response.status_code == 200

        assert response.headers["content-type"] == "text/csv; charset=utf-8"
        assert (
            response.headers["content-disposition"]
            == 'attachment; filename="dokumentsteckbrief_search_results.csv"'
        )

        reader = csv.reader(response.iter_lines())
        rows = list(reader)[1:]

        assert rows == [
            [
                "D0000001V1.0",
                "D",
                steckbrief_1.name,
                "-",
                "-",
                steckbrief_1.freigabe_status_label,
                steckbrief_1.letzte_aenderung.isoformat(),
                f"{baseurl}/document-profiles/D0000001/1.0",
            ],
            [
                "D0000002V1.0",
                "D",
                steckbrief_2.name,
                "-",
                "-",
                steckbrief_2.freigabe_status_label,
                steckbrief_2.letzte_aenderung.isoformat(),
                f"{baseurl}/document-profiles/D0000002/1.0",
            ],
        ]


class TestSchemaDownload:
    def test_should_download_a_schema_csv_file(
        self, baseurl: str, runner: CommandRunner, client: TestClient
    ):
        factory = Xdf2Factory()
        schema_1 = factory.schema(id="S0000001", version="1.0").save(runner)
        schema_2 = factory.schema(id="S0000002", version="1.0").save(runner)

        response = client.get(f"{ENDPOINT}?resource=schema&order_by=id_asc")
        assert response.status_code == 200

        assert response.headers["content-type"] == "text/csv; charset=utf-8"
        assert (
            response.headers["content-disposition"]
            == 'attachment; filename="schema_search_results.csv"'
        )

        reader = csv.reader(response.iter_lines())
        rows = list(reader)[1:]

        assert rows == [
            [
                "S0000001V1.0",
                "D",
                schema_1.name,
                "-",
                "-",
                schema_1.freigabe_status_label,
                schema_1.letzte_aenderung.isoformat(),
                f"{baseurl}/schemas/S0000001/1.0",
            ],
            [
                "S0000002V1.0",
                "D",
                schema_2.name,
                "-",
                "-",
                schema_2.freigabe_status_label,
                schema_2.letzte_aenderung.isoformat(),
                f"{baseurl}/schemas/S0000002/1.0",
            ],
        ]


class TestFieldDownload:
    def test_should_download_a_field_csv_file(
        self, baseurl: str, runner: CommandRunner, client: TestClient
    ):
        factory = Xdf2Factory()
        field_1 = factory.field(
            id="F0000001", version="1.0", status=Status.AKTIV
        ).build()
        field_2 = factory.field(
            id="F0000002", version="1.0", status=Status.AKTIV
        ).build()
        schema = factory.schema().with_field(field_1).with_field(field_2).save(runner)

        response = client.get(f"{ENDPOINT}?resource=field")
        assert response.status_code == 200

        assert response.headers["content-type"] == "text/csv; charset=utf-8"
        assert (
            response.headers["content-disposition"]
            == 'attachment; filename="datenfeld_search_results.csv"'
        )

        reader = csv.reader(response.iter_lines())
        rows = list(reader)[1:]

        assert rows == [
            [
                "F0000001V1.0",
                "D",
                field_1.name,
                "-",
                "-",
                "in Bearbeitung",
                schema.letzte_aenderung.isoformat(),
                f"{baseurl}/fields/baukasten/F0000001/1.0",
            ],
            [
                "F0000002V1.0",
                "D",
                field_2.name,
                "-",
                "-",
                "in Bearbeitung",
                schema.letzte_aenderung.isoformat(),
                f"{baseurl}/fields/baukasten/F0000002/1.0",
            ],
        ]


class TestGroupDownload:
    def test_should_download_a_group_csv_file(
        self, baseurl: str, runner: CommandRunner, client: TestClient
    ):
        factory = Xdf2Factory()
        group_1 = factory.group(
            id="G0000001", version="1.0", status=Status.AKTIV
        ).build()
        group_2 = factory.group(
            id="G0000002", version="1.0", status=Status.AKTIV
        ).build()
        schema = factory.schema().with_group(group_1).with_group(group_2).save(runner)

        response = client.get(f"{ENDPOINT}?resource=group")
        assert response.status_code == 200

        assert response.headers["content-type"] == "text/csv; charset=utf-8"
        assert (
            response.headers["content-disposition"]
            == 'attachment; filename="datenfeldgruppe_search_results.csv"'
        )

        reader = csv.reader(response.iter_lines())
        rows = list(reader)[1:]

        assert rows == [
            [
                "G0000001V1.0",
                "D",
                group_1.name,
                "-",
                "-",
                "in Bearbeitung",
                schema.letzte_aenderung.isoformat(),
                f"{baseurl}/groups/baukasten/G0000001/1.0",
            ],
            [
                "G0000002V1.0",
                "D",
                group_2.name,
                "-",
                "-",
                "in Bearbeitung",
                schema.letzte_aenderung.isoformat(),
                f"{baseurl}/groups/baukasten/G0000002/1.0",
            ],
        ]


class TestLeistungssteckbriefDownload:
    def test_should_download_a_leistungssteckbrief_csv_file(
        self, baseurl: str, runner: CommandRunner, client: TestClient
    ):
        steckbrief_1 = (
            LeistungFactory.steckbrief(bezeichnung="A")
            .with_freigabestatus_katalog(FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD)
            .with_freigabestatus_bibliothek(FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD)
            .save_leika(runner)
        )
        steckbrief_2 = (
            LeistungFactory.steckbrief(bezeichnung="B")
            .with_freigabestatus_katalog(FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD)
            .with_freigabestatus_bibliothek(FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD)
            .save_leika(runner)
        )

        response = client.get(f"{ENDPOINT}?resource=service&order_by=titel_asc")
        assert response.status_code == 200

        assert response.headers["content-type"] == "text/csv; charset=utf-8"
        assert (
            response.headers["content-disposition"]
            == 'attachment; filename="leistungssteckbrief_search_results.csv"'
        )

        reader = csv.reader(response.iter_lines())
        rows = list(reader)[1:]

        assert rows == [
            [
                steckbrief_1.leistungsschluessel,
                "L",
                steckbrief_1.title,
                steckbrief_1.typisierung[0].value,
                "fachlich freigegeben (gold)",
                "fachlich freigegeben (gold)",
                steckbrief_1.geaendert_datum_zeit.isoformat()
                if steckbrief_1.geaendert_datum_zeit
                else "-",
                f"{baseurl}/services/{steckbrief_1.leistungsschluessel}",
            ],
            [
                steckbrief_2.leistungsschluessel,
                "L",
                steckbrief_2.title,
                steckbrief_2.typisierung[0].value,
                "fachlich freigegeben (gold)",
                "fachlich freigegeben (gold)",
                steckbrief_2.geaendert_datum_zeit.isoformat()
                if steckbrief_2.geaendert_datum_zeit
                else "-",
                f"{baseurl}/services/{steckbrief_2.leistungsschluessel}",
            ],
        ]


class TestLeistungsbeschreibungDownload:
    def test_should_download_a_leistungsbeschreibung_csv_file(
        self, baseurl: str, runner: CommandRunner, client: TestClient
    ):
        leistung_1 = (
            LeistungFactory.leistungsbeschreibung()
            .with_bezeichnung("Leistung A")
            .add_typisierung(LeistungsTypisierung.TYP_1)
            .add_typisierung(LeistungsTypisierung.TYP_2_3)
            .with_freigabestatus_katalog(FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD)
            .with_freigabestatus_bibliothek(FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD)
            .save_pvog(runner)
        )
        leistung_2 = (
            LeistungFactory.leistungsbeschreibung()
            .with_bezeichnung("Leistung B")
            .with_freigabestatus_katalog(FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD)
            .with_freigabestatus_bibliothek(FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD)
            .save_pvog(runner)
        )

        response = client.get(
            f"{ENDPOINT}?resource=service-description&order_by=titel_asc"
        )
        assert response.status_code == 200

        assert response.headers["content-type"] == "text/csv; charset=utf-8"
        assert (
            response.headers["content-disposition"]
            == 'attachment; filename="leistungsbeschreibung_search_results.csv"'
        )

        reader = csv.reader(response.iter_lines())
        rows = list(reader)[1:]

        assert rows == [
            [
                f"{leistung_1.redaktion_id}/{leistung_1.id}",
                "L",
                "Leistung A",
                "1,2/3",
                "fachlich freigegeben (gold)",
                "fachlich freigegeben (gold)",
                leistung_1.geaendert_datum_zeit.isoformat()
                if leistung_1.geaendert_datum_zeit
                else "-",
                f"{baseurl}/xzufi-services/{leistung_1.redaktion_id}/{leistung_1.id}",
            ],
            [
                f"{leistung_2.redaktion_id}/{leistung_2.id}",
                "L",
                "Leistung B",
                "-",
                "fachlich freigegeben (gold)",
                "fachlich freigegeben (gold)",
                leistung_2.geaendert_datum_zeit.isoformat()
                if leistung_2.geaendert_datum_zeit
                else "-",
                f"{baseurl}/xzufi-services/{leistung_2.redaktion_id}/{leistung_2.id}",
            ],
        ]


class TestProzesseDownload:
    def test_should_download_a_prozess_csv_file(
        self, baseurl: str, runner: CommandRunner, client: TestClient
    ):
        now = utc_now()

        prozess_1 = ProzessFactory(
            name="A",
            detaillierungsstufe=None,
            freigabe_status=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
            letzter_aenderungszeitpunkt=now,
        ).save(runner)
        prozess_2 = ProzessFactory(
            name="B",
            detaillierungsstufe=None,
            freigabe_status=None,
            letzter_aenderungszeitpunkt=None,
        ).save(runner)

        response = client.get(f"{ENDPOINT}?resource=process")
        assert response.status_code == 200

        assert response.headers["content-type"] == "text/csv; charset=utf-8"
        assert (
            response.headers["content-disposition"]
            == 'attachment; filename="prozess_search_results.csv"'
        )

        reader = csv.reader(response.iter_lines())
        rows = list(reader)[1:]

        assert rows == [
            [
                prozess_1.id,
                "P",
                prozess_1.name,
                "-",
                "-",
                "fachlich freigegeben (gold)",
                now.isoformat(),
                f"{baseurl}/processes/{prozess_1.id}",
            ],
            [
                prozess_2.id,
                "P",
                prozess_2.name,
                "-",
                "-",
                "-",
                "-",
                f"{baseurl}/processes/{prozess_2.id}",
            ],
        ]


class TestMusterprozesseDownload:
    def test_should_download_a_musterprozess_csv_file(
        self, baseurl: str, runner: CommandRunner, client: TestClient
    ):
        now = utc_now()

        prozess_1 = ProzessFactory(
            name="A",
            detaillierungsstufe=Detaillierungsstufe.MUSTERINFORMATIONEN,
            freigabe_status=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
            letzter_aenderungszeitpunkt=now,
        ).save(runner)
        prozess_2 = ProzessFactory(
            name="B",
            detaillierungsstufe=Detaillierungsstufe.MUSTERINFORMATIONEN,
            freigabe_status=None,
            letzter_aenderungszeitpunkt=None,
        ).save(runner)

        response = client.get(f"{ENDPOINT}?resource=sampleprocess")
        assert response.status_code == 200

        assert response.headers["content-type"] == "text/csv; charset=utf-8"
        assert (
            response.headers["content-disposition"]
            == 'attachment; filename="musterprozess_search_results.csv"'
        )

        reader = csv.reader(response.iter_lines())
        rows = list(reader)[1:]

        assert rows == [
            [
                prozess_1.id,
                "P",
                prozess_1.name,
                "-",
                "-",
                "fachlich freigegeben (gold)",
                now.isoformat(),
                f"{baseurl}/processes/{prozess_1.id}",
            ],
            [
                prozess_2.id,
                "P",
                prozess_2.name,
                "-",
                "-",
                "-",
                "-",
                f"{baseurl}/processes/{prozess_2.id}",
            ],
        ]


class TestProzessklasseDownload:
    def test_should_download_a_prozessklasse(
        self, baseurl: str, runner: CommandRunner, client: TestClient
    ):
        now = utc_now()

        klasse_1 = ProzessklasseFactory(
            name="Prozess A",
            freigabe_status=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
            letzter_aenderungszeitpunkt=now,
        ).save(runner)
        klasse_2 = ProzessklasseFactory(
            name="Prozess B", freigabe_status=None, letzter_aenderungszeitpunkt=None
        ).save(runner)

        response = client.get(f"{ENDPOINT}?resource=processclass")
        assert response.status_code == 200

        assert response.headers["content-type"] == "text/csv; charset=utf-8"
        assert (
            response.headers["content-disposition"]
            == 'attachment; filename="prozessklasse_search_results.csv"'
        )

        reader = csv.reader(response.iter_lines())
        rows = list(reader)[1:]

        assert rows == [
            [
                klasse_1.id,
                "P",
                "Prozess A",
                "-",
                "fachlich freigegeben (gold)",
                "-",
                now.isoformat(),
                f"{baseurl}/processclasses/{klasse_1.id}",
            ],
            [
                klasse_2.id,
                "P",
                "Prozess B",
                "-",
                "-",
                "-",
                "-",
                f"{baseurl}/processclasses/{klasse_2.id}",
            ],
        ]
