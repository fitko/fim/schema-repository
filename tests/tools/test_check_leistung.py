from fastapi.testclient import TestClient

from tests.data import XZUFI_2_2_0_DATA


ENDPOINT = "/tools/check-leistung/report"


def test_get_quality_report(client: TestClient):
    with open(XZUFI_2_2_0_DATA / "leistung_minimal.xml", "rb") as file:
        response = client.post(ENDPOINT, files=[("leistung", file)])

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert "15 Fehlerhafte Checks" in response.text
