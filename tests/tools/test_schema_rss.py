from datetime import datetime, timezone
from fastapi.testclient import TestClient
from freezegun import freeze_time

from fimportal import xml
from tests.conftest import CommandRunner
from tests.factories.xdf2 import (
    Xdf2Factory,
    Xdf2SteckbriefImportFactory,
)


def test_should_return_all_latest_version_updates_for_a_schema(
    baseurl: str, runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()

    update_1_time = "2023-10-17T00:00:00"
    with freeze_time(update_1_time):
        factory.schema(id="S12345", version="1.0").save(runner)

    update_2_time = "2023-10-18T00:00:00"
    with freeze_time(update_2_time):
        factory.schema(id="S12345", version="2.0").save(runner)

    update_3_time = "2023-10-19T00:00:00"
    with freeze_time(update_3_time):
        factory.schema(id="S12345", version="1.1").save(runner)

    response = client.get("/tools/rss/schemas/S12345")
    assert response.status_code == 200

    expected_result = f"""<?xml version="1.0" encoding="UTF-8" ?>
        <rss version="2.0">
            <channel>
              <title>Stammdatenschema S12345 Updates</title>
              <link>{baseurl}/schemas/S12345/latest</link>
              <description>Aktuelle Updates für das Stammdatenschema S12345</description>
              <item>
                <title>S12345 v1.1 Update</title>
                <link>{baseurl}/schemas/S12345/1.1</link>
                <description>Das Stammdatenschema S12345 v1.1 wurde aktualisiert</description>
                <pubDate>Thu, 19 Oct 2023 00:00:00 +0000</pubDate>
              </item>
              <item>
                <title>S12345 v2.0 Update</title>
                <link>{baseurl}/schemas/S12345/2.0</link>
                <description>Das Stammdatenschema S12345 v2.0 wurde aktualisiert</description>
                <pubDate>Wed, 18 Oct 2023 00:00:00 +0000</pubDate>
              </item>
              <item>
                <title>S12345 v1.0 Update</title>
                <link>{baseurl}/schemas/S12345/1.0</link>
                <description>Das Stammdatenschema S12345 v1.0 wurde aktualisiert</description>
                <pubDate>Tue, 17 Oct 2023 00:00:00 +0000</pubDate>
              </item>
            </channel>
        </rss> 
    """

    assert xml.prettify(response.text) == xml.prettify(expected_result)


def test_should_return_all_recent_updates_for_the_baustein(
    baseurl: str, runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    factory.schema().save(
        runner,
        erstellungs_zeitpunkt=datetime(2023, 5, 17, tzinfo=timezone.utc),
    )

    steckbrief = Xdf2SteckbriefImportFactory(
        letzte_aenderung=datetime(2023, 10, 18, tzinfo=timezone.utc)
    ).save(runner)

    schema = (
        factory.schema(bezug="Bezug Schema")
        .with_group()
        .with_field()
        .save(runner, erstellungs_zeitpunkt=datetime(2023, 10, 19, tzinfo=timezone.utc))
    )

    query_time = "2023-10-21T00:00:00"
    with freeze_time(query_time):
        response = client.get("/tools/rss/schemas")

    assert response.status_code == 200
    expected_result = xml.prettify(f"""<?xml version="1.0" encoding="UTF-8" ?>
        <rss version="2.0">
            <channel>
              <title>Updates Baustein Datenfelder</title>
              <description>Aktuelle Updates des Bausteins Datenfelder</description>
              <item>
                <link>{baseurl}/schemas/{schema.fim_id}/{schema.fim_version}</link>
                <title>Schema '{schema.name}' wurde aktualisiert ({schema.fim_id} V{schema.fim_version})</title>
                <description>Handlungsgrundlage: {schema.bezug[0]} | Status: {schema.freigabe_status_label}</description>
                <pubDate>Thu, 19 Oct 2023 00:00:00 +0000</pubDate>
                <author>{schema.status_gesetzt_durch}</author>
              </item>
              <item>
                <link>{baseurl}/fields/{schema.datenfelder[0].namespace}/{schema.datenfelder[0].fim_id}/{schema.datenfelder[0].fim_version}</link>
                <title>Datenfeld '{schema.datenfelder[0].name}' wurde aktualisiert ({schema.datenfelder[0].fim_id} V{schema.datenfelder[0].fim_version})</title>
                <description>Handlungsgrundlage: - | Status: {schema.datenfelder[0].freigabe_status_label}</description>
                <pubDate>Thu, 19 Oct 2023 00:00:00 +0000</pubDate>
                <author></author>
              </item>
              <item>
                <link>{baseurl}/groups/{schema.datenfeldgruppen[0].namespace}/{schema.datenfeldgruppen[0].fim_id}/{schema.datenfeldgruppen[0].fim_version}</link>
                <title>Datenfeldgruppe '{schema.datenfeldgruppen[0].name}' wurde aktualisiert ({schema.datenfeldgruppen[0].fim_id} V{schema.datenfeldgruppen[0].fim_version})</title>
                <description>Handlungsgrundlage: - | Status: {schema.datenfeldgruppen[0].freigabe_status_label}</description>
                <pubDate>Thu, 19 Oct 2023 00:00:00 +0000</pubDate>
                <author></author>
              </item>
              <item>
                <link>{baseurl}/document-profiles/{steckbrief.fim_id}/{steckbrief.fim_version}</link>
                <title>Dokumentensteckbrief '{steckbrief.name}' wurde aktualisiert ({steckbrief.fim_id} V{steckbrief.fim_version})</title>
                <description>Handlungsgrundlage: - | Status: {steckbrief.freigabe_status_label}</description>
                <pubDate>Wed, 18 Oct 2023 00:00:00 +0000</pubDate>
                <author></author>
              </item>
            </channel>
          </rss>
    """)
    assert xml.prettify(response.text) == expected_result


def test_should_replace_illegal_characters(
    baseurl: str, runner: CommandRunner, client: TestClient
):
    schema = (
        Xdf2Factory()
        .schema(bezug="Ein Bezug & ein schlechtes Zeichen<")
        .save(
            runner,
            erstellungs_zeitpunkt=datetime(2023, 10, 17, tzinfo=timezone.utc),
        )
    )

    query_time = "2023-10-21T00:00:00"
    with freeze_time(query_time):
        response = client.get("/tools/rss/schemas")

    expected_result = xml.prettify(f"""<?xml version="1.0" encoding="UTF-8" ?>
          <rss version="2.0">
            <channel>
              <title>Updates Baustein Datenfelder</title>
              <description>Aktuelle Updates des Bausteins Datenfelder</description>
              <item>
                <link>{baseurl}/schemas/{schema.fim_id}/{schema.fim_version}</link>
                <title>Schema '{schema.name}' wurde aktualisiert ({schema.fim_id} V{schema.fim_version})</title>
                <description>Handlungsgrundlage: Ein Bezug &amp; ein schlechtes Zeichen&lt; | Status: {schema.freigabe_status_label}</description>
                <pubDate>Tue, 17 Oct 2023 00:00:00 +0000</pubDate>
                <author>{schema.status_gesetzt_durch}</author>
              </item>
            </channel>
          </rss>
    """)
    assert xml.prettify(response.text) == expected_result


def test_should_return_404_for_unknown_schema(client: TestClient):
    response = client.get("/tools/rss/schemas/S12345")

    assert response.status_code == 404
