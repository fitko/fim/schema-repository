from fastapi.testclient import TestClient

from tests.data import XDF2_DATA


def test_should_return_the_quality_report(client: TestClient):
    with open(XDF2_DATA / "select.xml", "rb") as schema:
        response = client.post(
            "/tools/check-xdf2",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 200
    assert response.json() == {
        "group_reports": [],
        "total_group_checks": 0,
        "field_reports": [
            {
                "identifier": {"id": "F123", "version": "1.0", "nummernkreis": "12"},
                "failing_checks": [
                    {
                        "code": 1115,
                        "error_type": "warning",
                        "message": "Auf die Codeliste urn:de:example_1 kann nicht im XRepository zugegriffen werden. Dies könnte auf einen Fehler in der URN oder der Version der Codeliste zurückzuführen sein.",
                    }
                ],
            }
        ],
        "total_field_checks": 1,
        "rule_reports": [],
        "total_rule_checks": 0,
        "schema_checks": [],
        "total_checks": 1,
    }


def test_should_work_for_schema_without_version(client: TestClient):
    with open(XDF2_DATA / "schema_without_versions.xml", "rb") as schema:
        response = client.post(
            "/tools/check-xdf2",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 200
