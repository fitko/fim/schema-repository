import pathlib

from fastapi.testclient import TestClient

from fimportal import xrepository
from tests.data import XDF3_DATA

PROJECT_DIR = pathlib.Path(__file__).parents[2]


def test_should_return_json_converter_html_page(client: TestClient):
    response = client.get("/tools/xdf3-json-schema-converter")

    assert response.status_code == 200


def test_should_return_the_generated_json_schema(
    client: TestClient, xrepository_client: xrepository.FakeClient
):
    xrepository_client.register_code_list_file(XDF3_DATA / "code_list_a.xml")

    with (
        open(XDF3_DATA / "schema_with_code_list.xdf3.xml", "rb") as schema,
    ):
        response = client.post(
            "/tools/xdf3-json-schema-converter",
            files=[("schema", schema)],
        )

    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    assert (
        response.headers["content-disposition"]
        == 'attachment; filename="S12345V1.0.0_schema.json"'
    )

    assert response.json()["$schema"] == "https://json-schema.org/draft/2020-12/schema"


def test_should_fail_for_missing_code_lists(client: TestClient):
    with open(XDF3_DATA / "schema_with_code_list.xdf3.xml", "rb") as schema:
        response = client.post(
            "/tools/xdf3-json-schema-converter",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 400
    assert (
        response.json()["detail"]
        == "Cannot find code list 'urn:de:code_list_a_1' in XRepository"
    )


def test_should_ignore_unused_code_lists(client: TestClient):
    with (
        open(XDF3_DATA / "schema_without_code_lists.xdf3.xml", "rb") as schema,
        open(XDF3_DATA / "code_list_a.xml", "rb") as code_list,
    ):
        response = client.post(
            "/tools/xdf3-json-schema-converter",
            files=[("schema", schema), ("code_lists", code_list)],
        )

    assert response.status_code == 200


def test_should_fail_for_invalid_schema(client: TestClient):
    with open(XDF3_DATA / "code_list_a.xml", "rb") as schema:
        response = client.post(
            "/tools/xdf3-json-schema-converter",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 400
    assert response.json() == {
        "detail": "Cannot parse schema: Element '{http://docs.oasis-open.org/codelist/ns/genericode/1.0/}CodeList': No matching global declaration available for the validation root., line 2"
    }


def test_bug_267(client: TestClient):
    """
    See https://gitlab.opencode.de/fitko/fim/schema-repository/-/issues/267
    """
    with open(XDF3_DATA / "hundesteuer.xdf3.xml", "rb") as schema:
        response = client.post(
            "/tools/xdf3-json-schema-converter",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 400


def test_should_work_for_schema_without_version(client: TestClient):
    with open(XDF3_DATA / "schema_without_versions.xml", "rb") as schema:
        response = client.post(
            "/tools/xdf3-json-schema-converter",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 200
