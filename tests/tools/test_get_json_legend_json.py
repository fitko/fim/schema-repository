import orjson
from fastapi.testclient import TestClient
from freezegun import freeze_time

from fimportal.helpers import format_iso8601
from tests.conftest import CommandRunner
from tests.factories.xdf3 import (
    Xdf3Factory,
)

PATH = "/tools/fim-json-legend"


def test_should_work_with_just_the_fim_id(runner: CommandRunner, client: TestClient):
    """
    Older JSON Schemas just require the FIM ID as the identifier, our newer converter
    also uses the field version. If no version is specified, just the latest data should be used.
    """
    factory = Xdf3Factory()
    field_old = factory.field(id="F0000001", version="1.0.0").build()
    field_new = factory.field(id="F0000001", version="2.0.0").build()
    group = factory.group().with_field(field_new).build()

    with freeze_time("2023-10-17T00:00:00"):
        factory.schema().with_group(group).with_field(field_old).save(runner)

    payload = orjson.dumps(
        {
            group.identifier.id: {
                field_new.identifier.id: "value",
            }
        }
    )

    response = client.post(
        PATH,
        files=[("data", payload)],
    )

    assert response.status_code == 200
    assert response.json() == {
        "fields": {
            f"{field_new.identifier.id}V{field_new.identifier.version}": {
                "namespace": "baukasten",
                "fim_id": field_new.identifier.id,
                "fim_version": field_new.identifier.version,
                "nummernkreis": "00000",
                "name": field_new.name,
                "beschreibung": field_new.beschreibung,
                "definition": field_new.definition,
                "bezug": [],
                "freigabe_status": field_new.freigabe_status.value,
                "freigabe_status_label": field_new.freigabe_status.to_label(),
                "gueltig_ab": None,
                "gueltig_bis": None,
                "letzte_aenderung": format_iso8601(field_new.letzte_aenderung),
                "last_update": "2023-10-17T00:00:00Z",
                "feldart": field_new.feldart.value,
                "datentyp": field_new.datentyp.value,
                "status_gesetzt_durch": field_new.status_gesetzt_durch,
                "status_gesetzt_am": None,
                "veroeffentlichungsdatum": None,
                "versionshinweis": field_new.versionshinweis,
                "xdf_version": "3.0.0",
                "fts_match": None,
                "is_latest": True,
            }
        },
        "groups": {
            f"{group.identifier.id}V{group.identifier.version}": {
                "namespace": "baukasten",
                "fim_id": group.identifier.id,
                "fim_version": group.identifier.version,
                "nummernkreis": "00000",
                "bezug": [],
                "name": group.name,
                "beschreibung": group.beschreibung,
                "definition": group.definition,
                "freigabe_status": group.freigabe_status.value,
                "freigabe_status_label": group.freigabe_status.to_label(),
                "status_gesetzt_durch": group.status_gesetzt_durch,
                "xdf_version": "3.0.0",
                "status_gesetzt_am": None,
                "veroeffentlichungsdatum": None,
                "gueltig_ab": None,
                "gueltig_bis": None,
                "letzte_aenderung": format_iso8601(group.letzte_aenderung),
                "last_update": "2023-10-17T00:00:00Z",
                "versionshinweis": group.versionshinweis,
                "is_latest": True,
                "fts_match": None,
            }
        },
    }


def test_should_work_with_fim_id_and_version(runner: CommandRunner, client: TestClient):
    factory = Xdf3Factory()
    field_old = factory.field(id="F0000001", version="1.0.0").build()
    field_new = factory.field(id="F0000001", version="2.0.0").build()
    group = factory.group().with_field(field_old).build()

    with freeze_time("2023-10-17T00:00:00"):
        factory.schema().with_group(group).with_field(field_new).save(runner)

    payload = orjson.dumps(
        {
            f"{group.identifier.id}V{group.identifier.version}": {
                "F0000001V1.0.0": "value",
            }
        }
    )

    response = client.post(
        PATH,
        files=[("data", payload)],
    )

    assert response.status_code == 200
    assert response.json() == {
        "fields": {
            f"{field_old.identifier.id}V{field_old.identifier.version}": {
                "namespace": "baukasten",
                "fim_id": field_old.identifier.id,
                "fim_version": field_old.identifier.version,
                "nummernkreis": "00000",
                "name": field_old.name,
                "beschreibung": field_old.beschreibung,
                "definition": field_old.definition,
                "bezug": [],
                "freigabe_status": field_old.freigabe_status.value,
                "freigabe_status_label": field_old.freigabe_status.to_label(),
                "gueltig_ab": None,
                "gueltig_bis": None,
                "letzte_aenderung": format_iso8601(field_old.letzte_aenderung),
                "last_update": "2023-10-17T00:00:00Z",
                "feldart": field_old.feldart.value,
                "datentyp": field_old.datentyp.value,
                "status_gesetzt_durch": field_old.status_gesetzt_durch,
                "status_gesetzt_am": None,
                "veroeffentlichungsdatum": None,
                "versionshinweis": field_new.versionshinweis,
                "xdf_version": "3.0.0",
                "fts_match": None,
                "is_latest": False,
            }
        },
        "groups": {
            f"{group.identifier.id}V{group.identifier.version}": {
                "namespace": "baukasten",
                "fim_id": group.identifier.id,
                "fim_version": group.identifier.version,
                "nummernkreis": "00000",
                "bezug": [],
                "name": group.name,
                "beschreibung": group.beschreibung,
                "definition": group.definition,
                "freigabe_status": group.freigabe_status.value,
                "freigabe_status_label": group.freigabe_status.to_label(),
                "status_gesetzt_durch": group.status_gesetzt_durch,
                "xdf_version": "3.0.0",
                "status_gesetzt_am": None,
                "veroeffentlichungsdatum": None,
                "gueltig_ab": None,
                "gueltig_bis": None,
                "letzte_aenderung": format_iso8601(group.letzte_aenderung),
                "last_update": "2023-10-17T00:00:00Z",
                "versionshinweis": group.versionshinweis,
                "is_latest": True,
                "fts_match": None,
            }
        },
    }


def test_should_include_unknown_fields_as_none(
    runner: CommandRunner, client: TestClient
):
    payload = orjson.dumps(
        {
            "F0000001V1.0.0": "value",
            "F0000002": "another value",
        }
    )

    response = client.post(
        PATH,
        files=[("data", payload)],
    )

    assert response.status_code == 200
    assert response.json() == {
        "fields": {
            "F0000001V1.0.0": None,
            "F0000002": None,
        },
        "groups": {},
    }


def test_should_fail_for_invalid_json(client: TestClient):
    response = client.post(
        PATH,
        files=[("data", "<not></json>")],
    )

    assert response.status_code == 400
    assert response.json() == {
        "detail": "unexpected character: line 1 column 1 (char 0)"
    }
