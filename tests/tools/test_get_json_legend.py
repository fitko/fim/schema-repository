import orjson
from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.factories.xdf3 import (
    Xdf3Factory,
)


def test_should_return_the_form_page(client: TestClient):
    response = client.get("/tools/fim-json-legend")

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_should_return_the_html_page(runner: CommandRunner, client: TestClient):
    factory = Xdf3Factory()
    field = factory.field().build()
    group = factory.group().with_field(field).build()
    factory.schema().with_group(group).save(runner)

    payload = orjson.dumps(
        {
            group.identifier.id: {
                field.identifier.id: "value",
            },
            # Unknown group
            "G000001234": {
                # Unknown Field
                "F000001234": "value"
            },
        }
    )

    response = client.post(
        "/tools/fim-json-legend/result",
        files=[
            ("data", payload),
        ],
    )

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_should_print_the_error_for_json_data(client: TestClient):
    response = client.post(
        "/tools/fim-json-legend/result",
        files=[
            ("data", "<not></json>"),
        ],
    )

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert "unexpected character: line 1 column 1 (char 0)" in response.text
