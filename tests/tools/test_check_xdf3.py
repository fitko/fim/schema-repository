from fastapi.testclient import TestClient

from tests.data import XDF3_DATA


def test_should_return_the_check_page(client: TestClient):
    response = client.get("/tools/check-xdf3")

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_should_return_the_html_page_for_no_failing_checks(client: TestClient):
    with open(XDF3_DATA / "schema_without_code_lists.xdf3.xml", "rb") as schema:
        response = client.post(
            "/tools/check-xdf3/report",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert "kritische Fehler vorhanden" not in response.text
    assert "methodische Fehler vorhanden" not in response.text
    assert "Warnungen vorhanden" not in response.text
    assert "Hinweise vorhanden" not in response.text


def test_should_return_the_html_page_for_failing_checks(client: TestClient):
    with open(XDF3_DATA / "schema.xml", "rb") as schema:
        response = client.post(
            "/tools/check-xdf3/report",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert "1087" in response.text
    assert "1013" in response.text
    assert "1106" in response.text
    assert "1043" in response.text


def test_should_print_the_error_for_invalid_schema(client: TestClient):
    with open(XDF3_DATA / "code_list_a.xml", "rb") as schema:
        response = client.post(
            "/tools/check-xdf3/report",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert "Cannot parse schema" in response.text


def test_should_work_for_schema_without_version(client: TestClient):
    with open(XDF3_DATA / "schema_without_versions.xml", "rb") as schema:
        response = client.post(
            "/tools/check-xdf3/report",
            files=[
                ("schema", schema),
            ],
        )

    assert response.status_code == 200
