from datetime import date

import pytest
from fastapi.testclient import TestClient
from freezegun import freeze_time

from fimportal.din91379 import parse_string_latin
from fimportal.helpers import format_iso8601
from fimportal.json_schema import xdf2
from fimportal.models.imports import XdfVersion
from fimportal.xdatenfelder import xdf3
from tests.conftest import CommandRunner
from tests.factories.xdf2 import GroupFactory, Xdf2Factory
from tests.factories.xdf3 import Xdf3Factory


def test_should_correctly_return_a_group(
    runner: CommandRunner,
    client: TestClient,
):
    factory = Xdf3Factory()
    field = factory.field(id="F12000").build()
    group = (
        factory.group(id="G12000")
        .with_field(
            field,
            xdf3.Anzahl(1, 2),
            [xdf3.Rechtsbezug(parse_string_latin("Rechtsbezug"), link="link")],
        )
        .build()
    )
    xdf3_import = factory.schema("S12000").with_group(group).build_import()
    schema = xdf3_import.schema_message.schema

    with freeze_time("2023-10-17T00:00:00"):
        runner.import_xdf3(xdf3_import)

    response = client.get(
        f"/api/v1/groups/baukasten/{group.identifier.id}/{group.identifier.version}"
    )

    assert response.status_code == 200

    assert response.json() == {
        "namespace": "baukasten",
        "fim_id": group.identifier.id,
        "fim_version": group.identifier.version,
        "nummernkreis": group.identifier.nummernkreis,
        "name": group.name,
        "beschreibung": group.beschreibung,
        "definition": group.definition,
        "bezug": group.bezug,
        "freigabe_status": group.freigabe_status.value,
        "freigabe_status_label": group.freigabe_status.to_label(),
        "status_gesetzt_am": group.status_gesetzt_am,
        "status_gesetzt_durch": group.status_gesetzt_durch,
        "gueltig_ab": group.gueltig_ab,
        "gueltig_bis": group.gueltig_bis,
        "versionshinweis": group.versionshinweis,
        "veroeffentlichungsdatum": group.veroeffentlichungsdatum,
        "letzte_aenderung": format_iso8601(group.letzte_aenderung),
        "last_update": "2023-10-17T00:00:00Z",
        "is_latest": True,
        "schemas": [
            {
                "fim_id": schema.identifier.id,
                "fim_version": schema.identifier.assert_version(),
                "nummernkreis": schema.identifier.nummernkreis,
                "name": schema.name,
                "beschreibung": schema.beschreibung,
                "definition": schema.definition,
                "bezug": schema.bezug,
                "freigabe_status": xdf3_import.freigabe_status.value,
                "freigabe_status_label": xdf3_import.freigabe_status.to_label(),
                "gueltig_ab": schema.gueltig_ab,
                "gueltig_bis": schema.gueltig_bis,
                "status_gesetzt_durch": schema.status_gesetzt_durch,
                "versionshinweis": schema.versionshinweis,
                "stichwort": schema.stichwort,
                "steckbrief_id": schema.dokumentsteckbrief.id,
                "xdf_version": XdfVersion.XDF3.value,
                "letzte_aenderung": format_iso8601(schema.letzte_aenderung),
                "last_update": "2023-10-17T00:00:00Z",
                "status_gesetzt_am": schema.status_gesetzt_am,
                "bezug_components": ["Rechtsbezug"],
                "bezeichnung": schema.bezeichnung,
                "veroeffentlichungsdatum": schema.veroeffentlichungsdatum,
                "is_latest": True,
                "fts_match": None,
            }
        ],
        "datenfelder": [
            {
                "namespace": "baukasten",
                "fim_id": field.identifier.id,
                "fim_version": field.identifier.assert_version(),
                "nummernkreis": field.identifier.nummernkreis,
                "name": field.name,
                "beschreibung": field.beschreibung,
                "bezug": field.bezug,
                "datentyp": field.datentyp.value,
                "definition": field.definition,
                "feldart": field.feldart.value,
                "freigabe_status": field.freigabe_status.value,
                "freigabe_status_label": field.freigabe_status.to_label(),
                "gueltig_ab": field.gueltig_ab,
                "gueltig_bis": field.gueltig_bis,
                "last_update": "2023-10-17T00:00:00Z",
                "letzte_aenderung": format_iso8601(field.letzte_aenderung),
                "status_gesetzt_am": field.status_gesetzt_am,
                "status_gesetzt_durch": field.status_gesetzt_durch,
                "veroeffentlichungsdatum": field.veroeffentlichungsdatum,
                "versionshinweis": field.versionshinweis,
                "xdf_version": XdfVersion.XDF3.value,
                "fts_match": None,
                "is_latest": True,
            },
        ],
        "datenfeldgruppen": [],
        "children": [
            {
                "namespace": "baukasten",
                "fim_id": field.identifier.id,
                "fim_version": field.identifier.assert_version(),
                "type": "Feld",
                "bezug": ["Rechtsbezug"],
                "anzahl": "1:2",
            }
        ],
        "regeln": [],
        "relation": [],
        "xdf_version": "3.0.0",
    }


@pytest.mark.parametrize(
    "group_factory,freigabe_status",
    [
        (
            Xdf2Factory().group(status=xdf2.Status.INAKTIV),
            xdf3.FreigabeStatus.INAKTIV,
        ),
        (
            Xdf2Factory().group(status=xdf2.Status.IN_VORBEREITUNG),
            xdf3.FreigabeStatus.IN_BEARBEITUNG,
        ),
        (
            Xdf2Factory().group(
                status=xdf2.Status.AKTIV,
                freigabedatum=None,
            ),
            xdf3.FreigabeStatus.IN_BEARBEITUNG,
        ),
        (
            Xdf2Factory().group(
                status=xdf2.Status.AKTIV,
                freigabedatum=date.today(),
            ),
            xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        ),
    ],
)
def test_should_correctly_map_the_freigabe_status(
    runner: CommandRunner,
    client: TestClient,
    group_factory: GroupFactory,
    freigabe_status: xdf3.FreigabeStatus,
):
    group = group_factory.build()
    group_factory.factory.schema().with_group(group).save(runner)

    response = client.get(
        f"/api/v1/groups/baukasten/{group.identifier.id}/{group.identifier.version}"
    )

    assert response.status_code == 200
    assert response.json()["freigabe_status"] == freigabe_status.value


def test_should_include_all_children(
    runner: CommandRunner,
    client: TestClient,
):
    factory = Xdf3Factory()
    field = factory.field(id="F120001").build()
    sub_group = (
        factory.group(id="G120001")
        .with_field(
            field,
        )
        .build()
    )
    group = factory.group(id="G120002").with_group(sub_group).build()
    (factory.schema("S12000").with_group(group).save(runner))

    response = client.get(
        f"/api/v1/groups/baukasten/{group.identifier.id}/{group.identifier.version}"
    )

    assert response.status_code == 200
    result = response.json()

    assert len(result["datenfeldgruppen"]) == 1
    assert result["datenfeldgruppen"][0]["namespace"] == "baukasten"
    assert result["datenfeldgruppen"][0]["fim_id"] == sub_group.identifier.id
    assert (
        result["datenfeldgruppen"][0]["fim_version"]
        == sub_group.identifier.assert_version()
    )

    assert len(result["datenfelder"]) == 1
    assert result["datenfelder"][0]["namespace"] == "baukasten"
    assert result["datenfelder"][0]["fim_id"] == field.identifier.id
    assert result["datenfelder"][0]["fim_version"] == field.identifier.assert_version()


def test_should_return_latest_version(runner: CommandRunner, client: TestClient):
    factory = Xdf2Factory()
    latest_group_version = factory.group(
        version="2.0",
        bezug="Bezug",
    ).build()
    with freeze_time("2023-10-17T00:00:00"):
        schema = factory.schema().with_group(latest_group_version).save(runner)

    previous_group_version = factory.group(
        id=latest_group_version.identifier.id,
        version="1.0",
    ).build()
    factory.schema().with_group(previous_group_version).save(runner)

    response = client.get(
        f"/api/v1/groups/baukasten/{latest_group_version.identifier.id}/latest"
    )

    assert response.status_code == 200
    assert response.json()["fim_id"] == latest_group_version.identifier.id
    assert response.json()["fim_version"] == latest_group_version.identifier.version
    assert response.json() == {
        "namespace": "baukasten",
        "fim_id": latest_group_version.identifier.id,
        "fim_version": latest_group_version.identifier.version,
        "nummernkreis": latest_group_version.identifier.get_xdf3_nummernkreis(),
        "name": latest_group_version.name,
        "beschreibung": latest_group_version.beschreibung,
        "definition": latest_group_version.definition,
        "bezug": ["Bezug"],
        "freigabe_status": 2,
        "freigabe_status_label": "in Bearbeitung",
        "status_gesetzt_am": latest_group_version.freigabedatum,
        "status_gesetzt_durch": latest_group_version.fachlicher_ersteller,
        "gueltig_ab": latest_group_version.gueltig_ab,
        "gueltig_bis": latest_group_version.gueltig_bis,
        "versionshinweis": latest_group_version.versionshinweis,
        "veroeffentlichungsdatum": latest_group_version.veroeffentlichungsdatum,
        "letzte_aenderung": format_iso8601(schema.letzte_aenderung),
        "last_update": "2023-10-17T00:00:00Z",
        "is_latest": True,
        "schemas": [
            {
                "fim_id": schema.fim_id,
                "fim_version": schema.fim_version,
                "nummernkreis": schema.nummernkreis,
                "name": schema.name,
                "beschreibung": schema.beschreibung,
                "definition": schema.definition,
                "bezug": schema.bezug,
                "freigabe_status": schema.freigabe_status.value,
                "freigabe_status_label": schema.freigabe_status_label,
                "gueltig_ab": schema.gueltig_ab,
                "gueltig_bis": schema.gueltig_bis,
                "status_gesetzt_durch": schema.status_gesetzt_durch,
                "versionshinweis": schema.versionshinweis,
                "stichwort": schema.stichwort,
                "steckbrief_id": schema.steckbrief_id,
                "xdf_version": schema.xdf_version.value,
                "letzte_aenderung": format_iso8601(schema.letzte_aenderung),
                "last_update": "2023-10-17T00:00:00Z",
                "status_gesetzt_am": schema.status_gesetzt_am,
                "bezug_components": ["Bezug"],
                "bezeichnung": schema.bezeichnung,
                "veroeffentlichungsdatum": schema.veroeffentlichungsdatum,
                "is_latest": True,
                "fts_match": None,
            }
        ],
        "datenfelder": [],
        "datenfeldgruppen": [],
        "children": [],
        "regeln": [],
        "relation": [],
        "xdf_version": "2.0",
    }


def test_should_fail_for_unknown_group_with_latest_version(client: TestClient):
    response = client.get("/api/v1/groups/baukasten/G1/latest")

    assert response.status_code == 404
    assert response.json() == {
        "detail": "Could not find group G1Vlatest in namespace baukasten."
    }


def test_should_fail_for_unknown_group(
    client: TestClient,
):
    response = client.get("/api/v1/groups/baukasten/G1/1.0")

    assert response.status_code == 404
    assert response.json() == {
        "detail": "Could not find group G1V1.0 in namespace baukasten."
    }


def test_should_correctly_return_a_group_bezug_with_special_characters(
    runner: CommandRunner,
    client: TestClient,
):
    """This test is intended to test `database.insert_missing_groups`.

    This happens when we import a group that has a different Nummernkreis
    than the schema with `strict=false.`

    To verify, you can change `database.insert_missing_groups` so that
    it inputs `["Foo"]` instead of `group.bezug` into the query. Then,
    this test should fail at

    ```
    assert ['Foo'] == ['Bezug', '<>', "&%'", '"', '""', '\\', '\\"']
    ```
    """
    factory = Xdf3Factory()
    field = factory.field(id="F12000").build()
    group = (
        factory.group(
            id="G13000",
        )
        .with_bezug("Bezug")
        .with_bezug("<>")
        .with_bezug("&%'")
        .with_bezug('"')
        .with_bezug('""')
        .with_bezug("\\")
        .with_bezug('\\"')
        .with_field(
            field,
            xdf3.Anzahl(1, 2),
            [xdf3.Rechtsbezug(parse_string_latin("Rechtsbezug"), link="link")],
        )
        .build()
    )
    xdf3_import = factory.schema("S12000").with_group(group).build_import()
    xdf3_import.schema_message.schema

    with freeze_time("2023-10-17T00:00:00"):
        runner.import_xdf3(xdf3_import, strict=False)

    response = client.get(
        f"/api/v1/groups/baukasten/{group.identifier.id}/{group.identifier.version}"
    )

    assert response.status_code == 200
    assert response.json()["bezug"] == ["Bezug", "<>", "&%'", '"', '""', "\\", '\\"']
