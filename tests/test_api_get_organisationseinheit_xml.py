from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.factories.xzufi import (
    TEST_ORGANISATIONSEINHEIT_XML,
    OrganisationseinheitFactory,
)

ENDPOINT = "/api/v0/organizational-unit"


def test_return_existing_organisationseinheit(
    runner: CommandRunner, client: TestClient
):
    orga_einheit = OrganisationseinheitFactory().save(
        runner, xml_content=TEST_ORGANISATIONSEINHEIT_XML
    )

    response = client.get(
        f"{ENDPOINT}/{orga_einheit.redaktion_id}/{orga_einheit.id}/xzufi"
    )

    assert response.status_code == 200
    assert response.text == TEST_ORGANISATIONSEINHEIT_XML
    assert (
        response.headers["content-disposition"]
        == f'attachment; filename="organisationseinheit_{orga_einheit.redaktion_id}_{orga_einheit.id}.xzufi.xml"'
    )


def test_fail_for_unknown_organisationeinheit(client: TestClient):
    response = client.get(f"{ENDPOINT}/L1234/oe_1/xzufi")

    assert response.status_code == 404
    assert response.json() == {
        "detail": "Could not find Organisationseinheit oe_1 from Redaktion L1234.",
    }
