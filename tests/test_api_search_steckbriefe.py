from typing import Any

import pytest
from fastapi.testclient import TestClient
from freezegun import freeze_time

from fimportal.helpers import format_iso8601, optional_date_to_string
from fimportal.models.imports import Xdf2SteckbriefImport
from fimportal.xdatenfelder import xdf3
from tests.conftest import CommandRunner
from tests.data import XDF2_DATA
from tests.factories import Xdf2SteckbriefImportFactory


def test_should_correctly_return_a_steckbrief(
    runner: CommandRunner,
    client: TestClient,
):
    freeze_timestamp = "2023-10-17T00:00:00Z"
    with freeze_time(freeze_timestamp):
        steckbrief = (
            Xdf2SteckbriefImportFactory("D12345", "1.0", bezug="Rechtsbezug")
            .full()
            .save(runner)
        )

    response = client.get("/api/v1/document-profiles")

    assert response.status_code == 200
    assert response.json() == {
        "offset": 0,
        "limit": 200,
        "count": 1,
        "total_count": 1,
        "items": [
            {
                "fim_id": steckbrief.fim_id,
                "fim_version": steckbrief.fim_version,
                "nummernkreis": steckbrief.nummernkreis,
                "name": steckbrief.name,
                "definition": steckbrief.definition,
                "freigabe_status": steckbrief.freigabe_status.value,
                "freigabe_status_label": steckbrief.freigabe_status_label,
                "status_gesetzt_durch": steckbrief.status_gesetzt_durch,
                "status_gesetzt_am": optional_date_to_string(
                    steckbrief.status_gesetzt_am
                ),
                "gueltig_ab": optional_date_to_string(steckbrief.gueltig_ab),
                "gueltig_bis": optional_date_to_string(steckbrief.gueltig_bis),
                "bezug": steckbrief.bezug,
                "versionshinweis": steckbrief.versionshinweis,
                "veroeffentlichungsdatum": optional_date_to_string(
                    steckbrief.veroeffentlichungsdatum
                ),
                "letzte_aenderung": format_iso8601(steckbrief.letzte_aenderung),
                "last_update": freeze_timestamp,
                "bezeichnung": steckbrief.bezeichnung,
                "dokumentart": steckbrief.dokumentart.value,
                "stichwort": steckbrief.stichwort,
                "xdf_version": "2.0",
                "beschreibung": steckbrief.beschreibung,
                "ist_abstrakt": steckbrief.ist_abstrakt,
                "hilfetext": steckbrief.hilfetext,
                "is_latest": True,
                "fts_match": None,
            }
        ],
    }


def test_should_load_the_steckbrief_with_updated_since(
    runner: CommandRunner, client: TestClient
):
    early_date = "2023-10-17T00:00:00"
    late_date = "2023-10-18T00:00:00"

    with freeze_time(early_date):
        Xdf2SteckbriefImportFactory("D123").save(runner)

    with freeze_time(late_date):
        Xdf2SteckbriefImportFactory("D234").save(runner)

    response = client.get(f"/api/v1/document-profiles?updated_since={late_date}")
    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "D234"


def test_should_return_all_document_profile(runner: CommandRunner, client: TestClient):
    Xdf2SteckbriefImportFactory().save(runner)
    Xdf2SteckbriefImportFactory().save(runner)

    response = client.get("/api/v1/document-profiles")

    assert response.status_code == 200
    assert response.json()["count"] == 2


def test_should_set_the_total_count(runner: CommandRunner, client: TestClient):
    for _ in range(5):
        Xdf2SteckbriefImportFactory().save(runner)

    response = client.get("/api/v1/document-profiles?limit=2")

    assert response.status_code == 200

    result = response.json()
    assert result["count"] == 2
    assert result["offset"] == 0
    assert result["limit"] == 2
    assert result["total_count"] == 5


def test_should_search_for_steckbrief_name(runner: CommandRunner, client: TestClient):
    steckbrief = Xdf2SteckbriefImportFactory(name="Personalausweis").save(runner)
    Xdf2SteckbriefImportFactory(name="Umzug").save(runner)

    response = client.get("/api/v1/document-profiles?name=Perso")

    assert response.status_code == 200
    results: list[Any] = response.json()["items"]
    assert len(results) == 1
    assert results[0]["fim_id"] == steckbrief.fim_id


def test_should_ignore_search_term_case(runner: CommandRunner, client: TestClient):
    Xdf2SteckbriefImportFactory(name="personalausweis").save(runner)
    Xdf2SteckbriefImportFactory(name="Personalausweis").save(runner)

    response = client.get("/api/v1/document-profiles?name=Perso")

    assert response.status_code == 200
    assert response.json()["count"] == 2


def test_should_work_with_an_empty_term(runner: CommandRunner, client: TestClient):
    Xdf2SteckbriefImportFactory().save(runner)

    response = client.get("/api/v1/document-profiles?name=")

    assert response.status_code == 200
    assert response.json()["count"] == 1


@pytest.mark.parametrize(
    "id, term",
    [
        ("D12345", "1"),
        ("D12345", "12000"),
    ],
)
def test_should_filter_for_nummernkreis(
    runner: CommandRunner, client: TestClient, id: str, term: str
):
    Xdf2SteckbriefImportFactory(id=id).save(runner)

    response = client.get(f"/api/v1/document-profiles?nummernkreis={term}")

    assert response.status_code == 200
    results: list[Any] = response.json()["items"]
    assert len(results) == 1


@pytest.mark.parametrize(
    "id, term",
    [
        ("D12345", "2"),
        ("D12345", "2000"),
    ],
)
def test_should_fail_filter_for_invalid_nummernkreis(
    runner: CommandRunner, client: TestClient, id: str, term: str
):
    Xdf2SteckbriefImportFactory(id=id).save(runner)

    response = client.get(f"/api/v1/document-profiles?nummernkreis={term}")

    assert response.status_code == 200
    assert response.json()["count"] == 0


@pytest.mark.skip("Test as soon as we have xdf3 support for 'Steckbrief'")
def test_should_filter_for_stichwort(runner: CommandRunner, client: TestClient):
    pass


def test_should_filter_for_bezug(runner: CommandRunner, client: TestClient):
    Xdf2SteckbriefImportFactory(bezug="TestBezug").save(runner)

    response = client.get("/api/v1/document-profiles?bezug=testbezug")

    assert response.status_code == 200
    assert response.json()["count"] == 1


def test_should_fail_filter_for_invalid_bezug(
    runner: CommandRunner, client: TestClient
):
    Xdf2SteckbriefImportFactory(bezug="TestBezug").save(runner)

    response = client.get("/api/v1/document-profiles?bezug=wrongbezug")

    assert response.status_code == 200
    assert response.json()["count"] == 0


def test_should_filter_by_dokumentart(runner: CommandRunner, client: TestClient):
    Xdf2SteckbriefImportFactory(id="D123", dokumentart=xdf3.Dokumentart.ANTRAG).save(
        runner
    )
    Xdf2SteckbriefImportFactory(id="D234", dokumentart=xdf3.Dokumentart.ANZEIGE).save(
        runner
    )

    response = client.get(
        f"/api/v1/document-profiles?dokumentart={xdf3.Dokumentart.ANTRAG.value}"
    )

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "D123"


def test_should_filter_for_freigabe_status(runner: CommandRunner, client: TestClient):
    steckbrief = Xdf2SteckbriefImportFactory(
        freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
    ).save(runner)

    Xdf2SteckbriefImportFactory(
        freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER
    ).save(runner)

    response = client.get(
        f"/api/v1/document-profiles?freigabe_status={xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value}"
    )

    assert response.status_code == 200
    results: list[Any] = response.json()["items"]
    assert len(results) == 1
    assert results[0]["fim_id"] == steckbrief.fim_id


def test_should_combine_filters(runner: CommandRunner, client: TestClient):
    steckbrief = Xdf2SteckbriefImportFactory(
        name="Personalausweis",
        freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
    ).save(runner)

    Xdf2SteckbriefImportFactory(
        name="Umzug", freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
    ).save(runner)

    response = client.get(
        f"/api/v1/document-profiles?name=Perso&freigabe_status={xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value}"
    )

    assert response.status_code == 200
    results: list[Any] = response.json()["items"]
    assert len(results) == 1
    assert results[0]["fim_id"] == steckbrief.fim_id


def test_should_fail_for_invalid_freigabe_status(client: TestClient):
    response = client.get("/api/v1/document-profiles?freigabe_status=invalid")

    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "ctx": {
                    "expected": "1, 2, 3, 4, 5, 6, 7 or 8",
                },
                "input": "invalid",
                "loc": ["query", "freigabe_status", 0],
                "msg": "Input should be 1, 2, 3, 4, 5, 6, 7 or 8",
                "type": "enum",
            }
        ]
    }


@pytest.mark.parametrize("query_parameter", ["Bundesredaktion", "Bundes", "bund"])
def test_should_filter_for_status_gesetzt_durch(
    runner: CommandRunner, client: TestClient, query_parameter: str
):
    steckbrief = Xdf2SteckbriefImportFactory(
        fachlicher_ersteller="Bundesredaktion"
    ).save(runner)
    Xdf2SteckbriefImportFactory(fachlicher_ersteller="Landesredaktion").save(runner)

    response = client.get(
        f"/api/v1/document-profiles?status_gesetzt_durch={query_parameter}"
    )

    assert response.status_code == 200
    results = response.json()["items"]
    assert len(results) == 1
    assert results[0]["fim_id"] == steckbrief.fim_id


@pytest.mark.parametrize(
    "fts",
    [
        ("ausbildung", ["Ausbildungsförderung"]),
        ("Antrag", ["Antrag"]),
        (
            "eine beschreibung",
            ["Beschreibung"],
        ),  # multiple searchterms are separated and connected with an and-relation
        ("D00000", ["D00000130"]),
    ],
)
def test_should_filter_by_fts_query(
    runner: CommandRunner, client: TestClient, fts: tuple[str, list[str]]
):
    fts_query, fts_matches = fts
    with open(XDF2_DATA / "steckbrief.xdf2.xml", "rb") as steckbrief_file:
        steckbrief = Xdf2SteckbriefImport.from_bytes(
            steckbrief_file.read(),
            freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        )
        runner.import_xdf2_steckbrief(steckbrief)

    response = client.get(f"/api/v1/document-profiles?fts_query={fts_query}")

    assert response.status_code == 200
    assert response.json()["count"] == 1

    assert response.json()["items"][0]["fim_id"] == "D00000130"
    for match in fts_matches:
        assert f"[[[{match}]]]" in response.json()["items"][0]["fts_match"]


def test_should_filter_by_fts_query_handlungsgrundlage(
    runner: CommandRunner, client: TestClient
):
    steckbrief = Xdf2SteckbriefImportFactory(bezug="Match").save(runner)
    Xdf2SteckbriefImportFactory(bezug="random").save(runner)

    response = client.get(
        "/api/v1/document-profiles?fts_query=match&suche_nur_in=Rechtsgrundlagen"
    )

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["fim_id"] == steckbrief.fim_id


def test_should_filter_by_fts_query_status_gestetzt_durch(
    runner: CommandRunner, client: TestClient
):
    steckbrief = Xdf2SteckbriefImportFactory(fachlicher_ersteller="Match").save(runner)
    Xdf2SteckbriefImportFactory(fachlicher_ersteller="random").save(runner)

    response = client.get(
        "/api/v1/document-profiles?fts_query=match&suche_nur_in=Status_gesetzt_durch"
    )

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["fim_id"] == steckbrief.fim_id


def test_should_filter_by_fts_query_versionshinweis(
    runner: CommandRunner, client: TestClient
):
    steckbrief = Xdf2SteckbriefImportFactory(versionshinweis="Match").save(runner)
    Xdf2SteckbriefImportFactory(versionshinweis="random").save(runner)

    response = client.get(
        "/api/v1/document-profiles?fts_query=match&suche_nur_in=Versionshinweis"
    )

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["fim_id"] == steckbrief.fim_id


def test_should_combine_fts_query_with_other_filters(
    runner: CommandRunner, client: TestClient
):
    Xdf2SteckbriefImportFactory(
        name="TestName", freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
    ).save(runner)

    response = client.get(
        f"/api/v1/document-profiles?fts_query=content&freigabe_status={xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER.value}"
    )

    assert response.status_code == 200
    assert response.json()["count"] == 0


@pytest.mark.parametrize("query_parameter", ["BEzeichnung", "zeich", "bezeichnung"])
def test_should_get_steckbrief_by_partial_bezeichnung(
    runner: CommandRunner, client: TestClient, query_parameter: str
):
    Xdf2SteckbriefImportFactory(bezeichnung_eingabe="bezeichnung").save(runner)

    response = client.get(f"/api/v1/document-profiles?bezeichnung={query_parameter}")

    assert response.status_code == 200
    assert response.json()["count"] == 1


@pytest.mark.parametrize(
    "query_parameter", ["recht", "Rechtsbezug", "bezug", "BEZUG", "echt"]
)
def test_should_get_steckbrief_by_bezug(
    runner: CommandRunner, client: TestClient, query_parameter: str
):
    Xdf2SteckbriefImportFactory(bezug="Rechtsbezug").save(runner)

    response = client.get(f"/api/v1/document-profiles?bezug={query_parameter}")
    assert response.status_code == 200
    assert response.json()["count"] == 1


@pytest.mark.parametrize(
    "query_parameter",
    ["Freigabe", "freigabe", "Freigabe durch", "durch", "XUnternehmen"],
)
def test_should_get_steckbrief_by_versionshinweis(
    runner: CommandRunner, client: TestClient, query_parameter: str
):
    Xdf2SteckbriefImportFactory(versionshinweis="Freigabe durch XUnternehmen").save(
        runner
    )

    response = client.get(
        f"/api/v1/document-profiles?versionshinweis={query_parameter}"
    )
    assert response.status_code == 200
    assert response.json()["count"] == 1


def test_should_filter_by_group_number(runner: CommandRunner, client: TestClient):
    Xdf2SteckbriefImportFactory(id="D12345").save(runner)

    response = client.get("/api/v1/document-profiles?nummernkreis=12")

    assert response.status_code == 200
    assert response.json()["count"] == 1


def test_should_fail_by_wrong_group_number(runner: CommandRunner, client: TestClient):
    wrong_group_number = "wrong_group_number"

    Xdf2SteckbriefImportFactory().save(runner)

    response = client.get(
        f"/api/v1/document-profiles?nummernkreis={wrong_group_number}"
    )

    assert response.status_code == 200
    assert response.json()["count"] == 0


def test_should_return_paginated_result(client: TestClient):
    response = client.get("/api/v1/document-profiles")

    assert response.status_code == 200

    assert response.json() == {
        "items": [],
        "offset": 0,
        "limit": 200,
        "count": 0,
        "total_count": 0,
    }


def test_should_filter_for_xdf_version_xdf3(runner: CommandRunner, client: TestClient):
    Xdf2SteckbriefImportFactory().save(runner)

    response = client.get("/api/v1/document-profiles?xdf_version=3.0.0")

    assert response.status_code == 200
    results: list[Any] = response.json()["items"]
    assert len(results) == 0


def test_should_filter_for_xdf_version_xdf2(runner: CommandRunner, client: TestClient):
    Xdf2SteckbriefImportFactory().save(runner)

    response = client.get("/api/v1/document-profiles?xdf_version=2.0")

    assert response.status_code == 200
    results: list[Any] = response.json()["items"]
    assert len(results) == 1


def test_should_not_filter_for_invalid_xdf_version(
    runner: CommandRunner, client: TestClient
):
    Xdf2SteckbriefImportFactory().save(runner)

    response = client.get("/api/v1/document-profiles?xdf_version=2.0.0")

    assert response.status_code == 422


@pytest.mark.parametrize(
    "fts_query",
    ['\\"\\"', 'Hello "World\\"'],
)
def test_should_search_with_special_characters(
    runner: CommandRunner, client: TestClient, fts_query: str
):
    """
    We used to have a bug where the search term "\\ did result in a status code 500.

    We had forgotten to escape the backslash which resulted in invalid
    to_tsquery input.
    """
    response = client.get(f"/api/v1/document-profiles?fts_query={fts_query}")

    assert response.status_code == 200
