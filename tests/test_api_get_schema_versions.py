from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.factories.xdf2 import Xdf2Factory


def test_should_return_all_versions_of_a_schema(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    factory.schema(id="S12345", version="2.0").save(runner)
    factory.schema(id="S12345", version="1.0").save(runner)

    response = client.get("/api/v1/schemas/S12345")

    assert response.status_code == 200

    result = response.json()
    assert len(result) == 2
    assert result[0]["fim_version"] == "1.0"
    assert result[1]["fim_version"] == "2.0"


def test_should_return_empty_list_for_unknown_schema(client: TestClient):
    response = client.get("/api/v1/schemas/S12345")

    assert response.status_code == 200
    assert response.json() == []
