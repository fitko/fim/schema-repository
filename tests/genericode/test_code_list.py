import pytest

from fimportal.genericode import (
    ParserException,
    GenericodeException,
    load_code_list,
    parse_code_list,
    serialize_code_list,
    Identifier,
    CodeList,
)
from tests.data import GENERICODE_DATA


class TestParser:
    def test_should_correctly_parse_a_code_list(self):
        code_list = load_code_list(GENERICODE_DATA / "anschrift.xml")

        assert code_list == CodeList(
            identifier=Identifier(
                canonical_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige",
                version="2018-01-27",
                canonical_version_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige_2018-01-27",
            ),
            default_code_key="code",
            default_name_key="name",
            columns={
                "code": ["001", "002", "003"],
                "name": [
                    "Anschrift des Antragstellers",
                    "Anschrift des gesetzlichen Vertreters",
                    "Andere, abweichende Anschrift",
                ],
            },
        )

    def test_should_correctly_parse_a_code_list_with_shuffled_nodes(self):
        code_list = load_code_list(GENERICODE_DATA / "anschrift_shuffled_nodes.xml")

        assert code_list == CodeList(
            identifier=Identifier(
                canonical_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige",
                version="2018-01-27",
                canonical_version_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige_2018-01-27",
            ),
            default_code_key="code",
            default_name_key="name",
            columns={
                "code": ["001", "002", "003"],
                "name": [
                    "Anschrift des Antragstellers",
                    "Anschrift des gesetzlichen Vertreters",
                    "Andere, abweichende Anschrift",
                ],
            },
        )

    def test_should_ignore_empty_rows(self):
        code_list = load_code_list(GENERICODE_DATA / "anschrift_empty_row.xml")

        assert code_list == CodeList(
            identifier=Identifier(
                canonical_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige",
                version="2018-01-27",
                canonical_version_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige_2018-01-27",
            ),
            default_code_key="code",
            default_name_key="name",
            columns={
                "code": ["001", "002", "003"],
                "name": [
                    "Anschrift des Antragstellers",
                    "Anschrift des gesetzlichen Vertreters",
                    "Andere, abweichende Anschrift",
                ],
            },
        )

    def test_should_use_default_key_refs_for_code(self):
        code_list = load_code_list(GENERICODE_DATA / "anschrift_empty_key_refs.xml")

        assert code_list == CodeList(
            identifier=Identifier(
                canonical_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige",
                version="2018-01-27",
                canonical_version_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige_2018-01-27",
            ),
            default_code_key="Code",
            default_name_key=None,
            columns={
                "Code": ["001", "002", "003"],
                "Codename": [
                    "Anschrift des Antragstellers",
                    "Anschrift des gesetzlichen Vertreters",
                    "Andere, abweichende Anschrift",
                ],
            },
        )

    def test_should_fall_back_to_first_available_key_column(self):
        code_list = load_code_list(GENERICODE_DATA / "anschrift_unknown_key_column.xml")

        assert code_list == CodeList(
            identifier=Identifier(
                canonical_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige",
                version="2018-01-27",
                canonical_version_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige_2018-01-27",
            ),
            default_code_key="code",
            default_name_key=None,
            columns={
                "code": ["001", "002", "003"],
                "name": [
                    "Anschrift des Antragstellers",
                    "Anschrift des gesetzlichen Vertreters",
                    "Andere, abweichende Anschrift",
                ],
            },
        )

    def test_should_fill_missing_row_values(self):
        code_list = load_code_list(GENERICODE_DATA / "anschrift_missing_row_value.xml")

        assert code_list == CodeList(
            identifier=Identifier(
                canonical_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige",
                version="2018-01-27",
                canonical_version_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige_2018-01-27",
            ),
            default_code_key="code",
            default_name_key="name",
            columns={
                "code": [None, "002", "003"],
                "name": [
                    "Anschrift des Antragstellers",
                    "Anschrift des gesetzlichen Vertreters",
                    "Andere, abweichende Anschrift",
                ],
            },
        )

    def test_should_fail_for_duplicate_column_id(self):
        with pytest.raises(
            ParserException,
            match="Could not parse code list: Duplicate column id 'code'",
        ):
            load_code_list(GENERICODE_DATA / "anschrift_duplicate_column_id.xml")

    def test_should_fail_for_invalid_xml(self):
        with pytest.raises(
            ParserException,
            match="Could not parse code list: Invalid xml document",
        ):
            load_code_list(GENERICODE_DATA / "invalid_xml.xml")

    def test_should_work_without_internal_namespaces(self):
        load_code_list(GENERICODE_DATA / "anschrift_without_xml_namespaces.xml")


class TestCodeList:
    def test_should_return_the_default_key_column(self):
        code_list = CodeList(
            identifier=Identifier(
                canonical_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige",
                version="2018-01-27",
                canonical_version_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige_2018-01-27",
            ),
            default_code_key="code",
            default_name_key="name",
            columns={
                "code": ["001", "002", "003"],
                "name": ["A", "B", "C"],
            },
        )

        assert code_list.get_key_column() == ["001", "002", "003"]

    def test_should_fail_to_return_key_column_with_empty_value(self):
        code_list = CodeList(
            identifier=Identifier(
                canonical_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige",
                version="2018-01-27",
                canonical_version_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige_2018-01-27",
            ),
            default_code_key="code",
            default_name_key=None,
            columns={
                "code": [None, "002"],
            },
        )

        with pytest.raises(
            GenericodeException, match="Missing value in key column 'code'"
        ):
            code_list.get_key_column()

    def test_should_fail_to_return_key_column_with_duplicate_value(self):
        code_list = CodeList(
            identifier=Identifier(
                canonical_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige",
                version="2018-01-27",
                canonical_version_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige_2018-01-27",
            ),
            default_code_key="code",
            default_name_key="name",
            columns={
                "code": ["001", "001"],
                "name": ["A", "B"],
            },
        )

        with pytest.raises(
            GenericodeException, match="Duplicate value '001' in key column"
        ):
            code_list.get_key_column()

    def test_should_fail_for_unknown_key_column(self):
        code_list = CodeList(
            identifier=Identifier(
                canonical_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige",
                version="2018-01-27",
                canonical_version_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige_2018-01-27",
            ),
            default_code_key="code",
            default_name_key="name",
            columns={
                "code": ["001", "002", "003"],
                "name": ["Label 1", "Label 2", "Label 3"],
            },
        )

        with pytest.raises(GenericodeException, match="Unknown column 'invalid'"):
            code_list.get_key_column("invalid")


class TestSelectNameColumn:
    def test_should_use_the_default_name_key_by_default(self):
        code_list = CodeList(
            identifier=Identifier(
                canonical_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige",
                version="2018-01-27",
                canonical_version_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige_2018-01-27",
            ),
            default_code_key="code",
            default_name_key="name_column",
            columns={
                "code": ["001", "002", "003"],
                "name_column": ["A", "B", "C"],
            },
        )

        assert code_list.select_name_column() == ["A", "B", "C"]

    def test_should_return_name_column_with_duplicate_values(self):
        code_list = CodeList(
            identifier=Identifier(
                canonical_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige",
                version="2018-01-27",
                canonical_version_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige_2018-01-27",
            ),
            default_code_key="code",
            default_name_key=None,
            columns={
                "code": ["001", "002", "003"],
                "name": ["A", "B", "B"],
            },
        )

        assert code_list.select_name_column() == ["A", "B", "B"]

    def test_should_probe_for_Codename(self):
        code_list = CodeList(
            identifier=Identifier(
                canonical_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige",
                version="2018-01-27",
                canonical_version_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige_2018-01-27",
            ),
            default_code_key="code",
            default_name_key=None,
            columns={
                "code": ["001", "002", "003"],
                "Codename": ["A", "B", "C"],
            },
        )

        assert code_list.select_name_column() == ["A", "B", "C"]

    def test_should_probe_for_name(self):
        code_list = CodeList(
            identifier=Identifier(
                canonical_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige",
                version="2018-01-27",
                canonical_version_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige_2018-01-27",
            ),
            default_code_key="code",
            default_name_key=None,
            columns={
                "code": ["001", "002", "003"],
                "name": ["A", "B", "C"],
            },
        )

        assert code_list.select_name_column() == ["A", "B", "C"]

    def test_should_fail_for_missing_name_column(self):
        code_list = CodeList(
            identifier=Identifier(
                canonical_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige",
                version="2018-01-27",
                canonical_version_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige_2018-01-27",
            ),
            default_code_key="code",
            default_name_key=None,
            columns={
                "code": ["001", "002", "003"],
            },
        )

        with pytest.raises(
            GenericodeException,
            match="Cannot select name column for code list urn:de:fim:codeliste:anschriftantragstellervertretersonstige_2018-01-27",
        ):
            assert code_list.select_name_column()


class TestSerialize:
    def test_should_correctly_serialize_a_code_list(self):
        code_list = CodeList(
            identifier=Identifier(
                canonical_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige",
                version="2018-01-27",
                canonical_version_uri="urn:de:fim:codeliste:anschriftantragstellervertretersonstige_2018-01-27",
            ),
            default_code_key="code",
            default_name_key="name",
            columns={
                "code": ["001", "002", "003"],
                "name": ["Label A", "Label B", "Label C"],
            },
        )

        data = serialize_code_list(code_list)

        assert parse_code_list(data) == code_list
