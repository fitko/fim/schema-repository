from typing import AsyncContextManager, Callable

import pytest
from asyncpg import Pool

from fimportal import xzufi
from fimportal.cli.commands.import_leistungen import sync_xzufi_leistungen
from fimportal.common import FreigabeStatus
from fimportal.models.xzufi import Link, XzufiSource
from fimportal.service import Service
from fimportal.xzufi.client import TEST_LEISTUNG_XML
from fimportal.xzufi.common import XZUFI_REDAKTION_ID, LeistungsTypisierung
from fimportal.xzufi.leistung import ModulTextTyp
from tests.factories.xzufi import LeistungFactory, TextmodulFactory


@pytest.mark.asyncio(loop_scope="session")
async def test_should_add_new_leistungen(
    get_service: Callable[[], AsyncContextManager[Service]],
    async_db_pool: Pool,
):
    leistungsschluessel = "99123000000000"
    links = [
        "a link",
        "another link",
        "https://leika-schul.zfinder.de",
        "urheber_link.com",
    ]
    leistung = (
        LeistungFactory(
            redaktion_id=XZUFI_REDAKTION_ID,
            freigabestatus_bibliothek=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
            bezeichnung=TextmodulFactory(modul_typ=ModulTextTyp.LEISTUNGSBEZEICHNUNG)
            .with_link(links[0])
            .build(),
            rechtsgrundlagen=TextmodulFactory(modul_typ=ModulTextTyp.RECHTSGRUNDLAGEN)
            .with_link(links[1])
            .build(),
            volltext=TextmodulFactory(modul_typ=ModulTextTyp.VOLLTEXT)
            .with_link(links[2])
            .build(),
            urheber=TextmodulFactory(modul_typ=ModulTextTyp.URHEBER)
            .with_link(links[3])
            .build(),
        )
        .add_leistungsschluessel(leistungsschluessel)
        .add_typisierung(LeistungsTypisierung.TYP_2)
        .build()
    )
    xzufi_client = xzufi.FakeClient(XZUFI_REDAKTION_ID)
    xzufi_client.add_leistung(leistung)

    await sync_xzufi_leistungen(
        xzufi_client=xzufi_client,
        pool=async_db_pool,
        immutable_base_url="localhost",
    )

    async with get_service() as service:
        identifier = leistung.get_identifier()
        assert identifier is not None

        leistung_out = await service.get_leistungssteckbrief(leistungsschluessel)
        assert leistung_out is not None

        links_leistung = await service.get_links_for_leistung(identifier)
        assert links_leistung == [
            Link("a link", None, None),
            Link("another link", None, None),
        ]


@pytest.mark.asyncio(loop_scope="session")
async def test_should_update_existing_leistungen(
    get_service: Callable[[], AsyncContextManager[Service]],
    async_db_pool: Pool,
):
    factory = LeistungFactory.steckbrief().with_bezeichnung("pre")
    leistung = factory.build()
    async with get_service() as service:
        await service.import_leistung_batch(
            [(leistung, TEST_LEISTUNG_XML)],
            source=XzufiSource.XZUFI,
        )

    async with get_service() as service:
        leistung_out = await service.get_leistungssteckbrief(
            leistung.get_leistungsschluessel()[0]
        )
        assert leistung_out is not None
        assert leistung_out.title == "pre"

    leistung_post = factory.with_bezeichnung("post").build()
    xzufi_client = xzufi.FakeClient(XZUFI_REDAKTION_ID)
    xzufi_client.add_leistung(leistung_post)

    await sync_xzufi_leistungen(
        xzufi_client=xzufi_client,
        immutable_base_url="localhost",
        pool=async_db_pool,
    )

    async with get_service() as service:
        leistung_out = await service.get_leistungssteckbrief(
            leistung.get_leistungsschluessel()[0]
        )
        assert leistung_out is not None
        assert leistung_out.title == "post"


@pytest.mark.asyncio(loop_scope="session")
async def test_should_delete_removed_leistungen_and_links(
    get_service: Callable[[], AsyncContextManager[Service]],
    async_db_pool: Pool,
):
    uri = "some-link.com"
    leistung = LeistungFactory(
        redaktion_id=XZUFI_REDAKTION_ID,
        bezeichnung=TextmodulFactory(modul_typ=ModulTextTyp.LEISTUNGSBEZEICHNUNG)
        .with_link(uri)
        .build(),
    ).build()
    async with get_service() as service:
        await service.import_leistung_batch(
            [(leistung, TEST_LEISTUNG_XML)],
            source=XzufiSource.XZUFI,
        )

    await sync_xzufi_leistungen(
        xzufi_client=xzufi.FakeClient(XZUFI_REDAKTION_ID),
        immutable_base_url="localhost",
        pool=async_db_pool,
    )

    async with get_service() as service:
        identifier = leistung.get_identifier()
        assert identifier is not None

        leistung_out = await service.get_leistungsbeschreibung(identifier)
        assert leistung_out is None

        link = await service.get_link(uri)
        assert link is None


@pytest.mark.asyncio(loop_scope="session")
async def test_should_not_delete_links_that_are_in_use(
    get_service: Callable[[], AsyncContextManager[Service]],
    async_db_pool: Pool,
):
    uri = "some-link.com"
    leistung_A = (
        LeistungFactory.steckbrief()
        .with_bezeichnung(
            TextmodulFactory(modul_typ=ModulTextTyp.LEISTUNGSBEZEICHNUNG)
            .with_link(uri)
            .build()
        )
        .build()
    )
    leistung_B = (
        LeistungFactory.steckbrief()
        .with_bezeichnung(
            bezeichnung=TextmodulFactory(modul_typ=ModulTextTyp.LEISTUNGSBEZEICHNUNG)
            .with_link(uri)
            .build(),
        )
        .build()
    )

    async with get_service() as service:
        await service.import_leistung_batch(
            [(leistung_A, TEST_LEISTUNG_XML), (leistung_B, TEST_LEISTUNG_XML)],
            source=XzufiSource.XZUFI,
        )

    xzufi_client = xzufi.FakeClient(XZUFI_REDAKTION_ID)
    xzufi_client.add_leistung(leistung_A)

    await sync_xzufi_leistungen(
        xzufi_client=xzufi_client,
        immutable_base_url="localhost",
        pool=async_db_pool,
    )

    async with get_service() as service:
        leistung_out = await service.get_leistungssteckbrief(
            leistung_A.get_leistungsschluessel()[0]
        )
        assert leistung_out is not None

        leistung_out = await service.get_leistungssteckbrief(
            leistung_B.get_leistungsschluessel()[0]
        )
        assert leistung_out is None

        link = await service.get_link(uri)
        assert link is not None
