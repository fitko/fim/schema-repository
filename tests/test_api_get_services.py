from datetime import timedelta
import pytest
from fastapi.testclient import TestClient

from fimportal.common import FreigabeStatus, RawCode
from fimportal.helpers import format_iso8601, utc_now
from fimportal.xzufi.common import (
    GERMAN,
    HyperlinkErweitert,
    Identifikator,
    LeistungRedaktionId,
    StringLocalized,
)
from fimportal.xzufi.leistung import (
    Leistungsstruktur,
    LeistungsstrukturObjekt,
    LeistungsstrukturObjektMitVerrichtung,
    LeistungsstrukturObjektMitVerrichtungUndDetail,
    LeistungsTyp,
    LeistungsTypisierung,
    ModulTextTyp,
    Textmodul,
    parse_leistung,
)
from tests.conftest import CommandRunner
from tests.data import XZUFI_2_2_0_DATA
from tests.factories.xzufi import LeistungFactory

ENDPOINT = "/api/v0/services"


def test_return_empty_list(client: TestClient):
    response = client.get(ENDPOINT)

    assert response.status_code == 200

    assert response.json() == {
        "items": [],
        "count": 0,
        "offset": 0,
        "limit": 200,
        "total_count": 0,
    }


def test_return_existing_leistungen(runner: CommandRunner, client: TestClient):
    now = utc_now()

    LeistungFactory(
        redaktion_id=LeistungRedaktionId.LEIKA.value,
        leistung_id="leistung_1",
        freigabestatus_katalog=FreigabeStatus.ENTWURF,
        freigabestatus_bibliothek=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        bezeichnung="Leistung 1",
        bezeichnung_2="LB2",
        kurztext="Kurztext",
        volltext="Volltext",
        rechtsgrundlagen="Rechtsgrundlagen",
    ).add_typisierung(LeistungsTypisierung.TYP_2).add_leistungsschluessel(
        "99123400000000"
    ).with_versionsinformation(
        erstellt_datum_zeit=now,
        geaendert_datum_zeit=now,
    ).add_klassifizierung(list_uri="list_uri", value="value").save_leika(runner)

    response = client.get(ENDPOINT)

    assert response.status_code == 200
    assert response.json() == {
        "items": [
            {
                "redaktion_id": "TSA_TELEPORT",
                "leistung_id": "leistung_1",
                "title": "Leistung 1",
                "leistungsbezeichnung": "Leistung 1",
                "leistungsbezeichnung_2": "LB2",
                "freigabestatus_katalog": FreigabeStatus.ENTWURF.value,
                "freigabestatus_bibliothek": FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value,
                "leistungsschluessel": "99123400000000",
                "typisierung": [LeistungsTypisierung.TYP_2.value],
                "leistungstyp": None,
                "sdg_informationsbereiche": [],
                "klassifizierung": [{"list_uri": "list_uri", "value": "value"}],
                "rechtsgrundlagen": "Rechtsgrundlagen",
                "leistungsbeschreibungen": [],
                "erstellt_datum_zeit": format_iso8601(now),
                "geaendert_datum_zeit": format_iso8601(now),
                "fts_match": None,
            }
        ],
        "count": 1,
        "offset": 0,
        "limit": 200,
        "total_count": 1,
    }


def test_return_redaktionen_with_leistungsbeschreibungen(
    runner: CommandRunner, client: TestClient
):
    LeistungFactory.steckbrief(leistungsschluessel="99123400000000").save_leika(runner)
    LeistungFactory(
        redaktion_id="Redaktion A", leistung_id="l1"
    ).add_leistungsschluessel("99123400000000").save_pvog(runner)
    LeistungFactory(redaktion_id="Redaktion B").save_pvog(runner)

    response = client.get(ENDPOINT)
    assert response.status_code == 200

    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["leistungsbeschreibungen"] == [["Redaktion A", "l1"]]


@pytest.mark.parametrize(
    "fts_query,fts_matches",
    [
        ("9915", ["99154033000000"]),
        ("Informationen", ["Informationen"]),
        ("vaccination", ["vaccination"]),
        (
            "allgemeine zugangsrechte",
            ["Allgemeine", "Zugangsrechte"],
        ),  # multiple searchterms are separated and connected with an and-relation
        ("https://leika-schul.zfinder", ["leika-schul.zfinder.de"]),
    ],
)
def test_get_leistung_by_fts_query(
    runner: CommandRunner, client: TestClient, fts_query: str, fts_matches: list[str]
):
    data = (XZUFI_2_2_0_DATA / "leistung_steckbrief.xml").read_bytes()

    leistung = parse_leistung(data)
    runner.import_leika(leistung, xml_content=data.decode("utf-8"))

    response = client.get(f"{ENDPOINT}?fts_query={fts_query}")
    assert response.status_code == 200

    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["leistung_id"] == leistung.id.value
    for match in fts_matches:
        assert match in result["items"][0]["fts_match"]


def test_leistung_by_fts_query_leistungsbezeichnung(
    runner: CommandRunner, client: TestClient
):
    leistung = LeistungFactory.steckbrief(bezeichnung="Match").save_leika(runner)
    LeistungFactory.steckbrief(bezeichnung="Random").save_leika(runner)

    response = client.get(
        f"{ENDPOINT}?fts_query=match&suche_nur_in=leistungsbezeichnung"
    )

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["leistung_id"] == leistung.leistung_id


def test_leistung_by_fts_query_leistungsbezeichnung_2(
    runner: CommandRunner, client: TestClient
):
    leistung = LeistungFactory.steckbrief(bezeichnung_2="Match").save_leika(runner)
    LeistungFactory.steckbrief(bezeichnung_2="Random").save_leika(runner)

    response = client.get(
        f"{ENDPOINT}?fts_query=match&suche_nur_in=leistungsbezeichnung_II"
    )

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["leistung_id"] == leistung.leistung_id


def test_leistung_by_fts_query_rechtsgrundlagen(
    runner: CommandRunner, client: TestClient
):
    leistung = LeistungFactory.steckbrief(rechtsgrundlagen="Match").save_leika(runner)
    LeistungFactory.steckbrief(rechtsgrundlagen="Random").save_leika(runner)

    response = client.get(f"{ENDPOINT}?fts_query=match&suche_nur_in=rechtsgrundlagen")

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["leistung_id"] == leistung.leistung_id


def test_get_leistung_by_typisierung(runner: CommandRunner, client: TestClient):
    leistung = LeistungFactory.steckbrief(
        typisierung=LeistungsTypisierung.TYP_2
    ).save_leika(runner)
    LeistungFactory.steckbrief(typisierung=LeistungsTypisierung.TYP_3).save_leika(
        runner
    )

    # First, make sure both leistungen are imported
    response = client.get(f"{ENDPOINT}")
    assert response.status_code == 200
    assert response.json()["count"] == 2

    response = client.get(f"{ENDPOINT}?typisierung={LeistungsTypisierung.TYP_2.value}")
    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["leistung_id"] == leistung.leistung_id


@pytest.mark.parametrize(
    "search_term",
    [
        ("991234"),
        ("1234"),
        ("40000"),
    ],
)
def test_get_leistung_by_leika(
    runner: CommandRunner, client: TestClient, search_term: str
):
    leistung = LeistungFactory.steckbrief(
        leistungsschluessel="99123400000000"
    ).save_leika(runner)
    LeistungFactory.steckbrief(leistungsschluessel="99123500000000").save_leika(runner)

    # First, make sure both leistungen are imported
    response = client.get(f"{ENDPOINT}")
    assert response.status_code == 200
    assert response.json()["count"] == 2

    response = client.get(f"{ENDPOINT}?leistungsschluessel={search_term}")
    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["leistung_id"] == leistung.leistung_id


def test_get_leistung_by_freigabestatus_katalog(
    runner: CommandRunner, client: TestClient
):
    leistung = (
        LeistungFactory.steckbrief()
        .with_freigabestatus_katalog(FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD)
        .save_leika(runner)
    )
    LeistungFactory.steckbrief().with_freigabestatus_katalog(
        FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER
    ).save_leika(runner)

    response = client.get(
        f"{ENDPOINT}?freigabe_status_katalog={FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value}"
    )
    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["leistung_id"] == leistung.leistung_id


def test_get_leistung_by_freigabestatus_bibliothek(
    runner: CommandRunner, client: TestClient
):
    leistung = LeistungFactory.stammtext(
        freigabestatus_bibliothek=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD
    ).save_leika(runner)
    LeistungFactory.stammtext(
        freigabestatus_bibliothek=FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER
    ).save_leika(runner)

    response = client.get(
        f"{ENDPOINT}?freigabe_status_bibliothek={FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value}"
    )
    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["leistung_id"] == leistung.leistung_id


@pytest.mark.parametrize(
    "rechtsgrundlagen",
    [
        ("rechts"),
        ("some"),
        ("GRUND"),
        ("e rec"),
        ("Title"),
        ("Beschreibung"),
    ],
)
def test_get_leistung_by_rechtsgrundlagen(
    runner: CommandRunner, client: TestClient, rechtsgrundlagen: str
):
    leistung = (
        LeistungFactory.steckbrief()
        .with_rechtsgrundlagen(
            Textmodul(
                position_darstellung=0,
                id=None,
                id_sekundaer=[],
                gueltigkeit=[],
                leika_textmodul=ModulTextTyp.RECHTSGRUNDLAGEN,
                leika_textmodul_abweichende_bezeichnung=[],
                inhalt=[StringLocalized(GERMAN, "some rechtsgrundlagen")],
                weiterfuehrender_link=[
                    HyperlinkErweitert(
                        position_darstellung=None,
                        language_code=None,
                        uri="test",
                        titel="Title",
                        beschreibung="Beschreibung",
                    )
                ],
            ),
        )
        .save_leika(runner)
    )

    LeistungFactory.steckbrief().with_rechtsgrundlagen("another one").save_leika(runner)

    # First, make sure both leistungen are imported
    response = client.get(f"{ENDPOINT}")
    assert response.status_code == 200
    assert response.json()["count"] == 2

    response = client.get(f"{ENDPOINT}?rechtsgrundlagen={rechtsgrundlagen}")
    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["leistung_id"] == leistung.leistung_id


@pytest.mark.parametrize(
    "title",
    [
        ("bezeichnung"),
        ("Leistung"),
    ],
)
def test_get_leistung_by_title(runner: CommandRunner, client: TestClient, title: str):
    leistung = (
        LeistungFactory.steckbrief()
        .with_bezeichnung("Leistungsbezeichnung")
        .save_leika(runner)
    )

    LeistungFactory.steckbrief().with_bezeichnung("another one").save_leika(runner)

    # First, make sure both leistungen are imported
    response = client.get(f"{ENDPOINT}")
    assert response.status_code == 200
    assert response.json()["count"] == 2

    response = client.get(f"{ENDPOINT}?title={title}")
    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["leistung_id"] == leistung.leistung_id


@pytest.mark.parametrize(
    "leistungsbezeichnung",
    [
        ("bezeichnung"),
        ("Leistung"),
        ("EISTUNG"),
    ],
)
def test_get_leistung_by_bezeichnung(
    runner: CommandRunner, client: TestClient, leistungsbezeichnung: str
):
    leistung = (
        LeistungFactory.steckbrief()
        .with_bezeichnung("Eine Leistungsbezeichnung")
        .save_leika(runner)
    )

    LeistungFactory.steckbrief().with_bezeichnung("another one").save_leika(runner)

    # First, make sure both leistungen are imported
    response = client.get(f"{ENDPOINT}")
    assert response.status_code == 200
    assert response.json()["count"] == 2

    response = client.get(f"{ENDPOINT}?leistungsbezeichnung={leistungsbezeichnung}")
    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["leistung_id"] == leistung.leistung_id


@pytest.mark.parametrize(
    "leistungsbezeichnung_II",
    [
        ("bezeichnung II"),
        ("Leistung"),
        ("EISTUNG"),
    ],
)
def test_get_leistung_by_leistungsbezeichnung_II(
    runner: CommandRunner, client: TestClient, leistungsbezeichnung_II: str
):
    leistung = (
        LeistungFactory.steckbrief()
        .with_leistungsbezeichnung_II("Leistungsbezeichnung II")
        .save_leika(runner)
    )

    LeistungFactory.steckbrief().with_leistungsbezeichnung_II("another one").save_leika(
        runner
    )

    # First, make sure both leistungen are imported
    response = client.get(f"{ENDPOINT}")
    assert response.status_code == 200
    assert response.json()["count"] == 2

    response = client.get(f"{ENDPOINT}?leistungsbezeichnung2={leistungsbezeichnung_II}")
    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["leistung_id"] == leistung.leistung_id


@pytest.mark.parametrize(
    "struktur,leistungstyp",
    [
        (LeistungsstrukturObjekt(None), LeistungsTyp.LEISTUNGS_OBJEKT),
        (
            LeistungsstrukturObjektMitVerrichtung(
                None,
                Identifikator(
                    scheme_agency_id=None,
                    scheme_agency_name=None,
                    scheme_data_uri=None,
                    scheme_id=None,
                    scheme_name=None,
                    scheme_uri=None,
                    scheme_version_id=None,
                    value="L1234",
                ),
                RawCode("", "", ""),
            ),
            LeistungsTyp.LEISTUNGS_OBJEKT_MIT_VERRICHTUNG,
        ),
        (
            LeistungsstrukturObjektMitVerrichtungUndDetail(
                None,
                Identifikator(
                    scheme_agency_id=None,
                    scheme_agency_name=None,
                    scheme_data_uri=None,
                    scheme_id=None,
                    scheme_name=None,
                    scheme_uri=None,
                    scheme_version_id=None,
                    value="L1234",
                ),
                RawCode("", "", ""),
                [StringLocalized("GERMAN", "SOME CONTENT")],
            ),
            LeistungsTyp.LEISTUNGS_OBJEKT_MIT_VERRICHTUNG_UND_DETAIL,
        ),
    ],
)
def test_get_leistung_by_leistungstyp(
    runner: CommandRunner,
    client: TestClient,
    struktur: Leistungsstruktur,
    leistungstyp: LeistungsTyp,
):
    leistung = LeistungFactory.steckbrief().with_struktur(struktur).save_leika(runner)

    LeistungFactory.steckbrief().with_struktur(None).save_leika(runner)

    # First, make sure both leistungen are imported
    response = client.get(f"{ENDPOINT}")
    assert response.status_code == 200
    assert response.json()["count"] == 2

    response = client.get(f"{ENDPOINT}?leistungstyp={leistungstyp.value}")

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["leistung_id"] == leistung.leistung_id


@pytest.mark.parametrize("einheitlicher_ansprechpartner", [(True, 0), (False, 1)])
def test_get_leistung_by_einheitlicher_ansprechpartner(
    runner: CommandRunner,
    client: TestClient,
    einheitlicher_ansprechpartner: tuple[bool | None, int],
):
    value, result_index = einheitlicher_ansprechpartner
    leistungen = [
        LeistungFactory.steckbrief()
        .with_einheitlicher_ansprechpartner(True)
        .save_leika(runner),
        LeistungFactory.steckbrief()
        .with_einheitlicher_ansprechpartner(False)
        .save_leika(runner),
        LeistungFactory.steckbrief()
        .with_einheitlicher_ansprechpartner(None)
        .save_leika(runner),
    ]

    response = client.get(f"{ENDPOINT}?einheitlicher_ansprechpartner={str(value)}")

    assert response.status_code == 200

    result = response.json()
    assert result["count"] == 1
    assert (
        result["items"][0]["leistungsschluessel"]
        == leistungen[result_index].leistungsschluessel
    )


def test_get_leistung_by_updated_since(runner: CommandRunner, client: TestClient):
    now = utc_now()

    leistung = (
        LeistungFactory.steckbrief()
        .with_versionsinformation(geaendert_datum_zeit=utc_now())
        .save_leika(runner)
    )

    LeistungFactory.steckbrief().with_versionsinformation(
        geaendert_datum_zeit=now - timedelta(days=1)
    ).save_leika(runner)

    LeistungFactory.steckbrief().with_versionsinformation(
        geaendert_datum_zeit=None
    ).save_leika(runner)

    response = client.get(f"{ENDPOINT}?updated_since={format_iso8601(now)}")

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["leistung_id"] == leistung.leistung_id


def test_order_results_by_erstellt_datum_zeit(
    runner: CommandRunner, client: TestClient
):
    now = utc_now()

    leistung_first = (
        LeistungFactory.steckbrief()
        .with_versionsinformation(erstellt_datum_zeit=now - timedelta(days=1))
        .save_leika(runner)
    )
    leistung_second = (
        LeistungFactory.steckbrief()
        .with_versionsinformation(erstellt_datum_zeit=now)
        .save_leika(runner)
    )
    leistung_unknown = (
        LeistungFactory.steckbrief()
        .with_versionsinformation(erstellt_datum_zeit=None)
        .save_leika(runner)
    )

    # ascending order
    response = client.get(f"{ENDPOINT}?order_by=erstellt_datum_zeit_asc")
    assert response.status_code == 200
    results = response.json()
    assert results["items"][0]["leistung_id"] == leistung_first.leistung_id
    assert results["items"][1]["leistung_id"] == leistung_second.leistung_id
    assert results["items"][2]["leistung_id"] == leistung_unknown.leistung_id

    # descending order
    response = client.get(f"{ENDPOINT}?order_by=erstellt_datum_zeit_desc")
    assert response.status_code == 200
    results = response.json()
    assert results["items"][0]["leistung_id"] == leistung_second.leistung_id
    assert results["items"][1]["leistung_id"] == leistung_first.leistung_id
    assert results["items"][2]["leistung_id"] == leistung_unknown.leistung_id


def test_order_results_by_aktualisiert_datum_zeit(
    runner: CommandRunner, client: TestClient
):
    now = utc_now()

    leistung_second = (
        LeistungFactory.steckbrief()
        .with_versionsinformation(geaendert_datum_zeit=now)
        .save_leika(runner)
    )
    leistung_first = (
        LeistungFactory.steckbrief()
        .with_versionsinformation(geaendert_datum_zeit=now - timedelta(days=1))
        .save_leika(runner)
    )
    leistung_unknown = (
        LeistungFactory.steckbrief()
        .with_versionsinformation(geaendert_datum_zeit=None)
        .save_leika(runner)
    )

    # ascending order
    response = client.get(f"{ENDPOINT}?order_by=geaendert_datum_zeit_asc")
    assert response.status_code == 200
    results = response.json()
    assert results["items"][0]["leistung_id"] == leistung_first.leistung_id
    assert results["items"][1]["leistung_id"] == leistung_second.leistung_id
    assert results["items"][2]["leistung_id"] == leistung_unknown.leistung_id

    # descending order
    response = client.get(f"{ENDPOINT}?order_by=geaendert_datum_zeit_desc")
    assert response.status_code == 200
    results = response.json()
    assert results["items"][0]["leistung_id"] == leistung_second.leistung_id
    assert results["items"][1]["leistung_id"] == leistung_first.leistung_id
    assert results["items"][2]["leistung_id"] == leistung_unknown.leistung_id


def test_order_results_alphabetically(runner: CommandRunner, client: TestClient):
    leistung_second = LeistungFactory.steckbrief(bezeichnung="Second").save_leika(
        runner
    )
    leistung_first = LeistungFactory.steckbrief(bezeichnung="First").save_leika(runner)

    response = client.get(f"{ENDPOINT}?order_by=titel_asc")
    assert response.status_code == 200
    results = response.json()
    assert results["items"][0]["leistung_id"] == leistung_first.leistung_id
    assert results["items"][1]["leistung_id"] == leistung_second.leistung_id


def test_order_results_by_leistungsschluessel(
    runner: CommandRunner, client: TestClient
):
    leistung_second = LeistungFactory.steckbrief(
        leistungsschluessel="99001000000002"
    ).save_leika(runner)
    leistung_first = LeistungFactory.steckbrief(
        leistungsschluessel="99001000000001"
    ).save_leika(runner)

    response = client.get(f"{ENDPOINT}?order_by=leistungsschluessel_asc")
    assert response.status_code == 200
    results = response.json()
    assert results["items"][0]["leistung_id"] == leistung_first.leistung_id
    assert results["items"][1]["leistung_id"] == leistung_second.leistung_id


def test_order_results_by_relevance(runner: CommandRunner, client: TestClient):
    leistung_second = LeistungFactory.steckbrief(
        bezeichnung="A", bezeichnung_2="Personalausweis"
    ).save_leika(runner)
    leistung_first = LeistungFactory.steckbrief(
        bezeichnung="Personalausweis"
    ).save_leika(runner)
    leistung_third = LeistungFactory.steckbrief(
        bezeichnung="B", bezeichnung_2="Personalausweis"
    ).save_leika(runner)

    response = client.get(f"{ENDPOINT}?order_by=relevance&fts_query=personalausweis")
    assert response.status_code == 200
    results = response.json()
    assert results["items"][0]["leistung_id"] == leistung_first.leistung_id
    assert results["items"][1]["leistung_id"] == leistung_second.leistung_id
    assert results["items"][2]["leistung_id"] == leistung_third.leistung_id
