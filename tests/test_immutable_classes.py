from datetime import date, datetime

import pytest

from fimportal import din91379
from fimportal.xdatenfelder import xdf2, xdf3
from tests.data import XDF2_DATA, XDF3_DATA
from tests.factories.xdf2 import Xdf2Factory
from tests.factories.xdf3 import (
    Xdf3Factory,
)


@pytest.mark.parametrize(
    "filename",
    ["S1234V1.0.xml"],
)
def test_should_create_immutable_schema_from_xdf2_schema_file(filename: str):
    with open(XDF2_DATA / filename, "rb") as file:
        schema_xml = file.read()

    schema_message = xdf2.parse_schema_message(schema_xml)
    immutable_schema = xdf2.ImmutableSchema.from_schema(schema_message.schema)
    assert immutable_schema


@pytest.mark.parametrize(
    "filename",
    ["hundesteuer.xdf3.xml"],
)
def test_should_create_immutable_schema_from_xdf3_schema_file(filename: str):
    with open(XDF3_DATA / filename, "rb") as file:
        schema_xml = file.read()

    schema_message = xdf3.parse_schema_message(schema_xml)
    immutable_schema = xdf3.ImmutableSchema.from_schema(schema_message.schema)
    assert immutable_schema


@pytest.mark.parametrize(
    "schema",
    [
        "S1234V1.0.xml",
        Xdf2Factory().schema().build(),
        Xdf2Factory().schema().full().build(),
    ],
)
def test_should_correctly_serialize_and_deserialize_an_immutable_xdf2_schema(
    schema: str | xdf2.Schema,
):
    if isinstance(schema, str):
        with open(XDF2_DATA / schema, "rb") as file:
            schema_xml = file.read()

        schema_message = xdf2.parse_schema_message(schema_xml)
        immutable_schema = xdf2.ImmutableSchema.from_schema(
            schema_message.schema,
        )
    else:
        immutable_schema = xdf2.ImmutableSchema.from_schema(schema)

    json_representation = immutable_schema.model_dump_json()
    instance_from_json = xdf2.ImmutableSchema.model_validate_json(json_representation)

    assert immutable_schema == instance_from_json


@pytest.mark.parametrize(
    "schema",
    [
        "hundesteuer.xdf3.xml",
        Xdf3Factory().schema().build(),
        Xdf3Factory().schema().full().build(),
    ],
)
def test_should_correctly_serialize_and_deserialize_an_immutable_xdf3_schema(
    schema: str | xdf3.Schema,
):
    if isinstance(schema, str):
        with open(XDF3_DATA / schema, "rb") as file:
            schema_xml = file.read()

        schema_message = xdf3.parse_schema_message(schema_xml)
        immutable_schema = xdf3.ImmutableSchema.from_schema(schema_message.schema)
    else:
        immutable_schema = xdf3.ImmutableSchema.from_schema(schema)

    json_representation = immutable_schema.model_dump_json()
    instance_from_json = xdf3.ImmutableSchema.model_validate_json(json_representation)

    assert immutable_schema == instance_from_json


@pytest.mark.parametrize(
    "schema_i, schema_ii",
    [
        (
            xdf2.Schema(
                identifier=xdf2.Identifier("S01", xdf2.Version("1.0")),
                name="Schema",
                bezeichnung_eingabe=None,
                bezeichnung_ausgabe=None,
                beschreibung=None,
                definition=None,
                bezug=None,
                status=xdf2.Status.AKTIV,
                versionshinweis=None,
                gueltig_ab=None,
                gueltig_bis=None,
                fachlicher_ersteller=None,
                freigabedatum=None,
                veroeffentlichungsdatum=None,
                hilfetext=None,
                ableitungsmodifikationen_struktur=xdf2.AbleitungsmodifikationenStruktur.NICHT_MODIFIZIERBAR,
                ableitungsmodifikationen_repraesentation=xdf2.AbleitungsmodifikationenRepraesentation.NICHT_MODIFIZIERBAR,
                regeln=[],
                struktur=[],
            ),
            xdf2.Schema(
                identifier=xdf2.Identifier("S01", xdf2.Version("1.0")),
                name="Schema",
                bezeichnung_eingabe=None,
                bezeichnung_ausgabe=None,
                beschreibung=None,
                definition=None,
                bezug=None,
                status=xdf2.Status.IN_VORBEREITUNG,
                versionshinweis=None,
                gueltig_ab=None,
                gueltig_bis=None,
                fachlicher_ersteller=None,
                freigabedatum=date.today(),
                veroeffentlichungsdatum=date.today(),
                hilfetext=None,
                ableitungsmodifikationen_struktur=xdf2.AbleitungsmodifikationenStruktur.NICHT_MODIFIZIERBAR,
                ableitungsmodifikationen_repraesentation=xdf2.AbleitungsmodifikationenRepraesentation.NICHT_MODIFIZIERBAR,
                regeln=[],
                struktur=[],
            ),
        ),
        (
            Xdf2Factory()
            .schema(id="S000001234", name="The Schema", fachlicher_ersteller="Bob")
            .build(),
            Xdf2Factory()
            .schema(
                id="S000001234",
                name="The Schema",
                fachlicher_ersteller="Bob",
                freigabedatum=date(year=2023, month=1, day=1),
            )
            .build(),
        ),
    ],
)
def test_should_correctly_recognize_schemas_with_differing_mutable_attributes_as_equal_in_immutable_attibutes_xdf2(
    schema_i: xdf2.Schema, schema_ii: xdf2.Schema
):
    immutable_schema_i = xdf2.ImmutableSchema.from_schema(schema_i)
    immutable_schema_ii = xdf2.ImmutableSchema.from_schema(schema_ii)

    assert immutable_schema_i == immutable_schema_ii


@pytest.mark.parametrize(
    "schema_i, schema_ii",
    [
        (
            xdf3.Schema(
                identifier=xdf3.Identifier("S01000", xdf3.Version("1.0.0")),
                name=din91379.StringLatin("Schema"),
                beschreibung=None,
                definition=None,
                bezug=[],
                freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER,
                status_gesetzt_am=None,
                status_gesetzt_durch=None,
                gueltig_ab=None,
                gueltig_bis=None,
                versionshinweis=None,
                veroeffentlichungsdatum=None,
                letzte_aenderung=datetime(year=2024, month=1, day=1),
                relation=[],
                stichwort=[],
                bezeichnung=din91379.StringLatin("bez"),
                hilfetext=None,
                ableitungsmodifikationen_struktur=xdf3.AbleitungsmodifikationenStruktur.NICHT_MODIFIZIERBAR,
                ableitungsmodifikationen_repraesentation=xdf3.AbleitungsmodifikationenRepraesentation.NICHT_MODIFIZIERBAR,
                dokumentsteckbrief=xdf3.Identifier("D01000", xdf3.Version("1.0.0")),
                regeln=[],
                struktur=[],
            ),
            xdf3.Schema(
                identifier=xdf3.Identifier("S01000", xdf3.Version("1.0.0")),
                name=din91379.StringLatin("Schema"),
                beschreibung=None,
                definition=None,
                bezug=[],
                freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
                status_gesetzt_am=date.today(),
                status_gesetzt_durch=None,
                gueltig_ab=None,
                gueltig_bis=None,
                versionshinweis=None,
                veroeffentlichungsdatum=date.today(),
                letzte_aenderung=datetime.now(),
                relation=[
                    xdf3.Relation(
                        xdf3.RelationTyp.ERSETZT,
                        xdf3.Identifier("S01000", xdf3.Version("0.5.0")),
                    )
                ],
                stichwort=[],
                bezeichnung=din91379.StringLatin("bez"),
                hilfetext=None,
                ableitungsmodifikationen_struktur=xdf3.AbleitungsmodifikationenStruktur.NICHT_MODIFIZIERBAR,
                ableitungsmodifikationen_repraesentation=xdf3.AbleitungsmodifikationenRepraesentation.NICHT_MODIFIZIERBAR,
                dokumentsteckbrief=xdf3.Identifier("D01000", xdf3.Version("0.0.99")),
                regeln=[],
                struktur=[],
            ),
        ),
        (
            Xdf3Factory()
            .schema(id="S000001234", name="The Schema", bezeichnung="bez")
            .build(),
            Xdf3Factory()
            .schema(
                id="S000001234",
                name="The Schema",
                bezeichnung="bez",
                status_gesetzt_am=date(year=2022, month=2, day=2),
            )
            .build(),
        ),
    ],
)
def test_should_correctly_recognize_schemas_with_differing_mutable_attributes_as_equal_in_immutable_attibutes_xdf3(
    schema_i: xdf3.Schema, schema_ii: xdf3.Schema
):
    immutable_schema_i = xdf3.ImmutableSchema.from_schema(schema_i)
    immutable_schema_ii = xdf3.ImmutableSchema.from_schema(schema_ii)

    assert immutable_schema_i == immutable_schema_ii


@pytest.mark.parametrize(
    "schema_i, schema_ii",
    [
        (
            Xdf2Factory().schema(name="Ferdi").build(),
            Xdf2Factory().schema(name="Frieda").build(),
        ),
        (
            Xdf2Factory()
            .schema(id="S000001234", name="The Schema", fachlicher_ersteller="Bob")
            .build(),
            Xdf2Factory()
            .schema(id="S000001234", name="The Schema", fachlicher_ersteller="Bob")
            .with_field()
            .build(),
        ),
    ],
)
def test_should_correctly_recognize_schemas_with_differing_immutable_attributes_xdf2(
    schema_i: xdf2.Schema, schema_ii: xdf2.Schema
):
    immutable_schema_i = xdf2.ImmutableSchema.from_schema(schema_i)
    immutable_schema_ii = xdf2.ImmutableSchema.from_schema(schema_ii)

    assert immutable_schema_i != immutable_schema_ii


@pytest.mark.parametrize(
    "schema_i, schema_ii",
    [
        (
            Xdf3Factory().schema(name="Ferdi").build(),
            Xdf3Factory().schema(name="Frieda").build(),
        ),
        (
            Xdf3Factory()
            .schema(id="S000001234", name="The Schema", bezeichnung="bez")
            .build(),
            Xdf3Factory()
            .schema(id="S000001234", name="The Schema", bezeichnung="bez")
            .with_field()
            .build(),
        ),
        (
            Xdf3Factory()
            .schema(id="S000001234", name="The Schema", bezeichnung="bez")
            .build(),
            Xdf3Factory()
            .schema(
                id="S000001234",
                name="The Schema",
                bezeichnung="bez",
                gueltig_ab=date(year=1999, month=12, day=31),
            )
            .build(),
        ),
        (
            Xdf3Factory()
            .schema(id="S000001234", name="The Schema", bezeichnung="bez")
            .build(),
            Xdf3Factory()
            .schema(
                id="S000001234",
                name="The Schema",
                bezeichnung="bez",
                ableitungsmodifikationen_repraesentation=xdf3.AbleitungsmodifikationenRepraesentation.MODIFIZIERBAR,
            )
            .build(),
        ),
        # array order of bezug of struktur element is different
        (
            xdf3.Schema(
                identifier=xdf3.Identifier("S01000", xdf3.Version("1.0.0")),
                name=din91379.StringLatin("Schema"),
                beschreibung=None,
                definition=None,
                bezug=[],
                freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER,
                status_gesetzt_am=None,
                status_gesetzt_durch=None,
                gueltig_ab=None,
                gueltig_bis=None,
                versionshinweis=None,
                veroeffentlichungsdatum=None,
                letzte_aenderung=datetime(year=2024, month=1, day=1),
                relation=[],
                stichwort=[],
                bezeichnung=din91379.StringLatin("bez"),
                hilfetext=None,
                ableitungsmodifikationen_struktur=xdf3.AbleitungsmodifikationenStruktur.NICHT_MODIFIZIERBAR,
                ableitungsmodifikationen_repraesentation=xdf3.AbleitungsmodifikationenRepraesentation.NICHT_MODIFIZIERBAR,
                dokumentsteckbrief=xdf3.Identifier("D01000", xdf3.Version("1.0.0")),
                regeln=[],
                struktur=[
                    xdf3.ElementReference(
                        xdf3.Anzahl(1, 1),
                        [
                            xdf3.Rechtsbezug(din91379.StringLatin("first"), None),
                            xdf3.Rechtsbezug(din91379.StringLatin("second"), None),
                        ],
                        xdf3.ElementType.FELD,
                        xdf3.Identifier("F01000", xdf3.Version("1.0.0")),
                    )
                ],
            ),
            xdf3.Schema(
                identifier=xdf3.Identifier("S01000", xdf3.Version("1.0.0")),
                name=din91379.StringLatin("Schema"),
                beschreibung=None,
                definition=None,
                bezug=[],
                freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
                status_gesetzt_am=date.today(),
                status_gesetzt_durch=None,
                gueltig_ab=None,
                gueltig_bis=None,
                versionshinweis=None,
                veroeffentlichungsdatum=date.today(),
                letzte_aenderung=datetime.now(),
                relation=[
                    xdf3.Relation(
                        xdf3.RelationTyp.ERSETZT,
                        xdf3.Identifier("S01000", xdf3.Version("0.5.0")),
                    )
                ],
                stichwort=[],
                bezeichnung=din91379.StringLatin("bez"),
                hilfetext=None,
                ableitungsmodifikationen_struktur=xdf3.AbleitungsmodifikationenStruktur.NICHT_MODIFIZIERBAR,
                ableitungsmodifikationen_repraesentation=xdf3.AbleitungsmodifikationenRepraesentation.NICHT_MODIFIZIERBAR,
                dokumentsteckbrief=xdf3.Identifier("D01000", xdf3.Version("0.0.99")),
                regeln=[],
                struktur=[
                    xdf3.ElementReference(
                        xdf3.Anzahl(1, 1),
                        [
                            xdf3.Rechtsbezug(din91379.StringLatin("second"), None),
                            xdf3.Rechtsbezug(din91379.StringLatin("first"), None),
                        ],
                        xdf3.ElementType.FELD,
                        xdf3.Identifier("F01000", xdf3.Version("1.0.0")),
                    )
                ],
            ),
        ),
    ],
)
def test_should_correctly_recognize_schemas_with_differing_immutable_attributes_xdf3(
    schema_i: xdf3.Schema, schema_ii: xdf3.Schema
):
    immutable_schema_i = xdf3.ImmutableSchema.from_schema(schema_i)
    immutable_schema_ii = xdf3.ImmutableSchema.from_schema(schema_ii)

    assert immutable_schema_i != immutable_schema_ii


# TODO: Test veroeffentlichungsdatum edge case: changing an already set veroeffentlichungsdatum
# is not allowed in import

# TODO: Later test the above cases by using the outside behavior of the schema import
# instead of directly testing the under-the-hood classes.
