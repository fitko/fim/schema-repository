import pytest
from fastapi.testclient import TestClient

from fimportal import xzufi
from tests.conftest import CommandRunner
from tests.data import XZUFI_2_2_0_DATA
from tests.factories.xzufi import LeistungFactory


@pytest.mark.parametrize(
    "page",
    [
        "",
        "?language_code=de-DE",
        "?language_code=en",
    ],
)
@pytest.mark.parametrize(
    "filename",
    [
        "leistung_minimal.xml",
        "leistung_full.xml",
        "leistung_bearbeitungsdauer_empty.xml",
        "leistung_bearbeitungsdauer_frist_dauer.xml",
        "leistung_frist_dauer.xml",
        "leistung_frist_stichtag_datum.xml",
        "leistung_frist_stichtag_monat_tag.xml",
        "leistung_kosten_fix.xml",
        "leistung_kosten_frei.xml",
        "leistung_kosten_ungelisteter_typ.xml",
        "leistung_kosten_variabel.xml",
        "leistung_struktur_lo_leika.xml",
        "leistung_struktur_lo_individuell.xml",
        "leistung_struktur_lov.xml",
        "leistung_struktur_lovd.xml",
    ],
)
def test_load_leistungsbeschreibung_page(
    client: TestClient, runner: CommandRunner, filename: str, page: str
) -> None:
    with open(XZUFI_2_2_0_DATA / filename, "r", encoding="utf-8") as file:
        data = file.read()

    leistung = xzufi.parse_leistung(data)
    identifier = leistung.get_identifier()
    assert identifier is not None
    redaktion_id, leistung_id = identifier

    runner.import_pvog(leistung, xml_content=data)

    response = client.get(f"/xzufi-services/{redaktion_id}/{leistung_id}{page}")

    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_fail_for_steckbrief(client: TestClient, runner: CommandRunner) -> None:
    leistung = LeistungFactory.steckbrief().save_leika(runner)

    response = client.get(
        f"/xzufi-services/{leistung.redaktion_id}/{leistung.leistung_id}"
    )

    assert response.status_code == 404
    assert response.headers["content-type"] == "text/html; charset=utf-8"


def test_unknown_leistungsbeschreibung(client: TestClient) -> None:
    response = client.get("/xzufi-services/L1234/leistung_1")

    assert response.status_code == 404
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert "Leistung leistung_1 konnte nicht gefunden werden." in response.text
