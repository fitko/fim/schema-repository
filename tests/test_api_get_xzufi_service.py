from fastapi.testclient import TestClient

from fimportal.common import FreigabeStatus, RawCode
from fimportal.helpers import format_iso8601, utc_now
from fimportal.xzufi.leistung import (
    LeistungsTypisierung,
    LeistungsstrukturObjekt,
)
from tests.conftest import CommandRunner
from tests.factories.xzufi import LeistungFactory

ENDPOINT = "/api/v0/xzufi-services"


def test_return_existing_leistung(runner: CommandRunner, client: TestClient):
    now = utc_now()

    LeistungFactory(
        redaktion_id="redaktion_1",
        leistung_id="leistung_1",
        struktur=LeistungsstrukturObjekt(
            leistungsgruppierung=RawCode("", "", "leika_gruppierung")
        ),
        bezeichnung="Leistung 1",
        bezeichnung_2="LB2",
        kurztext="Kurztext",
        volltext="Volltext",
        rechtsgrundlagen="Rechtsgrundlagen",
        freigabestatus_katalog=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        freigabestatus_bibliothek=FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER,
    ).add_typisierung(LeistungsTypisierung.TYP_2).add_leistungsschluessel(
        "99123400000000"
    ).with_versionsinformation(
        erstellt_datum_zeit=now,
        geaendert_datum_zeit=now,
    ).save_pvog(runner)

    response = client.get(f"{ENDPOINT}/redaktion_1/leistung_1")

    assert response.status_code == 200
    assert response.json() == {
        "redaktion_id": "redaktion_1",
        "id": "leistung_1",
        "title": "Leistung 1",
        "leistungsbezeichnung": "Leistung 1",
        "leistungsbezeichnung_2": "LB2",
        "freigabestatus_katalog": FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value,
        "freigabestatus_bibliothek": FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER.value,
        "hierarchy_type": "beschreibung",
        "root_for_leistungsschluessel": None,
        "leistungstyp": "lo",
        "typisierung": [LeistungsTypisierung.TYP_2.value],
        "leistungsschluessel": ["99123400000000"],
        "kurztext": "Kurztext",
        "volltext": "Volltext",
        "rechtsgrundlagen": "Rechtsgrundlagen",
        "erstellt_datum_zeit": format_iso8601(now),
        "geaendert_datum_zeit": format_iso8601(now),
        "steckbrief_name": None,
    }


def test_fail_for_steckbriefe(client: TestClient, runner: CommandRunner):
    leistung = LeistungFactory.steckbrief().save_leika(runner)

    response = client.get(f"{ENDPOINT}/{leistung.redaktion_id}/{leistung.leistung_id}")

    assert response.status_code == 404
    assert response.json() == {
        "detail": f"Could not find service {leistung.leistung_id} from Redaktion {leistung.redaktion_id}.",
    }


def test_fail_for_unknown_leistung(client: TestClient):
    response = client.get(f"{ENDPOINT}/L1234/leistung_1")

    assert response.status_code == 404
    assert response.json() == {
        "detail": "Could not find service leistung_1 from Redaktion L1234.",
    }
