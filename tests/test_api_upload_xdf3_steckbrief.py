from fastapi.testclient import TestClient
from freezegun import freeze_time

from tests.conftest import CommandRunner
from tests.data import XDF3_DATA

POST_STECKBRIEF_ROUTE = "/api/v1/document-profiles/xdf3"


def test_should_fail_for_missing_access_token(client: TestClient):
    with open(XDF3_DATA / "steckbrief.xdf3.xml", "rb") as steckbrief:
        response = client.post(
            POST_STECKBRIEF_ROUTE,
            files=[("document_profile", steckbrief)],
        )

    assert response.status_code == 401
    assert response.json() == {"detail": "invalid access token"}


def test_should_fail_for_wrong_access_token(client: TestClient):
    with open(XDF3_DATA / "steckbrief.xdf3.xml", "rb") as steckbrief:
        response = client.post(
            POST_STECKBRIEF_ROUTE,
            files=[("document_profile", steckbrief)],
            headers={"Access-Token": "wrong_access_token"},
        )

    assert response.status_code == 401
    assert response.json() == {"detail": "invalid access token"}


def test_should_fail_for_invalid_steckbrief(runner: CommandRunner, client: TestClient):
    token = runner.create_access_token("12000")

    with open(XDF3_DATA / "code_list_a.xml", "rb") as schema:
        response = client.post(
            POST_STECKBRIEF_ROUTE,
            files=[("document_profile", schema)],
            headers={"Access-Token": token},
        )

    assert response.status_code == 400
    assert response.json() == {
        "detail": "Could not parse document profile: Unexpected child node [tag={http://docs.oasis-open.org/codelist/ns/genericode/1.0/}CodeList], line 2"
    }


def test_should_correctly_save_the_steckbrief(
    runner: CommandRunner, client: TestClient
):
    token = runner.create_access_token("02000")

    freeze_timestamp = "2023-10-17T00:00:00Z"
    with freeze_time(freeze_timestamp):
        with open(XDF3_DATA / "steckbrief.xdf3.xml", "rb") as steckbrief:
            response = client.post(
                POST_STECKBRIEF_ROUTE,
                files=[("document_profile", steckbrief)],
                headers={"Access-Token": token},
            )

    assert response.status_code == 200
    assert response.json() == {
        "fim_id": "D02000000001",
        "fim_version": "1.0.0",
        "nummernkreis": "02000",
        "name": "abstrakter Dokumentsteckbrief",
        "beschreibung": "Beschreibung des Dokumentensteckbriefs",
        "definition": "Definition des Berichts",
        "freigabe_status": 2,
        "freigabe_status_label": "in Bearbeitung",
        "status_gesetzt_durch": "Feldertest",
        "status_gesetzt_am": "2019-08-27",
        "gueltig_ab": "2019-09-01",
        "gueltig_bis": "2020-09-01",
        "hilfetext": "Ein Hilfetext",
        "ist_abstrakt": True,
        "bezug": ["Bezug"],
        "versionshinweis": "Version erstellt ohne Angabe eines Versionshinweises.",
        "veroeffentlichungsdatum": "2019-08-27",
        "letzte_aenderung": "2024-06-27T13:40:36Z",
        "last_update": freeze_timestamp,
        "bezeichnung": "abstrakter Dokumentsteckbrief",
        "dokumentart": "003",
        "stichwort": ["Freie und Hansestadt Hamburg"],
        "relation": [
            {"fim_id": "E00001", "fim_version": "1.0.0", "typ": "ERS", "name": None},
        ],
        "xdf_version": "3.0.0",
        "is_latest": True,
        "prozesse": [],
        "datenschemata": [],
    }


def test_should_correctly_save_special_characters_in_bezug(
    runner: CommandRunner, client: TestClient
):
    """This test is about potential escaping of special characters

    This test is less about steckbrief or bezug. It is about how the database
    receives and gives special characters and that no unwanted additional
    escape characters appear.
    """
    token = runner.create_access_token("02000")

    freeze_timestamp = "2023-10-17T00:00:00Z"
    with freeze_time(freeze_timestamp):
        with open(
            XDF3_DATA / "steckbrief_with_special_chars_in_bezug.xdf3.xml", "rb"
        ) as steckbrief:
            response = client.post(
                POST_STECKBRIEF_ROUTE,
                files=[("document_profile", steckbrief)],
                headers={"Access-Token": token},
            )

    assert response.status_code == 200
    assert response.json()["bezug"] == ["Bezug", 'B"e"z//ug', "Bez/u'&\"%g", "<>"]


def test_should_upload_even_for_identical_freigabe_status(
    runner: CommandRunner, client: TestClient
):
    token = runner.create_access_token("02000")
    content = (XDF3_DATA / "steckbrief.xdf3.xml").read_text()

    response = client.post(
        POST_STECKBRIEF_ROUTE,
        files=[("document_profile", content)],
        headers={"Access-Token": token},
    )
    assert response.status_code == 200

    # Reupload with the same freigabe status
    response = client.post(
        POST_STECKBRIEF_ROUTE,
        files=[("document_profile", content)],
        headers={"Access-Token": token},
    )
    assert response.status_code == 200


def test_should_fail_for_uploading_existing_steckbrief_with_changed_immutable_attribute(
    runner: CommandRunner, client: TestClient
):
    token = runner.create_access_token("02000")

    freeze_timestamp = "2023-10-17T00:00:00Z"
    with freeze_time(freeze_timestamp):
        with open(XDF3_DATA / "steckbrief.xdf3.xml", "rb") as steckbrief:
            response = client.post(
                POST_STECKBRIEF_ROUTE,
                files=[("document_profile", steckbrief)],
                headers={"Access-Token": token},
            )

    assert response.status_code == 200

    with freeze_time(freeze_timestamp):
        with open(
            XDF3_DATA / "steckbrief_with_changed_attributes.xdf3.xml", "rb"
        ) as steckbrief:
            response = client.post(
                POST_STECKBRIEF_ROUTE,
                files=[("document_profile", steckbrief)],
                headers={"Access-Token": token},
            )

    assert response.status_code == 400
    assert response.json() == {
        "detail": (
            "Cannot update document-profile D02000000001 version 1.0.0 because the following attributes are different:\n"
            "  - bezeichnung: abstrakter Dokumentsteckbrief -> abstrakterer Dokumentsteckbrief\n"
            "  - name: abstrakter Dokumentsteckbrief -> Dokumentsteckbrief"
        )
    }
