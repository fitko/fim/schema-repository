from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.factories import Xdf2Factory
from tests.factories.xdf3 import Xdf3Factory


def test_should_return_the_full_quality_report(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf3Factory()
    field = factory.field().build()
    schema = factory.schema().with_field(field).save(runner)

    response = client.get(
        f"/api/v1/schemas/{schema.fim_id}/{schema.fim_version}/quality-report"
    )

    assert response.status_code == 200
    assert response.json() == {
        "schema_checks": [],
        "group_reports": [],
        "field_reports": [
            {
                "failing_checks": [],
                "identifier": {
                    "id": field.identifier.id,
                    "version": field.identifier.assert_version(),
                },
            }
        ],
        "rule_reports": [],
        "total_checks": 0,
        "total_group_checks": 0,
        "total_field_checks": 0,
        "total_rule_checks": 0,
    }


def test_should_return_the_xdf2_quality_report(
    runner: CommandRunner, client: TestClient
):
    schema_import = Xdf2Factory().schema().save(runner)

    response = client.get(
        f"/api/v1/schemas/{schema_import.fim_id}/{schema_import.fim_version}/quality-report"
    )

    assert response.status_code == 200
    assert response.json() == {
        "schema_checks": [],
        "group_reports": [],
        "field_reports": [],
        "rule_reports": [],
        "total_checks": 0,
        "total_group_checks": 0,
        "total_field_checks": 0,
        "total_rule_checks": 0,
    }


def test_should_work_with_latest_version(runner: CommandRunner, client: TestClient):
    schema_import = Xdf2Factory().schema().save(runner)

    response = client.get(
        f"/api/v1/schemas/{schema_import.fim_id}/latest/quality-report"
    )

    assert response.status_code == 200
    assert response.json() == {
        "schema_checks": [],
        "group_reports": [],
        "field_reports": [],
        "rule_reports": [],
        "total_checks": 0,
        "total_group_checks": 0,
        "total_field_checks": 0,
        "total_rule_checks": 0,
    }


def test_should_return_correct_error_for_unknown_schema(client: TestClient):
    response = client.get("/api/v1/schemas/S1245/1.0/quality-report")

    assert response.status_code == 404
    assert response.json() == {"detail": "Could not find schema S1245V1.0."}
