from datetime import datetime, timezone

import pytest
from freezegun import freeze_time

from fimportal.models.imports import CodeListImport
from fimportal.service import Service
from tests.data import XDF2_DATA
from tests.factories.xdf2 import Xdf2Factory
from tests.factories.xdf3 import Xdf3Factory


class TestListAllSchemaIdentifiers:
    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_return_all_schema_identifiers(self, service: Service):
        factory = Xdf3Factory()
        xdf3_import = factory.schema().full().build_import()
        await service.import_xdf3_schema(xdf3_import, strict=True)

        identifiers = await service.get_all_schema_identifiers()

        assert identifiers == [(xdf3_import.id, xdf3_import.version)]


class TestUpdateXdf3Conversions:
    # TODO: This test fails currently. Fix the underlying bug. Ticket #792
    # @pytest.mark.asyncio(loop_scope="session")
    # async def test_should_not_update_json_schema_if_nothing_has_changed(
    #     self, service: Service
    # ):
    #     factory = Xdf3Factory()
    #     xdf3_import = factory.schema().full().build_import()
    #     await service.import_xdf3_schema(xdf3_import, strict=True)

    #     schema = await service.get_schema(xdf3_import.id, xdf3_import.version)
    #     assert schema is not None
    #     assert len(schema.json_schema_files) == 1

    #     await service.update_generated_data(xdf3_import.id, xdf3_import.version)

    #     updated_schema = await service.get_schema(xdf3_import.id, xdf3_import.version)
    #     assert updated_schema is not None
    #     assert len(updated_schema.json_schema_files) == 1
    #     assert updated_schema.last_update == schema.last_update

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_update_json_schema_if_the_output_changed(
        self, service: Service
    ):
        factory = Xdf3Factory()
        xdf3_import = factory.schema().full().build_import()
        with freeze_time("2023-10-17T00:00:00"):
            await service.import_xdf3_schema(xdf3_import, strict=True)

        schema = await service.get_schema(xdf3_import.id, xdf3_import.version)
        assert schema is not None
        assert len(schema.json_schema_files) == 1

        await _force_change_json_schema_content(service)
        with freeze_time("2023-10-18T00:00:00"):
            await service.update_generated_data(xdf3_import.id, xdf3_import.version)

        schema = await service.get_schema(xdf3_import.id, xdf3_import.version)
        assert schema is not None
        assert len(schema.json_schema_files) == 2
        assert schema.last_update == datetime(2023, 10, 18, tzinfo=timezone.utc)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_update_the_quality_report(self, service: Service):
        factory = Xdf3Factory()
        xdf3_import = factory.schema().full().build_import()
        with freeze_time("2023-10-17T00:00:00"):
            await service.import_xdf3_schema(xdf3_import, strict=True)

        quality_report = await service.get_schema_quality_report(
            xdf3_import.id, xdf3_import.version
        )
        assert quality_report is not None

        await _force_change_quality_report(service)
        with freeze_time("2023-10-18T00:00:00"):
            await service.update_generated_data(xdf3_import.id, xdf3_import.version)

        quality_report = await service.get_schema_quality_report(
            xdf3_import.id, xdf3_import.version
        )
        assert quality_report is not None

        schema = await service.get_schema(xdf3_import.id, xdf3_import.version)
        assert schema is not None
        assert schema.last_update == datetime(2023, 10, 18, tzinfo=timezone.utc)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_work_for_schema_with_code_lists(self, service: Service):
        with open(XDF2_DATA / "code_list_a.xml", "rb") as code_list_file:
            code_list_text = code_list_file.read()

        code_list = CodeListImport.from_bytes(code_list_text)

        factory = Xdf2Factory()
        field = factory.field().with_code_list(code_list).build()
        xdf2_import = (
            factory.schema().with_field(field).build_import(code_lists=[code_list])
        )
        await service.import_xdf2_schema(xdf2_import, strict=True)

        await service.update_generated_data(xdf2_import.id, xdf2_import.version)


class TestUpdateXdf2Conversions:
    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_not_update_json_schema_if_nothing_has_changed(
        self, service: Service
    ):
        xdf2_import = Xdf2Factory().schema().build_import()
        await service.import_xdf2_schema(xdf2_import, strict=True)

        schema = await service.get_schema(xdf2_import.id, xdf2_import.version)
        assert schema is not None
        assert len(schema.json_schema_files) == 1

        await service.update_generated_data(xdf2_import.id, xdf2_import.version)

        updated_schema = await service.get_schema(xdf2_import.id, xdf2_import.version)
        assert updated_schema is not None
        assert len(updated_schema.json_schema_files) == 1
        assert updated_schema.last_update == schema.last_update

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_update_json_schema_if_the_output_changed(
        self, service: Service
    ):
        xdf2_import = Xdf2Factory().schema().build_import()
        with freeze_time("2023-10-17T00:00:00"):
            await service.import_xdf2_schema(xdf2_import, strict=True)

        schema = await service.get_schema(xdf2_import.id, xdf2_import.version)
        assert schema is not None
        assert len(schema.json_schema_files) == 1

        await _force_change_json_schema_content(service)
        with freeze_time("2023-10-18T00:00:00"):
            await service.update_generated_data(xdf2_import.id, xdf2_import.version)

        schema = await service.get_schema(xdf2_import.id, xdf2_import.version)
        assert schema is not None
        assert len(schema.json_schema_files) == 2
        assert schema.last_update == datetime(2023, 10, 18, tzinfo=timezone.utc)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_not_update_xsd_if_nothing_has_changed(self, service: Service):
        xdf2_import = Xdf2Factory().schema().build_import()
        await service.import_xdf2_schema(xdf2_import, strict=True)

        schema = await service.get_schema(xdf2_import.id, xdf2_import.version)
        assert schema is not None
        assert len(schema.xsd_files) == 1

        await service.update_generated_data(xdf2_import.id, xdf2_import.version)

        schema = await service.get_schema(xdf2_import.id, xdf2_import.version)
        assert schema is not None
        assert len(schema.xsd_files) == 1

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_update_xsd_if_the_output_changed(self, service: Service):
        xdf2_import = Xdf2Factory().schema().build_import()
        with freeze_time("2023-10-17T00:00:00"):
            await service.import_xdf2_schema(xdf2_import, strict=True)

        schema = await service.get_schema(xdf2_import.id, xdf2_import.version)
        assert schema is not None
        assert len(schema.xsd_files) == 1

        await _force_change_xsd_content(service)
        with freeze_time("2023-10-18T00:00:00"):
            await service.update_generated_data(xdf2_import.id, xdf2_import.version)

        schema = await service.get_schema(xdf2_import.id, xdf2_import.version)
        assert schema is not None
        assert len(schema.xsd_files) == 2
        assert schema.last_update == datetime(2023, 10, 18, tzinfo=timezone.utc)

    @pytest.mark.asyncio(loop_scope="session")
    async def test_should_update_the_quality_report(self, service: Service):
        xdf2_import = Xdf2Factory().schema().build_import()
        with freeze_time("2023-10-17T00:00:00"):
            await service.import_xdf2_schema(xdf2_import, strict=True)

        quality_report = await service.get_schema_quality_report(
            xdf2_import.id, xdf2_import.version
        )
        assert quality_report is not None

        await _force_change_quality_report(service)
        with freeze_time("2023-10-18T00:00:00"):
            await service.update_generated_data(xdf2_import.id, xdf2_import.version)

        quality_report = await service.get_schema_quality_report(
            xdf2_import.id, xdf2_import.version
        )
        assert quality_report is not None

        schema = await service.get_schema(xdf2_import.id, xdf2_import.version)
        assert schema is not None
        assert schema.last_update == datetime(2023, 10, 18, tzinfo=timezone.utc)


async def _force_change_json_schema_content(service: Service):
    database = service._database  # type: ignore

    await database.connection.execute(
        """
            UPDATE json_schema_file
            SET canonical_hash = 'overwritten'
            """
    )


async def _force_change_xsd_content(service: Service):
    database = service._database  # type: ignore

    await database.connection.execute(
        """
            UPDATE xsd_file
            SET canonical_hash = 'overwritten'
            """
    )


async def _force_change_quality_report(service: Service):
    database = service._database  # type: ignore

    await database.connection.execute(
        """
            UPDATE schema
            SET quality_report = '{}'
            """
    )
