import pytest

from fimportal.common import FreigabeStatus
from fimportal.models.xzufi import XzufiSource
from fimportal.service import Service
from fimportal.xzufi.client import TEST_LEISTUNG_XML
from fimportal.xzufi.common import LeistungRedaktionId
from fimportal.xzufi.leistung import LeistungsTypisierung
from tests.factories.xzufi import LeistungFactory


@pytest.mark.asyncio(loop_scope="session")
async def test_should_upload_a_leistungsbeschreibung(
    service: Service,
):
    leistung = LeistungFactory().build()

    imported_leistungen = await service.import_leistung_batch(
        [(leistung, TEST_LEISTUNG_XML)],
        source=XzufiSource.PVOG,
    )

    saved_leistung = await service.get_leistungsbeschreibung(imported_leistungen[0])
    assert saved_leistung is not None


@pytest.mark.asyncio(loop_scope="session")
@pytest.mark.parametrize(
    "typisierung", [typisierung for typisierung in LeistungsTypisierung]
)
async def test_should_upload_a_steckbrief(
    service: Service, typisierung: LeistungsTypisierung
):
    leistung = LeistungFactory.steckbrief(typisierung=typisierung).build()

    await service.import_leistung_batch(
        [(leistung, TEST_LEISTUNG_XML)],
        source=XzufiSource.XZUFI,
    )

    saved_leistung = await service.get_leistungssteckbrief(
        leistung.get_leistungsschluessel()[0]
    )
    assert saved_leistung is not None
    assert not saved_leistung.has_stammtext


@pytest.mark.asyncio(loop_scope="session")
@pytest.mark.parametrize(
    "typisierung",
    [
        LeistungsTypisierung.TYP_2,
        LeistungsTypisierung.TYP_2A,
        LeistungsTypisierung.TYP_2B,
        LeistungsTypisierung.TYP_3,
        LeistungsTypisierung.TYP_3A,
        LeistungsTypisierung.TYP_3B,
        LeistungsTypisierung.TYP_2_3,
        LeistungsTypisierung.TYP_2_3A,
        LeistungsTypisierung.TYP_2_3B,
    ],
)
@pytest.mark.parametrize(
    "status_bibliothek",
    [
        FreigabeStatus.FACHLICH_FREIGEGEBEN_SILBER,
        FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
    ],
)
async def test_should_upload_a_stammtext(
    service: Service,
    typisierung: LeistungsTypisierung,
    status_bibliothek: FreigabeStatus,
):
    leistung = LeistungFactory.stammtext(
        typisierung=typisierung,
        freigabestatus_bibliothek=status_bibliothek,
    ).build()

    await service.import_leistung_batch(
        [(leistung, TEST_LEISTUNG_XML)],
        source=XzufiSource.XZUFI,
    )

    saved_leistung = await service.get_leistungssteckbrief(
        leistung.get_leistungsschluessel()[0]
    )
    assert saved_leistung is not None
    assert saved_leistung.has_stammtext


@pytest.mark.asyncio(loop_scope="session")
@pytest.mark.parametrize(
    "status_bibliothek",
    [
        None,
        FreigabeStatus.IN_PLANUNG,
        FreigabeStatus.IN_BEARBEITUNG,
        FreigabeStatus.ENTWURF,
        FreigabeStatus.METHODISCH_FREIGEGEBEN,
        FreigabeStatus.INAKTIV,
        FreigabeStatus.VORGESEHEN_ZUM_LOESCHEN,
    ],
)
async def test_should_not_mark_as_stammtext_with_wrong_status(
    service: Service, status_bibliothek: FreigabeStatus | None
):
    leistung = LeistungFactory.stammtext(
        freigabestatus_bibliothek=status_bibliothek,
    ).build()

    await service.import_leistung_batch(
        [(leistung, TEST_LEISTUNG_XML)],
        source=XzufiSource.XZUFI,
    )

    saved_leistung = await service.get_leistungssteckbrief(
        leistung.get_leistungsschluessel()[0]
    )
    assert saved_leistung is not None
    assert not saved_leistung.has_stammtext


@pytest.mark.asyncio(loop_scope="session")
@pytest.mark.parametrize(
    "typisierung",
    [
        LeistungsTypisierung.TYP_1,
        LeistungsTypisierung.TYP_4,
        LeistungsTypisierung.TYP_4A,
        LeistungsTypisierung.TYP_4B,
        LeistungsTypisierung.TYP_5,
        LeistungsTypisierung.TYP_6,
        LeistungsTypisierung.TYP_7,
        LeistungsTypisierung.TYP_8,
        LeistungsTypisierung.TYP_9,
        LeistungsTypisierung.TYP_10,
        LeistungsTypisierung.TYP_11,
        LeistungsTypisierung.TYP_12,
    ],
)
async def test_should_not_mark_as_stammtext_with_wrong_typisierung(
    service: Service, typisierung: LeistungsTypisierung
):
    # All typisierungen must be valid, a single invalid typisierung should already
    # result in this being categorized as only a steckbrief.
    leistung = LeistungFactory.stammtext().add_typisierung(typisierung).build()

    await service.import_leistung_batch(
        [(leistung, TEST_LEISTUNG_XML)],
        source=XzufiSource.XZUFI,
    )

    saved_leistung = await service.get_leistungssteckbrief(
        leistung.get_leistungsschluessel()[0]
    )
    assert saved_leistung is not None
    assert not saved_leistung.has_stammtext


@pytest.mark.asyncio(loop_scope="session")
async def test_should_not_upload_a_steckbrief_with_multiple_leistungsschluesseln(
    service: Service,
):
    leistung = (
        LeistungFactory.steckbrief().add_leistungsschluessel("99001001000000").build()
    )

    imported_leistungen = await service.import_leistung_batch(
        [(leistung, TEST_LEISTUNG_XML)],
        source=XzufiSource.XZUFI,
    )
    assert len(imported_leistungen) == 0


@pytest.mark.asyncio(loop_scope="session")
async def test_should_not_upload_a_stammtext_with_no_typisierungen(
    service: Service,
):
    leistung = (
        LeistungFactory(
            redaktion_id=LeistungRedaktionId.LEIKA.value,
            freigabestatus_bibliothek=FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        ).add_leistungsschluessel("99123000000000")
    ).build()

    await service.import_leistung_batch(
        [(leistung, TEST_LEISTUNG_XML)],
        source=XzufiSource.XZUFI,
    )

    saved_leistung = await service.get_leistungssteckbrief(
        leistung.get_leistungsschluessel()[0]
    )
    assert saved_leistung is not None
    assert not saved_leistung.has_stammtext


@pytest.mark.asyncio(loop_scope="session")
async def test_should_use_the_bezeichnung_as_title(service: Service):
    leistung = LeistungFactory(bezeichnung="Eine Leistung").build()

    imported_leistungen = await service.import_leistung_batch(
        [(leistung, TEST_LEISTUNG_XML)],
        source=XzufiSource.PVOG,
    )
    assert len(imported_leistungen) == 1

    saved_leistung = await service.get_leistungsbeschreibung(imported_leistungen[0])

    assert saved_leistung is not None
    assert saved_leistung.title == "Eine Leistung"


@pytest.mark.asyncio(loop_scope="session")
async def test_should_use_the_bezeichnung_2_as_title_if_bezeichnung_is_none(
    service: Service,
):
    leistung = LeistungFactory(bezeichnung=None, bezeichnung_2="Bezeichnung 2").build()

    imported_leistungen = await service.import_leistung_batch(
        [(leistung, TEST_LEISTUNG_XML)],
        source=XzufiSource.PVOG,
    )
    assert len(imported_leistungen) == 1

    saved_leistung = await service.get_leistungsbeschreibung(imported_leistungen[0])

    assert saved_leistung is not None
    assert saved_leistung.title == "Bezeichnung 2"


@pytest.mark.asyncio(loop_scope="session")
async def test_should_use_fallback_title_for_no_bezeichnung_at_all(
    service: Service,
):
    leistung = LeistungFactory(bezeichnung=None, bezeichnung_2=None).build()

    imported_leistungen = await service.import_leistung_batch(
        [(leistung, TEST_LEISTUNG_XML)],
        source=XzufiSource.PVOG,
    )
    assert len(imported_leistungen) == 1

    saved_leistung = await service.get_leistungsbeschreibung(imported_leistungen[0])

    assert saved_leistung is not None
    assert saved_leistung.title == "Kein Titel"
