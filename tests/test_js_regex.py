import re

import pytest

from fimportal.json_schema import check_js_regex
from fimportal.json_schema.regex import RegexException


def test_should_return_error_message_for_invalid_regex():
    with pytest.raises(
        RegexException,
        match=re.escape(
            "Invalid regular expression: /([0]{1}[1-9]{1}|[1-9]{1}[0-9]{1})[0-9]{3})/: Unmatched ')'"
        ),
    ):
        check_js_regex("([0]{1}[1-9]{1}|[1-9]{1}[0-9]{1})[0-9]{3})")


def test_should_work_for_valid_regex():
    check_js_regex("([0]{1}[1-9]{1}|[1-9]{1}[0-9]{1})[0-9]{3}")
