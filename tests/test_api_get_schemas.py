from datetime import date, timedelta

import pytest
from fastapi.testclient import TestClient
from freezegun import freeze_time

from fimportal.din91379 import parse_string_latin
from fimportal.helpers import utc_now
from fimportal.xdatenfelder import xdf3
from fimportal.xdatenfelder.xdf3.constants import FREIGABE_STATUS_VALID_FOR_UPLOAD
from tests.conftest import CommandRunner
from tests.data import XDF3_DATA
from tests.factories.xdf2 import Xdf2Factory
from tests.factories.xdf3 import (
    Xdf3Factory,
)

today = date.today()


@pytest.mark.parametrize(
    "query_parameter", ["some sch", "Some Schema Name", "name", "NAME", "chem"]
)
def test_get_schema_by_name(
    runner: CommandRunner, client: TestClient, query_parameter: str
):
    factory = Xdf2Factory()
    factory.schema(name="Some Schema Name", id="S01").save(runner)
    factory.schema(name="Something Different", id="S02").save(runner)

    response = client.get(f"/api/v1/schemas?name={query_parameter}")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "S01"


def test_return_an_empty_list(client: TestClient):
    response = client.get("/api/v1/schemas")

    assert response.status_code == 200

    assert response.json() == {
        "items": [],
        "count": 0,
        "offset": 0,
        "limit": 200,
        "total_count": 0,
    }


def test_schema_pagination_custom_limit(runner: CommandRunner, client: TestClient):
    factory = Xdf2Factory()
    for _ in range(5):
        factory.schema().save(runner)

    response = client.get("/api/v1/schemas?limit=5")

    assert response.status_code == 200

    result = response.json()
    assert result["count"] == 5
    assert result["offset"] == 0
    assert result["limit"] == 5
    assert result["total_count"] == 5


def test_schema_pagination_custom_limit_and_offset(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    for _ in range(5):
        factory.schema().save(runner)

    response = client.get("/api/v1/schemas?limit=5&offset=2")

    assert response.status_code == 200

    result = response.json()
    assert result["count"] == 3
    assert result["offset"] == 2
    assert result["limit"] == 5
    assert result["total_count"] == 5


def test_schema_pagination_wrong_limit(client: TestClient):
    response = client.get("/api/v1/schemas?limit=5000")

    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "input": "5000",
                "loc": ["query", "limit"],
                "msg": "Input should be less than or equal to 200",
                "type": "less_than_equal",
                "ctx": {"le": 200},
            }
        ]
    }


def test_schema_pagination_high_offset(runner: CommandRunner, client: TestClient):
    factory = Xdf2Factory()
    for _ in range(5):
        factory.schema().save(runner)

    response = client.get("/api/v1/schemas?offset=5&limit=5")

    assert response.status_code == 200
    assert response.json()["count"] == 0


@pytest.mark.parametrize(
    "search_term",
    ["1", "12000"],
)
def test_get_schema_by_nummernkreis(
    runner: CommandRunner, client: TestClient, search_term: str
):
    Xdf2Factory().schema("S12345").save(runner)

    response = client.get(f"/api/v1/schemas?nummernkreis={search_term}")

    assert response.status_code == 200
    assert response.json()["count"] == 1


@pytest.mark.parametrize(
    "search_term",
    ["2", "2000"],
)
def test_should_not_return_schema_by_wrong_nummernkreis(
    runner: CommandRunner, client: TestClient, search_term: str
):
    Xdf2Factory().schema("S12345").save(runner)

    response = client.get(f"/api/v1/schemas?nummernkreis={search_term}")

    assert response.status_code == 200
    assert response.json()["count"] == 0


def test_get_schema_by_author(runner: CommandRunner, client: TestClient):
    Xdf2Factory().schema(fachlicher_ersteller="Ersteller").save(runner)

    response = client.get("/api/v1/schemas?status_gesetzt_durch=Ersteller")

    assert response.status_code == 200
    assert response.json()["count"] == 1


@pytest.mark.parametrize("bezeichnung", ["Eingabe", "eing", "ung E", "ngabe"])
def test_get_schema_by_bezeichnung(
    runner: CommandRunner, client: TestClient, bezeichnung: str
):
    factory = Xdf2Factory()
    factory.schema(bezeichnung_eingabe="Bezeichnung Eingabe", id="S000001").save(runner)
    factory.schema(bezeichnung_eingabe="Bezeichnung Ausgabe", id="S000002").save(runner)

    response = client.get(f"/api/v1/schemas?bezeichnung={bezeichnung}")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "S000001"


def test_should_not_return_schema_for_wrong_author(
    runner: CommandRunner, client: TestClient
):
    Xdf2Factory().schema(fachlicher_ersteller="Ersteller").save(runner)

    response = client.get("/api/v1/schemas?status_gesetzt_durch=Wrong")

    assert response.status_code == 200
    assert response.json()["count"] == 0


@pytest.mark.parametrize(
    "gueltig_ab, gueltig_bis",
    [
        (None, None),
        (today, today + timedelta(days=1)),
        (None, today),
        (today, None),
    ],
)
def test_get_schema_by_gueltig_am(
    runner: CommandRunner,
    client: TestClient,
    gueltig_ab: date | None,
    gueltig_bis: date | None,
):
    gueltig_am = today.isoformat()

    factory = Xdf2Factory()
    factory.schema(gueltig_ab=gueltig_ab, gueltig_bis=gueltig_bis, id="S01").save(
        runner
    )
    factory.schema(
        gueltig_ab=today + timedelta(days=1),
        gueltig_bis=today + timedelta(days=2),
        id="S02",
    ).save(runner)

    response = client.get(f"/api/v1/schemas?gueltig_am={gueltig_am}")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "S01"


def test_get_schema_by_stichwort(runner: CommandRunner, client: TestClient):
    schema_stichwort = [xdf3.Stichwort(uri="testuri", value="testvalue")]

    Xdf3Factory().schema(stichwort=schema_stichwort).full().save(runner)

    response = client.get("/api/v1/schemas?stichwort=testuri::testvalue")

    assert response.status_code == 200

    items = response.json()["items"]

    assert items is not None
    assert len(items) == 1


def test_get_schema_by_wrong_stichwort(runner: CommandRunner, client: TestClient):
    Xdf3Factory().schema().full().save(runner)

    response = client.get("/api/v1/schemas?stichwort=testuri::testvalue")

    assert response.status_code == 200
    assert response.json()["count"] == 0


@pytest.mark.parametrize("search_term", ["Lorem", "Lor", "lor", "em", "ore", "loREM"])
def test_get_schema_by_different_bezug(
    runner: CommandRunner, client: TestClient, search_term: str
):
    factory = Xdf2Factory()
    factory.schema(id="S1234", version="1.1", bezug="Lorem").save(runner)
    factory.schema(id="S1234", version="1.0", bezug="Ipsum").save(runner)

    response = client.get(f"/api/v1/schemas?bezug={search_term}")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_version"] == "1.1"


@pytest.mark.parametrize("search_term", ["Lorem", "Lor", "lor", "em", "ore", "loREM"])
def test_get_schema_by_different_bezug_of_subelements(
    runner: CommandRunner, client: TestClient, search_term: str
):
    factory = Xdf2Factory()
    factory.schema(id="S1234", version="1.1", bezug="Lorem").save(runner)
    field = factory.field(bezug="Lorem").build()
    factory.schema(id="S001").with_field(field).save(runner)

    response = client.get(f"/api/v1/schemas?bezug_unterelemente={search_term}")

    assert response.status_code == 200
    assert len(response.json()["items"]) == 1
    assert response.json()["items"][0]["fim_id"] == "S001"


@pytest.mark.parametrize(
    "fts",
    [
        ("beschreibung", ["Beschreibung"]),
        ("VERSION", ["Versionshinweis"]),
        (
            "hilfe definition",
            ["Definition"],
        ),  # multiple searchterms are separated and connected with an and-relation
        ("S1234", ["S12345"]),
        ("'S1234", ["S12345"]),
    ],
)
def test_should_get_schemas_by_fts_query(
    runner: CommandRunner, client: TestClient, fts: tuple[str, list[str]]
):
    fts_query, fts_matches = fts
    path = XDF3_DATA / "schema_without_code_lists.xdf3.xml"
    runner.import_xdf3_file(str(path))

    response = client.get(f"/api/v1/schemas?fts_query={fts_query}")

    assert response.status_code == 200
    assert response.json()["count"] == 1

    assert response.json()["items"][0]["fim_id"] == "S12345"
    for match in fts_matches:
        assert f"[[[{match}]]]" in response.json()["items"][0]["fts_match"]


def test_should_filter_by_fts_query_handlungsgrundlage(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    schema = factory.schema(bezug="Match").save(runner)
    factory.schema(bezug="random").save(runner)

    response = client.get(
        "/api/v1/schemas?fts_query=match&suche_nur_in=Rechtsgrundlagen"
    )

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["fim_id"] == schema.fim_id


def test_should_filter_by_fts_query_status_gestetzt_durch(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    schema = factory.schema(fachlicher_ersteller="Match").save(runner)
    factory.schema(fachlicher_ersteller="random").save(runner)

    response = client.get(
        "/api/v1/schemas?fts_query=match&suche_nur_in=Status_gesetzt_durch"
    )

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["fim_id"] == schema.fim_id


def test_should_filter_by_fts_query_stichwort(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf3Factory()
    schema = factory.schema(stichwort=[xdf3.Stichwort("some-uri", "Match")]).save(
        runner
    )
    factory.schema(stichwort=[xdf3.Stichwort("some-uri", "random")]).save(runner)

    response = client.get("/api/v1/schemas?fts_query=match&suche_nur_in=Stichwort")

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["fim_id"] == schema.fim_id


def test_should_filter_by_fts_query_versionshinweis(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    schema = factory.schema(versionshinweis="Match").save(runner)
    factory.schema(versionshinweis="random").save(runner)

    response = client.get(
        "/api/v1/schemas?fts_query=match&suche_nur_in=Versionshinweis"
    )

    assert response.status_code == 200
    result = response.json()
    assert result["count"] == 1
    assert result["items"][0]["fim_id"] == schema.fim_id


def test_should_combine_fts_query_with_other_filters(
    runner: CommandRunner, client: TestClient
):
    path = XDF3_DATA / "schema_without_code_lists.xdf3.xml"
    runner.import_xdf3_file(str(path))

    factory = Xdf2Factory()
    factory.schema(name="Testname").save(
        runner, freigabe_status=xdf3.FreigabeStatus.ENTWURF
    )

    response = client.get(
        f"/api/v1/schemas?fts_query=testname&freigabe_status={xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD.value}"
    )

    assert response.status_code == 200
    assert response.json()["count"] == 0


def test_should_return_schemas_by_updated_since(
    runner: CommandRunner, client: TestClient
):
    early_date = "2023-10-17T00:00:00"
    late_date = "2023-10-18T00:00:00"

    factory = Xdf2Factory()

    with freeze_time(early_date):
        factory.schema("S123").save(runner)

    with freeze_time(late_date):
        factory.schema("S234").save(runner)

    response = client.get(f"/api/v1/schemas?updated_since={late_date}")
    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "S234"


def test_should_return_schema_filtered_by_status_gesetzt_seit(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    factory.schema(freigabedatum=date(2020, 11, 4), id="S001").save(runner)
    factory.schema(freigabedatum=date(2021, 11, 4), id="S002").save(runner)

    response = client.get(f"/api/v1/schemas?status_gesetzt_seit={date(2021, 1, 1)}")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "S002"


def test_should_return_schema_filtered_by_status_gesetzt_bis(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    factory.schema(freigabedatum=date(2020, 11, 4), id="S001").save(runner)
    factory.schema(freigabedatum=date(2021, 11, 4), id="S002").save(runner)

    response = client.get(f"/api/v1/schemas?status_gesetzt_bis={date(2021, 1, 1)}")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "S001"


def test_should_return_schema_filtered_by_status_gesetzt_am(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    factory.schema(freigabedatum=date(2020, 11, 4), id="S001").save(runner)
    factory.schema(freigabedatum=date(2021, 11, 4), id="S002").save(runner)
    factory.schema(freigabedatum=date(2022, 11, 4), id="S003").save(runner)

    response = client.get(
        f"/api/v1/schemas?status_gesetzt_seit={date(2021, 1, 1)}&status_gesetzt_bis={date(2022, 1, 1)}"
    )

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "S002"


def test_should_return_schema_filtered_by_xdf_version(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    factory.schema(id="S001").save(runner)

    Xdf3Factory().schema(id="S000002").full().save(runner)

    response = client.get("/api/v1/schemas?xdf_version=3.0.0")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == "S000002"


def test_should_not_return_schema_filtered_by_versionshinweis(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    factory.schema(versionshinweis="something").save(runner)

    response = client.get("/api/v1/schemas?Versionshinweis=different")

    assert response.status_code == 200
    assert response.json()["count"] == 0


def test_should_not_return_schema_filtered_by_versionshinweis_and_gueltig_am_filter(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf2Factory()
    factory.schema(versionshinweis="something").save(runner)

    response = client.get(
        "/api/v1/schemas?Versionshinweis=different&gueltig_am=2024-06-26"
    )

    assert response.status_code == 200
    assert response.json()["count"] == 0


def test_should_get_only_latest_schema_when_is_latest_is_true(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf3Factory()
    factory.schema(id="S12000", version="1.0.1").with_field().save(runner)
    factory.schema(id="S12000", version="1.0.2").with_field().save(runner)

    response = client.get("/api/v1/schemas?is_latest=true")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_version"] == "1.0.2"


def test_should_get_only_older_schemas_when_is_latest_is_false(
    runner: CommandRunner, client: TestClient
):
    factory = Xdf3Factory()
    factory.schema(id="S12000", version="1.0.1").with_field().save(runner)
    factory.schema(id="S12000", version="1.0.2").with_field().save(runner)
    factory.schema(id="S12000", version="1.0.3").with_field().save(runner)

    response = client.get("/api/v1/schemas?is_latest=false")

    assert response.status_code == 200
    assert response.json()["count"] == 2
    assert response.json()["items"][0]["fim_version"] in ("1.0.1", "1.0.2")
    assert response.json()["items"][1]["fim_version"] in ("1.0.1", "1.0.2")


def test_should_correctly_return_bezug_components_with_struktur_bezug_of_groups(
    runner: CommandRunner, client: TestClient
):
    """
    Test that a bug does not occur any more

    bezug_components used to be incomplete. The bezug of
    the ElementReferences of groups were not part of it.
    """
    factory = Xdf3Factory()
    field = factory.field(id="F12000").build()
    grandchild_group = (
        factory.group(id="G120001")
        .with_field(
            field,
            xdf3.Anzahl(1, 2),
            [xdf3.Rechtsbezug(parse_string_latin("Grandchild to Field"), link="link")],
        )
        .build()
    )
    child_group = (
        factory.group(id="G120002")
        .with_field(
            field,
            xdf3.Anzahl(1, 2),
            [xdf3.Rechtsbezug(parse_string_latin("Child to Grandchild"), link="link")],
        )
        .with_group(grandchild_group)
        .build()
    )
    xdf3_import = factory.schema("S12000").with_group(child_group).build_import()
    schema = xdf3_import.schema_message.schema

    runner.import_xdf3(xdf3_import)

    response = client.get("/api/v1/schemas")

    assert response.status_code == 200
    assert response.json()["count"] == 1
    assert response.json()["items"][0]["fim_id"] == schema.identifier.id
    assert response.json()["items"][0]["fim_version"] == schema.identifier.version

    assert response.json()["items"][0]["bezug_components"] == [
        "Child to Grandchild",
        "Grandchild to Field",
    ]


def test_results_by_multiple_freigabe_status(runner: CommandRunner, client: TestClient):
    for index, status in enumerate(FREIGABE_STATUS_VALID_FOR_UPLOAD):
        Xdf3Factory().schema(
            id=f"S00000{index+1}", freigabe_status=status
        ).with_field().save(runner)

    response = client.get("/api/v1/schemas?freigabe_status=5&freigabe_status=6")

    assert response.status_code == 200
    assert response.json()["count"] == 2
    assert response.json()["items"][0]["fim_id"] == "S000003"
    assert response.json()["items"][1]["fim_id"] == "S000004"


def test_order_results_by_id(runner: CommandRunner, client: TestClient):
    for index in range(3):
        Xdf3Factory().schema(id=f"S00000{index+1}").with_field().save(runner)

    response = client.get("/api/v1/schemas?order_by=id_asc")

    assert response.status_code == 200
    assert response.json()["count"] == 3
    assert response.json()["items"][0]["fim_id"] == "S000001"
    assert response.json()["items"][1]["fim_id"] == "S000002"
    assert response.json()["items"][2]["fim_id"] == "S000003"

    response = client.get("/api/v1/schemas?order_by=id_desc")

    assert response.status_code == 200
    assert response.json()["count"] == 3
    assert response.json()["items"][0]["fim_id"] == "S000003"
    assert response.json()["items"][1]["fim_id"] == "S000002"
    assert response.json()["items"][2]["fim_id"] == "S000001"


def test_order_results_by_name(runner: CommandRunner, client: TestClient):
    factory = Xdf3Factory()
    factory.schema(id="S000001", name="First").with_field().save(runner)
    factory.schema(id="S000002", name="Second").with_field().save(runner)
    factory.schema(id="S000003", name="Last").with_field().save(runner)

    response = client.get("/api/v1/schemas?order_by=name_asc")

    assert response.status_code == 200
    assert response.json()["count"] == 3
    assert response.json()["items"][0]["fim_id"] == "S000001"
    assert response.json()["items"][1]["fim_id"] == "S000003"
    assert response.json()["items"][2]["fim_id"] == "S000002"

    response = client.get("/api/v1/schemas?order_by=name_desc")

    assert response.status_code == 200
    assert response.json()["count"] == 3
    assert response.json()["items"][0]["fim_id"] == "S000002"
    assert response.json()["items"][1]["fim_id"] == "S000003"
    assert response.json()["items"][2]["fim_id"] == "S000001"


def test_order_results_by_changed_at(runner: CommandRunner, client: TestClient):
    now = utc_now()

    factory = Xdf3Factory()
    factory.schema(id="S000001", letzte_aenderung=now, name="NameB").with_field().save(
        runner
    )
    factory.schema(
        id="S000002", letzte_aenderung=now - timedelta(days=1), name="NameC"
    ).with_field().save(runner)
    factory.schema(id="S000003", letzte_aenderung=now, name="NameA").with_field().save(
        runner
    )

    response = client.get("/api/v1/schemas?order_by=geaendert_datum_zeit_asc")

    assert response.status_code == 200
    assert response.json()["count"] == 3
    assert response.json()["items"][0]["fim_id"] == "S000002"
    assert response.json()["items"][1]["fim_id"] == "S000003"
    assert response.json()["items"][2]["fim_id"] == "S000001"

    response = client.get("/api/v1/schemas?order_by=geaendert_datum_zeit_desc")

    assert response.status_code == 200
    assert response.json()["count"] == 3
    assert response.json()["items"][0]["fim_id"] == "S000003"
    assert response.json()["items"][1]["fim_id"] == "S000001"
    assert response.json()["items"][2]["fim_id"] == "S000002"
