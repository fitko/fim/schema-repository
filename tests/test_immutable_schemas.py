from fastapi.testclient import TestClient

from tests.conftest import CommandRunner
from tests.data import XDF2_DATA
from tests.factories.xdf3 import Xdf3Factory


def test_should_return_the_original_xdf2_file(
    runner: CommandRunner, client: TestClient
):
    schema = runner.import_xdf2_file(XDF2_DATA / "S1234V1.0.xml")
    upload = schema.uploads[0]

    response = client.get(upload.url)

    assert response.status_code == 200
    assert response.headers["content-type"] == "application/xml"
    assert (
        response.headers["content-disposition"]
        == f'attachment; filename="{upload.filename}"'
    )

    with open(XDF2_DATA / "S1234V1.0.xml", "rb") as file:
        assert response.read() == file.read()


def test_should_return_the_original_xdf3_file(
    runner: CommandRunner, client: TestClient
):
    xdf3_import = Xdf3Factory().schema().full().build_import()
    schema = runner.import_xdf3(xdf3_import)
    upload = schema.uploads[0]

    response = client.get(upload.url)

    assert response.status_code == 200
    assert response.headers["content-type"] == "application/xml"
    assert (
        response.headers["content-disposition"]
        == f'attachment; filename="{upload.filename}"'
    )

    assert response.read() == xdf3_import.schema_text.encode("utf-8")


def test_should_return_the_generated_json_schema_file(
    immutable_base_url: str, runner: CommandRunner, client: TestClient
):
    schema = runner.import_xdf2_file(XDF2_DATA / "S1234V1.0.xml")
    file = schema.json_schema_files[0]

    response = client.get(file.url)

    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    assert (
        response.headers["content-disposition"]
        == f'attachment; filename="{file.filename}"'
    )

    # Should set the correct schema id
    content = response.json()
    assert content["$id"] == f"{immutable_base_url}/immutable/schemas/{file.filename}"


def test_should_return_the_generated_xsd_file(
    runner: CommandRunner, client: TestClient
):
    schema = runner.import_xdf2_file(XDF2_DATA / "S1234V1.0.xml")
    file = schema.xsd_files[0]

    response = client.get(file.url)

    assert response.status_code == 200
    assert response.headers["content-type"] == "application/xml"
    assert (
        response.headers["content-disposition"]
        == f'attachment; filename="{file.filename}"'
    )


def test_should_fail_for_unknown_filename(client: TestClient):
    response = client.get("/immutable/schemas/unknown.xsd")

    assert response.status_code == 404
    assert response.json() == {"detail": "Could not find file unknown.xsd"}
