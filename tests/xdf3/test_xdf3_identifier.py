import pytest
from fimportal.xdatenfelder import xdf3
from fimportal.xdatenfelder.common import InternalParserException


def test_should_extract_the_nummernkreis():
    identifier = xdf3.Identifier(id="S12345", version=xdf3.parse_version("1.0.0"))

    assert identifier.nummernkreis == "12345"


def test_should_fail_for_short_id():
    with pytest.raises(InternalParserException):
        xdf3.Identifier(id="S1234", version=xdf3.parse_version("1.0.0"))


def test_should_fail_for_non_int_values():
    with pytest.raises(InternalParserException):
        xdf3.Identifier(id="SSSSSS", version=xdf3.parse_version("1.0.0"))
