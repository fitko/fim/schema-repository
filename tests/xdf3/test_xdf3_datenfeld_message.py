import pytest

from fimportal.xdatenfelder.xdf3.datenfeld_message import (
    DatenfeldMessage,
    parse_datenfeld_message,
    serialize_datenfeld_message,
)
from tests.factories.xdf3 import Xdf3Factory


@pytest.mark.parametrize(
    "message",
    [
        Xdf3Factory().field().minimal().message(),
        Xdf3Factory().field().full().message(),
    ],
)
def test_serialize_and_parse_message(message: DatenfeldMessage):
    xml = serialize_datenfeld_message(message)
    parsed_message = parse_datenfeld_message(xml)

    assert parsed_message == message
