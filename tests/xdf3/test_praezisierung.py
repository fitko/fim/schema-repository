import pytest

from fimportal.xdatenfelder.xdf3 import (
    Praezisierung,
    InvalidPraezisierungException,
)


def test_should_create_a_valid_praezisierung():
    Praezisierung(
        min_length=0,
        max_length=1,
        min_value="0",
        max_value="1",
        pattern=None,
    )


def test_should_fail_for_negative_min_length():
    with pytest.raises(InvalidPraezisierungException, match="Invalid min_length: -1"):
        Praezisierung(
            min_length=-1,
            max_length=None,
            min_value=None,
            max_value=None,
            pattern=None,
        )


def test_should_fail_for_non_positive_max_length():
    with pytest.raises(InvalidPraezisierungException, match="Invalid max_length: 0"):
        Praezisierung(
            min_length=None,
            max_length=0,
            min_value=None,
            max_value=None,
            pattern=None,
        )
