import pytest
from datetime import date, datetime, timezone
import re

from fimportal import genericode
from fimportal.din91379 import parse_string_latin
from fimportal.xdatenfelder import xdf3
from tests.data import XDF3_DATA


def test_should_parse_the_message_header():
    message = xdf3.load_schema_message(XDF3_DATA / "schema.xml")

    assert message.header == xdf3.MessageHeader(
        nachricht_id="abcd1234",
        erstellungs_zeitpunkt=datetime(2020, 9, 1, tzinfo=timezone.utc),
    )


def test_should_parse_the_schema():
    message = xdf3.load_schema_message(XDF3_DATA / "schema.xml")

    assert message.schema == xdf3.Schema(
        identifier=xdf3.Identifier(
            "S12345",
            version=xdf3.parse_version("1.0.0"),
        ),
        name=parse_string_latin("Test"),
        beschreibung=parse_string_latin("Eine Beschreibung"),
        definition=parse_string_latin("Eine Definition"),
        bezug=[
            xdf3.Rechtsbezug(
                parse_string_latin("Bezug"),
                link="some link",
            ),
        ],
        freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
        status_gesetzt_am=date(2023, 1, 1),
        status_gesetzt_durch=parse_string_latin("Test"),
        gueltig_ab=None,
        gueltig_bis=None,
        versionshinweis=parse_string_latin("Ein Versionshinweis"),
        veroeffentlichungsdatum=date(2023, 1, 1),
        letzte_aenderung=datetime(
            2020, 9, 1, hour=0, minute=0, second=0, tzinfo=timezone.utc
        ),
        relation=[
            xdf3.Relation(
                praedikat=xdf3.RelationTyp.ERSETZT,
                objekt=xdf3.Identifier("E00001", version=xdf3.parse_version("1.0.0")),
            )
        ],
        stichwort=[xdf3.Stichwort(uri="urn:xoev-de:beispiel", value="Stichwort")],
        bezeichnung=parse_string_latin("Eine Bezeichnung"),
        hilfetext=parse_string_latin("Ein Hilfetext"),
        ableitungsmodifikationen_struktur=xdf3.AbleitungsmodifikationenStruktur.ALLES_MODIFIZIERBAR,
        ableitungsmodifikationen_repraesentation=xdf3.AbleitungsmodifikationenRepraesentation.MODIFIZIERBAR,
        dokumentsteckbrief=xdf3.Identifier("D12345", xdf3.parse_version("1.0.0")),
        regeln=[xdf3.Identifier("R00001", version=xdf3.parse_version("1.2.0"))],
        struktur=[
            xdf3.ElementReference(
                anzahl=xdf3.Anzahl(1, 1),
                bezug=[
                    xdf3.Rechtsbezug(
                        parse_string_latin("Ein Rechtsbezug"),
                        link=None,
                    )
                ],
                element_type=xdf3.ElementType.GRUPPE,
                identifier=xdf3.Identifier("G00001", xdf3.parse_version("1.3.0")),
            ),
        ],
    )

    assert message.groups == {
        xdf3.Identifier("G00001", xdf3.parse_version("1.3.0")): xdf3.Gruppe(
            identifier=xdf3.Identifier(
                "G00001",
                version=xdf3.parse_version("1.3.0"),
            ),
            name=parse_string_latin("Natürliche Person"),
            beschreibung=parse_string_latin("Eine Beschreibung"),
            definition=parse_string_latin("Eine Definition"),
            bezug=[
                xdf3.Rechtsbezug(
                    parse_string_latin("Bezug"),
                    link="some link",
                ),
            ],
            freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
            status_gesetzt_am=date(2023, 1, 1),
            status_gesetzt_durch=parse_string_latin("Test"),
            gueltig_ab=None,
            gueltig_bis=None,
            versionshinweis=parse_string_latin("Ein Versionshinweis"),
            veroeffentlichungsdatum=date(2023, 1, 1),
            letzte_aenderung=datetime(
                2020, 9, 1, hour=0, minute=0, second=0, tzinfo=timezone.utc
            ),
            relation=[
                xdf3.Relation(
                    praedikat=xdf3.RelationTyp.ERSETZT,
                    objekt=xdf3.Identifier(
                        "E00001", version=xdf3.parse_version("1.0.0")
                    ),
                )
            ],
            stichwort=[xdf3.Stichwort(uri="urn:xoev-de:beispiel", value="Stichwort")],
            bezeichnung_eingabe=parse_string_latin("Natürliche Person"),
            bezeichnung_ausgabe=parse_string_latin("Natürliche Person"),
            schema_element_art=xdf3.SchemaElementArt.ABSTRAKT,
            hilfetext_eingabe=parse_string_latin("Hilfetext Eingabe"),
            hilfetext_ausgabe=parse_string_latin("Hilfetext Ausgabe"),
            art=xdf3.Gruppenart.AUSWAHL,
            regeln=[xdf3.Identifier("R00001", version=xdf3.parse_version("1.2.0"))],
            struktur=[
                xdf3.ElementReference(
                    anzahl=xdf3.Anzahl(1, 1),
                    bezug=[
                        xdf3.Rechtsbezug(
                            parse_string_latin("Ein Rechtsbezug"),
                            link=None,
                        )
                    ],
                    element_type=xdf3.ElementType.FELD,
                    identifier=xdf3.Identifier("F00001", xdf3.parse_version("1.1.0")),
                ),
            ],
        )
    }

    assert message.fields == {
        xdf3.Identifier("F00001", xdf3.parse_version("1.1.0")): xdf3.Datenfeld(
            identifier=xdf3.Identifier(
                "F00001",
                version=xdf3.parse_version("1.1.0"),
            ),
            name=parse_string_latin("Familienname"),
            beschreibung=parse_string_latin("Eine Beschreibung"),
            definition=parse_string_latin("Eine Definition"),
            bezug=[
                xdf3.Rechtsbezug(
                    parse_string_latin("Bezug"),
                    link="some link",
                ),
            ],
            freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
            status_gesetzt_am=date(2023, 1, 1),
            status_gesetzt_durch=parse_string_latin("Test"),
            gueltig_ab=None,
            gueltig_bis=None,
            versionshinweis=parse_string_latin("Ein Versionshinweis"),
            veroeffentlichungsdatum=date(2023, 1, 1),
            letzte_aenderung=datetime(
                2020, 9, 1, hour=0, minute=0, second=0, tzinfo=timezone.utc
            ),
            relation=[
                xdf3.Relation(
                    praedikat=xdf3.RelationTyp.ERSETZT,
                    objekt=xdf3.Identifier(
                        "E00001", version=xdf3.parse_version("1.0.0")
                    ),
                )
            ],
            stichwort=[xdf3.Stichwort(uri="urn:xoev-de:beispiel", value="Stichwort")],
            bezeichnung_eingabe=parse_string_latin("Familienname"),
            bezeichnung_ausgabe=parse_string_latin("Familienname"),
            schema_element_art=xdf3.SchemaElementArt.HARMONISIERT,
            hilfetext_eingabe=parse_string_latin("Hilfe Eingabe"),
            hilfetext_ausgabe=parse_string_latin("Hilfe Ausgabe"),
            feldart=xdf3.Feldart.EINGABE,
            datentyp=xdf3.Datentyp.TEXT,
            praezisierung=xdf3.Praezisierung(
                min_length=0,
                max_length=1,
                min_value="0",
                max_value="1",
                pattern="test",
            ),
            inhalt=parse_string_latin("Ein Inhalt"),
            vorbefuellung=xdf3.Vorbefuellung.KEINE,
            werte=None,
            codeliste_referenz=None,
            code_key=None,
            name_key=None,
            help_key=None,
            regeln=[xdf3.Identifier("R00001", version=xdf3.parse_version("1.2.0"))],
            max_size=None,
            media_type=[],
        )
    }

    assert message.rules == {
        xdf3.Identifier("R00001", version=xdf3.parse_version("1.2.0")): xdf3.Regel(
            identifier=xdf3.Identifier("R00001", version=xdf3.parse_version("1.2.0")),
            name="MindestEineAngabe",
            beschreibung="Eine Beschreibung",
            freitext_regel="Freitext",
            bezug=[
                xdf3.Rechtsbezug(text=parse_string_latin("Bezug"), link="some link")
            ],
            stichwort=[xdf3.Stichwort(uri="urn:xoev-de:beispiel", value="Stichwort")],
            fachlicher_ersteller="Bundesredaktion",
            letzte_aenderung=datetime(
                2020, 9, 1, hour=0, minute=0, second=0, tzinfo=timezone.utc
            ),
            typ=xdf3.Regeltyp.KOMPLEX,
            param=[],
            ziel=[],
            skript="function script() {}",
            fehler=[],
        )
    }


def test_should_parse_all_rule_attributes():
    message = xdf3.load_schema_message(XDF3_DATA / "schema_with_full_rule.xml")

    rule = message.rules[xdf3.Identifier("R00001", version=xdf3.parse_version("1.0.0"))]

    assert rule == xdf3.Regel(
        identifier=xdf3.Identifier("R00001", version=xdf3.parse_version("1.0.0")),
        name="MindestEineAngabe",
        beschreibung="Eine Beschreibung",
        freitext_regel="Freitext",
        bezug=[xdf3.Rechtsbezug(text=parse_string_latin("Bezug"), link="some link")],
        stichwort=[xdf3.Stichwort(uri="urn:xoev-de:beispiel", value="Stichwort")],
        fachlicher_ersteller="Bundesredaktion",
        letzte_aenderung=datetime(
            2020, 9, 1, hour=0, minute=0, second=0, tzinfo=timezone.utc
        ),
        typ=xdf3.Regeltyp.KOMPLEX,
        param=[
            xdf3.RegelParameter(
                element="element",
                name="name",
                typ=xdf3.RegelParamTyp.WERT,
            ),
        ],
        ziel=[
            xdf3.RegelZiel(
                name="name",
                element="element",
            ),
        ],
        skript="function script() {}",
        fehler=[
            xdf3.Fehlermeldung(code=10, message="Eine Fehlermeldung"),
        ],
    )


def test_should_parse_file_inputs():
    message = xdf3.load_schema_message(XDF3_DATA / "schema_file_input.xml")

    field = message.fields[
        xdf3.Identifier("F00001", version=xdf3.parse_version("1.1.0"))
    ]

    assert field.datentyp == xdf3.Datentyp.ANLAGE
    assert field.max_size == 10
    assert field.media_type == ["application/xml", "application/json"]


def test_should_parse_code_lists():
    message = xdf3.load_schema_message(XDF3_DATA / "schema_with_code_list.xml")

    assert message.get_code_list_identifiers() == {
        genericode.Identifier(
            canonical_uri="urn:de:code_list_a",
            version="1",
            canonical_version_uri="urn:de:code_list_a_1",
        )
    }


def test_should_parse_code_columns():
    message = xdf3.load_schema_message(XDF3_DATA / "schema_with_code_list.xml")

    field = message.fields[xdf3.Identifier("F00001", xdf3.parse_version("1.1.0"))]

    assert field.code_key == "code key"
    assert field.name_key == "name key"
    assert field.help_key == "help key"


def test_should_parse_embedded_code_list_values():
    message = xdf3.load_schema_message(XDF3_DATA / "schema_with_embedded_code_list.xml")

    field = message.fields[xdf3.Identifier("F00001", xdf3.parse_version("1.0.0"))]

    assert field.werte == [
        xdf3.ListenWert(code="key_1", name="Key 1", hilfe="Some help"),
        xdf3.ListenWert(code="key_2", name="Key 2", hilfe=None),
    ]


def test_should_fail_for_empty_embedded_code_list():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Cannot parse schema: Element '{urn:xoev-de:fim:standard:xdatenfelder_3.0.0}werte': Missing child element(s). Expected is ( {urn:xoev-de:fim:standard:xdatenfelder_3.0.0}wert )., line 103"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_with_empty_embedded_code_list.xml")


def test_should_fail_for_duplicate_code_list_value():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Could not parse schema: Duplicate key `key_1` in embedded code list, line 108"
        ),
    ):
        xdf3.load_schema_message(
            XDF3_DATA / "schema_with_duplicate_embedded_list_item.xml"
        )


def test_should_fail_for_code_list_reference_and_embedded_code_list():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Could not parse schema: Datenfeld cannot include both a code list reference and an embedded code list, line 68"
        ),
    ):
        xdf3.load_schema_message(
            XDF3_DATA / "schema_with_code_list_and_embedded_code_list.xml"
        )


def test_should_fail_for_invalid_feldart():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Cannot parse schema: Element 'code': [facet 'enumeration'] The value 'invalid' is not an element of the set {'hidden', 'input', 'label', 'locked', 'select'}., line 108"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_invalid_feldart.xdf3.xml")


def test_should_fail_for_invalid_datentyp():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Cannot parse schema: Element 'code': [facet 'enumeration'] The value 'invalid' is not an element of the set {'bool', 'date', 'datetime', 'file', 'num', 'num_currency', 'num_int', 'obj', 'text', 'text_latin', 'time'}., line 111"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_invalid_datentyp.xdf3.xml")


def test_should_fail_for_invalid_datetime():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Cannot parse schema: Element '{urn:xoev-de:fim:standard:xdatenfelder_3.0.0}letzteAenderung': 'invalid' is not a valid value of the atomic type 'xs:dateTime'., line 23"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_invalid_datetime.xdf3.xml")


def test_should_fail_for_invalid_date():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Cannot parse schema: Element '{urn:xoev-de:fim:standard:xdatenfelder_3.0.0}veroeffentlichungsdatum': 'invalid' is not a valid value of the atomic type 'xs:date'., line 22"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_invalid_date.xdf3.xml")


def test_should_fail_for_invalid_schemaelementart():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Cannot parse schema: Element 'code': [facet 'enumeration'] The value 'invalid' is not an element of the set {'ABS', 'HAR', 'RNG'}., line 75"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_invalid_schemaelementart.xdf3.xml")


def test_should_fail_for_invalid_vorbefuellung():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Cannot parse schema: Element 'code': [facet 'enumeration'] The value 'invalid' is not an element of the set {'keine', 'optional', 'verpflichtend'}., line 101"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_invalid_vorbefuellung.xdf3.xml")


def test_should_fail_for_invalid_ableitungsmodifikationen_struktur():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Cannot parse schema: Element 'code': [facet 'enumeration'] The value 'invalid' is not an element of the set {'0', '1', '2', '3'}., line 27"
        ),
    ):
        xdf3.load_schema_message(
            XDF3_DATA / "schema_invalid_ableitungsmodifikationen_struktur.xdf3.xml"
        )


def test_should_fail_for_invalid_ableitungsmodifikationen_repraesentation():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Cannot parse schema: Element 'code': [facet 'enumeration'] The value 'invalid' is not an element of the set {'0', '1'}., line 30"
        ),
    ):
        xdf3.load_schema_message(
            XDF3_DATA
            / "schema_invalid_ableitungsmodifikationen_repraesentation.xdf3.xml"
        )


def test_should_fail_for_invalid_freigabe_status():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Could not parse schema: Invalid FreigabeStatus 'invalid', line 77"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_invalid_freigabe_status.xdf3.xml")


def test_should_fail_for_empty_struktur():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Cannot parse schema: Element '{urn:xoev-de:fim:standard:xdatenfelder_3.0.0}stammdatenschema': Missing child element(s). Expected is one of ( {urn:xoev-de:fim:standard:xdatenfelder_3.0.0}regel, {urn:xoev-de:fim:standard:xdatenfelder_3.0.0}struktur )., line 7"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_empty_struktur.xdf3.xml")


def test_should_fail_for_media_type_in_text_input():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Could not parse schema: maxSize or mediaType must only be set for file inputs, line 68"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_text_input_with_media_type.xml")


def test_should_fail_for_max_size_in_text_input():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Could not parse schema: maxSize or mediaType must only be set for file inputs, line 68"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_text_input_with_max_size.xml")


def test_should_fail_for_invalid_relation():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape("Could not parse schema: Invalid Relation 'invalid', line 25"),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_invalid_relation.xml")


def test_should_fail_for_invalid_gruppenart():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Cannot parse schema: Element 'code': [facet 'enumeration'] The value 'invalid' is not an element of the set {'X'}., line 85"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_invalid_gruppenart.xml")


def test_should_fail_for_missing_regelziel_name():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Cannot parse schema: Element '{urn:xoev-de:fim:standard:xdatenfelder_3.0.0}ziel': The attribute 'name' is required but missing., line 61"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_missing_regelziel_name.xml")


def test_should_fail_for_missing_regelziel_element():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Cannot parse schema: Element '{urn:xoev-de:fim:standard:xdatenfelder_3.0.0}ziel': The attribute 'element' is required but missing., line 61"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_missing_regelziel_element.xml")


def test_should_fail_for_missing_regelparam_name():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Cannot parse schema: Element '{urn:xoev-de:fim:standard:xdatenfelder_3.0.0}param': The attribute 'name' is required but missing., line 61"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_missing_regelparam_name.xml")


def test_should_fail_for_missing_regelparam_element():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Cannot parse schema: Element '{urn:xoev-de:fim:standard:xdatenfelder_3.0.0}param': The attribute 'element' is required but missing., line 61"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_missing_regelparam_element.xml")


def test_should_fail_for_invalid_regelparam_typ():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Cannot parse schema: Element '{urn:xoev-de:fim:standard:xdatenfelder_3.0.0}typ': [facet 'enumeration'] The value 'invalid' is not an element of the set {'anzahl', 'objekt', 'selektor', 'wert'}., line 62"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_invalid_regelparam_typ.xml")


def test_should_fail_for_missing_fehlercode():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Cannot parse schema: Element '{urn:xoev-de:fim:standard:xdatenfelder_3.0.0}fehler': The attribute 'code' is required but missing., line 66"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_missing_fehlercode.xml")


def test_should_fail_for_invalid_fehlercode():
    with pytest.raises(
        xdf3.XdfException,
        match=re.escape(
            "Cannot parse schema: Element '{urn:xoev-de:fim:standard:xdatenfelder_3.0.0}fehler', attribute 'code': 'invalid' is not a valid value of the atomic type 'xs:int'., line 66"
        ),
    ):
        xdf3.load_schema_message(XDF3_DATA / "schema_invalid_fehlercode.xml")
