from fimportal.xdatenfelder.xdf3 import FreigabeStatus


def test_freigabestatus_string_conversion():
    for status in FreigabeStatus:
        label = status.to_label()
        assert FreigabeStatus.from_label(label) == status
