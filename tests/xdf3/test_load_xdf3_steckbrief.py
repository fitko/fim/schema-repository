from datetime import date, datetime, timezone

from fimportal.common import FreigabeStatus
from fimportal.din91379 import parse_string_latin
from fimportal.xdatenfelder.xdf3.common import (
    Dokumentart,
    Identifier,
    Rechtsbezug,
    Relation,
    RelationTyp,
    Stichwort,
    Version,
    parse_version,
)
from fimportal.xdatenfelder.xdf3.steckbrief_message import (
    Steckbrief,
    load_steckbrief_message,
)
from tests.data import XDF3_DATA


def test_should_parse_steckbrief():
    steckbrief_message = load_steckbrief_message(XDF3_DATA / "steckbrief.xdf3.xml")

    assert steckbrief_message.steckbrief == Steckbrief(
        identifier=Identifier("D02000000001", version=Version("1.0.0")),
        beschreibung=parse_string_latin("Beschreibung des Dokumentensteckbriefs"),
        name=parse_string_latin("abstrakter Dokumentsteckbrief"),
        definition=parse_string_latin("Definition des Berichts"),
        freigabe_status=FreigabeStatus.IN_BEARBEITUNG,
        status_gesetzt_durch=parse_string_latin("Feldertest"),
        versionshinweis=parse_string_latin(
            "Version erstellt ohne Angabe eines Versionshinweises."
        ),
        letzte_aenderung=datetime(2024, 6, 27, 13, 40, 20, tzinfo=timezone.utc),
        gueltig_ab=date(year=2019, month=9, day=1),
        gueltig_bis=date(year=2020, month=9, day=1),
        veroeffentlichungsdatum=date(year=2019, month=8, day=27),
        stichwort=[
            Stichwort(uri="Anwendungsgebiet", value="Freie und Hansestadt Hamburg")
        ],
        ist_abstrakt=True,
        dokumentart=Dokumentart.BERICHT,
        bezeichnung=parse_string_latin("abstrakter Dokumentsteckbrief"),
        status_gesetzt_am=date(year=2019, month=8, day=27),
        bezug=[
            Rechtsbezug(
                parse_string_latin("Bezug"),
                link="some link",
            ),
        ],
        relation=[
            Relation(
                praedikat=RelationTyp.ERSETZT,
                objekt=Identifier("E00001", version=parse_version("1.0.0")),
            )
        ],
        hilfetext=parse_string_latin("Ein Hilfetext"),
    )
