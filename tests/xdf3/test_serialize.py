import pytest

from fimportal.xdatenfelder import xdf3
from tests.factories.genericode import CodeListFactory
from tests.factories.xdf3 import Xdf3Factory, Xdf3FieldFactory


def test_should_serialize_a_minimal_message():
    factory = Xdf3Factory()
    message = factory.schema().with_field(factory.field().build()).message()

    content = xdf3.serialize_schema_message(message)

    assert xdf3.parse_schema_message(content) == message


def test_should_serialize_a_full_message():
    factory = Xdf3Factory()
    rule = factory.rule().full().build()
    field = factory.field().with_rule(rule).full().build()
    group = factory.group().with_rule(rule).with_field(field).full().build()
    message = (
        factory.schema()
        .with_rule(rule)
        .with_group(group)
        .with_field(field)
        .full()
        .message()
    )

    content = xdf3.serialize_schema_message(message)

    assert xdf3.parse_schema_message(content) == message


@pytest.mark.parametrize(
    "field_factory",
    [
        Xdf3Factory().field().minimal(),
        Xdf3Factory()
        .field()
        .input(
            xdf3.Datentyp.TEXT,
            praezisierung=xdf3.Praezisierung(
                min_value="0",
                max_value="10",
                min_length=1,
                max_length=99,
                pattern="pattern",
            ),
        ),
        Xdf3Factory()
        .field()
        .select(
            werte=[
                xdf3.ListenWert(
                    code="0",
                    name="Eintrag 1",
                    hilfe="Der erste Eintrag",
                )
            ]
        ),
        Xdf3Factory()
        .field()
        .select(
            code_list=CodeListFactory().build(),
            code_key="code",
            name_key="name",
            help_key="help",
        ),
        Xdf3Factory()
        .field()
        .input(
            xdf3.Datentyp.ANLAGE,
            max_size=10,
            media_type=["application/json", "application/xml"],
        ),
    ],
)
def test_should_serialize_fields(field_factory: Xdf3FieldFactory):
    factory = field_factory.factory
    field = field_factory.build()

    message = factory.schema().with_field(field).message()

    content = xdf3.serialize_schema_message(message)

    assert xdf3.parse_schema_message(content) == message


def test_should_serialize_full_groups():
    factory = Xdf3Factory()
    dummy_field = factory.field().build()
    dummy_rule = factory.rule().build()
    group = factory.group().with_field(dummy_field).with_rule(dummy_rule).full().build()
    message = factory.schema().with_group(group).message()

    content = xdf3.serialize_schema_message(message)

    assert xdf3.parse_schema_message(content) == message


def test_should_serialize_minimal_groups():
    factory = Xdf3Factory()
    dummy_field = factory.field().build()
    group = factory.group().minimal().with_field(dummy_field).build()
    message = factory.schema().with_group(group).message()

    content = xdf3.serialize_schema_message(message)

    assert xdf3.parse_schema_message(content) == message
