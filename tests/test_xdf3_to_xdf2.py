import json

import pytest

from fimportal import genericode
from fimportal.din91379 import parse_string_latin
from fimportal.xdatenfelder import xdf2, xdf3, xdf3_to_xdf2
from fimportal.xdatenfelder.common import ConversionException
from tests.factories.xdf3 import (
    Xdf3Factory,
)


class TestMessage:
    def test_should_convert_a_full_message(self):
        factory = Xdf3Factory()
        xdf3_rule = factory.rule(id="R1200034", version="1.0.0").build()
        xdf3_field = factory.field(id="F1200034", version="1.0.0").build()
        xdf3_message = (
            factory.schema(
                id="S1200034",
                version="1.0.0",
                name="Name",
                definition="Definition",
                beschreibung="Beschreibung",
                hilfetext="Hilfe",
                bezeichnung="Bezeichnung",
                status_gesetzt_durch="Ersteller",
            )
            .with_rule(xdf3_rule)
            .with_field(xdf3_field, anzahl=xdf3.Anzahl(min=0, max=1), bezug=[])
            .with_bezug("Rechtsbezug 1")
            .with_bezug("Rechtsbezug 2")
            .message()
        )

        result = xdf3_to_xdf2.convert_message(xdf3_message)

        assert result.message.header == xdf2.MessageHeader(
            nachricht_id=xdf3_message.header.nachricht_id,
            erstellungs_zeitpunkt=xdf3_message.header.erstellungs_zeitpunkt,
            referenz_id=None,
        )

        assert result.message.schema == xdf2.Schema(
            identifier=xdf2.Identifier("S1234", version=xdf2.parse_version("1.0")),
            name="Name",
            bezeichnung_eingabe="Bezeichnung",
            bezeichnung_ausgabe=None,
            beschreibung="Beschreibung",
            definition="Definition",
            bezug="Rechtsbezug 1;Rechtsbezug 2",
            status=xdf2.Status.AKTIV,
            versionshinweis=None,
            gueltig_ab=None,
            gueltig_bis=None,
            fachlicher_ersteller="Ersteller",
            freigabedatum=None,
            veroeffentlichungsdatum=None,
            hilfetext=parse_string_latin("Hilfe"),
            ableitungsmodifikationen_struktur=xdf3_message.schema.ableitungsmodifikationen_struktur,
            ableitungsmodifikationen_repraesentation=xdf3_message.schema.ableitungsmodifikationen_repraesentation,
            regeln=[
                xdf2.Identifier("R1234", version=xdf2.parse_version("1.0")),
            ],
            struktur=[
                xdf2.ElementReference(
                    anzahl=xdf2.Anzahl(min=0, max=1),
                    bezug=None,
                    element_type=xdf2.ElementType.FELD,
                    identifier=xdf2.Identifier(
                        "F1234", version=xdf2.parse_version("1.0")
                    ),
                )
            ],
        )

    def test_should_include_fields(self):
        factory = Xdf3Factory()
        xdf3_field = factory.field().build()
        xdf3_message = factory.schema().with_field(xdf3_field).message()

        result = xdf3_to_xdf2.convert_message(xdf3_message)

        assert len(result.message.fields) == 1

    def test_should_include_groups(self):
        factory = Xdf3Factory()
        xdf3_group = factory.group().full().build()
        xdf3_message = factory.schema().with_group(xdf3_group).message()

        result = xdf3_to_xdf2.convert_message(xdf3_message)

        assert len(result.message.groups) == 1

    def test_should_include_rules(self):
        factory = Xdf3Factory()
        xdf3_rule = factory.rule().build()
        xdf3_message = factory.schema().with_rule(xdf3_rule).message()

        result = xdf3_to_xdf2.convert_message(xdf3_message)

        assert len(result.message.rules) == 1

    def test_should_return_created_code_lists(self):
        factory = Xdf3Factory()
        xdf3_field = (
            factory.field()
            .select(werte=[xdf3.ListenWert("A", "Label", hilfe=None)])
            .build()
        )
        xdf3_message = factory.schema().with_field(xdf3_field).message()

        result = xdf3_to_xdf2.convert_message(xdf3_message)

        assert len(result.code_lists) == 1


class TestIdentifier:
    @pytest.mark.parametrize(
        "xdf3_identifier, xdf2_identifier",
        [
            (
                xdf3.Identifier("S1200034", xdf3.parse_version("1.0.0")),
                xdf2.Identifier("S1234", xdf2.parse_version("1.0")),
            ),
            (
                xdf3.Identifier("S1200034", xdf3.parse_version("1.2.3")),
                xdf2.Identifier("S1234", xdf2.parse_version("1.002003")),
            ),
        ],
    )
    def test_should_map_between_identifiers(
        self, xdf3_identifier: xdf3.Identifier, xdf2_identifier: xdf2.Identifier
    ):
        assert xdf3_to_xdf2.convert_identifier(xdf3_identifier) == xdf2_identifier

    def test_should_fail_for_large_minor_version(self):
        identifier = xdf3.Identifier("S1200034", xdf3.parse_version("1.1234.1"))
        with pytest.raises(ConversionException):
            xdf3_to_xdf2.convert_identifier(identifier)

    def test_should_fail_for_large_patch_version(self):
        identifier = xdf3.Identifier("S1200034", xdf3.parse_version("1.1.1234"))
        with pytest.raises(ConversionException):
            xdf3_to_xdf2.convert_identifier(identifier)

    def test_should_fail_for_non_000_unternummernkreis(self):
        identifier = xdf3.Identifier("S1200134", xdf3.parse_version("1.1.1"))
        with pytest.raises(ConversionException):
            xdf3_to_xdf2.convert_identifier(identifier)


class TestRule:
    def test_should_convert_a_rule(self):
        factory = Xdf3Factory()
        xdf3_rule = (
            factory.rule(
                id="R1200034",
                version="1.0.0",
                name="Regelname",
                beschreibung="Beschreibung",
                freitext_regel="Definition",
                fachlicher_ersteller="Ersteller",
                skript="Some Code",
            )
            .with_bezug("Rechtsbezug 1")
            .with_bezug("Rechtsbezug 2")
            .build()
        )

        xdf2_rule = xdf3_to_xdf2.convert_rule(xdf3_rule)

        assert xdf2_rule == xdf2.Regel(
            identifier=xdf2.Identifier("R1234", version=xdf2.parse_version("1.0")),
            name="Regelname",
            bezeichnung_eingabe=None,
            bezeichnung_ausgabe=None,
            beschreibung="Beschreibung",
            definition="Definition",
            bezug="Rechtsbezug 1;Rechtsbezug 2",
            status=xdf2.Status.AKTIV,
            versionshinweis=None,
            gueltig_ab=None,
            gueltig_bis=None,
            fachlicher_ersteller="Ersteller",
            freigabedatum=None,
            veroeffentlichungsdatum=None,
            script="Some Code",
        )


class TestField:
    def test_should_convert_a_field(self):
        factory = Xdf3Factory()
        xdf3_rule = factory.rule(id="R1200034", version="1.0.0").build()
        xdf3_field = (
            factory.field(
                id="F1200034",
                version="1.0.0",
                name="Feldname",
                beschreibung="Beschreibung",
                definition="Definition",
                freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
                bezeichnung_eingabe="Eingabe",
                bezeichnung_ausgabe="Ausgabe",
                status_gesetzt_durch="Ersteller",
                schema_element_art=xdf3.SchemaElementArt.HARMONISIERT,
                hilfetext_eingabe="Hilfe Eingabe",
                hilfetext_ausgabe="Hilfe Ausgabe",
            )
            .input(datentyp=xdf3.Datentyp.TEXT)
            .with_bezug("Rechtsbezug 1")
            .with_bezug("Rechtsbezug 2")
            .with_rule(xdf3_rule)
            .build()
        )

        xdf2_field = xdf3_to_xdf2.convert_field(
            xdf3_field, xdf3_to_xdf2.ConverterContext()
        )

        assert xdf2_field == xdf2.Datenfeld(
            identifier=xdf2.Identifier("F1234", version=xdf2.parse_version("1.0")),
            name="Feldname",
            bezeichnung_eingabe="Eingabe",
            bezeichnung_ausgabe="Ausgabe",
            beschreibung="Beschreibung",
            definition="Definition",
            bezug="Rechtsbezug 1;Rechtsbezug 2",
            status=xdf2.Status.AKTIV,
            versionshinweis=None,
            gueltig_ab=None,
            gueltig_bis=None,
            fachlicher_ersteller="Ersteller",
            freigabedatum=None,
            veroeffentlichungsdatum=None,
            schema_element_art=xdf3.SchemaElementArt.HARMONISIERT,
            hilfetext_eingabe="Hilfe Eingabe",
            hilfetext_ausgabe="Hilfe Ausgabe",
            feldart=xdf2.Feldart.INPUT,
            datentyp=xdf2.Datentyp.TEXT,
            praezisierung=None,
            inhalt=None,
            code_listen_referenz=None,
            regeln=[
                xdf2.Identifier("R1234", version=xdf2.parse_version("1.0")),
            ],
        )

    def test_should_convert_a_minimal_field(self):
        factory = Xdf3Factory()
        xdf3_field = factory.field(id="F1200034", version="1.0.0").minimal().build()

        xdf2_field = xdf3_to_xdf2.convert_field(
            xdf3_field, xdf3_to_xdf2.ConverterContext()
        )

        assert xdf2_field == xdf2.Datenfeld(
            identifier=xdf2.Identifier("F1234", version=xdf2.parse_version("1.0")),
            name=xdf3_field.name,
            bezeichnung_eingabe=xdf3_field.bezeichnung_eingabe,
            bezeichnung_ausgabe=None,
            beschreibung=None,
            definition=None,
            bezug=None,
            status=xdf2.Status.AKTIV,
            versionshinweis=None,
            gueltig_ab=None,
            gueltig_bis=None,
            fachlicher_ersteller=None,
            freigabedatum=None,
            veroeffentlichungsdatum=None,
            schema_element_art=xdf3.SchemaElementArt.RECHTSNORMGEBUNDEN,
            hilfetext_eingabe=None,
            hilfetext_ausgabe=None,
            feldart=xdf2.Feldart.INPUT,
            datentyp=xdf2.Datentyp.TEXT,
            praezisierung=None,
            inhalt=None,
            code_listen_referenz=None,
            regeln=[],
        )

    def test_should_convert_praezisierung(self):
        factory = Xdf3Factory()
        xdf3_field = (
            factory.field()
            .input(
                praezisierung=xdf3.Praezisierung(
                    min_length=10,
                    max_length=20,
                    min_value="test",
                    max_value="test",
                    pattern="test",
                )
            )
            .build()
        )

        xdf2_field = xdf3_to_xdf2.convert_field(
            xdf3_field, xdf3_to_xdf2.ConverterContext()
        )

        assert xdf2_field.praezisierung is not None
        assert json.loads(xdf2_field.praezisierung) == {
            "minLength": 10,
            "maxLength": 20,
            "minValue": "test",
            "maxValue": "test",
            "pattern": "test",
        }

    def test_should_extract_embedded_code_list(self):
        factory = Xdf3Factory()
        xdf3_field = (
            factory.field()
            .select(
                werte=[
                    xdf3.ListenWert(code="A", name="Label A", hilfe=None),
                    xdf3.ListenWert(code="B", name="Label B", hilfe=None),
                ]
            )
            .build()
        )

        context = xdf3_to_xdf2.ConverterContext()
        xdf2_field = xdf3_to_xdf2.convert_field(xdf3_field, context)

        assert xdf2_field.code_listen_referenz == xdf2.CodeListenReferenz(
            identifier=xdf2.CodeListIdentifier("C99000001", None),
            genericode_identifier=genericode.Identifier(
                canonical_uri="de:fitko:sammelrepository:xdf3-to-xdf2-converter:C99000001",
                version="1",
                canonical_version_uri="de:fitko:sammelrepository:xdf3-to-xdf2-converter:C99000001_1",
            ),
        )

        assert len(context.code_lists) == 1
        assert context.code_lists["C99000001"].columns == {
            "code": ["A", "B"],
            "name": ["Label A", "Label B"],
        }


class TestGroup:
    def test_should_convert_a_group(self):
        factory = Xdf3Factory()
        xdf3_rule = factory.rule(id="R1200034", version="1.0.0").build()
        xdf3_field = factory.field(id="F1200034", version="1.0.0").build()
        xdf3_group = (
            factory.group(
                id="G1200034",
                version="1.0.0",
                name="Name",
                beschreibung="Beschreibung",
                definition="Definition",
                freigabe_status=xdf3.FreigabeStatus.FACHLICH_FREIGEGEBEN_GOLD,
                bezeichnung_eingabe="Eingabe",
                bezeichnung_ausgabe="Ausgabe",
                status_gesetzt_durch="Ersteller",
                schema_element_art=xdf3.SchemaElementArt.HARMONISIERT,
                hilfetext_eingabe="Hilfe Eingabe",
                hilfetext_ausgabe="Hilfe Ausgabe",
            )
            .with_rule(xdf3_rule)
            .with_bezug("Rechtsbezug 1")
            .with_bezug("Rechtsbezug 2")
            .with_field(
                xdf3_field,
                anzahl=xdf3.Anzahl(min=0, max=1),
                bezug=[
                    xdf3.Rechtsbezug(parse_string_latin("Rechtsbezug A"), link=None),
                    xdf3.Rechtsbezug(parse_string_latin("Rechtsbezug B"), link=None),
                ],
            )
            .build()
        )

        xdf2_group = xdf3_to_xdf2.convert_group(xdf3_group)

        assert xdf2_group == xdf2.Gruppe(
            identifier=xdf2.Identifier("G1234", version=xdf2.parse_version("1.0")),
            name="Name",
            bezeichnung_eingabe="Eingabe",
            bezeichnung_ausgabe="Ausgabe",
            beschreibung="Beschreibung",
            definition="Definition",
            bezug="Rechtsbezug 1;Rechtsbezug 2",
            status=xdf2.Status.AKTIV,
            versionshinweis=None,
            gueltig_ab=None,
            gueltig_bis=None,
            fachlicher_ersteller="Ersteller",
            freigabedatum=None,
            veroeffentlichungsdatum=None,
            schema_element_art=xdf3.SchemaElementArt.HARMONISIERT,
            hilfetext_eingabe="Hilfe Eingabe",
            hilfetext_ausgabe="Hilfe Ausgabe",
            regeln=[
                xdf2.Identifier("R1234", version=xdf2.parse_version("1.0")),
            ],
            struktur=[
                xdf2.ElementReference(
                    anzahl=xdf2.Anzahl(min=0, max=1),
                    bezug="Rechtsbezug A;Rechtsbezug B",
                    element_type=xdf2.ElementType.FELD,
                    identifier=xdf2.Identifier(
                        "F1234", version=xdf2.parse_version("1.0")
                    ),
                )
            ],
        )
