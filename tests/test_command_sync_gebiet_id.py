from typing import AsyncContextManager, Callable
from asyncpg import Pool
import pytest

from fimportal.cli.commands.import_gebiet_id import (
    GEBIET_ID_KREIS_URI,
    GEBIET_ID_REGIONALSCHLUESSEL_URI,
    sync_gebiet_id,
)
from fimportal.service import Service
from fimportal.xrepository import FakeClient
from tests.factories.genericode import CodeListFactory


@pytest.mark.asyncio(loop_scope="session")
async def test_should_save_gebiet_id(
    get_service: Callable[[], AsyncContextManager[Service]],
    async_db_pool: Pool,
    xrepository_client: FakeClient,
):
    gebiet_id_rs = CodeListFactory(
        canonical_uri=GEBIET_ID_REGIONALSCHLUESSEL_URI,
        version="2024-01-01",
        default_code_key="SCHLUESSEL",
        default_name_key="Bezeichnung",
        columns={
            "SCHLUESSEL": ["1", "3"],
            "Bezeichnung": ["Kiel", "Berlin"],
        },
    ).build()
    gebiet_id_kreis = CodeListFactory(
        canonical_uri=GEBIET_ID_KREIS_URI,
        version="2024-01-01",
        default_code_key="SCHLUESSEL",
        default_name_key="Bezeichnung",
        columns={
            "SCHLUESSEL": ["5"],
            "Bezeichnung": ["Ostholstein"],
        },
    ).build()
    xrepository_client.register_kennungen(
        gebiet_id_rs.identifier.canonical_uri,
        [gebiet_id_rs.identifier.canonical_version_uri],
    )
    xrepository_client.register_code_list(gebiet_id_rs)
    xrepository_client.register_kennungen(
        gebiet_id_kreis.identifier.canonical_uri,
        [gebiet_id_kreis.identifier.canonical_version_uri],
    )
    xrepository_client.register_code_list(gebiet_id_kreis)

    await sync_gebiet_id(async_db_pool, "localhost", xrepository_client)

    async with get_service() as service:
        assert await service.get_gebiet_id_label("1") == "Kiel"
        assert await service.get_gebiet_id_label("2") is None
        assert await service.get_gebiet_id_label("3") == "Berlin"
        assert await service.get_gebiet_id_label("5") == "Ostholstein"


@pytest.mark.asyncio(loop_scope="session")
async def test_should_update_existing_gebiet_ids_for_newer_version(
    get_service: Callable[[], AsyncContextManager[Service]],
    xrepository_client: FakeClient,
    async_db_pool: Pool,
):
    gebiet_ids_rs_before = CodeListFactory(
        canonical_uri=GEBIET_ID_REGIONALSCHLUESSEL_URI,
        version="2024-01-01",
        default_code_key="SCHLUESSEL",
        default_name_key="Bezeichnung",
        columns={
            "SCHLUESSEL": ["1", "3"],
            "Bezeichnung": ["Kiel", "Berlin"],
        },
    ).build()
    gebiet_ids_kreis_before = CodeListFactory(
        canonical_uri=GEBIET_ID_KREIS_URI,
        version="2024-01-01",
        default_code_key="SCHLUESSEL",
        default_name_key="Bezeichnung",
        columns={
            "SCHLUESSEL": ["4"],
            "Bezeichnung": ["Dithmarschen"],
        },
    ).build()
    xrepository_client.register_kennungen(
        gebiet_ids_rs_before.identifier.canonical_uri,
        [gebiet_ids_rs_before.identifier.canonical_version_uri],
    )
    xrepository_client.register_code_list(gebiet_ids_rs_before)
    xrepository_client.register_kennungen(
        gebiet_ids_kreis_before.identifier.canonical_uri,
        [gebiet_ids_kreis_before.identifier.canonical_version_uri],
    )
    xrepository_client.register_code_list(gebiet_ids_kreis_before)
    await sync_gebiet_id(async_db_pool, "localhost", xrepository_client)
    xrepository_client.clear()

    gebiet_ids_rs_after = CodeListFactory(
        canonical_uri=GEBIET_ID_REGIONALSCHLUESSEL_URI,
        version="2025-01-01",
        default_code_key="SCHLUESSEL",
        default_name_key="Bezeichnung",
        columns={
            "SCHLUESSEL": ["1", "2", "3"],
            "Bezeichnung": ["Kiel", "Dortmund", "Potsdam"],
        },
    ).build()
    gebiet_ids_kreis_after = CodeListFactory(
        canonical_uri=GEBIET_ID_KREIS_URI,
        version="2025-01-01",
        default_code_key="SCHLUESSEL",
        default_name_key="Bezeichnung",
        columns={
            "SCHLUESSEL": ["4", "5"],
            "Bezeichnung": ["Pinneberg", "Nordfriesland"],
        },
    ).build()
    xrepository_client.register_kennungen(
        gebiet_ids_rs_after.identifier.canonical_uri,
        [gebiet_ids_rs_after.identifier.canonical_version_uri],
    )
    xrepository_client.register_code_list(gebiet_ids_rs_after)
    xrepository_client.register_kennungen(
        gebiet_ids_kreis_after.identifier.canonical_uri,
        [gebiet_ids_kreis_after.identifier.canonical_version_uri],
    )
    xrepository_client.register_code_list(gebiet_ids_kreis_after)

    await sync_gebiet_id(async_db_pool, "localhost", xrepository_client)

    async with get_service() as service:
        assert await service.get_gebiet_id_label("1") == "Kiel"
        assert await service.get_gebiet_id_label("2") == "Dortmund"
        assert await service.get_gebiet_id_label("3") == "Potsdam"
        assert await service.get_gebiet_id_label("4") == "Pinneberg"
        assert await service.get_gebiet_id_label("5") == "Nordfriesland"


@pytest.mark.asyncio(loop_scope="session")
async def test_should_not_update_existing_gebiet_ids_for_older_version(
    get_service: Callable[[], AsyncContextManager[Service]],
    xrepository_client: FakeClient,
    async_db_pool: Pool,
):
    xrepository_client.clear()
    gebiet_ids_rs_before = CodeListFactory(
        canonical_uri=GEBIET_ID_REGIONALSCHLUESSEL_URI,
        version="2025-01-01",
        default_code_key="SCHLUESSEL",
        default_name_key="Bezeichnung",
        columns={
            "SCHLUESSEL": ["1", "3"],
            "Bezeichnung": ["Kiel", "Berlin"],
        },
    ).build()
    xrepository_client.register_kennungen(
        gebiet_ids_rs_before.identifier.canonical_uri,
        [gebiet_ids_rs_before.identifier.canonical_version_uri],
    )
    xrepository_client.register_code_list(gebiet_ids_rs_before)
    await sync_gebiet_id(async_db_pool, "localhost", xrepository_client)
    xrepository_client.clear()

    gebiet_ids_rs_after = CodeListFactory(
        canonical_uri=GEBIET_ID_REGIONALSCHLUESSEL_URI,
        version="2024-01-01",
        default_code_key="SCHLUESSEL",
        default_name_key="Bezeichnung",
        columns={
            "SCHLUESSEL": ["1", "2", "3"],
            "Bezeichnung": ["Kiel", "Dortmund", "Potsdam"],
        },
    ).build()
    xrepository_client.register_kennungen(
        gebiet_ids_rs_after.identifier.canonical_uri,
        [gebiet_ids_rs_after.identifier.canonical_version_uri],
    )
    xrepository_client.register_code_list(gebiet_ids_rs_after)

    await sync_gebiet_id(async_db_pool, "localhost", xrepository_client)

    async with get_service() as service:
        assert await service.get_gebiet_id_label("1") == "Kiel"
        assert await service.get_gebiet_id_label("2") == "Dortmund"
        assert await service.get_gebiet_id_label("3") == "Berlin"


@pytest.mark.asyncio(loop_scope="session")
async def test_should_ignore_invalid_keys(
    get_service: Callable[[], AsyncContextManager[Service]],
    xrepository_client: FakeClient,
    async_db_pool: Pool,
):
    invalid_label = "1,6E+11"

    xrepository_client.clear()
    gebiet_ids_rs_before = CodeListFactory(
        canonical_uri=GEBIET_ID_REGIONALSCHLUESSEL_URI,
        version="2025-01-01",
        default_code_key="SCHLUESSEL",
        default_name_key="Bezeichnung",
        columns={
            "SCHLUESSEL": [invalid_label],
            "Bezeichnung": ["Kiel"],
        },
    ).build()
    xrepository_client.register_kennungen(
        gebiet_ids_rs_before.identifier.canonical_uri,
        [gebiet_ids_rs_before.identifier.canonical_version_uri],
    )
    xrepository_client.register_code_list(gebiet_ids_rs_before)
    await sync_gebiet_id(async_db_pool, "localhost", xrepository_client)

    async with get_service() as service:
        assert await service.get_gebiet_id_label(invalid_label) is None
