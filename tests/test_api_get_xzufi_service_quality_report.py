from fastapi.testclient import TestClient

from fimportal.helpers import utc_now
from tests.conftest import CommandRunner
from tests.factories.xzufi import LeistungFactory, LinkFactory, TextmodulFactory

ENDPOINT = "/api/v0/xzufi-services"


def test_get_leistung_quality_report(client: TestClient, runner: CommandRunner):
    uri = "another link"
    leistung = LeistungFactory(
        bezeichnung=TextmodulFactory().with_link(uri).build()
    ).build()
    identifier = leistung.get_identifier()
    assert identifier is not None
    redaktion_id, leistung_id = identifier
    runner.import_pvog(leistung)
    now = utc_now()
    runner.import_link(LinkFactory(uri=uri, last_checked=now, last_online=now).build())

    response = client.get(f"{ENDPOINT}/{redaktion_id}/{leistung_id}/quality-report")

    assert response.status_code == 200
    assert response.json() == {
        "redaktion_id": redaktion_id,
        "leistung_id": leistung_id,
        "link_status": [{"uri": uri, "is_online": True}],
    }


def test_get_leistung_quality_report_with_no_known_links(
    client: TestClient, runner: CommandRunner
):
    leistung = LeistungFactory(bezeichnung=TextmodulFactory(links=[]).build()).build()
    identifier = leistung.get_identifier()
    assert identifier is not None
    redaktion_id, leistung_id = identifier
    runner.import_pvog(leistung)

    response = client.get(f"{ENDPOINT}/{redaktion_id}/{leistung_id}/quality-report")

    assert response.status_code == 200
    assert response.json() == {
        "redaktion_id": redaktion_id,
        "leistung_id": leistung_id,
        "link_status": [],
    }
