from fimportal.xdatenfelder.xdf2 import Anzahl


def test_should_correctly_parse_optional_value():
    anzahl = Anzahl.from_string("0:1")

    assert anzahl == Anzahl(0, 1)
    assert anzahl.is_optional
    assert not anzahl.is_array


def test_should_correctly_parse_required_value():
    anzahl = Anzahl.from_string("1:1")

    assert anzahl == Anzahl(1, 1)
    assert not anzahl.is_optional
    assert not anzahl.is_array


def test_should_correctly_parse_limited_array():
    anzahl = Anzahl.from_string("0:5")

    assert anzahl == Anzahl(0, 5)
    assert not anzahl.is_optional
    assert anzahl.is_array


def test_should_correctly_parse_unlimited_array():
    anzahl = Anzahl.from_string("0:*")

    assert anzahl == Anzahl(0, None)
    assert not anzahl.is_optional
    assert anzahl.is_array
