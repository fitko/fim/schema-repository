import pytest

from fimportal.xdatenfelder.xdf2.datenfeldgruppe_message import (
    DatenfeldgruppeMessage,
    parse_datenfeldgruppe_message,
    serialize_datenfeldgruppe_message,
)
from tests.factories.xdf2 import Xdf2Factory


@pytest.mark.parametrize(
    "message",
    [
        Xdf2Factory().group().minimal().message(),
        Xdf2Factory().group().full().message(),
    ],
)
def test_serialize_and_parse_message(message: DatenfeldgruppeMessage):
    xml = serialize_datenfeldgruppe_message(message)
    parsed_message = parse_datenfeldgruppe_message(xml)

    assert parsed_message == message
