import pytest

from fimportal.xdatenfelder import xdf2


def test_should_correctly_parse_empty_constraints():
    constraints = xdf2.Constraints.parse(None)

    assert constraints == xdf2.Constraints(
        min_length=None,
        max_length=None,
        min_value=None,
        max_value=None,
        pattern=None,
    )


def test_should_correctly_parse_input_constraints():
    constraints = xdf2.Constraints.parse(
        '{"minLength":"1","maxLength":"120","minValue":"1","maxValue":"10","pattern":"[A-Z]+"}'
    )

    assert constraints == xdf2.Constraints(
        min_length=1,
        max_length=120,
        min_value=1,
        max_value=10,
        pattern="[A-Z]+",
    )


def test_should_correctly_parse_german_float_notation():
    constraints = xdf2.Constraints.parse(
        '{"minLength":"1.000","maxLength":"2.000","minValue":"0,01","maxValue":"9.999,99"}'
    )

    assert constraints == xdf2.Constraints(
        min_length=1_000,
        max_length=2_000,
        min_value=0.01,
        max_value=9_999.99,
        pattern=None,
    )


def test_should_ignore_empty_strings():
    constraints = xdf2.Constraints.parse(
        '{"minLength":"","maxLength":"","minValue":"","maxValue":"","pattern":""}'
    )

    assert constraints == xdf2.Constraints(
        min_length=None,
        max_length=None,
        min_value=None,
        max_value=None,
        pattern=None,
    )


def test_should_ignore_unknown_keys_strings():
    constraints = xdf2.Constraints.parse('{"note":"Some ignored note"}')

    assert constraints == xdf2.Constraints(
        min_length=None,
        max_length=None,
        min_value=None,
        max_value=None,
        pattern=None,
    )


def test_should_fail_for_invalid_json():
    with pytest.raises(xdf2.InvalidPraezisierungException):
        xdf2.Constraints.parse("invalid_json")
