from datetime import date

from fimportal.xdatenfelder.xdf2 import (
    Identifier,
    Status,
    Steckbrief,
    Version,
    load_steckbrief_message,
)
from tests.data import XDF2_DATA


def test_should_parse_steckbrief():
    steckbrief_message = load_steckbrief_message(XDF2_DATA / "steckbrief.xdf2.xml")

    assert steckbrief_message.steckbrief == Steckbrief(
        identifier=Identifier("D00000130", version=Version("1.0")),
        name="Antrag Ausbildungsförderung ",
        fachlicher_ersteller="BMBF",
        bezeichnung_eingabe="Antrag auf Ausbildungsförderung ",
        bezeichnung_ausgabe="Eine Bezeichnungsausgabe",
        beschreibung="Eine Steckrbief Beschreibung",
        definition="Definition",
        bezug="Ein Bezug",
        status=Status.AKTIV,
        versionshinweis="Version zur Veröffentlichung",
        freigabedatum=date(year=2019, month=8, day=27),
        veroeffentlichungsdatum=date(year=2019, month=8, day=27),
        gueltig_ab=date(year=2019, month=9, day=1),
        gueltig_bis=date(year=2020, month=9, day=1),
        hilfetext="Hilfetext",
        is_referenz=False,
        dokumentart="unbestimmt",
    )
