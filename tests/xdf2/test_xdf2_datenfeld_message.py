import pytest

from fimportal.xdatenfelder.xdf2.datenfeld_message import (
    DatenfeldMessage,
    parse_datenfeld_message,
    serialize_datenfeld_message,
)
from tests.factories.xdf2 import Xdf2Factory


@pytest.mark.parametrize(
    "message",
    [
        Xdf2Factory().field().minimal().message(),
        Xdf2Factory().field().full().message(),
    ],
)
def test_serialize_and_parse_message(message: DatenfeldMessage):
    xml = serialize_datenfeld_message(message)
    parsed_message = parse_datenfeld_message(xml)

    assert parsed_message == message
