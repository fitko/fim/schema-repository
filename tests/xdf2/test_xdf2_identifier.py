import pytest
from fimportal.xdatenfelder import xdf2
from fimportal.xdatenfelder.common import InternalParserException


def test_should_extract_the_nummernkreis():
    identifier = xdf2.Identifier(id="S12", version=xdf2.parse_version("1.0"))

    assert identifier.nummernkreis == "12"


def test_should_fail_for_short_id():
    with pytest.raises(InternalParserException):
        xdf2.Identifier(id="S1", version=xdf2.parse_version("1.0"))


def test_should_fail_for_non_int_values():
    with pytest.raises(InternalParserException):
        xdf2.Identifier(id="SSS", version=xdf2.parse_version("1.0"))
