import pytest

from fimportal.xdatenfelder.xdf2 import (
    Version,
    parse_version,
    InvalidVersionException,
)


def test_should_correctly_identify_invalid_versions():
    assert parse_version("1.1") == Version("1.1")
    assert parse_version("0.10") == Version("0.10")

    with pytest.raises(InvalidVersionException):
        parse_version("0.1.1")

    with pytest.raises(InvalidVersionException):
        parse_version("1")

    with pytest.raises(InvalidVersionException):
        parse_version("test")
