import xml.etree.ElementTree as ET

from fimportal.xdatenfelder.xdf2 import (
    Anzahl,
    Constraints,
    Datentyp,
    Identifier,
    parse_version,
)
from fimportal.xsd import (
    XS_MAX_INCLUSIVE,
    XS_MAX_LENGTH,
    XS_MIN_INCLUSIVE,
    XS_MIN_LENGTH,
    XS_PATTERN,
    add_constraints,
    combine_fields_and_groups,
    create_element,
    create_schema_id,
    get_type_property,
    process_anzahl,
)
from tests.factories.xdf2 import Xdf2Factory


def test_create_schema_name():
    identifier = Identifier(id="S00000093", version=parse_version("1.0"))
    schema_id = create_schema_id(identifier)

    assert schema_id, "fim.S00000093V1.0"


def test_get_type_property_field_string():
    field = Xdf2Factory().field(datentyp=Datentyp.TEXT).build()

    assert get_type_property(field), "xs:string"


def test_get_type_property_field_float():
    field = Xdf2Factory().field(datentyp=Datentyp.NUMMER).build()

    assert get_type_property(field), "xs:float"


def test_get_type_property_group():
    group = Xdf2Factory().group(id="G001").build()

    assert get_type_property(group), "xfd:G001"


def test_process_anzahl_min_occurs():
    element = ET.Element("xs:element")
    anzahl = Anzahl(min=0, max=1)

    process_anzahl(anzahl, element)

    assert element.attrib["minOccurs"] == "0"
    assert "maxOccurs" not in element.attrib


def test_process_anzahl_min_max_occurs():
    element = ET.Element("xs:element")
    anzahl = Anzahl(min=2, max=2)

    process_anzahl(anzahl, element)

    assert element.attrib["minOccurs"] == "2"
    assert element.attrib["maxOccurs"] == "2"


def test_process_anzahl_max_unbounded():
    element = ET.Element("xs:element")
    anzahl = Anzahl(min=1, max=None)

    process_anzahl(anzahl, element)

    assert element.attrib["minOccurs"] == "1"
    assert element.attrib["maxOccurs"] == "unbounded"


def test_create_element_datenfeld():
    field = (
        Xdf2Factory()
        .field(id="F123", version="1.0")
        .input(datentyp=Datentyp.TEXT)
        .build()
    )

    parent_element = ET.Element("parent")
    result_element = create_element(parent_element, field)

    assert result_element.get("name") == "F123V1.0"
    assert result_element.get("type") == "xs:string"


def test_always_use_string_type_for_select_fields():
    field = Xdf2Factory().field().select(datentyp=Datentyp.GANZZAHL).build()

    parent_element = ET.Element("parent")
    result_element = create_element(parent_element, field)

    assert result_element.get("type") == "xs:string"


def test_create_element_datenfeld_input():
    field = Xdf2Factory().field().input(datentyp=Datentyp.TEXT).build()

    parent_element = ET.Element("parent")
    result_element = create_element(parent_element, field)

    assert result_element.get("type") == "xs:string"


def test_create_element_gruppe():
    group = Xdf2Factory().group(id="G001", version="1.3").build()

    parent_element = ET.Element("parent")
    result_element = create_element(parent_element, group)

    assert result_element.get("name") == "G001V1.3"
    assert result_element.get("type") == "xfd:G001V1.3"


def test_create_element_gruppe_empty_structure():
    group = Xdf2Factory().group(id="G001", version="1.3").build()

    parent_element = ET.Element("parent")
    result_element = create_element(parent_element, group)

    assert result_element.get("name") == "G001V1.3"
    assert result_element.get("type") == "xfd:G001V1.3"
    assert len(list(result_element)) == 0


def test_create_element_gruppe_multiple_elements_structure():
    group = Xdf2Factory().group(id="G001", version="1.3").build()

    parent_element = ET.Element("parent")
    result_element = create_element(parent_element, group)

    assert result_element.get("name") == "G001V1.3"
    assert result_element.get("type") == "xfd:G001V1.3"
    assert len(list(result_element)) == 0


def test_combine_fields_and_groups():
    factory = Xdf2Factory()
    field_1 = factory.field(id="F001", name="Hinweis").build()
    field_2 = factory.field(id="F002", name="Hinweis").build()
    fields = {field_1.identifier: field_1, field_2.identifier: field_2}

    group_1 = factory.group(id="G001").build()
    group_2 = factory.group(id="G002").build()
    groups = {
        group_1.identifier: group_1,
        group_2.identifier: group_2,
    }

    combined = combine_fields_and_groups(groups, fields)

    assert combined == {
        "F001": field_1,
        "F002": field_2,
        "G001": group_1,
        "G002": group_2,
    }


def test_combine_fields_and_groups_empty_fields():
    group = Xdf2Factory().group(id="G001").build()
    groups = {group.identifier: group}

    combined = combine_fields_and_groups(groups, fields={})

    assert combined == {"G001": group}


def test_combine_fields_and_groups_empty_groups():
    field = Xdf2Factory().field(id="F001").build()
    fields = {field.identifier: field}

    combined = combine_fields_and_groups(groups={}, fields=fields)

    assert combined == {"F001": field}


def test_combine_fields_and_groups_empty_both():
    fields = {}
    groups = {}

    combined = combine_fields_and_groups(groups, fields)

    assert len(combined) == 0


def test_text_constraints_without_pattern():
    constraints = Constraints(
        min_length=10, max_length=120, min_value=None, max_value=None, pattern=None
    )

    restriction_element = ET.Element("restriction")
    add_constraints(Datentyp.TEXT, constraints, restriction_element)

    assert len(restriction_element.findall(XS_MIN_LENGTH)) == 1
    min_length_element = restriction_element.find(XS_MIN_LENGTH)
    assert min_length_element is not None, "min_length_element should not be None"
    assert min_length_element.get("value") == "10"

    assert len(restriction_element.findall(XS_MAX_LENGTH)) == 1
    max_length_element = restriction_element.find(XS_MAX_LENGTH)
    assert max_length_element is not None, "max_length_element should not be None"
    assert max_length_element.get("value") == "120"


def test_text_constraints_with_pattern():
    constraints = Constraints(
        min_length=10, max_length=120, min_value=None, max_value=None, pattern="[A-Z]+"
    )
    restriction_element = ET.Element("restriction")
    add_constraints(Datentyp.TEXT, constraints, restriction_element)

    min_length_elements = restriction_element.findall(XS_MIN_LENGTH)
    assert len(min_length_elements) == 1
    min_length_element = min_length_elements[0]
    assert min_length_element.get("value") == "10"

    max_length_elements = restriction_element.findall(XS_MAX_LENGTH)
    assert len(max_length_elements) == 1
    max_length_element = max_length_elements[0]
    assert max_length_element.get("value") == "120"

    pattern_elements = restriction_element.findall(XS_PATTERN)
    assert len(pattern_elements) == 1
    pattern_element = pattern_elements[0]
    assert pattern_element.get("value") == "[A-Z]+"


def test_ganzzahl_float_constraints():
    constraints = Constraints(
        min_length=None, max_length=None, min_value=1.5, max_value=15.5, pattern=None
    )
    restriction_element = ET.Element("restriction")
    add_constraints(Datentyp.GANZZAHL, constraints, restriction_element)

    min_inclusive_elements = restriction_element.findall(XS_MIN_INCLUSIVE)
    assert len(min_inclusive_elements) == 1
    min_inclusive_element = min_inclusive_elements[0]
    assert min_inclusive_element.get("value") == "1"

    max_inclusive_elements = restriction_element.findall(XS_MAX_INCLUSIVE)
    assert len(max_inclusive_elements) == 1
    max_inclusive_element = max_inclusive_elements[0]
    assert max_inclusive_element.get("value") == "15"


def test_ganzzahl_constraints():
    constraints = Constraints(
        min_length=None, max_length=None, min_value=1, max_value=100, pattern=None
    )

    restriction_element = ET.Element("restriction")
    add_constraints(Datentyp.GANZZAHL, constraints, restriction_element)

    min_inclusive_elements = restriction_element.findall(XS_MIN_INCLUSIVE)
    assert len(min_inclusive_elements) == 1
    min_inclusive_element = min_inclusive_elements[0]
    assert min_inclusive_element.get("value") == "1"

    max_inclusive_elements = restriction_element.findall(XS_MAX_INCLUSIVE)
    assert len(max_inclusive_elements) == 1
    max_inclusive_element = max_inclusive_elements[0]
    assert max_inclusive_element.get("value") == "100"


def test_pattern_constraints():
    constraints = Constraints(
        min_length=None,
        max_length=None,
        min_value=None,
        max_value=None,
        pattern="[A-Z]+",
    )

    restriction_element = ET.Element("restriction")
    add_constraints(Datentyp.TEXT, constraints, restriction_element)

    pattern_elements = restriction_element.findall(XS_PATTERN)
    assert len(pattern_elements) == 1
    pattern_element = pattern_elements[0]
    assert pattern_element.get("value") == "[A-Z]+"


def test_min_value_constraints():
    constraints = Constraints(
        min_length=None, max_length=None, min_value=1, max_value=None, pattern=None
    )

    restriction_element = ET.Element("restriction")
    add_constraints(Datentyp.GANZZAHL, constraints, restriction_element)

    min_inclusive_elements = restriction_element.findall(XS_MIN_INCLUSIVE)
    assert len(min_inclusive_elements) == 1
    min_inclusive_element = min_inclusive_elements[0]
    assert min_inclusive_element.get("value") == "1"


def test_max_value_constraints():
    constraints = Constraints(
        min_length=None, max_length=None, min_value=None, max_value=100, pattern=None
    )

    restriction_element = ET.Element("restriction")
    add_constraints(Datentyp.GANZZAHL, constraints, restriction_element)

    max_inclusive_elements = restriction_element.findall(XS_MAX_INCLUSIVE)
    assert len(max_inclusive_elements) == 1
    max_inclusive_element = max_inclusive_elements[0]
    assert max_inclusive_element.get("value") == "100"


def test_no_constraints():
    constraints = Constraints(
        min_length=None, max_length=None, min_value=None, max_value=None, pattern=None
    )

    restriction_element = ET.Element("restriction")
    add_constraints(Datentyp.TEXT, constraints, restriction_element)

    assert len(restriction_element.findall("*")) == 0
