import pytest
from fimportal.xdatenfelder import xdf2

from tests.factories.genericode import CodeListFactory
from tests.factories.xdf2 import Xdf2Factory, FieldFactory


def test_should_serialize_a_minimal_message():
    message = Xdf2Factory().schema().message()

    content = xdf2.serialize_schema_message(message)

    assert xdf2.parse_schema_message(content) == message


def test_should_serialize_a_full_message():
    factory = Xdf2Factory()
    rule = factory.rule().full().build()
    field = factory.field().with_rule(rule).full().build()
    group = factory.group().with_rule(rule).with_field(field).full().build()
    message = factory.schema().with_group(group).full().message()

    content = xdf2.serialize_schema_message(message)

    assert xdf2.parse_schema_message(content) == message


@pytest.mark.parametrize(
    "field_factory",
    [
        Xdf2Factory().field().minimal(),
        Xdf2Factory().field().input(xdf2.Datentyp.TEXT, praezisierung="praezisierung"),
        Xdf2Factory()
        .field()
        .select(
            code_list=CodeListFactory().build(),
        ),
        Xdf2Factory().field().input(xdf2.Datentyp.ANLAGE),
    ],
)
def test_should_serialize_fields(field_factory: FieldFactory):
    field = field_factory.build()
    message = field_factory.factory.schema().with_field(field).message()

    content = xdf2.serialize_schema_message(message)

    assert xdf2.parse_schema_message(content) == message


def test_should_serialize_full_groups():
    factory = Xdf2Factory()
    dummy_field = factory.field().build()
    dummy_rule = factory.rule().build()
    group = factory.group().with_field(dummy_field).with_rule(dummy_rule).full().build()
    message = factory.schema().with_group(group).message()

    content = xdf2.serialize_schema_message(message)

    assert xdf2.parse_schema_message(content) == message


def test_should_serialize_minimal_groups():
    factory = Xdf2Factory()
    dummy_field = factory.field().build()
    group = factory.group().minimal().with_field(dummy_field).build()
    message = factory.schema().with_group(group).message()

    content = xdf2.serialize_schema_message(message)

    assert xdf2.parse_schema_message(content) == message
