import datetime
import re

import pytest

from fimportal import genericode
from fimportal.xdatenfelder.common import XdfException
from fimportal.xdatenfelder.xdf2 import (
    AbleitungsmodifikationenRepraesentation,
    AbleitungsmodifikationenStruktur,
    Anzahl,
    CodeListenReferenz,
    CodeListIdentifier,
    Datenfeld,
    Datentyp,
    ElementReference,
    ElementType,
    Feldart,
    Gruppe,
    Identifier,
    MessageHeader,
    ParserException,
    Regel,
    Schema,
    SchemaElementArt,
    Status,
    Version,
    load_schema_message,
    parse_schema_message,
    serialize_schema_message,
)
from tests.data import XDF2_DATA
from tests.factories import CodeListFactory, FieldFactory, Xdf2Factory


class TestParseSchemaMessage:
    def test_should_parse_the_message_header(self):
        message = load_schema_message(XDF2_DATA / "S1234V1.0.xml")

        assert message.header == MessageHeader(
            nachricht_id="abcd1234",
            erstellungs_zeitpunkt=datetime.datetime(
                2020, 9, 1, tzinfo=datetime.timezone.utc
            ),
            referenz_id=None,
        )

    def test_should_parse_the_full_message(self):
        message = load_schema_message(XDF2_DATA / "S1234V1.0.xml")

        assert message.schema == Schema(
            identifier=Identifier("S1234", version=Version("1.0")),
            name="Test",
            fachlicher_ersteller="Test",
            bezeichnung_eingabe="Test",
            bezeichnung_ausgabe=None,
            beschreibung="Eine Beschreibung",
            definition="Eine Definition",
            bezug="Bezug",
            status=Status.AKTIV,
            versionshinweis="Ein Versionshinweis",
            freigabedatum=datetime.date(year=2023, month=1, day=1),
            veroeffentlichungsdatum=datetime.date(year=2023, month=1, day=2),
            gueltig_ab=datetime.date(year=2020, month=11, day=4),
            gueltig_bis=datetime.date(year=2020, month=11, day=5),
            hilfetext=None,
            ableitungsmodifikationen_struktur=AbleitungsmodifikationenStruktur.ALLES_MODIFIZIERBAR,
            ableitungsmodifikationen_repraesentation=AbleitungsmodifikationenRepraesentation.MODIFIZIERBAR,
            struktur=[
                ElementReference(
                    anzahl=Anzahl(1, 1),
                    bezug=None,
                    element_type=ElementType.GRUPPE,
                    identifier=Identifier("G12001", version=Version("1.3")),
                )
            ],
            regeln=[
                Identifier("R12001", Version("1.2")),
            ],
        )

        assert len(message.groups) == 1
        assert message.groups == {
            Identifier("G12001", Version("1.3")): Gruppe(
                identifier=Identifier("G12001", Version("1.3")),
                name="Natürliche Person",
                bezeichnung_eingabe="Natürliche Person",
                bezeichnung_ausgabe="Natürliche Person",
                beschreibung="Eine Beschreibung",
                definition="Eine Definition",
                bezug=None,
                status=Status.AKTIV,
                versionshinweis="Versionshinweis",
                freigabedatum=None,
                veroeffentlichungsdatum=None,
                fachlicher_ersteller="FIM Baustein Datenfelder",
                gueltig_ab=datetime.date(year=2020, month=11, day=4),
                gueltig_bis=datetime.date(year=2020, month=11, day=5),
                hilfetext_eingabe="Hilfetext Eingabe",
                hilfetext_ausgabe="Hilfetext Ausgabe",
                schema_element_art=SchemaElementArt.ABSTRAKT,
                struktur=[
                    ElementReference(
                        anzahl=Anzahl(1, 1),
                        bezug=None,
                        element_type=ElementType.FELD,
                        identifier=Identifier("F12001", Version("1.1")),
                    )
                ],
                regeln=[],
            )
        }

        assert len(message.fields) == 1
        assert message.fields == {
            Identifier("F12001", Version("1.1")): Datenfeld(
                identifier=Identifier("F12001", Version("1.1")),
                name="Familienname",
                bezeichnung_eingabe="Familienname",
                bezeichnung_ausgabe="Familienname",
                beschreibung="Eine Beschreibung",
                definition="Eine Definition",
                bezug="Ein Bezug",
                status=Status.AKTIV,
                versionshinweis=None,
                freigabedatum=datetime.date(year=2020, month=11, day=2),
                veroeffentlichungsdatum=datetime.date(year=2020, month=11, day=3),
                fachlicher_ersteller="FIM-Baustein Datenfelder",
                gueltig_ab=datetime.date(year=2020, month=11, day=4),
                gueltig_bis=datetime.date(year=2020, month=11, day=5),
                hilfetext_eingabe="Hilfe Eingabe",
                hilfetext_ausgabe="Hilfe Ausgabe",
                schema_element_art=SchemaElementArt.HARMONISIERT,
                feldart=Feldart.INPUT,
                datentyp=Datentyp.TEXT,
                praezisierung='{"minLength":"1","maxLength":"120"}',
                inhalt=None,
                code_listen_referenz=None,
                regeln=[],
            )
        }

        assert len(message.rules) == 1
        assert message.rules == {
            Identifier("R12001", Version("1.2")): Regel(
                identifier=Identifier("R12001", Version("1.2")),
                name="MindestEineAngabe",
                bezeichnung_eingabe="MindestEineAngabe",
                bezeichnung_ausgabe=None,
                beschreibung=None,
                definition="Eine Definition",
                bezug=None,
                status=Status.AKTIV,
                versionshinweis=None,
                gueltig_ab=datetime.date(year=2020, month=11, day=4),
                gueltig_bis=datetime.date(year=2020, month=11, day=5),
                fachlicher_ersteller="Bundesredaktion",
                freigabedatum=datetime.date(year=2023, month=1, day=1),
                veroeffentlichungsdatum=datetime.date(year=2023, month=1, day=2),
                script="function script() {}",
            )
        }

    def test_should_parse_select_date_field(self):
        message = load_schema_message(XDF2_DATA / "select.xml")

        assert len(message.fields) == 1
        assert message.fields[Identifier("F123", Version("1.0"))] == Datenfeld(
            identifier=Identifier("F123", Version("1.0")),
            name="Hinweis",
            fachlicher_ersteller="Creator",
            bezeichnung_eingabe="Hinweis",
            bezeichnung_ausgabe="Hinweis",
            beschreibung=None,
            definition=None,
            bezug=None,
            status=Status.AKTIV,
            versionshinweis=None,
            freigabedatum=None,
            veroeffentlichungsdatum=None,
            gueltig_ab=None,
            gueltig_bis=None,
            hilfetext_eingabe=None,
            hilfetext_ausgabe=None,
            schema_element_art=SchemaElementArt.RECHTSNORMGEBUNDEN,
            feldart=Feldart.SELECT,
            datentyp=Datentyp.TEXT,
            praezisierung=None,
            inhalt=None,
            code_listen_referenz=CodeListenReferenz(
                identifier=CodeListIdentifier("C123", None),
                genericode_identifier=genericode.Identifier(
                    canonical_uri="urn:de:example",
                    version="1",
                    canonical_version_uri="urn:de:example_1",
                ),
            ),
            regeln=[],
        )

    def test_should_ignore_empty_tags(self):
        message = load_schema_message(XDF2_DATA / "schema_empty_bezug.xml")

        assert message.schema.bezug is None
        assert message.schema.struktur[0].bezug is None
        assert message.fields[Identifier("F12001", None)].bezug is None

    def test_should_fail_for_invalid_version(self):
        with pytest.raises(
            XdfException,
            match=re.escape(
                "Could not parse schema: Invalid version: invalid_version, line 10"
            ),
        ):
            load_schema_message(XDF2_DATA / "schema_invalid_version.xml")

    def test_should_fail_for_inconsistent_field(self):
        with pytest.raises(
            ParserException,
            match=re.escape("inconsistent Datenfeld [id=F001, version=1.0"),
        ):
            load_schema_message(XDF2_DATA / "schema_inconsistent_field.xml")

    def test_should_fail_for_inconsistent_group(self):
        with pytest.raises(
            ParserException,
            match=re.escape("inconsistent Datenfeldgruppe [id=G001, version=1.3"),
        ):
            load_schema_message(XDF2_DATA / "schema_inconsistent_group.xml")

    def test_should_fail_for_inconsistent_rule(self):
        with pytest.raises(
            ParserException,
            match=re.escape("inconsistent Regel [id=R001, version=1.2"),
        ):
            load_schema_message(XDF2_DATA / "schema_inconsistent_rule.xml")

    def test_should_fail_for_missing_required_attribute(self):
        with pytest.raises(
            XdfException,
            match=re.escape(
                "Could not parse schema: Missing value: {urn:xoev-de:fim:standard:xdatenfelder_2}name, line 7"
            ),
        ):
            load_schema_message(XDF2_DATA / "schema_missing_name.xml")

    def test_should_fail_for_invalid_xml(self):
        with pytest.raises(
            XdfException,
            match=re.escape("Could not parse schema: Invalid xml document, line 16"),
        ):
            load_schema_message(XDF2_DATA / "schema_invalid_xml.xml")

    def test_should_fail_for_invalid_feldart(self):
        with pytest.raises(
            XdfException,
            match=re.escape(
                "Could not parse schema: Invalid Feldart 'invalid', line 54"
            ),
        ):
            load_schema_message(XDF2_DATA / "schema_invalid_feldart.xml")

    def test_should_fail_for_invalid_datentyp(self):
        with pytest.raises(
            XdfException,
            match=re.escape(
                "Could not parse schema: Invalid Datentyp 'unknown', line 57"
            ),
        ):
            load_schema_message(XDF2_DATA / "schema_invalid_datentyp.xml")

    def test_should_fail_for_invalid_datetime(self):
        with pytest.raises(
            XdfException,
            match=re.escape(
                "Could not parse schema: Invalid datetime 'invalid datetime', line 5"
            ),
        ):
            load_schema_message(XDF2_DATA / "schema_invalid_datetime.xml")

    def test_should_fail_for_invalid_date(self):
        with pytest.raises(
            XdfException,
            match=re.escape("Could not parse schema: Invalid date 'invalid', line 48"),
        ):
            load_schema_message(XDF2_DATA / "schema_invalid_date.xml")

    def test_should_fail_for_invalid_schemaelementart(self):
        with pytest.raises(
            XdfException,
            match=re.escape(
                "Could not parse schema: Invalid SchemaElementArt 'invalid', line 49"
            ),
        ):
            load_schema_message(XDF2_DATA / "schema_invalid_schemaelementart.xml")

    def test_should_fail_for_invalid_ableitungsmodifikationen_struktur(self):
        with pytest.raises(
            XdfException,
            match=re.escape(
                "Could not parse schema: Invalid AbleitungsmodifikationenStruktur 'invalid', line 24"
            ),
        ):
            load_schema_message(
                XDF2_DATA / "schema_invalid_ableitungsmodifikationen_struktur.xml"
            )

    def test_should_fail_for_invalid_ableitungsmodifikationen_repraesentation(self):
        with pytest.raises(
            XdfException,
            match=re.escape(
                "Could not parse schema: Invalid AbleitungsmodifikationenRepraesentation 'invalid', line 27"
            ),
        ):
            load_schema_message(
                XDF2_DATA
                / "schema_invalid_ableitungsmodifikationen_repraesentation.xml"
            )

    def test_should_fail_for_invalid_status(self):
        with pytest.raises(
            XdfException,
            match=re.escape(
                "Could not parse schema: Invalid Status 'invalid', line 17"
            ),
        ):
            load_schema_message(XDF2_DATA / "schema_invalid_status.xml")


class TestSerializeSchemaMessage:
    def test_should_serialize_a_minimal_message(self):
        message = Xdf2Factory().schema().message()

        content = serialize_schema_message(message)

        assert parse_schema_message(content) == message

    def test_should_serialize_a_full_message(self):
        message = Xdf2Factory().schema().full().message()

        content = serialize_schema_message(message)

        assert parse_schema_message(content) == message

    @pytest.mark.parametrize(
        "field_factory",
        [
            Xdf2Factory().field().minimal(),
            Xdf2Factory().field().input(Datentyp.TEXT, praezisierung="praezisierung"),
            Xdf2Factory()
            .field()
            .select(
                code_list=CodeListFactory().build(),
            ),
            Xdf2Factory().field().input(Datentyp.ANLAGE),
        ],
    )
    def test_should_serialize_fields(self, field_factory: FieldFactory):
        field = field_factory.build()
        message = field_factory.factory.schema().with_field(field).message()

        content = serialize_schema_message(message)

        assert parse_schema_message(content) == message


def test_should_return_all_referenced_code_lists():
    code_list_1 = CodeListFactory().build()
    code_list_2 = CodeListFactory().build()

    factory = Xdf2Factory()
    field_1 = factory.field().select(code_list=code_list_1).build()
    field_2 = factory.field().select(code_list=code_list_1).build()
    field_3 = factory.field().select(code_list=code_list_2).build()

    message = (
        factory.schema()
        .with_field(field_1)
        .with_field(field_2)
        .with_field(field_3)
        .message()
    )

    referenced_code_lists = message.get_code_list_identifiers()
    assert len(referenced_code_lists) == 2
    assert code_list_1.identifier in referenced_code_lists
    assert code_list_2.identifier in referenced_code_lists
