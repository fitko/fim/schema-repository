from datetime import date

from fastapi.testclient import TestClient
from freezegun import freeze_time

from fimportal.helpers import format_iso8601
from tests.conftest import CommandRunner
from tests.factories.xdf2 import Xdf2Factory


def test_should_return_all_versions_for_a_field(
    runner: CommandRunner, client: TestClient
):
    today = date.today()

    factory = Xdf2Factory()
    field_1 = factory.field(
        id="F12345",
        version="1.0",
        bezug="Bezug",
        gueltig_ab=today,
        gueltig_bis=today,
        freigabedatum=today,
        veroeffentlichungsdatum=today,
    ).build()
    xdf2_import_1 = factory.schema(id="S12345").with_field(field_1).build_import()

    field_2 = factory.field(
        id="F12345",
        version="2.0",
        bezug="Bezug",
        gueltig_ab=today,
        gueltig_bis=today,
        freigabedatum=today,
        veroeffentlichungsdatum=today,
    ).build()
    xdf2_import_2 = factory.schema(id="S12346").with_field(field_2).build_import()

    with freeze_time("2023-10-17T00:00:00"):
        runner.import_xdf2(xdf2_import_1)
        runner.import_xdf2(xdf2_import_2)

    response = client.get("/api/v1/fields/baukasten/F12345")

    assert response.status_code == 200
    assert response.json() == [
        {
            "namespace": "baukasten",
            "fim_id": field_1.identifier.id,
            "fim_version": field_1.identifier.version,
            "nummernkreis": "12000",
            "name": field_1.name,
            "beschreibung": field_1.beschreibung,
            "definition": field_1.definition,
            "bezug": ["Bezug"],
            "freigabe_status": xdf2_import_1.freigabe_status.value,
            "freigabe_status_label": xdf2_import_1.freigabe_status.to_label(),
            "feldart": field_1.feldart.value,
            "datentyp": field_1.datentyp.value,
            "status_gesetzt_durch": field_1.fachlicher_ersteller,
            "status_gesetzt_am": today.isoformat(),
            "versionshinweis": field_1.versionshinweis,
            "veroeffentlichungsdatum": today.isoformat(),
            "gueltig_ab": today.isoformat(),
            "gueltig_bis": today.isoformat(),
            "letzte_aenderung": format_iso8601(
                xdf2_import_1.schema_message.header.erstellungs_zeitpunkt
            ),
            "last_update": "2023-10-17T00:00:00Z",
            "xdf_version": "2.0",
            "fts_match": None,
            "is_latest": False,
        },
        {
            "namespace": "baukasten",
            "fim_id": field_2.identifier.id,
            "fim_version": field_2.identifier.version,
            "nummernkreis": "12000",
            "name": field_2.name,
            "beschreibung": field_2.beschreibung,
            "definition": field_2.definition,
            "bezug": ["Bezug"],
            "freigabe_status": xdf2_import_2.freigabe_status.value,
            "freigabe_status_label": xdf2_import_2.freigabe_status.to_label(),
            "feldart": field_2.feldart.value,
            "datentyp": field_2.datentyp.value,
            "status_gesetzt_durch": field_2.fachlicher_ersteller,
            "status_gesetzt_am": today.isoformat(),
            "versionshinweis": field_2.versionshinweis,
            "veroeffentlichungsdatum": today.isoformat(),
            "gueltig_ab": today.isoformat(),
            "gueltig_bis": today.isoformat(),
            "letzte_aenderung": format_iso8601(
                xdf2_import_2.schema_message.header.erstellungs_zeitpunkt
            ),
            "last_update": "2023-10-17T00:00:00Z",
            "xdf_version": "2.0",
            "fts_match": None,
            "is_latest": True,
        },
    ]


def test_should_return_empty_list_for_unknown_field(client: TestClient):
    response = client.get("/api/v1/fields/baukasten/F12345")

    assert response.status_code == 200
    assert response.json() == []
