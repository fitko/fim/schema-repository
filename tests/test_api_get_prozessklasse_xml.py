from datetime import UTC, datetime
from fastapi.testclient import TestClient
from freezegun import freeze_time
from fimportal import xml
from fimportal.xprozesse.client import TEST_PROZESSKLASSE_XML
from tests.conftest import CommandRunner

from tests.factories.prozess import ProzessklasseFactory

ENDPOINT = "/api/v0/processclasses"


def test_return_existing_prozessklasse(runner: CommandRunner, client: TestClient):
    id = "some prozess id"
    ProzessklasseFactory(id=id).save(runner, xml_content=TEST_PROZESSKLASSE_XML)

    with freeze_time("2023-10-17T00:00:00"):
        response = client.get(f"{ENDPOINT}/{id}/xprozess")

    assert response.status_code == 200

    expected_result = f"""<?xml version='1.0' encoding='utf-8'?>
        <xprozess:alleInhalte.export.0303 xmlns:xprozess="http://www.regierung-mv.de/xprozess/2" produkt="ADONIS" produkthersteller="BOC" xprozessVersion="2.0">
            <xprozess:nachrichtenkopf>
                <xprozess:nachrichtUUID>e05803ea-cbd7-4c97-93f9-0b088388a291</xprozess:nachrichtUUID>
                <xprozess:nachrichtentyp listURI="urn:xoev-de:xprozess:codeliste:nachricht" listVersionID="1.0">
                    <code>0303</code>
                </xprozess:nachrichtentyp>
                <xprozess:erstellungszeitpunkt>{datetime(2023, 10, 17, 0, 0, 0, tzinfo=UTC).isoformat()}</xprozess:erstellungszeitpunkt>
                <xprozess:autor>BOC</xprozess:autor>
            </xprozess:nachrichtenkopf>
            <xprozess:prozesskatalog>
                <xprozess:name>Prozesskatalog (FIM)</xprozess:name>
                <xprozess:prozessklasse>
                   <xprozess:id>00000000000000</xprozess:id>
                   <xprozess:name>Antrag Erteilung Aufenthaltserlaubnis</xprozess:name>
                </xprozess:prozessklasse>
            </xprozess:prozesskatalog>
            <xprozess:prozessbibliothek>
                <xprozess:name>FIM Prozessbibliothek Bund</xprozess:name>
            </xprozess:prozessbibliothek>
        </xprozess:alleInhalte.export.0303>
    """
    assert xml.prettify(response.text) == xml.prettify(expected_result)

    assert (
        response.headers["content-disposition"]
        == f'attachment; filename="{id}_prozessklasse.xprozess.xml"'
    )


def test_fail_for_unknown_prozess(client: TestClient):
    id = "some prozess id"
    response = client.get(f"{ENDPOINT}/{id}/xprozess")

    assert response.status_code == 404
    assert response.json() == {
        "detail": f"Could not find processclass {id}.",
    }
