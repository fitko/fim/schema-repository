from pathlib import Path


DATA_DIR = Path(__file__).parent

XDF2_DATA = DATA_DIR / "xdf2"
XDF3_DATA = DATA_DIR / "xdf3"
XZUFI_2_2_0_DATA = DATA_DIR / "xzufi_2_2_0"
XPROZESS2_DATA = DATA_DIR / "xprozess2"
GENERICODE_DATA = DATA_DIR / "genericode"
